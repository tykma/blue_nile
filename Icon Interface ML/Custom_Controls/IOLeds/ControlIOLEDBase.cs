﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlIOLEDBase.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls.IOLeds
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    /// <summary>
    /// User control for an I/O LED.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public partial class ControlIOLEDBase : UserControl
    {
        /// <summary>
        /// Whether or not the LED is enabled.
        /// </summary>
        private bool enabled;

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlIOLEDBase"/> class.
        /// </summary>
        public ControlIOLEDBase()
        {
            this.InitializeComponent();

            // Resent the picture click event.
            this.ux_Picture_IOLED.Click += this.CtlClick;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is enabled; otherwise, <c>false</c>.
        /// </value>
        [Category("My Properties")]
        [Description("Is LED enabled")]
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public bool IsEnabled
        {
            get
            {
                return this.enabled;
            }

            set
            {
                this.enabled = value;
                this.ux_Picture_IOLED.Image = this.enabled ? this.PictureActive : this.PictureInactive;
            }
        }

        /// <summary>
        /// Gets or sets the led label.
        /// </summary>
        /// <value>
        /// The led label.
        /// </value>
        [Category("My Properties")]
        [Description("Led Number")]
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public string LEDLabel
        {
            get
            {
                return this.ux_Label_IONum.Text;
            }

            set
            {
                this.ux_Label_IONum.Text = value;
            }
        }

        /// <summary>
        /// Gets the active picture.
        /// </summary>
        /// <value>
        /// The active picture.
        /// </value>
        private Image PictureActive => Properties.ImageResources.epb_green_led1;

        /// <summary>
        /// Gets the inactive picture.
        /// </summary>
        /// <value>
        /// The inactive picture.
        /// </value>
        private Image PictureInactive => Properties.ImageResources.gray_led;

        /// <summary>
        /// Set up click control.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void CtlClick(object sender, EventArgs e)
        {
            this.InvokeOnClick(this, EventArgs.Empty);
        }
    }
}
