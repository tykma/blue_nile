﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdDocPreview.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   The command to get a preview.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Drawing;
    using System.Runtime.ExceptionServices;

    /// <summary>
    /// The command preview.
    /// </summary>
    internal class CmdDocPreview : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdDocPreview" /> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking card API.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        public CmdDocPreview(IntPtr lmcApi, int width, int height)
            : base(lmcApi)
        {
            this.Height = height;
            this.Width = width;
        }

        /// <summary>
        /// Get a bitmap preview.
        /// </summary>
        /// <param name="nBmpWidth">Width of the bitmap.</param>
        /// <param name="nBmpHeight">Height of the bitmap.</param>
        /// <returns>
        /// An HBITMAP.
        /// </returns>
        private delegate IntPtr LMC1ApiGetPrevBitmap(int nBmpWidth, int nBmpHeight);

        /// <summary>
        /// Gets the preview height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        private int Height { get; }

        /// <summary>
        /// Gets the preview width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        private int Width { get; }

        /// <summary>
        /// The execute.
        /// </summary>
        /// <returns>
        /// The <see cref="Bitmap"/>.
        /// </returns>
       [HandleProcessCorruptedStateExceptions]
        internal Bitmap Execute()
        {
            var bitmap = this.LoadFunction<LMC1ApiGetPrevBitmap>("lmc1_GetPrevBitmap2");

            if (bitmap != null)
            {
                try
                {
                    var image = bitmap(this.Width, this.Height);

                    if (image != IntPtr.Zero)
                    {
                        var bmp = Image.FromHbitmap(image);
                        NativeMethods.DeleteObject(image);
                        return bmp;
                    }
                }
                catch (AccessViolationException)
                {
                    return new Bitmap(this.Width, this.Height);
                }
                catch (Exception)
                {
                    return new Bitmap(this.Width, this.Height);
                }
            
            }

            return new Bitmap(this.Width, this.Height);
        }
    }
}
