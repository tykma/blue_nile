﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SocketEventMessage.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.HTTPD
{
    using System;

    /// <summary>
    /// Status message to pass to events.
    /// </summary>
    public class SocketEventMessage : EventArgs
    {
        /// <summary>
        /// Gets or sets the CLIENT reference.
        /// </summary>
        /// <value>
        /// The client socket.
        /// </value>
        public ServerClient Client { get; set; }
    }
}
