﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HatchAttributes.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE
{
    /// <summary>
    /// Hatch attributes.
    /// </summary>
    public class HatchEntAttributes
    {
        public HatchEntAttributes(
            bool enableContour,
            int enableHatch,
            bool contourFirst,
            int penNo,
            int hatchType,
            bool allCalc,
            bool hatchEdge,
            bool hatchAverageLine,
            double hatchAngle,
            double hatchLineDist,
            double hatchEdgeDist,
            double hatchStartOffset,
           double hatchEndoffset,
            double dHatchLineReduction,
            double dHatchLoopDist,
            int nEdgeLoop,
            bool hatchLoopRev,
            bool hatchAutoRotate,
            double hatchRotateAngle,
            bool hatchCrossMode,
            int cycleCount)

        {
            this.EnableContour = enableContour;
            this.EnableHatch = enableHatch;
            this.ContourFirst = contourFirst;
            this.PenNumber = penNo;
            this.HatchType = hatchType;
            this.AllCalc = allCalc;
            this.HatchEdge = hatchEdge;
            this.HatchAverageLine = hatchAverageLine;
            this.HatchAngle = hatchAngle;
            this.HatchLineDist = hatchLineDist;
            this.HatchEdgeDist = hatchEdgeDist;
            this.HatchStartOffset = hatchStartOffset;
            this.HatchEndOffset = hatchEndoffset;
            this.HatchLineReduction = dHatchLineReduction;
            this.HatchLoopDistance = dHatchLoopDist;
            this.EdgeLoop = nEdgeLoop;
            this.HatchLoopRev = hatchLoopRev;
            this.HatchAutoRotate = hatchAutoRotate;
            this.HatchRotateAngle = hatchRotateAngle;
            this.HatchCrossMode = hatchCrossMode;
            this.CycleCount = cycleCount;
        }

        public int CycleCount { get; }

        public bool HatchCrossMode { get; }

        public double HatchRotateAngle { get; }

        public bool HatchAutoRotate { get; }

        public bool HatchLoopRev { get; }

        public int EdgeLoop { get; }

        public double HatchLoopDistance { get; }

        public double HatchLineReduction { get; }

        public double HatchEndOffset { get; }

        public double HatchStartOffset { get; }

        public double HatchEdgeDist { get; }

        public double HatchLineDist { get; }

        public double HatchAngle { get; }

        public bool EnableContour { get; }

        public int EnableHatch { get; }

        public bool ContourFirst { get; }

        public int PenNumber { get; }

        public int HatchType { get; }

        public bool AllCalc { get; }

        public bool HatchEdge { get; }

        public bool HatchAverageLine { get; }
    }
}
