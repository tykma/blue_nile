﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlButtonImport.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    using Icon_Interface.Properties;

    /// <summary>
    /// The control button import.
    /// </summary>
    public partial class ControlButtonImport : ControlButtonBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlButtonImport"/> class.
        /// </summary>
        public ControlButtonImport()
        {
            this.InitializeComponent();
            this.Customize();
        }

        /// <summary>
        /// The customize.
        /// </summary>
        private void Customize()
        {
            this.Text = Resources.Label_Import;
        }
    }
}
