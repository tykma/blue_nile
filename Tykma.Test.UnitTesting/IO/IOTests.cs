﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IOTests.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Tykma.Test.UnitTesting.IO
{
    using System;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Core.Engine;

    /// <summary>
    /// IO testing.
    /// </summary>
    [TestClass]
    public class IOTests
    {
        /// <summary>
        /// Verifies all outputs are off on start.
        /// </summary>
        [TestMethod]
        public void VerifyAllOutputsAreOffOnStart()
        {
            var ut = new OutputsMemory();

            Assert.AreEqual(ut.Out1, false);
            Assert.AreEqual(ut.Out2, false);
            Assert.AreEqual(ut.Out3, false);
            Assert.AreEqual(ut.Out4, false);
            Assert.AreEqual(ut.Out5, false);
            Assert.AreEqual(ut.Out6, false);
            Assert.AreEqual(ut.Out7, false);
            Assert.AreEqual(ut.Out8, false);
            Assert.AreEqual(ut.Out9, false);
            Assert.AreEqual(ut.Out10, false);
            Assert.AreEqual(ut.Out11, false);
            Assert.AreEqual(ut.Out12, false);
            Assert.AreEqual(ut.Out13, false);
            Assert.AreEqual(ut.Out14, false);
            Assert.AreEqual(ut.Out15, false);
        }

        /// <summary>
        /// Tests setting out put 1.
        /// </summary>
        [TestMethod]
        public void TestSettingOutput1()
        {
            var ut = new OutputsMemory();
            var iomanip = new OutputManipulation();
            var ioState = iomanip.SetOutputMemory(true, 0, ut);

            Assert.AreEqual(ioState.Out1, true);
            Assert.AreEqual(ioState.Out2, false);
            Assert.AreEqual(ioState.Out3, false);
            Assert.AreEqual(ioState.Out4, false);
            Assert.AreEqual(ioState.Out5, false);
            Assert.AreEqual(ioState.Out6, false);
            Assert.AreEqual(ioState.Out7, false);
            Assert.AreEqual(ioState.Out8, false);
            Assert.AreEqual(ioState.Out9, false);
            Assert.AreEqual(ioState.Out10, false);
            Assert.AreEqual(ioState.Out11, false);
            Assert.AreEqual(ioState.Out12, false);
            Assert.AreEqual(ioState.Out13, false);
            Assert.AreEqual(ioState.Out14, false);
            Assert.AreEqual(ioState.Out15, false);
            Assert.AreEqual(ioState.Out16, false);
        }

        /// <summary>
        /// Tests the setting output 1 twice.
        /// </summary>
        [TestMethod]
        public void TestSettingOutput1Twice()
        {
            var ut = new OutputsMemory();
            var iomanip = new OutputManipulation();
            var ioState = iomanip.SetOutputMemory(true, 0, ut);

            Assert.AreEqual(ioState.Out1, true);
            Assert.AreEqual(ioState.Out2, false);
            Assert.AreEqual(ioState.Out3, false);
            Assert.AreEqual(ioState.Out4, false);
            Assert.AreEqual(ioState.Out5, false);
            Assert.AreEqual(ioState.Out6, false);
            Assert.AreEqual(ioState.Out7, false);
            Assert.AreEqual(ioState.Out8, false);
            Assert.AreEqual(ioState.Out9, false);
            Assert.AreEqual(ioState.Out10, false);
            Assert.AreEqual(ioState.Out11, false);
            Assert.AreEqual(ioState.Out12, false);
            Assert.AreEqual(ioState.Out13, false);
            Assert.AreEqual(ioState.Out14, false);
            Assert.AreEqual(ioState.Out15, false);
            Assert.AreEqual(ioState.Out16, false);

            ioState = iomanip.SetOutputMemory(true, 0, ut);

            Assert.AreEqual(ioState.Out1, true);
            Assert.AreEqual(ioState.Out2, false);
            Assert.AreEqual(ioState.Out3, false);
            Assert.AreEqual(ioState.Out4, false);
            Assert.AreEqual(ioState.Out5, false);
            Assert.AreEqual(ioState.Out6, false);
            Assert.AreEqual(ioState.Out7, false);
            Assert.AreEqual(ioState.Out8, false);
            Assert.AreEqual(ioState.Out9, false);
            Assert.AreEqual(ioState.Out10, false);
            Assert.AreEqual(ioState.Out11, false);
            Assert.AreEqual(ioState.Out12, false);
            Assert.AreEqual(ioState.Out13, false);
            Assert.AreEqual(ioState.Out14, false);
            Assert.AreEqual(ioState.Out15, false);
            Assert.AreEqual(ioState.Out16, false);
        }

        /// <summary>
        /// Tests setting output 2.
        /// </summary>
        [TestMethod]
        public void TestSettingOutput2()
        {
            var ut = new OutputsMemory();
            var iomanip = new OutputManipulation();
            var ioState = iomanip.SetOutputMemory(true, 1, ut);

            Assert.AreEqual(ioState.Out1, false);
            Assert.AreEqual(ioState.Out2, true);
            Assert.AreEqual(ioState.Out3, false);
            Assert.AreEqual(ioState.Out4, false);
            Assert.AreEqual(ioState.Out5, false);
            Assert.AreEqual(ioState.Out6, false);
            Assert.AreEqual(ioState.Out7, false);
            Assert.AreEqual(ioState.Out8, false);
            Assert.AreEqual(ioState.Out9, false);
            Assert.AreEqual(ioState.Out10, false);
            Assert.AreEqual(ioState.Out11, false);
            Assert.AreEqual(ioState.Out12, false);
            Assert.AreEqual(ioState.Out13, false);
            Assert.AreEqual(ioState.Out14, false);
            Assert.AreEqual(ioState.Out15, false);
            Assert.AreEqual(ioState.Out16, false);
        }

        /// <summary>
        /// Tests setting output 3.
        /// </summary>
        [TestMethod]
        public void TestSettingOutput3()
        {
            var ut = new OutputsMemory();
            var iomanip = new OutputManipulation();
            var ioState = iomanip.SetOutputMemory(true, 2, ut);

            Assert.AreEqual(ioState.Out1, false);
            Assert.AreEqual(ioState.Out2, false);
            Assert.AreEqual(ioState.Out3, true);
            Assert.AreEqual(ioState.Out4, false);
            Assert.AreEqual(ioState.Out5, false);
            Assert.AreEqual(ioState.Out6, false);
            Assert.AreEqual(ioState.Out7, false);
            Assert.AreEqual(ioState.Out8, false);
            Assert.AreEqual(ioState.Out9, false);
            Assert.AreEqual(ioState.Out10, false);
            Assert.AreEqual(ioState.Out11, false);
            Assert.AreEqual(ioState.Out12, false);
            Assert.AreEqual(ioState.Out13, false);
            Assert.AreEqual(ioState.Out14, false);
            Assert.AreEqual(ioState.Out15, false);
            Assert.AreEqual(ioState.Out16, false);
        }

        /// <summary>
        /// Tests setting output 4.
        /// </summary>
        [TestMethod]
        public void TestSettingOutput4()
        {
            var ut = new OutputsMemory();
            var iomanip = new OutputManipulation();
            var ioState = iomanip.SetOutputMemory(true, 3, ut);

            Assert.AreEqual(ioState.Out1, false);
            Assert.AreEqual(ioState.Out2, false);
            Assert.AreEqual(ioState.Out3, false);
            Assert.AreEqual(ioState.Out4, true);
            Assert.AreEqual(ioState.Out5, false);
            Assert.AreEqual(ioState.Out6, false);
            Assert.AreEqual(ioState.Out7, false);
            Assert.AreEqual(ioState.Out8, false);
            Assert.AreEqual(ioState.Out9, false);
            Assert.AreEqual(ioState.Out10, false);
            Assert.AreEqual(ioState.Out11, false);
            Assert.AreEqual(ioState.Out12, false);
            Assert.AreEqual(ioState.Out13, false);
            Assert.AreEqual(ioState.Out14, false);
            Assert.AreEqual(ioState.Out15, false);
            Assert.AreEqual(ioState.Out16, false);
        }

        /// <summary>
        /// Tests setting output 3.
        /// </summary>
        [TestMethod]
        public void TestSettingOutput5()
        {
            var ut = new OutputsMemory();
            var iomanip = new OutputManipulation();
            var ioState = iomanip.SetOutputMemory(true, 4, ut);

            Assert.AreEqual(ioState.Out1, false);
            Assert.AreEqual(ioState.Out2, false);
            Assert.AreEqual(ioState.Out3, false);
            Assert.AreEqual(ioState.Out4, false);
            Assert.AreEqual(ioState.Out5, true);
            Assert.AreEqual(ioState.Out6, false);
            Assert.AreEqual(ioState.Out7, false);
            Assert.AreEqual(ioState.Out8, false);
            Assert.AreEqual(ioState.Out9, false);
            Assert.AreEqual(ioState.Out10, false);
            Assert.AreEqual(ioState.Out11, false);
            Assert.AreEqual(ioState.Out12, false);
            Assert.AreEqual(ioState.Out13, false);
            Assert.AreEqual(ioState.Out14, false);
            Assert.AreEqual(ioState.Out15, false);
            Assert.AreEqual(ioState.Out16, false);
        }

        /// <summary>
        /// Tests setting output 63.
        /// </summary>
        [TestMethod]
        public void TestSettingOutput6()
        {
            var ut = new OutputsMemory();
            var iomanip = new OutputManipulation();
            var ioState = iomanip.SetOutputMemory(true, 5, ut);

            Assert.AreEqual(ioState.Out1, false);
            Assert.AreEqual(ioState.Out2, false);
            Assert.AreEqual(ioState.Out3, false);
            Assert.AreEqual(ioState.Out4, false);
            Assert.AreEqual(ioState.Out5, false);
            Assert.AreEqual(ioState.Out6, true);
            Assert.AreEqual(ioState.Out7, false);
            Assert.AreEqual(ioState.Out8, false);
            Assert.AreEqual(ioState.Out9, false);
            Assert.AreEqual(ioState.Out10, false);
            Assert.AreEqual(ioState.Out11, false);
            Assert.AreEqual(ioState.Out12, false);
            Assert.AreEqual(ioState.Out13, false);
            Assert.AreEqual(ioState.Out14, false);
            Assert.AreEqual(ioState.Out15, false);
            Assert.AreEqual(ioState.Out16, false);
        }

        /// <summary>
        /// Tests setting output 7.
        /// </summary>
        [TestMethod]
        public void TestSettingOutput7()
        {
            var ut = new OutputsMemory();
            var iomanip = new OutputManipulation();
            var ioState = iomanip.SetOutputMemory(true, 6, ut);
            Assert.AreEqual(ioState.Out1, false);
            Assert.AreEqual(ioState.Out2, false);
            Assert.AreEqual(ioState.Out3, false);
            Assert.AreEqual(ioState.Out4, false);
            Assert.AreEqual(ioState.Out5, false);
            Assert.AreEqual(ioState.Out6, false);
            Assert.AreEqual(ioState.Out7, true);
            Assert.AreEqual(ioState.Out8, false);
            Assert.AreEqual(ioState.Out9, false);
            Assert.AreEqual(ioState.Out10, false);
            Assert.AreEqual(ioState.Out11, false);
            Assert.AreEqual(ioState.Out12, false);
            Assert.AreEqual(ioState.Out13, false);
            Assert.AreEqual(ioState.Out14, false);
            Assert.AreEqual(ioState.Out15, false);
            Assert.AreEqual(ioState.Out16, false);
        }

        /// <summary>
        /// Tests setting output 8.
        /// </summary>
        [TestMethod]
        public void TestSettingOutput8()
        {
            var ut = new OutputsMemory();
            var iomanip = new OutputManipulation();
            var ioState = iomanip.SetOutputMemory(true, 7, ut);
            Assert.AreEqual(ioState.Out1, false);
            Assert.AreEqual(ioState.Out2, false);
            Assert.AreEqual(ioState.Out3, false);
            Assert.AreEqual(ioState.Out4, false);
            Assert.AreEqual(ioState.Out5, false);
            Assert.AreEqual(ioState.Out6, false);
            Assert.AreEqual(ioState.Out7, false);
            Assert.AreEqual(ioState.Out8, true);
            Assert.AreEqual(ioState.Out9, false);
            Assert.AreEqual(ioState.Out10, false);
            Assert.AreEqual(ioState.Out11, false);
            Assert.AreEqual(ioState.Out12, false);
            Assert.AreEqual(ioState.Out13, false);
            Assert.AreEqual(ioState.Out14, false);
            Assert.AreEqual(ioState.Out15, false);
            Assert.AreEqual(ioState.Out16, false);
        }

        /// <summary>
        /// Tests setting output 9.
        /// </summary>
        [TestMethod]
        public void TestSettingOutput9()
        {
            var ut = new OutputsMemory();
            var iomanip = new OutputManipulation();
            var ioState = iomanip.SetOutputMemory(true, 8, ut);
            Assert.AreEqual(ioState.Out1, false);
            Assert.AreEqual(ioState.Out2, false);
            Assert.AreEqual(ioState.Out3, false);
            Assert.AreEqual(ioState.Out4, false);
            Assert.AreEqual(ioState.Out5, false);
            Assert.AreEqual(ioState.Out6, false);
            Assert.AreEqual(ioState.Out7, false);
            Assert.AreEqual(ioState.Out8, false);
            Assert.AreEqual(ioState.Out9, true); // *
            Assert.AreEqual(ioState.Out10, false);
            Assert.AreEqual(ioState.Out11, false);
            Assert.AreEqual(ioState.Out12, false);
            Assert.AreEqual(ioState.Out13, false);
            Assert.AreEqual(ioState.Out14, false);
            Assert.AreEqual(ioState.Out15, false);
            Assert.AreEqual(ioState.Out16, false);
        }

        /// <summary>
        /// Tests setting output 10.
        /// </summary>
        [TestMethod]
        public void TestSettingOutput10()
        {
            var ut = new OutputsMemory();
            var iomanip = new OutputManipulation();
            var ioState = iomanip.SetOutputMemory(true, 9, ut);
            Assert.AreEqual(ioState.Out1, false);
            Assert.AreEqual(ioState.Out2, false);
            Assert.AreEqual(ioState.Out3, false);
            Assert.AreEqual(ioState.Out4, false);
            Assert.AreEqual(ioState.Out5, false);
            Assert.AreEqual(ioState.Out6, false);
            Assert.AreEqual(ioState.Out7, false);
            Assert.AreEqual(ioState.Out8, false);
            Assert.AreEqual(ioState.Out9, false);
            Assert.AreEqual(ioState.Out10, true); // *
            Assert.AreEqual(ioState.Out11, false);
            Assert.AreEqual(ioState.Out12, false);
            Assert.AreEqual(ioState.Out13, false);
            Assert.AreEqual(ioState.Out14, false);
            Assert.AreEqual(ioState.Out15, false);
            Assert.AreEqual(ioState.Out16, false);
        }

        /// <summary>
        /// Tests setting output 11.
        /// </summary>
        [TestMethod]
        public void TestSettingOutput11()
        {
            var ut = new OutputsMemory();
            var iomanip = new OutputManipulation();
            var ioState = iomanip.SetOutputMemory(true, 10, ut);
            Assert.AreEqual(ioState.Out1, false);
            Assert.AreEqual(ioState.Out2, false);
            Assert.AreEqual(ioState.Out3, false);
            Assert.AreEqual(ioState.Out4, false);
            Assert.AreEqual(ioState.Out5, false);
            Assert.AreEqual(ioState.Out6, false);
            Assert.AreEqual(ioState.Out7, false);
            Assert.AreEqual(ioState.Out8, false);
            Assert.AreEqual(ioState.Out9, false);
            Assert.AreEqual(ioState.Out10, false);
            Assert.AreEqual(ioState.Out11, true);
            Assert.AreEqual(ioState.Out12, false);
            Assert.AreEqual(ioState.Out13, false);
            Assert.AreEqual(ioState.Out14, false);
            Assert.AreEqual(ioState.Out15, false);
            Assert.AreEqual(ioState.Out16, false);
        }

        /// <summary>
        /// Tests setting output 12.
        /// </summary>
        [TestMethod]
        public void TestSettingOutput12()
        {
            var ut = new OutputsMemory();
            var iomanip = new OutputManipulation();
            var ioState = iomanip.SetOutputMemory(true, 11, ut);
            Assert.AreEqual(ioState.Out1, false);
            Assert.AreEqual(ioState.Out2, false);
            Assert.AreEqual(ioState.Out3, false);
            Assert.AreEqual(ioState.Out4, false);
            Assert.AreEqual(ioState.Out5, false);
            Assert.AreEqual(ioState.Out6, false);
            Assert.AreEqual(ioState.Out7, false);
            Assert.AreEqual(ioState.Out8, false);
            Assert.AreEqual(ioState.Out9, false);
            Assert.AreEqual(ioState.Out10, false);
            Assert.AreEqual(ioState.Out11, false);
            Assert.AreEqual(ioState.Out12, true);
            Assert.AreEqual(ioState.Out13, false);
            Assert.AreEqual(ioState.Out14, false);
            Assert.AreEqual(ioState.Out15, false);
            Assert.AreEqual(ioState.Out16, false);
        }

        /// <summary>
        /// Tests setting output 13.
        /// </summary>
        [TestMethod]
        public void TestSettingOutput13()
        {
            var ut = new OutputsMemory();
            var iomanip = new OutputManipulation();
            var ioState = iomanip.SetOutputMemory(true, 12, ut);
            Assert.AreEqual(ioState.Out1, false);
            Assert.AreEqual(ioState.Out2, false);
            Assert.AreEqual(ioState.Out3, false);
            Assert.AreEqual(ioState.Out4, false);
            Assert.AreEqual(ioState.Out5, false);
            Assert.AreEqual(ioState.Out6, false);
            Assert.AreEqual(ioState.Out7, false);
            Assert.AreEqual(ioState.Out8, false);
            Assert.AreEqual(ioState.Out9, false);
            Assert.AreEqual(ioState.Out10, false);
            Assert.AreEqual(ioState.Out11, false);
            Assert.AreEqual(ioState.Out12, false);
            Assert.AreEqual(ioState.Out13, true);
            Assert.AreEqual(ioState.Out14, false);
            Assert.AreEqual(ioState.Out15, false);
            Assert.AreEqual(ioState.Out16, false);
        }

        /// <summary>
        /// Tests setting output 14.
        /// </summary>
        [TestMethod]
        public void TestSettingOutput14()
        {
            var ut = new OutputsMemory();
            var iomanip = new OutputManipulation();
            var ioState = iomanip.SetOutputMemory(true, 13, ut);
            Assert.AreEqual(ioState.Out1, false);
            Assert.AreEqual(ioState.Out2, false);
            Assert.AreEqual(ioState.Out3, false);
            Assert.AreEqual(ioState.Out4, false);
            Assert.AreEqual(ioState.Out5, false);
            Assert.AreEqual(ioState.Out6, false);
            Assert.AreEqual(ioState.Out7, false);
            Assert.AreEqual(ioState.Out8, false);
            Assert.AreEqual(ioState.Out9, false);
            Assert.AreEqual(ioState.Out10, false);
            Assert.AreEqual(ioState.Out11, false);
            Assert.AreEqual(ioState.Out12, false);
            Assert.AreEqual(ioState.Out13, false);
            Assert.AreEqual(ioState.Out14, true); // *
            Assert.AreEqual(ioState.Out15, false);
            Assert.AreEqual(ioState.Out16, false);
        }

        /// <summary>
        /// Tests setting output 15.
        /// </summary>
        [TestMethod]
        public void TestSettingOutput15()
        {
            var ut = new OutputsMemory();
            var iomanip = new OutputManipulation();
            var ioState = iomanip.SetOutputMemory(true, 14, ut);
            Assert.AreEqual(ioState.Out1, false);
            Assert.AreEqual(ioState.Out2, false);
            Assert.AreEqual(ioState.Out3, false);
            Assert.AreEqual(ioState.Out4, false);
            Assert.AreEqual(ioState.Out5, false);
            Assert.AreEqual(ioState.Out6, false);
            Assert.AreEqual(ioState.Out7, false);
            Assert.AreEqual(ioState.Out8, false);
            Assert.AreEqual(ioState.Out9, false);
            Assert.AreEqual(ioState.Out10, false);
            Assert.AreEqual(ioState.Out11, false);
            Assert.AreEqual(ioState.Out12, false);
            Assert.AreEqual(ioState.Out13, false);
            Assert.AreEqual(ioState.Out14, false);
            Assert.AreEqual(ioState.Out15, true); // *
            Assert.AreEqual(ioState.Out16, false);
        }

        /// <summary>
        /// Tests setting output 16.
        /// </summary>
        [TestMethod]
        public void TestSettingOutput16()
        {
            var ut = new OutputsMemory();
            var iomanip = new OutputManipulation();
            var ioState = iomanip.SetOutputMemory(true, 15, ut);
            Assert.AreEqual(ioState.Out1, false);
            Assert.AreEqual(ioState.Out2, false);
            Assert.AreEqual(ioState.Out3, false);
            Assert.AreEqual(ioState.Out4, false);
            Assert.AreEqual(ioState.Out5, false);
            Assert.AreEqual(ioState.Out6, false);
            Assert.AreEqual(ioState.Out7, false);
            Assert.AreEqual(ioState.Out8, false);
            Assert.AreEqual(ioState.Out9, false);
            Assert.AreEqual(ioState.Out10, false);
            Assert.AreEqual(ioState.Out11, false);
            Assert.AreEqual(ioState.Out12, false);
            Assert.AreEqual(ioState.Out13, false);
            Assert.AreEqual(ioState.Out14, false);
            Assert.AreEqual(ioState.Out15, false);
            Assert.AreEqual(ioState.Out16, true); // *
        }

        /// <summary>
        /// Tests for proper output words words.
        /// </summary>
        [TestMethod]
        public void TestForProperOutputWordsWords()
        {
            for (int i = 1; i <= 16; i++)
            {
                var ut = new OutputManipulation();
                var porttowrite = ut.GeneratePortWordToWrite(true, i, new OutputsMemory());
                Assert.AreEqual(Math.Pow(2, i - 1), porttowrite);
            }
        }

        /// <summary>
        /// Tests the port for setting output1 twice.
        /// </summary>
        [TestMethod]
        public void TestPortForSettingOutput1Twice()
        {
            var iomanip = new OutputManipulation();
            var outputsState = new OutputsMemory();

            iomanip.GeneratePortWordToWrite(true, 1, outputsState);

            // We'll set our output memory.
            outputsState = iomanip.SetOutputMemory(true, 1 - 1, outputsState);
            var datatoWrite2 = iomanip.GeneratePortWordToWrite(true, 1, outputsState);

            Assert.AreEqual(1, datatoWrite2);
        }

        /// <summary>
        /// Tests for proper output words extended.
        /// </summary>
        [TestMethod]
        public void TestForProperOutputwordsExtended()
        {
            OutputsMemory memory = new OutputsMemory();

            var ut = new OutputManipulation();

            Assert.AreEqual(1, ut.GeneratePortWordToWrite(true, 1, memory));
            memory = ut.SetOutputMemory(true, 0, memory);

            Assert.AreEqual(3, ut.GeneratePortWordToWrite(true, 2, memory));
            memory = ut.SetOutputMemory(true, 1, memory);

            Assert.AreEqual(7, ut.GeneratePortWordToWrite(true, 3, memory));
            memory = ut.SetOutputMemory(true, 2, memory);

            Assert.AreEqual(15, ut.GeneratePortWordToWrite(true, 4, memory));
            memory = ut.SetOutputMemory(true, 3, memory);

            Assert.AreEqual(31, ut.GeneratePortWordToWrite(true, 5, memory));
            memory = ut.SetOutputMemory(true, 4, memory);

            Assert.AreEqual(63, ut.GeneratePortWordToWrite(true, 6, memory));
            memory = ut.SetOutputMemory(true, 5, memory);

            Assert.AreEqual(127, ut.GeneratePortWordToWrite(true, 7, memory));
            memory = ut.SetOutputMemory(true, 6, memory);

            Assert.AreEqual(255, ut.GeneratePortWordToWrite(true, 8, memory));
            memory = ut.SetOutputMemory(true, 7, memory);

            Assert.AreEqual(511, ut.GeneratePortWordToWrite(true, 9, memory));
            memory = ut.SetOutputMemory(true, 8, memory);

            Assert.AreEqual(1023, ut.GeneratePortWordToWrite(true, 10, memory));
            memory = ut.SetOutputMemory(true, 9, memory);

            Assert.AreEqual(2047, ut.GeneratePortWordToWrite(true, 11, memory));
            memory = ut.SetOutputMemory(true, 10, memory);

            Assert.AreEqual(4095, ut.GeneratePortWordToWrite(true, 12, memory));
            memory = ut.SetOutputMemory(true, 11, memory);

            Assert.AreEqual(8191, ut.GeneratePortWordToWrite(true, 13, memory));
            memory = ut.SetOutputMemory(true, 12, memory);

            Assert.AreEqual(16383, ut.GeneratePortWordToWrite(true, 14, memory));
            memory = ut.SetOutputMemory(true, 13, memory);

            Assert.AreEqual(32767, ut.GeneratePortWordToWrite(true, 15, memory));
            memory = ut.SetOutputMemory(true, 14, memory);

            Assert.AreEqual(65535, ut.GeneratePortWordToWrite(true, 16, memory));
            ut.SetOutputMemory(true, 15, memory);
        }

        /// <summary>
        /// Tests the setting and unsetting of outputs.
        /// </summary>
        [TestMethod]
        public void TestSettingAndUnsettingOutputs()
        {
            OutputsMemory memory = new OutputsMemory();

            var ut = new OutputManipulation();

            Assert.AreEqual(128, ut.GeneratePortWordToWrite(true, 8, memory));
            memory = ut.SetOutputMemory(true, 7, memory);

            Assert.AreEqual(0, ut.GeneratePortWordToWrite(false, 8, memory));
            ut.SetOutputMemory(false, 7, memory);
        }
    }
}
