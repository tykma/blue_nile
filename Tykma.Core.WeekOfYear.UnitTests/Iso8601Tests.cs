﻿// <copyright file="Iso8601Tests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Tykma.Core.WeekOfYear.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// ISO 8601 week number tests.
    /// </summary>
    [TestClass]
    public class Iso8601Tests
    {
        /// <summary>
        /// Test ISO8601 year 2016.
        /// </summary>
        [TestMethod]
        public void Test2016()
        {
            IWeekOfYear weekofyear = new Iso8601();

            // Week 01 January 4, 2016 January 10, 2016
            Assert.AreEqual(1, weekofyear.GetWeek(new DateTime(2016, 01, 04)));
            Assert.AreEqual(1, weekofyear.GetWeek(new DateTime(2016, 01, 05)));
            Assert.AreEqual(1, weekofyear.GetWeek(new DateTime(2016, 01, 06)));
            Assert.AreEqual(1, weekofyear.GetWeek(new DateTime(2016, 01, 07)));
            Assert.AreEqual(1, weekofyear.GetWeek(new DateTime(2016, 01, 08)));
            Assert.AreEqual(1, weekofyear.GetWeek(new DateTime(2016, 01, 09)));
            Assert.AreEqual(1, weekofyear.GetWeek(new DateTime(2016, 01, 10)));

            // Week 02 January 11, 2016    January 17, 2016
            Assert.AreEqual(2, weekofyear.GetWeek(new DateTime(2016, 01, 11)));
            Assert.AreEqual(2, weekofyear.GetWeek(new DateTime(2016, 01, 12)));
            Assert.AreEqual(2, weekofyear.GetWeek(new DateTime(2016, 01, 13)));
            Assert.AreEqual(2, weekofyear.GetWeek(new DateTime(2016, 01, 14)));
            Assert.AreEqual(2, weekofyear.GetWeek(new DateTime(2016, 01, 15)));
            Assert.AreEqual(2, weekofyear.GetWeek(new DateTime(2016, 01, 16)));
            Assert.AreEqual(2, weekofyear.GetWeek(new DateTime(2016, 01, 17)));

            // Week 03  January 18, 2016    January 24, 2016
            Assert.AreEqual(3, weekofyear.GetWeek(new DateTime(2016, 01, 18)));
            Assert.AreEqual(3, weekofyear.GetWeek(new DateTime(2016, 01, 19)));
            Assert.AreEqual(3, weekofyear.GetWeek(new DateTime(2016, 01, 20)));
            Assert.AreEqual(3, weekofyear.GetWeek(new DateTime(2016, 01, 21)));
            Assert.AreEqual(3, weekofyear.GetWeek(new DateTime(2016, 01, 22)));
            Assert.AreEqual(3, weekofyear.GetWeek(new DateTime(2016, 01, 23)));
            Assert.AreEqual(3, weekofyear.GetWeek(new DateTime(2016, 01, 24)));

            // Week 04  January 25, 2016    January 31, 2016
            Assert.AreEqual(4, weekofyear.GetWeek(new DateTime(2016, 01, 25)));
            Assert.AreEqual(4, weekofyear.GetWeek(new DateTime(2016, 01, 26)));
            Assert.AreEqual(4, weekofyear.GetWeek(new DateTime(2016, 01, 27)));
            Assert.AreEqual(4, weekofyear.GetWeek(new DateTime(2016, 01, 28)));
            Assert.AreEqual(4, weekofyear.GetWeek(new DateTime(2016, 01, 29)));
            Assert.AreEqual(4, weekofyear.GetWeek(new DateTime(2016, 01, 30)));
            Assert.AreEqual(4, weekofyear.GetWeek(new DateTime(2016, 01, 31)));

            // Week 05  February 1, 2016    February 7, 2016
            Assert.AreEqual(5, weekofyear.GetWeek(new DateTime(2016, 02, 1)));
            Assert.AreEqual(5, weekofyear.GetWeek(new DateTime(2016, 02, 2)));
            Assert.AreEqual(5, weekofyear.GetWeek(new DateTime(2016, 02, 7)));
            Assert.AreEqual(5, weekofyear.GetWeek(new DateTime(2016, 02, 4)));
            Assert.AreEqual(5, weekofyear.GetWeek(new DateTime(2016, 02, 5)));
            Assert.AreEqual(5, weekofyear.GetWeek(new DateTime(2016, 02, 6)));
            Assert.AreEqual(5, weekofyear.GetWeek(new DateTime(2016, 02, 7)));

            // Week 06 February 8, 2016    February 14, 2016
            Assert.AreEqual(6, weekofyear.GetWeek(new DateTime(2016, 02, 8)));
            Assert.AreEqual(6, weekofyear.GetWeek(new DateTime(2016, 02, 9)));
            Assert.AreEqual(6, weekofyear.GetWeek(new DateTime(2016, 02, 10)));
            Assert.AreEqual(6, weekofyear.GetWeek(new DateTime(2016, 02, 11)));
            Assert.AreEqual(6, weekofyear.GetWeek(new DateTime(2016, 02, 12)));
            Assert.AreEqual(6, weekofyear.GetWeek(new DateTime(2016, 02, 13)));
            Assert.AreEqual(6, weekofyear.GetWeek(new DateTime(2016, 02, 14)));

            // Week 07  February 15, 2016   February 21, 2016
            Assert.AreEqual(7, weekofyear.GetWeek(new DateTime(2016, 02, 15)));
            Assert.AreEqual(7, weekofyear.GetWeek(new DateTime(2016, 02, 16)));
            Assert.AreEqual(7, weekofyear.GetWeek(new DateTime(2016, 02, 17)));
            Assert.AreEqual(7, weekofyear.GetWeek(new DateTime(2016, 02, 18)));
            Assert.AreEqual(7, weekofyear.GetWeek(new DateTime(2016, 02, 19)));
            Assert.AreEqual(7, weekofyear.GetWeek(new DateTime(2016, 02, 20)));
            Assert.AreEqual(7, weekofyear.GetWeek(new DateTime(2016, 02, 21)));

            // Week 08  February 22, 2016   February 28, 2016
            Assert.AreEqual(8, weekofyear.GetWeek(new DateTime(2016, 02, 22)));
            Assert.AreEqual(8, weekofyear.GetWeek(new DateTime(2016, 02, 23)));
            Assert.AreEqual(8, weekofyear.GetWeek(new DateTime(2016, 02, 24)));
            Assert.AreEqual(8, weekofyear.GetWeek(new DateTime(2016, 02, 25)));
            Assert.AreEqual(8, weekofyear.GetWeek(new DateTime(2016, 02, 26)));
            Assert.AreEqual(8, weekofyear.GetWeek(new DateTime(2016, 02, 27)));
            Assert.AreEqual(8, weekofyear.GetWeek(new DateTime(2016, 02, 28)));

            // Week 09 February 29, 2016   March 6, 2016
            Assert.AreEqual(9, weekofyear.GetWeek(new DateTime(2016, 02, 29)));
            Assert.AreEqual(9, weekofyear.GetWeek(new DateTime(2016, 03, 01)));
            Assert.AreEqual(9, weekofyear.GetWeek(new DateTime(2016, 03, 02)));
            Assert.AreEqual(9, weekofyear.GetWeek(new DateTime(2016, 03, 03)));
            Assert.AreEqual(9, weekofyear.GetWeek(new DateTime(2016, 03, 04)));
            Assert.AreEqual(9, weekofyear.GetWeek(new DateTime(2016, 03, 05)));
            Assert.AreEqual(9, weekofyear.GetWeek(new DateTime(2016, 03, 06)));

            // Week 10  March 7, 2016   March 13, 2016
            Assert.AreEqual(10, weekofyear.GetWeek(new DateTime(2016, 03, 07)));
            Assert.AreEqual(10, weekofyear.GetWeek(new DateTime(2016, 03, 08)));
            Assert.AreEqual(10, weekofyear.GetWeek(new DateTime(2016, 03, 09)));
            Assert.AreEqual(10, weekofyear.GetWeek(new DateTime(2016, 03, 10)));
            Assert.AreEqual(10, weekofyear.GetWeek(new DateTime(2016, 03, 11)));
            Assert.AreEqual(10, weekofyear.GetWeek(new DateTime(2016, 03, 12)));
            Assert.AreEqual(10, weekofyear.GetWeek(new DateTime(2016, 03, 13)));

            // Week 11 March 14, 2016  March 20, 2016
            Assert.AreEqual(11, weekofyear.GetWeek(new DateTime(2016, 03, 14)));
            Assert.AreEqual(11, weekofyear.GetWeek(new DateTime(2016, 03, 15)));
            Assert.AreEqual(11, weekofyear.GetWeek(new DateTime(2016, 03, 16)));
            Assert.AreEqual(11, weekofyear.GetWeek(new DateTime(2016, 03, 17)));
            Assert.AreEqual(11, weekofyear.GetWeek(new DateTime(2016, 03, 18)));
            Assert.AreEqual(11, weekofyear.GetWeek(new DateTime(2016, 03, 19)));
            Assert.AreEqual(11, weekofyear.GetWeek(new DateTime(2016, 03, 20)));

            // Week 12 March 21, 2016  March 27, 2016
            Assert.AreEqual(12, weekofyear.GetWeek(new DateTime(2016, 03, 21)));
            Assert.AreEqual(12, weekofyear.GetWeek(new DateTime(2016, 03, 22)));
            Assert.AreEqual(12, weekofyear.GetWeek(new DateTime(2016, 03, 23)));
            Assert.AreEqual(12, weekofyear.GetWeek(new DateTime(2016, 03, 24)));
            Assert.AreEqual(12, weekofyear.GetWeek(new DateTime(2016, 03, 25)));
            Assert.AreEqual(12, weekofyear.GetWeek(new DateTime(2016, 03, 26)));
            Assert.AreEqual(12, weekofyear.GetWeek(new DateTime(2016, 03, 27)));

            // Week 13  March 28, 2016  April 3, 2016
            Assert.AreEqual(13, weekofyear.GetWeek(new DateTime(2016, 03, 28)));
            Assert.AreEqual(13, weekofyear.GetWeek(new DateTime(2016, 03, 29)));
            Assert.AreEqual(13, weekofyear.GetWeek(new DateTime(2016, 03, 30)));
            Assert.AreEqual(13, weekofyear.GetWeek(new DateTime(2016, 03, 31)));
            Assert.AreEqual(13, weekofyear.GetWeek(new DateTime(2016, 04, 01)));
            Assert.AreEqual(13, weekofyear.GetWeek(new DateTime(2016, 04, 02)));
            Assert.AreEqual(13, weekofyear.GetWeek(new DateTime(2016, 04, 03)));

            // Week 14  April 4, 2016   April 10, 2016
            Assert.AreEqual(14, weekofyear.GetWeek(new DateTime(2016, 04, 04)));
            Assert.AreEqual(14, weekofyear.GetWeek(new DateTime(2016, 04, 05)));
            Assert.AreEqual(14, weekofyear.GetWeek(new DateTime(2016, 04, 06)));
            Assert.AreEqual(14, weekofyear.GetWeek(new DateTime(2016, 04, 07)));
            Assert.AreEqual(14, weekofyear.GetWeek(new DateTime(2016, 04, 09)));
            Assert.AreEqual(14, weekofyear.GetWeek(new DateTime(2016, 04, 09)));
            Assert.AreEqual(14, weekofyear.GetWeek(new DateTime(2016, 04, 10)));

            // Week 15   April 11, 2016  April 17, 2016
            Assert.AreEqual(15, weekofyear.GetWeek(new DateTime(2016, 04, 11)));
            Assert.AreEqual(15, weekofyear.GetWeek(new DateTime(2016, 04, 12)));
            Assert.AreEqual(15, weekofyear.GetWeek(new DateTime(2016, 04, 13)));
            Assert.AreEqual(15, weekofyear.GetWeek(new DateTime(2016, 04, 14)));
            Assert.AreEqual(15, weekofyear.GetWeek(new DateTime(2016, 04, 15)));
            Assert.AreEqual(15, weekofyear.GetWeek(new DateTime(2016, 04, 16)));
            Assert.AreEqual(15, weekofyear.GetWeek(new DateTime(2016, 04, 17)));

            // Week 16  April 18, 2016  April 24, 2016
            Assert.AreEqual(16, weekofyear.GetWeek(new DateTime(2016, 04, 18)));
            Assert.AreEqual(16, weekofyear.GetWeek(new DateTime(2016, 04, 19)));
            Assert.AreEqual(16, weekofyear.GetWeek(new DateTime(2016, 04, 20)));
            Assert.AreEqual(16, weekofyear.GetWeek(new DateTime(2016, 04, 21)));
            Assert.AreEqual(16, weekofyear.GetWeek(new DateTime(2016, 04, 22)));
            Assert.AreEqual(16, weekofyear.GetWeek(new DateTime(2016, 04, 23)));
            Assert.AreEqual(16, weekofyear.GetWeek(new DateTime(2016, 04, 24)));

            // Week 17 April 25, 2016  May 1, 2016
            Assert.AreEqual(17, weekofyear.GetWeek(new DateTime(2016, 04, 25)));
            Assert.AreEqual(17, weekofyear.GetWeek(new DateTime(2016, 04, 26)));
            Assert.AreEqual(17, weekofyear.GetWeek(new DateTime(2016, 04, 27)));
            Assert.AreEqual(17, weekofyear.GetWeek(new DateTime(2016, 04, 28)));
            Assert.AreEqual(17, weekofyear.GetWeek(new DateTime(2016, 04, 29)));
            Assert.AreEqual(17, weekofyear.GetWeek(new DateTime(2016, 04, 30)));
            Assert.AreEqual(17, weekofyear.GetWeek(new DateTime(2016, 05, 01)));

            // Week 18  May 2, 2016 May 8, 2016
            Assert.AreEqual(18, weekofyear.GetWeek(new DateTime(2016, 05, 02)));
            Assert.AreEqual(18, weekofyear.GetWeek(new DateTime(2016, 05, 03)));
            Assert.AreEqual(18, weekofyear.GetWeek(new DateTime(2016, 05, 04)));
            Assert.AreEqual(18, weekofyear.GetWeek(new DateTime(2016, 05, 05)));
            Assert.AreEqual(18, weekofyear.GetWeek(new DateTime(2016, 05, 06)));
            Assert.AreEqual(18, weekofyear.GetWeek(new DateTime(2016, 05, 07)));
            Assert.AreEqual(18, weekofyear.GetWeek(new DateTime(2016, 05, 08)));

            // Week 19  May 9, 2016 May 15, 2016
            Assert.AreEqual(19, weekofyear.GetWeek(new DateTime(2016, 05, 09)));
            Assert.AreEqual(19, weekofyear.GetWeek(new DateTime(2016, 05, 10)));
            Assert.AreEqual(19, weekofyear.GetWeek(new DateTime(2016, 05, 11)));
            Assert.AreEqual(19, weekofyear.GetWeek(new DateTime(2016, 05, 12)));
            Assert.AreEqual(19, weekofyear.GetWeek(new DateTime(2016, 05, 13)));
            Assert.AreEqual(19, weekofyear.GetWeek(new DateTime(2016, 05, 14)));
            Assert.AreEqual(19, weekofyear.GetWeek(new DateTime(2016, 05, 15)));

            // Week 20 May 16, 2016    May 22, 2016
            Assert.AreEqual(20, weekofyear.GetWeek(new DateTime(2016, 05, 16)));
            Assert.AreEqual(20, weekofyear.GetWeek(new DateTime(2016, 05, 17)));
            Assert.AreEqual(20, weekofyear.GetWeek(new DateTime(2016, 05, 18)));
            Assert.AreEqual(20, weekofyear.GetWeek(new DateTime(2016, 05, 19)));
            Assert.AreEqual(20, weekofyear.GetWeek(new DateTime(2016, 05, 20)));
            Assert.AreEqual(20, weekofyear.GetWeek(new DateTime(2016, 05, 21)));
            Assert.AreEqual(20, weekofyear.GetWeek(new DateTime(2016, 05, 22)));

            // Week 21 May 23, 2016    May 29, 2016
            Assert.AreEqual(21, weekofyear.GetWeek(new DateTime(2016, 05, 23)));
            Assert.AreEqual(21, weekofyear.GetWeek(new DateTime(2016, 05, 24)));
            Assert.AreEqual(21, weekofyear.GetWeek(new DateTime(2016, 05, 25)));
            Assert.AreEqual(21, weekofyear.GetWeek(new DateTime(2016, 05, 26)));
            Assert.AreEqual(21, weekofyear.GetWeek(new DateTime(2016, 05, 27)));
            Assert.AreEqual(21, weekofyear.GetWeek(new DateTime(2016, 05, 28)));
            Assert.AreEqual(21, weekofyear.GetWeek(new DateTime(2016, 05, 29)));

            // Week 22 May 30, 2016    June 5, 2016
            Assert.AreEqual(22, weekofyear.GetWeek(new DateTime(2016, 05, 30)));
            Assert.AreEqual(22, weekofyear.GetWeek(new DateTime(2016, 05, 31)));
            Assert.AreEqual(22, weekofyear.GetWeek(new DateTime(2016, 06, 01)));
            Assert.AreEqual(22, weekofyear.GetWeek(new DateTime(2016, 06, 02)));
            Assert.AreEqual(22, weekofyear.GetWeek(new DateTime(2016, 06, 03)));
            Assert.AreEqual(22, weekofyear.GetWeek(new DateTime(2016, 06, 04)));
            Assert.AreEqual(22, weekofyear.GetWeek(new DateTime(2016, 06, 05)));

            // Week 23 June 6, 2016    June 12, 2016
            Assert.AreEqual(23, weekofyear.GetWeek(new DateTime(2016, 06, 06)));
            Assert.AreEqual(23, weekofyear.GetWeek(new DateTime(2016, 06, 07)));
            Assert.AreEqual(23, weekofyear.GetWeek(new DateTime(2016, 06, 08)));
            Assert.AreEqual(23, weekofyear.GetWeek(new DateTime(2016, 06, 09)));
            Assert.AreEqual(23, weekofyear.GetWeek(new DateTime(2016, 06, 10)));
            Assert.AreEqual(23, weekofyear.GetWeek(new DateTime(2016, 06, 11)));
            Assert.AreEqual(23, weekofyear.GetWeek(new DateTime(2016, 06, 12)));

            // Week 24 June 13, 2016   June 19, 2016
            Assert.AreEqual(24, weekofyear.GetWeek(new DateTime(2016, 06, 13)));
            Assert.AreEqual(24, weekofyear.GetWeek(new DateTime(2016, 06, 14)));
            Assert.AreEqual(24, weekofyear.GetWeek(new DateTime(2016, 06, 15)));
            Assert.AreEqual(24, weekofyear.GetWeek(new DateTime(2016, 06, 16)));
            Assert.AreEqual(24, weekofyear.GetWeek(new DateTime(2016, 06, 17)));
            Assert.AreEqual(24, weekofyear.GetWeek(new DateTime(2016, 06, 18)));
            Assert.AreEqual(24, weekofyear.GetWeek(new DateTime(2016, 06, 19)));

            // Week 25 June 20, 2016   June 26, 2016
            Assert.AreEqual(25, weekofyear.GetWeek(new DateTime(2016, 06, 20)));
            Assert.AreEqual(25, weekofyear.GetWeek(new DateTime(2016, 06, 21)));
            Assert.AreEqual(25, weekofyear.GetWeek(new DateTime(2016, 06, 22)));
            Assert.AreEqual(25, weekofyear.GetWeek(new DateTime(2016, 06, 23)));
            Assert.AreEqual(25, weekofyear.GetWeek(new DateTime(2016, 06, 24)));
            Assert.AreEqual(25, weekofyear.GetWeek(new DateTime(2016, 06, 25)));
            Assert.AreEqual(25, weekofyear.GetWeek(new DateTime(2016, 06, 26)));

            // Week 26 June 27, 2016   July 3, 2016
            Assert.AreEqual(26, weekofyear.GetWeek(new DateTime(2016, 06, 27)));
            Assert.AreEqual(26, weekofyear.GetWeek(new DateTime(2016, 06, 28)));
            Assert.AreEqual(26, weekofyear.GetWeek(new DateTime(2016, 06, 29)));
            Assert.AreEqual(26, weekofyear.GetWeek(new DateTime(2016, 06, 30)));
            Assert.AreEqual(26, weekofyear.GetWeek(new DateTime(2016, 07, 01)));
            Assert.AreEqual(26, weekofyear.GetWeek(new DateTime(2016, 07, 02)));
            Assert.AreEqual(26, weekofyear.GetWeek(new DateTime(2016, 07, 03)));

            // Week 27 July 4, 2016    July 10, 2016
            Assert.AreEqual(27, weekofyear.GetWeek(new DateTime(2016, 07, 04)));
            Assert.AreEqual(27, weekofyear.GetWeek(new DateTime(2016, 07, 05)));
            Assert.AreEqual(27, weekofyear.GetWeek(new DateTime(2016, 07, 06)));
            Assert.AreEqual(27, weekofyear.GetWeek(new DateTime(2016, 07, 07)));
            Assert.AreEqual(27, weekofyear.GetWeek(new DateTime(2016, 07, 08)));
            Assert.AreEqual(27, weekofyear.GetWeek(new DateTime(2016, 07, 09)));
            Assert.AreEqual(27, weekofyear.GetWeek(new DateTime(2016, 07, 10)));

            // Week 28 July 11, 2016   July 17, 2016
            Assert.AreEqual(28, weekofyear.GetWeek(new DateTime(2016, 07, 11)));
            Assert.AreEqual(28, weekofyear.GetWeek(new DateTime(2016, 07, 12)));
            Assert.AreEqual(28, weekofyear.GetWeek(new DateTime(2016, 07, 13)));
            Assert.AreEqual(28, weekofyear.GetWeek(new DateTime(2016, 07, 14)));
            Assert.AreEqual(28, weekofyear.GetWeek(new DateTime(2016, 07, 15)));
            Assert.AreEqual(28, weekofyear.GetWeek(new DateTime(2016, 07, 16)));
            Assert.AreEqual(28, weekofyear.GetWeek(new DateTime(2016, 07, 17)));

            // Week 29 July 18, 2016   July 24, 2016
            Assert.AreEqual(29, weekofyear.GetWeek(new DateTime(2016, 07, 18)));
            Assert.AreEqual(29, weekofyear.GetWeek(new DateTime(2016, 07, 19)));
            Assert.AreEqual(29, weekofyear.GetWeek(new DateTime(2016, 07, 20)));
            Assert.AreEqual(29, weekofyear.GetWeek(new DateTime(2016, 07, 21)));
            Assert.AreEqual(29, weekofyear.GetWeek(new DateTime(2016, 07, 22)));
            Assert.AreEqual(29, weekofyear.GetWeek(new DateTime(2016, 07, 23)));
            Assert.AreEqual(29, weekofyear.GetWeek(new DateTime(2016, 07, 24)));

            // Week 30  July 25, 2016   July 31, 2016
            Assert.AreEqual(30, weekofyear.GetWeek(new DateTime(2016, 07, 25)));
            Assert.AreEqual(30, weekofyear.GetWeek(new DateTime(2016, 07, 26)));
            Assert.AreEqual(30, weekofyear.GetWeek(new DateTime(2016, 07, 27)));
            Assert.AreEqual(30, weekofyear.GetWeek(new DateTime(2016, 07, 28)));
            Assert.AreEqual(30, weekofyear.GetWeek(new DateTime(2016, 07, 29)));
            Assert.AreEqual(30, weekofyear.GetWeek(new DateTime(2016, 07, 30)));
            Assert.AreEqual(30, weekofyear.GetWeek(new DateTime(2016, 07, 31)));

            // Week 31 August 1, 2016  August 7, 2016
            Assert.AreEqual(31, weekofyear.GetWeek(new DateTime(2016, 08, 01)));
            Assert.AreEqual(31, weekofyear.GetWeek(new DateTime(2016, 08, 02)));
            Assert.AreEqual(31, weekofyear.GetWeek(new DateTime(2016, 08, 03)));
            Assert.AreEqual(31, weekofyear.GetWeek(new DateTime(2016, 08, 04)));
            Assert.AreEqual(31, weekofyear.GetWeek(new DateTime(2016, 08, 05)));
            Assert.AreEqual(31, weekofyear.GetWeek(new DateTime(2016, 08, 06)));
            Assert.AreEqual(31, weekofyear.GetWeek(new DateTime(2016, 08, 07)));

            // Week 32 August 8, 2016  August 14, 2016
            Assert.AreEqual(32, weekofyear.GetWeek(new DateTime(2016, 08, 08)));
            Assert.AreEqual(32, weekofyear.GetWeek(new DateTime(2016, 08, 09)));
            Assert.AreEqual(32, weekofyear.GetWeek(new DateTime(2016, 08, 10)));
            Assert.AreEqual(32, weekofyear.GetWeek(new DateTime(2016, 08, 11)));
            Assert.AreEqual(32, weekofyear.GetWeek(new DateTime(2016, 08, 12)));
            Assert.AreEqual(32, weekofyear.GetWeek(new DateTime(2016, 08, 13)));
            Assert.AreEqual(32, weekofyear.GetWeek(new DateTime(2016, 08, 14)));

            // Week 33 August 15, 2016 August 21, 2016
            Assert.AreEqual(33, weekofyear.GetWeek(new DateTime(2016, 08, 15)));
            Assert.AreEqual(33, weekofyear.GetWeek(new DateTime(2016, 08, 16)));
            Assert.AreEqual(33, weekofyear.GetWeek(new DateTime(2016, 08, 17)));
            Assert.AreEqual(33, weekofyear.GetWeek(new DateTime(2016, 08, 18)));
            Assert.AreEqual(33, weekofyear.GetWeek(new DateTime(2016, 08, 19)));
            Assert.AreEqual(33, weekofyear.GetWeek(new DateTime(2016, 08, 20)));
            Assert.AreEqual(33, weekofyear.GetWeek(new DateTime(2016, 08, 21)));

            // Week 34 August 22, 2016 August 28, 2016
            Assert.AreEqual(34, weekofyear.GetWeek(new DateTime(2016, 08, 22)));
            Assert.AreEqual(34, weekofyear.GetWeek(new DateTime(2016, 08, 23)));
            Assert.AreEqual(34, weekofyear.GetWeek(new DateTime(2016, 08, 24)));
            Assert.AreEqual(34, weekofyear.GetWeek(new DateTime(2016, 08, 25)));
            Assert.AreEqual(34, weekofyear.GetWeek(new DateTime(2016, 08, 26)));
            Assert.AreEqual(34, weekofyear.GetWeek(new DateTime(2016, 08, 27)));
            Assert.AreEqual(34, weekofyear.GetWeek(new DateTime(2016, 08, 28)));

            // Week 35 August 29, 2016 September 4, 2016
            Assert.AreEqual(35, weekofyear.GetWeek(new DateTime(2016, 08, 29)));
            Assert.AreEqual(35, weekofyear.GetWeek(new DateTime(2016, 08, 30)));
            Assert.AreEqual(35, weekofyear.GetWeek(new DateTime(2016, 08, 31)));
            Assert.AreEqual(35, weekofyear.GetWeek(new DateTime(2016, 09, 01)));
            Assert.AreEqual(35, weekofyear.GetWeek(new DateTime(2016, 09, 02)));
            Assert.AreEqual(35, weekofyear.GetWeek(new DateTime(2016, 09, 03)));
            Assert.AreEqual(35, weekofyear.GetWeek(new DateTime(2016, 09, 04)));

            // Week 36 September 5, 2016   September 11, 2016
            Assert.AreEqual(36, weekofyear.GetWeek(new DateTime(2016, 09, 05)));
            Assert.AreEqual(36, weekofyear.GetWeek(new DateTime(2016, 09, 06)));
            Assert.AreEqual(36, weekofyear.GetWeek(new DateTime(2016, 09, 07)));
            Assert.AreEqual(36, weekofyear.GetWeek(new DateTime(2016, 09, 08)));
            Assert.AreEqual(36, weekofyear.GetWeek(new DateTime(2016, 09, 09)));
            Assert.AreEqual(36, weekofyear.GetWeek(new DateTime(2016, 09, 10)));
            Assert.AreEqual(36, weekofyear.GetWeek(new DateTime(2016, 09, 11)));

            // Week 37 September 12, 2016  September 18, 2016
            Assert.AreEqual(37, weekofyear.GetWeek(new DateTime(2016, 09, 12)));
            Assert.AreEqual(37, weekofyear.GetWeek(new DateTime(2016, 09, 13)));
            Assert.AreEqual(37, weekofyear.GetWeek(new DateTime(2016, 09, 14)));
            Assert.AreEqual(37, weekofyear.GetWeek(new DateTime(2016, 09, 15)));
            Assert.AreEqual(37, weekofyear.GetWeek(new DateTime(2016, 09, 16)));
            Assert.AreEqual(37, weekofyear.GetWeek(new DateTime(2016, 09, 17)));
            Assert.AreEqual(37, weekofyear.GetWeek(new DateTime(2016, 09, 18)));

            // Week 38 September 19, 2016  September 25, 2016
            Assert.AreEqual(38, weekofyear.GetWeek(new DateTime(2016, 09, 19)));
            Assert.AreEqual(38, weekofyear.GetWeek(new DateTime(2016, 09, 20)));
            Assert.AreEqual(38, weekofyear.GetWeek(new DateTime(2016, 09, 21)));
            Assert.AreEqual(38, weekofyear.GetWeek(new DateTime(2016, 09, 22)));
            Assert.AreEqual(38, weekofyear.GetWeek(new DateTime(2016, 09, 23)));
            Assert.AreEqual(38, weekofyear.GetWeek(new DateTime(2016, 09, 24)));
            Assert.AreEqual(38, weekofyear.GetWeek(new DateTime(2016, 09, 25)));

            // Week 39 September 26, 2016  October 2, 2016
            Assert.AreEqual(39, weekofyear.GetWeek(new DateTime(2016, 09, 26)));
            Assert.AreEqual(39, weekofyear.GetWeek(new DateTime(2016, 09, 27)));
            Assert.AreEqual(39, weekofyear.GetWeek(new DateTime(2016, 09, 28)));
            Assert.AreEqual(39, weekofyear.GetWeek(new DateTime(2016, 09, 29)));
            Assert.AreEqual(39, weekofyear.GetWeek(new DateTime(2016, 09, 30)));
            Assert.AreEqual(39, weekofyear.GetWeek(new DateTime(2016, 10, 01)));
            Assert.AreEqual(39, weekofyear.GetWeek(new DateTime(2016, 10, 02)));

            // Week 40 October 3, 2016 October 9, 2016
            Assert.AreEqual(40, weekofyear.GetWeek(new DateTime(2016, 10, 03)));
            Assert.AreEqual(40, weekofyear.GetWeek(new DateTime(2016, 10, 04)));
            Assert.AreEqual(40, weekofyear.GetWeek(new DateTime(2016, 10, 05)));
            Assert.AreEqual(40, weekofyear.GetWeek(new DateTime(2016, 10, 06)));
            Assert.AreEqual(40, weekofyear.GetWeek(new DateTime(2016, 10, 07)));
            Assert.AreEqual(40, weekofyear.GetWeek(new DateTime(2016, 10, 08)));
            Assert.AreEqual(40, weekofyear.GetWeek(new DateTime(2016, 10, 09)));

            // Week 41 October 10, 2016    October 16, 2016
            Assert.AreEqual(41, weekofyear.GetWeek(new DateTime(2016, 10, 10)));
            Assert.AreEqual(41, weekofyear.GetWeek(new DateTime(2016, 10, 11)));
            Assert.AreEqual(41, weekofyear.GetWeek(new DateTime(2016, 10, 12)));
            Assert.AreEqual(41, weekofyear.GetWeek(new DateTime(2016, 10, 13)));
            Assert.AreEqual(41, weekofyear.GetWeek(new DateTime(2016, 10, 14)));
            Assert.AreEqual(41, weekofyear.GetWeek(new DateTime(2016, 10, 15)));
            Assert.AreEqual(41, weekofyear.GetWeek(new DateTime(2016, 10, 16)));

            // Week 42 October 17, 2016    October 23, 2016
            Assert.AreEqual(42, weekofyear.GetWeek(new DateTime(2016, 10, 17)));
            Assert.AreEqual(42, weekofyear.GetWeek(new DateTime(2016, 10, 18)));
            Assert.AreEqual(42, weekofyear.GetWeek(new DateTime(2016, 10, 19)));
            Assert.AreEqual(42, weekofyear.GetWeek(new DateTime(2016, 10, 20)));
            Assert.AreEqual(42, weekofyear.GetWeek(new DateTime(2016, 10, 21)));
            Assert.AreEqual(42, weekofyear.GetWeek(new DateTime(2016, 10, 22)));
            Assert.AreEqual(42, weekofyear.GetWeek(new DateTime(2016, 10, 23)));

            // Week 43 October 24, 2016    October 30, 2016
            Assert.AreEqual(43, weekofyear.GetWeek(new DateTime(2016, 10, 24)));
            Assert.AreEqual(43, weekofyear.GetWeek(new DateTime(2016, 10, 25)));
            Assert.AreEqual(43, weekofyear.GetWeek(new DateTime(2016, 10, 26)));
            Assert.AreEqual(43, weekofyear.GetWeek(new DateTime(2016, 10, 27)));
            Assert.AreEqual(43, weekofyear.GetWeek(new DateTime(2016, 10, 28)));
            Assert.AreEqual(43, weekofyear.GetWeek(new DateTime(2016, 10, 29)));
            Assert.AreEqual(43, weekofyear.GetWeek(new DateTime(2016, 10, 30)));

            // Week 44 October 31, 2016    November 6, 2016
            Assert.AreEqual(44, weekofyear.GetWeek(new DateTime(2016, 10, 31)));
            Assert.AreEqual(44, weekofyear.GetWeek(new DateTime(2016, 11, 01)));
            Assert.AreEqual(44, weekofyear.GetWeek(new DateTime(2016, 11, 02)));
            Assert.AreEqual(44, weekofyear.GetWeek(new DateTime(2016, 11, 03)));
            Assert.AreEqual(44, weekofyear.GetWeek(new DateTime(2016, 11, 04)));
            Assert.AreEqual(44, weekofyear.GetWeek(new DateTime(2016, 11, 05)));
            Assert.AreEqual(44, weekofyear.GetWeek(new DateTime(2016, 11, 06)));

            // Week 45 November 7, 2016    November 13, 2016
            Assert.AreEqual(45, weekofyear.GetWeek(new DateTime(2016, 11, 07)));
            Assert.AreEqual(45, weekofyear.GetWeek(new DateTime(2016, 11, 08)));
            Assert.AreEqual(45, weekofyear.GetWeek(new DateTime(2016, 11, 09)));
            Assert.AreEqual(45, weekofyear.GetWeek(new DateTime(2016, 11, 10)));
            Assert.AreEqual(45, weekofyear.GetWeek(new DateTime(2016, 11, 11)));
            Assert.AreEqual(45, weekofyear.GetWeek(new DateTime(2016, 11, 12)));
            Assert.AreEqual(45, weekofyear.GetWeek(new DateTime(2016, 11, 13)));

            // Week 46 November 14, 2016   November 20, 2016
            Assert.AreEqual(46, weekofyear.GetWeek(new DateTime(2016, 11, 14)));
            Assert.AreEqual(46, weekofyear.GetWeek(new DateTime(2016, 11, 15)));
            Assert.AreEqual(46, weekofyear.GetWeek(new DateTime(2016, 11, 16)));
            Assert.AreEqual(46, weekofyear.GetWeek(new DateTime(2016, 11, 17)));
            Assert.AreEqual(46, weekofyear.GetWeek(new DateTime(2016, 11, 18)));
            Assert.AreEqual(46, weekofyear.GetWeek(new DateTime(2016, 11, 19)));
            Assert.AreEqual(46, weekofyear.GetWeek(new DateTime(2016, 11, 20)));

            // Week 47 November 21, 2016   November 27, 2016
            Assert.AreEqual(47, weekofyear.GetWeek(new DateTime(2016, 11, 21)));
            Assert.AreEqual(47, weekofyear.GetWeek(new DateTime(2016, 11, 22)));
            Assert.AreEqual(47, weekofyear.GetWeek(new DateTime(2016, 11, 23)));
            Assert.AreEqual(47, weekofyear.GetWeek(new DateTime(2016, 11, 24)));
            Assert.AreEqual(47, weekofyear.GetWeek(new DateTime(2016, 11, 25)));
            Assert.AreEqual(47, weekofyear.GetWeek(new DateTime(2016, 11, 26)));
            Assert.AreEqual(47, weekofyear.GetWeek(new DateTime(2016, 11, 27)));

            // Week 48 November 28, 2016   December 4, 2016
            Assert.AreEqual(48, weekofyear.GetWeek(new DateTime(2016, 11, 28)));
            Assert.AreEqual(48, weekofyear.GetWeek(new DateTime(2016, 11, 29)));
            Assert.AreEqual(48, weekofyear.GetWeek(new DateTime(2016, 11, 30)));
            Assert.AreEqual(48, weekofyear.GetWeek(new DateTime(2016, 12, 01)));
            Assert.AreEqual(48, weekofyear.GetWeek(new DateTime(2016, 12, 02)));
            Assert.AreEqual(48, weekofyear.GetWeek(new DateTime(2016, 12, 03)));
            Assert.AreEqual(48, weekofyear.GetWeek(new DateTime(2016, 12, 04)));

            // Week 49 December 5, 2016    December 11, 2016
            Assert.AreEqual(49, weekofyear.GetWeek(new DateTime(2016, 12, 05)));
            Assert.AreEqual(49, weekofyear.GetWeek(new DateTime(2016, 12, 06)));
            Assert.AreEqual(49, weekofyear.GetWeek(new DateTime(2016, 12, 07)));
            Assert.AreEqual(49, weekofyear.GetWeek(new DateTime(2016, 12, 08)));
            Assert.AreEqual(49, weekofyear.GetWeek(new DateTime(2016, 12, 09)));
            Assert.AreEqual(49, weekofyear.GetWeek(new DateTime(2016, 12, 10)));
            Assert.AreEqual(49, weekofyear.GetWeek(new DateTime(2016, 12, 11)));

            // Week 50 December 12, 2016   December 18, 2016
            Assert.AreEqual(50, weekofyear.GetWeek(new DateTime(2016, 12, 12)));
            Assert.AreEqual(50, weekofyear.GetWeek(new DateTime(2016, 12, 13)));
            Assert.AreEqual(50, weekofyear.GetWeek(new DateTime(2016, 12, 14)));
            Assert.AreEqual(50, weekofyear.GetWeek(new DateTime(2016, 12, 15)));
            Assert.AreEqual(50, weekofyear.GetWeek(new DateTime(2016, 12, 16)));
            Assert.AreEqual(50, weekofyear.GetWeek(new DateTime(2016, 12, 17)));
            Assert.AreEqual(50, weekofyear.GetWeek(new DateTime(2016, 12, 18)));

            // Week 51 December 19, 2016   December 25, 2016
            Assert.AreEqual(51, weekofyear.GetWeek(new DateTime(2016, 12, 19)));
            Assert.AreEqual(51, weekofyear.GetWeek(new DateTime(2016, 12, 20)));
            Assert.AreEqual(51, weekofyear.GetWeek(new DateTime(2016, 12, 21)));
            Assert.AreEqual(51, weekofyear.GetWeek(new DateTime(2016, 12, 22)));
            Assert.AreEqual(51, weekofyear.GetWeek(new DateTime(2016, 12, 23)));
            Assert.AreEqual(51, weekofyear.GetWeek(new DateTime(2016, 12, 24)));
            Assert.AreEqual(51, weekofyear.GetWeek(new DateTime(2016, 12, 25)));

            // Week 52 December 26, 2016   January 1, 2017
            Assert.AreEqual(52, weekofyear.GetWeek(new DateTime(2016, 12, 26)));
            Assert.AreEqual(52, weekofyear.GetWeek(new DateTime(2016, 12, 27)));
            Assert.AreEqual(52, weekofyear.GetWeek(new DateTime(2016, 12, 28)));
            Assert.AreEqual(52, weekofyear.GetWeek(new DateTime(2016, 12, 29)));
            Assert.AreEqual(52, weekofyear.GetWeek(new DateTime(2016, 12, 30)));
            Assert.AreEqual(52, weekofyear.GetWeek(new DateTime(2016, 12, 31)));
            Assert.AreEqual(52, weekofyear.GetWeek(new DateTime(2017, 01, 01)));
        }

        /// <summary>
        /// Test ISO8601 year 2017.
        /// </summary>
        [TestMethod]
        public void Test2017()
        {
            int year = 2017;
            IWeekOfYear weekofyear = new Iso8601();

            int week = 1;

            // Week 01 January 2, 2017 January 8, 2017
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 02)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 03)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 04)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 05)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 06)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 07)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 08)));

            // Week 02 January 9, 2017 January 15, 2017
            week = 2;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 09)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 10)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 11)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 12)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 13)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 14)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 15)));

            // Week 03 January 16, 2017    January 22, 2017
            week = 3;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 16)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 17)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 18)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 19)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 20)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 21)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 22)));

            // Week 04 January 23, 2017    January 29, 2017
            week = 4;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 23)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 24)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 25)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 26)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 27)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 28)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 29)));

            // Week 05 January 30, 2017    February 5, 2017
            week = 5;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 30)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 01, 31)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 01)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 02)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 03)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 04)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 05)));

            // Week 06 February 6, 2017    February 12, 2017
            week = 6;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 06)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 07)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 08)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 09)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 10)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 11)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 12)));

            // Week 07 February 13, 2017   February 19, 2017
            week = 7;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 13)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 14)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 15)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 16)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 17)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 18)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 19)));

            // Week 08 February 20, 2017   February 26, 2017
            week = 8;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 20)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 21)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 22)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 23)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 24)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 25)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 26)));

            // Week 09 February 27, 2017   March 5, 2017
            week = 9;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 27)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 02, 28)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 01)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 02)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 03)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 04)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 05)));

            // Week 10 March 6, 2017   March 12, 2017
            week = 10;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 06)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 07)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 08)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 09)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 10)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 11)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 12)));

            // Week 11 March 13, 2017  March 19, 2017
            week = 11;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 13)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 14)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 15)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 16)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 17)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 18)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 19)));

            // Week 12 March 20, 2017  March 26, 2017
            week = 12;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 20)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 21)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 22)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 23)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 24)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 25)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 26)));

            // Week 13 March 27, 2017  April 2, 2017
            week = 13;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 27)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 28)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 29)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 30)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 03, 31)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 01)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 02)));

            // Week 14 April 3, 2017   April 9, 2017
            week = 14;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 03)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 04)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 05)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 06)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 07)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 08)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 09)));

            // Week 15 April 10, 2017  April 16, 2017
            week = 15;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 10)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 11)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 12)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 13)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 14)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 15)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 16)));

            // Week 16 April 17, 2017  April 23, 2017
            week = 16;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 17)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 18)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 19)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 20)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 21)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 22)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 23)));

            // Week 17 April 24, 2017  April 30, 2017
            week = 17;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 24)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 25)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 26)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 27)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 28)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 29)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 04, 30)));

            // Week 18 May 1, 2017 May 7, 2017
            week = 18;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 01)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 02)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 03)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 04)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 05)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 06)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 07)));

            // Week 19 May 8, 2017 May 14, 2017
            week = 19;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 08)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 09)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 10)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 11)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 12)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 13)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 14)));

            // Week 20 May 15, 2017    May 21, 2017
            week = 20;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 15)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 16)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 17)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 18)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 19)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 20)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 21)));

            // Week 21 May 22, 2017    May 28, 2017
            week = 21;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 22)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 23)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 24)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 25)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 26)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 27)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 28)));

            // Week 22 May 29, 2017    June 4, 2017
            week = 22;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 29)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 30)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 05, 31)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 01)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 02)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 03)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 04)));

            // Week 23 June 5, 2017    June 11, 2017
            week = 23;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 05)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 06)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 07)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 08)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 09)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 10)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 11)));

            // Week 24 June 12, 2017   June 18, 2017
            week = 24;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 12)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 13)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 14)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 15)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 16)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 17)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 18)));

            // Week 25 June 19, 2017   June 25, 2017
            week = 25;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 19)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 20)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 21)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 22)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 23)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 24)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 25)));

            // Week 26 June 26, 2017   July 2, 2017
            week = 26;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 26)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 27)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 28)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 29)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 06, 30)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 01)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 02)));

            // Week 27 July 3, 2017    July 9, 2017
            week = 27;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 03)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 04)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 05)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 06)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 07)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 08)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 09)));

            // Week 28 July 10, 2017   July 16, 2017
            week = 28;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 10)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 11)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 12)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 13)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 14)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 15)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 16)));

            // Week 29 July 17, 2017   July 23, 2017
            week = 29;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 17)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 18)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 19)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 20)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 21)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 22)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 23)));

            // Week 30 July 24, 2017   July 30, 2017
            week = 30;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 24)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 25)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 26)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 27)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 28)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 29)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 30)));

            // Week 31 July 31, 2017   August 6, 2017
            week = 31;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 07, 31)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 01)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 02)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 03)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 04)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 05)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 06)));

            // Week 32 August 7, 2017  August 13, 2017
            week = 32;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 07)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 08)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 09)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 10)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 11)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 12)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 13)));

            // Week 33 August 14, 2017 August 20, 2017
            week = 33;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 14)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 15)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 16)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 17)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 18)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 19)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 20)));

            // Week 34 August 21, 2017 August 27, 2017
            week = 34;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 21)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 22)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 23)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 24)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 25)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 26)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 27)));

            // Week 35 August 28, 2017 September 3, 2017
            week = 35;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 28)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 29)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 30)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 08, 31)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 01)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 02)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 03)));

            // Week 36 September 4, 2017   September 10, 2017
            week = 36;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 04)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 05)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 06)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 07)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 08)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 09)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 10)));

            // Week 37 September 11, 2017  September 17, 2017
            week = 37;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 11)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 12)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 13)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 14)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 15)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 16)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 17)));

            // Week 38 September 18, 2017  September 24, 2017
            week = 38;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 18)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 19)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 20)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 21)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 22)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 23)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 24)));

            // Week 39 September 25, 2017  October 1, 2017
            week = 39;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 25)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 26)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 27)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 28)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 29)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 09, 30)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 01)));

            // Week 40 October 2, 2017 October 8, 2017
            week = 40;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 02)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 03)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 04)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 05)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 06)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 07)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 08)));

            // Week 41 October 9, 2017 October 15, 2017
            week = 41;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 09)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 10)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 11)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 12)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 13)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 14)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 15)));

            // Week 42 October 16, 2017    October 22, 2017
            week = 42;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 16)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 17)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 18)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 19)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 20)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 21)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 22)));

            // Week 43 October 23, 2017    October 29, 2017
            week = 43;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 23)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 24)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 25)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 26)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 27)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 28)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 29)));

            // Week 44 October 30, 2017    November 5, 2017
            week = 44;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 30)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 10, 31)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 01)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 02)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 03)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 04)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 05)));

            // Week 45 November 6, 2017    November 12, 2017
            week = 45;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 06)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 07)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 08)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 09)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 10)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 11)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 12)));

            // Week 46 November 13, 2017   November 19, 2017
            week = 46;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 13)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 14)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 15)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 16)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 17)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 18)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 19)));

            // Week 47 November 20, 2017   November 26, 2017
            week = 47;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 20)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 21)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 22)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 23)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 24)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 25)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 26)));

            // Week 48 November 27, 2017   December 3, 2017
            week = 48;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 27)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 28)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 29)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 11, 30)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 01)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 02)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 03)));

            // Week 49 December 4, 2017    December 10, 2017
            week = 49;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 04)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 05)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 06)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 07)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 08)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 09)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 10)));

            // Week 50 December 11, 2017   December 17, 2017
            week = 50;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 11)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 12)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 13)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 14)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 15)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 16)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 17)));

            // Week 51 December 18, 2017   December 24, 2017
            week = 51;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 18)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 19)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 20)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 21)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 22)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 23)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 24)));

            // Week 52 December 25, 2017   December 31, 2017
            week = 52;
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 25)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 26)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 27)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 28)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 29)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 30)));
            Assert.AreEqual(week, weekofyear.GetWeek(new DateTime(year, 12, 31)));
        }

        /// <summary>
        /// Test ISO8601 year 2018.
        /// </summary>
        [TestMethod]
        public void Test2018Condensed()
        {
            int year = 2018;
            IWeekOfYear weekofyear = new Iso8601();

            // Week 01 January 1, 2018 January 7, 2018
            Assert.AreEqual(1, weekofyear.GetWeek(new DateTime(year, 01, 01)));
            Assert.AreEqual(1, weekofyear.GetWeek(new DateTime(year, 01, 07)));

            // Week 02 January 8, 2018 January 14, 2018
            Assert.AreEqual(2, weekofyear.GetWeek(new DateTime(year, 01, 08)));
            Assert.AreEqual(2, weekofyear.GetWeek(new DateTime(year, 01, 14)));

            // Week 03 January 15, 2018    January 21, 2018
            Assert.AreEqual(3, weekofyear.GetWeek(new DateTime(year, 01, 15)));
            Assert.AreEqual(3, weekofyear.GetWeek(new DateTime(year, 01, 21)));

            // Week 04 January 22, 2018    January 28, 2018
            Assert.AreEqual(4, weekofyear.GetWeek(new DateTime(year, 01, 22)));
            Assert.AreEqual(4, weekofyear.GetWeek(new DateTime(year, 01, 28)));

            // Week 05 January 29, 2018    February 4, 2018
            Assert.AreEqual(5, weekofyear.GetWeek(new DateTime(year, 01, 29)));
            Assert.AreEqual(5, weekofyear.GetWeek(new DateTime(year, 02, 04)));

            // Week 06 February 5, 2018    February 11, 2018
            Assert.AreEqual(6, weekofyear.GetWeek(new DateTime(year, 02, 05)));
            Assert.AreEqual(6, weekofyear.GetWeek(new DateTime(year, 02, 11)));

            // Week 07 February 12, 2018   February 18, 2018
            Assert.AreEqual(7, weekofyear.GetWeek(new DateTime(year, 02, 12)));
            Assert.AreEqual(7, weekofyear.GetWeek(new DateTime(year, 02, 18)));

            // Week 08 February 19, 2018   February 25, 2018
            Assert.AreEqual(8, weekofyear.GetWeek(new DateTime(year, 02, 19)));
            Assert.AreEqual(8, weekofyear.GetWeek(new DateTime(year, 02, 25)));

            // Week 09 February 26, 2018   March 4, 2018
            Assert.AreEqual(9, weekofyear.GetWeek(new DateTime(year, 02, 26)));
            Assert.AreEqual(9, weekofyear.GetWeek(new DateTime(year, 03, 04)));

            // Week 10 March 5, 2018   March 11, 2018
            Assert.AreEqual(10, weekofyear.GetWeek(new DateTime(year, 03, 05)));
            Assert.AreEqual(10, weekofyear.GetWeek(new DateTime(year, 03, 11)));

            // Week 11 March 12, 2018  March 18, 2018
            Assert.AreEqual(11, weekofyear.GetWeek(new DateTime(year, 03, 12)));
            Assert.AreEqual(11, weekofyear.GetWeek(new DateTime(year, 03, 18)));

            // Week 12 March 19, 2018  March 25, 2018
            Assert.AreEqual(12, weekofyear.GetWeek(new DateTime(year, 03, 19)));
            Assert.AreEqual(12, weekofyear.GetWeek(new DateTime(year, 03, 25)));

            // Week 13 March 26, 2018  April 1, 2018
            Assert.AreEqual(13, weekofyear.GetWeek(new DateTime(year, 03, 26)));
            Assert.AreEqual(13, weekofyear.GetWeek(new DateTime(year, 04, 1)));

            // Week 14 April 2, 2018   April 8, 2018
            Assert.AreEqual(14, weekofyear.GetWeek(new DateTime(year, 04, 2)));
            Assert.AreEqual(14, weekofyear.GetWeek(new DateTime(year, 04, 8)));

            // Week 15 April 9, 2018   April 15, 2018
            Assert.AreEqual(15, weekofyear.GetWeek(new DateTime(year, 04, 9)));
            Assert.AreEqual(15, weekofyear.GetWeek(new DateTime(year, 04, 15)));

            // Week 16 April 16, 2018  April 22, 2018
            Assert.AreEqual(16, weekofyear.GetWeek(new DateTime(year, 04, 16)));
            Assert.AreEqual(16, weekofyear.GetWeek(new DateTime(year, 04, 22)));

            // Week 17 April 23, 2018  April 29, 2018
            Assert.AreEqual(17, weekofyear.GetWeek(new DateTime(year, 04, 23)));
            Assert.AreEqual(17, weekofyear.GetWeek(new DateTime(year, 04, 29)));

            // Week 18 April 30, 2018  May 6, 2018
            Assert.AreEqual(18, weekofyear.GetWeek(new DateTime(year, 04, 30)));
            Assert.AreEqual(18, weekofyear.GetWeek(new DateTime(year, 05, 06)));

            // Week 19 May 7, 2018 May 13, 2018
            Assert.AreEqual(19, weekofyear.GetWeek(new DateTime(year, 05, 07)));
            Assert.AreEqual(19, weekofyear.GetWeek(new DateTime(year, 05, 13)));

            // Week 20 May 14, 2018    May 20, 2018
            Assert.AreEqual(20, weekofyear.GetWeek(new DateTime(year, 05, 14)));
            Assert.AreEqual(20, weekofyear.GetWeek(new DateTime(year, 05, 20)));

            // Week 21 May 21, 2018    May 27, 2018
            Assert.AreEqual(21, weekofyear.GetWeek(new DateTime(year, 05, 21)));
            Assert.AreEqual(21, weekofyear.GetWeek(new DateTime(year, 05, 27)));

            // Week 22 May 28, 2018    June 3, 2018
            Assert.AreEqual(22, weekofyear.GetWeek(new DateTime(year, 05, 28)));
            Assert.AreEqual(22, weekofyear.GetWeek(new DateTime(year, 06, 03)));

            // Week 23 June 4, 2018    June 10, 2018
            Assert.AreEqual(23, weekofyear.GetWeek(new DateTime(year, 06, 04)));
            Assert.AreEqual(23, weekofyear.GetWeek(new DateTime(year, 06, 10)));

            // Week 24 June 11, 2018   June 17, 2018
            Assert.AreEqual(24, weekofyear.GetWeek(new DateTime(year, 06, 11)));
            Assert.AreEqual(24, weekofyear.GetWeek(new DateTime(year, 06, 17)));

            // Week 25 June 18, 2018   June 24, 2018
            Assert.AreEqual(25, weekofyear.GetWeek(new DateTime(year, 06, 18)));
            Assert.AreEqual(25, weekofyear.GetWeek(new DateTime(year, 06, 24)));

            // Week 26 June 25, 2018   July 1, 2018
            Assert.AreEqual(26, weekofyear.GetWeek(new DateTime(year, 06, 25)));
            Assert.AreEqual(26, weekofyear.GetWeek(new DateTime(year, 07, 01)));

            // Week 27 July 2, 2018    July 8, 2018
            Assert.AreEqual(27, weekofyear.GetWeek(new DateTime(year, 07, 02)));
            Assert.AreEqual(27, weekofyear.GetWeek(new DateTime(year, 07, 08)));

            // Week 28 July 9, 2018    July 15, 2018
            Assert.AreEqual(28, weekofyear.GetWeek(new DateTime(year, 07, 09)));
            Assert.AreEqual(28, weekofyear.GetWeek(new DateTime(year, 07, 15)));

            // Week 29 July 16, 2018   July 22, 2018
            Assert.AreEqual(29, weekofyear.GetWeek(new DateTime(year, 07, 16)));
            Assert.AreEqual(29, weekofyear.GetWeek(new DateTime(year, 07, 22)));

            // Week 30 July 23, 2018   July 29, 2018
            Assert.AreEqual(30, weekofyear.GetWeek(new DateTime(year, 07, 23)));
            Assert.AreEqual(30, weekofyear.GetWeek(new DateTime(year, 07, 29)));

            // Week 31 July 30, 2018   August 5, 2018
            Assert.AreEqual(31, weekofyear.GetWeek(new DateTime(year, 07, 30)));
            Assert.AreEqual(31, weekofyear.GetWeek(new DateTime(year, 08, 05)));

            // Week 32 August 6, 2018  August 12, 2018
            Assert.AreEqual(32, weekofyear.GetWeek(new DateTime(year, 08, 06)));
            Assert.AreEqual(32, weekofyear.GetWeek(new DateTime(year, 08, 12)));

            // Week 33 August 13, 2018 August 19, 2018
            Assert.AreEqual(33, weekofyear.GetWeek(new DateTime(year, 08, 13)));
            Assert.AreEqual(33, weekofyear.GetWeek(new DateTime(year, 08, 19)));

            // Week 34 August 20, 2018 August 26, 2018
            Assert.AreEqual(34, weekofyear.GetWeek(new DateTime(year, 08, 26)));
            Assert.AreEqual(34, weekofyear.GetWeek(new DateTime(year, 08, 26)));

            // Week 35 August 27, 2018 September 2, 2018
            Assert.AreEqual(35, weekofyear.GetWeek(new DateTime(year, 08, 27)));
            Assert.AreEqual(35, weekofyear.GetWeek(new DateTime(year, 09, 02)));

            // Week 36 September 3, 2018   September 9, 2018
            Assert.AreEqual(36, weekofyear.GetWeek(new DateTime(year, 09, 03)));
            Assert.AreEqual(36, weekofyear.GetWeek(new DateTime(year, 09, 09)));

            // Week 37 September 10, 2018  September 16, 2018
            Assert.AreEqual(37, weekofyear.GetWeek(new DateTime(year, 09, 10)));
            Assert.AreEqual(37, weekofyear.GetWeek(new DateTime(year, 09, 16)));

            // Week 38 September 17, 2018  September 23, 2018
            Assert.AreEqual(38, weekofyear.GetWeek(new DateTime(year, 09, 17)));
            Assert.AreEqual(38, weekofyear.GetWeek(new DateTime(year, 09, 23)));

            // Week 39 September 24, 2018  September 30, 2018
            Assert.AreEqual(39, weekofyear.GetWeek(new DateTime(year, 09, 24)));
            Assert.AreEqual(39, weekofyear.GetWeek(new DateTime(year, 09, 30)));

            // Week 40 October 1, 2018 October 7, 2018
            Assert.AreEqual(40, weekofyear.GetWeek(new DateTime(year, 10, 01)));
            Assert.AreEqual(40, weekofyear.GetWeek(new DateTime(year, 10, 07)));

            // Week 41 October 8, 2018 October 14, 2018
            Assert.AreEqual(41, weekofyear.GetWeek(new DateTime(year, 10, 08)));
            Assert.AreEqual(41, weekofyear.GetWeek(new DateTime(year, 10, 14)));

            // Week 42 October 15, 2018    October 21, 2018
            Assert.AreEqual(42, weekofyear.GetWeek(new DateTime(year, 10, 15)));
            Assert.AreEqual(42, weekofyear.GetWeek(new DateTime(year, 10, 21)));

            // Week 43 October 22, 2018    October 28, 2018
            Assert.AreEqual(43, weekofyear.GetWeek(new DateTime(year, 10, 22)));
            Assert.AreEqual(43, weekofyear.GetWeek(new DateTime(year, 10, 28)));

            // Week 44 October 29, 2018    November 4, 2018
            Assert.AreEqual(44, weekofyear.GetWeek(new DateTime(year, 10, 29)));
            Assert.AreEqual(44, weekofyear.GetWeek(new DateTime(year, 11, 04)));

            // Week 45 November 5, 2018    November 11, 2018
            Assert.AreEqual(45, weekofyear.GetWeek(new DateTime(year, 11, 05)));
            Assert.AreEqual(45, weekofyear.GetWeek(new DateTime(year, 11, 11)));

            // Week 46 November 12, 2018   November 18, 2018
            Assert.AreEqual(46, weekofyear.GetWeek(new DateTime(year, 11, 12)));
            Assert.AreEqual(46, weekofyear.GetWeek(new DateTime(year, 11, 18)));

            // Week 47 November 19, 2018   November 25, 2018
            Assert.AreEqual(47, weekofyear.GetWeek(new DateTime(year, 11, 19)));
            Assert.AreEqual(47, weekofyear.GetWeek(new DateTime(year, 11, 25)));

            // Week 48 November 26, 2018   December 2, 2018
            Assert.AreEqual(48, weekofyear.GetWeek(new DateTime(year, 11, 26)));
            Assert.AreEqual(48, weekofyear.GetWeek(new DateTime(year, 12, 02)));

            // Week 49 December 3, 2018    December 9, 2018
            Assert.AreEqual(49, weekofyear.GetWeek(new DateTime(year, 12, 03)));
            Assert.AreEqual(49, weekofyear.GetWeek(new DateTime(year, 12, 09)));

            // Week 50 December 10, 2018   December 16, 2018
            Assert.AreEqual(50, weekofyear.GetWeek(new DateTime(year, 12, 10)));
            Assert.AreEqual(50, weekofyear.GetWeek(new DateTime(year, 12, 16)));

            // Week 51 December 17, 2018   December 23, 2018
            Assert.AreEqual(51, weekofyear.GetWeek(new DateTime(year, 12, 17)));
            Assert.AreEqual(51, weekofyear.GetWeek(new DateTime(year, 12, 23)));

            // Week 52 December 24, 2018   December 30, 2018
            Assert.AreEqual(52, weekofyear.GetWeek(new DateTime(year, 12, 24)));
            Assert.AreEqual(52, weekofyear.GetWeek(new DateTime(year, 12, 30)));
        }

        /// <summary>
        /// Test ISO8601 year 2019.
        /// </summary>
        [TestMethod]
        public void Test2019Condensed()
        {
            int year = 2019;
            IWeekOfYear weekofyear = new Iso8601();

            Assert.AreEqual(1, weekofyear.GetWeek(new DateTime(year, 01, 06)));
            Assert.AreEqual(2, weekofyear.GetWeek(new DateTime(year, 01, 13)));
            Assert.AreEqual(3, weekofyear.GetWeek(new DateTime(year, 01, 20)));
            Assert.AreEqual(4, weekofyear.GetWeek(new DateTime(year, 01, 27)));

            Assert.AreEqual(5, weekofyear.GetWeek(new DateTime(year, 02, 3)));
            Assert.AreEqual(6, weekofyear.GetWeek(new DateTime(year, 02, 10)));
            Assert.AreEqual(7, weekofyear.GetWeek(new DateTime(year, 02, 17)));
            Assert.AreEqual(8, weekofyear.GetWeek(new DateTime(year, 02, 24)));

            Assert.AreEqual(9, weekofyear.GetWeek(new DateTime(year, 03, 03)));
            Assert.AreEqual(10, weekofyear.GetWeek(new DateTime(year, 03, 10)));
            Assert.AreEqual(11, weekofyear.GetWeek(new DateTime(year, 03, 17)));
            Assert.AreEqual(12, weekofyear.GetWeek(new DateTime(year, 03, 24)));
            Assert.AreEqual(13, weekofyear.GetWeek(new DateTime(year, 03, 31)));

            Assert.AreEqual(14, weekofyear.GetWeek(new DateTime(year, 04, 07)));
            Assert.AreEqual(15, weekofyear.GetWeek(new DateTime(year, 04, 14)));
            Assert.AreEqual(16, weekofyear.GetWeek(new DateTime(year, 04, 21)));
            Assert.AreEqual(17, weekofyear.GetWeek(new DateTime(year, 04, 28)));

            Assert.AreEqual(18, weekofyear.GetWeek(new DateTime(year, 05, 05)));
            Assert.AreEqual(19, weekofyear.GetWeek(new DateTime(year, 05, 12)));
            Assert.AreEqual(20, weekofyear.GetWeek(new DateTime(year, 05, 19)));
            Assert.AreEqual(21, weekofyear.GetWeek(new DateTime(year, 05, 26)));

            Assert.AreEqual(22, weekofyear.GetWeek(new DateTime(year, 06, 02)));
            Assert.AreEqual(23, weekofyear.GetWeek(new DateTime(year, 06, 09)));
            Assert.AreEqual(24, weekofyear.GetWeek(new DateTime(year, 06, 16)));
            Assert.AreEqual(25, weekofyear.GetWeek(new DateTime(year, 06, 23)));
            Assert.AreEqual(26, weekofyear.GetWeek(new DateTime(year, 06, 30)));

            Assert.AreEqual(27, weekofyear.GetWeek(new DateTime(year, 07, 07)));
            Assert.AreEqual(28, weekofyear.GetWeek(new DateTime(year, 07, 14)));
            Assert.AreEqual(29, weekofyear.GetWeek(new DateTime(year, 07, 21)));
            Assert.AreEqual(30, weekofyear.GetWeek(new DateTime(year, 07, 28)));

            Assert.AreEqual(31, weekofyear.GetWeek(new DateTime(year, 08, 04)));
            Assert.AreEqual(32, weekofyear.GetWeek(new DateTime(year, 08, 11)));
            Assert.AreEqual(33, weekofyear.GetWeek(new DateTime(year, 08, 18)));
            Assert.AreEqual(34, weekofyear.GetWeek(new DateTime(year, 08, 25)));

            Assert.AreEqual(35, weekofyear.GetWeek(new DateTime(year, 09, 01)));
            Assert.AreEqual(36, weekofyear.GetWeek(new DateTime(year, 09, 08)));
            Assert.AreEqual(37, weekofyear.GetWeek(new DateTime(year, 09, 15)));
            Assert.AreEqual(38, weekofyear.GetWeek(new DateTime(year, 09, 22)));
            Assert.AreEqual(39, weekofyear.GetWeek(new DateTime(year, 09, 29)));

            Assert.AreEqual(40, weekofyear.GetWeek(new DateTime(year, 10, 06)));
            Assert.AreEqual(41, weekofyear.GetWeek(new DateTime(year, 10, 13)));
            Assert.AreEqual(42, weekofyear.GetWeek(new DateTime(year, 10, 20)));
            Assert.AreEqual(43, weekofyear.GetWeek(new DateTime(year, 10, 27)));

            Assert.AreEqual(44, weekofyear.GetWeek(new DateTime(year, 11, 03)));
            Assert.AreEqual(45, weekofyear.GetWeek(new DateTime(year, 11, 10)));
            Assert.AreEqual(46, weekofyear.GetWeek(new DateTime(year, 11, 17)));
            Assert.AreEqual(47, weekofyear.GetWeek(new DateTime(year, 11, 24)));

            Assert.AreEqual(48, weekofyear.GetWeek(new DateTime(year, 12, 01)));
            Assert.AreEqual(49, weekofyear.GetWeek(new DateTime(year, 12, 08)));
            Assert.AreEqual(50, weekofyear.GetWeek(new DateTime(year, 12, 15)));
            Assert.AreEqual(51, weekofyear.GetWeek(new DateTime(year, 12, 22)));
            Assert.AreEqual(52, weekofyear.GetWeek(new DateTime(year, 12, 29)));
        }

        /// <summary>
        /// Test 3000 years worth of ISO8601 weeks.
        /// </summary>
        [TestMethod]
        public void WeekNumbers_CorrectFor_3000Years()
        {
            IWeekOfYear weekofyear = new Iso8601();
            IWeekOfYear weekofyearnet = new Iso8601Verbose();

            var weekNumbersMethod1 = this.WeekNumbers3000Years(weekofyear.GetWeek).ToList();
            var weekNumbersMethod2 = this.WeekNumbers3000Years(weekofyearnet.GetWeek).ToList();

            CollectionAssert.AreEqual(weekNumbersMethod1, weekNumbersMethod2);
        }

        private IEnumerable<int> WeekNumbers3000Years(Func<DateTime, int> weekNumberCalculator)
        {
            var startDate = new DateTime(1, 1, 1);
            var endDate = new DateTime(3000, 12, 31);
            for (DateTime date = startDate; date < endDate; date = date.AddDays(1))
            {
                yield return weekNumberCalculator(date);
            }
        }
    }
}
