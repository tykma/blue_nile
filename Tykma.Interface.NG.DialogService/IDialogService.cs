﻿// <copyright file="IDialogService.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Interface.NG.DialogService
{
    /// <summary>
    /// Dialog service interface.
    /// </summary>
    public interface IDialogService
    {
        /// <summary>
        /// Shows the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="title">The title.</param>
        void ShowMessage(string message, string title);

        /// <summary>
        /// Hides the message.
        /// </summary>
        void HideMessage();

        /// <summary>
        /// Shows the error.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="title">The title.</param>
        void ShowError(string message, string title);
    }
}
