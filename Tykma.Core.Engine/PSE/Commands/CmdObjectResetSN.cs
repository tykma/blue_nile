﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdObjectResetSN.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// The command to reset an object's serial number.
    /// </summary>
    internal class CmdObjectResetSN : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdObjectDelete"/> class.
        /// </summary>
        /// <param name="lmcApi">The LMC API.</param>
        /// <param name="objectToDelete">The object to delete.</param>
        public CmdObjectResetSN(IntPtr lmcApi, string objectToReset)
            : base(lmcApi)
        {
            this.ObjectToReset = objectToReset;
        }

        /// <summary>
        /// Delete a given entity.
        /// </summary>
        /// <param name="pEntName">Name of the entity.</param>
        /// <returns>Return code.</returns>
        private delegate int LMC1ApiDeleteEnt([MarshalAs(UnmanagedType.LPWStr)] string pEntName);

        /// <summary>
        /// Gets the object to delete.
        /// </summary>
        /// <value>
        /// The object to delete.
        /// </value>
        private string ObjectToReset { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        /// <exception cref="Tykma.Core.Engine.CustomExceptions.MethodFaultException">Problem with command.</exception>
        internal bool Execute()
        {
            var lmc1DeleteEntity = this.LoadFunction<LMC1ApiDeleteEnt>("lmc1_TextResetSn");
            if (lmc1DeleteEntity == null)
            {
                throw new EngineNotConnectedException();
            }

            ReturnCodeMessage rm = this.GetMiniLaseReturnCode(lmc1DeleteEntity(this.ObjectToReset));

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
