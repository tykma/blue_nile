﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdConnClose.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;

    /// <summary>
    /// The command to close API.
    /// </summary>
    internal class CmdConnClose : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdConnClose" /> class.
        /// </summary>
        /// <param name="lmcApi">The API.</param>
        public CmdConnClose(IntPtr lmcApi)
            : base(lmcApi)
        {
        }

        /// <summary>
        /// Close connection to the card.
        /// </summary>
        /// <returns>Success code.</returns>
        private delegate int LMC1ApiClose();

        /// <summary>
        /// Executes this instance.
        /// </summary>
        internal void Execute()
        {
            var lmc1CloseCon = this.LoadFunction<LMC1ApiClose>("lmc1_Close");

            lmc1CloseCon?.Invoke();
        }
    }
}
