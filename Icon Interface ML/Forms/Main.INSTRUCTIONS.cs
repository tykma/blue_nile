﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.INSTRUCTIONS.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// Instruction form handling on the main UI form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Windows.Forms;

    using Icon_Interface.General;

    /// <summary>
    /// Handle instruction form.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// The instructions form.
        /// </summary>
        private Form instructionsForm;

        /// <summary>
        /// Occurs when a picture loaded is ready.
        /// </summary>
        public event EventHandler<InstructionsEventMessage> InstructionsLoadEvent;

        /// <summary>
        /// Initializes the instruction form.
        /// </summary>
        private void InitializeInstructionForm()
        {
            // Initialize the instructions form. We won't need to reference it as we'll just fire events to it.
            // ReSharper disable once ObjectCreationAsStatement
             this.instructionsForm = new InstructionsDisplay(this);
        }

        /// <summary>
        /// Shows the instructions form.
        /// </summary>
        /// <param name="jobLoadedName">The job loaded.</param>
        private void ShowInstructionsForm(string jobLoadedName)
        {
            if (TykmaGlobals.INISettings.INSTRUCTIONSENABLE)
            {
                var args = new InstructionsEventMessage { Status = ImageState.LoadInstructions, NameOfLoadedJob = jobLoadedName };

                if (this.InstructionsLoadEvent != null)
                {
                    this.InstructionsLoadEvent(this, args);
                }
            }

            this.ux_ScannerInput.Focus();
        }

        /// <summary>
        /// Clears the instructions.
        /// </summary>
        private void ClearInstructions()
        {
            var args = new InstructionsEventMessage { Status = ImageState.ClearInstructions, NameOfLoadedJob = string.Empty };

            if (this.InstructionsLoadEvent != null)
            {
                this.InstructionsLoadEvent(this, args);
            }
        }
    }
}
