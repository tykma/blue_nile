﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FoundFileData.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.FileParse
{
    using System.Collections.Generic;

    using Tykma.Core.Entity;

    /// <summary>
    /// The found file data.
    /// </summary>
    public class FoundFileData
    {
        /// <summary>
        /// The _job name.
        /// </summary>
        private string jobName = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="FoundFileData"/> class.
        /// </summary>
        /// <param name="entityList">The entity list.</param>
        /// <param name="jobName">Name of the job.</param>
        /// <param name="qty">The quantity.</param>
        internal FoundFileData(List<EntityInformation> entityList, string jobName, int qty)
        {
            this.ListOfEntities = entityList;
            this.JobName = jobName;
            this.Quantity = qty;
        }

        /// <summary>
        /// Gets the list of entities.
        /// </summary>
        /// <value>
        /// The list of entities.
        /// </value>
        public List<EntityInformation> ListOfEntities { get; private set; }

        /// <summary>
        /// Gets the name of the job.
        /// </summary>
        /// <value>
        /// The name of the job.
        /// </value>
        public string JobName
        {
            get
            {
                return this.jobName;
            }

            private set
            {
                this.jobName = value.TrimEnd();
            }
        }

        /// <summary>
        /// Gets the quantity.
        /// </summary>
        /// <value>
        /// The quantity.
        /// </value>
        public int Quantity { get; private set; }
    }
}
