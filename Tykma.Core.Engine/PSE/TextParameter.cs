﻿namespace Tykma.Core.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class TextParameter
    {
        public TextParameter(string font, double space,
            double charheight, double charwidthratio,
            double charangle, double charspace,
            double linespace, double nullcharwidthratio, int textalign, bool isbold, bool isitalic, int spaceMode)
        {
            this.FontName = font;
            this.FontSpace = space;
            this.CharacterHeight = charheight;
            this.CharacterWidthRatio = charwidthratio;
            this.CharacterAngle = charangle;
            this.CharacterSpace = charspace;
            this.LineSpace = linespace;
            this.NullCharWidthRatio = nullcharwidthratio;
            this.TextAlign = textalign;
            this.IsBold = isbold;
            this.IsItalic = isitalic;
            this.TextSpaceMode = spaceMode;
        }

        public int TextSpaceMode { get; }
        public string FontName { get; }

        public double FontSpace { get; }

        public double CharacterHeight { get; }

        public double CharacterWidthRatio { get; }

        public double CharacterAngle { get; }

        public double CharacterSpace { get; }

        public double LineSpace { get; }

        public double NullCharWidthRatio { get; }

        public int TextAlign { get; }

        public bool IsBold { get; }

        public bool IsItalic { get; }
    }
}
