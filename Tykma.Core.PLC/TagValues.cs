﻿//-----------------------------------------------------------------------
// <copyright file="TagValues.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Tykma.Core.PLC
{
    using System;

    /// <summary>
    /// Values of a tag.
    /// </summary>
    public class TagValues : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TagValues" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="nettype">The net type.</param>
        /// <param name="qualitystring">The quality string.</param>
        /// <param name="timestamp">The timestamp.</param>
        /// <param name="tagIsBooleanDataType">if set to <c>true</c> [tag is boolean data type].</param>
        public TagValues(string name, string value, string nettype, string qualitystring, string timestamp, bool tagIsBooleanDataType = false)
        {
            this.Name = name;
            this.NetType = nettype;
            this.QualityString = qualitystring;
            this.TimeStamp = timestamp;

            if (tagIsBooleanDataType)
            {
                if (value == "1" || value.Equals("True", StringComparison.InvariantCultureIgnoreCase))
                {
                    this.Value = "True";
                    this.BoolValue = true;
                }
                else
                {
                    this.Value = "False";
                    this.BoolValue = false;
                }
            }
            else
            {
                this.Value = value;
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public string Value { get; }

        /// <summary>
        /// Gets a value indicating whether or not the value is true.
        /// </summary>
        /// <value>
        /// The boolean value.
        /// </value>
        public bool BoolValue { get; }

        /// <summary>
        /// Gets the tag net type..
        /// </summary>
        /// <value>
        /// The type of the net.
        /// </value>
        public string NetType { get; }

        /// <summary>
        /// Gets the tag quality string.
        /// </summary>
        /// <value>
        /// The quality string.
        /// </value>
        public string QualityString { get; }

        /// <summary>
        /// Gets the tag time stamp.
        /// </summary>
        /// <value>
        /// The time stamp.
        /// </value>
        public string TimeStamp { get; }
    }
}