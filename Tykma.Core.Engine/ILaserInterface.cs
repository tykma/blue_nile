﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILaserInterface.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Threading.Tasks;
    using Tykma.Core.Engine.PSE;
    using Tykma.Core.Entity;

    /// <summary>
    /// Laser engine interface.
    /// Provides methods/properties which are common to all laser APIs.
    /// Minilase Pro SE invokes a DLL.
    /// Lighter uses the 'ethernet protocol script'.
    /// ****************
    /// Best suited for a COM interop (Smartist/Minilase pro/Scanmaster).
    /// Not every engine will implement every feature;
    /// there is some difference between lighter and PSE
    /// mostly involved data retrieval and axis handling.
    /// ****************
    /// Most of the commands will return a custom exception.
    /// </summary>
    public interface ILaserInterface
    {
        /// <summary>
        /// Occurs when data is ready.
        /// </summary>
        event EventHandler<LaserStatusMessage> LaserStatusMessage;

        /// <summary>
        /// Occurs when data is ready.
        /// </summary>
        event EventHandler<ConnStateMessage> ConnectionMessage;

        /// <summary>
        /// Gets the state of the outputs.
        /// </summary>
        /// <value>
        /// The state of the outputs.
        /// </value>
        OutputsMemory OutputsState { get; }

        /// <summary>
        /// Gets a value indicating whether [marking is busy].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [marking busy]; otherwise, <c>false</c>.
        /// </value>
        bool MarkingBusy { get; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="ILaserInterface"/> is connected.
        /// </summary>
        /// <value>
        ///   <c>true</c> if connected; otherwise, <c>false</c>.
        /// </value>
        bool Connected { get; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="ILaserInterface"/> is connecting.
        /// </summary>
        /// <value>
        ///   <c>true</c> if connecting; otherwise, <c>false</c>.
        /// </value>
        bool Connecting { get; }

        /// <summary>
        /// Gets a value indicating whether [limits on].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [limits on]; otherwise, <c>false</c>.
        /// </value>
        bool LimitsOn { get; }

        /// <summary>
        /// Gets a value indicating whether were [limits contoured].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [limits contoured]; otherwise, <c>false</c>.
        /// </value>
        bool LimitsContoured { get; }

        /// <summary>
        /// Gets the file extension.
        /// </summary>
        /// <value>
        /// The file extension.
        /// </value>
        string FileExtension { get; }

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <value>
        /// The engine.
        /// </value>
        EngineTypes.EngineType Engine { get; }

        /// <summary>
        /// Stops the mark.
        /// </summary>
        /// <returns>Return code message.</returns>
        Task<bool> StopMark();

        /// <summary>
        /// Starts the mark.
        /// </summary>
        /// <returns>Mark attempt success state.</returns>
        Task<bool> StartMark();

        /// <summary>
        /// Starts the limits.
        /// </summary>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        /// <param name="contourMark">if set to <c>true</c> [contour mark].</param>
        void StartLimits(bool enable, bool contourMark, string textName = null);


        /// <summary>
        /// Clears the project.
        /// </summary>
        /// <returns>Success state.</returns>
        Task<bool> ClearProject();

        /// <summary>
        /// Sets the specified I/O output on the 5V port.
        /// </summary>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        /// <param name="whichPort">Which pin.</param>
        /// <returns>
        /// Success state.
        /// </returns>
        Task<bool> SetOutput(bool enable, int whichPort);

        /// <summary>
        /// Rotates a given object.
        /// </summary>
        /// <param name="text">The object name.</param>
        /// <param name="centerX">The center in the X plane.</param>
        /// <param name="centerY">The center in the Y plane.</param>
        /// <param name="angle">The angle to rotate.</param>
        /// <returns>Return code message.</returns>
        bool RotateObject(string text, double centerX, double centerY, double angle);

        /// <summary>
        /// Resets the specified axis.
        /// </summary>
        /// <param name="axis1">if set to <c>true</c> [reset axis 1].</param>
        /// <param name="axis2">if set to <c>true</c> [reset axis 2].</param>
        /// <param name="axis3">if set to <c>true</c> [reset axis 3].</param>
        /// <param name="axis4">if set to <c>true</c> [reset axis 4].</param>
        /// <returns>
        /// Return code message.
        /// </returns>
        bool ResetAxis(bool axis1, bool axis2, bool axis3, bool axis4);

        /// <summary>
        /// Reads the I/O port.
        /// </summary>
        /// <returns>
        /// Return code.
        /// </returns>
        ushort ReadPort();

        /// <summary>
        /// Moves given object to specified position.
        /// </summary>
        /// <param name="text">The name of the object..</param>
        /// <param name="moveX">Position in X plane to move to.</param>
        /// <param name="moveY">Position in Y plane to move to.</param>
        /// <returns>Return code message.</returns>
        bool MoveObject(string text, double moveX, double moveY);

        /// <summary>
        /// Loads a document.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>Success state.</returns>
        Task<bool> LoadDocument(string fileName);

        /// <summary>
        /// Gets the list of objects.
        /// </summary>
        /// <returns>List of available objects.</returns>
        IEnumerable<string> GetListOfObjects();

        /// <summary>
        /// Gets the preview bitmap.
        /// </summary>
        /// <param name="width">The width of the bitmap to return.</param>
        /// <param name="height">The height of the bitmap to return.</param>
        /// <returns>
        /// A bitmap.
        /// </returns>
        Bitmap GetPreviewBitmap(int width, int height);

        Bitmap GetPreviewBitmap(int width, int height, string objName);

        /// <summary>
        /// Gets the data of an object by its name
        /// </summary>
        /// <param name="text">The object to look at.</param>
        /// <returns>
        /// Return code.
        /// </returns>
        string GetObjectDataByName(string text);

        /// <summary>
        /// Gets the axis coordinate.
        /// </summary>
        /// <param name="axis">The axis number.</param>
        /// <returns>
        /// Axis coordinate.
        /// </returns>
        double GetAxisCoordinate(int axis);

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        void Dispose();

        /// <summary>
        /// Connects to the card.
        /// </summary>
        /// <returns>Connection attempt success state.</returns>
        Task<bool> Connect();

        /// <summary>
        /// Disconnects from the engine.
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Changes the value of a text field.
        /// </summary>
        /// <param name="textName">Name of the text.</param>
        /// <param name="textValue">The text value.</param>
        /// <returns>
        /// Success state.
        /// </returns>
        Task<bool> ChangeTextByName(string textName, string textValue);

        /// <summary>
        /// Adds a text object to project.
        /// </summary>
        /// <param name="textObject">The text object.</param>
        /// <returns>Success state</returns>
        bool AddTextToProject(TextObjectAttributes textObject);

        /// <summary>
        /// Moves the axis to a specified point.
        /// </summary>
        /// <param name="axis">The axis number.</param>
        /// <param name="goalPos">The goal position.</param>
        /// <returns>
        /// Return code.
        /// </returns>
        bool AxisMoveToPosition(int axis, double goalPos);

        /// <summary>
        /// Moves the axis a by specified pulse.
        /// </summary>
        /// <param name="axis">The axis number.</param>
        /// <param name="goalPos">The pulses to send.</param>
        /// <returns>
        /// Return code.
        /// </returns>
        bool AxisMoveToPulse(int axis, double goalPos);

        /// <summary>
        /// Homes the specified axis.
        /// </summary>
        /// <param name="whichAxis">Which axis number to home.</param>
        /// <returns>
        /// Return Code.
        /// </returns>
        Task<bool> AxisFindHome(int whichAxis);

        /// <summary>
        /// Saves the file.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>Success state.</returns>
        bool SaveFile(FileInfo filename);

        /// <summary>
        /// Gets the documents list.
        /// </summary>
        /// <returns>
        /// List of documents.
        /// </returns>
        List<string> GetDocumentsList();

        /// <summary>
        /// Deletes an object.
        /// </summary>
        /// <param name="objectName">Name of the object.</param>
        /// <returns>Object delection success state.</returns>
        bool DeleteObject(string objectName);

        /// <summary>
        /// Gets the laser power setting.
        /// </summary>
        /// <param name="penNumber">The pen number.</param>
        /// <returns>
        /// Power setting.
        /// </returns>
        double GetLaserPower(int penNumber);

        /// <summary>
        /// Gets the laser frequency.
        /// </summary>
        /// <param name="penNumber">The pen number.</param>
        /// <returns>
        /// Frequency associated with given object.
        /// </returns>
        int GetLaserFrequency(int penNumber);

        /// <summary>
        /// Sets the laser power.
        /// </summary>
        /// <param name="penNumber">The pen number.</param>
        /// <param name="power">The power to set.</param>
        /// <returns>
        /// Sucess state.
        /// </returns>
        bool SetLaserPower(int penNumber, double power);

        EntityInformation GetObjectSize(string whichEntity);

        bool ScaleSize(string whichEntity, double centerX, double centerY, double scaleX, double scaleY);

        /// <summary>
        /// Sets the laser frequency.
        /// </summary>
        /// <param name="penNumber">The pen number.</param>
        /// <param name="frequency">The frequency to set.</param>
        /// <returns>
        /// Success state.
        /// </returns>
        bool SetLaserFrequency(int penNumber, int frequency);

        /// <summary>
        /// Split a field into boxes.
        /// </summary>
        /// <returns>Success state.</returns>
        /// <param name="boxes">List of boxes</param>
        bool SplitIntoBoxes(IList<SplitBox> boxes);

        Task<bool> StartMarkObject(string objectName);

        bool ResetSN(string obj);

        bool SetFontHeight(string entity, double fontHeight);

        bool SetFont(string entity, string fontName);

        bool SetFontItalic(string entity, bool italic);

        bool SetTextSpacing(string entity, double space);

        HatchEntAttributes GetHatchParameters(string id, int index);

        bool SetHatchParameters(string id, HatchEntAttributes attr, int index);

        bool SetVectorPath(string id, string path, string newID, double degree, bool scaling);

        int GetPenNumberFromObject(string whichEnt);

        int SetPenNumberToObject(string whichEnt, int penNo);
    }
}
