﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.IO.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//  Main form IO handling.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Icon_Interface.General;

    using Tykma.Core.Engine;
    using Tykma.Core.Engine.CustomExceptions;
    using Tykma.Icon.InputParse;

    /// <summary>
    /// IO class.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// The IO DWORD one shot.
        /// </summary>
        private ushort ioDwordOneShot;

        /// <summary>
        /// The io SHUTTERSTATE one shot.
        /// </summary>
        private bool ioShutterstateOneShotOpen;

        /// <summary>
        /// The io SHUTTERSTATE one shot.
        /// </summary>
        private bool ioShutterstateOneShotClosed;

        /// <summary>
        /// The input parser.
        /// </summary>
        private InputParser inputParser;

        private bool startInputOneShot = true;

        private bool stopInputOneShot = true;

        /// <summary>
        /// Gets or sets a value indicating whether the start input is active.
        /// </summary>
        private bool StartInput
        {
            get
            {
                return this.startInputOneShot;
            }

            set
            {
                this.startInputOneShot = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the stop input is active.
        /// </summary>
        private bool StopInput
        {
            get
            {
                return this.stopInputOneShot;
            }

            set
            {
                this.stopInputOneShot = value;
            }
        }

        /// <summary>
        /// Output writer wrapper.
        /// </summary>
        /// <param name="enable">The enable.</param>
        /// <param name="whichPort">The which port.</param>
        /// <returns>Success state.</returns>
        private async Task<bool> OutputWrite(bool enable, int whichPort)
        {
            if (whichPort > 0 && whichPort <= 16)
            {
                var listOfIO = new List<Custom_Controls.IOLeds.ControlIOLEDBase>
                                                {
                                                    this.ux_LED_Output1,
                                                    this.ux_LED_Output2,
                                                    this.ux_LED_Output3,
                                                    this.ux_LED_Output4,
                                                    this.ux_LED_Output5,
                                                    this.ux_LED_Output6,
                                                    this.ux_LED_Output7,
                                                    this.ux_LED_Output8,
                                                    this.ux_LED_Output9,
                                                    this.ux_LED_Output10,
                                                    this.ux_LED_Output11,
                                                    this.ux_LED_Output12,
                                                    this.ux_LED_Output13,
                                                    this.ux_LED_Output14,
                                                    this.ux_LED_Output15,
                                                    this.ux_LED_Output16,
                                                };

                try
                {
                    var result = await this.le.SetOutput(enable, whichPort);
                    listOfIO[whichPort - 1].IsEnabled = enable;
                    return true;
                }
                catch (MethodFaultException e)
                {
                    this.TimeStampEvent(string.Format("Can not set output: {0}", e.Message));
                    Logger.Error("Can not set output: {0}", e.Message);
                }
                catch (NotImplementedException e)
                {
                    this.TimeStampEvent(string.Format("Can not set output: {0}", e.Message));
                    Logger.Error("Can not set output: {0}", e.Message);
                }
                catch (EngineNotConnectedException)
                {
                    this.TimeStampEvent("Can't set output: Engine not connected.");
                    Logger.Error("Can't set output: Engine not connected.");
                }
            }

            return false;
        }

        /// <summary>
        /// Sets the ext start config..
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void InitializeExtIO()
        {
            this.inputParser = new InputParser(
                TykmaGlobals.INISettings.IOEXTSTART,
                TykmaGlobals.INISettings.IOEXTSTOP,
                TykmaGlobals.INISettings.IOEXTSHUTTERSTATE,
                TykmaGlobals.INISettings.IOINVERT);

            Logger.Trace("Set external start as {0}", this.inputParser.StartInput.ToString());
            Logger.Trace("Set external stop as {0}", this.inputParser.StopInput.ToString());
            Logger.Trace("Set external shutter state as {0}", this.inputParser.ShutterInput.ToString());

            if (TykmaGlobals.INISettings.IOSCANRATE > 0)
            {
                this.ux_Timer_IO.Interval = TykmaGlobals.INISettings.IOSCANRATE;
                Logger.Trace("Set IO scan rate to {0} ms", TykmaGlobals.INISettings.IOSCANRATE);
            }
        }

        /// <summary>
        /// Parses the IO.
        /// </summary>
        /// <param name="ioDword">The io DWORD.</param>
        private void ParseIO(ushort ioDword)
        {
            if (ioDword == this.ioDwordOneShot)
            {
                return;
            }

            this.ioDwordOneShot = ioDword;

            var listOfCommands = this.inputParser.GetInputType(ioDword);

            if (listOfCommands.Contains(InputCommand.InputType.Start))
            {
                if (!this.StartInput)
                {
                    this.StartInput = true;
                    Logger.Info(ioDword);
                    Logger.Info("Received external start");
                    this.StartSequence();
                }
            }
            else
            {
                this.StartInput = false;
            }

            if (listOfCommands.Contains(InputCommand.InputType.Stop))
            {
                if (!this.StopInput)
                {
                    this.StopInput = true;
                    Logger.Info("Received external stop");
                    this.StopSequence();
                }
            }
            else
            {
                this.StopInput = false;
            }

            // Is there a shutter signal?
            if (listOfCommands.Contains(InputCommand.InputType.ShutterState))
            {
                if (!this.ioShutterstateOneShotOpen)
                {
                    Logger.Info("Shutter opened.");
                    this.TimeStampEvent("Shutter opened");
                    this.ioShutterstateOneShotOpen = true;
                    this.ShowShutterOpenState(true);
                    this.ioShutterstateOneShotClosed = false;
                }
            }
            else
            {
                if (!this.ioShutterstateOneShotClosed)
                {
                    this.ioShutterstateOneShotClosed = true;
                    this.ShowShutterOpenState(false);
                    Logger.Info("Shutter closed.");
                    this.TimeStampEvent("Shutter closed");
                    this.ioShutterstateOneShotOpen = false;
                }
            }

            this.SetInputControl(ioDword);
        }

        /// <summary>
        /// Sets the input LEDs.
        /// </summary>
        /// <param name="word">The word.</param>
        private void SetInputControl(int word)
        {
            var listofControls = new List<Custom_Controls.IOLeds.ControlIOLEDBase>
                                     { this.ux_LED_Input1, this.ux_LED_Input2, this.ux_LED_Input3, this.ux_LED_Input4, this.ux_LED_Input5, this.ux_LED_Input6, this.ux_LED_Input7, this.ux_LED_Input8, this.ux_LED_Input9, this.ux_LED_Input10, this.ux_LED_Input11, this.ux_LED_Input12, this.ux_LED_Input13, this.ux_LED_Input14, this.ux_LED_Input15, this.ux_LED_Input16 };

            for (int i = 0; i < listofControls.Count; i++)
            {
                listofControls[i].IsEnabled = this.inputParser.IsInputOn(i, word);
            }
        }

        /// <summary>
        /// Handles the Tick event of the IO timer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void TimerIOTick(object sender, EventArgs e)
        {
            if (this.le.Connected)
            {
                ushort ioStatus = this.ReadPort();
                this.ParseIO(ioStatus);
            }
        }

        /// <summary>
        /// Pulses the door open.
        /// </summary>
        /// <param name="open">if set to <c>true</c> [open].</param>
        private async void PulseDoorOpen(bool open)
        {
            int whichOutput = open ? 1 : 2;

            await this.OutputWrite(true, whichOutput);
            await Task.Delay(500);
            await this.OutputWrite(false, whichOutput);
        }

        /// <summary>
        /// Handles the Click event of the Output1LED control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void Output1LedClick(object sender, EventArgs e)
        {
            await this.OutputWrite(!this.le.OutputsState.Out1, 1);
        }

        /// <summary>
        /// Handles the Click event of the Output2LED control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void Output2LedClick(object sender, EventArgs e)
        {
            await this.OutputWrite(!this.le.OutputsState.Out2, 2);
        }

        /// <summary>
        /// Handles the Click event of the Output3LED control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void Output3LedClick(object sender, EventArgs e)
        {
            await this.OutputWrite(!this.le.OutputsState.Out3, 3);
        }

        /// <summary>
        /// Handles the Click event of the Output4LED control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void Output4LedClick(object sender, EventArgs e)
        {
            await this.OutputWrite(!this.le.OutputsState.Out4, 4);
        }

        /// <summary>
        /// Handles the Click event of the Output5LED control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void Output5LedClick(object sender, EventArgs e)
        {
            await this.OutputWrite(!this.le.OutputsState.Out5, 5);
        }

        /// <summary>
        /// Handles the Click event of the Output6LED control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void Output6LedClick(object sender, EventArgs e)
        {
            await this.OutputWrite(!this.le.OutputsState.Out6, 6);
        }

        /// <summary>
        /// Handles the Click event of the Output7LED control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void Output7LedClick(object sender, EventArgs e)
        {
            await this.OutputWrite(!this.le.OutputsState.Out7, 7);
        }

        /// <summary>
        /// Handles the Click event of the Output8LED control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void Output8LedClick(object sender, EventArgs e)
        {
            await this.OutputWrite(!this.le.OutputsState.Out8, 8);
        }

        /// <summary>
        /// Handles the Click event of the Output9LED control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void Output9LedClick(object sender, EventArgs e)
        {
            await this.OutputWrite(!this.le.OutputsState.Out9, 9);
        }

        /// <summary>
        /// Handles the Click event of the Output10LED control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void Output10LedClick(object sender, EventArgs e)
        {
            await this.OutputWrite(!this.le.OutputsState.Out10, 10);
        }

        /// <summary>
        /// Handles the Click event of the Output11LED control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void Output11LedClick(object sender, EventArgs e)
        {
            await this.OutputWrite(!this.le.OutputsState.Out11, 11);
        }

        /// <summary>
        /// Handles the Click event of the Output12LED control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void Output12LedClick(object sender, EventArgs e)
        {
            await this.OutputWrite(!this.le.OutputsState.Out12, 12);
        }

        /// <summary>
        /// Handles the Click event of the Output13LED control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void Output13LedClick(object sender, EventArgs e)
        {
            await this.OutputWrite(!this.le.OutputsState.Out13, 13);
        }

        /// <summary>
        /// Handles the Click event of the Output14LED control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void Output14LedClick(object sender, EventArgs e)
        {
            await this.OutputWrite(!this.le.OutputsState.Out14, 14);
        }

        /// <summary>
        /// Handles the Click event of the Output15LED control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void Output15LedClick(object sender, EventArgs e)
        {
            await this.OutputWrite(!this.le.OutputsState.Out15, 15);
        }

        /// <summary>
        /// Handles the Click event of the Output16LED control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void Output16LedClick(object sender, EventArgs e)
        {
            await this.OutputWrite(!this.le.OutputsState.Out16, 16);
        }
    }
}
