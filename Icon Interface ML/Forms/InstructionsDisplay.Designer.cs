﻿namespace Icon_Interface.Forms
{
    partial class InstructionsDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InstructionsDisplay));
            this.ux_Split_Container = new System.Windows.Forms.SplitContainer();
            this.ux_Picture_Instructions = new Cyotek.Windows.Forms.ImageBox();
            this.ux_RTF_Instructions = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Split_Container)).BeginInit();
            this.ux_Split_Container.Panel1.SuspendLayout();
            this.ux_Split_Container.Panel2.SuspendLayout();
            this.ux_Split_Container.SuspendLayout();
            this.SuspendLayout();
            // 
            // ux_Split_Container
            // 
            this.ux_Split_Container.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Split_Container.Location = new System.Drawing.Point(0, 0);
            this.ux_Split_Container.Name = "ux_Split_Container";
            this.ux_Split_Container.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // ux_Split_Container.Panel1
            // 
            this.ux_Split_Container.Panel1.Controls.Add(this.ux_Picture_Instructions);
            // 
            // ux_Split_Container.Panel2
            // 
            this.ux_Split_Container.Panel2.Controls.Add(this.ux_RTF_Instructions);
            this.ux_Split_Container.Size = new System.Drawing.Size(694, 502);
            this.ux_Split_Container.SplitterDistance = 288;
            this.ux_Split_Container.TabIndex = 0;
            this.ux_Split_Container.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.SplitContainerSplitterMoved);
            // 
            // ux_Picture_Instructions
            // 
            this.ux_Picture_Instructions.AllowClickZoom = true;
            this.ux_Picture_Instructions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ux_Picture_Instructions.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Picture_Instructions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Picture_Instructions.GridScale = Cyotek.Windows.Forms.ImageBoxGridScale.Large;
            this.ux_Picture_Instructions.Image = global::Icon_Interface.Properties.ImageResources.flowers;
            this.ux_Picture_Instructions.Location = new System.Drawing.Point(0, 0);
            this.ux_Picture_Instructions.Name = "ux_Picture_Instructions";
            this.ux_Picture_Instructions.PixelGridColor = System.Drawing.Color.DarkGray;
            this.ux_Picture_Instructions.SelectionMode = Cyotek.Windows.Forms.ImageBoxSelectionMode.Zoom;
            this.ux_Picture_Instructions.Size = new System.Drawing.Size(694, 288);
            this.ux_Picture_Instructions.StepSize = new System.Drawing.Size(5, 5);
            this.ux_Picture_Instructions.TabIndex = 2;
            this.ux_Picture_Instructions.VirtualSize = new System.Drawing.Size(300, 200);
            this.ux_Picture_Instructions.Zoom = 110;
            // 
            // ux_RTF_Instructions
            // 
            this.ux_RTF_Instructions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_RTF_Instructions.Location = new System.Drawing.Point(0, 0);
            this.ux_RTF_Instructions.Name = "ux_RTF_Instructions";
            this.ux_RTF_Instructions.ReadOnly = true;
            this.ux_RTF_Instructions.Size = new System.Drawing.Size(694, 210);
            this.ux_RTF_Instructions.TabIndex = 0;
            this.ux_RTF_Instructions.Text = "";
            // 
            // InstructionsDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 502);
            this.Controls.Add(this.ux_Split_Container);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InstructionsDisplay";
            this.Text = "Instructions";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.InstructionsDisplayFormClosing);
            this.Load += new System.EventHandler(this.InstructionsDisplayLoad);
            this.ResizeEnd += new System.EventHandler(this.InstructionsDisplayResizeEnd);
            this.ux_Split_Container.Panel1.ResumeLayout(false);
            this.ux_Split_Container.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_Split_Container)).EndInit();
            this.ux_Split_Container.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer ux_Split_Container;
        private System.Windows.Forms.RichTextBox ux_RTF_Instructions;
        private Cyotek.Windows.Forms.ImageBox ux_Picture_Instructions;
    }
}