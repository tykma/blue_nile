﻿// <copyright file="IDatabaseUtil.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Integration.SQL
{
    using System.Collections.Generic;
    using System.Data;

    /// <summary>
    /// Database utility interface.
    /// </summary>
    public interface IDatabaseUtil
    {
        /// <summary>
        /// Query an SQL database.
        /// </summary>
        /// <param name="query">Select query that returns a data table.</param>
        /// <param name="parameters">Query parameters with their values.</param>
        /// <returns>Query results as a DataTable.</returns>
        DataTable Select(string query, Dictionary<string, object> parameters = null);
    }
}
