﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LaserStatusMessage.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine
{
    using System;

    using Tykma.Core.Engine.ElectroxEngine;

    /// <summary>
    /// Marking state event.
    /// </summary>
    public enum MarkState
    {
        /// <summary>
        /// Marking has completed.
        /// </summary>
        Complete,

        /// <summary>
        /// Marking has started.
        /// </summary>
        Started,

        /// <summary>
        /// Marking is in progress.
        /// </summary>
        InProgress,

        /// <summary>
        /// There was an exception.
        /// </summary>
        Exception,

        /// <summary>
        /// The limits started.
        /// </summary>
        LimitsStarted,

        /// <summary>
        /// The limits stopped.
        /// </summary>
        LimitsStopped,

        /// <summary>
        /// There is no state.
        /// </summary>
        NoState
    }

    /// <summary>
    /// Laser Status.
    /// </summary>
    public enum LaserStatus
    {
        /// <summary>
        /// Off or disconnected.
        /// </summary>
        LaserOff,

        /// <summary>
        /// Warming up.
        /// </summary>
        LaserWarmUp,

        /// <summary>
        /// Waiting for start signal.
        /// </summary>
        LaserWait,

        /// <summary>
        /// In stand-by mode.
        /// </summary>
        LaserStandBy,

        /// <summary>
        /// In stand-by mode with the shutter closed.
        /// </summary>
        LaserStandByShutterClosed,

        /// <summary>
        /// Ready to mark.
        /// </summary>
        LaserReady,

        /// <summary>
        /// Ready with shutter closed.
        /// </summary>
        LaserReadyShutterClosed,

        /// <summary>
        /// Emitting laser radiation.
        /// </summary>
        LaserEmission,

        /// <summary>
        /// Busy with the shutter closed.
        /// </summary>
        LaserBusyShutterClosed,

        /// <summary>
        /// Warning is present.
        /// </summary>
        LaserWarning,

        /// <summary>
        /// An error is present.
        /// </summary>
        LaserError,

        /// <summary>
        /// Undefined message.
        /// </summary>
        LaserUndefined,

        /// <summary>
        /// Timeout message.
        /// </summary>
         LaserTimeout
    }

    /// <summary>
    /// Laser status class that is passed to the UX in an event form.
    /// Used for notifying of laser of state changes.
    /// If laser goes from non-busy to busy (emission).
    /// Can be used to hold many laser states (standby, error)
    /// but it is not implemented by all engines - and is currently unused
    /// in the interest of unification and simplification.
    /// </summary>
    public class LaserStatusMessage : EventArgs
    {
        /// <summary>
        /// Gets or sets the STATUS string.
        /// </summary>
        /// <value>
        /// The STATUS string.
        /// </value>
        public MarkState Status { get; set; }

        /// <summary>
        /// Gets or sets the state of the laser.
        /// </summary>
        /// <value>
        /// The state of the laser.
        /// </value>
        public LaserStatus LaserState { get; set; }

        /// <summary>
        /// Gets or sets the laser error message.
        /// </summary>
        /// <value>
        /// The laser error message.
        /// </value>
        public ErrorMessage.ErrorType LaserErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the laser error code.
        /// </summary>
        /// <value>
        /// The laser error code.
        /// </value>
        public string LaserErrorCode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="LaserStatusMessage"/> is fault.
        /// </summary>
        /// <value>
        ///   <c>true</c> if fault; otherwise, <c>false</c>.
        /// </value>
        public bool Fault { get; set; }

        /// <summary>
        /// Gets or sets the error string.
        /// </summary>
        /// <value>
        /// The error string.
        /// </value>
        public string ErrorDescription { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [laser state update].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [laser state update]; otherwise, <c>false</c>.
        /// </value>
        public bool LaserStateUpdate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [laser error update].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [laser error update]; otherwise, <c>false</c>.
        /// </value>
        public bool LaserErrorUpdate { get; set; }
    }
}
