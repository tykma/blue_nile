﻿using System;
using System.Drawing;
using Tykma.Core.Entity;

namespace Tykma.Maths.EntitySize
{
    /// <summary>
    /// Entity scaling maths class.
    /// </summary>
    public class EntityScale
    {
        /// <summary>
        /// Scale an entity to a new boundary.
        /// </summary>
        /// <param name="ent"></param>
        /// <param name="newBoundary"></param>
        public EntityScale(EntityInformation ent, EntityInformation newBoundary)
        {
            var entRect = SizeHelpers.GetEntityRectangle(ent);
            var boundaryRect = SizeHelpers.GetEntityRectangle(newBoundary);

            if (entRect.Width > boundaryRect.Width)
            {
                this.IsBeyondBoundaryX = true;
            }

            if (entRect.Height > boundaryRect.Height)
            {
                this.IsBeyondBoundaryY = true;
            }

            //this.Scale = (boundaryRect.Width / entRect.Width), (boundaryRect.Height / entRect.Height);
            var x = (boundaryRect.Width / entRect.Width);
            var y = (boundaryRect.Height / entRect.Height);
            this.Linear = (x, y);


            // Let's do the math with aspect ratio scaling as well.
            var ratioX = boundaryRect.Width / entRect.Width;
            var ratioY = boundaryRect.Height / entRect.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var arX = entRect.Width / ratio;
            var arY = entRect.Height / ratio;

            this.Aspect = (ratio, ratio);
        }

        /// <summary>
        /// Gets or sets a value indicating whether object is beyond its X boundary.
        /// </summary>
        public bool IsBeyondBoundaryX { get; }

        /// <summary>
        /// Gets or sets a value indicating whether object is beyond its Y boundary.
        /// </summary>
        public bool IsBeyondBoundaryY { get; }

        /// <summary>
        /// Get linear scaling.
        /// </summary>
        public (double x, double y) Linear { get; }

        /// <summary>
        /// Get aspect ratio scaling.
        /// </summary>
        public (double x, double y) Aspect { get; }

    }
}