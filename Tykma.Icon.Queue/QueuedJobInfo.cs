﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QueuedJobInfo.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// Information about the queued job info.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.Queue
{
    using System;

    /// <summary>
    /// Class that holds information about queued jobs.
    /// </summary>
    public class QueuedJobInfo
    {
        /// <summary>
        /// The job hash.
        /// </summary>
        private string jobHash = string.Empty;

        /// <summary>
        /// The job name.
        /// </summary>
        private string jobName = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="QueuedJobInfo"/> class.
        /// </summary>
        /// <param name="whichJob">The which job.</param>
        public QueuedJobInfo(string whichJob)
        {
            this.JobName = whichJob;
        }

        /// <summary>
        /// Gets the name of the job.
        /// </summary>
        /// <value>
        /// The name of the job.
        /// </value>
        public string JobName
        {
            get
            {
                return this.jobName;
            }

            private set
            {
                this.Finished = false;
                this.jobName = value;
                this.jobHash = Guid.NewGuid().ToString();
            }
        }

        /// <summary>
        /// Gets the job hash.
        /// </summary>
        /// <value>
        /// The job hash.
        /// </value>
        public string JobHash => this.jobHash;

        /// <summary>
        /// Gets or sets a value indicating whether this job is finished.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [finished]; otherwise, <c>false</c>.
        /// </value>
        public bool Finished { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [failed].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [failed]; otherwise, <c>false</c>.
        /// </value>
        public bool Failed { get; set; }
    }
}
