﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlButtonSave.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    /// <summary>
    /// The control button: save.
    /// </summary>
    public class ControlButtonSave : ControlButtonBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlButtonSave"/> class.
        /// </summary>
        public ControlButtonSave()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// The initialize component.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)this.ux_Image_Icon).BeginInit();
            this.SuspendLayout();

            this.ux_Label_Action.Text = "Save";

            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Name = "ControlButtonSave";
            this.Size = new System.Drawing.Size(85, 71);
            ((System.ComponentModel.ISupportInitialize)this.ux_Image_Icon).EndInit();
            this.ResumeLayout(false);
        }
    }
}
