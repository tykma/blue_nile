﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstructionsEventMessage.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// An event from the instruction message.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.General
{
    using System;

    /// <summary>
    /// Image import state.
    /// </summary>
    internal enum ImageState
    {
        /// <summary>
        /// Load the instructions.
        /// </summary>
        LoadInstructions,

        /// <summary>
        /// Clear the instructions.
        /// </summary>
        ClearInstructions,

        /// <summary>
        /// Close the form.
        /// </summary>
        Close,
    }

    /// <summary>
    /// Status message to pass to events.
    /// </summary>
    public class InstructionsEventMessage : EventArgs
    {
        /// <summary>
        /// Gets or sets the STATUS string.
        /// </summary>
        /// <value>
        /// The STATUS string.
        /// </value>
        internal ImageState Status { get; set; }

        /// <summary>
        /// Gets or sets the name of loaded job.
        /// </summary>
        /// <value>
        /// The name of loaded job.
        /// </value>
        internal string NameOfLoadedJob { get; set; }
    }
}
