﻿// <copyright file="SplitObject.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.SplitMark
{
    /// <summary>
    /// A split object class.
    /// </summary>
    public class SplitObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SplitObject"/> class.
        /// </summary>
        /// <param name="degree">The move degree.</param>
        /// <param name="box">The box dimensions.</param>
        public SplitObject(double degree, Box box)
        {
            this.Degree = degree;
            this.SplitBox = box;
        }

        /// <summary>
        /// Gets a value indicating the movement degree.
        /// </summary>
        public double Degree { get; }

        /// <summary>
        /// Gets a value indicating the dimensions of the split box.
        /// </summary>
        public Box SplitBox { get; }
    }
}
