﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdParamSetHatch.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// Command to set the hatching/fill settings.
    /// </summary>
    internal class CmdParamSetHatch : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdParamSetHatch" /> class.
        /// </summary>
        /// <param name="lmcApi">The LMC API.</param>
        /// <param name="contour">if set to <c>true</c> [contour].</param>
        /// <param name="hatch1">The hatch1.</param>
        /// <param name="hatch2">The hatch2.</param>
        public CmdParamSetHatch(IntPtr lmcApi, bool contour, HatchAttributes hatch1, HatchAttributes hatch2)
            : base(lmcApi)
        {
            this.EnableContour = contour;
            this.Hatch1 = hatch1;
            this.Hatch2 = hatch2;
        }

        /// <summary>
        /// Set the hatch parameter.
        /// </summary>
        /// <param name="beEnableContour">if set to <c>true</c> [enable contour].</param>
        /// <param name="bEnableHatch1">Enable hatch 1.</param>
        /// <param name="nPenNo1">The pen no 1.</param>
        /// <param name="nHatchAttrib1">The hatch attribute 1.</param>
        /// <param name="dHatchEdgeDist1">The hatch edge distance 1.</param>
        /// <param name="dHatchLineDist1">The hatch line distance 1.</param>
        /// <param name="dHatchStartOffset1">The hatch start offset 1.</param>
        /// <param name="dHatchEndOffset1">The hatch end offset 1.</param>
        /// <param name="dHatchAngle1">The hatch angle 1.</param>
        /// <param name="bEnableHatch2">Enable hatch 2.</param>
        /// <param name="nPenNo2">Pen no 2.</param>
        /// <param name="nHatchAttrib2">The hatch attribute 2.</param>
        /// <param name="dHatchEdgeDist2">The hatch edge distance 2.</param>
        /// <param name="dHatchLineDist2">The hatch line distance 2.</param>
        /// <param name="dHatchStartOffset2">The hatch start offset 2.</param>
        /// <param name="dHatchEndOffset2">The hatch end offset 2.</param>
        /// <param name="dHatchAngle2">The hatch angle2.</param>
        /// <returns>
        /// Pointer to function.
        /// </returns>
        private delegate int LMC1ApiSetHatchParameter(
            bool beEnableContour,
            int bEnableHatch1,
            int nPenNo1,
            int nHatchAttrib1,
            double dHatchEdgeDist1,
            double dHatchLineDist1,
            double dHatchStartOffset1,
            double dHatchEndOffset1,
            double dHatchAngle1,
            int bEnableHatch2,
            int nPenNo2,
            int nHatchAttrib2,
            double dHatchEdgeDist2,
            double dHatchLineDist2,
            double dHatchStartOffset2,
            double dHatchEndOffset2,
            double dHatchAngle2);

        /// <summary>
        /// Gets a value indicating whether to enable contour.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enable contour]; otherwise, <c>false</c>.
        /// </value>
        private bool EnableContour { get; }

        /// <summary>
        /// Gets  the first hatch.
        /// </summary>
        /// <value>
        /// The hatch1.
        /// </value>
        private HatchAttributes Hatch1 { get; }

        /// <summary>
        /// Gets the second hatch.
        /// </summary>
        /// <value>
        /// The hatch2.
        /// </value>
        private HatchAttributes Hatch2 { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        internal bool Execute()
        {
            var lmc1SetHatchParameter = this.LoadFunction<LMC1ApiSetHatchParameter>("lmc1_SetHatchParam");

            if (lmc1SetHatchParameter == null)
            {
                throw new EngineNotConnectedException();
            }

            int result = lmc1SetHatchParameter(
                this.EnableContour,
                this.Hatch1.EnableHatch ? 1 : 0,
                this.Hatch1.PenNumber,
                this.Hatch1.HatchAttrib,
                this.Hatch1.HatchEdgeDistance,
                this.Hatch1.HatchLineDistance,
                this.Hatch1.HatchStartOffset,
                this.Hatch1.HatchEndOffset,
                this.Hatch1.HatchAngle,
                this.Hatch2.EnableHatch ? 1 : 0,
                this.Hatch2.PenNumber,
                this.Hatch2.HatchAttrib,
                this.Hatch2.HatchEdgeDistance,
                this.Hatch2.HatchLineDistance,
                this.Hatch2.HatchStartOffset,
                this.Hatch2.HatchEndOffset,
                this.Hatch2.HatchAngle);

            var rm = this.GetMiniLaseReturnCode(result);

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
