﻿// <copyright file="BasicSerializer.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Icon.ArraySerialization
{
    using System.Collections.Generic;
    using System.Linq;

    using Core.Entity;

    /// <summary>
    /// Basic serializer
    /// Takes a list of entities and returns them ordered by their selected index property.
    /// </summary>
    public class BasicSerializer : IArraySerializer
    {
        /// <summary>
        /// Gets the serialization layout.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <returns>An ordered list of entities</returns>
        public IOrderedEnumerable<EntityInformation> GetSerializationLayout(List<EntityInformation> list)
        {
            // Create our list to hold entities.
            var rlist = new List<EntityInformation>();

            // We'll iterate through every entity, look at its name, extract any digits and place it into our return list.
            foreach (var entity in list)
            {
                int num;
                if (int.TryParse(string.Join(string.Empty, entity.Name.Where(char.IsDigit)), out num))
                {
                    entity.SerializationIndex = num;
                    rlist.Add(entity);
                }
                else
                {
                    // If there is no number, there is no serialization index.
                    entity.SerializationIndex = -1;
                    rlist.Add(entity);
                }
            }

            // Sort our entity list by the serialization index.
            var orderedItems = from pair in rlist orderby pair.SerializationIndex ascending select pair;

            // At this point our list can have jumps, ABC1, ABC5, ABC3.
            // We'll give it a numeric index.
            int i = 0;

            foreach (var item in orderedItems.Where(item => item.SerializationIndex != -1))
            {
                item.SerializationIndex = i;
                i++;
            }

            return orderedItems;
        }
    }
}
