﻿// <copyright file="CmdBase.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.ComponentModel;
    using System.Reflection;
    using System.Runtime.InteropServices;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// The command base.
    /// </summary>
    internal abstract class CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdBase" /> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking API.</param>
        protected CmdBase(IntPtr lmcApi)
        {
            this.LibLmcapi = lmcApi;
        }

        /// <summary>
        /// Gets or sets the lib laser marking controller API.
        /// </summary>
        internal IntPtr LibLmcapi { get; set; }

        /// <summary>
        /// Loads function with the specified name.
        /// </summary>
        /// <typeparam name="T">Generic type.</typeparam>
        /// <param name="name">The name.</param>
        /// <returns>
        /// Function pointer.
        /// </returns>
        internal T LoadFunction<T>(string name)
                    where T : class
                {
            /* if (!this.MiniLaseLoaded)
             {
                 // ReSharper disable once SuspiciousTypeConversion.Global
                 // ReSharper disable once ExpressionIsAlwaysNull
                 return IntPtr.Zero as T;
             }*/

            IntPtr address = NativeMethods.GetProcAddress(this.LibLmcapi, name);
            if (address == IntPtr.Zero)
            {
                // ReSharper disable once SuspiciousTypeConversion.Global
                // ReSharper disable once ExpressionIsAlwaysNull
                return IntPtr.Zero as T;
            }

            var fnptr = Marshal.GetDelegateForFunctionPointer(address, typeof(T));
            return fnptr as T;
        }

        /// <summary>
        /// Parses a return code integer into a return code message.
        /// </summary>
        /// <param name="returnCode">The return code.</param>
        /// <returns>Completed ReturnCodeMessage.</returns>
        internal ReturnCodeMessage GetMiniLaseReturnCode(int returnCode)
        {
            var returnMessage = new ReturnCodeMessage();

            if (returnCode <= Enum.GetValues(typeof(ReturnCodeEnum.ReturnCode)).Length)
            {
                returnMessage.Status = returnCode == 0 ? ReturnCodeMsg.Info : ReturnCodeMsg.Error;
                returnMessage.ReturnDescription = this.GetDescription((ReturnCodeEnum.ReturnCode)returnCode);
                returnMessage.ReturnCode = returnCode;

                if ((ReturnCodeEnum.ReturnCode)returnCode == ReturnCodeEnum.ReturnCode.LMC1_ERR_MINILASERUN)
                {
                    returnMessage.HardError = true;
                }

                return returnMessage;
            }

            returnMessage.ReturnDescription = "Unknown Error: " + returnCode;
            returnMessage.Status = ReturnCodeMsg.Error;

            return returnMessage;
        }

        /// <summary>
        /// Gets the mini lase not loaded message.
        /// </summary>
        /// <returns>A return code message.</returns>
        internal ReturnCodeMessage GetMiniLaseNotLoadedMessage()
        {
            var rm = new ReturnCodeMessage { Status = ReturnCodeMsg.Error, ReturnDescription = "Engine not loaded" };
            return rm;
        }

        /// <summary>
        /// Tests the engine method presence.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <returns>Whether or not engine is connected.</returns>
        /// <exception cref="EngineNotConnectedException">Engine is not connected.</exception>
        /// <exception cref="MethodFaultException">Call not found.</exception>
        internal bool TestEngineAndMethodPresence(object method)
        {
            if (this.LibLmcapi == null)
            {
                throw new EngineNotConnectedException();
            }

            if (method == null)
            {
                throw new MethodFaultException("Call not found.");
            }

            return true;
        }

        /// <summary>
        /// Gets the description from an ENUM.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Description of ENUM.</returns>
        private string GetDescription(Enum value)
        {
            FieldInfo fieldInfo = value.GetType().GetField(value.ToString());
            var attribute = Attribute.GetCustomAttribute(fieldInfo, typeof(DescriptionAttribute)) as DescriptionAttribute;
            return attribute == null ? value.ToString() : attribute.Description;
        }
    }
}
