﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdDocLoad.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// Command which loads a project file.
    /// </summary>
    internal class CmdDocLoad : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdDocLoad"/> class.
        /// </summary>
        /// <param name="lmcApi">
        /// The laser marker card API.
        /// </param>
        /// <param name="whichFile">
        /// Which file.
        /// </param>
        public CmdDocLoad(IntPtr lmcApi, string whichFile)
            : base(lmcApi)
        {
            this.FileToLoad = whichFile;
        }

        /// <summary>
        /// Load a PSE file.
        /// </summary>
        /// <param name="fileToLoad">The file to load.</param>
        /// <returns>Success code.</returns>
        private delegate int LMC1ApiLoadFile([MarshalAs(UnmanagedType.LPWStr)] string fileToLoad);

        /// <summary>
        /// Gets the file (path) to load.
        /// </summary>
        /// <value>
        /// The file to load.
        /// </value>
        private string FileToLoad { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        internal bool Execute()
        {
            // Get pointer to load pse file function.
            var lmc1FileLoad = this.LoadFunction<LMC1ApiLoadFile>("lmc1_LoadEzdFile");

            if (lmc1FileLoad == null)
            {
                throw new EngineNotConnectedException();
            }

            // Return our success state.
            var rm = this.GetMiniLaseReturnCode(lmc1FileLoad(this.FileToLoad));

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
