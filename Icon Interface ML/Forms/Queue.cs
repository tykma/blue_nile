﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Queue.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// Queue handling class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    using Icon_Interface.Properties;

    using Tykma.Icon.Queue;

    /// <summary>
    /// Form for handling our queue.
    /// </summary>
    public partial class Queue : Form
    {
        /// <summary>
        /// The queued items.
        /// </summary>
        private readonly JobQueue queuedItems;

        /// <summary>
        /// Initializes a new instance of the <see cref="Queue"/> class.
        /// </summary>
        internal Queue()
        {
            this.InitializeComponent();
            this.queuedItems = new JobQueue();
        }

        /// <summary>
        /// The job add callback delegate.
        /// </summary>
        /// <param name="whichJob">The which job.</param>
        private delegate void AddJobCallBack(string whichJob);

        /// <summary>
        /// Occurs when the queue data changes.
        /// </summary>
        internal event EventHandler<QueueEventMessage> QueueAvailabilityChanged;

        /// <summary>
        /// Gets a value indicating whether [jobs are available in the queue].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [jobs are available]; otherwise, <c>false</c>.
        /// </value>
        internal bool JobsAvailable
        {
            get { return this.queuedItems.CountAllToDo() > 0; }
        }

        /// <summary>
        /// Gets the next available job.
        /// </summary>
        /// <value>
        /// The next job's name.
        /// </value>
        internal QueuedJobInfo NextJob
        {
            get { return this.queuedItems.NextJob(); }
        }

        /// <summary>
        /// Adds a job to the queue.
        /// </summary>
        /// <param name="whichJob">Which job to add.</param>
        internal void AddJob(string whichJob)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new AddJobCallBack(this.AddJob), whichJob);
            }
            else
            {
                // We'll add the job.
                this.queuedItems.AddItemToQueue(whichJob);

                /* If there is nothing in our queue list box - populate it.
                   This is to avoid an argument exception that will happen
                   if we try to update the binding source with nothing in the
                   listbox which is already bound */
                if (this.ux_list_Queue.Items.Count < 1)
                {
                    this.PopulateQueueList();
                }

                this.ShowTotalCount();
            }
        }

        /// <summary>
        /// Removes a job.
        /// </summary>
        /// <param name="jobHash">The job hash.</param>
        internal void RemoveJob(string jobHash)
        {
            // Dequeue the job.
            this.queuedItems.RemoveJob(jobHash);

            // Force the listbox to redraw.
            this.ux_list_Queue.Refresh();

            // Show current job.
            this.FocusOnCurrentJob();

            // Show the total count.
            this.ShowTotalCount();
        }

        /// <summary>
        /// Sets all colors.
        /// </summary>
        private void SetAllColors()
        {
            this.BackColor = Settings.Default.BackColor;
            this.ForeColor = Settings.Default.ForeColor;
        }

        /// <summary>
        /// Focuses on the current job.
        /// </summary>
        private void FocusOnCurrentJob()
        {
            // Since we're using a multiline selection -
            // we'll clear all the selected items otherwise
            // multiple jobs will be selected.
            this.ux_list_Queue.ClearSelected();

            // The unique hash of the next active job.
            var currentHash = this.queuedItems.NextJob().JobHash;

            // We'll iterate through the list and if the next job is in it (it should be)
            // we'll highlight it.
            for (int i = 0; i < this.ux_list_Queue.Items.Count; i++)
            {
                if (((QueuedJobInfo)this.ux_list_Queue.Items[i]).JobHash == currentHash)
                {
                    this.ux_list_Queue.SelectedIndex = i;
                    break;
                }
            }
        }

        /// <summary>
        /// Handles the DrawItem event of the list queue control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DrawItemEventArgs"/> instance containing the event data.</param>
        private void ListQueueDrawItem(object sender, DrawItemEventArgs e)
        {
            // Each binding item is a DataRowView object.
            if (this.ux_list_Queue.Items.Count < 1)
            {
                return;
            }

            e.DrawBackground();

            // string hash = ux_list_Queue.Get
            var jobInfo = (QueuedJobInfo)this.ux_list_Queue.Items[e.Index];

            // Define the default color of the brush as black.
            Brush myBrush = Brushes.Black;

            if (jobInfo.Failed)
            {
                myBrush = Brushes.Red;
            }
            else if (jobInfo.Finished)
            {
                myBrush = Brushes.DarkGray;
            }

            e.Graphics.DrawString(jobInfo.JobName, e.Font, myBrush, e.Bounds, StringFormat.GenericDefault);

            // If the ListBox has focus, draw a focus rectangle around the selected item.
            e.DrawFocusRectangle();
        }

        /// <summary>
        /// Shows the total queue count.
        /// </summary>
        private void ShowTotalCount()
        {
            this.Text = string.Format("{0}/{1} Items", this.queuedItems.CountAllToDo(), this.queuedItems.CountAll());
            this.UpdateListenersOfStatus();
        }

        /// <summary>
        /// Updates the listeners of queue status.
        /// </summary>
        private void UpdateListenersOfStatus()
        {
                var args = new QueueEventMessage { JobsAvailable = this.JobsAvailable };

                if (this.QueueAvailabilityChanged != null)
                {
                    this.QueueAvailabilityChanged(this, args);
                }
        }

        /// <summary>
        /// Handles the Load event of the Queue control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void QueueLoad(object sender, EventArgs e)
        {
            // ReSharper disable once ArrangeStaticMemberQualifier
            if ((Control.ModifierKeys & Keys.Shift) == 0)
            {
                string initLocation = Settings.Default.InitialLocationQueue;
                var il = new Point(0, 0);
                Size sz = this.Size;
                if (!string.IsNullOrWhiteSpace(initLocation))
                {
                    string[] parts = initLocation.Split(',');
                    if (parts.Length >= 2)
                    {
                        il = new Point(int.Parse(parts[0]), int.Parse(parts[1]));
                    }

                    if (parts.Length >= 4)
                    {
                        sz = new Size(int.Parse(parts[2]), int.Parse(parts[3]));
                    }
                }

                this.Size = sz;
                this.Location = il;
                this.SetAllColors();
            }

            this.ShowTotalCount();
        }

        /// <summary>
        /// Handles the FormClosing event of the Queue control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="FormClosingEventArgs" /> instance containing the event data.</param>
        private void QueueFormClosing(object sender, FormClosingEventArgs e)
        {
            // ReSharper disable once ArrangeStaticMemberQualifier
            if ((Control.ModifierKeys & Keys.Shift) == 0)
            {
                Point location = this.Location;
                Size size = this.Size;
                if (this.WindowState != FormWindowState.Normal)
                {
                    location = this.RestoreBounds.Location;
                    size = this.RestoreBounds.Size;
                }

                string initLocation = string.Join(",", location.X, location.Y, size.Width, size.Height);
                Settings.Default.InitialLocationQueue = initLocation;
                Settings.Default.Save();
            }

            e.Cancel = true;
        }

        /// <summary>
        /// Populates the queue list.
        /// </summary>
        private void PopulateQueueList()
        {
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.ux_list_Queue.BeginUpdate();
            this.ux_list_Queue.BindingContext = this.BindingContext;
            this.ux_list_Queue.DataSource = null;
            this.ux_list_Queue.DataSource = this.queuedItems.FullList;
            this.ux_list_Queue.DisplayMember = "JobName";
            this.ux_list_Queue.ValueMember = "JobHash";
            this.ux_list_Queue.DrawMode = DrawMode.OwnerDrawFixed;
            this.ux_list_Queue.DrawItem += this.ListQueueDrawItem;

            this.ux_list_Queue.EndUpdate();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the list queue control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ListQueueSelectedIndexChanged(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Handles the KeyDown event of the List_Queue control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void ListQueueKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.A | Keys.Control))
            {
                for (int i = 0; i < this.ux_list_Queue.Items.Count; i++)
                {
                    this.ux_list_Queue.SetSelected(i, true);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the Pic_ok control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void PicOkClick(object sender, EventArgs e)
        {
            foreach (QueuedJobInfo li in this.ux_list_Queue.SelectedItems)
            {
                this.queuedItems.EnableJob(li.JobHash);
            }

            this.ux_list_Queue.Refresh();
            this.ShowTotalCount();
        }

        /// <summary>
        /// Handles the Click event of the Pic_skip control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void PicSkipClick(object sender, EventArgs e)
        {
            foreach (QueuedJobInfo li in this.ux_list_Queue.SelectedItems)
            {
                this.queuedItems.RemoveJob(li.JobHash);
            }

            this.ux_list_Queue.Refresh();
            this.ShowTotalCount();
        }

        /// <summary>
        /// Handles the Click event of the pic_clear control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void PicClearClick(object sender, EventArgs e)
        {
            this.queuedItems.Clear();
            this.ux_list_Queue.DataSource = null;
            this.ShowTotalCount();
        }
    }
}
