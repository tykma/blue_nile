﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScanCommand.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.ScanParse
{
    /// <summary>
    /// An internal command.
    /// </summary>
    public class ScanCommand
    {
        /// <summary>
        /// Internal icon command.
        /// </summary>
        public enum InternalCommand
        {
            /// <summary>
            /// Start request.
            /// </summary>
            Start,

            /// <summary>
            /// Clear request.
            /// </summary>
            Clear,

            /// <summary>
            /// Limits request.
            /// </summary>
            Limits,

            /// <summary>
            /// Increase quantity request.
            /// </summary>
            QuantityUp,

            /// <summary>
            /// Decrease quantity request.
            /// </summary>
            QuantityDown,

            /// <summary>
            /// Data edit request.
            /// </summary>
            Edit,

            /// <summary>
            /// Show job list.
            /// </summary>
            ShowJobList,

            /// <summary>
            /// Home Z request.
            /// </summary>
            HomeAxisZ,

            /// <summary>
            /// Home R request.
            /// </summary>
            HomeAxisR,

            /// <summary>
            /// Debug request.
            /// </summary>
            Debug,

            /// <summary>
            /// None request.
            /// </summary>
            None,
        }

        /// <summary>
        /// Gets or sets the command.
        /// </summary>
        /// <value>
        /// The command.
        /// </value>
        public InternalCommand Command { get; set; }
    }
}
