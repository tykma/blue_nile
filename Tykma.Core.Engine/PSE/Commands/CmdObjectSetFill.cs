﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdObjectSetFill.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// The command to set the fill on an object.
    /// </summary>
    internal class CmdObjectSetFill : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdObjectSetFill"/> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking API.</param>
        /// <param name="objectToFill">The object to fill.</param>
        public CmdObjectSetFill(IntPtr lmcApi, string objectToFill)
            : base(lmcApi)
        {
            this.ObjectToFill = objectToFill;
        }

        /// <summary>
        /// Hatch a given entity.
        /// </summary>
        /// <param name="pEntName">Name of the entity.</param>
        /// <returns>Return code.</returns>
        private delegate int LMC1ApiHatchEntity([MarshalAs(UnmanagedType.LPWStr)] string pEntName);

        /// <summary>
        /// Gets the object to fill.
        /// </summary>
        /// <value>
        /// The object to fill.
        /// </value>
        private string ObjectToFill { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        /// <exception cref="EngineNotConnectedException">Engine not connected.</exception>
        /// <exception cref="MethodFaultException">Problem setting the fill.</exception>
        internal bool Execute()
        {
            var lmc1HatchEnt = this.LoadFunction<LMC1ApiHatchEntity>("lmc1_HatchEnt");
            if (lmc1HatchEnt == null)
            {
                throw new EngineNotConnectedException();
            }

            ReturnCodeMessage rm = this.GetMiniLaseReturnCode(lmc1HatchEnt(this.ObjectToFill));

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
