﻿namespace Icon_Interface.Custom_Controls
{
    partial class ControlFileSelect
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ux_tlp_Design = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Button_ChooseFile = new System.Windows.Forms.PictureBox();
            this.ux_Text_PathInput = new System.Windows.Forms.TextBox();
            this.ux_Label_FilePath = new System.Windows.Forms.Label();
            this.ux_tlp_Design.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Button_ChooseFile)).BeginInit();
            this.SuspendLayout();
            // 
            // ux_tlp_Design
            // 
            this.ux_tlp_Design.ColumnCount = 3;
            this.ux_tlp_Design.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.ux_tlp_Design.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 86.17747F));
            this.ux_tlp_Design.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.82253F));
            this.ux_tlp_Design.Controls.Add(this.ux_Button_ChooseFile, 0, 0);
            this.ux_tlp_Design.Controls.Add(this.ux_Text_PathInput, 0, 0);
            this.ux_tlp_Design.Controls.Add(this.ux_Label_FilePath, 0, 0);
            this.ux_tlp_Design.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_tlp_Design.Location = new System.Drawing.Point(0, 0);
            this.ux_tlp_Design.Name = "ux_tlp_Design";
            this.ux_tlp_Design.RowCount = 1;
            this.ux_tlp_Design.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_tlp_Design.Size = new System.Drawing.Size(758, 36);
            this.ux_tlp_Design.TabIndex = 1;
            // 
            // ux_Button_ChooseFile
            // 
            this.ux_Button_ChooseFile.BackgroundImage = global::Icon_Interface.Properties.ImageResources.folder_open;
            this.ux_Button_ChooseFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ux_Button_ChooseFile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Button_ChooseFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Button_ChooseFile.ErrorImage = null;
            this.ux_Button_ChooseFile.Location = new System.Drawing.Point(683, 3);
            this.ux_Button_ChooseFile.Name = "ux_Button_ChooseFile";
            this.ux_Button_ChooseFile.Size = new System.Drawing.Size(72, 30);
            this.ux_Button_ChooseFile.TabIndex = 35;
            this.ux_Button_ChooseFile.TabStop = false;
            // 
            // ux_Text_PathInput
            // 
            this.ux_Text_PathInput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Text_PathInput.Location = new System.Drawing.Point(203, 8);
            this.ux_Text_PathInput.Name = "ux_Text_PathInput";
            this.ux_Text_PathInput.Size = new System.Drawing.Size(474, 20);
            this.ux_Text_PathInput.TabIndex = 34;
            // 
            // ux_Label_FilePath
            // 
            this.ux_Label_FilePath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Label_FilePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Label_FilePath.Location = new System.Drawing.Point(3, 6);
            this.ux_Label_FilePath.Name = "ux_Label_FilePath";
            this.ux_Label_FilePath.Size = new System.Drawing.Size(194, 24);
            this.ux_Label_FilePath.TabIndex = 1;
            this.ux_Label_FilePath.Text = "File Path";
            // 
            // ControlFileSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ux_tlp_Design);
            this.Name = "ControlFileSelect";
            this.Size = new System.Drawing.Size(758, 36);
            this.ux_tlp_Design.ResumeLayout(false);
            this.ux_tlp_Design.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Button_ChooseFile)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel ux_tlp_Design;
        private System.Windows.Forms.PictureBox ux_Button_ChooseFile;
        private System.Windows.Forms.TextBox ux_Text_PathInput;
        private System.Windows.Forms.Label ux_Label_FilePath;
    }
}
