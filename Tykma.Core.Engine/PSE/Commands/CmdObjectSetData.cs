﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdObjectSetData.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   The command to set the data on a text object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;

    using CustomExceptions;

    /// <summary>
    /// Change the text of a string.
    /// </summary>
    internal class CmdObjectSetData : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdObjectSetData"/> class.
        /// </summary>
        /// <param name="lmcApi">The LMC API.</param>
        /// <param name="textName">Name of the text.</param>
        /// <param name="data">The data.</param>
        public CmdObjectSetData(IntPtr lmcApi, string textName, string data)
            : base(lmcApi)
        {
            this.TextName = textName;
            this.TextData = data;
        }

        /// <summary>
        /// Change text given an ID.
        /// </summary>
        /// <param name="strTextName">Name of the text.</param>
        /// <param name="strTextNew">The new text.</param>
        /// <returns>Success code.</returns>
        private delegate int LMC1ApiChangeTextByName([MarshalAs(UnmanagedType.LPWStr)] string strTextName, [MarshalAs(UnmanagedType.LPWStr)] string strTextNew);

        /// <summary>
        /// Gets the name of the text.
        /// </summary>
        /// <value>
        /// The name of the text.
        /// </value>
        private string TextName { get; }

        /// <summary>
        /// Gets the text data.
        /// </summary>
        /// <value>
        /// The text data.
        /// </value>
        private string TextData { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        internal bool Execute()
        {
            // Get a pointer to the change text function.
            var lmc1ChangeText = this.LoadFunction<LMC1ApiChangeTextByName>("lmc1_ChangeTextByName");

            if (lmc1ChangeText == null)
            {
                throw new EngineNotConnectedException();
            }

            // Call change text.
            int result = lmc1ChangeText(this.TextName, this.TextData);

            // Get success state.
            ReturnCodeMessage rm = this.GetMiniLaseReturnCode(result);

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
