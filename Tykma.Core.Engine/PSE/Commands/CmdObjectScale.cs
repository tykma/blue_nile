﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdObjectScale.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// Command to scale the size of an object.
    /// </summary>
    internal class CmdObjectScale : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdObjectScale"/> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking API.</param>
        /// <param name="nameOfObject">The name of object.</param>
        /// <param name="centerX">The center x.</param>
        /// <param name="centerY">The center y.</param>
        /// <param name="scaleX">The scale x.</param>
        /// <param name="scaleY">The scale y.</param>
        public CmdObjectScale(IntPtr lmcApi, string nameOfObject, double centerX, double centerY, double scaleX, double scaleY)
            : base(lmcApi)
        {
            this.ObjectToScale = nameOfObject;
            this.CenterX = centerX;
            this.CenterY = centerY;
            this.ScaleX = scaleX;
            this.ScaleY = scaleY;
        }

        /// <summary>
        /// Set the scale of an entity.
        /// </summary>
        /// <param name="pEntName">Name of the entity.</param>
        /// <param name="dCenX">The center X.</param>
        /// <param name="dCentY">The center Y.</param>
        /// <param name="dScaleX">The scale X.</param>
        /// <param name="dScaleY">The scale Y.</param>
        /// <returns>Return Code.</returns>
        private delegate int LMC1ApiScaleEnt(
                                            [MarshalAs(UnmanagedType.LPWStr)] string pEntName,
                                            double dCenX,
                                            double dCentY,
                                            double dScaleX,
                                            double dScaleY);

        /// <summary>
        /// Gets the object to scale.
        /// </summary>
        /// <value>
        /// The object to scale.
        /// </value>
        private string ObjectToScale { get; }

        /// <summary>
        /// Gets the center x.
        /// </summary>
        /// <value>
        /// The center x.
        /// </value>
        private double CenterX { get; }

        /// <summary>
        /// Gets the center y.
        /// </summary>
        /// <value>
        /// The center y.
        /// </value>
        private double CenterY { get; }

        /// <summary>
        /// Gets the scale x.
        /// </summary>
        /// <value>
        /// The scale x.
        /// </value>
        private double ScaleX { get; }

        /// <summary>
        /// Gets the scale y.
        /// </summary>
        /// <value>
        /// The scale y.
        /// </value>
        private double ScaleY { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        /// <exception cref="EngineNotConnectedException">Card not connected.</exception>
        /// <exception cref="MethodFaultException">Problem scaling objection.</exception>
        internal bool Execute()
        {
            var lmc1SetScale = this.LoadFunction<LMC1ApiScaleEnt>("lmc1_ScaleEnt");

            if (lmc1SetScale == null)
            {
                throw new EngineNotConnectedException();
            }

            var rm = this.GetMiniLaseReturnCode(lmc1SetScale(this.ObjectToScale, this.CenterX, this.CenterY, this.ScaleX, this.ScaleY));

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
