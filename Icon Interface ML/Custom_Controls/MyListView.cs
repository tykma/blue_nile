﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MyListView.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// Custom listview - double buffered to avoid flickering when adding or removing items.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    using System.Windows.Forms;

    /// <summary>
    /// A double buffered list view override.
    /// </summary>
    public sealed class MyListView : ListView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MyListView" /> class.
        /// </summary>
        public MyListView()
        {
            this.DoubleBuffered = true;
        }
    }
}
