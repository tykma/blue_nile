﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OutputManipulation.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Output manipulation class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine
{
    /// <summary>
    /// The output manipulation.
    /// </summary>
    public class OutputManipulation
    {
        /// <summary>
        /// Returns the necessary word to write if turning an input on or off.
        /// </summary>
        /// <param name="enable">Whether to enable the port.</param>
        /// <param name="whichPort">Which port.</param>
        /// <param name="mem">The memory.</param>
        /// <returns>
        /// Port to write in binary.
        /// </returns>
        public ushort GeneratePortWordToWrite(bool enable, int whichPort, OutputsMemory mem)
        {
            mem.OutputStates[whichPort - 1] = enable;

            return (ushort)this.GetWord(mem);
        }

        /// <summary>
        /// Sets the output memory.
        /// </summary>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        /// <param name="whichPort">The which port.</param>
        /// <param name="mem">The memory.</param>
        /// <returns>New output memory status.</returns>
        public OutputsMemory SetOutputMemory(bool enable, int whichPort, OutputsMemory mem)
        {
            mem.OutputStates[whichPort] = enable;
            return mem;
        }

        /// <summary>
        /// Gets the output word.
        /// </summary>
        /// <param name="mem">The memory.</param>
        /// <returns>
        /// Outputs word.
        /// </returns>
        private int GetWord(OutputsMemory mem)
        {
            int outword = 0;

            for (int i = 0; i < 16; i++)
            {
                if (mem.OutputStates[i])
                {
                    outword += this.IntPow(2, i);
                }
            }

            return outword;
        }

        /// <summary>
        /// Raise number to a power for integers.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="power">The power.</param>
        /// <returns>
        /// Number raised to specified power.
        /// </returns>
        private int IntPow(int number, int power)
        {
            int result = 1;
            for (int i = 0; i < power; i++)
            {
                result *= number;
            }

            return result;
        }
    }
}
