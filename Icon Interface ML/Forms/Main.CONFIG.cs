﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.CONFIG.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;

    using Icon_Interface.File_Management.CmdFileImporter;
    using Icon_Interface.General;

    /// <summary>
    /// Partial class - we will handle setting config settings here.
    /// This serves as connector between our configuration class and our main interface.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// Configures the settings.
        /// </summary>
        private void ConfigureIniSettings()
        {
            // PLC Settings.
            if (TykmaGlobals.PLCSettings.PLCENABLED)
            {
                // Set PLC IP and scan time in the settigns screen.
                this.ux_PLC_IP.Text = TykmaGlobals.PLCSettings.PLCIP;
                this.ux_PLC_ScanTime.Value = TykmaGlobals.PLCSettings.PLCMAINSCANTIME <= 0 ? 100 : TykmaGlobals.PLCSettings.PLCMAINSCANTIME;

                // Make sure the scantime isn't too low.
                if (TykmaGlobals.PLCSettings.PLCSECONDARYSCANTIME <= 0)
                {
                    TykmaGlobals.PLCSettings.PLCSECONDARYSCANTIME = 200;
                }
                else
                {
                    this.ux_PLC_ScanTimeSecondary.Value = TykmaGlobals.PLCSettings.PLCSECONDARYSCANTIME;
                }

                // If the PLC class has been instantiated, set the PLC ip to one from our settings.
                if (this.tykmaPlc != null)
                {
                    this.tykmaPlc.IpAddress = TykmaGlobals.PLCSettings.PLCIP;
                }

                // If the plc is visible we will enable the PLC option menu.
                this.ux_Control_PLCSetting.Visible = true;

                // If the plc is enabled we'll enable the plc status strip.
                this.ux_ToolStrip_PLC.Visible = true;
                this.ux_Status_PLC.Visible = true;
            }

            // GFX replace restart option is visible if the dynamic importer is enabled.
            this.ux_ToolStrip_GFXReplacer.Visible = TykmaGlobals.INISettings.USEDYNAMICIMPORT;

            // Data import settings.
            this.ux_Import_ToggleFileImport.Checked = TykmaGlobals.INISettings.DATASEARCHENABLED;
            TykmaGlobals.INISettings.DATASEARCHENABLED = this.ux_Import_ToggleFileImport.Checked;
            this.ux_Control_ImportPath.SelectedPath = TykmaGlobals.INISettings.DATASEARCHFOLDER;
            this.ux_Control_CompletePath.SelectedPath = TykmaGlobals.INISettings.DATADONEFOLDER;
            this.ux_Control_ErrorPath.SelectedPath = TykmaGlobals.INISettings.DATAERRORFOLDER;
            this.ux_Control_SettingImport.Visible = true;
            this.ux_Status_Import_State.Visible = true;

            this.ShowSearchStatus(TykmaGlobals.INISettings.DATASEARCHENABLED
                ? FileImportState.Ready
                : FileImportState.Disabled);

            this.ux_Import_ToggleJobLoadMode.Checked = TykmaGlobals.INISettings.DATAJOBLOADMODE;

            this.ux_Check_InstructionsEnabled.Checked = TykmaGlobals.INISettings.INSTRUCTIONSENABLE;

            if (TykmaGlobals.INISettings.DATASEARCHSPEED >= this.ux_Import_PollRate.Minimum &&
                TykmaGlobals.INISettings.DATASEARCHSPEED <= this.ux_Import_PollRate.Maximum)
            {
                this.ux_Import_PollRate.Value = TykmaGlobals.INISettings.DATASEARCHSPEED;
            }

            if (TykmaGlobals.INISettings.DATASEARCHSPEED >= this.ux_Import_ReadWait.Minimum &&
                TykmaGlobals.INISettings.DATASEARCHSPEED <= this.ux_Import_ReadWait.Maximum)
            {
                this.ux_Import_ReadWait.Value = TykmaGlobals.INISettings.DATAWAITFORREAD;
            }
        }

        /// <summary>
        /// Parses the command line arguments.
        /// </summary>
        private void ParseCommandLineArguments()
        {
            string[] args = Environment.GetCommandLineArgs();
            foreach (string arg in args)
            {
                var split = arg.Split('=');
                if (split[0].StartsWith("/dllpath"))
                {
                    this.fileOperations.OverrideConfigRootPath(split[1].TrimEnd('"'));
                    break;
                }
            }
        }
    }
}
