﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReturnCodeEnum.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   The return code enum.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE
{
    using System.ComponentModel;

    /// <summary>
    /// The return code enumerator.
    /// </summary>
    public static class ReturnCodeEnum
    {
        /// <summary>
        /// ReturnCode ENUM.
        /// </summary>
        public enum ReturnCode
        {
            // ReSharper disable InconsistentNaming

            /// <summary>
            /// Code: 0
            /// </summary>
            [Description("Success")]

            // ReSharper disable UnusedMember.Local
            LMC1_ERR_SUCCESS,

            /// <summary>
            ///  Code: 1
            /// </summary>
            [Description("MiniLase is not running")]
            LMC1_ERR_MINILASERUN,

            /// <summary>
            /// Code: 2
            /// </summary>
            [Description("Can not find MINILASE.CFG")]
            LMC1_ERR_NOFINDCFGFILE,

            /// <summary>
            /// Code: 3
            /// </summary>
            [Description("Can not connect to the LMC1 board")]
            LMC1_ERR_FAILEDOPEN,

            /// <summary>
            /// Code: 4
            /// </summary>
            [Description("Can not find valid lmc1 device")]
            LMC1_ERR_NODEVICE,

            /// <summary>
            /// Code: 5
            /// </summary>
            [Description("LMC1 version error")]
            LMC1_ERR_HARDVER,

            /// <summary>
            /// Code:  6
            /// </summary>
            [Description("Can not find configuration files")]
            LMC1_ERR_DEVCFG,

            /// <summary>
            /// Code:  7
            /// </summary>
            [Description("Alarm signal")]
            LMC1_ERR_STOPSIGNAL,

            /// <summary>
            /// Code:  8
            /// </summary>
            [Description("User stop")]
            LMC1_ERR_USERSTOP,

            /// <summary>
            /// Code:  9
            /// </summary>
            [Description("Unknown error")]
            LMC1_ERR_UNKNOW,

            /// <summary>
            /// Code:  10
            /// </summary>
            [Description("Overtime")]
            LMC1_ERR_OUTTIME,

            /// <summary>
            /// Code:  11
            /// </summary>
            [Description("Un-initialized")]
            LMC1_ERR_NOINITIAL,

            /// <summary>
            /// Code:  12
            /// </summary>
            [Description("File read error")]
            LMC1_ERR_READFILE,

            /// <summary>
            /// Code:  13
            /// </summary>
            [Description("Window handle is NULL")]
            LMC1_ERR_OWENWNDNULL,

            /// <summary>
            /// Code:  14
            /// </summary>
            [Description("Can not find designated font")]
            LMC1_ERR_NOFINDFONT,

            /// <summary>
            /// Code:  15
            /// </summary>
            [Description("Wrong pen number")]
            LMC1_ERR_PENNO,

            /// <summary>
            /// Code:  16
            /// </summary>
            [Description("Object is not a text object")]
            LMC1_ERR_NOTTEXT,

            /// <summary>
            /// Code:  17
            /// </summary>
            [Description("Save file failed")]
            LMC1_ERR_SAVEFILE,

            /// <summary>
            /// Code:  18
            /// </summary>
            [Description("Can not find designated object")]
            LMC1_ERR_NOFINDENT,

            /// <summary>
            /// Code:  19
            /// </summary>
            [Description("Can not run the operation")]
            LMC1_ERR_STATUE,

            /// <summary>
            /// Code:  20
            /// </summary>
            [Description("UNKNOWN")]
            LMC1_ERR_UNKNOWN,

            /// <summary>
            /// Code:  21
            /// </summary>
            [Description("License not found")]
            LMC_ERR_LIC

            // ReSharper restore UnusedMember.Local
            // ReSharper restore InconsistentNaming
        }
    }
}
