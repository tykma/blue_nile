﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UriParserTest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.HTTPD
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Icon.TCPCommand;

    /// <summary>
    /// URI parser test class.
    /// </summary>
    [TestClass]
    public class UriParserTest
    {
        /// <summary>
        /// Tests the basic query.
        /// </summary>
        [TestMethod]
        public void TestBasicQuery()
        {
            const string Query = "move_axis&axis=1&location=up&SPEED=fast";
            var parsed = TykmaUriParser.DecodeQueryParameters(Query);

            Assert.AreEqual(parsed["axis"], "1");
            Assert.AreEqual(parsed["location"], "up");
            Assert.AreEqual(parsed["speed"], "fast");
        }

        /// <summary>
        /// Tests for duplicate parameters.
        /// </summary>
        [TestMethod]
        public void TestDuplicateParameters()
        {
            const string Query = "move_axis&axis=1&location=up&SPEED=fast&location=down";
            var parsed = TykmaUriParser.DecodeQueryParameters(Query);

            Assert.AreEqual(parsed["axis"], "1");
            Assert.AreEqual(parsed["location"], "up");
            Assert.AreEqual(parsed["speed"], "fast");
        }

        /// <summary>
        /// Tests for empty query.
        /// </summary>
        [TestMethod]
        public void TestEmptyQuery()
        {
            string query = string.Empty;
            var parsed = TykmaUriParser.DecodeQueryParameters(query);

            Assert.AreEqual(0, parsed.Count);
        }

        /// <summary>
        /// Tests for null query.
        /// </summary>
        [TestMethod]
        public void TestNullQuery()
        {
            var parsed = TykmaUriParser.DecodeQueryParameters(null);

            Assert.AreEqual(0, parsed.Count);
        }

        /// <summary>
        /// Tests for invalid query.
        /// </summary>
        [TestMethod]
        public void TestInvalidQuery()
        {
            const string Query = "???&???&=&&&";
            var parsed = TykmaUriParser.DecodeQueryParameters(Query);

            Assert.AreEqual(0, parsed.Count);
        }
    }
}
