﻿//-----------------------------------------------------------------------
// <copyright file="EntityInformation.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Tykma.Core.Entity
{
    using System;

    /// <summary>
    /// Class which will hold information about 'objects' or 'entities'.
    /// An object is a data field which we are able to programmatically control.
    /// Most commonly - text strings, barcodes, data-matrices.
    /// The majority of the time we will only need to know its name and its value.
    /// Knowing its positional information may be useful for rotation/moving.
    /// *******
    /// The class can be extended or derived to add additional functionality.
    /// </summary>
    public class EntityInformation
    {
        /// <summary>
        /// Backing field for the entity name property.
        /// </summary>
        private string name;

        /// <summary>
        /// Backing field for the entity value property.
        /// </summary>
        private string value;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityInformation"/> class.
        /// </summary>
        public EntityInformation()
            : this(string.Empty, string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityInformation"/> class.
        /// </summary>
        /// <param name="name">Entity name.</param>
        /// <param name="value">Entity value.</param>
        public EntityInformation(string name, string value)
            : this(-1, name, value)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityInformation"/> class.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">Entity name.</param>
        /// <param name="value">Entity value.</param>
        public EntityInformation(int index, string name, string value)
            : this(index, name, value, 0, 0, 0, 0, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityInformation"/> class.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="name">Entity name.</param>
        /// <param name="value">Entity value.</param>
        /// <param name="minx">Entity min X.</param>
        /// <param name="maxx">Max X.</param>
        /// <param name="miny">Min Y.</param>
        /// <param name="maxy">Max Y.</param>
        /// <param name="z">Z position.</param>
        public EntityInformation(
            int index,
            string name,
            string value,
            double minx,
            double maxx,
            double miny,
            double maxy,
            double z)
        {
            this.Index = index;
            this.Name = name;
            this.Value = value;
            this.MinX = minx;
            this.MinY = miny;
            this.MaxX = maxx;
            this.MaxY = maxy;
            this.Z = z;
        }

        /// <summary>
        /// Gets or sets the index of the object.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public int Index { get; set; }

        /// <summary>
        /// Gets or sets the name of the object..
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get => this.name;

            set
            {
                this.name = value ?? string.Empty;

                var val = this.name.Split(':');

                if (val.Length > 1)
                {
                    if (int.TryParse(val[val.Length-1], out int indexvalue))
                    {
                        this.IndexCount = indexvalue;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the name of the entity without its index value.
        /// </summary>
        public string NameNoIndex
        {
            get
            {
                return this.Name.Split(':')[0];
            }
        }

        /// <summary>
        /// Gets or sets the value of the object..
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public string Value
        {
            get => this.value;

            set => this.value = value ?? string.Empty;
        }

        /// <summary>
        /// Gets or sets a value indicating whether [non text object].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [non text object]; otherwise, <c>false</c>.
        /// </value>
        public bool IsTextObject { get; set; }

        /// <summary>
        /// Gets or sets the min X position of the object.
        /// </summary>
        /// <value>
        /// The min X.
        /// </value>
        public double MinX { get; set; }

        /// <summary>
        /// Gets or sets the min Y position of the object.
        /// </summary>
        /// <value>
        /// The min Y.
        /// </value>
        public double MinY { get; set; }

        /// <summary>
        /// Gets or sets the max X position of the object.
        /// </summary>
        /// <value>
        /// The max X.
        /// </value>
        public double MaxX { get; set; }

        /// <summary>
        /// Gets or sets the max Y position of the object.
        /// </summary>
        /// <value>
        /// The max Y.
        /// </value>
        public double MaxY { get; set; }

        /// <summary>
        /// Gets or sets the Z-height of the object.
        /// </summary>
        /// <value>
        /// The Z.
        /// </value>
        public double Z { get; set; }

        /// <summary>
        /// Gets or sets the entity's index count value.
        /// </summary>
        public int IndexCount { get; set; }

        /// <summary>
        /// Gets or sets the index of the serialization.
        /// </summary>
        /// <value>
        /// The index of the serialization.
        /// </value>
        public int SerializationIndex { get; set; }

        /// <summary>
        /// Gets the width of the entity.
        /// </summary>
        public double Width
        {
            get
            {
                return Math.Sqrt(Math.Pow(this.MaxX - this.MinX, 2));
            }
        }

        public double CenterX
        {
            get
            {
                return (this.MaxX + this.MinX) / 2;
            }
        }

        public double CenterY
        {
            get
            {
                return (this.MaxY + this.MinY) / 2;
            }
        }


        /// <summary>
        /// Gets the height of the entity.
        /// </summary>
        public double Height
        {
            get
            {
                return Math.Sqrt(Math.Pow(this.MaxY - this.MinY, 2));
            }
        }
    }
}
