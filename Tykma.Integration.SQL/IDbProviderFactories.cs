﻿// <copyright file="IDbProviderFactories.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Integration.SQL
{
    using System.Data.Common;

    /// <summary>
    /// DB factory provider.
    /// </summary>
    public interface IDbProviderFactories
    {
        /// <summary>
        /// Get the DB provider factory.
        /// </summary>
        /// <param name="name">Database name.</param>
        /// <returns>A DB factory.</returns>
        DbProviderFactory GetFactory(string name);
    }
}
