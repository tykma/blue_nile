﻿// <copyright file="MarkZ.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.Engine.ConfigSettings
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;

    /// <summary>
    /// Mark Z logic/math to figure out focal length.
    /// </summary>
    public class MarkZ
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MarkZ"/> class.
        /// </summary>
        /// <param name="path">Path to mark configuration file.</param>
        public MarkZ(string path)
        {
            this.Config = new List<string>();

            if (File.Exists(path))
            {
                using (var sr = File.OpenRead(path))
                {
                    using (var rdr = new StreamReader(sr))
                    {
                        while (rdr.Peek() >= 0)
                        {
                            this.Config.Add(rdr.ReadLine());
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets a list with every line in the configuration file.
        /// </summary>
        private IList<string> Config { get; }

        /// <summary>
        /// Gets the focus length.
        /// </summary>
        /// <returns>Focus length.</returns>
        public double? GetFocusLength()
        {
            foreach (var item in this.Config)
            {
                if (item.StartsWith($"FocusLength"))
                {
                    var speed = item.Split('=')[1];

                    if (speed != null)
                    {
                        if (double.TryParse(speed, NumberStyles.Any, CultureInfo.InvariantCulture, out double value))
                        {
                            return value;
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Get the base height.
        /// </summary>
        /// <returns>Base height.</returns>
        public double? GetBaseHeight()
        {
            foreach (var item in this.Config)
            {
                if (item.StartsWith($"BaseHeight"))
                {
                    var speed = item.Split('=')[1];

                    if (speed != null)
                    {
                        if (double.TryParse(speed, NumberStyles.Any, CultureInfo.InvariantCulture, out double value))
                        {
                            return value;
                        }
                    }
                }
            }

            return null;
        }
    }
}
