﻿//-----------------------------------------------------------------------
// <copyright file="IngearLogix.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Tykma.Core.PLC.AB
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Timers;

    using Logix;

    /// <summary>
    /// InGear PLC class.
    /// </summary>
    public class IngearLogix : IPLCInterface, IDisposable
    {
        /// <summary>
        /// My important group scan group (faster scan).
        /// </summary>
        private readonly TagGroup myImportantGroup = new TagGroup();

        /// <summary>
        /// My unimportant group (slower scan).
        /// </summary>
        private readonly TagGroup myUnimportantGroup = new TagGroup();

        /// <summary>
        /// PLC connector thread.
        /// </summary>
        private readonly BackgroundWorker bgwplcConnecter;

        /// <summary>
        /// Initialize the controller class.
        /// </summary>
        private readonly Controller myPlc;

        /// <summary>
        /// The important group timer.
        /// </summary>
        private readonly Timer importantGroupTimer;

        /// <summary>
        /// The unimportant group timer.
        /// </summary>
        private readonly Timer unimportantGroupTimer;

        /// <summary>
        /// Initializes a new instance of the <see cref="IngearLogix" /> class.
        /// </summary>
        /// <param name="timeOut">The time out.</param>
        /// <param name="scanMain">The scan speed for the main group.</param>
        /// <param name="scanSecond">The scan speed for the second group.</param>
        /// <param name="plcType">The PLC type.</param>
        public IngearLogix(int timeOut, int scanMain, int scanSecond, EPLCType plcType)
        {
            this.bgwplcConnecter = new BackgroundWorker();
            this.bgwplcConnecter.DoWork += this.BbgwplcConnecterDoWork;
            this.bgwplcConnecter.RunWorkerCompleted += this.BbgwplcConnecterRunWorkerCompleted;

            this.myPlc = new Controller { Timeout = timeOut, };
            if (plcType == EPLCType.Micro)
            {
                this.myPlc.CPUType = Controller.CPU.MICRO;
            }

            // Timers.
            this.importantGroupTimer = new Timer { Interval = scanMain };
            this.importantGroupTimer.Elapsed += this.ImportantGroupElapsed;
            this.importantGroupTimer.Enabled = false;

            this.unimportantGroupTimer = new Timer { Interval = scanSecond };
            this.unimportantGroupTimer.Elapsed += this.UnimportantGroupElapsed;
            this.unimportantGroupTimer.Enabled = false;
        }

        /// <summary>
        /// Occurs when [connection_ status_ event] is triggered.
        /// </summary>
        public event EventHandler<ConnectionStatusMessage> ConnectionStatusEvent;

        /// <summary>
        /// Occurs when [tag_ added_ event].
        /// </summary>
        public event EventHandler<TagValues> TagChangeEvent;

        /// <summary>
        /// Gets or sets the IP address.
        /// </summary>
        /// <value>
        /// The IP address.
        /// </value>
        public string IpAddress { get; set; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="IngearLogix" /> is connected.
        /// </summary>
        /// <value>
        ///   <c>true</c> if connected; otherwise, <c>false</c>.
        /// </value>
        public bool Connected => this.myPlc.IsConnected;

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="IPLCInterface" /> is in simulation mode.
        /// </summary>
        /// <value>
        ///   <c>true</c> if simulation; otherwise, <c>false</c>.
        /// </value>
        public bool Simulation
        {
            get
            {
                return this.myPlc.Simulate;
            }

            set
            {
                this.myPlc.Simulate = value;
            }
        }

        /// <inheritdoc />
        public EPLCType PLCType => EPLCType.ABLogix;

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Connect to the PLC.
        /// </summary>
        public void Connect()
        {
            if (!this.bgwplcConnecter.IsBusy)
            {
                this.bgwplcConnecter.RunWorkerAsync();
            }
        }

        /// <summary>
        /// Disconnects the PLC.
        /// </summary>
        public void Disconnect()
        {
            this.myPlc.Disconnect();
        }

        /// <summary>
        /// Automatically add a tag to the group.
        /// </summary>
        /// <param name="tagName">Name of the tag.</param>
        /// <param name="importantGroup">if set to <c>true</c> [important group].</param>
        /// <param name="datatype">The data type.</param>
        /// <typeparam name="T">The type.</typeparam>
        /// <returns>
        /// Success state.
        /// </returns>
        public async Task<bool> AutoTagAdd<T>(string tagName, bool importantGroup, TagTypes.ETagType datatype)
        {
            if (this.Connected)
            {
                // Let's initialize the tag.
                Tag newTag = await this.CreateNewtag(tagName);

                newTag.MyObject = tagName;

                if (datatype == TagTypes.ETagType.Boolean)
                {
                    newTag.DataType = Tag.ATOMIC.BOOL;
                }
                else if (datatype == TagTypes.ETagType.String)
                {
                    newTag.DataType = Tag.ATOMIC.STRING;
                }
                else if (datatype == TagTypes.ETagType.Integer)
                {
                    newTag.DataType = Tag.ATOMIC.INT;
                }

                // If the quality is good, add the tag to our group.
                if (newTag.QualityCode == ResultCode.QUAL_GOOD)
                {
                    if (importantGroup)
                    {
                        // Avoid re-adding a tag if it's in the group already.
                        if (this.myImportantGroup.Tags.Cast<Tag>().Any(gTag => gTag.Name == tagName))
                        {
                            return false;
                        }

                        await Task.Run(() => this.myImportantGroup.AddTag(newTag));
                        this.PushTagToTagView(newTag);
                        this.ScanImportantGroup(true);
                        return true;
                    }

                    // Avoid re-adding a tag if it's in the group already.
                    if (this.myUnimportantGroup.Tags.Cast<Tag>().Any(gTag => gTag.Name == tagName))
                    {
                        return false;
                    }

                    await Task.Run(() => this.myUnimportantGroup.AddTag(newTag));

                    this.PushTagToTagView(newTag);
                    this.ScanUnimportantGroup(true);
                    return true;
                }

                return false;
            }

            return false;
        }

        /// <inheritdoc/>
        public async Task<bool> WriteTag<T>(string tagName, object value)
        {
            if (!this.Connected)
            {
                return false;
            }

            // If the tag value is null, set it to empty.
            if (value == null)
            {
                return false;
            }

            // If the tag name is empty, we can't write it. exit method.
            if (tagName == null)
            {
                return false;
            }

            var tag = new Tag();
            tag.Name = tagName;
            tag.Value = value;

            if (typeof(T) == typeof(short))
            {
                tag.DataType = Tag.ATOMIC.INT;
            }
            else if (typeof(T) == typeof(int))
            {
                tag.DataType = Tag.ATOMIC.DINT;
            }
            else if (typeof(T) == typeof(string))
            {
                tag.DataType = Tag.ATOMIC.STRING;
            }
            else if (typeof(T) == typeof(bool))
            {
                tag.DataType = Tag.ATOMIC.BOOL;
            }

            try
            {
                int r = 0;
                await Task.Run(() =>
                {
                    r = this.myPlc.WriteTag(tag);
                });

                if (r != Logix.ResultCode.E_SUCCESS)
                {
                    throw new Exception($"Tag {tagName} Write exception: {tag.ErrorString}");
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new Exception("Range Exception Fault");
            }

            return true;
        }

        /// <summary>
        /// Changes the scan times.
        /// </summary>
        /// <param name="mainInterval">The main interval.</param>
        /// <param name="secondaryInterval">The secondary interval.</param>
        public void ChangeScanTimes(int mainInterval, int secondaryInterval)
        {
            this.importantGroupTimer.Interval = mainInterval;
            this.unimportantGroupTimer.Interval = secondaryInterval;
        }

        /// <summary>
        /// Scans the important group.
        /// </summary>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        public void ScanImportantGroup(bool enable)
        {
            if (enable)
            {
                if (this.myImportantGroup.Tags.Count > 0)
                {
                    this.myImportantGroup.Controller = this.myPlc;
                    if (!this.importantGroupTimer.Enabled)
                    {
                        this.importantGroupTimer.Enabled = true;
                    }
                }
            }
            else
            {
                this.importantGroupTimer.Enabled = false;
                this.myImportantGroup.Clear();
            }
        }

        /// <summary>
        /// Scans the unimportant group.
        /// </summary>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        public void ScanUnimportantGroup(bool enable)
        {
            if (enable)
            {
                if (this.myUnimportantGroup.Tags.Count > 0)
                {
                    this.myUnimportantGroup.Controller = this.myPlc;
                    if (!this.unimportantGroupTimer.Enabled)
                    {
                        this.unimportantGroupTimer.Enabled = true;
                    }
                }
            }
            else
            {
                this.unimportantGroupTimer.Enabled = false;
                this.myUnimportantGroup.Clear();
            }
        }

        /// <summary>
        /// Reads a tag.
        /// </summary>
        /// <param name="tagName">Name of the tag.</param>
        /// <returns>Value of tag.</returns>
        public string TagReadUnimportant(string tagName)
        {
            if (this.myUnimportantGroup.Tags.Count > 0)
            {
                string value = (from Tag readTag in this.myUnimportantGroup.Tags
                                where readTag.Name == tagName.ToUpper()
                                select readTag.Value?.ToString()).FirstOrDefault();

                if (value == null)
                {
                    return string.Empty;
                }

                return value;
            }

            return string.Empty;
        }

        /// <summary>
        /// The bulk of the clean-up code is implemented in Dispose(true).
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }

            // Clear out the important group if it's not empty.
            if (this.myImportantGroup != null)
            {
                this.myImportantGroup.Clear();
                this.myImportantGroup.Dispose();
            }

            // Clear out the unimportant group if it's not empty.
            if (this.myUnimportantGroup != null)
            {
                this.myUnimportantGroup.Clear();
                this.myUnimportantGroup.Dispose();
            }

            // Free managed resources
            this.bgwplcConnecter?.Dispose();

            this.importantGroupTimer?.Dispose();

            this.unimportantGroupTimer?.Dispose();

            if (this.myPlc != null)
            {
                this.myPlc.Disconnect();
                this.myPlc.Dispose();
            }
        }

        /// <summary>
        /// Encodes the non ASCII characters.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>A string with the unicode characters escaped.</returns>
        private string EncodeNonAsciiCharacters(string value)
        {
            var sb = new StringBuilder();
            foreach (char c in value)
            {
                if (c > 127)
                {
                    // This character is too big for ASCII.
                    string encodedValue = @"\\u" + ((int)c).ToString("X");
                    sb.Append(encodedValue);
                }
                else
                {
                    sb.Append(c);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Creates a new tag.
        /// </summary>
        /// <param name="tagName">Name of the tag.</param>
        /// <returns>The newly created tag.</returns>
        private async Task<Tag> CreateNewtag(string tagName)
        {
            // Create the new tag.
            Tag newTag = await Task.Run(() => new Tag(this.myPlc, tagName.ToUpper()));

            if (newTag.QualityCode == ResultCode.QUAL_GOOD)
            {
                // This event handler will be common for all tags.
                newTag.Changed += this.TagChanged;
            }

            return newTag;
        }

        /// <summary>
        /// Pushes the tag to tag view.
        /// </summary>
        /// <param name="tagToPush">The tag to push.</param>
        private void PushTagToTagView(Tag tagToPush)
        {
            var setName = tagToPush.MyObject.ToString();

            string setNetType = tagToPush.NetType?.ToString() ?? "N/A";

            string setValue = string.Empty;

            if (tagToPush.Value != null)
            {
                setValue = tagToPush.Value.ToString();
            }

            var setTimeStamp = tagToPush.TimeStamp.ToString(CultureInfo.InvariantCulture);
            var setQualityString = tagToPush.QualityString;

            var args = new TagValues(setName, setValue, setNetType, setQualityString, setTimeStamp, tagToPush.DataType == Tag.ATOMIC.BOOL);

            // Now we'll need to raise an event that the UI thread can subscribe to.
            this.TagChangeEvent?.Invoke(this, args);
        }

        /// <summary>
        /// Tag changed data handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void TagChanged(object sender, EventArgs e)
        {
            // We'll normally pass this off to the UI thread.
            var args = (DataChangeEventArgs)e;

            // We set a tag name in MyObject, retrieve it.
            var setName = args.MyObject.ToString();

            // If there is a value, get it, otherwise set it to N/A.
            string setValue = args.Value?.ToString() ?? "N/A";

            // Let's get the timestamp of this tag.
            var setTimeStamp = args.TimeStamp.ToString(CultureInfo.InvariantCulture);

            // Quality string.
            var setQualityString = args.QualityString;

            Tag sentTag = (Tag)sender;

            // Create a new tag values class with all our variables.
            var tag = new TagValues(setName, setValue, string.Empty, setQualityString, setTimeStamp, sentTag.DataType == Tag.ATOMIC.BOOL);

            // Notify any subscribes of a tag change event.
            this.TagChangeEvent?.Invoke(this, tag);
        }

        /// <summary>
        /// Background worker for the PLC connection.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs" /> instance containing the event data.</param>
        private void BbgwplcConnecterDoWork(object sender, DoWorkEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.IpAddress))
            {
                return;
            }

            this.myPlc.IPAddress = this.IpAddress;

            if (!this.Connected)
            {
                this.myPlc.Connect();
            }
        }

        /// <summary>
        /// Connection complete background worker.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs" /> instance containing the event data.</param>
        private void BbgwplcConnecterRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var args = new ConnectionStatusMessage();

            if (this.Connected)
            {
                args.ConnectedMessage = "Success";
                args.Connected = true;
            }
            else
            {
                args.ConnectedMessage = this.myPlc.ErrorString;
                args.Connected = false;
            }

            this.ConnectionStatusEvent?.Invoke(this, args);
        }

        /// <summary>
        /// Important group timer elapsed.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="e">The <see cref="ElapsedEventArgs" /> instance containing the event data.</param>
        private void ImportantGroupElapsed(object source, ElapsedEventArgs e)
        {
            if (this.Connected)
            {
                if (this.myPlc != null)
                {
                    if (this.myImportantGroup != null && this.myImportantGroup.Count > 0)
                    {
                        try
                        {
                            foreach (Tag item in this.myImportantGroup.Tags)
                            {
                                this.myPlc.ReadTag(item);
                            }

                            // Task.Run(() => this.myPlc.GroupRead(this.myImportantGroup)).Wait();
                        }
                        catch (InvalidOperationException ae)
                        {
                            this.LogException(ae.Message);

                            // Do nothing.
                            // This happens because the timer's elapsed event can fire.
                            // Even after it's been stopped or disposed.
                            // So we've disposed of the tag group but it's still being read.
                        }
                    }
                }
            }
        }

        private void LogException(string ex)
        {
            if (ex != null)
            {
                // do nothing.
            }
        }

        /// <summary>
        /// Unimportant group timer elapsed.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="e">The <see cref="ElapsedEventArgs" /> instance containing the event data.</param>
        private void UnimportantGroupElapsed(object source, ElapsedEventArgs e)
        {
            if (this.Connected)
            {
                if (this.myUnimportantGroup != null)
                {
                    try
                    {
                        foreach (Tag item in this.myUnimportantGroup.Tags)
                        {
                            this.myPlc.ReadTag(item);
                        }

                        // Task.Run(() => this.myPlc.GroupRead(this.myUnimportantGroup)).Wait();
                    }
                    catch (InvalidOperationException ae)
                    {
                        this.LogException(ae.Message);

                        // Do nothing.
                        // This happens because the timer's elapsed event can fire.
                        // Even after it's been stopped or disposed.
                        // So we've disposed of the tag group but it's still being read.
                    }
                }
            }
        }
    }
}