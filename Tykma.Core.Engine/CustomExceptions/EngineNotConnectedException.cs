﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EngineNotConnectedException.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.CustomExceptions
{
    using System;

    /// <summary>
    /// An exception that occurs when an engine is not
    /// connected and we attempt to interact with it.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Usage",
        "CA2237:MarkISerializableTypesWithSerializable",
        Justification = "App will not run in multiple appDomains")]
    public class EngineNotConnectedException : Exception
    {
    }
}
