﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Auth.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// Authorization class for elevating user permissions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.General
{
    using System.Collections.Generic;
    using BCrypt.Net;
    using Icon_Interface.Properties;

    /// <summary>
    /// Authorization class for elevating user permissions.
    /// </summary>
    internal class Auth
    {
        /// <summary>
        /// Gets a value indicating whether [engineer authorized].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [engineer authorized]; otherwise, <c>false</c>.
        /// </value>
        internal bool EngineerAuthorized { get; private set; }

        /// <summary>
        /// Gets or sets the engineer password.
        /// </summary>
        /// <value>
        /// The engineer password.
        /// </value>
        private string EngineerPassword
        {
            get
            {
                return Settings.Default.EngineerPassword;
            }

            set
            {
                Settings.Default.EngineerPassword = BCrypt.HashPassword(value, BCrypt.GenerateSalt(12));
            }
        }

        /// <summary>
        /// Gets the master password.
        /// </summary>
        /// <value>
        /// The master password.
        /// </value>
        private string MasterPassword
        {
            get
            {
                return "$2a$10$MMRaBgQuV2HqNbnTCpZ9d.Eo6jzcQ/YJycHcMwOOElVg.UbAFtV2C";
            }
        }

        /// <summary>
        /// Verifies the engineer password.
        /// </summary>
        /// <param name="passToCheck">The pass to check.</param>
        /// <returns>Whether or not the password is accurate.</returns>
        internal bool VerifyEngineerPassword(string passToCheck)
        {
            var listOfValidHashes = new List<string> { this.EngineerPassword, this.MasterPassword };

            if (!this.EngineerAuthorized)
            {
                foreach (var hash in listOfValidHashes)
                {
                    if (BCrypt.Verify(passToCheck, hash))
                    {
                        this.EngineerAuthorized = true;
                    }
                }
            }

            return this.EngineerAuthorized;
        }

        /// <summary>
        /// Become operator.
        /// </summary>
        internal void BecomeOperator()
        {
            this.EngineerAuthorized = false;
        }

        /// <summary>
        /// Changes the engineer password.
        /// </summary>
        /// <param name="newPassword">The new password.</param>
        internal void ChangeEngineerPassword(string newPassword)
        {
            this.EngineerPassword = newPassword;
            Settings.Default.Save();
        }
    }
}
