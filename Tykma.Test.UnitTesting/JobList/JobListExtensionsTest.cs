﻿// <copyright file="JobListExtensionsTest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Test.UnitTesting.JobList
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Tykma.Icon.JobList;

    /// <summary>
    /// Test job list extensions.
    /// </summary>
    [TestClass]
    public class JobListExtensionsTest
    {
        /// <summary>
        /// Test basic extension stripping.
        /// </summary>
        [TestMethod]
        public void TestExtensionStrippingBasic()
        {
            var list = new List<string> { "Z.jpeg", "A..jpeg", "q.PNG" };

            list = list.RemoveExtensions().ToList();

            Assert.IsTrue(list.Contains("Z"));
            Assert.IsTrue(list.Contains("A."));
            Assert.IsTrue(list.Contains("q"));
        }

        /// <summary>
        /// Tests stripping an empty extension.
        /// </summary>
        [TestMethod]
        public void TestEmptyExtension()
        {
            var list = new List<string> { "Z", "A.", "q" };

            list = list.RemoveExtensions().ToList();

            Assert.IsTrue(list.Contains("Z"));
            Assert.IsTrue(list.Contains("A"));
            Assert.IsTrue(list.Contains("q"));
        }

        /// <summary>
        /// Tests removing extension on an empty string.
        /// </summary>
        [TestMethod]
        public void TestEmptyString()
        {
            var list = new List<string> { "Z", "A.", string.Empty };

            list = list.RemoveExtensions().ToList();

            Assert.IsTrue(list.Contains("Z"));
            Assert.IsTrue(list.Contains("A"));
            Assert.IsTrue(list.Contains(string.Empty));
        }

        /// <summary>
        /// Tests removing extension on a null string.
        /// </summary>
        [TestMethod]
        public void TestNullString()
        {
            var list = new List<string> { "Z", "A.", null };

            list = list.RemoveExtensions().ToList();

            Assert.IsTrue(list.Contains("Z"));
            Assert.IsTrue(list.Contains("A"));
            Assert.IsTrue(list.Contains(null));
        }

        /// <summary>
        /// Tests removing extension on a null list.
        /// </summary>
        [TestMethod]
        public void TestNullList()
        {
            List<string> list = null;

            // ReSharper disable once ExpressionIsAlwaysNull
            list = list.RemoveExtensions().ToList();

            Assert.AreEqual(0, list.Count);
        }
    }
}
