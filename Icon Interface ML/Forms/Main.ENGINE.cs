﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.ENGINE.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// Connection to laser engine portion of the main UI.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    using Icon_Interface.General;
    using Icon_Interface.Properties;

    using Tykma.Core.Engine;
    using Tykma.Core.Engine.ConfigSettings;
    using Tykma.Core.Engine.CustomExceptions;
    using Tykma.Core.Engine.PSE;
    using Tykma.Core.Entity;
    using Tykma.Core.SplitMark;
    using Tykma.Core.StringExtensions;
    using Tykma.Icon.AlphanumComparator;
    using Tykma.Icon.AxisInfo;
    using Tykma.Icon.RotaryMark.Logic;

    /// <summary>
    /// MiniLase partial class.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// The cycle timer.
        /// </summary>
        private readonly Stopwatch cycleTimer;

        private bool alarmsignal;

        /// <summary>
        /// The laser engine instance.
        /// </summary>
        private ILaserInterface le;

        /// <summary>
        /// For the connection timer.
        /// </summary>
        private int connectionBusyWaitCounter;

        /// <summary>
        /// Backing field for the split mark job property.
        /// </summary>
        private bool splitMarkJob;

        private Tykma.Icon.DateCode.IDateCode datecodegenerator = new Tykma.Icon.DateCode.TykmaDateCodes();

        /// <summary>
        /// The start fault.
        /// </summary>
        private enum Startfault
        {
            /// <summary>
            /// No job loaded
            /// </summary>
            NoJobLoaded,

            /// <summary>
            /// Marking is busy.
            /// </summary>
            MarkingBusy,

            /// <summary>
            /// Job is unauthorized.
            /// </summary>
            UnAuthorizedJob,

            /// <summary>
            /// It's okay to start.
            /// </summary>
            OkayToStart,

            /// <summary>
            /// The axis is in motion.
            /// </summary>
            AxisInMotion,

            /// <summary>
            /// The axis is not homed.
            /// </summary>
            AxisNotHomed,
        }

        private IEngineConfigurationHandler EngineConfiguration { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [mark stopped].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [mark stopped]; otherwise, <c>false</c>.
        /// </value>
        private bool MarkStopped { get; set; }

        /// <summary>
        /// Gets or sets the state of the job.
        /// </summary>
        /// <value>
        /// The state of the job.
        /// </value>
        private JobStatus.Status JobState { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not there is an array job loaded.
        /// </summary>
        private bool ArrayJobLoaded { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not there is a split mark in progress.
        /// </summary>
        private bool SplitMarkInProgress { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not there is an array mark in progress.
        /// </summary>
        private bool ArrayMarkInProgress { get; set; }

        /// <summary>
        /// Gets or sets a count of the array items marked.
        /// </summary>
        private int ArrayItemsMarked { get; set; }

        /// <summary>
        /// Gets or sets a list of all IDs in the project.
        /// </summary>
        private List<EntityInformation> AllIDs { get; set; }

        /// <summary>
        /// Gets or sets a list of currently marked object.
        /// </summary>
        private string ActivelyMarkedObject { get; set; }

        /// <summary>
        /// Gets or sets a list of all the entities that have been marked during array marking.
        /// </summary>
        private List<EntityInformation> MarkedItemsInArray { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the loaded job is a split mark job.
        /// </summary>
        private bool SplitMarkJob
        {
            get
            {
                return this.splitMarkJob;
            }

            set
            {
                this.splitMarkJob = value;
            }
        }

        /// <summary>
        /// Gets or sets the Z-Height.
        /// </summary>
        private double? ZHeight { get; set; }

        /// <summary>
        /// Initializes the minilase DLL.
        /// </summary>
        private void InitializeEngine()
        {
            // Initialize the marking processor.
            if (TykmaGlobals.INISettings.ENGINETYPE == 0)
            {
                this.le = new MiniLase(this.fileOperations.MainFolder.FullName, TykmaGlobals.INISettings.MOTFENABLED);

                var configPath = Path.Combine(this.fileOperations.MainFolder.FullName, "plug", "markcfg7");

                this.EngineConfiguration = new SEEngineConfigurationHandler(configPath);
            }

            // Subscribe to the mark state events.
            this.le.LaserStatusMessage += this.MarkStateEvent;

            // Subscribe to the card connected event.
            this.le.ConnectionMessage += this.ConnStateEvent;

            this.RotaryMarkLogic = new RotaryMark();

            this.RotaryMarkLogic.ParametersUpdated += async (s, e) => await this.RotaryMarkLogicOnParametersUpdatedAsync(s, e);
        }

        private async Task RotaryMarkLogicOnParametersUpdatedAsync(object sender, bool b)
        {
            if (this.SplitMarkJob)
            {
                await this.ChangeTextByName("%DIAMETER", this.RotaryMarkLogic.Diameter.ToString());
                await this.ChangeTextByName("%SPLIT", this.RotaryMarkLogic.SplitSize.ToString());
                if (this.axisZ.Enabled)
                {
                    await this.ChangeTextByName("%Z", this.RotaryMarkLogic.Z.ToString());

                    this.GetZHeight();
                }
                this.InitDataGridView();
                this.UpdatePreview();
            }
        }

        /// <summary>
        /// Mark state changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void MarkStateEvent(object sender, LaserStatusMessage e)
        {
            this.InvokeIfRequired(() =>
                {
                    LaserStatusMessage markingState = e;

                    this.alarmsignal = false;

                    switch (markingState.Status)
                    {
                        case MarkState.Complete:

                            this.EndOfRun();

                            break;

                        case MarkState.InProgress:

                            this.TimeStampEvent(Resources.Msg_Marking_In_Progress);

                            break;

                        case MarkState.Started:

                            if (!this.SplitMarkInProgress)
                            {
                                this.ShowMarkingInProgressScreen();

                                this.TimeStampEvent(Resources.Msg_Marking_Started);
                            }

                            break;

                        case MarkState.Exception:

                            if (Debugger.IsAttached)
                            {
                                this.EndOfRun();
                                break;
                            }

                            this.LockTheDisplay(false);
                            this.ShowMainScreen();

                            if (this.le.LimitsOn)
                            {
                                this.PreStartLimits(false, false);
                            }

                            this.TimeStampEvent(string.Format("Mark Fault: {0}", markingState.ErrorDescription));
                            Logger.Info("Mark Fault: {0}", markingState.ErrorDescription);
                            this.alarmsignal = true;

                            break;

                        case MarkState.LimitsStarted:

                            this.ux_ControlButton_Limits.ux_Image_Icon.Image = ImageResources.stop_cycle_active;
                            this.TimeStampEvent(Resources.Msg_Limits_Started);
                            Logger.Info(Resources.Msg_Limits_Started);

                            break;

                        case MarkState.LimitsStopped:

                            this.ux_ControlButton_Limits.ux_Image_Icon.Image = ImageResources.limits1;
                            if (!markingState.Fault)
                            {
                                this.TimeStampEvent(Resources.Msg_Limits_Stopped);
                                Logger.Info(Resources.Msg_Limits_Stopped);
                            }
                            else
                            {
                                this.TimeStampEvent(
                                    string.Format("Limits fault: {0}", markingState.ErrorDescription),
                                    true);
                                Logger.Error("Limits fault: {0}", markingState.ErrorDescription);
                            }

                            break;
                    }
                });
        }

        /// <summary>
        /// Connection state has changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void ConnStateEvent(object sender, ConnStateMessage e)
        {
            this.InvokeIfRequired(
                async () =>
                {
                    ConnStateMessage connState = e;
                    if (connState.Status == ConnectionState.Connected)
                    {
                        this.ux_Timer_CardConnection.Stop();
                        this.ux_StatusLabel_Connecting.Visible = false;

                        this.TimeStampEvent(Resources.Msg_System_Ready);
                        Logger.Info(Resources.Msg_System_Ready);

                        this.ioDwordOneShot = this.ReadPort();
                        this.ux_Timer_IO.Start();
                        this.GetListOfProjects();
                        this.ConfigureFileSystemWatcher();
                        this.SetSoftwareReady(false);
                        this.ClearSequence();
                        this.LoadLastMarkingJob();

                        if (TykmaGlobals.INISettings.HOMEONOPEN)
                        {
                            try
                            {
                                this.le.ResetAxis(this.axisR.Enabled, this.axisZ.Enabled, false, false);
                            }
                            catch (EngineNotConnectedException)
                            {
                                Logger.Error("Can't reset axis: {0}", "Engine not connected.");
                            }
                            catch (MethodFaultException err)
                            {
                                Logger.Error("Can't reset axis: {0}", err.Message);
                            }
                            catch (NotImplementedException err)
                            {
                                Logger.Error("Can't reset axis: {0}", err.Message);
                            }
                            catch (NotSupportedException)
                            {
                                Logger.Error("Can't reset axis: {0}", "Method not implemented.");
                            }

                            var axesToHome = new List<AxisSetting>();
                            if (this.axisR.Enabled)
                            {
                                this.axisR.NeedsHoming = true;
                                axesToHome.Add(this.axisR);
                            }

                            if (this.axisZ.Enabled)
                            {
                                this.axisZ.NeedsHoming = true;
                                axesToHome.Add(this.axisZ);
                            }

                            await this.HomeAnAxis(axesToHome);
                        }
                    }
                    else if (connState.Status == ConnectionState.NotConnected)
                    {
                        this.TimeStampEvent(string.Format("{0} {1}.", Resources.Msg_Could_Not_Initialize_Card, connState.Fault));
                        Logger.Info("{0} {1}.", Resources.Msg_Could_Not_Initialize_Card, connState.Fault);
                        this.ux_Timer_CardConnection.Stop();

                        this.ux_StatusLabel_Connecting.Text = Resources.Label_Disconnected;
                        this.ux_StatusLabel_Connecting.ForeColor = Color.DarkRed;
                        this.ux_StatusLabel_Connecting.Visible = true;
                    }
                });
        }

        private bool SetFontHeight(string id, double mm)
        {
            try
            {
                var result = this.le.SetFontHeight(id, mm);

                return result;
            }
            catch (EngineNotConnectedException)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", "Can not set font height", "Engine not connected"), true);
                Logger.Error("{0}: {1}", "Can not set font height", "Engine not connected");
            }
            catch (MethodFaultException err)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", "Can not set font height", err.Message), true);
                Logger.Error("{0}: {1}", "Can not set font height", err.Message);
            }
            catch (NotSupportedException)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", "Can not set font height", "Method not implemented."), true);
                Logger.Error("{0}: {1}", "Can not set font height", "Method not implemented.");
            }

            return false;
        }


        /// <summary>
        /// Sets the software ready.
        /// </summary>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        private async void SetSoftwareReady(bool enable)
        {
            await this.OutputWrite(enable, TykmaGlobals.INISettings.IOSOFTWAREREADY);
            this.ux_ControlButton_Start.ux_Label_Action.Font = new Font(this.ux_ControlButton_Start.ux_Label_Action.Font, enable ? FontStyle.Bold : FontStyle.Regular);
            Logger.Debug(enable ? Resources.Msg_SoftwareReadyON : Resources.Msg_SoftwareReadyOFF);
        }

        /// <summary>
        /// Checks whether we are ready to start a sequence.
        /// </summary>
        /// <returns>Enumerated state.</returns>
        private Startfault CheckStartReady()
        {
            // Do not continue if we're already marking.
            if (this.le.MarkingBusy)
            {
                return Startfault.MarkingBusy;
            }

            if (TykmaGlobals.INISettings.NETENABLESJOB)
            {
                if (this.JobState != JobStatus.Status.Enabled)
                {
                    this.TimeStampEvent("Job not enabled");
                    Logger.Info("Job not enabled");
                    return Startfault.UnAuthorizedJob;
                }
            }

            if (!this.markingJobState.Loaded)
            {
                this.TimeStampEvent(Resources.Msg_Job_Not_Loaded);
                Logger.Info("{0}: {1}", Resources.Msg_Sequence_Cant_Start, Resources.Msg_Job_Not_Loaded);
                return Startfault.NoJobLoaded;
            }

            if (this.axisR.Homing)
            {
                this.TimeStampEvent(string.Format("{0} {1}", this.axisR.CommonName, Resources.Msg_Axis_Is_In_Motion), true);
                Logger.Info("{0}: {1} {2}", Resources.Msg_Sequence_Cant_Start, Resources.Msg_Axis_Is_In_Motion, this.axisR.CommonName);
                return Startfault.AxisInMotion;
            }

            if (this.axisZ.Homing)
            {
                this.TimeStampEvent(string.Format("{0} {1}", this.axisZ.CommonName, Resources.Msg_Axis_Is_In_Motion), true);
                Logger.Info("{0}: {1} {2}", Resources.Msg_Sequence_Cant_Start, Resources.Msg_Axis_Is_In_Motion, this.axisZ.CommonName);
                return Startfault.AxisInMotion;
            }

            if (this.axisR.Enabled)
            {
                if (this.axisR.NeedsHoming)
                {
                    this.TimeStampEvent(string.Format("{0} {1}", this.axisR.CommonName, Resources.Msg_Axis_Not_Homed), true);
                    Logger.Info("{0}: {1} {2}", Resources.Msg_Sequence_Cant_Start, this.axisR.CommonName, Resources.Msg_Axis_Not_Homed);
                    return Startfault.AxisNotHomed;
                }
            }

            if (this.axisZ.Enabled)
            {
                if (this.axisZ.NeedsHoming)
                {
                    this.TimeStampEvent(
                        string.Format("{0} {1}", this.axisZ.CommonName, Resources.Msg_Axis_Not_Homed),
                        true);
                    Logger.Info(
                        "{0}: {1} {2}",
                        Resources.Msg_Sequence_Cant_Start,
                        this.axisZ.CommonName,
                        Resources.Msg_Axis_Not_Homed);
                    return Startfault.AxisNotHomed;
                }
            }

            // If we're in data search mode and a command file was not loaded, do not continue.
            if (TykmaGlobals.INISettings.DATASEARCHENABLED)
            {
                if (!this.markingJobState.Authorized)
                {
                    this.TimeStampEvent(Resources.Msg_Command_File_Not_Loaded);
                    Logger.Info("{0}: {1}", Resources.Msg_Sequence_Cant_Start, Resources.Msg_Command_File_Not_Loaded);
                    return Startfault.UnAuthorizedJob;
                }
            }

            return Startfault.OkayToStart;
        }

        /// <summary>
        /// Starts the sequence.
        /// </summary>
        private async void StartSequence()
        {
            if (TykmaGlobals.INISettings.USEQUEUE)
            {
                this.LoadNextQueueJob();
            }

            Startfault startCheck = this.CheckStartReady();

            if (startCheck != Startfault.OkayToStart)
            {
                if (startCheck == Startfault.AxisNotHomed)
                {
                    bool homingSuccess = await this.HomeRequiredAxes();
                    if (!homingSuccess)
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
            }

            this.SetCustomDateCode();

            // If the limits are on, abort unless we're in always on limits mode.
            if (TykmaGlobals.INISettings.ALWAYSONLIMITS)
            {
                this.PreStartLimits(false, true);
            }
            else
            {
                if (this.le.LimitsOn)
                {
                    this.PreStartLimits(false, true);
                    this.TimeStampEvent(Resources.Msg_Limits_Turned_Off);
                    Logger.Info(Resources.Msg_Limits_Turned_Off);
                }
            }

            // Wait for limits to truly turn off before starting a cycle.
            while (this.le.LimitsOn)
            {
                await Task.Delay(50);
            }

            if (this.SplitMarkJob)
            {
                this.ShowMarkingInProgressScreen();
                await this.StartMarkRotary();
                this.EndOfRun();
            }
            else if (this.ArrayJobLoaded)
            {
                this.TimeStampEvent("Array Detected.");
                this.ShowMarkingInProgressScreen();
                await this.StartMarkArray();
                this.EndOfRun();
            }
            else
            {
                await this.StartMark();
            }
        }

        /// <summary>
        /// Shows the marking in progress screen.
        /// </summary>
        private void ShowMarkingInProgressScreen()
        {
            if (this.ArrayMarkInProgress)
            {
                this.ux_Label_MarkingObject.Text = this.ActivelyMarkedObject;
                return;
            }
            else
            {
                this.ux_Label_MarkingObject.Text = string.Empty;
            }

            this.LockTheDisplay(true);

            this.cycleTimer.Reset();
            this.cycleTimer.Start();
            this.MarkStopped = false;
        }

        /// <summary>
        /// Start marking a rotary project.
        /// </summary>
        /// <returns>Success state.</returns>
        private async Task<bool> StartMarkRotary()
        {
            this.SplitMarkInProgress = true;
            this.MarkStopped = false;
            var ents = new List<EntityInformation>();
            var allents = new List<EntityInformation>();

            foreach (DataGridViewRow item in this.ux_DataGridView.Rows)
            {
                if (!item.Cells[1].ReadOnly)
                {
                    allents.Add(new EntityInformation(item.Cells[0].Value.ToString(), item.Cells[1].Value.ToString()));
                }
            }

            foreach (var obj in this.GetListOfAllEntities())
            {
                if (!obj.StartsWith("%"))
                {
                    var sz = this.le.GetObjectSize(obj);
                    ents.Add(sz);
                }
            }

            if (this.axisR.Enabled && this.axisR.NeedsHoming)
            {
                this.TimeStampEvent("Rotary requires homing");
                bool homingSuccess = await this.HomeAnAxis(this.axisR);
                if (!homingSuccess)
                {
                    return false;
                }
            }

            if (this.axisZ.Enabled && this.axisZ.NeedsHoming)
            {
                this.TimeStampEvent("Z requires homing");
                bool homingSuccess = await this.HomeAnAxis(this.axisZ);
                if (!homingSuccess)
                {
                    return false;
                }
            }

            var splitObjects = new Tykma.Core.SplitMark.SplitMarkLogic().GetSplitObjects(this.RotaryMarkLogic.SplitSize, this.RotaryMarkLogic.Diameter, ents);

            var boxes = new List<SplitBox>();

            foreach (var item in splitObjects)
            {
                var sb = default(SplitBox);
                double adder = 0;
                sb.Build(item.SplitBox.X1 - adder, item.SplitBox.Y1, item.SplitBox.X2 + adder, item.SplitBox.Y2);
                boxes.Add(sb);
            }

            this.le.SplitIntoBoxes(boxes);

            var allboxEnts = this.GetListOfAllEntities().Where(x => x.StartsWith("BOX"));

            var ids = allboxEnts.Count();

            // We'll need to move all the boxes into position.
            foreach (var id in allboxEnts)
            {
                var size = this.le.GetObjectSize(id);

                // How far to 0?
                var centerX = (size.MaxX + size.MinX) / 2;

                var pos = 0 - centerX;
                this.le.MoveObject(id, pos, 0);
            }

            //this.le.SaveFile(new FileInfo(@"C:\temp\bn.pse"));

            if (this.ZHeight != null)
            {
                try
                {
                    await this.MoveZIntoPosition(this.ZHeight.Value);
                }
                catch (Exception e)
                {
                    this.TimeStampEvent("Axis Move: " + e.Message);
                    this.MarkStopped = true;
                    this.TimeStampEvent("Mark fault");
                    this.axisZ.NeedsHoming = true;
                }
            }

            int casev = 1;
            if (this.ActiveBlueNileObject != null)
            {
                if (this.ActiveBlueNileObject.IsFontInside)
                {
                    this.TimeStampEvent("Marking inside diameter");
                    Logger.Info("Marking inside diameter");
                    casev = -1;
                }
                else
                {
                    this.TimeStampEvent("Marking outside diameter");
                    Logger.Info("Marking outside diameter");
                }
            }

            if (!this.MarkStopped)
            {
                for (int i = 0; i < ids; i++)
                {
                    if (splitObjects.Count >= i)
                    {
                        if (!this.MarkStopped)
                        {
                            var mult = this.EngineConfiguration.GetPulsesPerUnitRotary(this.axisR.Number);

                            if (splitObjects[i].Degree > 0)
                            {
                                this.TimeStampEvent($"Moving to degree {splitObjects[i].Degree * casev:#.##}");
                            }
                            else
                            {
                                this.TimeStampEvent("Moving to degree 0");
                            }

                            bool continueMarking = false;
                            try
                            {
                                continueMarking = this.le.AxisMoveToPosition(this.axisR.Number, splitObjects[i].Degree * casev);
                            }
                            catch (Exception e)
                            {
                                this.TimeStampEvent("Axis Move: " + e.Message);
                                continueMarking = false;
                            }

                            if (continueMarking)
                            {
                                continueMarking = await this.StartMarkObject($"BOX{i + 1}");
                            }

                            if (!continueMarking)
                            {
                                this.MarkStopped = true;
                                this.TimeStampEvent("Mark fault");
                                this.EndOfRun();
                            }
                        }
                    }
                }
            }
            else
            {
                this.EndOfRun();
            }

            await this.LoadJobAbstractor(this.markingJobState.Name, true);

            foreach (var item in allents)
            {
                await this.ChangeTextByName(item.Name, item.Value);
            }

            return true;
        }

        /// <summary>
        /// Start marking an object.
        /// </summary>
        /// <param name="objectToMark">The object name ot mark.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        private async Task<bool> StartMarkObject(string objectToMark)
        {
            try
            {
                return await this.le.StartMarkObject(objectToMark);
            }
            catch (Exception e)
            {
                this.TimeStampEvent(e.Message);
                Logger.Error(e.Message);
                return false;
            }
        }

        private async Task<bool> StartMarkArray()
        {
            this.ArrayItemsMarked = 0;
            this.ArrayMarkInProgress = true;
            this.MarkStopped = false;

            this.MarkedItemsInArray?.Clear();
            this.MarkedItemsInArray = this.ArrayHelperClass.EntsToMark(this.AllIDs, this.ux_Control_Quantity.Value);

            foreach (var markitem in this.MarkedItemsInArray)
            {
                if (!this.MarkStopped)
                {
                    this.ActivelyMarkedObject = markitem.Name;
                    this.TimeStampEvent($"Marking Object: {markitem.Name}");
                    await this.StartMarkObject(markitem.Name);
                }
                else
                {
                    this.ArrayMarkInProgress = false;
                    this.EndOfRun();
                }
            }

            this.ArrayItemsMarked = this.ArrayHelperClass.ArrayMax(this.MarkedItemsInArray);
            this.ArrayMarkInProgress = false;

            return true;
        }

        /// <summary>
        /// Starts the mark.
        /// </summary>
        /// <returns>Success state.</returns>
        private async Task<bool> StartMark()
        {
            try
            {
                return await this.le.StartMark();
            }
            catch (EngineNotConnectedException)
            {
                this.TimeStampEvent("Can't start cycle: Engine not connected.");
                Logger.Error("Can't start cycle: Engine not connected.");
                this.ConnStateEvent(this, new ConnStateMessage { Status = ConnectionState.NotConnected });
                return false;
            }
        }

        /// <summary>
        /// The cycle has ended.
        /// </summary>
        private async void EndOfRun()
        {
            this.JobState = this.MarkStopped ? JobStatus.Status.Stopped : JobStatus.Status.Marked;

            if (!this.MarkStopped && this.SplitMarkInProgress)
            {
                return;
            }

            if (!this.MarkStopped && this.ArrayMarkInProgress)
            {
                return;
            }

            if (TykmaGlobals.INISettings.IOSETCOMPLETE)
            {
                await this.SetCycleComplete();
            }

            this.cycleTimer.Stop();
            this.LockTheDisplay(false);
            this.BumpAxisHomeCounters();

            if (!this.MarkStopped)
            {
                var ids = this.GetListOfIDs();
                if (this.ArrayJobLoaded)
                {
                    this.SetMarkArraySerializationPostMark(ids);
                }

                this.IncrementConsumedCounters(ids);
            }

            this.SetCombinationIDs();

            this.TimeStampEvent(string.Format(
                                "{0} ({1}: {2})",
                                Resources.Msg_Marking_Completed,
                                Resources.Msg_Mark_Time,
                                this.cycleTimer.Elapsed.ToString(@"hh\:mm\:ss\.f")));
            Logger.Info("{0} {1}", Resources.Msg_Mark_Time, this.cycleTimer.Elapsed.ToString(@"hh\:mm\:ss\.f"));

            if (TykmaGlobals.INISettings.USEQUEUE)
            {
                if (!this.MarkStopped)
                {
                    this.RemoveMarkedQueueJob();
                    this.ClearSequence();
                }
            }

            var listOfAxesToHome = new List<AxisSetting>();

            if (this.MarkStopped)
            {
                this.axisZ.NeedsHoming = true;
            }

            if (this.axisZ.NeedsHoming && this.axisZ.Enabled)
            {
                listOfAxesToHome.Add(this.axisZ);
            }

            if (this.axisR.NeedsHoming && this.axisR.Enabled)
            {
                listOfAxesToHome.Add(this.axisR);
            }

            if (!this.MarkStopped)
            {
                await this.HomeAnAxis(listOfAxesToHome);
            }

            this.ShowMainScreen();

            if (TykmaGlobals.INISettings.USECOUNTER)
            {
                // If the quantity is larger than 1, we'll subtract, otherwise we will clear the sequence.
                if (this.ArrayItemsMarked > 0)
                {
                    if (!this.MarkStopped)
                    {
                        if (this.ArrayItemsMarked >= this.ux_Control_Quantity.Value && !TykmaGlobals.INISettings.COUNTERNODECREMENT)
                        {
                            this.ClearSequence();
                        }
                        else
                        {
                            if (!TykmaGlobals.INISettings.COUNTERNODECREMENT)
                            {
                                this.ux_Control_Quantity.Value -= this.ArrayItemsMarked;
                            }

                            this.SetMarkArraySerializationPreMark();
                        }
                    }
                }
                else
                {
                    if (this.ux_Control_Quantity.Value > 1)
                    {
                        if (!this.MarkStopped)
                        {
                            if (!TykmaGlobals.INISettings.COUNTERNODECREMENT)
                            {
                                this.ux_Control_Quantity.Value -= 1;
                            }

                            if (TykmaGlobals.INISettings.ALWAYSONLIMITS && this.markingJobState.Loaded)
                            {
                                this.PreStartLimits(true, true);
                            }
                        }

                        return;
                    }

                    if (TykmaGlobals.INISettings.TAGFEEDER)
                    {
                        await this.SetCycleComplete();
                    }

                    Logger.Info(Resources.Msg_Entire_Quantity_Marked);
                    this.ClearSequence();
                }
            }

            if (TykmaGlobals.INISettings.DATASEARCHENABLED)
            {
                if (TykmaGlobals.INISettings.CLEARAFTERFINISH)
                {
                    this.ClearSequence();
                    this.markingJobState.Authorized = false;
                }
                else
                {
                    if (TykmaGlobals.INISettings.CLEARDATAAFTERFINISH)
                    {
                        foreach (EntityInformation entity in this.GetListOfIDs())
                        {
                            EntityInformation entity1 = entity;
                            await this.ChangeTextByName(entity1.Name, string.Empty);
                        }
                    }

                    this.UpdatePreview();

                    this.markingJobState.Authorized = true;
                    this.cmdFile.Search(true);
                }

                if (!this.MarkStopped)
                {
                    if (!this.cmdFile.MoveLastFile())
                    {
                        if (TykmaGlobals.INISettings.CLEARAFTERFINISH)
                        {
                            this.TimeStampEvent(Resources.Msg_Could_Not_Move_Command_File);
                            Logger.Fatal(Resources.Msg_Could_Not_Move_Command_File);
                        }
                    }
                }
            }

            if (TykmaGlobals.INISettings.SAVEPROJECTAFTERRUN)
            {
                if (this.le.Engine == EngineTypes.EngineType.PSE)
                {
                    string fileWithExtension = string.Format("{0}{01}", this.markingJobState.Name, this.le.FileExtension);
                    var file = new FileInfo(Path.Combine(this.fileOperations.ProjectFolder.FullName, fileWithExtension));

                    this.TimeStampEvent(this.SaveFile(file) ? "Saved project file" : "Could not save project file.");
                }
            }

            if (TykmaGlobals.INISettings.ALWAYSONLIMITS && this.markingJobState.Loaded)
            {
                this.PreStartLimits(true, true);
            }

            if (TykmaGlobals.INISettings.CLEARSEQUENCEEND)
            {
                this.ClearSequence();
            }
            else
            {
                var width = this.ux_nud_FontWidth.Value;
                var height = this.ux_nud_fontHeight.Value;// / TykmaGlobals.INISettings.FONTSIZEMULTIPLIER;

                var d = this.RotaryMarkLogic.Diameter;
                var s = this.RotaryMarkLogic.SplitSize;
                await this.HandleBlueNileActiveObject();
                this.RotaryMarkLogic.Set(d.ToString(), s.ToString(), 0, this.RotaryMarkLogic.IsVisible);


                if (TykmaGlobals.INISettings.HEIGHTKEEPASPECT)
                {
                    this.ux_nud_fontHeight.Value = height;
                }
                else
                {
                    this.ux_nud_fontHeight.Value = height;
                    this.ux_nud_FontWidth.Value = width;
                }
            }

            if (TykmaGlobals.INISettings.DATACLEARONFINISH)
            {
                if (TykmaGlobals.INISettings.DATASEARCHENABLED || TykmaGlobals.INISettings.USECOUNTER
                    || TykmaGlobals.INISettings.USEQUEUE)
                {
                }
                else
                {
                    this.TimeStampEvent("Clearing all data");
                    foreach (var ent in this.GetListOfIDs())
                    {
                        await this.ChangeTextByName(ent.Name, string.Empty);
                    }

                    this.InitDataGridView();
                    this.UpdatePreview();
                }
            }
        }

        /// <summary>
        /// Stops the sequence.
        /// </summary>
        /// <param name="exception">if set to <c>true</c> [exception].</param>
        private void StopSequence(bool exception = false)
        {
            if (this.alarmsignal || this.le.MarkingBusy || this.axisZ.Homing || this.axisR.Homing || exception)
            {
                if (this.axisZ.Enabled)
                {
                    this.axisZ.NeedsHoming = true;
                    this.TimeStampEvent(string.Format("{0} - {1} {2}", Resources.Msg_Stopped_During_Cycle, this.axisZ.CommonName, Resources.Msg_Axis_Needs_To_Be_Homed));
                    Logger.Info("{0} - {1} {2}", Resources.Msg_Stopped_During_Cycle, this.axisZ.CommonName, Resources.Msg_Axis_Needs_To_Be_Homed);
                }

                if (this.axisR.Enabled)
                {
                    this.axisR.NeedsHoming = true;
                    this.TimeStampEvent(string.Format("{0} - {1} {2}", Resources.Msg_Stopped_During_Cycle, this.axisR.CommonName, Resources.Msg_Axis_Needs_To_Be_Homed));
                    Logger.Info("{0} - {1} {2}", Resources.Msg_Stopped_During_Cycle, this.axisR.CommonName, Resources.Msg_Axis_Needs_To_Be_Homed);
                }
            }

            this.alarmsignal = false;
            this.MarkStopped = true;
            this.JobState = JobStatus.Status.Stopped;

            try
            {
                this.le.StopMark();
                this.TimeStampEvent("Received cycle stop.");
                Logger.Info("Received cycle stop.");
            }
            catch (MethodFaultException e)
            {
                this.TimeStampEvent(string.Format("Stop fault {0}", e.Message));
                Logger.Error("Stop fault {0}", e.Message);
            }
            catch (EngineNotConnectedException)
            {
                this.TimeStampEvent("Can't stop cycle: Engine not connected.");
                Logger.Error("Can't stop cycle: Engine not connected.");
            }
            catch (AggregateException e)
            {
                var firstOrDefault = e.InnerExceptions.FirstOrDefault();
                this.TimeStampEvent(string.Format("Stop fault {0}", firstOrDefault.Message));
                Logger.Error("Stop fault {0}", firstOrDefault.Message);
            }

            this.LockTheDisplay(false);
        }

        /// <summary>
        /// Pre-starts limits.
        /// </summary>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        /// <param name="contourMark">if set to <c>true</c> [contour mark].</param>
        private void PreStartLimits(bool enable, bool contourMark)
        {
            if (!this.markingJobState.Loaded && enable)
            {
                this.TimeStampEvent(Resources.Msg_Job_Not_Loaded);
                Logger.Info("{0} - {1}", Resources.Main_Limits, Resources.Msg_Job_Not_Loaded);
                return;
            }

            if (enable)
            {
                this.TimeStampEvent(Resources.Msg_Starting_Limits);
                Logger.Info(Resources.Msg_Starting_Limits);

                if (this.GetListOfAllEntities().Contains("%TRACE"))
                {
                    this.StartLimitsEntity(true, "%TRACE");
                }
                else
                {
                    this.StartLimits(true, contourMark);
                }
            }
            else
            {
                this.StartLimits(false, contourMark);
                if (this.le.LimitsOn)
                {
                    this.TimeStampEvent(Resources.Msg_Stopping_Limits);
                    Logger.Info(Resources.Msg_Stopping_Limits);
                }
            }
        }

        /// <summary>
        /// Starts the limits.
        /// </summary>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        /// <param name="contourMark">if set to <c>true</c> [contour mark].</param>
        private void StartLimits(bool enable, bool contourMark)
        {
            try
            {
                this.le.StartLimits(enable, contourMark);
            }
            catch (NotImplementedException)
            {
                Logger.Error("Limits not implemented");
            }
            catch (EngineNotConnectedException)
            {
                Logger.Error("Limits - engine not connected.");
            }
        }

        private void StartLimitsEntity(bool enable, string entName)
        {
            try
            {
                this.le.StartLimits(enable, false, entName);
            }
            catch (NotImplementedException)
            {
                Logger.Error("Limits not implemented");
            }
            catch (EngineNotConnectedException)
            {
                Logger.Error("Limits - engine not connected.");
            }
        }

        /// <summary>
        /// Connects to the card.
        /// </summary>
        /// <param name="startTimer">if set to <c>true</c> [start timer].</param>
        private async void ConnectToCard(bool startTimer = false)
        {
            // We will start a timer that will show animation/progress and call a connection event.
            if (startTimer)
            {
                this.ux_Timer_CardConnection.Start();
            }

            // This will return an event with the connection state.
            try
            {
                await this.le.Connect();
            }
            catch (NotImplementedException)
            {
                var e = new ConnStateMessage { Status = ConnectionState.NotConnected };
                this.ConnStateEvent(this, e);
                this.TimeStampEvent("Engine library fault");
                Logger.Error("Engine does not support connect.");
            }
            catch (EngineNotConnectedException)
            {
            }
        }

        /// <summary>
        /// Clears the project.
        /// </summary>
        /// <returns>Success state.</returns>
        private async Task<bool> ClearProject()
        {
            var result = await this.le.ClearProject();
            return result;
        }

        /// <summary>
        /// Loads the marking job.
        /// </summary>
        private async void LoadLastMarkingJob()
        {
            if (TykmaGlobals.INISettings.ENABLELASTJOBLOAD && !TykmaGlobals.INISettings.DATAJOBLOADMODE)
            {
                if (await this.LoadJobAbstractor(TykmaGlobals.INISettings.JOBTOLOAD))
                {
                    Logger.Info("{0}: {1}", Resources.Msg_Loaded_Job, TykmaGlobals.INISettings.JOBTOLOAD);
                }
                else
                {
                    Logger.Fatal(Resources.Msg_Could_Not_Load_Job);
                }
            }
        }

        /// <summary>
        /// Saves the file.
        /// </summary>
        /// <param name="whichFile">Which file.</param>
        /// <returns>Boolean indicating whether or not file was saved.</returns>
        private bool SaveFile(FileInfo whichFile)
        {
            try
            {
                this.le.SaveFile(whichFile);
            }
            catch (NotImplementedException)
            {
                this.TimeStampEvent("Can't save file: Method not implemented.");
                Logger.Error("Can't save file: Method not implemented.");
                return false;
            }
            catch (EngineNotConnectedException)
            {
                this.TimeStampEvent("Can't save file: Engine not connected.");
                Logger.Error("Can't save file: Engine not connected.");
                return false;
            }
            catch (MethodFaultException e)
            {
                this.TimeStampEvent(string.Format("Can't save file: {0}", e.Message));
                Logger.Error("Can't save file: {0}", e.Message);
                return false;
            }
            catch (Exception e)
            {
                this.TimeStampEvent(string.Format("Can't save file: {0}", e.Message));
                Logger.Error("Can't save file: {0}", e.Message);
            }

            return true;
        }

        /// <summary>
        /// Loads the job abstracter.
        /// </summary>
        /// <param name="whichJob">The which job.</param>
        /// <param name="loadingInternally">if set to <c>true</c> [loading internally].</param>
        /// <returns>
        /// Success state.
        /// </returns>
        private async Task<bool> LoadJobAbstractor(string whichJob, bool loadingInternally = false, bool ignorerotary = false)
        {
            var sb = new StringBuilder();

            if (this.le.Engine == EngineTypes.EngineType.PSE)
            {
                string jobWithExtension = whichJob + this.le.FileExtension;

                if (jobWithExtension.FilePathHasInvalidChars())
                {
                    this.TimeStampEvent(Resources.Msg_Job_Invalid_Chars, true);
                    Logger.Trace(Resources.Msg_Job_Invalid_Chars);
                    return false;
                }
                else
                {
                    sb.Append(Path.Combine(this.fileOperations.ProjectFolder.FullName, jobWithExtension));
                }
            }

            if (await this.LoadJob(sb.ToString(), loadingInternally, ignorerotary))
            {
                this.JobState = JobStatus.Status.Loaded;
                return true;
            }

            this.JobState = JobStatus.Status.NotLoaded;
            return false;
        }

        /// <summary>
        /// Loads the job.
        /// </summary>
        /// <param name="whichJob">Which job.</param>
        /// <param name="loadingInternally">if set to <c>true</c> [loading internally].</param>
        /// <returns>
        /// Success status.
        /// </returns>
        private async Task<bool> LoadJob(string whichJob, bool loadingInternally = false, bool ignorerotary = false)
        {
            this.ux_Status_InnerOrOuterRing.Visible = false;

            this.ZHeight = null;

            if (!loadingInternally)
            {
                if ((TykmaGlobals.INISettings.DATASEARCHENABLED && TykmaGlobals.INISettings.DATAJOBLOADMODE) || !TykmaGlobals.INISettings.OPERATORCANLOADJOB)
                {
                    this.TimeStampEvent(Resources.Msg_Import_Mode_Job_Load_Disabled, true);
                    Logger.Trace(Resources.Msg_Import_Mode_Job_Load_Disabled);
                    return false;
                }
            }

            if (whichJob.FilePathHasInvalidChars())
            {
                this.TimeStampEvent(Resources.Msg_Job_Invalid_Chars, true);
                Logger.Trace(Resources.Msg_Job_Invalid_Chars);
                return false;
            }

            bool loadSuccess = false;
            bool okaytoLoad = true;

            // Get the filename without extension.
            string fileName = Path.GetFileNameWithoutExtension(whichJob);

            if (this.le.Engine == EngineTypes.EngineType.PSE)
            {
                if (!File.Exists(whichJob))
                {
                    okaytoLoad = false;
                }
            }

            // Try to load the job if it exists.
            if (okaytoLoad)
            {
                try
                {
                    loadSuccess = await this.le.LoadDocument(whichJob);
                }
                catch (EngineNotConnectedException)
                {
                    this.TimeStampEvent("Can't load job: Engine not connected.");
                    Logger.Error("Can't load job: Engine not connected.");
                    this.ux_Timer_ReconnectToEngine.Start();
                }
                catch (MethodFaultException e)
                {
                    this.TimeStampEvent(string.Format("Can't load job: {0}", e.Message));
                    Logger.Error("Can't load job: {0}", e.Message);
                }
            }
            else
            {
                this.TimeStampEvent(string.Format("{0}: {1}", Resources.Msg_Job_Not_Found, fileName), true);
                Logger.Info("{0}: {1}", Resources.Msg_Job_Not_Found, fileName);
                return false;
            }

            // Continue if the job is loaded, otherwise clear sequence.
            if (loadSuccess)
            {
                this.markingJobState.Name = fileName;
                this.TimeStampEvent(Resources.Msg_Loaded_Job + ": " + fileName);
                Logger.Info("{0}: {1}", Resources.Msg_Loaded_Job, fileName);
                this.markingJobState.Loaded = true;
            }
            else
            {
                // We will need to change the color on every label other than this one.
                foreach (Label projLabel in this.ux_JobList.Controls)
                {
                    projLabel.Image = new Bitmap(ImageResources.button_blue);
                }

                this.markingJobState.Loaded = false;
            }

            // If the job was loaded succesfully, initialize the datagridview and start/stop command file searcher.
            if (this.markingJobState.Loaded)
            {
                this.AllIDs = this.GetAllEnts().ToList();

                this.ArrayJobLoaded = this.ArrayHelperClass.IsArray(this.AllIDs);

                if (this.ArrayJobLoaded)
                {
                    // this.TimeStampEvent("Array Job Detected");
                }

                if (TykmaGlobals.INISettings.USECOUNTER)
                {
                    if (this.ArrayJobLoaded)
                    {
                        this.ux_Control_Quantity.Value = this.ArrayHelperClass.ArrayMax(this.AllIDs);
                    }
                    else
                    {
                        this.ux_Control_Quantity.Value = this.ux_Control_Quantity.Minimum;
                    }
                }

                var ids = this.GetListOfIDs();

                this.SetCustomDateCode();
                this.SetAllGlobalVariables(ids);

                this.InitDataGridView();

                this.SetCombinationIDs();

                this.HandleBoxFilling();

                this.GetZHeight(ids);

                this.ShowInstructionsForm(fileName);

                // We'll start the file searcher if it's enabled..
                if (TykmaGlobals.INISettings.DATASEARCHENABLED)
                {
                    if (!TykmaGlobals.INISettings.DATAJOBLOADMODE)
                    {
                        this.cmdFile.Search(true);
                        this.TimeStampEvent(
                            string.Format("{0}: {1}", Resources.Msg_File_Search_Started_In, TykmaGlobals.INISettings.DATASEARCHFOLDER));
                        Logger.Debug("{0}: {1}", Resources.Msg_File_Search_Started_In, TykmaGlobals.INISettings.DATASEARCHFOLDER);
                    }
                }

                // We will need to change the color on every label other than this one.
                foreach (Label projLabel in this.ux_JobList.Controls)
                {
                    if (fileName != null && projLabel.Text.ToLower() != fileName.ToLower())
                    {
                        projLabel.Image = new Bitmap(ImageResources.button_blue);
                    }
                    else
                    {
                        projLabel.Image = new Bitmap(ImageResources.button_glow);
                    }

                    for (int i = 0; i < this.jobList.ListCount; i++)
                    {
                        if (this.jobList.GetList(i).Contains(whichJob))
                        {
                            if (this.selectedJobListIndex != i)
                            {
                                this.selectedJobListIndex = i;
                                this.PopulateJobList();
                            }

                            break;
                        }
                    }
                }
            }

            // See if we will need to force the axes to re-home.
            if (TykmaGlobals.INISettings.ZAXISHOMEONNEWJOB)
            {
                this.axisR.NeedsHoming = true;
                this.axisZ.NeedsHoming = true;
            }

            this.UpdatePreview();
            this.UpdateEditButtonVisibility();
            this.ShowHideClearButton();

            // Turn on the limits if they're enabled.
            if (TykmaGlobals.INISettings.ALWAYSONLIMITS)
            {
                this.PreStartLimits(true, true);
            }

            if (!TykmaGlobals.INISettings.DATASEARCHENABLED)
            {
                this.SetSoftwareReady(this.markingJobState.Loaded);
            }

            if (TykmaGlobals.INISettings.ENABLELASTJOBLOAD && !TykmaGlobals.INISettings.DATAJOBLOADMODE)
            {
                TykmaGlobals.INISettings.JOBTOLOAD = fileName;
            }

            if (!ignorerotary)
            {
                if (!loadingInternally)
                {
                    this.InitializeRotaryMark(this.GetListOfIDs());
                }
                else
                {
                    await this.RotaryMarkLogicOnParametersUpdatedAsync(new object(), true);
                }
            }

            this.SplitMarkInProgress = false;

            return this.markingJobState.Loaded;
        }

        /// <summary>
        /// Gets a list of all the entities in a project.
        /// </summary>
        /// <returns>List of all entities.</returns>
        private IEnumerable<string> GetListOfAllEntities()
        {
            if (this.le != null)
            {
                try
                {
                    return this.le.GetListOfObjects();
                }
                catch (EngineNotConnectedException)
                {
                    Logger.Error("Can't get list of IDs: engine not connected");
                }
            }

            return new List<string>();
        }

        /// <summary>
        /// Gets a list of all the entities in the loaded project.
        /// </summary>
        /// <returns>List of all entities.</returns>
        private IEnumerable<EntityInformation> GetAllEnts()
        {
            var rlist = new List<EntityInformation>();
            if (this.le != null)
            {
                try
                {
                    var ids = this.le.GetListOfObjects();

                    foreach (var item in ids)
                    {
                        var ent = new EntityInformation();
                        ent.Name = item;

                        rlist.Add(ent);
                    }
                }
                catch (EngineNotConnectedException)
                {
                    Logger.Error("Can't get list of IDs: engine not connected");
                }
            }

            return rlist;
        }

        /// <summary>
        /// Gets a list of all ids.
        /// </summary>
        /// <returns>A dictionary of IDs.</returns>
        private IEnumerable<EntityInformation> GetListOfIDs()
        {
            // List which will hold all found IDs.
            var listOfEntities = new List<EntityInformation>();

            foreach (string item in this.le.GetListOfObjects())
            {
                var newEntity = new EntityInformation { Name = item };

                try
                {
                    if (item.StartsWith("BOX"))
                    {
                    }
                    else
                    {
                        newEntity.Value = this.le.GetObjectDataByName(item);

                        newEntity.IsTextObject = true;
                    }
                }
                catch (EngineNotConnectedException)
                {
                    this.TimeStampEvent("Could not get IDs - engine not connected");
                    Logger.Error("Could not get IDs - engine not connected");
                }
                catch (MethodFaultException e)
                {
                    newEntity.IsTextObject = false;
                    if (!e.Message.Contains("not a text object"))
                    {
                        this.TimeStampEvent(string.Format("Problem getting ID: \"{0}\" - {1}", item, e.Message));
                        Logger.Error("Problem getting ID: \"{0}\" - {1}", item, e.Message);
                    }
                }

                if (listOfEntities.All(entity => entity.Name != newEntity.Name))
                {
                    listOfEntities.Add(newEntity);
                }
            }

            var comp = new AlphanumComparator();

            listOfEntities.Sort((lhs, rhs) => comp.Compare(lhs.Name, rhs.Name));

            return listOfEntities;
        }

        /// <summary>
        /// Updates the preview.
        /// </summary>
        private void UpdatePreview()
        {
            // ReSharper disable ConvertIfStatementToConditionalTernaryExpression
            if (this.markingJobState.Loaded)
            {
                try
                {
                    Bitmap bmp;

                    // If the window is minimized it will return a height of '1'.
                    if (this.WindowState == FormWindowState.Minimized)
                    {
                        bmp = this.le.GetPreviewBitmap(this.ux_PreviewPic.Image.Width, this.ux_PreviewPic.Image.Height);
                    }
                    else
                    {
                        bmp = this.le.GetPreviewBitmap(this.ux_PreviewPic.Width, this.ux_PreviewPic.Height);
                    }

                    if (this.ux_PreviewPic.Image != null)
                    {
                        this.ux_PreviewPic.Image.Dispose();
                    }

                    if (TykmaGlobals.INISettings.PREVIEWANGLE > 0)
                    {
                        bmp = bmp.RotateImage(TykmaGlobals.INISettings.PREVIEWANGLE, true, false, Color.White);
                    }

                    this.ux_PreviewPic.Image = bmp;
                }
                catch (EngineNotConnectedException)
                {
                    Logger.Error("Get preview fault - Engine not connected");
                }
            }
            else
            {
                this.ux_PreviewPic.Image = new Bitmap(this.ux_PreviewPic.Width, this.ux_PreviewPic.Height);
            }

            // ReSharper restore ConvertIfStatementToConditionalTernaryExpression
        }

        /// <summary>
        /// Timer which will indicate that the software is trying to connect to a card.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void TimerConnectionToCardTick(object sender, EventArgs e)
        {
            string spinIndicator;

            switch (this.connectionBusyWaitCounter)
            {
                case 1:
                    spinIndicator = " ◐";
                    break;
                case 2:
                    spinIndicator = " ◓";
                    break;
                case 3:
                    spinIndicator = " ◑";
                    break;
                default:
                    spinIndicator = " ◒";
                    this.connectionBusyWaitCounter = 0;
                    break;
            }

            this.connectionBusyWaitCounter += 1;

            this.ux_StatusLabel_Connecting.ForeColor = Color.ForestGreen;
            this.ux_StatusLabel_Connecting.Visible = true;

            this.ux_StatusLabel_Connecting.Text = Resources.Main_TimerConnectionToCard_Tick_Connecting + spinIndicator;
        }

        /// <summary>
        /// Handles the Tick event of the Timer_ReconnectToEngine control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void TimerReconnectToEngineTick(object sender, EventArgs e)
        {
            if (this.le.Connected || this.le.Connecting)
            {
                this.ux_Timer_ReconnectToEngine.Stop();
            }
            else
            {
                this.ux_Timer_ReconnectToEngine.Stop();
                this.ConnectToCard();
            }
        }

        /// <summary>
        /// Moves the object.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="moveX">The move x.</param>
        /// <param name="moveY">The move y.</param>
        private void MoveObject(string text, double moveX, double moveY)
        {
            try
            {
                this.le.MoveObject(text, moveX, moveY);
            }
            catch (EngineNotConnectedException)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", "Can not nudge object", "Engine not connected"), true);
                Logger.Error("{0}: {1}", "Can not nudge object", "Engine not connected");
            }
            catch (MethodFaultException err)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", "Can not nudge object", err.Message), true);
                Logger.Error("{0}: {1}", "Can not nudge object", err.Message);
            }
            catch (NotImplementedException)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", "Can not nudge object", "Method not implemented."), true);
                Logger.Error("{0}: {1}", "Can not nudge object", "Method not implemented.");
            }
        }

        /// <summary>
        /// Deletes the object.
        /// </summary>
        /// <param name="objectName">Name of the object.</param>
        /// <returns>Object deletion success state.</returns>
        private bool DeleteObject(string objectName)
        {
            try
            {
                this.le.DeleteObject(objectName);
                return true;
            }
            catch (MethodFaultException err)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", "Can not delete object", err.Message), true);
                Logger.Error("{0}: {1}", "Can not delete object", err.Message);
            }
            catch (NotImplementedException)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", "Can not delete object", "Method not implemented."), true);
                Logger.Error("{0}: {1}", "Can not delete object", "Method not implemented.");
            }

            return false;
        }

        /// <summary>
        /// Rotates an object.
        /// </summary>
        /// <param name="text">The object to rotate.</param>
        /// <param name="centerX">The center x.</param>
        /// <param name="centerY">The center y.</param>
        /// <param name="angle">The angle.</param>
        private void RotateObject(string text, double centerX, double centerY, double angle)
        {
            try
            {
                this.le.RotateObject(text, centerX, centerY, angle);
            }
            catch (EngineNotConnectedException)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", "Can not rotate object", "Engine not connected"), true);
                Logger.Error("{0}: {1}", "Can not rotate object", "Engine not connected");
            }
            catch (MethodFaultException err)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", "Can not rotate object", err.Message), true);
                Logger.Error("{0}: {1}", "Can not rotate object", err.Message);
            }
            catch (NotImplementedException)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", "Can not rotate object", "Method not implemented."), true);
                Logger.Error("{0}: {1}", "Can not rotate object", "Method not implemented.");
            }
        }

        /// <summary>
        /// Reads the port.
        /// </summary>
        /// <returns>Port data.</returns>
        private ushort ReadPort()
        {
            ushort data = 0;

            try
            {
                data = this.le.ReadPort();
            }
            catch (EngineNotConnectedException)
            {
                Logger.Error("IO error: {0}", "Engine not connected");
            }
            catch (NotImplementedException)
            {
                // Do nothing.
            }
            catch (MethodFaultException err)
            {
                Logger.Error("IO error: {0}", err.Message);

                if (err.ShowStopping)
                {
                    this.le.Disconnect();
                    this.ux_Timer_IO.Enabled = false;
                    var e = new ConnStateMessage { Status = ConnectionState.NotConnected };
                    this.ConnStateEvent(this, e);
                    this.TimeStampEvent("Engine connection fault.", true);
                }
            }

            return data;
        }

        /// <summary>
        /// Gets the laser frequency.
        /// </summary>
        /// <param name="penNumber">The pen number.</param>
        /// <returns>
        /// Laser Frequency.
        /// </returns>
        private int GetLaserFrequency(int penNumber)
        {
            string cantGetFreq = "Can not get frequency";

            try
            {
                return this.le.GetLaserFrequency(penNumber);
            }
            catch (EngineNotConnectedException)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", cantGetFreq, "Engine not connected"), true);
                Logger.Error("{0}: {1}", cantGetFreq, "Engine not connected");
            }
            catch (MethodFaultException err)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", cantGetFreq, err.Message), true);
                Logger.Error("{0}: {1}", cantGetFreq, err.Message);
            }
            catch (NotImplementedException)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", cantGetFreq, "Method not implemented."), true);
                Logger.Error("{0}: {1}", cantGetFreq, "Method not implemented.");
            }

            return -1;
        }

        /// <summary>
        /// Sets the cycle complete.
        /// </summary>
        /// <returns>Success state.</returns>
        private async Task<bool> SetCycleComplete()
        {
            await this.OutputWrite(true, 3);
            await Task.Delay(TykmaGlobals.INISettings.IOCOMPLETLENGTH);
            await this.OutputWrite(false, 3);
            return true;
        }

        private async Task<bool> MoveZIntoPosition(double pos)
        {
            MarkZ markz = new MarkZ(Path.Combine(TykmaGlobals.INISettings.SEINSTALLDIR, "plug", "MarkZ.par"));
            var bh = markz.GetBaseHeight();
            var fh = markz.GetFocusLength();

            if (bh == null || fh == null)
            {
                return false;
            }

            var zpos = bh - fh - pos;

            this.TimeStampEvent($"Moving Z into position: {zpos.Value:#.###}");
            await Task.Run(() => this.le.AxisMoveToPosition(this.axisZ.Number, zpos.Value));

            return true;
        }

        /// <summary>
        /// Gets the laser power.
        /// </summary>
        /// <param name="penNumber">The pen number.</param>
        /// <returns>
        /// Laser Power.
        /// </returns>
        private double GetLaserPower(int penNumber)
        {
            string cantGetFreq = "Can not get power";

            try
            {
                return this.le.GetLaserPower(penNumber);
            }
            catch (EngineNotConnectedException)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", cantGetFreq, "Engine not connected"), true);
                Logger.Error("{0}: {1}", cantGetFreq, "Engine not connected");
            }
            catch (MethodFaultException err)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", cantGetFreq, err.Message), true);
                Logger.Error("{0}: {1}", cantGetFreq, err.Message);
            }
            catch (NotImplementedException)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", cantGetFreq, "Method not implemented."), true);
                Logger.Error("{0}: {1}", cantGetFreq, "Method not implemented.");
            }

            return -1;
        }

        /// <summary>
        /// Sets the laser power.
        /// </summary>
        /// <param name="penNumber">The pen number.</param>
        /// <param name="power">The power.</param>
        /// <returns>Succcess state.</returns>
        private bool SetLaserPower(int penNumber, double power)
        {
            string cantGetFreq = "Can not set power";

            try
            {
                return this.le.SetLaserPower(penNumber, power);
            }
            catch (EngineNotConnectedException)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", cantGetFreq, "Engine not connected"), true);
                Logger.Error("{0}: {1}", cantGetFreq, "Engine not connected");
            }
            catch (MethodFaultException err)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", cantGetFreq, err.Message), true);
                Logger.Error("{0}: {1}", cantGetFreq, err.Message);
            }
            catch (NotImplementedException)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", cantGetFreq, "Method not implemented."), true);
                Logger.Error("{0}: {1}", cantGetFreq, "Method not implemented.");
            }

            return false;
        }

        /// <summary>
        /// Changes the value of a given object..
        /// </summary>
        /// <param name="objectName">Name of the object.</param>
        /// <param name="text">The new data.</param>
        /// <param name="idCombine">Whether we should do ID combination logic.</param>
        /// <returns>Success state.</returns>
        private async Task<bool> ChangeTextByName(string objectName, string text, bool idCombine = true)
        {
            try
            {
                var result = await this.le.ChangeTextByName(objectName, text);
                this.HandleBoxFilling();

                if (objectName.Equals("%Z"))
                {
                    this.GetZHeight();
                }

                if (idCombine)
                {
                    this.SetCombinationIDs();
                }

                return result;
            }
            catch (EngineNotConnectedException)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", "Can not change object data", "Engine not connected"), true);
                Logger.Error("{0}: {1}", "Can not change object data", "Engine not connected");
            }
            catch (MethodFaultException err)
            {
                if (!err.Message.Contains("not a text object"))
                {
                    this.TimeStampEvent(string.Format("{0}: {1}", "Can not change object data", err.Message), true);
                    Logger.Error("{0}: {1}", "Can not change object data", err.Message);
                }
            }
            catch (NotSupportedException)
            {
                this.TimeStampEvent(string.Format("{0}: {1}", "Can not change object data", "Method not implemented."), true);
                Logger.Error("{0}: {1}", "Can not change object data", "Method not implemented.");
            }

            return false;
        }

        /// <summary>
        /// Stops an axis.
        /// </summary>
        /// <param name="whichAxis">Which axis.</param>
        /// <returns>Success state.</returns>
        private bool StopAnAxis(AxisSetting whichAxis)
        {
            this.TimeStampEvent(string.Format("Stopping {0} Axis", whichAxis.CommonName));

            try
            {
                this.le.AxisMoveToPosition(whichAxis.Number, this.le.GetAxisCoordinate(whichAxis.Number));
                return true;
            }
            catch (MethodFaultException err)
            {
                Logger.Error("Axis move: {0}", err.Message);
            }
            catch (EngineNotConnectedException)
            {
                this.TimeStampEvent("Can not move axis - engine not connected");
            }

            return false;
        }

        /// <summary>
        /// Sets the custom date code.
        /// </summary>
        private async void SetCustomDateCode()
        {
            // Let's see if we need to set a date code.
            if (TykmaGlobals.INISettings.DATECODEID != string.Empty)
            {
                if (this.AllIDs.Any(item => item.Name == TykmaGlobals.INISettings.DATECODEID))
                {
                    var code = this.datecodegenerator.GetDateCode(TykmaGlobals.INISettings.DATECODEFORMAT);

                    await this.ChangeTextByName(TykmaGlobals.INISettings.DATECODEID, code);
                    Logger.Info("Set custom date code to: {0}", code);
                    this.TimeStampEvent(string.Format("Set custom date code to: {0}", code));
                }
            }
        }
    }
}
