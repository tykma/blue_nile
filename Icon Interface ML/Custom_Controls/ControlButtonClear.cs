﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlButtonClear.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    using System.ComponentModel;
    using System.Drawing;

    using Icon_Interface.Properties;

    /// <summary>
    /// Clear icon.
    /// </summary>
    public class ControlButtonClear : ControlButtonBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlButtonClear"/> class.
        /// </summary>
        public ControlButtonClear()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes the component.
        /// </summary>
        private void InitializeComponent()
        {
            ComponentResourceManager resources = new ComponentResourceManager(typeof(ControlButtonClear));
            ((ISupportInitialize)this.ux_Image_Icon).BeginInit();
            this.SuspendLayout();

            // ux_Image_Icon
            this.ux_Image_Icon.Image = ImageResources._105454_3d_glossy_orange_orb_icon_business_trashcan3;

            // ux_Label_Action
            this.ux_Label_Action.Text = Resources.Main_Clear_Button;

            // ControlButtonClear
            this.AutoScaleDimensions = new SizeF(6F, 13F);
            this.Name = "ControlButtonClear";
            ((ISupportInitialize)this.ux_Image_Icon).EndInit();
            this.ResumeLayout(false);
        }
    }
}
