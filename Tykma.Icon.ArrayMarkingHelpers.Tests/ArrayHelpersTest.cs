using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Tykma.Core.Entity;

namespace Tykma.Icon.ArrayMarkingHelpers.Tests
{
    [TestClass]
    public class ArrayHelpersTest
    {
        [TestMethod]
        public void TestIsArray()
        {
            var l = new List<EntityInformation>();

            var ent = new EntityInformation("BOX:1", string.Empty);
            l.Add(ent);

            Assert.IsTrue(new ArrayHelper().IsArray(l));
        }

        [TestMethod]
        public void TestIsNotArray()
        {
            var l = new List<EntityInformation>();

            var ent = new EntityInformation("BOX", string.Empty);
            l.Add(ent);

            Assert.IsFalse(new ArrayHelper().IsArray(l));
        }

        [TestMethod]
        public void TestArrayCount()
        {
            var l = new List<EntityInformation>();

            for (int i = 0; i <= 100; i++)
            {
                l.Add(new EntityInformation($"BOX:{i}", ""));
            }

            Assert.AreEqual(100, new ArrayHelper().ArrayMax(l));
        }

        [TestMethod]
        public void TestArrayItems()
        {
            var l = new List<EntityInformation>();

            l.Add(new EntityInformation($"BOX:4", ""));
            l.Add(new EntityInformation($"BOX:2", ""));
            l.Add(new EntityInformation($"BOX:3", ""));
            l.Add(new EntityInformation($"BOX:1", ""));

            var mark = new ArrayHelper().EntsToMark(l, 4);

            Assert.AreEqual(4, mark.Count);

            Assert.AreEqual("BOX:1", mark[0].Name);
            Assert.AreEqual("BOX:2", mark[1].Name);
            Assert.AreEqual("BOX:3", mark[2].Name);
            Assert.AreEqual("BOX:4", mark[3].Name);
        }

        [TestMethod]
        public void TestArrayItemsMore()
        {
            var l = new List<EntityInformation>();

            l.Add(new EntityInformation($"BOX:4", ""));
            l.Add(new EntityInformation($"BOX:2", ""));
            l.Add(new EntityInformation($"BOX:3", ""));
            l.Add(new EntityInformation($"BOX:1", ""));

            var mark = new ArrayHelper().EntsToMark(l, 40);

            Assert.AreEqual(4, mark.Count);

            Assert.AreEqual("BOX:1", mark[0].Name);
            Assert.AreEqual("BOX:2", mark[1].Name);
            Assert.AreEqual("BOX:3", mark[2].Name);
            Assert.AreEqual("BOX:4", mark[3].Name);
        }


        [TestMethod]
        public void TestArrayItemsLess()
        {
            var l = new List<EntityInformation>();

            l.Add(new EntityInformation($"BOX:4", ""));
            l.Add(new EntityInformation($"BOX:2", ""));
            l.Add(new EntityInformation($"BOX:3", ""));
            l.Add(new EntityInformation($"BOX:1", ""));

            var mark = new ArrayHelper().EntsToMark(l, 2);

            Assert.AreEqual(2, mark.Count);

            Assert.AreEqual("BOX:1", mark[0].Name);
            Assert.AreEqual("BOX:2", mark[1].Name);
        }

        [TestMethod]
        public void TestArrayToMarkIncludesNonArrayedObjects()
        {
            var l = new List<EntityInformation>();

            l.Add(new EntityInformation($"BOX:4", ""));
            l.Add(new EntityInformation($"BOX:2", ""));
            l.Add(new EntityInformation($"BOX:3", ""));
            l.Add(new EntityInformation($"BOX:1", ""));
            l.Add(new EntityInformation($"HELLO:1", ""));

            var mark = new ArrayHelper().EntsToMark(l, 4);

            Assert.AreEqual(5, mark.Count);

            Assert.AreEqual("BOX:1", mark[0].Name);
            Assert.AreEqual("HELLO:1", mark[1].Name);
            Assert.AreEqual("BOX:2", mark[2].Name);
            Assert.AreEqual("BOX:3", mark[3].Name);
            Assert.AreEqual("BOX:4", mark[4].Name);
        }

        [TestMethod]
        public void TestArrayMarkingWithDoubleColon()
        {
            var l = new List<EntityInformation>();
            l.Add(new EntityInformation($"BOX:1,:1", ""));
            l.Add(new EntityInformation($"BOX:1,:2", ""));

            var mark = new ArrayHelper().EntsToMark(l, 2);
            Assert.AreEqual(2, mark.Count);

        }
    }
}
