﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QueueTests.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.Queue
{
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Icon.Queue;

    /// <summary>
    /// The unit test for queue class.
    /// </summary>
    [TestClass]
    public class QueueTests
    {
        /// <summary>
        /// Test that adding item to a queue works.
        /// </summary>
        [TestMethod]
        public void TestAddingItemToQueue()
        {
            JobQueue queue = new JobQueue();
            queue.AddItemToQueue("hi");
            var job = queue.NextJob();
            Assert.AreEqual("hi", job.JobName);
        }

        /// <summary>
        /// Tests removing an item from the queue.
        /// </summary>
        [TestMethod]
        public void TestRemovingItemFromQueue()
        {
            JobQueue queue = new JobQueue();
            queue.AddItemToQueue("removeme");
            Assert.AreEqual(1, queue.CountAllToDo());
            var job = queue.NextJob();
            queue.RemoveJob(job.JobHash);
            Assert.AreEqual(0, queue.CountAllToDo());
        }

        /// <summary>
        /// Tests that duplicate jobs are treated differently.
        /// </summary>
        [TestMethod]
        public void TestDuplicateJobsTreatedDifferently()
        {
            JobQueue queue = new JobQueue();
            queue.AddItemToQueue("test1");
            queue.AddItemToQueue("test1");
            Assert.AreEqual(2, queue.CountAllToDo());
        }

        /// <summary>
        /// Tests clearing jobs.
        /// </summary>
        [TestMethod]
        public void TestClearingJobs()
        {
            JobQueue queue = new JobQueue();
            queue.AddItemToQueue("test1");
            queue.Clear();
            Assert.AreEqual(0, queue.CountAllToDo());
            Assert.AreEqual(0, queue.CountAll());
        }

        /// <summary>
        /// Tests adding many jobs and verifies there are no duplicates..
        /// </summary>
        [TestMethod]
        public void TestManyJobsForCollisions()
        {
            JobQueue queue = new JobQueue();
            for (int i = 0; i < 10000; i++)
            {
                queue.AddItemToQueue("test" + i);
            }

            Assert.AreEqual(10000, queue.CountAllToDo());
        }

        /// <summary>
        /// Tests the exhausting job.
        /// </summary>
        [TestMethod]
        public void TestExhaustingJob()
        {
            JobQueue queue = new JobQueue();
            queue.AddItemToQueue("test1");
            var job = queue.NextJob();
            queue.RemoveJob(job.JobHash);

            var jobFound = false;

            foreach (var queuedJob in queue.FullList.Where(queuedJob => queuedJob.JobHash == job.JobHash))
            {
                jobFound = true;
                Assert.IsTrue(queuedJob.Finished);
            }

            Assert.IsTrue(jobFound);
        }

        /// <summary>
        /// Tests an empty queued job.
        /// </summary>
        [TestMethod]
        public void TestEmptyQueuedJob()
        {
            JobQueue queue = new JobQueue();
            var job = queue.NextJob();
            Assert.AreEqual(string.Empty, job.JobName);
        }

        /// <summary>
        /// Tests the next job when none available.
        /// </summary>
        [TestMethod]
        public void TestNextJobWhenNoneAvailable()
        {
            JobQueue queue = new JobQueue();
            queue.AddItemToQueue("test1");
            var job = queue.NextJob();
            queue.RemoveJob(job.JobHash);
            var job2 = queue.NextJob();
            Assert.AreEqual(string.Empty, job2.JobName);
        }

        /// <summary>
        /// Tests if job is complete.
        /// </summary>
        [TestMethod]
        public void TestIfJobIsComplete()
        {
            JobQueue queue = new JobQueue();
            queue.AddItemToQueue("test1");
            Assert.IsFalse(queue.IsJobComplete(queue.NextJob().JobHash));
        }

        /// <summary>
        /// Tests the enabling job.
        /// </summary>
        [TestMethod]
        public void TestEnablingJob()
        {
            JobQueue queue = new JobQueue();
            queue.AddItemToQueue("test1");
            var job = queue.NextJob();
            queue.RemoveJob(job.JobHash);
            Assert.IsTrue(job.Finished);

            queue.EnableJob(job.JobHash);
            Assert.IsFalse(job.Finished);
        }

        /// <summary>
        /// Tests the queue jobs available message.
        /// </summary>
        [TestMethod]
        public void TestQueueJobsAvailableMessage()
        {
            var args = new QueueEventMessage { JobsAvailable = true };

            Assert.IsTrue(args.JobsAvailable);

            var argsFalse = new QueueEventMessage { JobsAvailable = false };

            Assert.IsFalse(argsFalse.JobsAvailable);
        }

        /// <summary>
        /// Tests set queued item as failed.
        /// </summary>
        [TestMethod]
        public void TestSetQueuedItemAsFailed()
        {
            var qji = new QueuedJobInfo("Z");
            qji.Failed = true;

            Assert.IsTrue(qji.Failed);
            qji.Failed = false;

            Assert.IsFalse(qji.Failed);
        }
    }
}
