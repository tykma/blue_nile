﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScanSimpleMark.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.ScanParse
{
    using Tykma.Core.StringExtensions;

    /// <summary>
    /// A single ID is set.
    /// </summary>
    public class ScanSimpleMark : IBaseDataReturn
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScanSimpleMark"/> class.
        /// </summary>
        /// <param name="stripPrefix">if set to <c>true</c> [strip prefix].</param>
        /// <param name="prefix">The prefix.</param>
        internal ScanSimpleMark(bool stripPrefix, string prefix)
        {
            this.StripPrefix = stripPrefix;
            this.PrefixToStrip = prefix;
            this.Data = string.Empty;
        }

        /// <summary>
        /// Gets or sets a value indicating whether [first identifier].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [first identifier]; otherwise, <c>false</c>.
        /// </value>
        public bool FirstID { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The SQL command.
        /// </value>
        public string Data { get; set; }

        /// <summary>
        /// Gets a value indicating whether we will strip the prefix.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [strip prefix]; otherwise, <c>false</c>.
        /// </value>
        private bool StripPrefix { get; }

        /// <summary>
        /// Gets the prefix to strip.
        /// </summary>
        /// <value>
        /// The prefix to strip.
        /// </value>
        private string PrefixToStrip { get; }

        /// <summary>
        /// Adds the specified name value pair.
        /// </summary>
        /// <param name="value">The value.</param>
        public void Add(string value)
        {
            string processedValue = value;

            // Make sure the data longer than character.
            // Otherwise "*" would be a First ID and not a second, since it starts and ends with the prefix.
            if (value.Length > 1)
            {
                if (value.StartsWith(this.PrefixToStrip) && value.EndsWith(this.PrefixToStrip))
                {
                    this.FirstID = true;
                }
            }

            if (this.StripPrefix)
            {
                processedValue = value.RemovePrefix(this.PrefixToStrip);
                processedValue = processedValue.RemoveSuffix(this.PrefixToStrip);
            }

            this.Data = processedValue;
        }
    }
}
