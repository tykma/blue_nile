﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConnectionStatusMessage.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.PLC
{
    using System;

    /// <summary>
    /// Connection status string.
    /// </summary>
    public class ConnectionStatusMessage : EventArgs
    {
        /// <summary>
        /// Gets or sets the CONNECTED string.
        /// </summary>
        /// <value>
        /// The CONNECTED string.
        /// </value>
        public string ConnectedMessage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ConnectionStatusMessage" /> is CONNECTED.
        /// </summary>
        /// <value>
        ///   <c>true</c> if CONNECTED; otherwise, <c>false</c>.
        /// </value>
        public bool Connected { get; set; }
    }
}