﻿//-----------------------------------------------------------------------
// <copyright file="Server.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Tykma.Core.HTTPD
{
    using System;
    using System.Net;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// HTTPD server.
    /// Initializes TCP listener threads which will wait for incoming connections.
    /// Returns connection status and events when a socket is connected.
    /// ............
    /// Operation:
    /// Server spins a TCP Listener.
    /// When a connection is made Server sends the socket in an event.
    /// Listener can use the client socket to wait and read data.
    /// Once data is retrieved listener can respond using the client socket.
    /// </summary>
    public class Server : IDisposable
    {
        /// <summary>
        /// The server name.
        /// </summary>
        private const string HttpdServerName = "tykma_ml_httpd";

        /// <summary>
        /// The TCP listener.
        /// </summary>
        private readonly TcpListenerEx tcpListener;

        /// <summary>
        /// The listen task.
        /// </summary>
        private readonly Task listenTask;

        /// <summary>
        /// The cancellation token.
        /// </summary>
        private readonly CancellationTokenSource tokenListen = new CancellationTokenSource();

        /// <summary>
        /// Initializes a new instance of the <see cref="Server"/> class.
        /// </summary>
        /// <param name="port">The port.</param>
        public Server(int port)
        {
            // Start listening
            this.tcpListener = new TcpListenerEx(IPAddress.Any, port);
            this.tcpListener.Start();

            // Start a background thread to listen for incoming connection.
            this.listenTask = Task.Factory.StartNew(this.ListenLoop, TaskCreationOptions.LongRunning, this.tokenListen.Token);
        }

        /// <summary>
        /// Occurs when data is ready.
        /// </summary>
        public event EventHandler<SocketEventMessage> SocketConnected;

        /// <summary>
        /// Gets a value indicating whether [is connected].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is connected]; otherwise, <c>false</c>.
        /// </value>
        public bool IsConnected => this.tcpListener.Active;

        /// <summary>
        /// Gets the name of the server.
        /// </summary>
        /// <value>
        /// The name of the server.
        /// </value>
        internal string ServerName => HttpdServerName;

        /// <summary>
        /// Disconnects this instance.
        /// </summary>
        public void Disconnect()
        {
            if (this.listenTask != null)
            {
                this.tokenListen.Cancel();
                this.tcpListener.Stop();
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (this.listenTask == null)
            {
            }
        }

        /// <summary>
        /// The loop which will listen for a connection.
        /// </summary>
        /// <param name="o">The object.</param>
        private async void ListenLoop(object o)
        {
            try
            {
                while (!this.tokenListen.Token.IsCancellationRequested)
                {
                    // Wait for connection
                    if (this.tcpListener != null)
                    {
                        var socket = await this.tcpListener.AcceptSocketAsync();
                        if (socket == null)
                        {
                            break;
                        }

                        // Got new connection, create a client handler for it
                        var client = new ServerClient(this, socket);
                        var args = new SocketEventMessage { Client = client };
                        this.SocketConnected?.Invoke(this, args);
                    }
                }
            }
            catch (ObjectDisposedException)
            {
                // Log.Debug("Media HttpServer closed.");
            }
        }
    }
}
