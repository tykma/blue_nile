﻿//-----------------------------------------------------------------------
// <copyright file="OSInfo.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Tykma.Core.OSInfo
{
    using System;
    using System.Globalization;
    using System.Runtime.InteropServices;

    /// <summary>
    /// OSInfo Class.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1124:Do not use regions", Justification = "Useful in this file.")]
    public static class OSInfo
    {
        // ReSharper disable InconsistentNaming
        #region Private Constants

        /// <summary>
        /// NT WS.
        /// </summary>
        private const int NTWORKSTATION = 1;

        /// <summary>
        /// NT DC.
        /// </summary>
        private const int NTDOMAINCONTROLLER = 2;

        /// <summary>
        /// The NT Server.
        /// </summary>
        private const int NTSERVER = 3;

        /// <summary>
        /// ENTERPRISE version.
        /// </summary>
        private const int VERSUITEENTERPRISE = 0x00000002;

        /// <summary>
        /// Embedded NT.
        /// </summary>
        private const int VERSUITEEMBEDDEDNT = 0x00000040;

        /// <summary>
        /// Data Center.
        /// </summary>
        private const int VERSUITEDATACENTER = 0x00000080;

        /// <summary>
        /// Personal version.
        /// </summary>
        private const int VERSUITEPERSONAL = 0x00000200;

        /// <summary>
        /// Blade version.
        /// </summary>
        private const int VERSUITEBLADE = 0x00000400;

        /// <summary>
        /// Storage server.
        /// </summary>
        private const int VERSUITESTORAGESERVER = 0x00002000;

        /// <summary>
        /// Compute server.
        /// </summary>
        private const int VERSUITECOMPUTESERV = 0x00004000;

        /// <summary>
        /// BUSINESS Version.
        /// </summary>
        private const int PRODUCTBUSINESS = 0x00000006;

        /// <summary>
        /// Business N Version.
        /// </summary>
        private const int PRODUCTBUSINESSN = 0x00000010;

        /// <summary>
        /// Cluster server.
        /// </summary>
        private const int PRODUCTCLUSTERSERVER = 0x00000012;

        /// <summary>
        /// DataCenter Server.
        /// </summary>
        private const int PRODUCTDATACENTERSERVER = 0x00000008;

        /// <summary>
        /// DataCenter server core.
        /// </summary>
        private const int PRODUCTDATACENTERSERVERCORE = 0x0000000C;

        /// <summary>
        /// DataCenter server core V.
        /// </summary>
        private const int PRODUCTDATACENTERSERVERCOREV = 0x00000027;

        /// <summary>
        /// DataCenter server V.
        /// </summary>
        private const int PRODUCTDATACENTERSERVERV = 0x00000025;

        /// <summary>
        /// Enterprise version.
        /// </summary>
        private const int PRODUCTENTERPRISE = 0x00000004;

        /// <summary>
        /// Enterprise E version.
        /// </summary>
        private const int PRODUCTENTERPRISEE = 0x00000046;

        /// <summary>
        /// Enterprise N.
        /// </summary>
        private const int PRODUCTENTERPRISEN = 0x0000001B;

        /// <summary>
        /// Enterprise server.
        /// </summary>
        private const int PRODUCTENTERPRISESERVER = 0x0000000A;

        /// <summary>
        /// Enterprise server core.
        /// </summary>
        private const int PRODUCTENTERPRISESERVERCORE = 0x0000000E;

        /// <summary>
        /// Enterprise server core V.
        /// </summary>
        private const int PRODUCTENTERPRISESERVERCOREV = 0x00000029;

        /// <summary>
        /// Enterprise server IA64.
        /// </summary>
        private const int PRODUCTENTERPRISESERVERIA64 = 0x0000000F;

        /// <summary>
        /// Enterprise server V.
        /// </summary>
        private const int PRODUCTENTERPRISESERVERV = 0x00000026;

        /// <summary>
        /// Home basic.
        /// </summary>
        private const int PRODUCTHOMEBASIC = 0x00000002;

        /// <summary>
        /// Home basic E.
        /// </summary>
        private const int PRODUCTHOMEBASICE = 0x00000043;

        /// <summary>
        /// Home basic N.
        /// </summary>
        private const int PRODUCTHOMEBASICN = 0x00000005;

        /// <summary>
        /// Home Premium.
        /// </summary>
        private const int PRODUCTHOMEPREMIUM = 0x00000003;

        /// <summary>
        /// Home Premium E.
        /// </summary>
        private const int PRODUCTHOMEPREMIUME = 0x00000044;

        /// <summary>
        /// Home premium N.
        /// </summary>
        private const int PRODUCTHOMEPREMIUMN = 0x0000001A;

        /// <summary>
        /// MBS Management.
        /// </summary>
        private const int PRODUCTMEDIUMBUSINESSSERVERMANAGEMENT = 0x0000001E;

        /// <summary>
        /// MBS messaging..
        /// </summary>
        private const int PRODUCTMEDIUMBUSINESSSERVERMESSAGING = 0x00000020;

        /// <summary>
        /// MBS Security.
        /// </summary>
        private const int PRODUCTMEDIUMBUSINESSSERVERSECURITY = 0x0000001F;

        /// <summary>
        /// Professional product.
        /// </summary>
        private const int PRODUCTPROFESSIONAL = 0x00000030;

        /// <summary>
        /// Professional product E.
        /// </summary>
        private const int PRODUCTPROFESSIONALE = 0x00000045;

        /// <summary>
        /// Professional product N.
        /// </summary>
        private const int PRODUCTPROFESSIONALN = 0x00000031;

        /// <summary>
        /// Server for small business.
        /// </summary>
        private const int PRODUCTSERVERFORSMALLBUSINESS = 0x00000018;

        /// <summary>
        /// Server for small business V.
        /// </summary>
        private const int PRODUCTSERVERFORSMALLBUSINESSV = 0x00000023;

        /// <summary>
        /// Small business server.
        /// </summary>
        private const int PRODUCTSMALLBUSINESSSERVER = 0x00000009;

        /// <summary>
        /// Standard server.
        /// </summary>
        private const int PRODUCTSTANDARDSERVER = 0x00000007;

        /// <summary>
        /// Standard server core.
        /// </summary>
        private const int PRODUCTSTANDARDSERVERCORE = 0x0000000D;

        /// <summary>
        /// Standard server core v.
        /// </summary>
        private const int PRODUCTSTANDARDSERVERCOREV = 0x00000028;

        /// <summary>
        /// Standard server V.
        /// </summary>
        private const int PRODUCTSTANDARDSERVERV = 0x00000024;

        /// <summary>
        /// Starter version.
        /// </summary>
        private const int PRODUCTSTARTER = 0x0000000B;

        /// <summary>
        /// Starter E.
        /// </summary>
        private const int PRODUCTSTARTERE = 0x00000042;

        /// <summary>
        /// Starter N.
        /// </summary>
        private const int PRODUCTSTARTERN = 0x0000002F;

        /// <summary>
        /// Storage Enterprise server.
        /// </summary>
        private const int PRODUCTSTORAGEENTERPRISESERVER = 0x00000017;

        /// <summary>
        /// Storage express server.
        /// </summary>
        private const int PRODUCTSTORAGESEXPRESSSERVER = 0x00000014;

        /// <summary>
        /// Storage standard server.
        /// </summary>
        private const int PRODUCTSTORAGESTANDARDSERVER = 0x00000015;

        /// <summary>
        /// Storage workgroup server.
        /// </summary>
        private const int PRODUCTSTORAGEWORKGROUPSERVER = 0x00000016;

        /// <summary>
        /// Ultimate product.
        /// </summary>
        private const int PRODUCTULTIMATE = 0x00000001;

        /// <summary>
        /// Ultimate E product.
        /// </summary>
        private const int PRODUCTULTIMATEE = 0x00000047;

        /// <summary>
        /// Ultimate N product.
        /// </summary>
        private const int PRODUCTULTIMATEN = 0x0000001C;

        /// <summary>
        /// Web server product.
        /// </summary>
        private const int PRODUCTWEBSERVER = 0x00000011;

        /// <summary>
        /// Web server core product.
        /// </summary>
        private const int PRODUCTWEBSERVERCORE = 0x0000001D;

        /// <summary>
        /// The MINORVERSION.
        /// </summary>
        private const int VERMINORVERSION = 0x0000001;

        /// <summary>
        /// The MAJORVERSION.
        /// </summary>
        private const int VERMAJORVERSION = 0x0000002;

        /// <summary>
        /// The BUILDVERSION.
        /// </summary>
        private const int VERBUILDVERSION = 0x0000004;

        /// <summary>
        /// The SERVICE PACK MAJOR version.
        /// </summary>
        private const int VERSERVICEPACKMAJOR = 0x0000020;

        /// <summary>
        /// The product type.
        /// </summary>
        private const int VERPRODUCTTYPE = 0x0000080;

        /// <summary>
        /// Version equal bitmask.
        /// </summary>
        private const int VEREQUAL = 1;

        /// <summary>
        /// Version greater or equal to bitmask.
        /// </summary>
        private const int VERGREATEREQUAL = 3;

        /// <summary>
        /// System metric: Tablet PC:.
        /// </summary>
        private const int SMTABLETPC = 86;

        /// <summary>
        /// System metric: Media Center PC.
        /// </summary>
        private const int SMMEDIACENTER = 87;

        /// <summary>
        /// System metric: Server R2.
        /// </summary>
        private const int SMSERVERR2 = 89;

        // ReSharper restore InconsistentNaming
        #endregion

        #region Enums

        /// <summary>
        /// List of all operating systems.
        /// </summary>
        public enum OSList
        {
            /// <summary>
            /// Version of Windows 95/98, NT4.0 or 2000.
            /// </summary>
            Windows2000AndPrevious,

            /// <summary>
            /// Windows XP x86
            /// </summary>
            WindowsXp,

            /// <summary>
            /// Windows XP x64
            /// </summary>
            WindowsXp64,

            /// <summary>
            /// Windows Vista
            /// </summary>
            WindowsVista,

            /// <summary>
            /// Windows 7
            /// </summary>
            Windows7,

            /// <summary>
            /// Windows 8
            /// </summary>
            Windows8,

            /// <summary>
            /// Windows 8
            /// </summary>
            Windows81,

            /// <summary>
            /// Windows 2003 Server
            /// </summary>
            Windows2003,

            /// <summary>
            /// Windows 2003 R2 Server
            /// </summary>
            Windows2003R2,

            /// <summary>
            /// Windows 2008 Server
            /// </summary>
            Windows2008,

            /// <summary>
            /// Windows 2008 R2 Server
            /// </summary>
            Windows2008R2,

            /// <summary>
            /// Windows 2012 Server
            /// </summary>
            Windows2012,
        }

        /// <summary>
        /// List of available status of current OS.
        /// </summary>
        public enum OsSupport
        {
            /// <summary>
            /// Blocked: will cause an immediate exit of the program
            /// </summary>
            Blocked = 0,

            /// <summary>
            /// FullySupported: self explanatory
            /// </summary>
            FullySupported = 1,

            /// <summary>
            /// NotSupported: officially not supported, will log/display a warning
            /// </summary>
            NotSupported = 2,
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the full version of the operating system running on this computer.
        /// </summary>
        public static string OSVersion => Environment.OSVersion.Version.ToString();

        /// <summary>
        /// Gets the major version of the operating system running on this computer.
        /// </summary>
        public static int OSMajorVersion => Environment.OSVersion.Version.Major;

        /// <summary>
        /// Gets the minor version of the operating system running on this computer.
        /// </summary>
        public static int OSMinorVersion => Environment.OSVersion.Version.Minor;

        /// <summary>
        /// Gets the build version of the operating system running on this computer.
        /// </summary>
        public static int OSBuildVersion => Environment.OSVersion.Version.Build;

        /// <summary>
        /// Gets the revision version of the operating system running on this computer.
        /// </summary>
        public static int OSRevisionVersion => Environment.OSVersion.Version.Revision;

        /// <summary>
        /// Gets the main version of the service pack running on this computer.
        /// </summary>
        public static int OSServicePackMajor
        {
            get
            {
                var operatingSystemVersionInfo = new OSVERSIONINFOEX
                {
                    OSVersionInfoSize = Marshal.SizeOf(typeof(OSVERSIONINFOEX)),
                };
                if (!NativeMethods.GetVersionEx(ref operatingSystemVersionInfo))
                {
                    return -1;
                }

                return operatingSystemVersionInfo.ServicePackMajor;
            }
        }

        /// <summary>
        /// Gets the main version of the service pack running on this computer.
        /// </summary>
        public static int OSServicePackMinor
        {
            get
            {
                var operatingSystemVersionInfo = new OSVERSIONINFOEX
                {
                    OSVersionInfoSize = Marshal.SizeOf(typeof(OSVERSIONINFOEX)),
                };
                return !NativeMethods.GetVersionEx(ref operatingSystemVersionInfo) ? -1 : operatingSystemVersionInfo.ServicePackMinor;
            }
        }

        /// <summary>
        /// Gets the string description of the service pack running on this computer.
        /// </summary>
        public static string OSServicePackDesc
        {
            get
            {
                var operatingSystemVersionInfo = new OSVERSIONINFOEX
                {
                    OSVersionInfoSize = Marshal.SizeOf(typeof(OSVERSIONINFOEX)),
                };
                return !NativeMethods.GetVersionEx(ref operatingSystemVersionInfo) ? string.Empty : operatingSystemVersionInfo.CSDVersion;
            }
        }

        /// <summary>
        /// Gets the product type of the operating system running on this computer.
        /// </summary>
        public static byte OSProductType
        {
            get
            {
                var operatingSystemVersionInfo = new OSVERSIONINFOEX
                {
                    OSVersionInfoSize = Marshal.SizeOf(typeof(OSVERSIONINFOEX)),
                };
                if (!NativeMethods.GetVersionEx(ref operatingSystemVersionInfo))
                {
                    return 0x0;
                }

                return operatingSystemVersionInfo.ProductType;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns the product type of the operating system running on this computer.
        /// </summary>
        /// <returns>A string containing the the operating system product type.</returns>
        public static string GetOSProductType()
        {
            var operatingSystemVersionInfo = new OSVERSIONINFOEX
            {
                OSVersionInfoSize = Marshal.SizeOf(typeof(OSVERSIONINFOEX)),
            };
            if (!NativeMethods.GetVersionEx(ref operatingSystemVersionInfo))
            {
                return string.Empty;
            }

            switch (OSMajorVersion)
            {
                case 4:
                    if (OSProductType == NTWORKSTATION)
                    {
                        // Windows NT 4.0 Workstation
                        return " Workstation";
                    }

                    if (OSProductType == NTSERVER)
                    {
                        // Windows NT 4.0 Server
                        return " Server";
                    }

                    return string.Empty;
                case 5:
                    if (NativeMethods.GetSystemMetrics(SMMEDIACENTER))
                    {
                        return " Media Center";
                    }

                    if (NativeMethods.GetSystemMetrics(SMTABLETPC))
                    {
                        return " Tablet PC";
                    }

                    if (OSProductType == NTWORKSTATION)
                    {
                        if ((operatingSystemVersionInfo.SuiteMask & VERSUITEEMBEDDEDNT) == VERSUITEEMBEDDEDNT)
                        {
                            // Windows XP Embedded.
                            return " Embedded";
                        }

                        return (operatingSystemVersionInfo.SuiteMask & VERSUITEPERSONAL) == VERSUITEPERSONAL ? " Home" : " Professional";
                        //// Windows XP / Windows 2000 Professional
                    }

                    if (OSProductType == NTSERVER || OSProductType == NTDOMAINCONTROLLER)
                    {
                        if (OSMinorVersion == 0)
                        {
                            if ((operatingSystemVersionInfo.SuiteMask & VERSUITEDATACENTER) == VERSUITEDATACENTER)
                            {
                                // Windows 2000 Datacenter Server
                                return " Datacenter Server";
                            }

                            if ((operatingSystemVersionInfo.SuiteMask & VERSUITEENTERPRISE) == VERSUITEENTERPRISE)
                            {
                                // Windows 2000 Advanced Server
                                return " Advanced Server";
                            }

                            // Windows 2000 Server
                            return " Server";
                        }

                        if (OSMinorVersion == 2)
                        {
                            if ((operatingSystemVersionInfo.SuiteMask & VERSUITEDATACENTER) == VERSUITEDATACENTER)
                            {
                                // Windows Server 2003 Datacenter Edition
                                return " Datacenter Edition";
                            }

                            if ((operatingSystemVersionInfo.SuiteMask & VERSUITEENTERPRISE) == VERSUITEENTERPRISE)
                            {
                                // Windows Server 2003 Enterprise Edition
                                return " Enterprise Edition";
                            }

                            if ((operatingSystemVersionInfo.SuiteMask & VERSUITESTORAGESERVER) == VERSUITESTORAGESERVER)
                            {
                                // Windows Server 2003 Storage Edition
                                return " Storage Edition";
                            }

                            if ((operatingSystemVersionInfo.SuiteMask & VERSUITECOMPUTESERV) == VERSUITECOMPUTESERV)
                            {
                                // Windows Server 2003 Compute Cluster Edition
                                return " Compute Cluster Edition";
                            }

                            if ((operatingSystemVersionInfo.SuiteMask & VERSUITEBLADE) == VERSUITEBLADE)
                            {
                                // Windows Server 2003 Web Edition
                                return " Web Edition";
                            }

                            // Windows Server 2003 Standard Edition
                            return " Standard Edition";
                        }
                    }

                    break;
                case 6:
                    int strProductType;
                    NativeMethods.GetProductInfo(operatingSystemVersionInfo.MajorVersion, operatingSystemVersionInfo.MinorVersion, 0, 0, out strProductType);
                    switch (strProductType)
                    {
                        case PRODUCTULTIMATE:
                        case PRODUCTULTIMATEE:
                        case PRODUCTULTIMATEN:
                            return "Ultimate Edition";
                        case PRODUCTPROFESSIONAL:
                        case PRODUCTPROFESSIONALE:
                        case PRODUCTPROFESSIONALN:
                            return "Professional";
                        case PRODUCTHOMEPREMIUM:
                        case PRODUCTHOMEPREMIUME:
                        case PRODUCTHOMEPREMIUMN:
                            return "Home Premium Edition";
                        case PRODUCTHOMEBASIC:
                        case PRODUCTHOMEBASICE:
                        case PRODUCTHOMEBASICN:
                            return "Home Basic Edition";
                        case PRODUCTENTERPRISE:
                        case PRODUCTENTERPRISEE:
                        case PRODUCTENTERPRISEN:
                        case PRODUCTENTERPRISESERVERV:
                            return "Enterprise Edition";
                        case PRODUCTBUSINESS:
                        case PRODUCTBUSINESSN:
                            return "Business Edition";
                        case PRODUCTSTARTER:
                        case PRODUCTSTARTERE:
                        case PRODUCTSTARTERN:
                            return "Starter Edition";
                        case PRODUCTCLUSTERSERVER:
                            return "Cluster Server Edition";
                        case PRODUCTDATACENTERSERVER:
                        case PRODUCTDATACENTERSERVERV:
                            return "Datacenter Edition";
                        case PRODUCTDATACENTERSERVERCORE:
                        case PRODUCTDATACENTERSERVERCOREV:
                            return "Datacenter Edition (core installation)";
                        case PRODUCTENTERPRISESERVER:
                            return "Enterprise Edition";
                        case PRODUCTENTERPRISESERVERCORE:
                        case PRODUCTENTERPRISESERVERCOREV:
                            return "Enterprise Edition (core installation)";
                        case PRODUCTENTERPRISESERVERIA64:
                            return "Enterprise Edition for Itanium-based Systems";
                        case PRODUCTSMALLBUSINESSSERVER:
                            return "Small Business Server";
                        case PRODUCTSERVERFORSMALLBUSINESS:
                        case PRODUCTSERVERFORSMALLBUSINESSV:
                            return "Windows Essential Server Solutions";
                        case PRODUCTSTANDARDSERVER:
                        case PRODUCTSTANDARDSERVERV:
                            return "Standard Edition";
                        case PRODUCTSTANDARDSERVERCORE:
                        case PRODUCTSTANDARDSERVERCOREV:
                            return "Standard Edition (core installation)";
                        case PRODUCTWEBSERVER:
                        case PRODUCTWEBSERVERCORE:
                            return "Web Server Edition";
                        case PRODUCTMEDIUMBUSINESSSERVERMANAGEMENT:
                        case PRODUCTMEDIUMBUSINESSSERVERMESSAGING:
                        case PRODUCTMEDIUMBUSINESSSERVERSECURITY:
                            return "Windows Essential Business Server ";
                        case PRODUCTSTORAGEENTERPRISESERVER:
                        case PRODUCTSTORAGESEXPRESSSERVER:
                        case PRODUCTSTORAGESTANDARDSERVER:
                        case PRODUCTSTORAGEWORKGROUPSERVER:
                            return "Storage Server";
                    }

                    break;
            }

            return string.Empty;
        }

        /// <summary>
        /// Returns the service pack information of the operating system running on this computer.
        /// </summary>
        /// <returns>A string containing the the operating system service pack information.</returns>
        public static string GetOSServicePack()
        {
            var operatingSystemVersionInfo = new OSVERSIONINFOEX
            {
                OSVersionInfoSize = Marshal.SizeOf(typeof(OSVERSIONINFOEX)),
            };
            return !NativeMethods.GetVersionEx(ref operatingSystemVersionInfo) ? string.Empty : operatingSystemVersionInfo.CSDVersion;
        }

        /// <summary>
        /// Returns the name of the operating system running on this computer.
        /// </summary>
        /// <returns>A string containing the the operating system name.</returns>
        public static string GetOSNameString()
        {
            OperatingSystem operatingInfo = Environment.OSVersion;
            string operatingSystemName = "UNKNOWN";

            switch (operatingInfo.Platform)
            {
                case PlatformID.Win32Windows:
                    {
                        switch (OSMinorVersion)
                        {
                            case 0:
                                {
                                    operatingSystemName = "Windows 95";
                                    break;
                                }

                            case 10:
                                {
                                    operatingSystemName = operatingInfo.Version.Revision.ToString(CultureInfo.InvariantCulture) == "2222A" ? "Windows 98 Second Edition" : "Windows 98";
                                    break;
                                }

                            case 90:
                                {
                                    operatingSystemName = "Windows Me";
                                    break;
                                }
                        }

                        break;
                    }

                case PlatformID.Win32NT:
                    {
                        switch (OSMajorVersion)
                        {
                            case 3:
                                {
                                    operatingSystemName = "Windows NT 3.51";
                                    break;
                                }

                            case 4:
                                {
                                    operatingSystemName = "Windows NT 4.0";
                                    break;
                                }

                            case 5:
                                {
                                    switch (OSMinorVersion)
                                    {
                                        case 0:
                                            operatingSystemName = "Windows 2000";
                                            break;
                                        case 1:
                                            operatingSystemName = "Windows XP";
                                            break;
                                        case 2:
                                            if (OSProductType == NTWORKSTATION)
                                            {
                                                operatingSystemName = "WindowsXP x64";
                                            }
                                            else
                                            {
                                                operatingSystemName = NativeMethods.GetSystemMetrics(SMSERVERR2) ? "Windows Server 2003 R2" : "Windows Server 2003";
                                            }

                                            break;
                                    }

                                    break;
                                }

                            case 6:
                                {
                                    switch (OSMinorVersion)
                                    {
                                        case 0:
                                            operatingSystemName = OSProductType == NTWORKSTATION ? "Windows Vista" : "Windows 2008";
                                            break;
                                        case 1:
                                            operatingSystemName = OSProductType == NTWORKSTATION ? "Windows 7" : "Windows 2008 R2";
                                            break;
                                        case 2:
                                            operatingSystemName = OSProductType == NTWORKSTATION ? "Windows 8" : "Windows 2012";
                                            break;
                                        case 3:
                                            operatingSystemName = OSProductType == NTWORKSTATION ? "Windows 81" : "Windows 2012";
                                            break;
                                    }

                                    break;
                                }
                        }

                        break;
                    }
            }

            if (!IsOSAsReported(OSMajorVersion, OSMinorVersion, OSBuildVersion, OSProductType, (short)OSServicePackMajor))
            {
                operatingSystemName = "Compatibilty Mode: " + operatingSystemName;
            }

            return operatingSystemName;
        }

        /// <summary>
        /// Returns the name of the operating system running on this computer.
        /// </summary>
        /// <returns>A string containing the the operating system name.</returns>
        public static OSList GetOSName()
        {
            switch (OsVersionInt())
            {
                case 51:
                    return OSList.WindowsXp;
                case 52:
                    if (OSProductType == NTWORKSTATION)
                    {
                        return OSList.WindowsXp64;
                    }

                    return NativeMethods.GetSystemMetrics(SMSERVERR2) ? OSList.Windows2003R2 : OSList.Windows2003;
                case 60:
                    return OSProductType == NTWORKSTATION ? OSList.WindowsVista : OSList.Windows2008;
                case 61:
                    return OSProductType == NTWORKSTATION ? OSList.Windows7 : OSList.Windows2008R2;
                case 62:
                    return OSProductType == NTWORKSTATION ? OSList.Windows8 : OSList.Windows2012;
                case 63:
                    return OSProductType == NTWORKSTATION ? OSList.Windows81 : OSList.Windows2012;
            }

            return OSList.Windows2000AndPrevious;
        }

        /// <summary>
        /// Return a full version string.
        /// </summary>
        /// <returns>A string representing a fully displayable version.</returns>
        public static string GetOSDisplayVersion()
        {
            string servicePack = GetOSServicePack();
            if (!string.IsNullOrEmpty(servicePack))
            {
                servicePack = " ( " + servicePack + " )";
            }

            return GetOSNameString() + servicePack + " [" + OSVersion + "]";
        }

        /// <summary>
        /// Return a value that indicate if the OS is blocked, supported, or officially unsupported.
        /// </summary>
        /// <returns>Whether or not the OS is supported.</returns>
        public static OsSupport GetOSSupported()
        {
            if (VerifyDesktopOSMinRequirement(5, 1, 2600, NTWORKSTATION, 3))
            { // XP SP3
                return OsSupport.NotSupported;
            }

            if (VerifyDesktopOSMinRequirement(6, 0, 6000, NTWORKSTATION, 2))
            { // Vista SP2
                return OsSupport.FullySupported;
            }

            if (VerifyDesktopOSMinRequirement(6, 1, 7600, NTWORKSTATION, 0))
            { // Win7 RTM
                return OsSupport.FullySupported;
            }

            if (VerifyDesktopOSMinRequirement(6, 2, 9200, NTWORKSTATION, 0))
            { // Windows 8 RTM
                return OsSupport.FullySupported;
            }

            if (VerifyDesktopOSMinRequirement(6, 3, 9431, NTWORKSTATION, 0))
            { // Windows 8.1 Preview
                return OsSupport.FullySupported;
            }

            if (IsServer())
            { // any server OS
                return OsSupport.NotSupported;
            }

            return OsSupport.Blocked;
        }

        /// <summary>
        /// Return if running on XP or later.
        /// </summary>
        /// <returns>true means XP or later.</returns>
        /// <returns>false means 2000 or previous.</returns>
        public static bool XpOrLater()
        {
            return VerifyVersionGreaterEqual(5, 1);
        }

        /// <summary>
        /// Return if running on XP 64 or later.
        /// </summary>
        /// <returns>true means XP 64 or later.</returns>
        /// <returns>false means XP or previous.</returns>
        public static bool Xp64OrLater()
        {
            return VerifyVersionGreaterEqual(5, 2);
        }

        /// <summary>
        /// Return if running on Vista or later.
        /// </summary>
        /// <returns>true means Vista or later.</returns>
        /// <returns>false means Xp or previous.</returns>
        public static bool VistaOrLater()
        {
            return VerifyVersionGreaterEqual(6, 0);
        }

        /// <summary>
        /// Return if running on Windows7 or later.
        /// </summary>
        /// <returns>true means Windows7 or later.</returns>
        /// <returns>false means Vista or previous.</returns>
        public static bool Win7OrLater()
        {
            return VerifyVersionGreaterEqual(6, 1);
        }

        /// <summary>
        /// Return if running on Windows8 or later.
        /// </summary>
        /// <returns>true means Windows8 or later.</returns>
        /// <returns>false means Win7 or previous.</returns>
        public static bool Win8OrLater()
        {
            return VerifyVersionGreaterEqual(6, 2);
        }

        /// <summary>
        /// Return a numeric value representing OS version.
        /// </summary>
        /// <returns>(OSMajorVersion * 10 + OSMinorVersion).</returns>
        public static int OsVersionInt()
        {
            return (OSMajorVersion * 10) + OSMinorVersion;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Checks if OS is later then major / minor version.
        /// </summary>
        /// <param name="majorVersion">Major OS version.</param>
        /// <param name="minorVersion">Minor OS version.</param>
        /// <returns>True if OS is later than version supplied as parameters.</returns>
        private static bool VerifyVersionGreaterEqual(int majorVersion, int minorVersion)
        {
            ulong condition = 0;
            var operatingSystemVersionInfo = new OSVERSIONINFOEX
            {
                OSVersionInfoSize = Marshal.SizeOf(typeof(OSVERSIONINFOEX)),
                MajorVersion = majorVersion,
                MinorVersion = minorVersion,
            };
            condition = NativeMethods.VerSetConditionMask(condition, VERMAJORVERSION, VERGREATEREQUAL);
            condition = NativeMethods.VerSetConditionMask(condition, VERMINORVERSION, VERGREATEREQUAL);
            return NativeMethods.VerifyVersionInfo(ref operatingSystemVersionInfo, VERMAJORVERSION | VERMINORVERSION, condition);
        }

        /// <summary>
        /// Checks OS for required service pack and build version.
        /// </summary>
        /// <param name="majorVersion">Major OS version.</param>
        /// <param name="minorVersion">Minor OS version.</param>
        /// <param name="buildVersion">OS Build Version.</param>
        /// <param name="productType">OS Product Type.</param>
        /// <param name="servicePack">Minimum Major Service PackVersion.</param>
        /// <returns>True if Major / Minor OS versions match and service pack / build version are >= parameters.</returns>
        private static bool VerifyDesktopOSMinRequirement(int majorVersion, int minorVersion, int buildVersion, byte productType, short servicePack)
        {
            ulong condition = 0;
            var operatingSystemVersionInfo = new OSVERSIONINFOEX
            {
                OSVersionInfoSize = Marshal.SizeOf(typeof(OSVERSIONINFOEX)),
                MajorVersion = majorVersion,
                MinorVersion = minorVersion,
                BuildNumber = buildVersion,
                ProductType = productType,
                ServicePackMajor = servicePack,
            };
            condition = NativeMethods.VerSetConditionMask(condition, VERMAJORVERSION, VEREQUAL);
            condition = NativeMethods.VerSetConditionMask(condition, VERMINORVERSION, VEREQUAL);
            condition = NativeMethods.VerSetConditionMask(condition, VERPRODUCTTYPE, VEREQUAL);
            condition = NativeMethods.VerSetConditionMask(condition, VERSERVICEPACKMAJOR, VERGREATEREQUAL);
            condition = NativeMethods.VerSetConditionMask(condition, VERBUILDVERSION, VERGREATEREQUAL);
            return NativeMethods.VerifyVersionInfo(ref operatingSystemVersionInfo, VERMAJORVERSION | VERMINORVERSION | VERPRODUCTTYPE | VERSERVICEPACKMAJOR | VERBUILDVERSION, condition);
        }

        /// <summary>
        /// Checks whether the OS version reported via GetVersionEx matches that of VerifyVersionInfo
        /// When running in compatibility mode GetVersionEx can return the value of the
        /// compatibility setting rather than the actual OS.
        /// </summary>
        /// <param name="majorVersion">Reported OS Major Version.</param>
        /// <param name="minorVersion">Reported OS Minor Version.</param>
        /// <param name="buildVersion">Reported OS Build Version.</param>
        /// <param name="productType">Reported OS Product Type.</param>
        /// <param name="servicePack">Reported OS Major Service Pack Version.</param>
        /// <returns>True if actual OS matches reported one.</returns>
        private static bool IsOSAsReported(int majorVersion, int minorVersion, int buildVersion, byte productType, short servicePack)
        {
            ulong condition = 0;
            var operatingSystemVersionInfo = new OSVERSIONINFOEX
            {
                OSVersionInfoSize = Marshal.SizeOf(typeof(OSVERSIONINFOEX)),
                MajorVersion = majorVersion,
                MinorVersion = minorVersion,
                BuildNumber = buildVersion,
                ProductType = productType,
                ServicePackMajor = servicePack,
            };
            condition = NativeMethods.VerSetConditionMask(condition, VERMAJORVERSION, VEREQUAL);
            condition = NativeMethods.VerSetConditionMask(condition, VERMINORVERSION, VEREQUAL);
            condition = NativeMethods.VerSetConditionMask(condition, VERPRODUCTTYPE, VEREQUAL);
            condition = NativeMethods.VerSetConditionMask(condition, VERSERVICEPACKMAJOR, VEREQUAL);
            condition = NativeMethods.VerSetConditionMask(condition, VERBUILDVERSION, VEREQUAL);
            return NativeMethods.VerifyVersionInfo(ref operatingSystemVersionInfo, VERMAJORVERSION | VERMINORVERSION | VERPRODUCTTYPE | VERSERVICEPACKMAJOR | VERBUILDVERSION, condition);
        }

        /// <summary>
        /// Identifies if OS is a Windows Server OS.
        /// </summary>
        /// <returns>True if OS is a Windows Server OS.</returns>
        private static bool IsServer()
        {
            ulong condition = 0;
            var operatingSystemVersionInfo = new OSVERSIONINFOEX
            {
                OSVersionInfoSize = Marshal.SizeOf(typeof(OSVERSIONINFOEX)),
                ProductType = NTWORKSTATION, // note the check is that this is not equal as per MS documenation
            };
            condition = NativeMethods.VerSetConditionMask(condition, VERPRODUCTTYPE, VEREQUAL);
            return !NativeMethods.VerifyVersionInfo(ref operatingSystemVersionInfo, VERPRODUCTTYPE, condition);
        }

        #endregion

        #region Structs

        /// <summary>
        /// OS Version struct.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        internal struct OSVERSIONINFOEX
        {
            /// <summary>
            /// The OS version info size.
            /// </summary>
            public int OSVersionInfoSize;

            /// <summary>
            /// The major version.
            /// </summary>
            public int MajorVersion;

            /// <summary>
            /// The minor version.
            /// </summary>
            public int MinorVersion;

            /// <summary>
            /// The build number.
            /// </summary>
            public int BuildNumber;

            /// <summary>
            /// The platform id.
            /// </summary>
            // ReSharper disable once MemberCanBePrivate.Global
            public int PlatformId;

            /// <summary>
            /// The CSD version.
            /// </summary>
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string CSDVersion;

            /// <summary>
            /// The service pack major number.
            /// </summary>
            public short ServicePackMajor;

            /// <summary>
            /// The service pack minor version.
            /// </summary>
            // ReSharper disable once FieldCanBeMadeReadOnly.Global
            public short ServicePackMinor;

            /// <summary>
            /// The suite mask.
            /// </summary>
            // ReSharper disable once FieldCanBeMadeReadOnly.Global
            public short SuiteMask;

            /// <summary>
            /// The product type.
            /// </summary>
            public byte ProductType;

            /// <summary>
            /// Reserved byte.
            /// </summary>
            // ReSharper disable once FieldCanBeMadeReadOnly.Global
            public byte Reserved;
        }

        #endregion
    }
}