﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tykma.Custom.PLC.Simulator
{
    using System.Runtime.CompilerServices;

    public partial class PLCForm : Form, INotifyPropertyChanged
    {

        public string Connecting
        {
            get
            {
                return this.IsConnectingLabel.Text;
            }
            set
            {
                this.InvokeIfRequired(
                    () =>
                        {
                            this.IsConnectingLabel.Text = value;
                        });
               
            }
        }

        public string Diameter
        {
            get
            {
                return this.diameter.Text;
            }

            set
            {
                this.diameter.Text = value;
            }
        }

        public string IP
        {
            get
            {
                return this.IPLabel.Text;
            }

            set
            {
                this.IPLabel.Text = value;
            }
        }

        public bool StartRequest
        {
            get
            {
                return this.cbStartRequest.Checked;
            }

            set
            {
                this.cbStartRequest.Checked = value;
            }
        }

        public bool DataReady
        {
            get
            {
                return this.cbDataReady.Checked;
            }

            set
            {
                this.cbDataReady.Checked = value;
                this.OnPropertyChanged();
            }
        }

        public bool StartSequence
        {
            get
            {
                return this.startSeq.Checked;
            }

            set
            {
                this.startSeq.Checked = value;
                this.OnPropertyChanged();
            }
        }

        public PLCForm()
        {
            InitializeComponent();
            
        }

        private void cbDataReady_CheckedChanged(object sender, EventArgs e)
        {
            this.DataReady = this.cbDataReady.Checked;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void startSeq_CheckedChanged(object sender, EventArgs e)
        {
            this.StartSequence = this.startSeq.Checked;
        }
    }
}
