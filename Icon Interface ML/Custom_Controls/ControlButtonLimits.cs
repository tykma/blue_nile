﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlButtonLimits.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    using System.ComponentModel;
    using System.Drawing;

    using Icon_Interface.Properties;

    /// <summary>
    /// The control button for limits.
    /// </summary>
    public class ControlButtonLimits : ControlButtonBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlButtonLimits"/> class.
        /// </summary>
        public ControlButtonLimits()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initialize component.
        /// </summary>
        private void InitializeComponent()
        {
            ((ISupportInitialize)this.ux_Image_Icon).BeginInit();
            this.SuspendLayout();

            // ux_Image_Icon
            this.ux_Image_Icon.Image = ImageResources.limits1;

            // ux_Label_Action
            this.ux_Label_Action.Text = Resources.Main_Limits;

            // ControlButtonLimits
            this.AutoScaleDimensions = new SizeF(6F, 13F);
            this.Name = "ControlButtonLimits";
            ((ISupportInitialize)this.ux_Image_Icon).EndInit();
            this.ResumeLayout(false);
        }
    }
}
