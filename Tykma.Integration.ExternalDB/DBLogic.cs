﻿// <copyright file="DBLogic.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Integration.ExternalDB
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;

    using Tykma.Integration.SQL;

    /// <summary>
    /// DB logic.
    /// </summary>
    public class DBLogic : IDBLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DBLogic"/> class.
        /// </summary>
        /// <param name="connString">The connection string.</param>
        /// <param name="providerName">The provider name.</param>
        /// <param name="primKey">Primary column.</param>
        /// <param name="templateColumn">Template column.</param>
        /// <param name="table">Table to look at.</param>
        /// <param name="useTop">Use TOP instead of LIMIT syntax.</param>
        [ExcludeFromCodeCoverage]
        public DBLogic(string connString, string providerName, string primKey, string templateColumn, string table, bool useTop)
            : this(new DatabaseUtil(connString, providerName), primKey, templateColumn, table, useTop)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DBLogic"/> class.
        /// </summary>
        /// <param name="dbUtil">The database utility class.</param>
        /// <param name="primColumn">The primary column name.</param>
        /// <param name="templateColumn">Template column.</param>
        /// <param name="table">Table to look at.</param>
        /// <param name="useTop">Use TOP instead of LIMIT syntax.</param>
        public DBLogic(IDatabaseUtil dbUtil, string primColumn, string templateColumn, string table, bool useTop)
        {
            this.DatabaseUtility = dbUtil;
            this.PrimaryColumn = primColumn;
            this.TemplateColumn = templateColumn;
            this.Table = table;
            this.UseTop = useTop;
        }

        /// <summary>
        /// Gets the database utility.
        /// </summary>
        private IDatabaseUtil DatabaseUtility { get; }

        private string PrimaryColumn { get; }

        private string TemplateColumn { get; }

        private string Table { get; }

        private bool UseTop { get; }

        /// <inheritdoc />
        public DBData GetData(string value)
        {
            //var builder = new SqlCommandBuilder();
            //string escapedTableName = builder.QuoteIdentifier(this.Table);
            //string escapedPrimaryField = builder.QuoteIdentifier(this.PrimaryColumn);

            string query;

            if (this.UseTop)
            {
                query = $"Select TOP 1 * FROM {this.Table} WHERE {this.PrimaryColumn} LIKE @SEARCHVALUE";
            }
            else
            {
                query = $"Select * FROM {this.Table} WHERE {this.PrimaryColumn} LIKE {value}";
            }

            var dict = new Dictionary<string, object> { { "SEARCHVALUE", value } };

            var dt = this.DatabaseUtility.Select(query, dict);

            string templatename = null;
            var rDict = new Dictionary<string, string>();

            if (dt?.Rows.Count > 0)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.Equals(this.TemplateColumn))
                    {
                        templatename = dt.Rows[0][col.ColumnName].ToString();
                    }
                    else
                    {
                        if (!rDict.ContainsKey(col.ColumnName))
                        {
                            rDict.Add(col.ColumnName, dt.Rows[0][col.ColumnName].ToString());
                        }
                    }
                }
            }

            return new DBData(templatename, rDict);
        }
    }
}
