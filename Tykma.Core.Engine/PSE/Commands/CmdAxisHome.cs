﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdAxisHome.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// The command to set axis home.
    /// </summary>
    internal class CmdAxisHome : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdAxisHome"/> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking API.</param>
        /// <param name="axis">The axis.</param>
        public CmdAxisHome(IntPtr lmcApi, int axis)
            : base(lmcApi)
        {
            this.AxisToHome = axis;
        }

        /// <summary>
        /// Home the axis delegate.
        /// </summary>
        /// <param name="axis">The axis.</param>
        /// <returns>Return code.</returns>
        private delegate int LMC1ApiAxisCorrectOrigin(int axis);

        /// <summary>
        /// Gets the axis to home.
        /// </summary>
        /// <value>
        /// The axis to home.
        /// </value>
        private int AxisToHome { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        internal bool Execute()
        {
            var lmc1AxisCorrectOrigin = this.LoadFunction<LMC1ApiAxisCorrectOrigin>("lmc1_AxisCorrectOrigin");

            if (lmc1AxisCorrectOrigin == null)
            {
                throw new EngineNotConnectedException();
            }

            int result = 0;

            for (int i = 0; i < 2; i++)
            {
                result = lmc1AxisCorrectOrigin(this.AxisToHome);

                if (result == 0)
                {
                    break;
                }
            }

            var rm = this.GetMiniLaseReturnCode(result);
            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(this.GetMiniLaseReturnCode(result).ReturnDescription);
        }
    }
}
