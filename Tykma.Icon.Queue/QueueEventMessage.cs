﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QueueEventMessage.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.Queue
{
    /// <summary>
    /// Status message to pass to events.
    /// </summary>
    public class QueueEventMessage
    {
        /// <summary>
        /// Gets or sets a value indicating whether [jobs are available].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [jobs available]; otherwise, <c>false</c>.
        /// </value>
        public bool JobsAvailable { get; set; }
    }
}
