﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Nudger.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Form which houses nudger controls.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    using Icon_Interface.General;
    using Icon_Interface.Nudger;
    using Icon_Interface.Properties;

    /// <summary>
    /// Form which houses nudging controls.
    /// </summary>
    public partial class Nudger : Form
    {
        /// <summary>
        /// The timer for moving.
        /// </summary>
        private readonly Timer timerMove = new Timer();

        /// <summary>
        /// The nudge image resources.
        /// </summary>
        private readonly NudgerImages nudgeImg = new NudgerImages();

        /// <summary>
        /// Initializes a new instance of the <see cref="Nudger"/> class.
        /// </summary>
        internal Nudger()
        {
            this.InitializeComponent();
            this.timerMove.Interval = 25;
            this.timerMove.Tick += this.TimerMoveTick;
        }

        /// <summary>
        /// Occurs when a nudge event happens.
        /// </summary>
        internal event EventHandler<Nudgement> NudgeEvent;

        /// <summary>
        /// Occurs when a nudge stops
        /// </summary>
        internal event EventHandler<bool> NudgeStopEvent;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
                if (this.nudgeImg != null)
                {
                    this.nudgeImg.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Handles the Tick event of the TimerMove control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void TimerMoveTick(object sender, EventArgs e)
        {
           var nudge = (Nudgement)((Timer)sender).Tag;

            // ReSharper disable once ArrangeStaticMemberQualifier
           if (Control.ModifierKeys == Keys.Shift)
            {
                nudge.NudgeDistance = TykmaGlobals.INISettings.NUDGEDISTANCE * 5;
            }
            else
            {
                nudge.NudgeDistance = TykmaGlobals.INISettings.NUDGEDISTANCE;
            }

           this.UpdateListenersOfStatus(nudge);
        }

        /// <summary>
        /// Updates the listeners of nudge status.
        /// </summary>
        /// <param name="nudge">The nudge.</param>
        private void UpdateListenersOfStatus(Nudgement nudge)
        {
             if (this.NudgeEvent != null)
             {
                 this.NudgeEvent(this, nudge);
             }
        }

        /// <summary>
        /// Adjustments the stopped.
        /// </summary>
        private void AdjustmentStopped()
        {
            this.ux_Pic_NudgeUp.Image = this.nudgeImg.ArrowUp;
            this.ux_Pic_NudgeDown.Image = this.nudgeImg.ArrowDown;
            this.ux_Pic_NudgeLeft.Image = this.nudgeImg.ArrowLeft;
            this.ux_Pic_NudgeRight.Image = this.nudgeImg.ArrowRight;
            this.ux_Pic_NudgeCW.Image = this.nudgeImg.ArrowClockWise;
            this.ux_Pic_NudgeCCW.Image = this.nudgeImg.ArrowCounterClockwise;

            this.timerMove.Stop();
            if (this.NudgeStopEvent != null)
            {
                this.NudgeStopEvent(this, true);
            }
        }

        /// <summary>
        /// Handles the Load event of the nudging control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void NudgerLoad(object sender, EventArgs e)
        {
            // ReSharper disable once ArrangeStaticMemberQualifier
            if ((Control.ModifierKeys & Keys.Shift) == 0)
            {
                string initLocation = Settings.Default.InitialLocationNudger;
                var il = new Point(0, 0);
                Size sz = this.Size;
                if (!string.IsNullOrWhiteSpace(initLocation))
                {
                    string[] parts = initLocation.Split(',');
                    if (parts.Length >= 2)
                    {
                        il = new Point(int.Parse(parts[0]), int.Parse(parts[1]));
                    }

                    if (parts.Length >= 4)
                    {
                        sz = new Size(int.Parse(parts[2]), int.Parse(parts[3]));
                    }
                }

                this.Size = sz;
                this.Location = il;
                this.SetAllColors();
            }
        }

        /// <summary>
        /// Handles the FormClosing event of the nudging control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="FormClosingEventArgs"/> instance containing the event data.</param>
        private void NudgerFormClosing(object sender, FormClosingEventArgs e)
        {
            // ReSharper disable once ArrangeStaticMemberQualifier
            if ((Control.ModifierKeys & Keys.Shift) == 0)
            {
                Point location = this.Location;
                Size size = this.Size;
                if (this.WindowState != FormWindowState.Normal)
                {
                    location = this.RestoreBounds.Location;
                    size = this.RestoreBounds.Size;
                }

                string initLocation = string.Join(",", location.X, location.Y, size.Width, size.Height);
                Settings.Default.InitialLocationNudger = initLocation;
                Settings.Default.Save();
            }

            e.Cancel = true;
        }

        /// <summary>
        /// Sets all colors.
        /// </summary>
        private void SetAllColors()
        {
            this.BackColor = Settings.Default.BackColor;
            this.ForeColor = Settings.Default.ForeColor;
        }

        /// <summary>
        /// Handles the MouseDown event of the Pic_NudgeRight control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void PicNudgeRightMouseDown(object sender, MouseEventArgs e)
        {
            this.StartMoveRight();
        }

        /// <summary>
        /// Handles the MouseDown event of the Pic_NudgeDown control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void PicNudgeDownMouseDown(object sender, MouseEventArgs e)
        {
            this.StartMoveDown();
        }

        /// <summary>
        /// Handles the MouseDown event of the Pic_NudgeLeft control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void PicNudgeLeftMouseDown(object sender, MouseEventArgs e)
        {
            this.StartMoveLeft();
        }

        /// <summary>
        /// Handles the MouseDown event of the Pic_NudgeUp control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void PicNudgeUpMouseDown(object sender, MouseEventArgs e)
        {
            this.StartMoveUp();
        }

        /// <summary>
        /// Handles the MouseDown event of the Pic_NudgeCCW control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void PicNudgeCcwMouseDown(object sender, MouseEventArgs e)
        {
            this.StartMoveCounterClockWise();
        }

        /// <summary>
        /// Handles the MouseDown event of the Pic_NudgeCW control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void PicNudgeCwMouseDown(object sender, MouseEventArgs e)
        {
            this.StartMoveClockWise();
        }

        /// <summary>
        /// Handles the MouseUp event of the Pic_NudgeRight control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void PicNudgeRightMouseUp(object sender, MouseEventArgs e)
        {
            this.ux_Pic_NudgeRight.Image = this.nudgeImg.ArrowRight;
            this.AdjustmentStopped();
        }

        /// <summary>
        /// Handles the MouseUp event of the Pic_NudgeDown control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void PicNudgeDownMouseUp(object sender, MouseEventArgs e)
        {
            this.ux_Pic_NudgeDown.Image = this.nudgeImg.ArrowDown;
            this.AdjustmentStopped();
        }

        /// <summary>
        /// Handles the MouseUp event of the Pic_NudgeLeft control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void PicNudgeLeftMouseUp(object sender, MouseEventArgs e)
        {
            this.AdjustmentStopped();
        }

        /// <summary>
        /// Handles the MouseUp event of the Pic_NudgeUp control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void PicNudgeUpMouseUp(object sender, MouseEventArgs e)
        {
            this.AdjustmentStopped();
        }

        /// <summary>
        /// Handles the MouseUp event of the Pic_NudgeCCW control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void PicNudgeCcwMouseUp(object sender, MouseEventArgs e)
        {
            this.AdjustmentStopped();
        }

        /// <summary>
        /// Handles the MouseUp event of the Pic_NudgeCW control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void PicNudgeCwMouseUp(object sender, MouseEventArgs e)
        {
            this.AdjustmentStopped();
        }

        /// <summary>
        /// Handles the KeyDown event of the Nudge control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void NudgerKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    this.StartMoveLeft();
                    break;
                case Keys.Right:
                    this.StartMoveRight();
                    break;
                case Keys.Up:
                    this.StartMoveUp();
                    break;
                case Keys.Down:
                    this.StartMoveDown();
                    break;
                case Keys.PageUp:
                    this.StartMoveCounterClockWise();
                    break;
                case Keys.PageDown:
                    this.StartMoveClockWise();
                    break;
            }
        }

        /// <summary>
        /// Handles the KeyUp event of the Nudge control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void NudgerKeyUp(object sender, KeyEventArgs e)
        {
            if (this.timerMove.Enabled && e.KeyCode != Keys.ShiftKey)
            {
                this.AdjustmentStopped();
            }
        }

        /// <summary>
        /// Starts the move clock wise.
        /// </summary>
        private void StartMoveClockWise()
        {
            this.ux_Pic_NudgeCW.Image = this.nudgeImg.ArrowActivatedClockWise;
            this.timerMove.Tag = new Nudgement(Nudgement.NudgeType.ClockWise, TykmaGlobals.INISettings.NUDGEDISTANCE);
            this.timerMove.Start();
        }

        /// <summary>
        /// Starts the move counter clock wise.
        /// </summary>
        private void StartMoveCounterClockWise()
        {
            this.ux_Pic_NudgeCCW.Image = this.nudgeImg.ArrowActivatedCounterClockwise;
            this.timerMove.Tag = new Nudgement(Nudgement.NudgeType.CounterClockwise, TykmaGlobals.INISettings.NUDGEDISTANCE);
            this.timerMove.Start();
        }

        /// <summary>
        /// Starts the move up.
        /// </summary>
        private void StartMoveUp()
        {
            this.ux_Pic_NudgeUp.Image = this.nudgeImg.ArrowActivatedUp;
            this.timerMove.Tag = new Nudgement(Nudgement.NudgeType.Up, TykmaGlobals.INISettings.NUDGEDISTANCE);
            this.timerMove.Start();
        }

        /// <summary>
        /// Starts the move down.
        /// </summary>
        private void StartMoveDown()
        {
            this.ux_Pic_NudgeDown.Image = this.nudgeImg.ArrowActivatedDown;
            this.timerMove.Tag = new Nudgement(Nudgement.NudgeType.Down, TykmaGlobals.INISettings.NUDGEDISTANCE);
            this.timerMove.Start();
        }

        /// <summary>
        /// Starts the move left.
        /// </summary>
        private void StartMoveLeft()
        {
            this.ux_Pic_NudgeLeft.Image = this.nudgeImg.ArrowActivatedLeft;
            this.timerMove.Tag = new Nudgement(Nudgement.NudgeType.Left, TykmaGlobals.INISettings.NUDGEDISTANCE);
            this.timerMove.Start();
        }

        /// <summary>
        /// Starts the move right.
        /// </summary>
        private void StartMoveRight()
        {
            this.ux_Pic_NudgeRight.Image = this.nudgeImg.ArrowActivatedRight;
            this.timerMove.Tag = new Nudgement(Nudgement.NudgeType.Right, TykmaGlobals.INISettings.NUDGEDISTANCE);
            this.timerMove.Start();
        }
    }
}
