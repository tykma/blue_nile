﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ElectroxJob.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.ElectroxEngine
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using Tykma.Core.Entity;

    /// <summary>
    /// The electrox job.
    /// </summary>
    public class ElectroxJob
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ElectroxJob"/> class.
        /// </summary>
        /// <param name="fullJob">The full job.</param>
        public ElectroxJob(List<string> fullJob)
        {
            this.FullJob = fullJob;
        }

        /// <summary>
        /// Gets the full job.
        /// </summary>
        /// <value>
        /// The full job.
        /// </value>
        public List<string> FullJob { get; }

        /// <summary>
        /// Gets the job name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get
            {
                string jobname = string.Empty;
                foreach (var lines in this.FullJob)
                {
                    if (lines.StartsWith("Name "))
                    {
                        jobname = lines.Split(' ')[1];
                    }
                }

                return jobname;
            }
        }

        /// <summary>
        /// Gets all editable objects.
        /// </summary>
        /// <returns>List of editable objects.</returns>
        public IEnumerable<EntityInformation> GetAllEditableObjects()
        {
            var listofEntities = new List<EntityInformation>();

            bool idFound = false;
            string idName = string.Empty;

            foreach (var line in this.FullJob)
            {
                if (line.StartsWith("LABEL "))
                {
                    idFound = true;
                    idName = line.Split(' ')[1];
                }
                else
                {
                    if (line.StartsWith("STRING "))
                    {
                        if (idFound)
                        {
                            listofEntities.Add(new EntityInformation { Name = idName, Value = line.Split(' ')[1] });
                        }

                        idFound = false;
                    }
                }
            }

            return listofEntities;
        }

        /// <summary>
        /// Gets the data from object.
        /// </summary>
        /// <param name="objectName">Name of the object.</param>
        /// <returns>Value of an object.</returns>
        public string GetDataFromObject(string objectName)
        {
            EntityInformation match = this.GetAllEditableObjects().FirstOrDefault(p => p.Name == objectName);
            return match == null ? string.Empty : match.Value;
        }

        /// <summary>
        /// Saves the file.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>Success state.</returns>
        public bool SaveFile(FileInfo filename)
        {
            if (this.FullJob.Count == 0)
            {
                return false;
            }

            File.WriteAllLines(filename.FullName, this.FullJob);
            return true;
        }

        /// <summary>
        /// Changes the data of an object.
        /// </summary>
        /// <param name="objectName">Name of the object.</param>
        /// <param name="newData">The new data.</param>
        /// <returns>Success state.</returns>
        public bool ChangeDataObject(string objectName, string newData)
        {
            var requestedObjectExists = this.GetAllEditableObjects().Any(entity => entity.Name == objectName);

            if (requestedObjectExists)
            {
                for (int i = 0; i < this.FullJob.Count; i++)
                {
                    if (this.FullJob[i].StartsWith(string.Format("LABEL {0}", objectName)))
                    {
                        for (int c = i + 1; c < this.FullJob.Count; c++)
                        {
                            if (this.FullJob[c].StartsWith("LABEL "))
                            {
                                return false;
                            }
                            else
                            {
                                if (this.FullJob[c].StartsWith("STRING "))
                                {
                                    this.FullJob[c] = string.Format("STRING {0}", newData);
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                return false;
            }

            return false;
        }
    }
}