﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputMask.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   IO masks.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.InputParse
{
    using System;

    /// <summary>
    /// IO masks.
    /// </summary>
    // ReSharper disable once ClassNeverInstantiated.Global
    public class InputMask
    {
        /// <summary>
        /// Regular I/O Masks.
        /// </summary>
        [Flags]
        public enum IOMasks
        {
            /// <summary>
            /// I/O 1.
            /// </summary>
            Input0 = 1,

            /// <summary>
            /// I/O 2
            /// </summary>
            Input1 = 2,

            /// <summary>
            /// I/O 3
            /// </summary>
            Input2 = 4,

            /// <summary>
            /// I/O 4
            /// </summary>
            Input3 = 8,

            /// <summary>
            /// I/O 5
            /// </summary>
            Input4 = 16,

            /// <summary>
            /// I/O 6
            /// </summary>
            Input5 = 32,

            /// <summary>
            /// I/O 7
            /// </summary>
            Input6 = 64,

            /// <summary>
            /// I/O 8
            /// </summary>
            Input7 = 128,

            /// <summary>
            /// I/O 9
            /// </summary>
            Input8 = 256,

            /// <summary>
            /// I/O 10
            /// </summary>
            Input9 = 512,

            /// <summary>
            /// I/O 11
            /// </summary>
            Input10 = 1024,

            /// <summary>
            /// I/O 12
            /// </summary>
            Input11 = 2048,

            /// <summary>
            /// I/O 13
            /// </summary>
            Input12 = 4096,

            /// <summary>
            /// I/O 14
            /// </summary>
            Input13 = 8192,

            /// <summary>
            /// I/O 15
            /// </summary>
            Input14 = 16384,

            /// <summary>
            /// I/O 16
            /// </summary>
            Input15 = 32768,
        }
    }
}
