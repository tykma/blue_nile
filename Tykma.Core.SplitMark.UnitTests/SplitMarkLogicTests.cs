﻿// <copyright file="SplitMarkLogicTests.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.SplitMark.UnitTests
{
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Core.Entity;
    using Tykma.Core.SplitMark;

    /// <summary>
    /// Tests for the splitmark (rotary) logic class.
    /// </summary>
    [TestClass]
    public class SplitMarkLogicTests
    {
        /// <summary>
        /// Test getting a list of objects.
        /// </summary>
        [TestMethod]
        public void TestGettingListOfObjects()
        {
            var l = new List<EntityInformation>();

            l.Add(new EntityInformation(0, string.Empty, string.Empty, -50, -25, -10, 10, 0));
            l.Add(new EntityInformation(0, string.Empty, string.Empty, -25, 0, -10, 10, 0));
            l.Add(new EntityInformation(0, string.Empty, string.Empty, 0, 25, -10, 10, 0));
            l.Add(new EntityInformation(0, string.Empty, string.Empty, 25, 50, -10, 10, 0));

            var objs = new SplitMarkLogic().GetSplitObjects(20, 31.83, l);

            Assert.AreEqual(5, objs.Count);

            Assert.AreEqual(-50, objs[0].SplitBox.X1);
            Assert.AreEqual(-30, objs[0].SplitBox.X2);
            Assert.AreEqual(-10, objs[0].SplitBox.Y1);
            Assert.AreEqual(10, objs[0].SplitBox.Y2);

            Assert.AreEqual(-30, objs[1].SplitBox.X1);
            Assert.AreEqual(-10, objs[1].SplitBox.X2);
            Assert.AreEqual(-10, objs[1].SplitBox.Y1);
            Assert.AreEqual(10, objs[1].SplitBox.Y2);

            Assert.AreEqual(-10, objs[2].SplitBox.X1);
            Assert.AreEqual(10, objs[2].SplitBox.X2);
            Assert.AreEqual(-10, objs[2].SplitBox.Y1);
            Assert.AreEqual(10, objs[2].SplitBox.Y2);

            Assert.AreEqual(10, objs[3].SplitBox.X1);
            Assert.AreEqual(30, objs[3].SplitBox.X2);
            Assert.AreEqual(-10, objs[3].SplitBox.Y1);
            Assert.AreEqual(10, objs[3].SplitBox.Y2);

            Assert.AreEqual(30, objs[4].SplitBox.X1);
            Assert.AreEqual(50, objs[4].SplitBox.X2);
            Assert.AreEqual(-10, objs[4].SplitBox.Y1);
            Assert.AreEqual(10, objs[4].SplitBox.Y2);

            Assert.AreEqual(72, objs[1].Degree, .01);
        }
    }
}