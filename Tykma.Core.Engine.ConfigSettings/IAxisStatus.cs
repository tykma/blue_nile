﻿// <copyright file="IAxisStatus.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.Engine.ConfigSettings
{
    using System.Collections.Generic;

    /// <summary>
    /// Axis status interface.
    /// </summary>
    public interface IAxisStatus
    {
        /// <summary>
        /// Determines whether specified axis is enabled.
        /// </summary>
        /// <param name="whichAxis">Which axis.</param>
        /// <returns>Whether or not the axis is enabled.</returns>
        bool IsAxisOn(int whichAxis);

        /// <summary>
        /// Set an axis homing speed.
        /// </summary>
        /// <param name="whichAxis">Axis to configure.</param>
        /// <param name="speed">The homing speed.</param>
        /// <returns>New configuration.</returns>
        List<string> SetAxisHomeSpeed(int whichAxis, int speed);

        /// <summary>
        /// Toggle an axis as enabled/disabled.
        /// </summary>
        /// <param name="whichAxis">The axis to configure.</param>
        /// <param name="enabled">Enable or disable the axis.</param>
        /// <returns>The new configuration file.</returns>
        List<string> ToggleAxisEnabled(int whichAxis, bool enabled);

        /// <summary>
        /// Sets the axis homing time out interval.
        /// </summary>
        /// <param name="whichAxis">Axis to set.</param>
        /// <param name="timeout">The timeout in ms.</param>
        /// <returns>The new configuration file.</returns>
        IList<string> SetHomeTimeOut(int whichAxis, int timeout);

        /// <summary>
        /// Get the axis homing time out interval.
        /// </summary>
        /// <param name="whichAxis">Axist o look at.</param>
        /// <returns>The homing time out interval.</returns>
        int GetAxisTimeOut(int whichAxis);

        /// <summary>
        /// Get the home speed of a specified axis.
        /// </summary>
        /// <param name="whichAxis">The axis to get the speed from.</param>
        /// <returns>Homing speed.</returns>
        int GetAxisHomeSpeed(int whichAxis);

        /// <summary>
        /// Set axis minimum position.
        /// </summary>
        /// <param name="whichAxis">The axis.</param>
        /// <param name="min">Minimum position.</param>
        /// <returns>New configuration.</returns>
        IList<string> SetAxisMinPosition(int whichAxis, int min);

        /// <summary>
        /// Set the axis maximum position.
        /// </summary>
        /// <param name="whichAxis">The axis.</param>
        /// <param name="max">Maxium position.</param>
        /// <returns>New configuration.</returns>
        IList<string> SetAxisMaximumPosition(int whichAxis, int max);

        /// <summary>
        /// Get the axis minimum position.
        /// </summary>
        /// <param name="whichAxis">The axis.</param>
        /// <returns>Minimum position,.</returns>
        int GetAxisMinimumPosition(int whichAxis);

        /// <summary>
        /// Get the axis maximum position.
        /// </summary>
        /// <param name="whichAxis">The axis.</param>
        /// <returns>Maximum position.</returns>
        int GetAxisMaximumPosition(int whichAxis);

        /// <summary>
        /// Gets the axis pulses per round value.
        /// </summary>
        /// <param name="whichAxis">Axis.</param>
        /// <returns>The pulses per round.</returns>
        int GetPulsesPerRound(int whichAxis);

        /// <summary>
        /// Gets the axis distance per round value.
        /// </summary>
        /// <param name="whichAxis">Axis.</param>
        /// <returns>Distance per round.</returns>
        int GetDistancePerRound(int whichAxis);
    }
}
