﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PenParameter.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE
{
    /// <summary>
    /// Pen parameter class.
    /// </summary>
    public class PenParameter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PenParameter" /> class.
        /// </summary>
        public PenParameter()
        {
            this.PolyTC = 0;
            this.FlySpeed = 0;
            this.PulsePointMode = false;
            this.PointTime = 0;
            this.AcccelerationDistance = 0;
            this.EndCompensate = 0;
            this.JumpDistanceTC = 0;
            this.JumpPositionTC = 0;
            this.EndTC = 0;
            this.LaserOffTC = 0;
            this.StartTC = 0;
            this.QPulseWidth = 0;
            this.Frequency = 0;
            this.Current = 0;
            this.PowerRatio = 0;
            this.MarkLoop = 0;
            this.MarkSpeed = 0;
            this.PenNumber = 0;
            this.PulseCount = 0;
        }

        /// <summary>
        /// Gets or sets the pen number.
        /// </summary>
        /// <value>
        /// The pen number.
        /// </value>
        internal int PenNumber { get; set; }

        /// <summary>
        /// Gets or sets the mark loop.
        /// </summary>
        /// <value>
        /// The mark loop.
        /// </value>
        internal int MarkLoop { get; set; }

        /// <summary>
        /// Gets or sets the mark speed.
        /// </summary>
        /// <value>
        /// The mark speed.
        /// </value>
        internal double MarkSpeed { get; set; }

        /// <summary>
        /// Gets or sets the power ratio.
        /// </summary>
        /// <value>
        /// The power ratio.
        /// </value>
        internal double PowerRatio { get; set; }

        /// <summary>
        /// Gets or sets the current.
        /// </summary>
        /// <value>
        /// The current.
        /// </value>
        internal double Current { get; set; }

        /// <summary>
        /// Gets or sets the frequency.
        /// </summary>
        /// <value>
        /// The frequency.
        /// </value>
        internal short Frequency { get; set; }

        /// <summary>
        /// Gets or sets the width of the Q pulse.
        /// </summary>
        /// <value>
        /// The width of the Q pulse.
        /// </value>
        internal int QPulseWidth { get; set; }

        /// <summary>
        /// Gets or sets the start TC.
        /// </summary>
        /// <value>
        /// The start TC.
        /// </value>
        internal int StartTC { get; set; }

        /// <summary>
        /// Gets or sets the laser off TC.
        /// </summary>
        /// <value>
        /// The laser off TC.
        /// </value>
        internal int LaserOffTC { get; set; }

        /// <summary>
        /// Gets or sets the end TC.
        /// </summary>
        /// <value>
        /// The end TC.
        /// </value>
        internal int EndTC { get; set; }

        /// <summary>
        /// Gets or sets the poly TC.
        /// </summary>
        /// <value>
        /// The poly TC.
        /// </value>
        internal int PolyTC { get; set; }

        /// <summary>
        /// Gets or sets the jump speed.
        /// </summary>
        /// <value>
        /// The jump speed.
        /// </value>
        internal double JumpSpeed { get; set; }

        /// <summary>
        /// Gets or sets the jump position TC.
        /// </summary>
        /// <value>
        /// The jump position TC.
        /// </value>
        internal int JumpPositionTC { get; set; }

        /// <summary>
        /// Gets or sets the jump distance TC.
        /// </summary>
        /// <value>
        /// The jump distance TC.
        /// </value>
        internal int JumpDistanceTC { get; set; }

        /// <summary>
        /// Gets or sets the end compensation.
        /// </summary>
        /// <value>
        /// The end complete.
        /// </value>
        internal double EndCompensate { get; set; }

        /// <summary>
        /// Gets or sets the acceleration distance.
        /// </summary>
        /// <value>
        /// The acceleration distance.
        /// </value>
        internal double AcccelerationDistance { get; set; }

        /// <summary>
        /// Gets or sets the point time.
        /// </summary>
        /// <value>
        /// The point time.
        /// </value>
        internal double PointTime { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [pulse point mode].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [pulse point mode]; otherwise, <c>false</c>.
        /// </value>
        internal bool PulsePointMode { get; set; }

        /// <summary>
        /// Gets or sets the fly speed.
        /// </summary>
        /// <value>
        /// The fly speed.
        /// </value>
        internal double FlySpeed { get; set; }

        /// <summary>
        /// Gets or sets the pulse count.
        /// </summary>
        /// <value>
        /// The pulse count.
        /// </value>
        internal int PulseCount { get; set; }
    }
}