﻿// <copyright file="ExcelCX.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.SQL.Client
{
    using System;
    using ClosedXML.Excel;

    /// <summary>
    /// Excel database retrieval class.
    /// </summary>
    public class ExcelCX : ISqlClient
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExcelCX"/> class.
        /// </summary>
        /// <param name="path">Excel file path.</param>
        /// <param name="worksheet">Worksheet.</param>
        /// <param name="partNumber">The primary key.</param>
        /// <param name="jobColumn">The template key.</param>
        /// <param name="qtyColumn">The quantity column.</param>
        public ExcelCX(string path, string worksheet, string partNumber, string jobColumn, string qtyColumn)
        {
            this.Path = path;
            this.WorkSheet = worksheet;
            this.PartNumber = partNumber;
            this.JobColumn = jobColumn;
            this.QuantityColumn = qtyColumn;
        }

        private string Path { get; }

        private string WorkSheet { get; }

        private string PartNumber { get; }

        private string JobColumn { get; }

        private string QuantityColumn { get; }

        /// <inheritdoc />
        public SQLRetrievedData GetData(string id)
        {
            var returnSqlData = new SQLRetrievedData();

            XLWorkbook wb;
            IXLWorksheet ws;

            try
            {
                wb = new XLWorkbook(this.Path);
                ws = wb.Worksheet(this.WorkSheet);
            }
            catch (Exception err)
            {
                returnSqlData.SetFault(err.Message);
                return returnSqlData;
            }

            var firstPossibleAddress = ws.FirstRowUsed().FirstCell().Address;

            var lastPossibleAddress = ws.RangeUsed().LastCellUsed().Address;

            bool found = false;

            foreach (var row in ws.Rows())
            {
                foreach (var cell in row.Cells())
                {
                    if (cell.Value.Equals(this.PartNumber))
                    {
                        firstPossibleAddress = cell.Address;
                        found = true;
                        break;
                    }
                }

                if (found)
                {
                    break;
                }
            }

            var range = ws.Range(firstPossibleAddress, lastPossibleAddress).RangeUsed().AsTable();

            var h = range.DataRange.Table.HeadersRow();

            string partNumberCol = string.Empty;

            bool foundPartNumberCol = false;
            foreach (var item in h.Cells())
            {
                if (item.GetString().Equals(this.PartNumber, StringComparison.InvariantCultureIgnoreCase))
                {
                    partNumberCol = item.GetString();
                    foundPartNumberCol = true;
                    break;
                }
            }

            if (!foundPartNumberCol)
            {
                returnSqlData.SetFault($"Could not find column: {this.PartNumber}");
                return returnSqlData;
            }

            foreach (var row in range.DataRange.RowsUsed())
            {
                if (row.Field(partNumberCol).Value.ToString().Equals(id))
                {
                    foreach (var f in range.Fields)
                    {
                        var obj = row.Field(f.Name);

                        if (f.Name.Equals(this.JobColumn))
                        {
                            returnSqlData.SetSQLJob(obj.Value.ToString());
                        }
                        else if (f.Name.Equals(this.QuantityColumn))
                        {
                            returnSqlData.SetQuantity(obj.GetValue<string>());
                        }
                        else
                        {
                            returnSqlData.AddItemToList(f.Name.TrimEnd(), obj.GetFormattedString());
                        }
                    }

                    return returnSqlData;
                }
            }

            returnSqlData.SetFault($"{this.PartNumber}: {id} was not found.");

            return returnSqlData;
        }

        /// <inheritdoc />
        public bool SetData(string primkey, string data, string column)
        {
            return false;
        }
    }
}
