﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScanSQL.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Information class to hold SQL data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.ScanParse
{
    /// <summary>
    /// SQL command is returned.
    /// </summary>
    public class ScanSQL : IBaseDataReturn
    {
        /// <summary>
        /// Gets or sets the SQL command.
        /// </summary>
        /// <value>
        /// The SQL command.
        /// </value>
        public string Data { get; set; }
    }
}
