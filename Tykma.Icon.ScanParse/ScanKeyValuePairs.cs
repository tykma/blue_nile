﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScanKeyValuePairs.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.ScanParse
{
    using System.Collections.Generic;

    using Tykma.Core.Entity;

    /// <summary>
    /// Key value pairs returned.
    /// </summary>
    public class ScanKeyValuePairs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScanKeyValuePairs"/> class.
        /// </summary>
        public ScanKeyValuePairs()
        {
            this.IDDataValuePairs = new List<EntityInformation>();
        }

        /// <summary>
        /// Gets or sets the identifier data value pairs.
        /// </summary>
        /// <value>
        /// The identifier data value pairs.
        /// </value>
        public List<EntityInformation> IDDataValuePairs { get; set; }

        /// <summary>
        /// Gets a value indicating whether [data present].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [data present]; otherwise, <c>false</c>.
        /// </value>
        public bool DataPresent => this.IDDataValuePairs.Count > 0;

        /// <summary>
        /// Adds the specified name value pair.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public void Add(string name, string value)
        {
            var kvp = new EntityInformation { Name = name, Value = value };
            this.IDDataValuePairs.Add(kvp);
        }
    }
}
