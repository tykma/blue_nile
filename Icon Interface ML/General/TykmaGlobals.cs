﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TykmaGlobals.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Global application settings.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.General
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Net;
    using System.Reflection;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Config.Net;
    using Icon_Interface.File_Management;
    using Tykma.Core.Engine;

    /// <summary>
    /// A class that hold all global variables and methods.
    /// </summary>
    public static class TykmaGlobals
    {
        /// <summary>
        /// The PLC settings.
        /// </summary>
        internal static readonly IPLCSettings PLCSettings;

        /// <summary>
        /// The SQL settings.
        /// </summary>
        internal static readonly ISQLSettings SQLSettings;

        /// <summary>
        /// The INI settings.
        /// </summary>
        internal static readonly IApplicationSettings INISettings;

        /// <summary>
        /// The project's name.
        /// </summary>
        private const string ThisProjectName = "Tykma_Icon";

        /// <summary>
        /// Initializes static members of the <see cref="TykmaGlobals"/> class.
        /// </summary>
        static TykmaGlobals()
        {
            string sqlpath = @"C:\tykma\custom\Tykma_Icon\SQL_bn.json";
            SQLSettings = new ConfigurationBuilder<ISQLSettings>().CacheFor(TimeSpan.Zero).UseJsonFile(sqlpath).Build();
            foreach (var setting in SQLSettings.GetType().GetProperties())
            {
                var val = setting.GetValue(SQLSettings);
            }

            string plcpath = @"C:\tykma\custom\Tykma_Icon\PLC.json";
            PLCSettings = new ConfigurationBuilder<IPLCSettings>().CacheFor(TimeSpan.Zero).UseJsonFile(plcpath).Build();
            foreach (var setting in PLCSettings.GetType().GetProperties())
            {
                var val = setting.GetValue(PLCSettings);
            }

            string appsettingspath = @"C:\tykma\custom\Tykma_Icon\config_bn.json";
            INISettings = new ConfigurationBuilder<IApplicationSettings>().CacheFor(TimeSpan.Zero).UseJsonFile(appsettingspath).Build();

            foreach (var setting in INISettings.GetType().GetProperties())
            {
                var val = setting.GetValue(INISettings);
            }
        }

        /// <summary>
        /// Gets the name of the project.
        /// </summary>
        /// <value>
        /// The name of the project.
        /// </value>
        internal static string ProjectName
        {
            get { return ThisProjectName; }
        }

        /// <summary>
        /// Write default settings.
        /// </summary>
        /// <param name="obj">Parameter.</param>
        public static void WriteDefaultSettings(object obj)
        {
            foreach (var prop in obj.GetType().GetProperties())
            {
                if (IsSimpleOrSettable(prop.PropertyType))
                {
                    prop.SetValue(obj, prop.GetValue(obj));
                }
                else
                {
                    WriteDefaultSettings(prop.GetValue(obj));
                }
            }
        }

        /// <summary>
        /// Identify whether type is simple or settable.
        /// </summary>
        /// <param name="type">Type.</param>
        /// <returns>Whether type is simple or settable.</returns>
        public static bool IsSimpleOrSettable(Type type)
        {
            return true;
        }

        /// <summary>
        /// VB.net 'with' work-around.
        /// </summary>
        /// <typeparam name="T">The type.</typeparam>
        /// <param name="item">The item.</param>
        /// <param name="work">The work.</param>
        public static void Use<T>(this T item, Action<T> work)
        {
            work(item);
        }

        /// <summary>
        /// Gets information about the software.
        /// </summary>
        /// <returns>List of software information.</returns>
        public static IEnumerable<string> SoftwareInformation()
        {
            var infoList = new List<string>();

            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            AssemblyTitleAttribute titleAttribute = null;

            if (attributes.Length > 0)
            {
                // Select the first one
                titleAttribute = (AssemblyTitleAttribute)attributes[0];
            }

            string title = string.Empty;
            if (titleAttribute != null)
            {
                title = titleAttribute.Title;
            }

            infoList.Add("Company: Tykma Electrox");
            infoList.Add(string.Format("Title: {0}", title));
            infoList.Add(string.Format("Version: {0}", Assembly.GetEntryAssembly().GetName().Version));
            infoList.Add(string.Format("Custom: {0}", "Base"));
            infoList.Add(string.Format("Authors: {0}", "Youri Karpeev"));
            infoList.Add(string.Format("Minilase Pro SE: {0}", "1.0.0.4"));
            infoList.Add(string.Format("Engine: {0}", typeof(ILaserInterface).Assembly.GetName().Version));
            infoList.Add(Environment.NewLine);
            return infoList;
        }

        /// <summary>
        /// Invokes a method if it is required.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="action">The action.</param>
        public static void InvokeIfRequired(this ISynchronizeInvoke obj, MethodInvoker action)
        {
            if (obj.InvokeRequired)
            {
                try
                {
                    var args = new object[0];
                    obj.Invoke(action, args);
                }
                catch (ObjectDisposedException)
                {
                }
            }
            else
            {
                action();
            }
        }

        /// <summary>
        /// Forgets the specified task.
        /// </summary>
        /// <param name="task">The task.</param>
        public static void Forget(this Task task)
        {
        }

        /// <summary>
        /// Enable double buffered setting on a specified control.
        /// </summary>
        /// <param name="control">The UI control to change.</param>
        public static void SetDoubleBuffered(Control control)
        {
            // set instance non-public property with name "DoubleBuffered" to true
            typeof(Control).InvokeMember(
                "DoubleBuffered",
                BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                null,
                control,
                new object[] { true });
        }
    }
}