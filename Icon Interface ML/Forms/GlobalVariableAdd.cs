﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlobalVariableAdd.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   A form for adding a global variable item to the database.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Windows.Forms;

    using Tykma.Icon.Globals;

    /// <summary>
    /// Add global variables form.
    /// </summary>
    public partial class GlobalVariableAdd : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GlobalVariableAdd"/> class.
        /// </summary>
        public GlobalVariableAdd()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets the result text.
        /// </summary>
        /// <value>
        /// The result text.
        /// </value>
        internal string IDValue { get; private set; }

        /// <summary>
        /// Gets the name of the identifier.
        /// </summary>
        /// <value>
        /// The name of the identifier.
        /// </value>
        internal string IDName { get; private set; }

        /// <summary>
        /// Gets the type of the selected variable.
        /// </summary>
        /// <value>
        /// The type of the selected variable.
        /// </value>
        internal EGlobalTypes.GlobalVariableType SelectedVariableType { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether [is counter].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is counter]; otherwise, <c>false</c>.
        /// </value>
        internal bool IsCounter { get; set; }

        /// <summary>
        /// Handles the Click event of the Button Cancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ButtonCancelGvClick(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Handles the Click event of the Button_OKAddGV control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ButtonOkAddGvClick(object sender, EventArgs e)
        {
            this.IDName = this.ux_Text_AddVarID.Text;
            this.IDValue = this.ux_Text_AddVarValue.Text;

            if (this.IsCounter)
            {
                // ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
                if (this.ux_Check_ALphaGlobal.Checked)
                {
                    this.SelectedVariableType = EGlobalTypes.GlobalVariableType.Counter_Alpha;
                }
                else
                {
                    this.SelectedVariableType = EGlobalTypes.GlobalVariableType.Counter_Integer;
                }
            }
            else
            {
                this.SelectedVariableType = EGlobalTypes.GlobalVariableType.String;
            }

            this.DialogResult = DialogResult.OK;
        }

        /// <summary>
        /// Handles the KeyPress event of the Text Add variable value control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyPressEventArgs"/> instance containing the event data.</param>
        private void TextAddVarValueKeyPress(object sender, KeyPressEventArgs e)
        {
            if (!this.IsCounter)
            {
                return;
            }

            if (char.IsDigit(e.KeyChar) || e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
