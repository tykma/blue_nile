﻿// <copyright file="ListExtensions.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Icon.JobList
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    /// <summary>
    /// Extensions for list.
    /// </summary>
    public static class ListExtensions
    {
        /// <summary>
        /// Removes the extensions.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <returns>List without extensions.</returns>
        public static IEnumerable<string> RemoveExtensions(this IEnumerable<string> list)
        {
            return list?.Select(Path.GetFileNameWithoutExtension).ToList() ?? new List<string>();
        }

        /// <summary>
        /// Breaks the list into groups with each group containing no more than the specified group size.
        /// </summary>
        /// <typeparam name="T">The type.</typeparam>
        /// <param name="values">The values.</param>
        /// <param name="groupSize">Size of the group.</param>
        /// <param name="maxCount">The max count.</param>
        /// <returns>List of split lists.</returns>
        public static List<List<T>> SplitList<T>(this IEnumerable<T> values, int groupSize, int? maxCount = null)
        {
            var result = new List<List<T>>();

            // Quick and special scenario.
            var enumerable = values as T[] ?? values.ToArray();
            if (enumerable.Length <= groupSize || groupSize <= 0)
            {
                result.Add(enumerable.ToList());
            }
            else
            {
                List<T> valueList = enumerable.ToList();
                int startIndex = 0;
                int count = valueList.Count;

                // ReSharper disable once ConditionIsAlwaysTrueOrFalse
                while (startIndex < count && (!maxCount.HasValue || (maxCount.HasValue && startIndex < maxCount)))
                {
                    int elementCount = (startIndex + groupSize > count) ? count - startIndex : groupSize;
                    result.Add(valueList.GetRange(startIndex, elementCount));
                    startIndex += elementCount;
                }
            }

            return result;
        }
    }
}
