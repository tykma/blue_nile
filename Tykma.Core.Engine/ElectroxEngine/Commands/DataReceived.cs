﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataReceived.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   The data received class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.ElectroxEngine.Commands
{
    /// <summary>
    /// The data received.
    /// </summary>
    public class DataReceived
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataReceived"/> class.
        /// </summary>
        public DataReceived()
        {
            this.ReceivedData = string.Empty;
        }

        /// <summary>
        /// Gets or sets the received data.
        /// </summary>
        public string ReceivedData { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether there was a timeout.
        /// </summary>
        public bool Timeout { get; set; }

        /// <summary>
        /// Gets or sets the response code.
        /// </summary>
        /// <value>
        /// The response code.
        /// </value>
        public ReturnStateParse.EStatResponses ResponseCode { get; set; }

        /// <summary>
        /// Gets a value indicating whether the command was successful.
        /// </summary>
        public bool Success
        {
            get
            {
                return !this.Timeout;
            }
        }
    }
}
