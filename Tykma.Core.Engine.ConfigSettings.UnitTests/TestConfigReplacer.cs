﻿// <copyright file="TestConfigReplacer.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.Engine.ConfigSettings.UnitTests
{
    using System;
    using System.IO;
    using System.IO.Abstractions;
    using System.IO.Abstractions.TestingHelpers;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using NSubstitute;

    /// <summary>
    /// SE Config replacement unit tests.
    /// </summary>
    [TestClass]
    public class TestConfigReplacer
    {
        /// <summary>
        /// Tests the that file is deleted if it exists.
        /// </summary>
        [TestMethod]
        public void TestThatFileIsDeletedIfItExists()
        {
            string configPath = @"C:\temp\config.txt";

            var fs = Substitute.For<IFileSystem>();
            var file = Substitute.For<FileBase>();
            var dir = Substitute.For<DirectoryBase>();
            fs.Directory.Returns(dir);

            file.Exists(configPath).Returns(true);

            fs.File.Returns(file);

            var replacer = new ConfigReplacer(configPath, fs);

            using (var s = new MemoryStream())
            {
                using (var b = new BinaryWriter(s))
                {
                    b.Write('a');
                    replacer.ReplaceConfig(s);
                }
            }

            // Confirm that we received a check for the file.
            fs.File.Received().Exists(configPath);

            fs.File.Received().Delete(configPath);
        }

        /// <summary>
        /// Tests the that file is not deleted if it does not exist.
        /// </summary>
        [TestMethod]
        public void TestThatFileIsNotDeletedIfItDoesNotExist()
        {
            string configPath = @"C:\temp\config.txt";

            var fs = Substitute.For<IFileSystem>();

            var dir = Substitute.For<DirectoryBase>();
            fs.Directory.Returns(dir);
            var file = Substitute.For<FileBase>();
            fs.File.Returns(file);
            file.Exists(configPath).Returns(false);

            var replacer = new ConfigReplacer(configPath, fs);

            using (var s = new MemoryStream())
            {
                using (var b = new BinaryWriter(s))
                {
                    b.Write('a');
                    replacer.ReplaceConfig(s);
                }
            }

            // Confirm that we received a check for the file.
            file.Received().Exists(configPath);

            file.DidNotReceive().Delete(configPath);
        }

        /// <summary>
        /// Tests that file is created.
        /// </summary>
        [TestMethod]
        public void TestThatFileIsCreated()
        {
            string configPath = @"C:\Program Files (x86)\minilase pro se\plug\config";
            var mock = new MockFileSystem();

            Assert.IsFalse(mock.FileExists(configPath));

            var replacer = new ConfigReplacer(configPath, mock);

            using (var s = new MemoryStream())
            {
                using (var b = new BinaryWriter(s))
                {
                    b.Write('a');
                    Assert.IsTrue(replacer.ReplaceConfig(s));
                }
            }

            Assert.IsTrue(mock.FileExists(configPath));

            var data = mock.File.ReadAllLines(configPath);

            Assert.AreEqual(1, data.Length);
            Assert.AreEqual("a", data[0]);
        }

        /// <summary>
        /// Tests that an exception returns false.
        /// </summary>
        [TestMethod]
        public void TestThatExceptionReturnsFalse()
        {
            string configPath = @"C:\temp\config.txt";
            var fs = Substitute.For<IFileSystem>();
            var file = Substitute.For<FileBase>();
            file.Exists(configPath).Returns(true);

            var dir = Substitute.For<DirectoryBase>();
            fs.Directory.Returns(dir);
            fs.File.Returns(file);

            var replacer = new ConfigReplacer(configPath, fs);

            dir.When(x => x.CreateDirectory(Arg.Any<string>())).Do(
            x => throw new IOException());

            using (var s = new MemoryStream())
            {
                using (var b = new BinaryWriter(s))
                {
                    b.Write('a');
                    Assert.IsFalse(replacer.ReplaceConfig(s));
                }
            }
        }

        /// <summary>
        /// Tests that an access exception returns false.
        /// </summary>
        [TestMethod]
        public void TestThatAccessExceptionReturnsFalse()
        {
            string configPath = @"C:\temp\config.txt";
            var fs = Substitute.For<IFileSystem>();
            var file = Substitute.For<FileBase>();
            file.Exists(configPath).Returns(true);
            var dir = Substitute.For<DirectoryBase>();
            fs.Directory.Returns(dir);
            fs.File.Returns(file);

            var replacer = new ConfigReplacer(configPath, fs);

            dir.When(x => x.CreateDirectory(Arg.Any<string>())).Do(
                x => throw new UnauthorizedAccessException());

            using (var s = new MemoryStream())
            {
                using (var b = new BinaryWriter(s))
                {
                    b.Write('a');
                    Assert.IsFalse(replacer.ReplaceConfig(s));
                }
            }
        }
    }
}
