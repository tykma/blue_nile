﻿// <copyright file="IMarkingWindow.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.Engine.ConfigSettings
{
    using System.Collections.Generic;
    using System.Drawing;

    /// <summary>
    /// Axis status interface.
    /// </summary>
    public interface IMarkingWindow
    {
        /// <summary>
        /// Gets the size.
        /// </summary>
        /// <value>
        /// The size.
        /// </value>
        Size Size { get; }

        /// <summary>
        /// Gets the field offset X.
        /// </summary>
        double OffsetX { get; }

        /// <summary>
        /// Gets the field offset X.
        /// </summary>
        double OffsetY { get; }

        /// <summary>
        /// Gets the field angle.
        /// </summary>
        double FieldAngle { get; }

        /// <summary>
        /// Gets a value indicating whether galvo 1 is the X galvo.
        /// </summary>
        bool IsGalvo1X { get; }

        /// <summary>
        /// Set the size of the field.
        /// </summary>
        /// <param name="fieldSize">The new field size.</param>
        /// <returns>New config.</returns>
        IList<string> SetSize(int fieldSize);

        /// <summary>
        /// Set the field offset.
        /// </summary>
        /// <param name="x">Set the X offset.</param>
        /// <param name="y">Set the Y offset.</param>
        /// <returns>New configuration.s.</returns>
        IList<string> SetOffset(double x, double y);

        /// <summary>
        /// Sets the field angle (rotation).
        /// </summary>
        /// <param name="angle">The angle.</param>
        /// <returns>New configuration.</returns>
        IList<string> SetFieldAngle(double angle);

        /// <summary>
        /// Sets the first galvo as the X galvo.
        /// </summary>
        /// <param name="enable">Whether the galvo is an X galvo or not.</param>
        /// <returns>New configuration.</returns>
        IList<string> SetGalvo1AsX(bool enable);
    }
}
