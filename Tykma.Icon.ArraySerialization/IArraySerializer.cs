﻿// <copyright file="IArraySerializer.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Icon.ArraySerialization
{
    using System.Collections.Generic;
    using System.Linq;

    using Core.Entity;

    /// <summary>
    /// Array serializer interface.
    /// </summary>
    public interface IArraySerializer
    {
        /// <summary>
        /// Gets the serialization overlay.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <returns>Serialization overlay.</returns>
        IOrderedEnumerable<EntityInformation> GetSerializationLayout(List<EntityInformation> list);
    }
}
