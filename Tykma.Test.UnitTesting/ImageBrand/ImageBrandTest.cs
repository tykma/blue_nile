﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImageBrandTest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.ImageBrand
{
    using System.Diagnostics.CodeAnalysis;
    using System.Drawing;
    using System.IO;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Icon.ImageBranding;

    /// <summary>
    /// The image brand test.
    /// </summary>
    [TestClass]
    public class ImageBrandTest
    {
        /// <summary>
        /// Test Getting an RTF File.
        /// </summary>
        [TestMethod]
        public void TestGettingRTFFile()
        {
            const string MainFolder = @"C:\temp\tykma";
            const string InstrFolder = @"C:\temp\tykma\instructions\";
            Directory.CreateDirectory(@"C:\temp\tykma\instructions\");
            File.Create(@"C:\temp\tykma\instructions\job123.rtf");

            var imageBrands = new ImageBranding(MainFolder, InstrFolder, InstrFolder, Color.Blue);

            var rtflocation = imageBrands.GetRTFLocation("job123");

            Assert.AreEqual(@"C:\temp\tykma\instructions\job123.rtf", rtflocation.FullName);
        }

        /// <summary>
        /// Tests the non existent RTF file.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        [ExcludeFromCodeCoverage]
        public void TestNonExistentRTFFile()
        {
            const string MainFolder = @"C:\temp\tykma";
            const string InstrFolder = @"C:\temp\tykma\instructions_nonexistent\";

            var imageBrands = new ImageBranding(MainFolder, InstrFolder, InstrFolder, Color.Blue);

            var rtflocation = imageBrands.GetRTFLocation("job123");
        }

        /// <summary>
        /// Tests the non existent Image file.
        /// </summary>
        [TestMethod]
        public void TestNonExistentImageFile()
        {
            const string MainFolder = @"C:\temp\tykma";
            const string InstrFolder = @"C:\temp\tykma\instructions_nonexistent\";

            var imageBrands = new ImageBranding(MainFolder, InstrFolder, InstrFolder, Color.Blue);

            var imgLocation = imageBrands.GetJobPicture("job123");

            Assert.AreEqual(imageBrands.FileFault.Height, imgLocation.Height);
        }

        /// <summary>
        /// Tests the empty folders.
        /// </summary>
        [TestMethod]
        public void TestEmptyFolders()
        {
            var imageBrands = new ImageBranding(string.Empty, string.Empty, string.Empty, Color.Blue);
            Assert.AreEqual(@"C:\tykma\custom\tykma_icon\rtf\default.rtf", imageBrands.GetRTFLocation("default").FullName);
        }

        /// <summary>
        /// Test Getting an image File.
        /// </summary>
        [TestMethod]
        public void TestGettingImageFile()
        {
            const string MainFolder = @"C:\temp\tykma";
            const string InstrFolder = @"C:\temp\tykma\instructions\";

            Directory.CreateDirectory(@"C:\temp\tykma\instructions\");

            var b = new Bitmap(1, 1);
            b.SetPixel(0, 0, Color.White);
            var result = new Bitmap(b, 1024, 1024);

            result.Save(@"C:\temp\tykma\instructions\job123.pnG");

            var imageBrands = new ImageBranding(MainFolder, InstrFolder, InstrFolder, Color.Blue);

            Image imgLocation = imageBrands.GetJobPicture("job123");
            Assert.AreEqual(1024, imgLocation.Height);
        }
    }
}
