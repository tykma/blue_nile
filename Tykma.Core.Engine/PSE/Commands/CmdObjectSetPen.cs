﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdObjectGetPen.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// Command to get a pen number from an object.
    /// </summary>
    internal class CmdObjectSetPen : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdObjectGetPen"/> class.
        /// </summary>
        /// <param name="lmcApi">
        /// The laser marking API.
        /// </param>
        /// <param name="objectToGetPen">
        /// The object to get pen.
        /// </param>
        public CmdObjectSetPen(IntPtr lmcApi, string objectToSetPen, int penNo)
            : base(lmcApi)
        {
            this.ObjectToSetPenFrom = objectToSetPen;
            this.PenIndex = penNo;
        }
        [DllImport("MarkPSE", EntryPoint = "lmc1_SetEntAllChildPen", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern void SetEntAllChildPen(string strEntName, int nPenNo);


        /// <summary>
        /// Gets the object to get pen from.
        /// </summary>
        /// <value>
        /// The object to get pen from.
        /// </value>
        private string ObjectToSetPenFrom { get; }

        private int PenIndex { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Pen number.</returns>
        /// <exception cref="Tykma.Core.Engine.CustomExceptions.EngineNotConnectedException">Engine not connected.</exception>
        internal int Execute()
        {

            SetEntAllChildPen(this.ObjectToSetPenFrom, this.PenIndex);
            return 0;
        }
    }
}
