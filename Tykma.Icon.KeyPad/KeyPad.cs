﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KeyPad.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.KeyPad
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    /// <summary>
    /// The key pad.
    /// </summary>
    public class KeyPad : IDisposable
    {
        /// <summary>
        /// The key pad form.
        /// </summary>
        private readonly KeyPadEntryForm keyPadForm;

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyPad"/> class.
        /// </summary>
        /// <param name="backColor">Color of the back.</param>
        /// <param name="foreColor">Color of the fore.</param>
        public KeyPad(Color backColor, Color foreColor)
        {
            this.BackColor = backColor;
            this.ForeColor = foreColor;
            this.keyPadForm = new KeyPadEntryForm(this.BackColor, this.ForeColor);
        }

        /// <summary>
        /// Gets the color of the backround.
        /// </summary>
        /// <value>
        /// The color of the back.
        /// </value>
        private Color BackColor { get; }

        /// <summary>
        /// Gets the foreground color.
        /// </summary>
        /// <value>
        /// The color of the foreground.
        /// </value>
        private Color ForeColor { get; }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <returns>Operator entered value.</returns>
        public int GetValue()
        {
            if (this.keyPadForm.ShowDialog() == DialogResult.OK)
            {
                return this.keyPadForm.UserEnteredValue;
            }

            return 0;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.keyPadForm.Dispose();
        }
    }
}
