﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlButtonPLC.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    using Icon_Interface.Properties;

    /// <summary>
    /// The control button for a PLC.
    /// </summary>
    public partial class ControlButtonPLC : ControlButtonBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlButtonPLC"/> class.
        /// </summary>
        public ControlButtonPLC()
        {
            this.InitializeComponent();
            this.Customize();
        }

        /// <summary>
        /// The customization method..
        /// </summary>
        private void Customize()
        {
            this.Text = Resources.Label_PLC;
        }
    }
}
