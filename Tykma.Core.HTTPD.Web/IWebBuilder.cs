﻿// <copyright file="IWebBuilder.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Core.HTTPD.Web
{
    using System.Collections.Generic;

    /// <summary>
    /// TCP/IP API Web builder interface.
    /// </summary>
    public interface IWebBuilder
    {
        /// <summary>
        /// Gets the page.
        /// </summary>
        /// <param name="jobList">The job list.</param>
        /// <returns>A complete page.</returns>
        string GetPage(List<string> jobList);
    }
}
