﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringExtendTests.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Defines the UnitTest1 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.StringExtensions
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Core.StringExtensions;

    /// <summary>
    /// Test for string extensions helper test.
    /// </summary>
    [TestClass]
    public class StringExtendTests
    {
        /// <summary>
        /// Tests the string clean extension method.
        /// </summary>
        [TestMethod]
        public void TestStringClean()
        {
            // This method will remove newlines and replace them with spaces.
            Assert.AreEqual("test clean", "test\r\nclean".Clean());
            Assert.AreEqual("test clean", "test\n\rclean".Clean());
            Assert.AreEqual("test clean", "test\nclean".Clean());
            Assert.AreEqual("test clean", "test\rclean".Clean());
            Assert.AreEqual("test clean", "test\r\r\r\n\rclean".Clean());
            Assert.AreEqual(" ", "\r\r\r\r\r\r\n".Clean());
            Assert.AreEqual(string.Empty, string.Empty.Clean());
        }

        /// <summary>
        /// Tests the case insensitive replace method.
        /// </summary>
        [TestMethod]
        public void TestCaseInsensitiveReplace()
        {
            Assert.AreEqual("ABC456", "ABC123".CaseInsensitiveReplace("123", "456"));
            Assert.AreEqual("AbC456", "AbC123".CaseInsensitiveReplace("123", "456"));
            Assert.AreEqual("abC666", "abC555".CaseInsensitiveReplace("555", "666"));
            Assert.AreEqual("TESTAaA", "TESTreplace".CaseInsensitiveReplace("replace", "AaA"));
            Assert.AreEqual("abC666", "abC555".CaseInsensitiveReplace("555", "666"));
            Assert.AreEqual("TESTrePlace", "TESTAaA".CaseInsensitiveReplace("AaA", "rePlace"));
            Assert.AreEqual(string.Empty, "ZzZ".CaseInsensitiveReplace("zzz", string.Empty));
            Assert.AreEqual(
                string.Empty,
                string.Empty.CaseInsensitiveReplace(string.Empty, string.Empty));
            Assert.AreEqual("QWERTY", "QWERTY".CaseInsensitiveReplace(string.Empty, string.Empty));
            Assert.AreEqual("QWERTY", "QWERTY".CaseInsensitiveReplace("MISSING", "BBBBB"));
        }

        /// <summary>
        /// Substring extension test.
        /// </summary>
        [TestMethod]
        public void SubStringTest()
        {
            const string TestString = "GET /index.html HTTP/1.1";
            Assert.AreEqual("index.html ", TestString.Substring(from: @"GET /", until: "HTTP/1.1"));
        }

        /// <summary>
        /// Test double suffixes.
        /// </summary>
        [TestMethod]
        public void SubstringTestDoubleSuffix()
        {
            const string TestString = "z123z123z";
            Assert.AreEqual("123", TestString.Substring(from: @"z", until: "z"));
        }

        /// <summary>
        /// Test getting substring with the first anchor being null.
        /// </summary>
        [TestMethod]
        public void SubstringNullFromTest()
        {
            const string TestString = "abc123z";
            Assert.AreEqual("abc123", TestString.Substring(from: null, until: "z"));
        }

        /// <summary>
        /// Subs the string empty HTTPD test.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        [ExcludeFromCodeCoverage]
        public void SubStringEmptyHttpdTest()
        {
            const string TestString = "GET HTTP/1.1";
            Assert.AreEqual(" ", TestString.Substring(from: @"GET /", until: "HTTP/1.1"));
        }

        /// <summary>
        /// Test for null strings.
        /// </summary>
        [TestMethod]
        public void SubStringTestNull()
        {
            const string TestString = "abcxyz";
            Assert.AreEqual(string.Empty, TestString.Substring(from: @"abc", until: "xyz"));
        }

        /// <summary>
        /// Test for missing suffixes.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        [ExcludeFromCodeCoverage]
        public void SubStringTestMissingSuffix()
        {
            const string TestString = "abc";
            Assert.AreEqual(string.Empty, TestString.Substring(from: @"abc", until: "xyz"));
        }

        /// <summary>
        /// Test with a missing prefix.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        [ExcludeFromCodeCoverage]
        public void SubStringTestMissingPrefix()
        {
            const string TestString = "xyz";
            Assert.AreEqual(string.Empty, TestString.Substring(from: @"abc", until: "xyz"));
        }

        /// <summary>
        /// Test with missing anchors.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        [ExcludeFromCodeCoverage]
        public void SubStringTestMissingPrefixes()
        {
            const string TestString = "xyz";
            Assert.AreEqual(string.Empty, TestString.Substring(from: @"45345", until: "345"));
        }

        /// <summary>
        /// Test string empty to substring.
        /// </summary>
        [TestMethod]
        public void SubStringTestAllEmpty()
        {
            string testString = string.Empty;
            Assert.AreEqual(string.Empty, testString.Substring(from: string.Empty, until: string.Empty));
        }

        /// <summary>
        /// Tests removing prefix.
        /// </summary>
        [TestMethod]
        public void TestRemovingPrefix()
        {
            const string TestString = "prefixed";
            Assert.AreEqual("fixed", TestString.RemovePrefix("pre"));
        }

        /// <summary>
        /// Tests the removing null prefix.
        /// </summary>
        [TestMethod]
        public void TestRemovingNullPrefix()
        {
            const string TestString = "ZZZ";
            Assert.AreEqual("ZZZ", TestString.RemovePrefix(null));
        }

        /// <summary>
        /// Tests the removing null suffix.
        /// </summary>
        [TestMethod]
        public void TestRemovingNullSuffix()
        {
            const string TestString = "ZZZ";
            Assert.AreEqual("ZZZ", TestString.RemoveSuffix(null));
        }

        /// <summary>
        /// Tests removing suffix.
        /// </summary>
        [TestMethod]
        public void TestRemovingSuffix()
        {
            const string TestString = "prefixed";
            Assert.AreEqual("pre", TestString.RemoveSuffix("fixed"));
        }

        /// <summary>
        /// Tests removing empty prefix.
        /// </summary>
        [TestMethod]
        public void TestRemovingEmptyPrefix()
        {
            const string TestString = "preFix";
            Assert.AreEqual("preFix", TestString.RemovePrefix(string.Empty));
        }

        /// <summary>
        /// Tests removing empty prefix.
        /// </summary>
        [TestMethod]
        public void TestRemovingEmptySuffix()
        {
            const string TestString = "suffix";
            Assert.AreEqual("suffix", TestString.RemoveSuffix(string.Empty));
        }

        /// <summary>
        /// Tests removing prefix from empty string.
        /// </summary>
        [TestMethod]
        public void TestRemovingEmptyStringPrefix()
        {
            Assert.AreEqual(string.Empty, string.Empty.RemovePrefix("AAAA"));
        }

        /// <summary>
        /// Tests removing suffix from empty string.
        /// </summary>
        [TestMethod]
        public void TestRemovingEmptyStringSuffix()
        {
            Assert.AreEqual(string.Empty, string.Empty.RemoveSuffix("AAAA"));
        }

        /// <summary>
        /// Tests getting a boolean value from a string.
        /// </summary>
        [TestMethod]
        public void TestTryBool()
        {
            Assert.IsTrue("True".TryGetBool());
            Assert.IsTrue("TrUe".TryGetBool());
            Assert.IsTrue("TRUE".TryGetBool());
            Assert.IsFalse("1".TryGetBool());
            Assert.IsFalse("false".TryGetBool());
            Assert.IsFalse("0".TryGetBool());
            Assert.IsFalse("no".TryGetBool());
        }

        /// <summary>
        /// Tests the case compare insensitive.
        /// </summary>
        [TestMethod]
        public void TestCaseCompareInsensitive()
        {
            Assert.IsTrue("ZZZZZ".CaseInsensitiveCompare("zzzzz"));
            Assert.IsFalse("ZZZZZ".CaseInsensitiveCompare("zzzzzz"));
            Assert.IsTrue(string.Empty.CaseInsensitiveCompare(string.Empty));
            Assert.IsFalse("Z".CaseInsensitiveCompare(string.Empty));
            Assert.IsTrue("abc123".CaseInsensitiveCompare("ABC123"));
        }

        /// <summary>
        /// Test adding an extension.
        /// </summary>
        [TestMethod]
        public void AddExtensionTest()
        {
            const string X = "file";
            string newstring = X.AddExtension("txt");

            Assert.AreEqual("file.txt", newstring);
        }

        /// <summary>
        /// Test adding an empty extension.
        /// </summary>
        [TestMethod]
        public void AddEmptyExtensionTest()
        {
            const string X = "file";
            string newstring = X.AddExtension(string.Empty);

            Assert.AreEqual("file.", newstring);
        }

        /// <summary>
        /// Test adding an extension to an empty string.
        /// </summary>
        [TestMethod]
        public void AddExtensionToEmptyStringTest()
        {
            string x = string.Empty;
            string newstring = x.AddExtension("txt");

            Assert.AreEqual(".txt", newstring);
        }

        /// <summary>
        /// Tests the is numeric extension.
        /// </summary>
        [TestMethod]
        public void TestIsNumeric()
        {
            Assert.IsFalse("abc123".IsNumeric());
            Assert.IsTrue("000123".IsNumeric());
            Assert.IsTrue("5234234234234".IsNumeric());
        }
    }
}
