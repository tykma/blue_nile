﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileParserTest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.FileParser
{
    using System.IO;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Core.Entity;
    using Tykma.Icon.FileParse;

    /// <summary>
    /// The file parser test.
    /// </summary>
    [TestClass]
    public class FileParserTest
    {
        /// <summary>
        /// Tests the job as first line.
        /// </summary>
        [TestMethod]
        public void TestJobAsFirstLine()
        {
            using (var ms = new MemoryStream())
            {
                var sw = new StreamWriter(ms);

                sw.WriteLine("*JOB1");
                sw.Flush();

                var parser = new TykmaParse();

                var jobInfo = parser.ParseJob(ms);
                Assert.AreEqual("JOB1", jobInfo.JobName);

                sw.Dispose();
            }
        }

        /// <summary>
        /// Tests the job with spaces at end.
        /// </summary>
        [TestMethod]
        public void TestJobWithSpacesAtEnd()
        {
            using (var ms = new MemoryStream())
            {
                var sw = new StreamWriter(ms);

                sw.WriteLine("*JOB1   ");
                sw.Flush();

                var parser = new TykmaParse();

                var jobInfo = parser.ParseJob(ms);
                Assert.AreEqual("JOB1", jobInfo.JobName);

                sw.Dispose();
            }
        }

        /// <summary>
        /// Tests the job in middle of stream.
        /// </summary>
        [TestMethod]
        public void TestJobInMiddleOfStream()
        {
            using (var ms = new MemoryStream())
            {
                var sw = new StreamWriter(ms);
                sw.WriteLine("data:123 ");
                sw.WriteLine("asdfasf ");
                sw.WriteLine("*JOB1   ");
                sw.Flush();

                var parser = new TykmaParse();

                var jobInfo = parser.ParseJob(ms);
                Assert.AreEqual("JOB1", jobInfo.JobName);

                sw.Dispose();
            }
        }

        /// <summary>
        /// Tests the quantity setting.
        /// </summary>
        [TestMethod]
        public void TestQuantitySetting()
        {
            using (var ms = new MemoryStream())
            {
                var sw = new StreamWriter(ms);
                sw.WriteLine("#555");
                sw.WriteLine("asdfasf ");
                sw.WriteLine("*JOB1   ");
                sw.Flush();

                var parser = new TykmaParse();

                var jobInfo = parser.ParseJob(ms);
                Assert.AreEqual(555, jobInfo.Quantity);

                sw.Dispose();
            }
        }

        /// <summary>
        /// Tests the quantity with space.
        /// </summary>
        [TestMethod]
        public void TestQuantityWithSpace()
        {
            using (var ms = new MemoryStream())
            {
                var sw = new StreamWriter(ms);
                sw.WriteLine("#555  ");
                sw.WriteLine("asdfasf ");
                sw.WriteLine("*JOB1   ");
                sw.Flush();

                var parser = new TykmaParse();

                var jobInfo = parser.ParseJob(ms);
                Assert.AreEqual(555, jobInfo.Quantity);

                sw.Dispose();
            }
        }

        /// <summary>
        /// Tests the invalid quantity.
        /// </summary>
        [TestMethod]
        public void TestInvalidQuantity()
        {
            using (var ms = new MemoryStream())
            {
                var sw = new StreamWriter(ms);
                sw.WriteLine("#zzzz");
                sw.WriteLine("asdfasf ");
                sw.WriteLine("*JOB1   ");
                sw.Flush();

                var parser = new TykmaParse();

                var jobInfo = parser.ParseJob(ms);
                Assert.AreEqual(1, jobInfo.Quantity);

                sw.Dispose();
            }
        }

        /// <summary>
        /// Tests the identifier setting.
        /// </summary>
        [TestMethod]
        public void TestIDSetting()
        {
            using (var ms = new MemoryStream())
            {
                var sw = new StreamWriter(ms);
                sw.WriteLine("#zzzz");
                sw.WriteLine("testID:Data123");
                sw.WriteLine("*JOB1   ");
                sw.Flush();

                var parser = new TykmaParse();

                var jobInfo = parser.ParseJob(ms);

                var entity = new EntityInformation();

                foreach (var ent in jobInfo.ListOfEntities)
                {
                    if (ent.Name == "testID")
                    {
                        entity = ent;
                    }
                }

                Assert.AreEqual("testID", entity.Name);
                Assert.AreEqual("Data123", entity.Value);
                sw.Dispose();
            }
        }

        /// <summary>
        /// Test setting an ID that has a colon.
        /// </summary>
        [TestMethod]
        public void TestSettingIDWithColon()
        {
            using (var ms = new MemoryStream())
            {
                var sw = new StreamWriter(ms);
                sw.WriteLine("testID:A:B");
                sw.Flush();

                var parser = new TykmaParse();

                var jobInfo = parser.ParseJob(ms);

                var entity = new EntityInformation();

                foreach (var ent in jobInfo.ListOfEntities)
                {
                    if (ent.Name == "testID")
                    {
                        entity = ent;
                    }
                }

                Assert.AreEqual("testID", entity.Name);
                Assert.AreEqual("A:B", entity.Value);
                sw.Dispose();
            }
        }

        /// <summary>
        /// Tests the identifier setting with empty value.
        /// </summary>
        [TestMethod]
        public void TestIDSettingWithEmptyValue()
        {
            using (var ms = new MemoryStream())
            {
                var sw = new StreamWriter(ms);
                sw.WriteLine("#zzzz");
                sw.WriteLine("testID:");
                sw.WriteLine("*JOB1   ");
                sw.Flush();

                var parser = new TykmaParse();

                var jobInfo = parser.ParseJob(ms);

                var entity = new EntityInformation();

                foreach (var ent in jobInfo.ListOfEntities)
                {
                    if (ent.Name == "testID")
                    {
                        entity = ent;
                    }
                }

                Assert.AreEqual("testID", entity.Name);
                Assert.AreEqual(string.Empty, entity.Value);
                sw.Dispose();
            }
        }

        /// <summary>
        /// Tests the setting identical identifier.
        /// </summary>
        [TestMethod]
        public void TestSettingIdenticalID()
        {
            using (var ms = new MemoryStream())
            {
                var sw = new StreamWriter(ms);
                sw.WriteLine("#zzzz");
                sw.WriteLine("testID:data:123");
                sw.WriteLine("testID:data:123");
                sw.WriteLine("*JOB1   ");
                sw.Flush();

                var parser = new TykmaParse();

                var jobInfo = parser.ParseJob(ms);

                Assert.AreEqual(1, jobInfo.ListOfEntities.Count);
                sw.Dispose();
            }
        }

        /// <summary>
        /// Tests the identifier setting with separator as data.
        /// </summary>
        [TestMethod]
        public void TestSettingIDwithSeparatorAsData()
        {
            using (var ms = new MemoryStream())
            {
                var sw = new StreamWriter(ms);
                sw.WriteLine("#zzzz");
                sw.WriteLine("testID:data:123");
                sw.WriteLine("*JOB1   ");
                sw.Flush();

                var parser = new TykmaParse();

                var jobInfo = parser.ParseJob(ms);

                var entity = new EntityInformation();

                foreach (var ent in jobInfo.ListOfEntities)
                {
                    if (ent.Name == "testID")
                    {
                        entity = ent;
                    }
                }

                Assert.AreEqual("testID", entity.Name);
                Assert.AreEqual("data:123", entity.Value);
                sw.Dispose();
            }
        }

        /// <summary>
        /// Tests the basic data with all options.
        /// </summary>
        [TestMethod]
        public void TestBasicDataWithAllOptions()
        {
            using (var ms = new MemoryStream())
            {
                var sw = new StreamWriter(ms);
                sw.WriteLine("#100");
                sw.WriteLine("testID:data:123");
                sw.WriteLine("MODEL:12345");
                sw.WriteLine("MODEL:12345");
                sw.WriteLine("MODEL:12345");
                sw.WriteLine("MODEL:12345");
                sw.WriteLine("MODEL:12345");
                sw.WriteLine("*JOB1 ");
                sw.Flush();

                var parser = new TykmaParse();

                var jobInfo = parser.ParseJob(ms);

                bool testIDExists = false;
                foreach (var ent in jobInfo.ListOfEntities.Where(ent => ent.Name == "testID"))
                {
                    testIDExists = true;
                    Assert.AreEqual("data:123", ent.Value);
                }

                Assert.IsTrue(testIDExists);

                bool modelIDExists = false;
                foreach (var ent in jobInfo.ListOfEntities.Where(ent => ent.Name == "MODEL"))
                {
                    modelIDExists = true;
                    Assert.AreEqual("12345", ent.Value);
                }

                Assert.IsTrue(modelIDExists);

                Assert.AreEqual("JOB1", jobInfo.JobName);
                Assert.AreEqual(100, jobInfo.Quantity);
                Assert.AreEqual(2, jobInfo.ListOfEntities.Count);

                sw.Dispose();
            }
        }

        /// <summary>
        /// Tests an empty file.
        /// </summary>
        [TestMethod]
        public void TestEmptyFile()
        {
            using (var ms = new MemoryStream())
            {
                var sw = new StreamWriter(ms);
                sw.Flush();
                var parser = new TykmaParse();

                var jobInfo = parser.ParseJob(ms);

                Assert.AreEqual(0, jobInfo.ListOfEntities.Count);
                Assert.AreEqual(string.Empty, jobInfo.JobName);
                Assert.AreEqual(0, jobInfo.ListOfEntities.Count);
                sw.Dispose();
            }
        }

        /// <summary>
        /// Tests an invalid file.
        /// </summary>
        [TestMethod]
        public void TestInvalidFile()
        {
            using (var ms = new MemoryStream())
            {
                var sw = new StreamWriter(ms);
                sw.WriteLine("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
                sw.WriteLine();
                sw.WriteLine("123123");
                sw.Flush();
                var parser = new TykmaParse();

                var jobInfo = parser.ParseJob(ms);

                Assert.AreEqual(0, jobInfo.ListOfEntities.Count);
                Assert.AreEqual(string.Empty, jobInfo.JobName);
                Assert.AreEqual(0, jobInfo.ListOfEntities.Count);
                sw.Dispose();
            }
        }
    }
}
