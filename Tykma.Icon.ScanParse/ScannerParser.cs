﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScannerParser.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   This class will receive a scanned text input and identify what type of command is associated with it.
//   We will use prefixes, suffixes and configuration options to handle how the commands are interpreted.
//   It will return 'classes'/'objects' with the associated parsed and sanitized data to the calling class.
//   It will be up to the calling class to interpret and handle the result.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.ScanParse
{
    using System;

    using Tykma.Core.StringExtensions;

    /// <summary>
    /// Scanner parser class. It will parse a barcode and return information back on what to do with it.
    /// </summary>
    public class ScannerParser
    {
        /// <summary>
        /// Parses a code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="usingQueue">if set to <c>true</c> [usingQueue].</param>
        /// <param name="simpleMarkMode">if set to <c>true</c> [simple mark mode].</param>
        /// <param name="simplePrefixSuffix">The simple prefix suffix.</param>
        /// <param name="stripPrefixandSuffix">if set to <c>true</c> [strip prefix and suffix].</param>
        /// <param name="sqlServer">Whether or not SQL is enabled.</param>
        /// <returns>
        /// Scan info type.
        /// </returns>
        public object ParseCode(string code, bool usingQueue, bool simpleMarkMode, string simplePrefixSuffix, bool stripPrefixandSuffix, bool sqlServer)
        {
            // Internal command.
            if (code.StartsWith("!"))
            {
                var scanCommand = new ScanCommand();

                if (code.CaseInsensitiveCompare("!start"))
                {
                    scanCommand.Command = ScanCommand.InternalCommand.Start;
                }
                else if (code.CaseInsensitiveCompare("!clear"))
                {
                    scanCommand.Command = ScanCommand.InternalCommand.Clear;
                }
                else if (code.CaseInsensitiveCompare("!limits"))
                {
                    scanCommand.Command = ScanCommand.InternalCommand.Limits;
                }
                else if (code.CaseInsensitiveCompare("!qty_up"))
                {
                    scanCommand.Command = ScanCommand.InternalCommand.QuantityUp;
                }
                else if (code.CaseInsensitiveCompare("!qty_down"))
                {
                    scanCommand.Command = ScanCommand.InternalCommand.QuantityDown;
                }
                else if (code.CaseInsensitiveCompare("!edit"))
                {
                    scanCommand.Command = ScanCommand.InternalCommand.Edit;
                }
                else if (code.CaseInsensitiveCompare("!joblist"))
                {
                    scanCommand.Command = ScanCommand.InternalCommand.ShowJobList;
                }
                else if (code.CaseInsensitiveCompare("!homez"))
                {
                    scanCommand.Command = ScanCommand.InternalCommand.HomeAxisZ;
                }
                else if (code.CaseInsensitiveCompare("!homer"))
                {
                    scanCommand.Command = ScanCommand.InternalCommand.HomeAxisR;
                }
                else if (code.CaseInsensitiveCompare("!debug"))
                {
                    scanCommand.Command = ScanCommand.InternalCommand.Debug;
                }
                else
                {
                    scanCommand.Command = ScanCommand.InternalCommand.None;
                }

                return scanCommand;
            }

            // A quantity.
            if (code.StartsWith("#"))
            {
                string value = code.Replace(@"#", string.Empty);

                int qtyValue;

                bool res = int.TryParse(value, out qtyValue);

                var sq = new ScanQuantity();
                if (res)
                {
                    sq.Quantity = qtyValue;
                }

                return sq;
            }

            // Explicit Job request.
            if (code.StartsWith("job:"))
            {
                return new ScanJob { Data = code.RemovePrefix("job:") };
            }

            // Sql query.
            if (code.StartsWith("^"))
            {
                return new ScanSQL { Data = code.TrimStart('^') };
            }

            // Set ID query
            if (code.StartsWith("@"))
            {
                var skv = new ScanKeyValuePairs();
                var strippedCode = code.RemovePrefix("@");
                if (strippedCode.Contains(":"))
                {
                    var parts = strippedCode.Split(new[] { ':' }, 2);
                    {
                        if (parts.Length > 1)
                        {
                            if (!string.IsNullOrEmpty(parts[0]))
                            {
                                skv.Add(parts[0], parts[1]);
                            }
                        }
                    }
                }

                return skv;
            }

            // Possible queue request.
            if (usingQueue)
            {
                var qc = new ScanQueueInfo();
                if (code.Contains("|"))
                {
                    var twoPart = code.Split(Convert.ToChar("|"));
                    if (twoPart.Length > 1)
                    {
                        int result;
                        if (int.TryParse(twoPart[1], out result))
                        {
                            qc.Data = twoPart[0];
                            qc.Quantity = result;
                        }
                    }
                }
                else
                {
                    qc.Data = code;
                    qc.Quantity = 1;
                }

                return qc;
            }

            // A simple mark mode object.
            if (simpleMarkMode)
            {
                var smm = new ScanSimpleMark(stripPrefixandSuffix, simplePrefixSuffix);
                smm.Add(code);

                return smm;
            }

            if (sqlServer)
            {
                return new ScanSQL { Data = code };
            }
            else
            {
                // Basic job load.
                return new ScanJob { Data = code };
            }
        }
    }
}
