﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tykma.Icon.BoxFill.Tests
{
    using Tykma.Core.Entity;

    [TestClass]
    public class BoxFillingTests
    {
        [TestMethod]
        public void TestBaseGetOffsetToTextObjectCenteredInBoundingBox()
        {
            var box = new EntityInformation { MinX = -50, MinY = -50, MaxX = 50, MaxY = 50 };

            var ent = new EntityInformation { MinX = 0, MinY = 0, MaxX = 10, MaxY = 10 };


            var boxfill = new BoundaryBoxFiller(box, ent);

            var offset = boxfill.GetOffsetToTextObjectCenteredInBoundingBox();

            Assert.AreEqual(-5, offset.X, "X offset wasn't -5");
            Assert.AreEqual(-5, offset.Y, "Y Offset wasn't -5");
        }

        [TestMethod]
        public void TestGettingOffsetWithAllZeroes()
        {
            var box = new EntityInformation { MinX = 0, MinY = 0, MaxX = 0, MaxY = 0 };

            var ent = new EntityInformation { MinX = 0, MinY = 0, MaxX = 0, MaxY = 0 };


            var boxfill = new BoundaryBoxFiller(box, ent);

            var offset = boxfill.GetOffsetToTextObjectCenteredInBoundingBox();

            Assert.AreEqual(0, offset.X, "X offset wasn't 0");
            Assert.AreEqual(0, offset.Y, "Y Offset wasn't 0");
        }

        [TestMethod]
        public void TestGettingScaleSize()
        {
            var box = new EntityInformation { MinX = -50, MinY = -50, MaxX = 50, MaxY = 50 };

            var ent = new EntityInformation { MinX = 0, MinY = 0, MaxX = 10, MaxY = 10 };


            var boxfill = new BoundaryBoxFiller(box, ent);

            var scale = boxfill.GetScaleSize();

            Assert.AreEqual(10, scale.X);
            Assert.AreEqual(10, scale.Y);
        }

        [TestMethod]
        public void TestGettingScaleSizeTwo()
        {
            var box = new EntityInformation { MinX = -25, MinY = -25, MaxX = 25, MaxY = 25 };

            var ent = new EntityInformation { MinX = 0, MinY = 0, MaxX = 10, MaxY = 10 };


            var boxfill = new BoundaryBoxFiller(box, ent);

            var scale = boxfill.GetScaleSize();

            Assert.AreEqual(5, scale.X);
            Assert.AreEqual(5, scale.Y);
        }
    }
}
