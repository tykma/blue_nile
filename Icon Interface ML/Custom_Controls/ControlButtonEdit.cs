﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlButtonEdit.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    using System.ComponentModel;
    using System.Drawing;

    using Icon_Interface.Properties;

    /// <summary>
    /// The control button: edit.
    /// </summary>
    public class ControlButtonEdit : ControlButtonBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlButtonEdit"/> class.
        /// </summary>
        public ControlButtonEdit()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// The initialize component.
        /// </summary>
        private void InitializeComponent()
        {
            ComponentResourceManager resources = new ComponentResourceManager(typeof(ControlButtonEdit));
            ((ISupportInitialize)this.ux_Image_Icon).BeginInit();
            this.SuspendLayout();

            // ux_Image_Icon
            this.ux_Image_Icon.Image = (Image)resources.GetObject("ux_Image_Icon.Image");

            // ux_Label_Action
            this.ux_Label_Action.Text = Resources.Button_Edit;

            // ControlButtonEdit
            this.AutoScaleDimensions = new SizeF(6F, 13F);
            this.Name = "ControlButtonEdit";
            ((ISupportInitialize)this.ux_Image_Icon).EndInit();
            this.ResumeLayout(false);
        }
    }
}
