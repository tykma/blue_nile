﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScanJob.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.ScanParse
{
    /// <summary>
    /// Scanned data is a job.
    /// </summary>
    public class ScanJob : IBaseDataReturn
    {
        /// <summary>
        /// Gets or sets the SQL command.
        /// </summary>
        /// <value>
        /// The data command.
        /// </value>
        public string Data { get; set; }
    }
}
