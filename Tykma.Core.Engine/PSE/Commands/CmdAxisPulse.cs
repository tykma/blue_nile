﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdAxisPulse.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// Command to move an axis to a pulse.
    /// </summary>
    internal class CmdAxisPulse : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdAxisPulse"/> class.
        /// </summary>
        /// <param name="lmcApi">
        /// The laser marking API.
        /// </param>
        /// <param name="axisToMove">
        /// The axis to move.
        /// </param>
        /// <param name="pulsesToMoveTo">
        /// The pulses to move to.
        /// </param>
        public CmdAxisPulse(IntPtr lmcApi, int axisToMove, double pulsesToMoveTo)
            : base(lmcApi)
        {
            this.AxisToMove = axisToMove;
            this.PulsesMoveTo = pulsesToMoveTo;
        }

                /// <summary>
        /// Move axis to specified pulse.
        /// </summary>
        /// <param name="axis">The axis.</param>
        /// <param name="goalPos">The pulse goal.</param>
        /// <returns>Return code.</returns>
        private delegate int LMC1ApiAxisMoveToPulse(int axis, double goalPos);

        /// <summary>
        /// Gets the axis to move.
        /// </summary>
        /// <value>
        /// The axis to move.
        /// </value>
        private int AxisToMove { get; }

        /// <summary>
        /// Gets the pulses move to.
        /// </summary>
        /// <value>
        /// The pulses move to.
        /// </value>
        private double PulsesMoveTo { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="EngineNotConnectedException">
        /// Engine not connected exception.
        /// </exception>
        /// <exception cref="MethodFaultException">
        /// Problem moving axis to the pulse.
        /// </exception>
        internal bool Execute()
        {
            var moveResult = this.LoadFunction<LMC1ApiAxisMoveToPulse>("lmc1_AxisMoveToPulse");

            if (moveResult == null)
            {
                throw new EngineNotConnectedException();
            }

            var rm = this.GetMiniLaseReturnCode(moveResult(this.AxisToMove, this.PulsesMoveTo));

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
