﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdExecMarkObject.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.ExceptionServices;
    using System.Runtime.InteropServices;

    using Tykma.Core.Engine.PSE.Commands;

    /// <summary>
    /// Command to start a marking cycle.
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal class CmdExecMarkObject : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdExecMarkObject" /> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking API.</param>
        /// <param name="entName">Entity name.</param>
        public CmdExecMarkObject(IntPtr lmcApi, string entName)
            : base(lmcApi)
        {
            this.EntityName = entName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CmdExecMarkObject"/> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking API.</param>
        public CmdExecMarkObject(IntPtr lmcApi)
            : base(lmcApi)
        {
            this.EntityName = string.Empty;
        }



        /// <summary>
        /// Gets or sets the entity name.
        /// </summary>
        public string EntityName { get; set; }


        [DllImport("MarkPSE", EntryPoint = "lmc1_MarkEntity", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern int MarkEntity(string EntName);

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>A return code message.</returns>
        [HandleProcessCorruptedStateExceptions]
        public bool Execute()
        {
            try
            {
                int result = MarkEntity(this.EntityName);

                if (result != 0)
                {
                    var rm = this.GetMiniLaseReturnCode(result);
                    throw new Exception(rm.ReturnDescription);
                }
                else
                {
                    return true;
                }
            }
            catch (AccessViolationException)
            {
                return false;
            }
        }
    }
}
