﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IdToTest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.ScanParse
{
    using Tykma.Core.Entity;

    /// <summary>
    /// Extend entity info to hold expected values.
    /// </summary>
    internal class IdToTest : EntityInformation
    {
        /// <summary>
        /// Gets the expected name.
        /// </summary>
        /// <value>
        /// The expected name.
        /// </value>
        internal string ExpectedName => this.Name;

        /// <summary>
        /// Gets the expected value.
        /// </summary>
        /// <value>
        /// The expected value.
        /// </value>
        internal string ExpectedValue => this.Value;
    }
}
