﻿// <copyright file="GetValueOrDefaultTest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Test.UnitTesting.HTTPD
{
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Tykma.Icon.TCPCommand;

    /// <summary>
    /// Test the get value or default dictionary extension.
    /// </summary>
    [TestClass]
    public class GetValueOrDefaultTest
    {
        /// <summary>
        /// Test basic useage of get value or default.
        /// </summary>
        [TestMethod]
        public void GetDefaultValueTest()
        {
            var dict = new Dictionary<int, string> { { 1, "abc" } };

            var value = dict.GetValueOrDefault(1);

            Assert.AreEqual("abc", value);

            var value2 = dict.GetValueOrDefault(2, "abc123");

            Assert.AreEqual("abc123", value2);

            var value3 = dict.GetValueOrDefault(1, "default");

            Assert.AreEqual("abc", value3);
        }

        /// <summary>
        /// Test get value or default with a null dictionary.
        /// </summary>
        [TestMethod]
        public void GetDefaultValueNullDictionaryTest()
        {
            var dict = new Dictionary<int, string>();
            dict = null;

            var value = dict.GetValueOrDefault(1, "ZZ");

            Assert.AreEqual("ZZ", value);
        }
    }
}
