﻿// <copyright file="JobStatus.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Core.Engine
{
    /// <summary>
    /// Class which holds the status of a currently loaded job.
    /// Not set directly in the laser engine interfaces but used by the UX.
    /// The status is currently exposed through the TCP/IP interface
    /// and is most useful for integration.
    /// </summary>
    public static class JobStatus
    {
        /// <summary>
        /// Job status enumerators.
        /// </summary>
        public enum Status
        {
            /// <summary>
            /// The job was loaded.
            /// </summary>
            Loaded,

            /// <summary>
            /// The job data was set.
            /// </summary>
            DataSet,

            /// <summary>
            /// Job has not been loaded.
            /// </summary>
            NotLoaded,

            /// <summary>
            /// Job has been marked.
            /// </summary>
            Marked,

            /// <summary>
            /// Job has been traced.
            /// </summary>
            Traced,

            /// <summary>
            /// The job was enabled.
            /// </summary>
            Enabled,

            /// <summary>
            /// The job was stopped
            /// </summary>
            Stopped
        }
    }
}
