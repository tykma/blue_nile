﻿namespace Icon_Interface.Forms
{
    partial class Nudger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Nudger));
            this.ux_Nudger_Main = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Pic_NudgeRight = new System.Windows.Forms.PictureBox();
            this.ux_Pic_NudgeLeft = new System.Windows.Forms.PictureBox();
            this.ux_Pic_NudgeUp = new System.Windows.Forms.PictureBox();
            this.ux_Pic_NudgeDown = new System.Windows.Forms.PictureBox();
            this.ux_tlp_Rotate = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Pic_NudgeCCW = new System.Windows.Forms.PictureBox();
            this.ux_Pic_NudgeCW = new System.Windows.Forms.PictureBox();
            this.ux_lbl_faster = new System.Windows.Forms.Label();
            this.ux_Nudger_Main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_NudgeRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_NudgeLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_NudgeUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_NudgeDown)).BeginInit();
            this.ux_tlp_Rotate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_NudgeCCW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_NudgeCW)).BeginInit();
            this.SuspendLayout();
            // 
            // ux_Nudger_Main
            // 
            this.ux_Nudger_Main.ColumnCount = 3;
            this.ux_Nudger_Main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_Nudger_Main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.ux_Nudger_Main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.ux_Nudger_Main.Controls.Add(this.ux_Pic_NudgeRight, 2, 1);
            this.ux_Nudger_Main.Controls.Add(this.ux_Pic_NudgeLeft, 0, 1);
            this.ux_Nudger_Main.Controls.Add(this.ux_Pic_NudgeUp, 1, 0);
            this.ux_Nudger_Main.Controls.Add(this.ux_Pic_NudgeDown, 1, 2);
            this.ux_Nudger_Main.Controls.Add(this.ux_tlp_Rotate, 1, 1);
            this.ux_Nudger_Main.Controls.Add(this.ux_lbl_faster, 0, 3);
            this.ux_Nudger_Main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Nudger_Main.Location = new System.Drawing.Point(0, 0);
            this.ux_Nudger_Main.Name = "ux_Nudger_Main";
            this.ux_Nudger_Main.RowCount = 4;
            this.ux_Nudger_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_Nudger_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_Nudger_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_Nudger_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.ux_Nudger_Main.Size = new System.Drawing.Size(234, 211);
            this.ux_Nudger_Main.TabIndex = 0;
            // 
            // ux_Pic_NudgeRight
            // 
            this.ux_Pic_NudgeRight.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Pic_NudgeRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Pic_NudgeRight.Image = global::Icon_Interface.Properties.ImageResources.forwardarrow;
            this.ux_Pic_NudgeRight.Location = new System.Drawing.Point(158, 66);
            this.ux_Pic_NudgeRight.Name = "ux_Pic_NudgeRight";
            this.ux_Pic_NudgeRight.Size = new System.Drawing.Size(73, 57);
            this.ux_Pic_NudgeRight.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Pic_NudgeRight.TabIndex = 0;
            this.ux_Pic_NudgeRight.TabStop = false;
            this.ux_Pic_NudgeRight.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PicNudgeRightMouseDown);
            this.ux_Pic_NudgeRight.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PicNudgeRightMouseUp);
            // 
            // ux_Pic_NudgeLeft
            // 
            this.ux_Pic_NudgeLeft.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Pic_NudgeLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Pic_NudgeLeft.Image = global::Icon_Interface.Properties.ImageResources.left_arrow;
            this.ux_Pic_NudgeLeft.Location = new System.Drawing.Point(3, 66);
            this.ux_Pic_NudgeLeft.Name = "ux_Pic_NudgeLeft";
            this.ux_Pic_NudgeLeft.Size = new System.Drawing.Size(71, 57);
            this.ux_Pic_NudgeLeft.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Pic_NudgeLeft.TabIndex = 1;
            this.ux_Pic_NudgeLeft.TabStop = false;
            this.ux_Pic_NudgeLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PicNudgeLeftMouseDown);
            this.ux_Pic_NudgeLeft.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PicNudgeLeftMouseUp);
            // 
            // ux_Pic_NudgeUp
            // 
            this.ux_Pic_NudgeUp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Pic_NudgeUp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Pic_NudgeUp.Image = global::Icon_Interface.Properties.ImageResources.up_arrow;
            this.ux_Pic_NudgeUp.Location = new System.Drawing.Point(80, 3);
            this.ux_Pic_NudgeUp.Name = "ux_Pic_NudgeUp";
            this.ux_Pic_NudgeUp.Size = new System.Drawing.Size(72, 57);
            this.ux_Pic_NudgeUp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Pic_NudgeUp.TabIndex = 2;
            this.ux_Pic_NudgeUp.TabStop = false;
            this.ux_Pic_NudgeUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PicNudgeUpMouseDown);
            this.ux_Pic_NudgeUp.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PicNudgeUpMouseUp);
            // 
            // ux_Pic_NudgeDown
            // 
            this.ux_Pic_NudgeDown.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Pic_NudgeDown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Pic_NudgeDown.Image = global::Icon_Interface.Properties.ImageResources.down_arrow;
            this.ux_Pic_NudgeDown.Location = new System.Drawing.Point(80, 129);
            this.ux_Pic_NudgeDown.Name = "ux_Pic_NudgeDown";
            this.ux_Pic_NudgeDown.Size = new System.Drawing.Size(72, 57);
            this.ux_Pic_NudgeDown.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Pic_NudgeDown.TabIndex = 3;
            this.ux_Pic_NudgeDown.TabStop = false;
            this.ux_Pic_NudgeDown.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PicNudgeDownMouseDown);
            this.ux_Pic_NudgeDown.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PicNudgeDownMouseUp);
            // 
            // ux_tlp_Rotate
            // 
            this.ux_tlp_Rotate.ColumnCount = 2;
            this.ux_tlp_Rotate.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ux_tlp_Rotate.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ux_tlp_Rotate.Controls.Add(this.ux_Pic_NudgeCCW, 0, 0);
            this.ux_tlp_Rotate.Controls.Add(this.ux_Pic_NudgeCW, 1, 0);
            this.ux_tlp_Rotate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_tlp_Rotate.Location = new System.Drawing.Point(80, 66);
            this.ux_tlp_Rotate.Name = "ux_tlp_Rotate";
            this.ux_tlp_Rotate.RowCount = 1;
            this.ux_tlp_Rotate.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ux_tlp_Rotate.Size = new System.Drawing.Size(72, 57);
            this.ux_tlp_Rotate.TabIndex = 4;
            // 
            // ux_Pic_NudgeCCW
            // 
            this.ux_Pic_NudgeCCW.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Pic_NudgeCCW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Pic_NudgeCCW.Image = ((System.Drawing.Image)(resources.GetObject("ux_Pic_NudgeCCW.Image")));
            this.ux_Pic_NudgeCCW.Location = new System.Drawing.Point(3, 3);
            this.ux_Pic_NudgeCCW.Name = "ux_Pic_NudgeCCW";
            this.ux_Pic_NudgeCCW.Size = new System.Drawing.Size(30, 51);
            this.ux_Pic_NudgeCCW.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Pic_NudgeCCW.TabIndex = 5;
            this.ux_Pic_NudgeCCW.TabStop = false;
            this.ux_Pic_NudgeCCW.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PicNudgeCcwMouseDown);
            this.ux_Pic_NudgeCCW.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PicNudgeCcwMouseUp);
            // 
            // ux_Pic_NudgeCW
            // 
            this.ux_Pic_NudgeCW.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Pic_NudgeCW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Pic_NudgeCW.Image = global::Icon_Interface.Properties.ImageResources.rotate_cw1;
            this.ux_Pic_NudgeCW.Location = new System.Drawing.Point(39, 3);
            this.ux_Pic_NudgeCW.Name = "ux_Pic_NudgeCW";
            this.ux_Pic_NudgeCW.Size = new System.Drawing.Size(30, 51);
            this.ux_Pic_NudgeCW.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Pic_NudgeCW.TabIndex = 4;
            this.ux_Pic_NudgeCW.TabStop = false;
            this.ux_Pic_NudgeCW.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PicNudgeCwMouseDown);
            this.ux_Pic_NudgeCW.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PicNudgeCwMouseUp);
            // 
            // ux_lbl_faster
            // 
            this.ux_lbl_faster.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_lbl_faster.AutoSize = true;
            this.ux_Nudger_Main.SetColumnSpan(this.ux_lbl_faster, 3);
            this.ux_lbl_faster.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_lbl_faster.Location = new System.Drawing.Point(36, 193);
            this.ux_lbl_faster.Name = "ux_lbl_faster";
            this.ux_lbl_faster.Size = new System.Drawing.Size(162, 13);
            this.ux_lbl_faster.TabIndex = 5;
            this.ux_lbl_faster.Text = "Hold shift for broader adjustment.";
            // 
            // Nudger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 211);
            this.Controls.Add(this.ux_Nudger_Main);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(200, 200);
            this.Name = "Nudger";
            this.Text = "Nudger";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NudgerFormClosing);
            this.Load += new System.EventHandler(this.NudgerLoad);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NudgerKeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.NudgerKeyUp);
            this.ux_Nudger_Main.ResumeLayout(false);
            this.ux_Nudger_Main.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_NudgeRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_NudgeLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_NudgeUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_NudgeDown)).EndInit();
            this.ux_tlp_Rotate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_NudgeCCW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_NudgeCW)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel ux_Nudger_Main;
        private System.Windows.Forms.PictureBox ux_Pic_NudgeRight;
        private System.Windows.Forms.PictureBox ux_Pic_NudgeLeft;
        private System.Windows.Forms.PictureBox ux_Pic_NudgeUp;
        private System.Windows.Forms.PictureBox ux_Pic_NudgeDown;
        private System.Windows.Forms.TableLayoutPanel ux_tlp_Rotate;
        private System.Windows.Forms.PictureBox ux_Pic_NudgeCCW;
        private System.Windows.Forms.PictureBox ux_Pic_NudgeCW;
        private System.Windows.Forms.Label ux_lbl_faster;
    }
}