﻿// <copyright file="GlobalExtensions.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Icon.Globals
{
    using System.Globalization;

    /// <summary>
    /// Extension for global variable items.
    /// </summary>
    public static class GlobalExtensions
    {
        /// <summary>
        /// Increments a global variable item.
        /// </summary>
        /// <param name="gv">The gv.</param>
        /// <returns>An incremented global variable item.</returns>
        public static GlobalVariableItem Increment(this GlobalVariableItem gv)
        {
            long result;
            if (long.TryParse(gv.Value, out result))
            {
                gv.Value = (result + 1).ToString(CultureInfo.InvariantCulture).PadLeft(gv.Value.TrimStart('-').Length, '0');
            }

            return gv;
        }
    }
}
