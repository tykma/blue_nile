﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdAxisGetCoordinate.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;

    using CustomExceptions;

    /// <summary>
    /// Get the coordinate of a specified axis.
    /// </summary>
    internal class CmdAxisGetCoordinate : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdAxisGetCoordinate"/> class.
        /// </summary>
        /// <param name="lmcApi">
        /// The laser marking API.
        /// </param>
        /// <param name="axis">
        /// The axis.
        /// </param>
        public CmdAxisGetCoordinate(IntPtr lmcApi, int axis)
            : base(lmcApi)
        {
            this.AxisToGetCoordinateFrom = axis;
        }

        /// <summary>
        /// Get the coordinate of a specified axis.
        /// </summary>
        /// <param name="axis">The axis.</param>
        /// <returns>The coordinate.</returns>
        private delegate double LMC1ApiGetAxisCoor(int axis);

        /// <summary>
        /// Gets the axis to get coordinate from.
        /// </summary>
        /// <value>
        /// The axis to get coordinate from.
        /// </value>
        private int AxisToGetCoordinateFrom { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Axis coordinate.</returns>
        internal double Execute()
        {
            var coor = this.LoadFunction<LMC1ApiGetAxisCoor>("lmc1_GetAxisCoor");

            if (coor == null)
            {
                throw new EngineNotConnectedException();
            }

            double loadReturn = coor(this.AxisToGetCoordinateFrom);
            return loadReturn;
        }
    }
}
