﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISqlClient.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.SQL.Client
{
    /// <summary>
    /// The SQL client interface.
    /// </summary>
    public interface ISqlClient
    {
        /// <summary>
        /// Gets the data.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// The <see cref="SQLRetrievedData" />.
        /// </returns>
        SQLRetrievedData GetData(string id);

        /// <summary>
        /// Writes the data.
        /// </summary>
        /// <param name="primkey">The primary key.</param>
        /// <param name="data">The data.</param>
        /// <param name="column">The column.</param>
        /// <returns>Sucess state.</returns>
        bool SetData(string primkey, string data, string column);
    }
}
