﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEvoCommand.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.ElectroxEngine.Commands
{
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// Evolution command interface.
    /// </summary>
    public interface IEvoCommand : IDisposable
    {
        /// <summary>
        /// Gets the command to send.
        /// </summary>
        /// <value>
        /// The command to send.
        /// </value>
        string CommandToSend { get; }

        /// <summary>
        /// Gets the command finish time out.
        /// </summary>
        /// <value>
        /// The command finish time out.
        /// </value>
        double CommandFinishTimeOut { get; }

        /// <summary>
        /// Gets a value indicating whether [command responds with data].
        /// </summary>
        /// <value>
        /// <c>true</c> if [command responds with data]; otherwise, <c>false</c>.
        /// </value>
        bool CommandRespondsWithData { get; }

        /// <summary>
        /// Gets a value indicating whether [command responds with code].
        /// </summary>
        /// <value>
        /// <c>true</c> if [command responds with code]; otherwise, <c>false</c>.
        /// </value>
        bool CommandRespondsWithCode { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<DataReceived> Execute();
    }
}
