﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Tykma.Core.Entity;

namespace Tykma.Maths.EntitySize
{
    /// <summary>
    /// Entity size helpers.
    /// </summary>
    public static class SizeHelpers
    {
        /// <summary>
        /// Get the rectangular boundary of an entity.
        /// </summary>
        /// <param name="ent">Entity.</param>
        /// <returns>Rectangle object.</returns>
        public static RectangleF GetEntityRectangle(EntityInformation ent)
        {
            var p1 = new PointF((float)ent.MinX, (float)ent.MinY);
            var p2 = new PointF((float)ent.MaxX, (float)ent.MaxY);

            float bottom = Math.Min(p1.Y, p2.Y);
            float top = Math.Max(p1.Y, p2.Y);
            float left = Math.Min(p1.X, p2.X);
            float right = Math.Max(p1.X, p2.X);

            RectangleF rect = RectangleF.FromLTRB(left, top, right, bottom);

            return rect;
        }

        /// <summary>
        /// Get boundary rectangle from a list of entities.
        /// </summary>
        /// <param name="ents"></param>
        /// <returns></returns>
        public static RectangleF GetEntityRectangle(IList<EntityInformation> ents)
        {
            if (ents == null || ents.Count == 0)
            {
                return new RectangleF();
            }

            double maxx = ents.Select(entity => entity.MaxX).Concat(new double[] { }).Max();
            double minx = ents.Select(entity => entity.MinX).Concat(new double[] { }).Min();
            double maxy = ents.Select(entity => entity.MaxY).Concat(new double[] { }).Max();
            double miny = ents.Select(entity => entity.MinY).Concat(new double[] { }).Min();

            var ent = new EntityInformation(0, null, null, minx, maxx, miny, maxy,0);

            return GetEntityRectangle(ent);
        }
    }
}
