﻿// <copyright file="GlobalSuppressions.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.SpacingRules", "SA1008:Opening parenthesis must be spaced correctly", Justification = "C#7.0 Tuples>", Scope = "member", Target = "~M:Tykma.Core.Engine.ConfigSettings.UnitTests.SEConfigurationHandlerTests.TestSettingMarkingOffsets")]