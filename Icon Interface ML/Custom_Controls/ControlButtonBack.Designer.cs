﻿namespace Icon_Interface.Custom_Controls
{
    partial class ControlButtonBack
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.ux_Image_Icon)).BeginInit();
            this.SuspendLayout();
            // 
            // ux_Image_Icon
            // 
            this.ux_Image_Icon.Image = global::Icon_Interface.Properties.ImageResources._000477_3d_glossy_blue_orb_icon_media_a_media21_arrow_back;
            // 
            // ux_Label_Action
            // 
            this.ux_Label_Action.Text = "Back";
            // 
            // ControlButtonBack
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ControlButtonBack";
            ((System.ComponentModel.ISupportInitialize)(this.ux_Image_Icon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}
