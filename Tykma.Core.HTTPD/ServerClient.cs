﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServerClient.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.HTTPD
{
    using System;
    using System.IO;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading.Tasks;

    using Tykma.Core.StringExtensions;

    /// <summary>
    /// Client class - responds to HTTP queries.
    /// </summary>
    public class ServerClient
    {
        /// <summary>
        /// The server reference.
        /// </summary>
        private readonly Server server;

        /// <summary>
        /// The socket reference.
        /// </summary>
        private readonly Socket socket;

        /// <summary>
        /// The network stream.
        /// </summary>
        private readonly NetworkStream networkStream;

        /// <summary>
        /// A stream reader.
        /// </summary>
        private readonly StreamReader streamReader;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServerClient"/> class.
        /// </summary>
        /// <param name="server">The server.</param>
        /// <param name="socket">The socket.</param>
        public ServerClient(Server server, Socket socket)
        {
            this.server = server;
            this.socket = socket;

            // Set up streams
            this.networkStream = new NetworkStream(socket, true);
            this.streamReader = new StreamReader(this.networkStream);
        }

        /// <summary>
        /// Get's the requested string.
        /// </summary>Y
        /// <returns>Requested string.</returns>
        public async Task<string> Do()
        {
            // We are executed on a separate thread from listener, but will release this back to the threadpool as often as we can.
            for (; ;)
            {
                var line = await this.streamReader.ReadLineAsync();
                if (!this.socket.Connected || line == null)
                {
                    return string.Empty;
                }

                // You probably want to throttle incoming data (if someone dumped a large file with no GET you would be reading forever)

                // Look for GET-portion of header (first line) that contains the filename being requested
                if (line.ToUpperInvariant().StartsWith("GET /") && line.ToUpperInvariant().EndsWith("HTTP/1.1"))
                {
                    var file = line.Substring(@from: @"GET /", until: "HTTP/1.1");

                    // Default document is index.html
                    if (string.IsNullOrWhiteSpace(file))
                    {
                        file = "index.html";
                    }

                    return file;
                }
                else
                {
                    return "index.html";
                }
            }
        }

        /// <summary>
        /// Sends the response.
        /// </summary>
        /// <param name="responseCode">The response code.</param>
        /// <param name="contentType">Type of the content.</param>
        /// <param name="msg">The MSG.</param>
        /// <returns>A task.</returns>
        public async Task SendResponse(string responseCode, string contentType, string msg)
        {
            byte[] data = Encoding.ASCII.GetBytes(msg);
            await this.SendResponse(responseCode, contentType, data);
        }

        /// <summary>
        /// Sends a file.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>A file.</returns>
        public async Task SendFile(byte[] file)
        {
            // Get info and assemble header.
            // ReSharper disable once InconsistentNaming
            const string ContentType = "image/x-ms-bmp";

            // ReSharper disable once InconsistentNaming
            const string ResponseCode = "200 OK";

            await this.SendResponse(ResponseCode, ContentType, file);
        }

        /// <summary>
        /// Sends a response.
        /// </summary>
        /// <param name="responseCode">The response code.</param>
        /// <param name="contentType">Type of the content.</param>
        /// <param name="data">The data.</param>
        /// <returns>
        /// A task.
        /// </returns>
        private async Task SendResponse(string responseCode, string contentType, byte[] data)
        {
            string header = $"HTTP/1.1 {responseCode}\r\n" + $"Server: {this.server.ServerName}\r\n"
                            + $"Content-Length: {data.Length}\r\n" + $"Content-Type: {contentType}\r\n"
                            + "Keep-Alive: Close\r\n" + "\r\n";

            // Send header & data
            var headerBytes = Encoding.ASCII.GetBytes(header);
            if (this.networkStream != null)
            {
                try
                {
                    await this.networkStream.WriteAsync(headerBytes, 0, headerBytes.Length);
                    await this.networkStream.WriteAsync(data, 0, data.Length);
                    await this.networkStream.FlushAsync();
                }
                catch (ObjectDisposedException)
                {
                    // error.
                }
                catch (IOException)
                {
                    // error
                }

                // Close connection (we don't support keep-alive)
                this.networkStream.Dispose();
            }

            this.streamReader.Dispose();
        }
    }
}
