﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EngineTypes.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine
{
    /// <summary>
    /// Class that identifies the possible engine types that are implemented.
    /// It is exposed in the laser interface class and should be
    /// set in each individual engine handling class.
    /// Required because there are slight differences in handling of different engines
    /// even though they implement the same interface.
    /// </summary>
    public static class EngineTypes
    {
        /// <summary>
        /// Possible engine types.
        /// </summary>
        public enum EngineType
        {
            /// <summary>
            /// MiniLase pro SE.
            /// </summary>
            PSE,

            /// <summary>
            /// Lighter through TCP/IP.
            /// </summary>
            LighterEthernet,

            /// <summary>
            /// A fake engine.
            /// </summary>
            FakeEngine,

            /// <summary>
            /// Electrox card communication.
            /// </summary>
            ElectroxEngine
        }
    }
}
