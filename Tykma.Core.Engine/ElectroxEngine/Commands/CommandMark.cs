﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandMark.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   The command to start a mark.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.ElectroxEngine.Commands
{
    using System;
    using System.Threading.Tasks;

    using Electrox.Evolution.Communications;

    /// <summary>
    /// The command mark.
    /// </summary>
    public class CommandMark : IEvoCommand
    {
        /// <summary>
        /// The send string.
        /// </summary>
        private const string SendString = "MARK";

        /// <summary>
        /// Command receives data.
        /// </summary>
        private const bool CommandReceivesData = false;

        /// <summary>
        /// The command responds with code
        /// </summary>
        private const bool CommandReceivesCode = false;

        /// <summary>
        /// The timeout for transmission.
        /// </summary>
        private readonly TimeSpan transmissionTimeout = new TimeSpan(0, 0, 0, 0, 500);

        /// <summary>
        /// The send command
        /// </summary>
        private readonly CommandBase sendCommand;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandMark" /> class.
        /// </summary>
        /// <param name="comms">The communications library.</param>
        public CommandMark(Communications comms)
        {
            this.sendCommand = new CommandBase(comms)
                                   {
                                       CommandToSend = this.CommandToSend,
                                       CommandRespondsWithData = this.CommandRespondsWithData,
                                       CommandRespondsWithCode = this.CommandRespondsWithCode,
                                       CommandFinishTimeOut = this.CommandFinishTimeOut
                                   };
        }

        /// <summary>
        /// Gets the command to send.
        /// </summary>
        public string CommandToSend
        {
            get
            {
                return string.Format("{0}", SendString);
            }
        }

        /// <summary>
        /// Gets the command finish time out.
        /// </summary>
        /// <value>
        /// The command finish time out.
        /// </value>
        public double CommandFinishTimeOut
        {
            get
            {
                return this.transmissionTimeout.TotalMilliseconds;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [command responds with data].
        /// </summary>
        /// <value>
        /// <c>true</c> if [command responds with data]; otherwise, <c>false</c>.
        /// </value>
        public bool CommandRespondsWithData
        {
            get
            {
                return CommandReceivesData;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [command responds with code].
        /// </summary>
        /// <value>
        /// <c>true</c> if [command responds with code]; otherwise, <c>false</c>.
        /// </value>
        public bool CommandRespondsWithCode
        {
            get
            {
                return CommandReceivesCode;
            }
        }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success st</returns>
        public async Task<DataReceived> Execute()
        {
            return await this.sendCommand.Execute();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.sendCommand.Dispose();
        }
    }
}
