﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JobList.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.JobList
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// A class that handles structuring the job list.
    /// It will split the job list into multiple lists - of a certain amount.
    /// </summary>
    public class JobList
    {
        /// <summary>
        /// The master list.
        /// </summary>
        private List<List<string>> masterList = new List<List<string>>();

        /// <summary>
        /// Initializes a new instance of the <see cref="JobList" /> class.
        /// </summary>
        /// <param name="listSplitLength">Length of the list split.</param>
        public JobList(int listSplitLength)
        {
            this.ListSplitLength = listSplitLength;
        }

        /// <summary>
        /// Gets how many total lists of jobs are there.
        /// </summary>
        /// <value>
        /// The list count.
        /// </value>
        public int ListCount => this.masterList.Count;

        /// <summary>
        /// Sets the list to parse.
        /// </summary>
        /// <value>
        /// The list to parse.
        /// </value>
        public List<string> ListToParse
        {
            set
            {
                if (value != null)
                {
                    value.Sort();
                    this.masterList = value.SplitList(this.ListSplitLength);
                }
            }
        }

        /// <summary>
        /// Gets the length of the list split.
        /// </summary>
        /// <value>
        /// The length of the list split.
        /// </value>
        private int ListSplitLength { get; }

        /// <summary>
        /// Returns the specified list.
        /// </summary>
        /// <param name="whichList">Which list.</param>
        /// <returns>
        /// A list.
        /// </returns>
        public IEnumerable<string> GetList(int whichList)
        {
            if (whichList >= 0 && this.masterList.Count > whichList)
            {
                return this.masterList[whichList];
            }

            return new List<string>();
        }

        /// <summary>
        /// Gets the entire list.
        /// </summary>
        /// <returns>Entire list.</returns>
        public List<string> GetEntireList()
        {
            return this.masterList.SelectMany(subList => subList).ToList();
        }

        /// <summary>
        /// Gets the entire list.
        /// </summary>
        /// <param name="onlyNames">if set to <c>true</c> strip extensions and paths.</param>
        /// <returns>
        /// The entire list.
        /// </returns>
        public List<string> GetEntireList(bool onlyNames)
        {
            if (onlyNames)
            {
                return this.GetEntireList().RemoveExtensions().ToList();
            }

            return this.GetEntireList();
        }
    }
}
