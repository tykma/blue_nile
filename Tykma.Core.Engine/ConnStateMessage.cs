﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConnStateMessage.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine
{
    using System;

    /// <summary>
    /// Connection status.
    /// </summary>
    public enum ConnectionState
    {
        /// <summary>
        /// ML connected.
        /// </summary>
        Connected,

        /// <summary>
        /// ML not connected.
        /// </summary>
        NotConnected,
    }

    /// <summary>
    /// A class which holds a connection status message.
    /// This class is passed in an event to the UX controls on completion of a connection method.
    /// It will identify the status of the connection - whether it was successful or not
    /// and what fault exists if the connection is not successful.
    /// Connection status is an event to accommodate the long delay that is possible
    /// when establishing a connection (USB initialization, network time outs)
    /// On the UX side - nothing should be happening until this connection is made.
    /// </summary>
    public class ConnStateMessage : EventArgs
    {
        /// <summary>
        /// Gets or sets the STATUS enumerator.
        /// </summary>
        /// <value>
        /// The STATUS string.
        /// </value>
        public ConnectionState Status { get; set; }

        /// <summary>
        /// Gets or sets the fault.
        /// </summary>
        /// <value>
        /// The fault.
        /// </value>
        public string Fault { get; set; }

        /// <summary>
        /// Gets or sets the connection message.
        /// </summary>
        /// <value>
        /// The connection message.
        /// </value>
        public string ConnectionMessage { get; set; }
    }
}
