﻿using System.Collections.Generic;
using System.IO.Abstractions.TestingHelpers;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tykma.Icon.PLC.MessagesDB.Tests
{
    [TestClass]
    public class PLCMessagesTest
    {
        /// <summary>
        /// Test database is created if it does not exist.
        /// </summary>
        [TestMethod]
        public void TestDatabaseCreation()
        {
            // Arrange
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
            });

            var messages = new MessagesDB(@"C:\messages.csv", fileSystem);

            var data = fileSystem.File.ReadAllLines(@"C:\messages.csv");

            Assert.AreEqual(2, data.Length);

            Assert.AreEqual("Code,Color,Message", data[0]);
            Assert.AreEqual("0,White,Ready", data[1]);
        }

        [TestMethod]
        [DataRow(0, "White", "Ready")]
        [DataRow(1, "Red", "Major Fault")]
        public void TestDatabaseLoading(int statusCode, string color, string msg)
        {

            var sb = new StringBuilder();
            sb.AppendLine("Code,Color,Message");
            sb.AppendLine("0,White,Ready");
            sb.AppendLine("1,Red,Major Fault");

            // Arrange
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { @"c:\messages.csv", new MockFileData(sb.ToString()) },

            });

            var messages = new MessagesDB(@"C:\messages.csv", fileSystem);
            messages.LoadDB();

            Assert.AreEqual(msg, messages.GetMessage(statusCode).Message);
            Assert.AreEqual(color, messages.GetMessage(statusCode).Color);
        }
    }
}
