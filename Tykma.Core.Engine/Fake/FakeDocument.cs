﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FakeDocument.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.Fake
{
    using System.Collections.Generic;

    using Tykma.Core.Entity;

    /// <summary>
    /// The existing document.
    /// </summary>
    public class FakeDocument
    {
        /// <summary>
        /// Gets or sets the name of the document.
        /// </summary>
        /// <value>
        /// The name of the document.
        /// </value>
        public string DocumentName { get; set; }

        /// <summary>
        /// Gets or sets the entity list.
        /// </summary>
        /// <value>
        /// The entity list.
        /// </value>
        public IEnumerable<EntityInformation> EntityList { get; set; }
    }
}
