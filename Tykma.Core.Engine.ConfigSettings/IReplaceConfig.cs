﻿// <copyright file="IReplaceConfig.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.Engine.ConfigSettings
{
    using System.IO;

    /// <summary>
    /// Configuration replacer interface.
    /// </summary>
    public interface IReplaceConfig
    {
        /// <summary>
        /// Replaces the configuration file.
        /// </summary>
        /// <param name="configLines">The configuration file lines.</param>
        /// <returns>Success state.</returns>
        bool ReplaceConfig(MemoryStream configLines);
    }
}
