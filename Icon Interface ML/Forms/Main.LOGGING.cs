﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.LOGGING.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Main UI logging and tiemstamping class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Drawing;
    using System.Globalization;
    using System.Text;
    using System.Windows.Forms;

    using NLog;

    using Tykma.Core.StringExtensions;

    /// <summary>
    /// Logging and timestamps.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The previous message logged.
        /// </summary>
        private string prevMessage;

        /// <summary>
        /// The previous message time.
        /// </summary>
        private DateTime prevMessageTime;

        /// <summary>
        /// Initializes the logging view.
        /// </summary>
        private void InitializeLoggingView()
        {
            // Add columns
            this.ux_EventLog.Columns.Add("Time", -1, HorizontalAlignment.Left);
            this.ux_EventLog.Columns.Add("Message", -1, HorizontalAlignment.Left);
        }

        /// <summary>
        /// Adds an event to the event log.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        /// <param name="faultMessage">Whether or not this is a fault message.</param>
        private void TimeStampEvent(string msg, bool faultMessage = false)
        {
            if (msg == this.prevMessage)
            {
                var timePassed = DateTime.Now - this.prevMessageTime;
                if (timePassed.TotalSeconds < 1)
                {
                    this.prevMessageTime = DateTime.Now;
                    return;
                }

                this.prevMessageTime = DateTime.Now;
            }
            else
            {
                this.prevMessageTime = DateTime.Now;
                this.prevMessage = msg;
            }

            var msgToAdd = msg.Clean();

            // Try and prevent object disposed exceptions.
            if (!this.Disposing && !this.IsDisposed)
            {
                this.BeginInvoke(new Action(() => this.AddTimeStamps(msgToAdd, faultMessage)));
            }
        }

        /// <summary>
        /// Adds the time stamps.
        /// </summary>
        /// <param name="msgToAdd">The MSG to add.</param>
        /// <param name="faultMessage">Is this a fault message.</param>
        private void AddTimeStamps(string msgToAdd, bool faultMessage = false)
        {
            this.ux_EventLog.SuspendLayout();

            if (this.ux_EventLog.Items.Count > 250)
            {
                this.ux_EventLog.Items.RemoveAt(0);
            }

            string time = DateTime.Now.ToString(CultureInfo.InvariantCulture);
            var item1 = new ListViewItem(new[] { time, msgToAdd }) { UseItemStyleForSubItems = false };
            item1.SubItems[1].Font = new Font(item1.Font, FontStyle.Regular);
            item1.SubItems[0].Font = new Font(item1.Font, FontStyle.Regular);

            this.ux_EventLog.Columns[1].Width = -1;
            this.ux_EventLog.Columns[1].Width = -2;

            if (faultMessage)
            {
                item1.ForeColor = Color.Red;
                item1.SubItems[1].ForeColor = Color.Red;
            }

            this.ux_EventLog.Items.Add(item1);

            this.ux_Status_Events.Font = faultMessage
                                             ? new Font(this.ux_Status_Events.Font, FontStyle.Bold)
                                             : new Font(this.ux_Status_Events.Font, FontStyle.Regular);

            this.ux_Status_Events.Text = string.Format("{0}: {1}", time, msgToAdd);

            // Scroll to the end of the listbox.
            this.ux_EventLog.EnsureVisible(this.ux_EventLog.Items.Count - 1);

            this.ux_EventLog.ResumeLayout();
        }

        /// <summary>
        /// Handles the Click event of the Context_EventLogCopy control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ContextEventLogCopyClick(object sender, EventArgs e)
        {
            if (this.ux_EventLog.SelectedItems.Count != 0)
            {
                var sb = new StringBuilder();

                foreach (ListViewItem item in this.ux_EventLog.SelectedItems)
                {
                    sb.AppendLine(string.Format("{0}: {1}", item.SubItems[0].Text, item.SubItems[1].Text));
                }

                Clipboard.SetText(sb.ToString());
            }
        }

        /// <summary>
        /// Handles the Click event of the Context_EventClear control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ContextEventClearClick(object sender, EventArgs e)
        {
            this.ux_EventLog.Items.Clear();
        }
    }
}
