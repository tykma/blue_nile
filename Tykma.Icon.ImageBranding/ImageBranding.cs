﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImageBranding.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.ImageBranding
{
    using System;
    using System.Drawing;
    using System.IO;

    using Tykma.Core.StringExtensions;

    /// <summary>
    /// Class that returns custom images.
    /// </summary>
    public class ImageBranding
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ImageBranding" /> class.
        /// </summary>
        /// <param name="settingsFolder">The settings folder.</param>
        /// <param name="instructionsFolderRTF">The instructions folder RTF.</param>
        /// <param name="instructionsFolderImg">The instructions folder image.</param>
        /// <param name="backColor">The back Color.</param>
        public ImageBranding(string settingsFolder, string instructionsFolderRTF, string instructionsFolderImg, Color backColor)
        {
            this.InstructionsFolderRTF = new DirectoryInfo(@"C:\tykma\custom\tykma_icon\rtf");
            this.InstructionsFolderImg = new DirectoryInfo(@"C:\tykma\custom\tykma_icon\img");

            try
            {
                this.ImagesMainFolder = new DirectoryInfo(Path.Combine(settingsFolder, "img"));
            }
            catch (Exception)
            {
                this.ImagesMainFolder = new DirectoryInfo(@"C:\tykma\custom\tykma_icon\img");
            }

            try
            {
                this.RTFMainFolder = new DirectoryInfo(Path.Combine(settingsFolder, "RTF"));
            }
            catch (Exception)
            {
                this.RTFMainFolder = new DirectoryInfo(@"C:\tykma\custom\tykma_icon\rtf");
            }

            if (!instructionsFolderRTF.FilePathHasInvalidChars() && !string.IsNullOrEmpty(instructionsFolderRTF))
            {
                try
                {
                    this.InstructionsFolderRTF = new DirectoryInfo(instructionsFolderRTF);
                }
                catch (Exception)
                {
                    // ignored
                }
            }

            if (!instructionsFolderImg.FilePathHasInvalidChars() && !string.IsNullOrEmpty(instructionsFolderImg))
            {
                try
                {
                    this.InstructionsFolderImg = new DirectoryInfo(instructionsFolderImg);
                }
                catch (Exception)
                {
                    // ignored
                }
            }

            this.BackColor = backColor;
        }

        /// <summary>
        /// Gets the no job loaded image.
        /// </summary>
        /// <value>
        /// The no job loaded.
        /// </value>
        public Image NoJobLoaded
        {
            get
            {
                var nojobloadedimg = new FileInfo(Path.Combine(this.ImagesMainFolder.FullName, "nojobloaded.png"));

                if (nojobloadedimg.Exists)
                {
                    return Image.FromFile(nojobloadedimg.FullName, true);
                }

                return this.DrawText("No Job \r\n Loaded", new Font("Times New Roman", 56), Color.DarkBlue, this.BackColor);
            }
        }

        /// <summary>
        /// Gets the file searching image.
        /// </summary>
        /// <value>
        /// The file searching.
        /// </value>
        public Image FileSearching
        {
            get
            {
                var searchingimg = new FileInfo(Path.Combine(this.ImagesMainFolder.FullName, "searching.png"));

                if (searchingimg.Exists)
                {
                    return Image.FromFile(searchingimg.FullName, true);
                }

                return this.DrawText("Searching", new Font("Times New Roman", 56), Color.DarkBlue, this.BackColor);
            }
        }

        /// <summary>
        /// Gets the file searching error image.
        /// </summary>
        /// <value>
        /// The file searching.
        /// </value>
        public Image FileFault
        {
            get
            {
                var fileproblemimg = new FileInfo(Path.Combine(this.ImagesMainFolder.FullName, "fileproblem.png"));

                if (fileproblemimg.Exists)
                {
                    return Image.FromFile(fileproblemimg.FullName, true);
                }

                return this.DrawText("File fault", new Font("Times New Roman", 56), Color.DarkBlue, this.BackColor);
            }
        }

        /// <summary>
        /// Gets the images main folder.
        /// </summary>
        /// <value>
        /// The images main folder.
        /// </value>
        public DirectoryInfo ImagesMainFolder { get; }

        /// <summary>
        /// Gets the RTF main folder.
        /// </summary>
        /// <value>
        /// The RTF main folder.
        /// </value>
        public DirectoryInfo RTFMainFolder { get; }

        /// <summary>
        /// Gets the instructions documents folder.
        /// </summary>
        /// <value>
        /// The instructions folder.
        /// </value>
        private DirectoryInfo InstructionsFolderRTF { get; }

        /// <summary>
        /// Gets the instructions images folder.
        /// </summary>
        /// <value>
        /// The instructions folder.
        /// </value>
        private DirectoryInfo InstructionsFolderImg { get; }

        /// <summary>
        /// Gets the 'no instruction picture'.
        /// </summary>
        /// <value>
        /// The file searching.
        /// </value>
        private Image NoInstructionPicture
        {
            get
            {
                var fileinstructionimg = new FileInfo(Path.Combine(this.ImagesMainFolder.FullName, "default.png"));

                if (fileinstructionimg.Exists)
                {
                    return Image.FromFile(fileinstructionimg.FullName, true);
                }

                return this.DrawText("No image \r\n found", new Font("Times New Roman", 56), Color.DarkBlue, this.BackColor);
            }
        }

        /// <summary>
        /// Gets the back color of the interface.
        /// </summary>
        /// <value>
        /// The color of the back.
        /// </value>
        private Color BackColor { get; }

        /// <summary>
        /// Gets the job picture.
        /// </summary>
        /// <param name="jobLoadedName">
        /// Name of the job loaded.
        /// </param>
        /// <returns>
        /// An image.
        /// </returns>
        public Image GetJobPicture(string jobLoadedName)
        {
            if (this.InstructionsFolderImg.Exists)
            {
                foreach (FileInfo fi in this.InstructionsFolderImg.GetFiles())
                {
                    if (string.Compare(Path.GetFileNameWithoutExtension(fi.Name), jobLoadedName, StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        Image img = this.FileFault;
                        try
                        {
                            img = Image.FromFile(fi.FullName);
                        }
                        catch
                        {
                            img = this.FileFault;
                        }

                        return img;
                    }
                }
            }

            return this.NoInstructionPicture;
        }

        /// <summary>
        /// Gets the RTF location.
        /// </summary>
        /// <param name="jobLoadedName">
        /// Name of the job loaded.
        /// </param>
        /// <returns>
        /// The RTF location.
        /// </returns>
        public FileInfo GetRTFLocation(string jobLoadedName)
        {
            string jobWithExtension = jobLoadedName.AddExtension("rtf");

            var rtfFile = new FileInfo(Path.Combine(this.InstructionsFolderRTF.FullName, jobWithExtension));

            if (rtfFile.Exists)
            {
                return rtfFile;
            }

            var defaultFile = new FileInfo(Path.Combine(this.RTFMainFolder.FullName, @"default.rtf"));

            if (defaultFile.Exists)
            {
                return defaultFile;
            }

            throw new FileNotFoundException();
        }

        /// <summary>
        /// Draws a text on an image.
        /// </summary>
        /// <param name="text">The text to draw.</param>
        /// <param name="font">The font.</param>
        /// <param name="textColor">Color of the text.</param>
        /// <param name="backColor">Color of the back.</param>
        /// <returns>
        /// An image with the chosen text.
        /// </returns>
        private Image DrawText(string text, Font font, Color textColor, Color backColor)
        {
            // First, create a dummy bitmap just to get a graphics object.
            Image img = new Bitmap(1, 1);
            Graphics drawing = Graphics.FromImage(img);

            // Measure the string to see how big the image needs to be
            // SizeF textSize = drawing.MeasureString(text, font);

            // Free up the dummy image and old graphics object.
            img.Dispose();
            drawing.Dispose();

            // Create a new image of the right size.
            img = new Bitmap(400, 400);

            drawing = Graphics.FromImage(img);

            // Paint the background.
            drawing.Clear(backColor);

            // Create a brush for the text.
            Brush textBrush = new SolidBrush(textColor);

            drawing.DrawString(text, font, textBrush, 0, img.Height / 2f);

            drawing.Save();

            textBrush.Dispose();
            drawing.Dispose();

            return img;
        }
    }
}
