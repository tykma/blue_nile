﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlButtonAxis.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    using Icon_Interface.Properties;

    /// <summary>
    /// Axis setting button.
    /// </summary>
    public partial class ControlButtonAxis : ControlButtonBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlButtonAxis"/> class.
        /// </summary>
        public ControlButtonAxis()
        {
            this.InitializeComponent();
            this.Customize();
        }

        /// <summary>
        /// Customize method.
        /// </summary>
        private void Customize()
        {
            this.Text = Resources.Label_Axis;
        }
    }
}
