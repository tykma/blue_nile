﻿// <copyright file="Main.Custom.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Icon_Interface.Forms
{
    using System;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Icon_Interface.General;
    using Tykma.Core.Engine;
    using Tykma.Core.Engine.CustomExceptions;
    using Tykma.Core.Entity;
    using Tykma.Custom.BlueNile.DBLogic;

    public partial class Main
    {
        private BlueNileDBLogic DBLogic { get; set; }

        public void InitializeBlueNile()
        {
            this.DBLogic = new BlueNileDBLogic(
                TykmaGlobals.SQLSettings.SQLCONNSTRING,
                TykmaGlobals.SQLSettings.SQLTABLE,
                TykmaGlobals.SQLSettings.SQLJOBCOLUMN);
        }

        private BlueNileDataObject ActiveBlueNileObject { get; set; }

        public void HandleBlueNileScannedData(string data)
        {
            BlueNileDataObject dataobject;



            try
            {
                dataobject = this.DBLogic.GetBlueNileData(data);
            }
            catch (Exception err)
            {
                this.TimeStampEvent(err.Message, true);
                Logger.Error(err.Message);
                return;
            }

            //if (Debugger.IsAttached)
            //{
            //    dataobject = new BlueNileDataObject("14K Rose Gold",
            //        "My Forever Love",
            //        10,
            //        "ENG_TEMPLATE",
            //        "Blah", 15, true, "SKU", "Arial", 10, "Arial", "14K Rose Gold");
            //}

            this.ActiveBlueNileObject = dataobject;
            this.HandleBlueNileActiveObject();
        }

        private bool DuplicateLaserSettings(string src, string dst)
        {

            try
            {
                var p = this.le.GetPenNumberFromObject(src);
                this.le.SetPenNumberToObject(dst, p);
                return true;
            }
            catch (Exception)
            {
                this.TimeStampEvent($"Problem setting laser settings");
            }

            return false;
        }

        public void PositionTraceBox()
        {
            var text = this.le.GetObjectSize("TEXT");
            var tracer = this.le.GetObjectSize("%TRACE");

            var r = text.Height / tracer.Height;
            this.le.ScaleSize("%TRACE", 0, 0, 1, r);

            text = this.le.GetObjectSize("TEXT");
            tracer = this.le.GetObjectSize("%TRACE");

            this.AlignObjects(text, tracer, EAlignType.Center);
        }

        public void SetTextFill()
        {
            for (int x = 1; x <= 3; x++)
            {
                Tykma.Core.Engine.PSE.HatchEntAttributes fill = null;

                try
                {
                    fill = this.le.GetHatchParameters("%FILL", x);
                }
                catch (Exception err)
                {
                    this.TimeStampEvent($"Problem getting fill {x}", true);
                }

                if (fill != null)
                {
                    this.DuplicateLaserSettings("%FILL", "TEXT");
                    try
                    {
                        this.le.SetHatchParameters("TEXT", fill, x);
                    }
                    catch (MethodFaultException err)
                    {
                    }
                }
            }
        }

        public enum EAlignType
        {
            Left,

            Center,
        }

        public void AlignObjects(EntityInformation refObject, EntityInformation moveObject, EAlignType alignType)
        {
            double diffX = 0;
            double diffY = 0;

            if (alignType == EAlignType.Center)
            {
                diffX = refObject.CenterX - moveObject.CenterX;
                diffY = refObject.CenterY - moveObject.CenterY;
            }
            else if (alignType == EAlignType.Left)
            {
                diffX = 0 - (moveObject.MaxX - refObject.MinX);
                diffY = 0 - (moveObject.CenterY - refObject.CenterY);
            }

            this.le.MoveObject(moveObject.Name, diffX, diffY);
        }

        public async Task<bool> HandleBlueNileActiveObject()
        {
            if (this.ActiveBlueNileObject == null)
            {
                return false;
            }

            var j = await this.LoadJobAbstractor(this.ActiveBlueNileObject.Template, true, true);

            if (!j)
            {
                return false;
            }

            var szofobj = this.le.GetObjectSize("%ITEM");

            var ta = new TextObjectAttributes(
                "TEXT",
                this.ActiveBlueNileObject.EngraveText,
                szofobj.CenterX,
                szofobj.CenterY,
                0,
                8,
                0,
                0,
                true);

            this.ux_Status_InnerOrOuterRing.Visible = true;

            if (this.ActiveBlueNileObject.IsFontInside)
            {
                this.ux_Status_InnerOrOuterRing.Text = "Inner";
                this.ux_Status_InnerOrOuterRing.BackColor = System.Drawing.Color.Orange;
            }
            else
            {
                this.ux_Status_InnerOrOuterRing.Text = "Outer";
                this.ux_Status_InnerOrOuterRing.BackColor = System.Drawing.Color.LightGreen;
            }

            Logger.Info($"Set engrave text data to: {this.ActiveBlueNileObject.EngraveText}");

            this.le.AddTextToProject(ta);

            if (this.ux_cb_lock.Checked)
            {
                this.SetFontHeight("TEXT", (double)this.ux_nud_fontHeight.Value * TykmaGlobals.INISettings.FONTSIZEMULTIPLIER);
                Logger.Info($"Set font height to: {(double)this.ux_nud_fontHeight.Value * TykmaGlobals.INISettings.FONTSIZEMULTIPLIER}");
            }
            else
            {
                this.SetFontHeight("TEXT", this.ActiveBlueNileObject.EngravingAreaHeight * TykmaGlobals.INISettings.FONTSIZEMULTIPLIER);
                Logger.Info($"Set font height to: {this.ActiveBlueNileObject.EngravingAreaHeight * TykmaGlobals.INISettings.FONTSIZEMULTIPLIER}");
            }

            this.le.SetFont("TEXT", this.ActiveBlueNileObject.FontName);
            Logger.Info($"Set font name to: {this.ActiveBlueNileObject.FontName}");
            var textsize = this.le.GetObjectSize("TEXT");
            this.AlignObjects(szofobj, textsize, EAlignType.Center);
            this.SetTextFill();
            this.PositionTraceBox();

            await this.ChangeTextByName("%DIAMETER", this.ActiveBlueNileObject.FontDiameter.ToString());

            this.InitializeRotaryMark(this.GetListOfIDs());
            this.UpdateEditButtonVisibility();


            if (this.ux_cb_lock.Checked)
            {
                this.ResizeObject();
            }
            else
            {
                supressResize = true;
                this.ux_nud_FontWidth.Value = (decimal)textsize.Width;
                this.ux_nud_fontHeight.Value = (decimal)this.ActiveBlueNileObject.EngravingAreaHeight;
                supressResize = false;
            }

            this.UpdatePreview();

            return true;
        }

        private bool supressResize;

        private void Main_KeyDown(object sender, KeyEventArgs e)
        {

            decimal d = TykmaGlobals.INISettings.WIDTHNUDGESIZE;
            if (e.KeyCode == Keys.PageUp)
            {
                this.ux_nud_FontWidth.Value += d;
            }
            else if (e.KeyCode == Keys.PageDown)
            {
                var val = this.ux_nud_FontWidth.Value;

                if (val - d > this.ux_nud_FontWidth.Minimum)
                {
                    this.ux_nud_FontWidth.Value -= d;
                }
            }
        }

        private void ResizeObject()
        {
            if (supressResize)
            {
                return;
            }

            try
            {
                var requestedWidth = (double)this.ux_nud_FontWidth.Value;
                var currentWidth = this.le.GetObjectSize("TEXT");

                var widthaspect = requestedWidth / currentWidth.Width;

                this.le.ScaleSize("TEXT", 0, 0, widthaspect, 1);

                this.UpdatePreview();
            }
            catch (Exception err)
            {

            }
        }

        private void ResizeHeight()
        {

            if (supressResize)
            {
                return;
            }

            try
            {

                this.SetFontHeight("TEXT", (double)this.ux_nud_fontHeight.Value * TykmaGlobals.INISettings.FONTSIZEMULTIPLIER);
                var textsize = this.le.GetObjectSize("TEXT");
                var szofobj = this.le.GetObjectSize("%ITEM");
                this.AlignObjects(szofobj, textsize, EAlignType.Center);

                if (TykmaGlobals.INISettings.HEIGHTKEEPASPECT)
                {
                    this.ux_nud_FontWidth.Value = (decimal)textsize.Width;
                }
                else
                {
                    this.ResizeObject();
                }

                this.PositionTraceBox();
                this.UpdatePreview();
            }
            catch (Exception err)
            {

            }
        }
    }
}
