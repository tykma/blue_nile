﻿//-----------------------------------------------------------------------
// <copyright file="TagTypes.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Tykma.Core.PLC
{
    /// <summary>
    /// PLC tag data types.
    /// </summary>
    public static class TagTypes
    {
        /// <summary>
        /// PLC tag data type enumerator.
        /// </summary>
        public enum ETagType
        {
            /// <summary>
            /// Boolean tag.
            /// </summary>
            Boolean,

            /// <summary>
            /// String tag.
            /// </summary>
            String,

            /// <summary>
            /// Integer tag.
            /// </summary>
            Integer,
        }
    }
}
