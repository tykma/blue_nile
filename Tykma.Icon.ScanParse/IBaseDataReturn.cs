﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBaseDataReturn.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.ScanParse
{
    /// <summary>
    /// A simple return command base class.
    /// </summary>
    public interface IBaseDataReturn
    {
        /// <summary>
        /// Gets or sets the scanned data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        string Data { get; set; }
    }
}
