﻿// <copyright file="TestTykmaDateCode.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Icon.DateCode.UnitTests
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Tykma date code class tests.
    /// </summary>
    [TestClass]
    public class TestTykmaDateCode
    {
        /// <summary>
        /// Tests the year and week.
        /// </summary>
        [TestMethod]
        public void TestYearAndWeek()
        {
            IDateCode dc = new TykmaDateCodes();

            var code = dc.GetDateCode("%yy%week", new DateTime(2016, 01, 04));

            Assert.AreEqual("1601", code);

            Assert.AreEqual("201601", dc.GetDateCode("%yyyy%week", new DateTime(2016, 01, 04)));
        }

        /// <summary>
        /// Tests spaces in date codes.
        /// </summary>
        [TestMethod]
        public void TestSpacesInDateCodes()
        {
            IDateCode dc = new TykmaDateCodes();

            var code = dc.GetDateCode("%yy %week", new DateTime(2016, 01, 04));

            Assert.AreEqual("16 01", code);
        }

        /// <summary>
        /// Tests multiple week codes.
        /// </summary>
        [TestMethod]
        public void TestMultipleWeekCodes()
        {
            IDateCode dc = new TykmaDateCodes();

            var code = dc.GetDateCode("%week%week", new DateTime(2016, 01, 04));

            Assert.AreEqual("0101", code);
        }

        /// <summary>
        /// Tests an invalid code.
        /// </summary>
        [TestMethod]
        public void TestInvalidCode()
        {
            IDateCode dc = new TykmaDateCodes();

            var code = dc.GetDateCode("invalidcode", new DateTime(2016, 01, 04));

            Assert.AreEqual("invalidcode", code);
        }
    }
}
