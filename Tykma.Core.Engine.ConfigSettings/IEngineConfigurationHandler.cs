﻿// <copyright file="IEngineConfigurationHandler.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.Engine.ConfigSettings
{
    /// <summary>
    /// Engine configuration handler interface.
    /// Exposes functionality of subclasses which handle parsing/reading/setting configurations.
    /// </summary>
    public interface IEngineConfigurationHandler
    {
        /// <summary>
        /// Gets or sets a value indicating the marking window size.
        /// </summary>
        int MarkingWindowSize { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the marking window offset.
        /// </summary>
        (double X, double Y) MarkingOffset { get; set; }

        /// <summary>
        /// Gets or sets a  value indicating the field window angle.
        /// </summary>
        double FieldAngle { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the first first galvo is the X galvo.
        /// </summary>
        bool Galvo1AsX { get; set; }

        /// <summary>
        /// Reload the configuration file and put it into memory.
        /// </summary>
        void Refresh();

        /// <summary>
        /// Determines whether or not an axis is on.
        /// </summary>
        /// <param name="whichAxis">The axis status to retrieve.</param>
        /// <returns>Whether or not axis is enabled in the configuration.</returns>
        bool IsAxisOn(int whichAxis);

        /// <summary>
        /// Gets the homing speed of a specified axis.
        /// </summary>
        /// <param name="whichAxis">Axis to retrieve homing speed for.</param>
        /// <returns>The axis homing speed.</returns>
        int GetAxisHomeSpeed(int whichAxis);

        /// <summary>
        /// Get the axis homing timeout interval.
        /// </summary>
        /// <param name="whichAxis">The axis to retrieve.</param>
        /// <returns>The home timeout interval.</returns>
        int GetAxisHomeTimeout(int whichAxis);

        /// <summary>
        /// Get the axis minimium position.
        /// </summary>
        /// <param name="whichAxis">Axis.</param>
        /// <returns>The minimum position.</returns>
        int GetAxisMinPosition(int whichAxis);

        /// <summary>
        /// Get the axis maximu position.
        /// </summary>
        /// <param name="whichAxis">Axis.</param>
        /// <returns>The maximum position.</returns>
        int GetAxisMaxPosition(int whichAxis);

        /// <summary>
        /// Gets a value indicating the pulses per unit of movement.
        /// </summary>
        /// <param name="whichAxis">Axis.</param>
        /// <returns>The pulses per unit.</returns>
        double GetPulsesPerUnit(int whichAxis);

        /// <summary>
        /// Gets a value indicating the pulses per unit for a rotary.
        /// </summary>
        /// <param name="whichAxis">The rotary axis.</param>
        /// <returns>Pulses per unit.</returns>
        double GetPulsesPerUnitRotary(int whichAxis);

        /// <summary>
        /// Toggles an axis as enabled or disabled.
        /// </summary>
        /// <param name="whichAxis">Axis number to toggle.</param>
        /// <param name="enabled">Whether to enable or disable the axis.</param>
        void SetAxisState(int whichAxis, bool enabled);

        /// <summary>
        /// Sets the homing speed of a specified axis.
        /// </summary>
        /// <param name="whichAxis">Axis to set.</param>
        /// <param name="speed">Homing speed to set.</param>
        void SetAxisHomeSpeed(int whichAxis, int speed);

        /// <summary>
        /// Set the axis minimum position.
        /// </summary>
        /// <param name="whichAxis">Axis to set.</param>
        /// <param name="pos">The position.</param>
        void SetAxisMinPosition(int whichAxis, int pos);

        /// <summary>
        /// Set the axis maximum position.
        /// </summary>
        /// <param name="whichAxis">Axis to set.</param>
        /// <param name="pos">The position.</param>
        void SetAxisMaxPosition(int whichAxis, int pos);

        /// <summary>
        /// Sets the axis homing interval timeout.
        /// </summary>
        /// <param name="whichAxis">The axis to set.</param>
        /// <param name="speed">Speed to set.</param>
        void SetAxisHomeTimeout(int whichAxis, int speed);
    }
}
