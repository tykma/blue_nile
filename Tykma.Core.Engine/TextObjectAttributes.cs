﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TextObjectAttributes.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// Hold the attributes of a text object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine
{
    using Entity;

    /// <summary>
    /// A class to hold the attributes of a text object.
    /// </summary>
    public class TextObjectAttributes : EntityInformation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TextObjectAttributes"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="data">The data.</param>
        /// <param name="posx">The position x.</param>
        /// <param name="posy">The position y.</param>
        /// <param name="posz">The position z.</param>
        /// <param name="align">The alignment.</param>
        /// <param name="rangle">The rotation angle.</param>
        /// <param name="penNum">The pen number.</param>
        /// <param name="hatched">if set to <c>true</c> [hatched].</param>
        public TextObjectAttributes(string id, string data, double posx, double posy, double posz, int align, double rangle, int penNum, bool hatched)
        {
            this.Name = id;
            this.Value = data;
            this.PositionX = posx;
            this.PositionY = posy;
            this.Z = posz;
            this.Align = align;
            this.RotationAngle = rangle;
            this.PenNumber = penNum;
            this.Hatched = hatched;
        }

        /// <summary>
        /// Gets or sets the position x.
        /// </summary>
        /// <value>
        /// The position x.
        /// </value>
        public double PositionX { get; set; }

        /// <summary>
        /// Gets or sets the position y.
        /// </summary>
        /// <value>
        /// The position y.
        /// </value>
        public double PositionY { get; set; }

        /// <summary>
        /// Gets or sets the alignment.
        /// </summary>
        /// <value>
        /// The align.
        /// </value>
        public int Align { get; set; }

        /// <summary>
        /// Gets or sets the rotation angle.
        /// </summary>
        /// <value>
        /// The rotation angle.
        /// </value>
        public double RotationAngle { get; set; }

        /// <summary>
        /// Gets or sets the pen number.
        /// </summary>
        /// <value>
        /// The pen number.
        /// </value>
        public int PenNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="TextObjectAttributes"/> is hatched.
        /// </summary>
        /// <value>
        ///   <c>true</c> if hatched; otherwise, <c>false</c>.
        /// </value>
        public bool Hatched { get; set; }
    }
}
