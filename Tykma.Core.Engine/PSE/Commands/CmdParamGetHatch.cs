﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdParamEntGetHatch.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;
    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// Command to set the hatching/fill settings.
    /// </summary>
    internal class CmdParamGetHatch : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdParamSetHatch" /> class.
        /// </summary>
        /// <param name="lmcApi">The LMC API.</param>
        /// <param name="contour">if set to <c>true</c> [contour].</param>
        /// <param name="hatch1">The hatch1.</param>
        /// <param name="hatch2">The hatch2.</param>
        public CmdParamGetHatch(IntPtr lmcApi, string entity, int hatchIndex)
            : base(lmcApi)
        {
            this.Ent = entity;
            this.Index = hatchIndex;
        }

        private string Ent { get; }

        private int Index { get; }

        [DllImport("MarkPSE", EntryPoint = "lmc1_GetHatchEntParam2", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern int GetHatchEntParam2(string HatchName,
            ref bool bEnableContour,
            int nParamIndex,
            ref int bEnableHatch,
            ref bool bContourFirst,
            ref int nPenNo,
            ref int nHatchType,
            ref bool bHatchAllCalc,
            ref bool bHatchEdge,
            ref bool bHatchAverageLine,
            ref double dHatchAngle,
            ref double dHatchLineDist,
            ref double dHatchEdgeDist,
            ref double dHatchStartOffset,
            ref double dHatchEndOffset,
            ref double dHatchLineReduction,//直线缩进
            ref double dHatchLoopDist,//环间距
            ref int nEdgeLoop,//环数
            ref bool nHatchLoopRev,//环形反转
            ref bool bHatchAutoRotate,//是否自动旋转角度
            ref double dHatchRotateAngle,
            ref bool bHatchCrossMode,
            ref int dCycCount);

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        internal HatchEntAttributes Execute()
        {
            bool enableCountour = false;
            int paramIndex = 1;
            int bEnableHatch = 0;
            bool bCountourFirst = false;
            int nPenNo = 0;
            int nHatchType = 0;
            bool bHatchAllCalc = false;
            bool bHatchEdge = false;
            bool bHatchAverageLine = false;
            double dHatchAngle = 0;
            double dHatchLineDist = 0;
            double dHatchEdgeDist = 0;
            double dHatchStartOffset = 0;
            double dHatchEndOffset = 0;
            double dHatchLineReduction = 0;
            double dHatchLoopDist = 0;
            int nEdgeLoop = 0;
            bool nHatchLoopRev = false;
            bool bHatchAutoRotate = false;
            double dHatchRotateAngle = 0;
            bool bHatchCrossMode = false;
            int dCycCount = 0;

            int result = GetHatchEntParam2(
                this.Ent,
                ref enableCountour,
                this.Index,
                ref bEnableHatch,
                ref bCountourFirst,
                ref nPenNo,
                ref nHatchType,
                ref bHatchAllCalc,
                ref bHatchEdge,
                ref bHatchAverageLine,
                ref dHatchAngle,
                ref dHatchLineDist,
                ref dHatchEdgeDist,
                ref dHatchStartOffset,
                ref dHatchEndOffset,
                ref dHatchLineReduction,
                ref dHatchLoopDist,
                ref nEdgeLoop,
                ref nHatchLoopRev,
                ref bHatchAutoRotate,
                ref dHatchRotateAngle,
                ref bHatchCrossMode,
                ref dCycCount);

            var rm = this.GetMiniLaseReturnCode(result);

            if (rm.Success)
            {
                return new HatchEntAttributes(
                    enableCountour,
                    bEnableHatch,
                    bCountourFirst,
                    nPenNo,
                    nHatchType,
                    bHatchAllCalc,
                    bHatchEdge,
                    bHatchAverageLine,
                    dHatchAngle,
                    dHatchLineDist,
                    dHatchEdgeDist,
                    dHatchStartOffset,
                    dHatchEndOffset,
                    dHatchLineReduction,
                    dHatchLoopDist,
                    nEdgeLoop,
                    nHatchLoopRev,
                    bHatchAutoRotate,
                    dHatchRotateAngle,
                    bHatchCrossMode,
                    dCycCount);
            }

            throw new Exception(rm.ReturnDescription);
        }
    }
}
