﻿//-----------------------------------------------------------------------
// <copyright file="SQLRetrievedData.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Tykma.Core.SQL.Client
{
    using System.Collections.Generic;

    using Tykma.Core.Entity;

    /// <summary>
    /// Holds data retrieved from the SQL database.
    /// </summary>
    public class SQLRetrievedData
    {
        /// <summary>
        /// The retrieved tags.
        /// </summary>
        private readonly List<EntityInformation> retrievedTags = new List<EntityInformation>();

        /// <summary>
        /// Backing field for the quantity property.
        /// </summary>
        private int quantity;

        /// <summary>
        /// Initializes a new instance of the <see cref="SQLRetrievedData"/> class.
        /// </summary>
        public SQLRetrievedData()
        {
            this.JobName = string.Empty;
            this.FaultMessage = string.Empty;
            this.quantity = 1;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SQLRetrievedData"/> class.
        /// </summary>
        /// <param name="jobname">The job name.</param>
        public SQLRetrievedData(string jobname)
            : this()
        {
            this.SetSQLJob(jobname);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SQLRetrievedData" /> class.
        /// </summary>
        /// <param name="jobname">The job name.</param>
        /// <param name="quantity">The quantity.</param>
        public SQLRetrievedData(string jobname, string quantity)
            : this(jobname)
        {
            this.SetQuantity(quantity);
        }

        /// <summary>
        /// Gets the name of the job.
        /// </summary>
        /// <value>
        /// The name of the job.
        /// </value>
        public string JobName { get; private set; }

        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        /// <value>
        /// The quantity.
        /// </value>
        public int Quantity
        {
            get
            {
                return this.quantity > 0 ? this.quantity : 1;
            }

            set
            {
                this.quantity = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether there was a [fault].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [fault]; otherwise, <c>false</c>.
        /// </value>
        public bool Fault { get; private set; }

        /// <summary>
        /// Gets the fault message.
        /// </summary>
        /// <value>
        /// The fault message.
        /// </value>
        public string FaultMessage { get; private set; }

        /// <summary>
        /// Gets the retrieved tags.
        /// </summary>
        /// <value>
        /// The retrieved tags.
        /// </value>
        public IEnumerable<EntityInformation> RetrievedTags => this.retrievedTags;

        /// <summary>
        /// Sets the fault message.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        internal void SetFault(string msg)
        {
            this.Fault = true;
            this.FaultMessage = msg;
        }

        /// <summary>
        /// Adds the item to list.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="value">The value.</param>
        internal void AddItemToList(string id, string value)
        {
            this.retrievedTags.Add(new EntityInformation { Name = id, Value = value });
        }

        /// <summary>
        /// Sets the SQL job.
        /// </summary>
        /// <param name="job">The job to set.</param>
        internal void SetSQLJob(string job)
        {
            this.JobName = job.Trim();
        }

        /// <summary>
        /// Sets the quantity.
        /// </summary>
        /// <param name="qty">The qty.</param>
        internal void SetQuantity(string qty)
        {
            if (!string.IsNullOrEmpty(qty))
            {
                int result;

                if (int.TryParse(qty, out result))
                {
                    this.Quantity = result;
                }
            }
        }
    }
}