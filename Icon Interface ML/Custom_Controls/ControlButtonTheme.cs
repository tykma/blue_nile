﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlButtonTheme.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    using Icon_Interface.Properties;

    /// <summary>
    /// The control button theme.
    /// </summary>
    public partial class ControlButtonTheme : ControlButtonBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlButtonTheme"/> class.
        /// </summary>
        public ControlButtonTheme()
        {
            this.InitializeComponent();
            this.ux_Label_Action.Text = Resources.Label_Theme;
        }
    }
}
