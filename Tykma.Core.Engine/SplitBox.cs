﻿// <copyright file="SplitBox.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Core.Engine
{
    /// <summary>
    /// Split box structure.
    /// </summary>
    public struct SplitBox
    {
        /// <summary>
        /// X1
        /// </summary>
        public double x1;

        /// <summary>
        /// Y1
        /// </summary>
        public double y1;

        /// <summary>
        /// X2
        /// </summary>
        public double x2;

        /// <summary>
        ///  Y2
        /// </summary>
        public double y2;

        /// <summary>
        /// Construct the split box.
        /// </summary>
        /// <param name="dx1">X1</param>
        /// <param name="dy1">Y1</param>
        /// <param name="dx2">X2</param>
        /// <param name="dy2">Y2</param>
        public void Build(double dx1, double dy1, double dx2, double dy2)
        {
            this.x1 = dx1;
            this.x2 = dx2;
            this.y1 = dy1;
            this.y2 = dy2;
        }
    }
}
