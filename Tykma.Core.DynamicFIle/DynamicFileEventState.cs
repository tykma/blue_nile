﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DynamicFileEventState.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.DynamicFile
{
    using System;

    /// <summary>
    /// Dynamic file event state.
    /// </summary>
    public class DynamicFileEventState : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicFileEventState" /> class.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="nameOfFile">The name of file.</param>
        /// <param name="error">The error.</param>
        public DynamicFileEventState(DynamicState state, string nameOfFile, string error = "")
        {
            this.Error = error;
            this.State = state;
            this.FileName = nameOfFile;
        }

        /// <summary>
        /// Found file status.
        /// </summary>
        public enum DynamicState
        {
            /// <summary>
            /// A file was found.
            /// </summary>
            FoundFile,

            /// <summary>
            /// The file was moved.
            /// </summary>
            MoveCompleted,

            /// <summary>
            /// There was an error.
            /// </summary>
            Error,
        }

        /// <summary>
        /// Gets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        public string Error { get; private set; }

        /// <summary>
        /// Gets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        public DynamicState State { get; private set; }

        /// <summary>
        /// Gets the name of the file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
        public string FileName { get; private set; }
    }
}
