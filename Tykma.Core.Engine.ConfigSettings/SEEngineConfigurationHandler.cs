﻿// <copyright file="SEEngineConfigurationHandler.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.Engine.ConfigSettings
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.IO.Abstractions;
    using System.Text;

    /// <summary>
    /// Configuration handler for minilase pro SE.
    /// It interacts with classes that handle text file parsing.
    /// </summary>
    public class SEEngineConfigurationHandler : IEngineConfigurationHandler
    {
        /// <summary>
        /// The lines in the configuration file.
        /// </summary>
        private List<string> lines;

        /// <summary>
        /// Initializes a new instance of the <see cref="SEEngineConfigurationHandler"/> class.
        /// </summary>
        /// <param name="path">Configuration file path.</param>
        public SEEngineConfigurationHandler(string path)
            : this(new FileSystem(), path, new ConfigReplacer(path))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SEEngineConfigurationHandler"/> class.
        /// </summary>
        /// <param name="fs">The filesystem.</param>
        /// <param name="path">The configuration file path.</param>
        /// <param name="configReplacer">Configuation replacer.</param>
        public SEEngineConfigurationHandler(IFileSystem fs, string path, IReplaceConfig configReplacer)
        {
            this.FS = fs;
            this.Path = path;
            this.Refresh();
            this.ConfigReplacer = configReplacer;
        }

        /// <inheritdoc />
        public int MarkingWindowSize
        {
            get => new MarkingWindowFromConfig(this.lines).Size.Width;

            set
            {
                string text = string.Join(Environment.NewLine, new MarkingWindowFromConfig(this.lines).SetSize(value));
                this.ReplaceConfig(new MemoryStream(Encoding.UTF8.GetBytes(text)));
                this.Refresh();
            }
        }

        /// <inheritdoc />
        public (double X, double Y) MarkingOffset
        {
            get
            {
                var x = new MarkingWindowFromConfig(this.lines).OffsetX;
                var y = new MarkingWindowFromConfig(this.lines).OffsetY;

                return (x, y);
            }

            set
            {
                string text = string.Join(Environment.NewLine, new MarkingWindowFromConfig(this.lines).SetOffset(value.X, value.Y));
                this.ReplaceConfig(new MemoryStream(Encoding.UTF8.GetBytes(text)));

                this.Refresh();
            }
        }

        /// <inheritdoc />
        public double FieldAngle
        {
            get => new MarkingWindowFromConfig(this.lines).FieldAngle;

            set
            {
                string text = string.Join(Environment.NewLine, new MarkingWindowFromConfig(this.lines).SetFieldAngle(value));
                this.ReplaceConfig(new MemoryStream(Encoding.UTF8.GetBytes(text)));

                this.Refresh();
            }
        }

        /// <inheritdoc />
        public bool Galvo1AsX
        {
            get => new MarkingWindowFromConfig(this.lines).IsGalvo1X;

            set
            {
                string text = string.Join(Environment.NewLine, new MarkingWindowFromConfig(this.lines).SetGalvo1AsX(value));
                this.ReplaceConfig(new MemoryStream(Encoding.UTF8.GetBytes(text)));

                this.Refresh();
            }
        }

        /// <summary>
        /// Gets the file system.
        /// </summary>
        private IFileSystem FS { get; }

        /// <summary>
        /// Gets the configuration replacing logic.
        /// </summary>
        private IReplaceConfig ConfigReplacer { get; }

        /// <summary>
        /// Gets the configuration file path.
        /// </summary>
        private string Path { get; }

        /// <inheritdoc />
        public bool IsAxisOn(int whichAxis)
        {
            return new AxisStatusConfig(this.lines).IsAxisOn(whichAxis);
        }

        /// <inheritdoc />
        public double GetPulsesPerUnit(int whichAxis)
        {
            var axisConfig = new AxisStatusConfig(this.lines);

            return (double)axisConfig.GetPulsesPerRound(whichAxis) / axisConfig.GetDistancePerRound(whichAxis);
        }

        /// <inheritdoc />
        public double GetPulsesPerUnitRotary(int whichAxis)
        {
            var axisConfig = new AxisStatusConfig(this.lines);

            return (double)axisConfig.GetPulsesPerRound(whichAxis) / 360;
        }

        /// <inheritdoc />
        public void SetAxisState(int whichAxis, bool enable)
        {
            var ax = new AxisStatusConfig(this.lines).ToggleAxisEnabled(whichAxis, enable);

            string text = string.Join(Environment.NewLine, ax);

            MemoryStream stringInMemoryStream =
                new MemoryStream(Encoding.UTF8.GetBytes(text));

            this.ReplaceConfig(stringInMemoryStream);

            this.Refresh();
        }

        /// <inheritdoc />
        public int GetAxisHomeSpeed(int whichAxis)
        {
            return new AxisStatusConfig(this.lines).GetAxisHomeSpeed(whichAxis);
        }

        /// <inheritdoc />
        public void SetAxisHomeSpeed(int whichAxis, int speed)
        {
            var ax = new AxisStatusConfig(this.lines).SetAxisHomeSpeed(whichAxis, speed);

            string text = string.Join(Environment.NewLine, ax);

            MemoryStream stringInMemoryStream =
                new MemoryStream(Encoding.UTF8.GetBytes(text));

            this.ReplaceConfig(stringInMemoryStream);

            this.Refresh();
        }

        /// <inheritdoc />
        public void SetAxisMinPosition(int whichAxis, int pos)
        {
            var ax = new AxisStatusConfig(this.lines).SetAxisMinPosition(whichAxis, pos);

            string text = string.Join(Environment.NewLine, ax);

            MemoryStream stringInMemoryStream =
                new MemoryStream(Encoding.UTF8.GetBytes(text));

            this.ReplaceConfig(stringInMemoryStream);

            this.Refresh();
        }

        /// <inheritdoc />
        public void SetAxisMaxPosition(int whichAxis, int pos)
        {
            var ax = new AxisStatusConfig(this.lines).SetAxisMaximumPosition(whichAxis, pos);

            string text = string.Join(Environment.NewLine, ax);

            MemoryStream stringInMemoryStream =
                new MemoryStream(Encoding.UTF8.GetBytes(text));

            this.ReplaceConfig(stringInMemoryStream);

            this.Refresh();
        }

        /// <inheritdoc />
        public void SetAxisHomeTimeout(int whichAxis, int speed)
        {
            var ax = new AxisStatusConfig(this.lines).SetHomeTimeOut(whichAxis, speed);

            string text = string.Join(Environment.NewLine, ax);

            MemoryStream stringInMemoryStream =
                new MemoryStream(Encoding.UTF8.GetBytes(text));

            this.ReplaceConfig(stringInMemoryStream);

            this.Refresh();
        }

        /// <inheritdoc />
        public int GetAxisHomeTimeout(int whichAxis)
        {
            return new AxisStatusConfig(this.lines).GetAxisTimeOut(whichAxis);
        }

        /// <inheritdoc />
        public int GetAxisMinPosition(int whichAxis)
        {
            return new AxisStatusConfig(this.lines).GetAxisMinimumPosition(whichAxis);
        }

        /// <inheritdoc />
        public int GetAxisMaxPosition(int whichAxis)
        {
            return new AxisStatusConfig(this.lines).GetAxisMaximumPosition(whichAxis);
        }

        /// <inheritdoc />
        public void Refresh()
        {
            this.lines = new List<string>();

            if (this.FS.File.Exists(this.Path))
            {
                using (var sr = this.FS.File.OpenRead(this.Path))
                {
                    using (var rdr = new StreamReader(sr))
                    {
                        while (rdr.Peek() >= 0)
                        {
                            this.lines.Add(rdr.ReadLine());
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Replace config with a memory stream.
        /// </summary>
        /// <param name="ms">The new config.</param>
        private void ReplaceConfig(MemoryStream ms)
        {
            this.ConfigReplacer.ReplaceConfig(ms);
        }
    }
}
