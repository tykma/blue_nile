﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlVerboseCheckBox.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    using Icon_Interface.Properties;

    /// <summary>
    /// A verbose check box.
    /// </summary>
    public partial class ControlVerboseCheckBox : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlVerboseCheckBox"/> class.
        /// </summary>
        public ControlVerboseCheckBox()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the setting label.
        /// </summary>
        /// <value>
        /// The setting label.
        /// </value>
        [Category("My Properties")]
        [Description("Setting Label")]
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public string SettingLabel
        {
            get
            {
                return this.ux_Label_Setting.Text;
            }

            set
            {
                this.ux_Label_Setting.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ControlVerboseCheckBox"/> is checked.
        /// </summary>
        /// <value>
        ///   <c>true</c> if checked; otherwise, <c>false</c>.
        /// </value>
        public bool Checked
        {
            get
            {
                return this.ux_CheckBox_Check.Checked;
            }

            set
            {
                this.CheckBoxChanged(this, new EventArgs());
                this.ux_CheckBox_Check.Checked = value;
            }
        }

        /// <summary>
        /// CheckBoxes the changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void CheckBoxChanged(object sender, EventArgs e)
        {
            this.ux_CheckBox_Check.Text = this.ux_CheckBox_Check.Checked ? Resources.Label_Enabled : Resources.Label_Disabled;
        }
    }
}
