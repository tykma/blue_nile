﻿// <copyright file="MarkingWindowFromConfig.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.Engine.ConfigSettings
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Globalization;
    using System.Linq;

    /// <summary>
    /// Retrieve marking window size from a configuration file on disk.
    /// </summary>
    /// <seealso cref="Tykma.Core.Engine.ConfigSettings.IMarkingWindow" />
    public class MarkingWindowFromConfig : IMarkingWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MarkingWindowFromConfig"/> class.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public MarkingWindowFromConfig(IList<string> config)
        {
            this.Config = config;
        }

        /// <summary>
        /// Gets the size.
        /// </summary>
        /// <value>
        /// The size.
        /// </value>
        public Size Size
        {
            get
            {
                foreach (var number in from line in this.Config where line.StartsWith("FIELDSIZE") select line.Replace("FIELDSIZE=", string.Empty))
                {
                    if (decimal.TryParse(number, NumberStyles.Any, CultureInfo.InvariantCulture, out decimal dout))
                    {
                        return new Size(Convert.ToInt32(dout), Convert.ToInt32(dout));
                    }
                }

                return new Size(0, 0);
            }
        }

        /// <inheritdoc/>
        public double OffsetX
        {
            get
            {
                foreach (var number in from line in this.Config where line.StartsWith("FIELDOFFSETX") select line.Replace("FIELDOFFSETX=", string.Empty))
                {
                    if (double.TryParse(number, NumberStyles.Any, CultureInfo.InvariantCulture, out double dout))
                    {
                        return dout;
                    }
                }

                return 0;
            }
        }

        /// <inheritdoc/>
        public double OffsetY
        {
            get
            {
                foreach (var number in from line in this.Config where line.StartsWith("FIELDOFFSETY") select line.Replace("FIELDOFFSETY=", string.Empty))
                {
                    if (double.TryParse(number, NumberStyles.Any, CultureInfo.InvariantCulture, out double dout))
                    {
                        return dout;
                    }
                }

                return 0;
            }
        }

        /// <inheritdoc />
        public double FieldAngle
        {
            get
            {
                foreach (var number in from line in this.Config where line.StartsWith("FIELDANGLE") select line.Replace("FIELDANGLE=", string.Empty))
                {
                    if (double.TryParse(number, NumberStyles.Any, CultureInfo.InvariantCulture, out double dout))
                    {
                        return dout;
                    }
                }

                return 0;
            }
        }

        /// <inheritdoc/>
        public bool IsGalvo1X
        {
            get
            {
                foreach (var number in from line in this.Config where line.StartsWith("GALVOX") select line.Replace("GALVOX=", string.Empty))
                {
                    if (number.Equals("0"))
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        private IList<string> Config { get; }

        /// <inheritdoc />
        public IList<string> SetSize(int fieldSize)
        {
            var newConfig = new List<string>();

            foreach (var line in this.Config)
            {
                if (line.StartsWith("FIELDSIZE"))
                {
                    var sizetoset = fieldSize.ToString("E").ToLower();
                    newConfig.Add($"FIELDSIZE={sizetoset}");
                }
                else
                {
                    newConfig.Add(line);
                }
            }

            return newConfig;
        }

        /// <inheritdoc />
        public IList<string> SetOffset(double x, double y)
        {
            var newConfig = new List<string>();

            var xtoset = x.ToString("E").ToLower();
            var ytoset = y.ToString("E").ToLower();

            foreach (var line in this.Config)
            {
                if (line.StartsWith("FIELDOFFSET"))
                {
                    if (line.StartsWith("FIELDOFFSETX"))
                    {
                        newConfig.Add($"FIELDOFFSETX={xtoset}");
                    }
                    else if (line.StartsWith("FIELDOFFSETY"))
                    {
                        newConfig.Add($"FIELDOFFSETY={ytoset}");
                    }
                }
                else
                {
                    newConfig.Add(line);
                }
            }

            return newConfig;
        }

        /// <inheritdoc />
        public IList<string> SetFieldAngle(double angle)
        {
            var newConfig = new List<string>();

            foreach (var line in this.Config)
            {
                if (line.StartsWith("FIELDANGLE"))
                {
                    var angletoset = angle.ToString("E").ToLower();
                    newConfig.Add($"FIELDANGLE={angletoset}");
                }
                else
                {
                    newConfig.Add(line);
                }
            }

            return newConfig;
        }

        /// <inheritdoc />
        public IList<string> SetGalvo1AsX(bool enable)
        {
            string val = enable ? "0" : "1";

            var newConfig = new List<string>();

            foreach (var line in this.Config)
            {
                newConfig.Add(line.StartsWith("GALVOX") ? $"GALVOX={val}" : line);
            }

            return newConfig;
        }
    }
}
