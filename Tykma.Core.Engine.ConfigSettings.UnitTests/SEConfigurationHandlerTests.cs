﻿// <copyright file="SEConfigurationHandlerTests.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.Engine.ConfigSettings.UnitTests
{
    using System;
    using System.IO;
    using System.IO.Abstractions;
    using System.Linq;
    using System.Text;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using NSubstitute;

    /// <summary>
    /// SE configuration handler unit tests.
    /// </summary>
    [TestClass]
    public class SEConfigurationHandlerTests
    {
        /// <summary>
        /// Test basic file loading/initialization.
        /// </summary>
        [TestMethod]
        public void TestBasicFileLoad()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            // Create the configuration handling engine.
            // ReSharper disable once UnusedVariable
            var seengine = new SEEngineConfigurationHandler(fs, path, null);

            fs.Received().File.Exists(path);
            fs.Received().File.OpenRead(path);
        }

        /// <summary>
        /// Test getting the marking size.
        /// </summary>
        [TestMethod]
        public void TestGetMarkingSize()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, null);

            Assert.AreEqual(260, seengine.MarkingWindowSize);
        }

        /// <summary>
        /// Test getting the field angle.
        /// </summary>
        [TestMethod]
        public void TestGetFieldAngle()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, null);

            Assert.AreEqual(45, seengine.FieldAngle);
        }

        /// <summary>
        /// Test is axis on.
        /// </summary>
        [TestMethod]
        public void TestGetIsAxisOn()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, null);

            Assert.IsTrue(seengine.IsAxisOn(0));
            Assert.IsTrue(seengine.IsAxisOn(1));
        }

        /// <summary>
        /// Test axis homing speed.
        /// </summary>
        [TestMethod]
        public void TestGetAxisHomeSpeed()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, null);

            Assert.AreEqual(1000, seengine.GetAxisHomeSpeed(0));
            Assert.AreEqual(1500, seengine.GetAxisHomeSpeed(1));
        }

        /// <summary>
        /// Test axis timeout.
        /// </summary>
        [TestMethod]
        public void TestGetAxisTimeout()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, null);

            Assert.AreEqual(10, seengine.GetAxisHomeTimeout(0));
            Assert.AreEqual(100, seengine.GetAxisHomeTimeout(1));
        }

        /// <summary>
        /// Test axis minimum position.
        /// </summary>
        [TestMethod]
        public void TestGetAxisMinimumPosition()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, null);

            Assert.AreEqual(-360, seengine.GetAxisMinPosition(0));
            Assert.AreEqual(0, seengine.GetAxisMinPosition(1));
        }

        /// <summary>
        /// Test axis maximum position.
        /// </summary>
        [TestMethod]
        public void TestGetAxisMaximumPosition()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, null);

            Assert.AreEqual(360, seengine.GetAxisMaxPosition(0));
            Assert.AreEqual(1000, seengine.GetAxisMaxPosition(1));
        }

        /// <summary>
        /// Test getting offsets.
        /// </summary>
        [TestMethod]
        public void TestGettingOffsets()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, null);

            var offsets = seengine.MarkingOffset;

            Assert.AreEqual(1, offsets.X);
            Assert.AreEqual(2, offsets.Y);
        }

        /// <summary>
        /// Test setting axis 0 home speed.
        /// </summary>
        [TestMethod]
        public void TestSettingAxis0HomeSpeed()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var replacer = Substitute.For<IReplaceConfig>();

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, replacer);

            // Return the stream again since it will be disposed the first time.
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var ms = new MemoryStream();
            replacer.ReplaceConfig(Arg.Do<MemoryStream>(x => ms = x));

            seengine.SetAxisHomeSpeed(0, 1200);

            replacer.ReceivedWithAnyArgs().ReplaceConfig(Arg.Any<MemoryStream>());

            var result = Encoding.
                ASCII.
                GetString(ms.ToArray()).
                Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            var newconfig = new AxisStatusConfig(result);

            Assert.AreEqual(1200, newconfig.GetAxisHomeSpeed(0));
        }

        /// <summary>
        /// Test setting axis 1 home speed.
        /// </summary>
        [TestMethod]
        public void TestSettingAxis1HomeSpeed()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var replacer = Substitute.For<IReplaceConfig>();

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, replacer);

            // Return the stream again since it will be disposed the first time.
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var ms = new MemoryStream();
            replacer.ReplaceConfig(Arg.Do<MemoryStream>(x => ms = x));

            seengine.SetAxisHomeSpeed(1, 1200);

            replacer.ReceivedWithAnyArgs().ReplaceConfig(Arg.Any<MemoryStream>());

            var result = Encoding.
                ASCII.
                GetString(ms.ToArray()).
                Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            var newconfig = new AxisStatusConfig(result);

            Assert.AreEqual(1200, newconfig.GetAxisHomeSpeed(1));
        }

        /// <summary>
        /// Test setting the marking size.
        /// </summary>
        [TestMethod]
        public void TestSettingMarkingSize()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var replacer = Substitute.For<IReplaceConfig>();

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, replacer);

            // Return the stream again since it will be disposed the first time.
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var ms = new MemoryStream();
            replacer.ReplaceConfig(Arg.Do<MemoryStream>(x => ms = x));

            seengine.MarkingWindowSize = 440;

            replacer.ReceivedWithAnyArgs().ReplaceConfig(Arg.Any<MemoryStream>());

            var result = Encoding.
                ASCII.
                GetString(ms.ToArray()).
                Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            var newconfig = new MarkingWindowFromConfig(result);

            Assert.AreEqual(440, newconfig.Size.Width);
            Assert.AreEqual(440, newconfig.Size.Height);
        }

        /// <summary>
        /// Test setting the axis 0 state.
        /// </summary>
        [TestMethod]
        public void TestSettingAxis0State()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var replacer = Substitute.For<IReplaceConfig>();

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, replacer);

            // Return the stream again since it will be disposed the first time.
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var ms = new MemoryStream();
            replacer.ReplaceConfig(Arg.Do<MemoryStream>(x => ms = x));

            seengine.SetAxisState(0, true);

            replacer.ReceivedWithAnyArgs().ReplaceConfig(Arg.Any<MemoryStream>());

            var result = Encoding.
                ASCII.
                GetString(ms.ToArray()).
                Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            var newconfig = new AxisStatusConfig(result);

            Assert.IsTrue(newconfig.IsAxisOn(0));
        }

        /// <summary>
        /// Test setting the axis 1 state.
        /// </summary>
        [TestMethod]
        public void TestSettingAxis1State()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var replacer = Substitute.For<IReplaceConfig>();

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, replacer);

            // Return the stream again since it will be disposed the first time.
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var ms = new MemoryStream();
            replacer.ReplaceConfig(Arg.Do<MemoryStream>(x => ms = x));

            seengine.SetAxisState(1, true);

            replacer.ReceivedWithAnyArgs().ReplaceConfig(Arg.Any<MemoryStream>());

            var result = Encoding.
                ASCII.
                GetString(ms.ToArray()).
                Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            var newconfig = new AxisStatusConfig(result);

            Assert.IsTrue(newconfig.IsAxisOn(1));
        }

        /// <summary>
        /// Test setting the axis 0 minimum position.
        /// </summary>
        [TestMethod]
        public void TestSettingAxis0MinimumPosition()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var replacer = Substitute.For<IReplaceConfig>();

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, replacer);

            // Return the stream again since it will be disposed the first time.
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var ms = new MemoryStream();
            replacer.ReplaceConfig(Arg.Do<MemoryStream>(x => ms = x));

            seengine.SetAxisMinPosition(0, -444);

            replacer.ReceivedWithAnyArgs().ReplaceConfig(Arg.Any<MemoryStream>());

            var result = Encoding.
                ASCII.
                GetString(ms.ToArray()).
                Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            var newconfig = new AxisStatusConfig(result);

            Assert.AreEqual(-444, newconfig.GetAxisMinimumPosition(0));
        }

        /// <summary>
        /// Test setting the axis 1 minimum position.
        /// </summary>
        [TestMethod]
        public void TestSettingAxis1MinimumPosition()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var replacer = Substitute.For<IReplaceConfig>();

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, replacer);

            // Return the stream again since it will be disposed the first time.
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var ms = new MemoryStream();
            replacer.ReplaceConfig(Arg.Do<MemoryStream>(x => ms = x));

            seengine.SetAxisMinPosition(1, -445);

            replacer.ReceivedWithAnyArgs().ReplaceConfig(Arg.Any<MemoryStream>());

            var result = Encoding.
                ASCII.
                GetString(ms.ToArray()).
                Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            var newconfig = new AxisStatusConfig(result);

            Assert.AreEqual(-445, newconfig.GetAxisMinimumPosition(1));
        }

        /// <summary>
        /// Test setting the axis 0 maximum position.
        /// </summary>
        [TestMethod]
        public void TestSettingAxis0MaximumPosition()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var replacer = Substitute.For<IReplaceConfig>();

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, replacer);

            // Return the stream again since it will be disposed the first time.
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var ms = new MemoryStream();
            replacer.ReplaceConfig(Arg.Do<MemoryStream>(x => ms = x));

            seengine.SetAxisMaxPosition(0, -444);

            replacer.ReceivedWithAnyArgs().ReplaceConfig(Arg.Any<MemoryStream>());

            var result = Encoding.
                ASCII.
                GetString(ms.ToArray()).
                Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            var newconfig = new AxisStatusConfig(result);

            Assert.AreEqual(-444, newconfig.GetAxisMaximumPosition(0));
        }

        /// <summary>
        /// Test setting the axis 1 maximum position.
        /// </summary>
        [TestMethod]
        public void TestSettingAxis1MaximumPosition()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var replacer = Substitute.For<IReplaceConfig>();

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, replacer);

            // Return the stream again since it will be disposed the first time.
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var ms = new MemoryStream();
            replacer.ReplaceConfig(Arg.Do<MemoryStream>(x => ms = x));

            seengine.SetAxisMaxPosition(1, -444);

            replacer.ReceivedWithAnyArgs().ReplaceConfig(Arg.Any<MemoryStream>());

            var result = Encoding.
                ASCII.
                GetString(ms.ToArray()).
                Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            var newconfig = new AxisStatusConfig(result);

            Assert.AreEqual(-444, newconfig.GetAxisMaximumPosition(1));
        }

        /// <summary>
        /// Test setting the marking offsets.
        /// </summary>
        [TestMethod]
        public void TestSettingMarkingOffsets()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var replacer = Substitute.For<IReplaceConfig>();

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, replacer);

            // Return the stream again since it will be disposed the first time.
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var ms = new MemoryStream();
            replacer.ReplaceConfig(Arg.Do<MemoryStream>(x => ms = x));

            seengine.MarkingOffset = (1.44, 4.11);

            replacer.ReceivedWithAnyArgs().ReplaceConfig(Arg.Any<MemoryStream>());

            var result = Encoding.
                ASCII.
                GetString(ms.ToArray()).
                Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            var newconfig = new MarkingWindowFromConfig(result);

            Assert.AreEqual(1.44, newconfig.OffsetX);
            Assert.AreEqual(4.11, newconfig.OffsetY);
        }

        /// <summary>
        /// Test setting the field angle.
        /// </summary>
        [TestMethod]
        public void TestSetFieldAngle()
        {
            // Set the emulation config location path.
            string path = @"C:\config";

            // Create a file system mock.
            var fs = Substitute.For<IFileSystem>();

            // When asked if our config exists, we will return true.
            fs.File.Exists(path).Returns(true);

            // When asked to read a config we will return our sample config.
            var str = ConfigHelper.GetSampleFile().Aggregate(string.Empty, (current, item) => current + (item + Environment.NewLine));
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var replacer = Substitute.For<IReplaceConfig>();

            // Create the configuration handling engine.
            var seengine = new SEEngineConfigurationHandler(fs, path, replacer);

            // Return the stream again since it will be disposed the first time.
            fs.File.OpenRead(path).Returns(new MemoryStream(Encoding.UTF8.GetBytes(str)));

            var ms = new MemoryStream();
            replacer.ReplaceConfig(Arg.Do<MemoryStream>(x => ms = x));

            seengine.FieldAngle = 33;

            replacer.ReceivedWithAnyArgs().ReplaceConfig(Arg.Any<MemoryStream>());

            var result = Encoding.
                ASCII.
                GetString(ms.ToArray()).
                Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            var newconfig = new MarkingWindowFromConfig(result);

            Assert.AreEqual(33, newconfig.FieldAngle);
        }
    }
}
