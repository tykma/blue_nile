﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CueTextBox.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// TextBox override to provide a 'cue' used for giving operator a message.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    using System;
    using System.Windows.Forms;

    using Icon_Interface.Pinvokes;

    /// <summary>
    /// Cue text box override.
    /// </summary>
    internal class CueTextBox : TextBox
    {
        /// <summary>
        /// The ECM cue banner.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        internal const uint EMSETCUEBANNER = ECMFIRST + 1;

        /// <summary>
        /// The ECM first.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private const uint ECMFIRST = 0x1500;

        /// <summary>
        /// The cue.
        /// </summary>
        private string cue;

        /// <summary>
        /// Gets or sets the cue.
        /// </summary>
        /// <value>
        /// The cue.
        /// </value>
        public string Cue
        {
            get
            {
                return this.cue;
            }

            set
            {
                this.cue = value;
                this.UpdateCue();
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.HandleCreated" /> event.
        /// </summary>
        /// <param name="e">The event data.</param>
        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            this.UpdateCue();
        }

        /// <summary>
        /// Updates the cue.
        /// </summary>
        private void UpdateCue()
        {
            if (this.IsHandleCreated && this.cue != null)
            {
                NativeMethods.SendMessageW(this.Handle, EMSETCUEBANNER, (IntPtr)1, this.cue);
            }
        }
    }
}