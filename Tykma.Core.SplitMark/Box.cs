﻿// <copyright file="Box.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.SplitMark
{
    /// <summary>
    /// A split box.
    /// </summary>
    public class Box
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Box"/> class.
        /// </summary>
        /// <param name="x1">Min X.</param>
        /// <param name="x2">Max X. </param>
        /// <param name="y1">Min Y.</param>
        /// <param name="y2">Max Y.</param>
        public Box(double x1, double x2, double y1, double y2)
        {
            this.X1 = x1;
            this.X2 = x2;
            this.Y1 = y1;
            this.Y2 = y2;
        }

        /// <summary>
        /// Gets a value indicating the X1 point.
        /// </summary>
        public double X1 { get; }

        /// <summary>
        /// Gets a value indicating the X2 point.
        /// </summary>
        public double X2 { get; }

        /// <summary>
        /// Gets a value indicating the Y1 point.
        /// </summary>
        public double Y1 { get; }

        /// <summary>
        /// Gets a value indicating the Y2 point.
        /// </summary>
        public double Y2 { get; }
    }
}
