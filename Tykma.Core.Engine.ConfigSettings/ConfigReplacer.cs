﻿// <copyright file="ConfigReplacer.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.Engine.ConfigSettings
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.IO.Abstractions;

    /// <summary>
    /// Minilase Pro SE Config Replacer.
    /// </summary>
    /// <seealso cref="Tykma.Core.Engine.ConfigSettings.IReplaceConfig" />
    public class ConfigReplacer : IReplaceConfig
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigReplacer" /> class.
        /// </summary>
        /// <param name="configPath">The configuration path.</param>
        /// <param name="fs">The fs.</param>
        public ConfigReplacer(string configPath, IFileSystem fs)
        {
            this.ConfigPath = configPath;
            this.FS = fs;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigReplacer"/> class.
        /// </summary>
        /// <param name="configPath">The configuration path.</param>
        [ExcludeFromCodeCoverage]
        public ConfigReplacer(string configPath)
            : this(configPath, new FileSystem())
        {
        }

        /// <summary>
        /// Gets the configuration path.
        /// </summary>
        /// <value>
        /// The configuration path.
        /// </value>
        private string ConfigPath { get; }

        /// <summary>
        /// Gets  the file system abstractor.
        /// </summary>
        /// <value>
        /// The filesystem abstractor.
        /// </value>
        private IFileSystem FS { get; }

        /// <summary>
        /// Replaces the configuration file.
        /// </summary>
        /// <param name="configLines">The configuration file lines.</param>
        /// <returns>
        /// Success state.
        /// </returns>
        public bool ReplaceConfig(MemoryStream configLines)
        {
            if (this.FS.File.Exists(this.ConfigPath))
            {
                this.FS.File.Delete(this.ConfigPath);
            }

            configLines.Position = 0;

            try
            {
                if (!this.FS.Directory.Exists(Path.GetDirectoryName(this.ConfigPath)))
                {
                    this.FS.Directory.CreateDirectory(Path.GetDirectoryName(this.ConfigPath));
                }

                this.FS.File.WriteAllBytes(this.ConfigPath, configLines.ToArray());
            }
            catch (UnauthorizedAccessException)
            {
                return false;
            }
            catch (IOException)
            {
                return false;
            }

            return true;
        }
    }
}
