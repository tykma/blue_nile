﻿using System;

namespace Tykma.Icon.BoxFill
{
    using Tykma.Core.Entity;

    public class BoundaryBoxFiller
    {

        private EntityInformation Box { get; }

        private EntityInformation Ent { get; }
        public BoundaryBoxFiller(EntityInformation box, EntityInformation ent)
        {
            this.Box = box;
            this.Ent = ent;
        }

        public PointD GetScaleSize()
        {
            // Let's get the width and height of the bounding box.
            //var boundBoxHeight = Math.Abs(this.Box.MaxY - this.Box.MinY);
            //var boundBoxWidth = Math.Abs(this.Box.MaxX) + Math.Abs(this.Box.MinX);


            // Let's get the width and height of the text object.
           // var textHeight = Math.Abs(this.Ent.MaxY - this.Ent.MinY);
           // var textWidth = Math.Abs(this.Ent.MaxX - this.Ent.MinX);

            var ratioX = this.Box.Width / this.Ent.Width;
            var ratioY = this.Box.Height / this.Ent.Height;

            var ratio = Math.Min(ratioX, ratioY);

            //var newWidth = textWidth * ratio;
            //var newHeight = textHeight * ratio;
            return new PointD(ratio, ratio);
            //return new PointD(newWidth / textWidth, newHeight / textHeight);
        }

        public PointD GetCenterOfBoundingBox()
        {
            var bbCenterX = (this.Box.MaxX + this.Box.MinX) / 2;
            var bbCenterY = (this.Box.MaxY + this.Box.MinY) / 2;

            return new PointD(bbCenterX, bbCenterY);
        }

        public PointD GetOffsetToTextObjectCenteredInBoundingBox()
        {
            var bbCenterX = (this.Box.MaxX + this.Box.MinX) / 2;
            var bbCenterY = (this.Box.MaxY + this.Box.MinY) / 2;

            var textCenterX = (this.Ent.MaxX + this.Ent.MinX) / 2;
            var textCenterY = (this.Ent.MaxY + this.Ent.MinY) / 2;

            return new PointD(bbCenterX - textCenterX, bbCenterY - textCenterY);
        }
    }
}
