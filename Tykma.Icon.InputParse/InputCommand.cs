﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputCommand.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.InputParse
{
    /// <summary>
    /// Input command class.
    /// </summary>
    public class InputCommand
    {
        /// <summary>
        /// AN input type.
        /// </summary>
        public enum InputType
        {
            /// <summary>
            /// The start.
            /// </summary>
            Start,

            /// <summary>
            /// The stop.
            /// </summary>
            Stop,

            /// <summary>
            /// The shutter state.
            /// </summary>
            ShutterState,
        }
    }
}
