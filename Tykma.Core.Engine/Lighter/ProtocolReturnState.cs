﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProtocolReturnState.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.Lighter
{
    using System.Linq;

    /// <summary>
    /// Connection status string.
    /// </summary>
    public class ProtocolReturnState
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProtocolReturnState"/> class.
        /// </summary>
        /// <param name="response">The response.</param>
        internal ProtocolReturnState(string response)
        {
            if (!string.IsNullOrEmpty(response))
            {
                string fullString = response;
                string first4Removed = string.Join(string.Empty, fullString.Skip(4));
                string lasttworemoved = first4Removed.Remove(first4Removed.Length - 2);
                this.ReceivedString = lasttworemoved;

                // Let's find out if there was an error code or not.
                if (response.Length >= 4)
                {
                    if (response[3] == 6)
                    {
                        this.Ok = true;
                    }
                    else
                    {
                        // We have an error message.
                        this.Ok = false;
                        this.ErrorCode = this.ReceivedString;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the error code string.
        /// </summary>
        /// <value>
        /// The ERRO_CODE string.
        /// </value>
        public string ErrorCode { get; private set; }

        /// <summary>
        /// Gets the received string.
        /// </summary>
        /// <value>
        /// The string.
        /// </value>
        public string ReceivedString { get; }

        /// <summary>
        /// Gets a value indicating whether this return message is OK.
        /// </summary>
        /// <value>
        ///   <c>true</c> if OK; otherwise, <c>false</c>.
        /// </value>
        public bool Ok { get; private set; }
    }
}