﻿// <copyright file="DBLogicTests.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Integration.ExternalDB.Tests
{
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using NSubstitute;

    using Tykma.Integration.SQL;

    /// <summary>
    /// Unit tests for the DB logic class.
    /// </summary>
    [TestClass]
    public class DBLogicTests
    {
        /// <summary>
        /// Test SQL Query sends the correct query.
        /// </summary>
        [TestMethod]
        public void TestSQLQuerySendsCorrectCommand()
        {
            var dbmock = Substitute.For<IDatabaseUtil>();

            var dblogic = new DBLogic(dbmock, "Primary Column", "Template Column", "Data Table", true);

            string query = string.Empty;
            Dictionary<string, object> dictionary = new Dictionary<string, object>();

            dbmock.Select(Arg.Do<string>(x => query = x), Arg.Do<Dictionary<string, object>>(x => dictionary = x));

            dblogic.GetData("123");

            Assert.AreEqual("Select TOP 1 * FROM [Data Table] WHERE [Primary Column] LIKE @SEARCHVALUE", query);

            var value = dictionary.FirstOrDefault();

            Assert.AreEqual(1, dictionary.Count);
            Assert.AreEqual("SEARCHVALUE", value.Key);
            Assert.AreEqual("123", value.Value);
        }

        /// <summary>
        /// Test SQL Query sends the correct query when not using TOP but LIMIT .
        /// </summary>
        [TestMethod]
        public void TestSQLQuerySendsCorrectCommandWhenUsingLimit()
        {
            var dbmock = Substitute.For<IDatabaseUtil>();

            var dblogic = new DBLogic(dbmock, "Primary Column", "Template Column", "Data Table", false);

            string query = string.Empty;
            Dictionary<string, object> dictionary = new Dictionary<string, object>();

            dbmock.Select(Arg.Do<string>(x => query = x), Arg.Do<Dictionary<string, object>>(x => dictionary = x));

            dblogic.GetData("123");

            Assert.AreEqual("Select * FROM [Data Table] WHERE [Primary Column] LIKE @SEARCHVALUE LIMIT 1", query);

            var value = dictionary.FirstOrDefault();

            Assert.AreEqual(1, dictionary.Count);
            Assert.AreEqual("SEARCHVALUE", value.Key);
            Assert.AreEqual("123", value.Value);
        }

        /// <summary>
        /// Test that the datatable returned from SQL is parsed correctly.
        /// </summary>
        [TestMethod]
        public void TestSQLQueryDatatableParsing()
        {
            var dbmock = Substitute.For<IDatabaseUtil>();

            var dblogic = new DBLogic(dbmock, "Primary Column", "Template Column", "Data Table", true);

            DataTable table = new DataTable { Columns = { "Template Column", "Serial Number" }, };
            table.Rows.Add("template1", "serial123");

            dbmock.Select(null, null).ReturnsForAnyArgs(table);

            var data = dblogic.GetData("123");
            Assert.AreEqual("template1", data.Template);
            Assert.AreEqual(1, data.DataFields.Count);

            var df = data.DataFields.FirstOrDefault();
            Assert.AreEqual("Serial Number", df.Key);
            Assert.AreEqual("serial123", df.Value);
        }
    }
}