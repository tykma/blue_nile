﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlQuantity.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    using System;
    using System.Windows.Forms;

    using Icon_Interface.Properties;

    using Tykma.Icon.KeyPad;

    /// <summary>
    /// The control quantity.
    /// </summary>
    public partial class ControlQuantity : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlQuantity"/> class.
        /// </summary>
        public ControlQuantity()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets the maximum value of or counter..
        /// </summary>
        /// <value>
        /// The maximum value.
        /// </value>
        public int Maximum => (int)this.ux_Quantity.Maximum;

        /// <summary>
        /// Gets the minimum value of our counter.
        /// </summary>
        /// <value>
        /// The minimum value.
        /// </value>
        public int Minimum => (int)this.ux_Quantity.Minimum;

        /// <summary>
        /// Gets or sets the counter value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public int Value
        {
            get
            {
                return (int)this.ux_Quantity.Value;
            }

            set
            {
                this.ux_Quantity.Value = value > 0 ? value : this.Minimum;
            }
        }

        /// <summary>
        /// Resets our counter.
        /// </summary>
        public void Reset()
        {
            this.ux_Quantity.Value = this.Minimum;
        }

        /// <summary>
        /// Increment our counter.
        /// </summary>
        public void Increment()
        {
            this.ux_Quantity.UpButton();
        }

        /// <summary>
        /// Decrement our counter.
        /// </summary>
        public void Decrement()
        {
            this.ux_Quantity.DownButton();
        }

        /// <summary>
        /// Handles the ValueChanged event of the Quantity control.
        /// </summary>
        /// <param name="sender">
        /// The source of the event.
        /// </param>
        /// <param name="e">
        /// The <see cref="EventArgs"/> instance containing the event data.
        /// </param>
        private void QuantityValueChanged(object sender, EventArgs e)
        {
            var nud = (NumericUpDown)sender;
            if (nud.Value == 0)
            {
                nud.Value = 1;
            }
        }

        /// <summary>
        /// Buttons the key pad show click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ButtonKeyPadShowClick(object sender, EventArgs e)
        {
            using (var qty = new KeyPad(Settings.Default.BackColor, Settings.Default.ForeColor))
            {
                var val = qty.GetValue();

                if (val < this.Maximum && val > this.Minimum)
                {
                    this.Value = val;
                }
            }
        }
    }
}
