﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComparatorTest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.Comparator
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Icon.AlphanumComparator;

    /// <summary>
    /// Comparator test.
    /// </summary>
    [TestClass]
    public class ComparatorTest
    {
        /// <summary>
        /// Tests the numeric sort; all numeric.
        /// </summary>
        [TestMethod]
        public void TestNumericSortAllNumeric()
        {
            List<string> items = new List<string> { "1", "44", "22", "4", "5" };
            items.Sort(new AlphanumComparator());
            var expectedItems = new List<string> { "1", "4", "5", "22", "44" };
            Assert.IsTrue(items.SequenceEqual(expectedItems));
        }

        /// <summary>
        /// Tests the numeric sort mixes alpha numeric.
        /// </summary>
        [TestMethod]
        public void TestNumericSortMixesAlphaNumeric()
        {
            var items = new List<string> { "a", "c", "d", "b", "1", "2", "22", "3" };
            items.Sort(new AlphanumComparator());
            var expectedItems = new List<string> { "1", "2", "3", "22", "a", "b", "c", "d" };
            Assert.IsTrue(items.SequenceEqual(expectedItems));
        }

        /// <summary>
        /// Tests the numeric sort all alpha.
        /// </summary>
        [TestMethod]
        public void TestNumericSortAllAlpha()
        {
            var items = new List<string> { "a", "c", "d", "b" };
            items.Sort(new AlphanumComparator());
            var expectedItems = new List<string> { "a", "b", "c", "d" };
            Assert.IsTrue(items.SequenceEqual(expectedItems));
        }

        /// <summary>
        /// Tests the numeric sort large integer.
        /// </summary>
        [TestMethod]
        public void TestNumericSortLargeInt()
        {
            var items = new List<string> { "11111111111111111111111111111111111111111", "c", "d", "b" };
            items.Sort(new AlphanumComparator());
            var expectedItems = new List<string> { "11111111111111111111111111111111111111111", "b", "c", "d" };
            Assert.IsTrue(items.SequenceEqual(expectedItems));
        }

        /// <summary>
        /// Tests the numeric sort large integer.
        /// </summary>
        [TestMethod]
        public void TestNumericSortVariousInts()
        {
            var items = new List<string> { "11111111111111111111111111111111111111111", "a10a10", "1234566456456456456", "z3" };
            items.Sort(new AlphanumComparator());
            var expectedItems = new List<string> { "11111111111111111111111111111111111111111", "1234566456456456456", "a10a10", "z3" };
            Assert.IsTrue(items.SequenceEqual(expectedItems));
        }

        /// <summary>
        /// Tests the numeric sort with nulls.
        /// </summary>
        [TestMethod]
        public void TestNumericSortWithNulls()
        {
            var items = new List<string> { "111111111111111111111z11111111111111111111", string.Empty, "1234566456456456456", string.Empty };
            items.Sort(new AlphanumComparator());
            var expectedItems = new List<string> { string.Empty, string.Empty, "111111111111111111111z11111111111111111111", "1234566456456456456" };
            Assert.IsTrue(items.SequenceEqual(expectedItems));
        }

        /// <summary>
        /// Tests the numeric sort with nulls.
        /// </summary>
        [TestMethod]
        public void TestNumericSortEmptyList()
        {
            var items = new List<string>();
            items.Sort(new AlphanumComparator());
            Assert.IsTrue(items.SequenceEqual(new List<string>()));
        }

        /// <summary>
        /// Tests the compare.
        /// </summary>
        [TestMethod]
        public void TestCompare()
        {
            Assert.AreEqual(0, new AlphanumComparator().Compare("a", "a"));
            Assert.AreEqual(1, new AlphanumComparator().Compare("bbbb", "a"));
            Assert.AreEqual(-25, new AlphanumComparator().Compare("aaaa", "zzzz"));
            Assert.AreEqual(-2, new AlphanumComparator().Compare("a", "c"));
        }
    }
}
