﻿// <copyright file="ArcLengthMathTests.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.SplitMark.UnitTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Arc length calculation tests.
    /// </summary>
    [TestClass]
    public class ArcLengthMathTests
    {
        /// <summary>
        /// Test the arc length formula.
        /// </summary>
        [TestMethod]
        public void TestArcLength()
        {
            Assert.AreEqual(57.29, GeometryHelpers.GetCentralAngle(10, 10), .01);
        }
    }
}
