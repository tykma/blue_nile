﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandBase.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.ElectroxEngine.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Timers;

    using Electrox.Evolution.Communications;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// The command base.
    /// </summary>
    public class CommandBase : IDisposable
    {
        /// <summary>
        /// The line feed character.
        /// </summary>
        private readonly byte[] lineFeed = { 13 };

        /// <summary>
        /// The reference to a communications class.
        /// </summary>
        private readonly Communications communications;

        /// <summary>
        /// The time out timer
        /// </summary>
        private readonly Timer timetOutCommantSentTimer;

        /// <summary>
        /// Data is received.
        /// </summary>
        private TaskCompletionSource<DataReceived> dataReceived;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandBase"/> class.
        /// </summary>
        /// <param name="communications">
        /// The communications.
        /// </param>
        internal CommandBase(Communications communications)
        {
            this.communications = communications;
            this.communications.StartTransmission += this.StartTransmissionHandler;
            this.communications.EndTransmission += this.EndTransmissionEventHandler;
            this.communications.DataRecieved += this.DataReceivedEventHandler;
            this.timetOutCommantSentTimer = new Timer();
            this.timetOutCommantSentTimer.Elapsed += this.TimeoutTimerElapsed;
        }

        /// <summary>
        /// Gets or sets the command to send.
        /// </summary>
        /// <value>
        /// The command to send.
        /// </value>
        public string CommandToSend { get; set; }

        /// <summary>
        /// Gets or sets the command finish time out.
        /// </summary>
        /// <value>
        /// The command finish time out.
        /// </value>
        public double CommandFinishTimeOut { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [command responds with data].
        /// </summary>
        /// <value>
        /// <c>true</c> if [command responds with data]; otherwise, <c>false</c>.
        /// </value>
        public bool CommandRespondsWithData { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [command responds with code].
        /// </summary>
        /// <value>
        /// <c>true</c> if [command responds with code]; otherwise, <c>false</c>.
        /// </value>
        public bool CommandRespondsWithCode { get; set; }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.dataReceived?.TrySetResult(new DataReceived());

            this.timetOutCommantSentTimer.Stop();
            this.timetOutCommantSentTimer.Dispose();
        }

        /// <summary>
        /// Execute the command.
        /// </summary>
        /// <returns>Execution state.</returns>
        internal async Task<DataReceived> Execute()
        {
            return await this.SendCommand();
        }

        /// <summary>
        /// The send command.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private async Task<DataReceived> SendCommand()
        {
            if (!this.communications.IsConnected)
            {
                throw new EngineNotConnectedException();
            }

            var fullSend = this.Combine(Encoding.UTF8.GetBytes(this.CommandToSend), this.lineFeed).ToArray();

            this.timetOutCommantSentTimer.Interval = this.CommandFinishTimeOut;

            this.timetOutCommantSentTimer.Start();

            this.dataReceived = new TaskCompletionSource<DataReceived>();
            this.communications.SendCommand(fullSend);
            var result = await this.dataReceived.Task.ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// Timer out timer elapsed event.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.
        /// </param>
        private void TimeoutTimerElapsed(object sender, ElapsedEventArgs e)
        {
            this.timetOutCommantSentTimer.Stop();

            if (this.dataReceived != null)
            {
                var dr = new DataReceived { Timeout = true };
                this.dataReceived.TrySetResult(dr);
            }
        }

        /// <summary>
        /// The start transmission handler.
        /// </summary>
        private void StartTransmissionHandler()
        {
        }

        /// <summary>
        /// The end transmission event handler.
        /// </summary>
        private void EndTransmissionEventHandler()
        {
            if (this.CommandRespondsWithData || this.CommandRespondsWithCode)
            {
                return;
            }

            if (this.dataReceived != null)
            {
                this.timetOutCommantSentTimer.Stop();
                var dr = new DataReceived { Timeout = false };
                this.dataReceived.TrySetResult(dr);
            }
        }

        /// <summary>
        /// Data is received handler.
        /// </summary>
        /// <param name="data">The data.</param>
        private void DataReceivedEventHandler(string data)
        {
            if (this.dataReceived != null)
            {
                this.timetOutCommantSentTimer.Stop();
                var dr = new DataReceived { Timeout = false, ReceivedData = data };

                if (this.CommandRespondsWithCode)
                {
                    dr.ResponseCode = new ReturnStateParse(dr.ReceivedData).CommandParse();
                }

                this.dataReceived.TrySetResult(dr);
            }
        }

        /// <summary>
        /// Combine multiple arrays into a single one.
        /// </summary>
        /// <param name="arrays">
        /// Combined arrays.
        /// </param>
        /// <returns>
        /// Arrays combined.
        /// </returns>
        private IEnumerable<byte> Combine(params byte[][] arrays)
        {
            return arrays.SelectMany(a => a);
        }
    }
}
