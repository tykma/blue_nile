﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScanQueueInfo.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.ScanParse
{
    /// <summary>
    /// A scanned queue info class, it will contain some data (job to load).
    /// Optionally it will contain a quantity.
    /// </summary>
    public class ScanQueueInfo : IBaseDataReturn, IBaseQuantityReturn
    {
        /// <summary>
        /// Gets or sets the SQL command.
        /// </summary>
        /// <value>
        /// The SQL command.
        /// </value>
        public string Data { get; set; }

        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        /// <value>
        /// The quantity.
        /// </value>
        public int Quantity { get; set; }
    }
}
