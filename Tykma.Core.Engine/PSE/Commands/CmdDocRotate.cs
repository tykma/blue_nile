﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdDocRotate.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// Command to rotate a document.
    /// </summary>
    internal class CmdDocRotate : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdDocRotate" /> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking API.</param>
        /// <param name="centerX">The center x.</param>
        /// <param name="centerY">The center y.</param>
        /// <param name="angleOfRotation">The angle of rotation.</param>
        public CmdDocRotate(IntPtr lmcApi, double centerX, double centerY, double angleOfRotation)
            : base(lmcApi)
        {
            this.CenterX = centerX;
            this.CenterY = centerY;
            this.RotationAngle = angleOfRotation;
        }

        /// <summary>
        /// Rotate the marking window.
        /// </summary>
        /// <param name="dCenterX">The center X.</param>
        /// <param name="dCenterY">The center Y.</param>
        /// <param name="dRotateAng">The rotation angle.</param>
        /// <returns>>Return code.</returns>
        private delegate int LMC1ApiSetRotateParam(double dCenterX, double dCenterY, double dRotateAng);

        /// <summary>
        /// Gets the center x.
        /// </summary>
        /// <value>
        /// The center x.
        /// </value>
        private double CenterX { get; }

        /// <summary>
        /// Gets the center y.
        /// </summary>
        /// <value>
        /// The center y.
        /// </value>
        private double CenterY { get; }

        /// <summary>
        /// Gets the rotation angle.
        /// </summary>
        /// <value>
        /// The rotation angle.
        /// </value>
        private double RotationAngle { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        internal bool Execute()
        {
            var lmc1RotateReturn = this.LoadFunction<LMC1ApiSetRotateParam>("lmc1_SetRotateParam");

            if (lmc1RotateReturn == null)
            {
                throw new EngineNotConnectedException();
            }

            var rm = this.GetMiniLaseReturnCode(lmc1RotateReturn(this.CenterX, this.CenterY, this.RotationAngle));

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
