﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdIORead.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// Command to read an input port.
    /// </summary>
    internal class CmdIORead : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdIORead"/> class.
        /// </summary>
        /// <param name="lmcApi">
        /// The laser marking API.
        /// </param>
        public CmdIORead(IntPtr lmcApi)
            : base(lmcApi)
        {
        }

        /// <summary>
        /// Read the input port on the card.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns>Return code.</returns>
        private delegate int LMC1ApiReadPort([MarshalAs(UnmanagedType.U2)] ref ushort data);

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>The read port data.</returns>
        /// <exception cref="Tykma.Core.Engine.CustomExceptions.MethodFaultException">Problem with command.</exception>
        internal ushort Execute()
        {
            var readPort = this.LoadFunction<LMC1ApiReadPort>("lmc1_ReadPort");

            if (readPort == null)
            {
                throw new EngineNotConnectedException();
            }

            ushort data = 0;

            var rm = this.GetMiniLaseReturnCode(readPort(ref data));

            if (rm.Success)
            {
                return data;
            }

            throw new MethodFaultException(rm.ReturnDescription, rm.HardError);
        }
    }
}
