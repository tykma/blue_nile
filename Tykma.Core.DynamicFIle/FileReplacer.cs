﻿//-----------------------------------------------------------------------
// <copyright file="FileReplacer.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Tykma.Core.DynamicFile
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Timers;

    /// <summary>
    /// File replacer library.
    /// It is intended to search a directory for a file to be placed there by an external system.
    /// On finding the file, it will put it into a specified location and create an alert.
    /// The best use is to take PLTs and DXFs and place
    /// them into a location referenced by in a laser template project.
    /// </summary>
    public sealed class FileReplacer
    {
        /// <summary>
        /// The scan timer.
        /// </summary>
        private readonly Timer scanTimer;

        /// <summary>
        /// The available extensions.
        /// </summary>
        private readonly List<string> availableExtensions = new List<string>();

        /// <summary>
        /// The oldest file found.
        /// </summary>
        private FileInfo oldestFile;

        /// <summary>
        /// The file one shot.
        /// </summary>
        private string fileOneShot;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileReplacer" /> class.
        /// </summary>
        /// <param name="sourcePath">The source path.</param>
        /// <param name="destFile">The destination file.</param>
        /// <param name="extensionFilter">The extension filter.</param>
        /// <param name="scanTime">The scan time.</param>
        public FileReplacer(string sourcePath, FileInfo destFile, IEnumerable<string> extensionFilter, int scanTime)
        {
            this.SourceFilePath = sourcePath;
            this.DestinationFile = destFile;

            foreach (var item in extensionFilter)
            {
                this.AddExtensions(item);
            }

            // Initialize the timer.
            this.scanTimer = new Timer { Interval = scanTime };

            this.scanTimer.Elapsed += this.ScanTimerElapsed;

            this.scanTimer.AutoReset = true;
            this.scanTimer.Enabled = true;

            this.LockedFileTimeout = 10000;
        }

        /// <summary>
        /// Occurs when file is ready.
        /// </summary>
        public event EventHandler<DynamicFileEventState> FileSearchEvent;

        /// <summary>
        /// Gets a value indicating whether this instance is enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is enabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsEnabled { get; private set; }

        /// <summary>
        /// Gets the extensions searched.
        /// </summary>
        /// <value>
        /// The extensions searched.
        /// </value>
        public List<string> ExtensionsSearched => this.availableExtensions;

        /// <summary>
        /// Gets the source file path.
        /// </summary>
        /// <value>
        /// The source file path.
        /// </value>
        private string SourceFilePath { get; }

        /// <summary>
        /// Gets the locked file timeout.
        /// </summary>
        /// <value>
        /// The locked file timeout.
        /// </value>
        private int LockedFileTimeout { get; }

        /// <summary>
        /// Gets the destination file.
        /// </summary>
        /// <value>
        /// The destination file.
        /// </value>
        private FileInfo DestinationFile { get; }

        /// <summary>
        /// Enables the file searcher.
        /// </summary>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        public void Enabled(bool enable)
        {
            this.scanTimer.Enabled = enable;

            this.IsEnabled = enable;
            if (enable)
            {
                this.scanTimer.Start();
            }
            else
            {
                this.scanTimer.Stop();
            }
        }

        /// <summary>
        /// Determines whether [is file locked] [the specified file].
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>Whether or not a file is locked.</returns>
        private bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                stream?.Close();
            }

            // File is not locked.
            return false;
        }

        /// <summary>
        /// Adds the extensions.
        /// </summary>
        /// <param name="extension">The extension.</param>
        private void AddExtensions(string extension)
        {
            string extensionFull = $"{"."}{extension.ToLower(CultureInfo.InvariantCulture)}";

            if (this.availableExtensions.Contains(extensionFull, StringComparer.OrdinalIgnoreCase))
            {
            }
            else
            {
                this.availableExtensions.Add(extensionFull);
            }
        }

        /// <summary>
        /// Files the found.
        /// </summary>
        /// <param name="fileInfo">The file information.</param>
        private void FileFound(FileInfo fileInfo)
        {
            if (this.FileSearchEvent != null)
            {
                var msg = new DynamicFileEventState(DynamicFileEventState.DynamicState.FoundFile, fileInfo.Name);
                this.FileSearchEvent(this, msg);
            }

            string errorMsg = string.Empty;
            bool errorOccured = false;

            if (File.Exists(fileInfo.FullName))
            {
                if (this.DestinationFile != null)
                {
                    var directoryInfo = this.DestinationFile.Directory;

                    if (directoryInfo == null)
                    {
                        errorOccured = true;
                        errorMsg = "Destination path invalid";
                    }
                    else if (!directoryInfo.Exists)
                    {
                        try
                        {
                            Directory.CreateDirectory(directoryInfo.FullName);
                        }
                        catch (Exception)
                        {
                            errorOccured = true;
                            errorMsg = "Could not create destination directory";
                        }
                    }
                }
                else
                {
                    if (this.FileSearchEvent != null)
                    {
                        errorMsg = "Destination path invalid";
                        var msg = new DynamicFileEventState(DynamicFileEventState.DynamicState.Error, fileInfo.Name, errorMsg);
                        this.FileSearchEvent(this, msg);
                        return;
                    }
                }

                var sw = new Stopwatch();
                sw.Start();
                if (!errorOccured)
                {
                    try
                    {
                        if (this.DestinationFile != null && this.DestinationFile.Exists)
                        {
                            this.DestinationFile.Delete();
                        }

                        while (this.IsFileLocked(fileInfo))
                        {
                            if (sw.ElapsedMilliseconds > this.LockedFileTimeout)
                            {
                                sw.Stop();
                                break;
                            }
                        }

                        if (File.Exists(this.DestinationFile.FullName))
                        {
                            File.Delete(this.DestinationFile.FullName);
                        }

                        File.Move(fileInfo.FullName, this.DestinationFile.FullName);
                    }
                    catch (Exception)
                    {
                        errorOccured = true;
                        errorMsg = "Could not move file";
                    }

                    this.fileOneShot = string.Empty;
                }
            }
            else
            {
                errorOccured = true;
                errorMsg = "File not found";
            }

            if (this.FileSearchEvent != null)
            {
                DynamicFileEventState.DynamicState state = errorOccured ? DynamicFileEventState.DynamicState.Error : DynamicFileEventState.DynamicState.MoveCompleted;

                var msg = new DynamicFileEventState(state, fileInfo.Name, errorMsg);
                this.FileSearchEvent(this, msg);
            }

            // this.scanTimer.Enabled = true;
        }

        /// <summary>
        /// Unimportant group timer elapsed.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="e">The <see cref="ElapsedEventArgs" /> instance containing the event data.</param>
        private void ScanTimerElapsed(object source, ElapsedEventArgs e)
        {
            // We will look for a new file here.
            // First, check that our file search directory exists.
            if (Directory.Exists(this.SourceFilePath))
            {
                var searchDirectory = new DirectoryInfo(this.SourceFilePath);

                var files = searchDirectory.GetFiles().Where(file => this.availableExtensions.Contains(file.Extension)).ToList();

                this.oldestFile = files.OrderByDescending(f => f.LastWriteTime).FirstOrDefault();

                if (this.fileOneShot == null)
                {
                    this.fileOneShot = string.Empty;
                }

                if ((this.oldestFile != null) && this.oldestFile.FullName != this.fileOneShot)
                {
                    this.fileOneShot = this.oldestFile.FullName;

                    // We have a file.
                    this.FileFound(this.oldestFile);
                }
            }
        }
    }
}
