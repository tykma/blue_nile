﻿// <copyright file="IApplicationSettings.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Icon_Interface.File_Management
{
    using Config.Net;

    /// <summary>
    /// Application settings.
    /// </summary>
    public interface IApplicationSettings
    {
#pragma warning disable SA1124 // Do not use regions

        #region CMD FILE DATA IMPORTING

        /// <summary>
        /// Gets or sets a value indicating whether data importing is enabled.
        /// </summary>
        [Option(Alias = "DATA_IMPORT_ENABLED", DefaultValue = "False")]
        bool DATASEARCHENABLED { get; set; }

        /// <summary>
        /// Gets or sets the data Import Search Folder.
        /// </summary>
        [Option(Alias = "DATA_IMPORT_SEARCH_FOLDER", DefaultValue = @"C:\Temp\")]
        string DATASEARCHFOLDER { get; set; }

        /// <summary>
        /// Gets or sets the data Import Done Folder.
        /// </summary>
        [Option(Alias = "DATA_IMPORT_DONE_FOLDER", DefaultValue = @"C:\Temp\Done\")]
        string DATADONEFOLDER { get; set; }

        /// <summary>
        /// Gets or sets the data Import Error Folder.
        /// </summary>
        [Option(Alias = "DATA_IMPORT_ERROR_FOLDER", DefaultValue = @"C:\Temp\NoMark\")]
        string DATAERRORFOLDER { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether data Import Search Speed.
        /// </summary>
        [Option(Alias = "DATA_IMPORT_SEARCH_SPEED", DefaultValue = "1000")]
        int DATASEARCHSPEED { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the data Import Search Wait.
        /// </summary>
        [Option(Alias = "DATA_IMPORT_SEARCH_WAIT", DefaultValue = "500")]
        int DATAWAITFORREAD { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the data Import is in 'job load' mode.
        /// </summary>
        [Option(Alias = "DATA_IMPORT_JOB_LOAD_MODE", DefaultValue = "False")]
        bool DATAJOBLOADMODE { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the data Import clears on finish.
        /// </summary>
        [Option(Alias = "DATA_IMPORT_CLEAR_ON_FINISH", DefaultValue = "False")]
        bool CLEARAFTERFINISH { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether data Import clears data on finish.
        /// </summary>
        [Option(Alias = "DATA_IMPORT_CLEAR_DATA_FINISH", DefaultValue = "False")]
        bool CLEARDATAAFTERFINISH { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether data import starts sequence on file load.
        /// </summary>
        [Option(Alias = "DATA_IMPORT_START_ON_LOAD", DefaultValue = "False")]
        bool DATAIMPORTSTARTONLOAD { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we will be searching for a file even if one is loaded - disable queuing.
        /// </summary>
        [Option(Alias = "DATA_IMPORT_SEARCH_AFTER_COMPLETE", DefaultValue = "False")]
        bool DATAIMPORTSEARCHAFTERCOMPLETE { get; set; }

        #endregion

        #region IO

        /// <summary>
        /// Gets or sets a value indicating the external Start Input.
        /// </summary>
        [Option(Alias = "IO_EXT_START", DefaultValue = "4")]
        int IOEXTSTART { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the software ready output.
        /// </summary>
        [Option(Alias = "IO_SOFT_READY", DefaultValue = "1")]
        int IOSOFTWAREREADY { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the external Stop Input.
        /// </summary>
        [Option(Alias = "IO_EXT_STOP", DefaultValue = "5")]
        int IOEXTSTOP { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the external shutter state input.
        /// </summary>
        [Option(Alias = "IO_SHUTTER_STATE", DefaultValue = "6")]
        int IOEXTSHUTTERSTATE { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether I/O is inverted.
        /// </summary>
        [Option(Alias = "IO_INVERT", DefaultValue = "True")]
        bool IOINVERT { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the I/O Scanrate.
        /// </summary>
        [Option(Alias = "IO_SCANRATE", DefaultValue = "200")]
        int IOSCANRATE { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to manually set cycle complete.
        /// </summary>
        [Option(Alias = "IO_SET_COMPLETE", DefaultValue = "True")]
        bool IOSETCOMPLETE { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the cycle complete length.
        /// </summary>
        [Option(Alias = "IO_COMPLETE_LENGTH", DefaultValue = "1000")]
        int IOCOMPLETLENGTH { get; set; }

        #endregion

        #region INSTRUCTIONS

        /// <summary>
        /// Gets or sets a value indicating whether or not the instructions are enabled.
        /// </summary>
        [Option(Alias = "INSTRUCTIONS_ENABLE", DefaultValue = "False")]
        bool INSTRUCTIONSENABLE { get; set; }

        #endregion

        #region TWEAKS

        /// <summary>
        /// Gets or sets a value indicating whether or not we will use a tree job list.
        /// </summary>
        [Option(Alias = "TREE_JOB_LIST", DefaultValue = "False")]
        bool TREEJOBLIST { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not we will use a counter.
        /// </summary>
        [Option(Alias = "USE_COUNTER", DefaultValue = "False")]
        bool USECOUNTER { get; set; }

        [Option(Alias = "COUNTER_NO_DECREMENT", DefaultValue = "False")]
        bool COUNTERNODECREMENT { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not we will always turn limits on.
        /// </summary>
        [Option(Alias = "ALWAYS_ON_LIMITS", DefaultValue = "False")]
        bool ALWAYSONLIMITS { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the start button is hidden.
        /// </summary>
        [Option(Alias = "HIDE_START_BUTTON", DefaultValue = "False")]
        bool HIDESTARTBUTTON { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the edit button is hidden.
        /// </summary>
        [Option(Alias = "HIDE_EDIT_BUTTON", DefaultValue = "False")]
        bool HIDEEDITBUTTON { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the limits button is hidden.
        /// </summary>
        [Option(Alias = "HIDE_LIMITS_BUTTON", DefaultValue = "False")]
        bool HIDELIMITSBUTTON { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the show serial input control is enabled.
        /// </summary>
        [Option(Alias = "SHOW_SN_INPUT_CONTROL", DefaultValue = "False")]
        bool SHOWSNINPUT { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not we will use a queue form.
        /// </summary>
        [Option(Alias = "USE_QUEUE", DefaultValue = "False")]
        bool USEQUEUE { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not an operator is allowed to load jobs.
        /// </summary>
        [Option(Alias = "OPERATOR_CAN_LOAD_JOB", DefaultValue = "True")]
        bool OPERATORCANLOADJOB { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the system is in mark on the fly mode.
        /// </summary>
        [Option(Alias = "MOTF_ENABLED", DefaultValue = "False")]
        bool MOTFENABLED { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to clear variable data on mark finish.
        /// </summary>
        [Option(Alias = "DATA_CLEAR_FINISH", DefaultValue = "False")]
        bool DATACLEARONFINISH { get; set; }

        [Option(Alias = "CLEAR_SEQUENCE_END", DefaultValue = "True")]
        bool CLEARSEQUENCEEND { get; set; }

        /// <summary>
        /// Gets or sets a value of the date code id.
        /// </summary>
        [Option(Alias = "DATE_CODE_ID", DefaultValue = "_DATECODE")]
        string DATECODEID { get; set; }

        /// <summary>
        /// Gets or sets a value of the date code format.
        /// </summary>
        [Option(Alias = "DATE_CODE_FORMAT", DefaultValue = "%yy%week")]
        string DATECODEFORMAT { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show or hide the rotary mark window.
        /// </summary>
        [Option(Alias = "SHOW_ROTARY_SETTINGS", DefaultValue = "True")]
        bool SHOWROTARYSETTINGS { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether system is in tag feeder mode.
        /// </summary>
        [Option(Alias = "TAG_FEEDER", DefaultValue = "False")]
        bool TAGFEEDER { get; set; }

        /// <summary>
        /// Gets or sets the preview angle value.
        /// </summary>
        [Option(Alias = "PREVIEW_ANGLE", DefaultValue = "0")]
        int PREVIEWANGLE { get; set; }

        #endregion

        #region JOB INFO

        /// <summary>
        /// Gets or sets the ddefault job to load.
        /// </summary>
        [Option(Alias = "JOB_TO_LOAD", DefaultValue = @"default")]
        string JOBTOLOAD { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not 'load last job' is set.
        /// </summary>
        [Option(Alias = "ENABLE_LAST_JOB_LOAD", DefaultValue = "False")]
        bool ENABLELASTJOBLOAD { get; set; }

        #endregion

        #region FOLDERS

        /// <summary>
        /// Gets or sets the settings folder.
        /// </summary>
        [Option(Alias = "SETTINGS_FOLDER", DefaultValue = @"C:\tykma\custom\Tykma_Icon\")]
        string SETTINGSFOLDER { get; set; }

        /// <summary>
        /// Gets or sets tthe minilase pro se install folder.
        /// </summary>
        [Option(Alias = "SE_INSTALL_DIR", DefaultValue = @"C:\Program Files (x86)\Minilase Pro SE\")]
        string SEINSTALLDIR { get; set; }

        /// <summary>
        /// Gets or sets the projects folder.
        /// </summary>
        [Option(Alias = "PROJECTS_FOLDER", DefaultValue = @"C:\tykma\projects\")]
        string PROJECTSFOLDER { get; set; }

        /// <summary>
        /// Gets or sets the instructions image folder.
        /// </summary>
        [Option(Alias = "INSTRUCTIONS_IMAGE_FOLDER", DefaultValue = @"C:\tykma\img\")]
        string INSTRUCTIONSIMAGEFOLDER { get; set; }

        /// <summary>
        /// Gets or sets the instructions RTF folder.
        /// </summary>
        [Option(Alias = "INSTRUCTIONS_RTF_FOLDER", DefaultValue = @"C:\tykma\rtf\")]
        string INSTRUCTIONSRTFFOLDER { get; set; }

        #endregion

        #region UX

        /// <summary>
        /// Gets or sets the initial dimensions for the instructions form.
        /// </summary>
        [Option(Alias = "INITIAL_DIMENSIONS_INSTRUCTIONS", DefaultValue = @"0,0,400,800")]
        string INITIALDIMENSIONSINSTRUCTIONS { get; set; }

        /// <summary>
        /// Gets or sets the splitter location on our image/rtf divider.
        /// </summary>
        [Option(Alias = "HORIZONTAL_SPLITTER_INSTRUCTIONS", DefaultValue = @"288")]
        int HORIZONTALSPLITTERINSTRUCTIONS { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to hide the job list.
        /// </summary>
        [Option(Alias = "HIDE_JOB_LIST", DefaultValue = @"False")]
        bool HIDEJOBLIST { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to save a project after it has been ran.
        /// </summary>
        [Option(Alias = "SAVE_PROJECT_AFTER_RUN", DefaultValue = @"False")]
        bool SAVEPROJECTAFTERRUN { get; set; }

        #endregion

        #region NET INTERFACE

        /// <summary>
        /// Gets or sets a value indicating whether or not the net interface is enabled.
        /// </summary>
        [Option(Alias = "NET_ENABLE", DefaultValue = "False")]
        bool NETENABLE { get; set; }

        /// <summary>
        /// Gets or sets the net interface port.
        /// </summary>
        [Option(Alias = "NET_TCP_PORT", DefaultValue = "5287")]
        int NETTCPPORT { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the net interface must enable a job.
        /// </summary>
        [Option(Alias = "NET_ENABLES_JOB", DefaultValue = "False")]
        bool NETENABLESJOB { get; set; }

        #endregion

        #region AXIS

        /// <summary>
        /// Gets or sets a value indicating whether or not the enabled axis will be homed on start up.
        /// </summary>
        [Option(Alias = "HOME_ON_OPEN", DefaultValue = "True")]
        bool HOMEONOPEN { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not there is a Z axis present.
        /// </summary>
        [Option(Alias = "Z_AXIS_PRESENT", DefaultValue = "False")]
        bool ZAXISPRESENT { get; set; }

        /// <summary>
        /// Gets or sets the Z-axis homing frequency.
        /// </summary>
        [Option(Alias = "Z_HOME_FREQUENCY", DefaultValue = "5")]
        int ZHOMEFREQUENCY { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not axis will be homed on every new job.
        /// </summary>
        [Option(Alias = "Z_AXIS_HOME_ON_NEW_JOB", DefaultValue = "False")]
        bool ZAXISHOMEONNEWJOB { get; set; }

        /// <summary>
        /// Gets or sets the z-axis number (0 or 1).
        /// </summary>
        [Option(Alias = "Z_AXIS_NUMBER", DefaultValue = "1")]
        int ZAXISNUMBER { get; set; }

        /// <summary>
        /// Gets or sets the z-axis min value.
        /// </summary>
        [Option(Alias = "Z_AXIS_MIN", DefaultValue = "0")]
        double ZAXISMIN { get; set; }

        /// <summary>
        /// Gets or sets the z-axis max value.
        /// </summary>
        [Option(Alias = "Z_AXIS_MAX", DefaultValue = "100")]
        double ZAXISMAX { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not there is an R axis present.
        /// </summary>
        [Option(Alias = "R_AXIS_PRESENT", DefaultValue = "True")]
        bool RAXISPRESENT { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the r-axis number (0 or 1).
        /// </summary>
        [Option(Alias = "R_AXIS_NUMBER", DefaultValue = "0")]
        int RAXISNUMBER { get; set; }

        /// <summary>
        /// Gets or sets the rr-axis homing frequency.
        /// </summary>
        [Option(Alias = "R_HOME_FREQUENCY", DefaultValue = "1")]
        int RHOMEFREQUENCY { get; set; }

        /// <summary>
        /// Gets or sets the r-axis min value.
        /// </summary>
        [Option(Alias = "R_AXIS_MIN", DefaultValue = "0")]
        double RAXISMIN { get; set; }

        /// <summary>
        /// Gets or sets the R-axis max.
        /// </summary>
        [Option(Alias = "R_AXIS_MAX", DefaultValue = "360")]
        double RAXISMAX { get; set; }

        #endregion.

        #region NUDGER

        /// <summary>
        /// Gets or sets a value indicating whether the nudge form is enabled.
        /// </summary>
        [Option(Alias = "USE_NUDGE_FORM", DefaultValue = "False")]
        bool USENUDGEFORM { get; set; }

        /// <summary>
        /// Gets or sets the nudge Distance.
        /// </summary>
        [Option(Alias = "NUDGE_DISTANCE", DefaultValue = "00.10")]
        double NUDGEDISTANCE { get; set; }

        #endregion

        #region SIMPLE MARK MODE

        /// <summary>
        /// Gets or sets a value indicating whether simple mark mode is enabled.
        /// </summary>
        [Option(Alias = "SIMPLE_MARK_MODE", DefaultValue = "False")]
        bool SIMPLEMARKMODE { get; set; }

        /// <summary>
        /// Gets or sets the simple mark mode first id suffix/prefix.
        /// </summary>
        [Option(Alias = "SIMPLE_MARK_FIRSTIDPREFSUFF", DefaultValue = "*")]
        string SIMPLEMARKFIRSTIDPREFSUFF { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether suffix/prefix will be stripped in simple mark mode.
        /// </summary>
        [Option(Alias = "SIMPLE_MARK_STRIP_SUFFIX", DefaultValue = "False")]
        bool SIMPLEMARKSTRIPSUFFIX { get; set; }

        /// <summary>
        /// Gets or sets the first id name in simple mark.
        /// </summary>
        [Option(Alias = "SIMPLE_MARK_FIRSTID", DefaultValue = "SERIAL")]
        string SIMPLEMARKFIRSTID { get; set; }

        /// <summary>
        /// Gets or sets the simple mark mode - second id.
        /// </summary>
        [Option(Alias = "SIMPLE_MARK_SECONDID", DefaultValue = "NUMBER")]
        string SIMPLEMARKSECONDID { get; set; }

        #endregion

        #region ENGINE

        /// <summary>
        /// Gets or sets the engine type.
        /// </summary>
        [Option(Alias = "ENGINE_TYPE", DefaultValue = "0")]
        int ENGINETYPE { get; set; }

        #endregion

        #region DYNAMIC IMPORT

        /// <summary>
        /// Gets or sets a value indicating whether or not dynamic file import is enabled.
        /// </summary>
        [Option(Alias = "USE_DYNAMIC_FILE", DefaultValue = "False")]
        bool USEDYNAMICIMPORT { get; set; }

        /// <summary>
        /// Gets or sets the poll rate for the dynamic file importer.
        /// </summary>
        [Option(Alias = "DYNAMIC_POLL_RATE", DefaultValue = "500")]
        int DYNAMICIMPORTPOLLRATE { get; set; }

        /// <summary>
        /// Gets or sets the dynamic file import source folder.
        /// </summary>
        [Option(Alias = "DYNAMIC_SOURCE_FOLDER", DefaultValue = @"C:\plttemp\")]
        string DYNAMICSRCFOLDER { get; set; }

        /// <summary>
        /// Gets or sets the the dynamic file destination.
        /// </summary>
        [Option(Alias = "DYNAMIC_DESTINATION_FILE", DefaultValue = @"C:\Temp\plttemp\moveto\file.graphic")]
        string DYNAMICDSTFILE { get; set; }

        /// <summary>
        /// Gets or sets the file extensions accepted for the dynamic file - comma separated.
        /// </summary>
        [Option(Alias = "DYNAMIC_FILE_EXTENSIONS", DefaultValue = @"plt,dxf,hpgl")]
        string DYNAMICEXTENSIONS { get; set; }

        #endregion

        [Option(Alias = "FONT_SIZE_MULTIPLIER", DefaultValue = @"1")]
        double FONTSIZEMULTIPLIER { get; set; }

        [Option(Alias = "WIDTH_NUDGE_SIZE", DefaultValue = @".1")]
        decimal WIDTHNUDGESIZE { get; set; }

        [Option(Alias = "HEIGHT_KEEP_ASPECT", DefaultValue = false)]
        bool HEIGHTKEEPASPECT { get; set; }

#pragma warning restore SA1124 // Do not use regions

        // ReSharper restore InconsistentNaming
    }
}