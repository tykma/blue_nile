﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ErrorParser.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.ElectroxEngine
{
    using Tykma.Core.StringExtensions;

    /// <summary>
    /// The error parser.
    /// </summary>
    public class ErrorParser
    {
        /// <summary>
        /// Parses the message.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <returns>Type of error received.</returns>
        public ErrorMessage ParseMessage(string errorMessage)
        {
            // A string will come in as "ERR0001000000000000<CR>"
            if (errorMessage.StartsWith("ERR") && errorMessage.EndsWith("\r"))
            {
                var parsedMessage = errorMessage.Substring(from: "ERR", until: "\r");

                if (parsedMessage == "8000000000000000")
                {
                    return new ErrorMessage(ErrorMessage.ErrorType.InterlockFault, 3);
                }

                var rtrncode = new ErrorMessage(ErrorMessage.ErrorType.Other, 4);

                return rtrncode;
            }
            else
            {
                return new ErrorMessage(ErrorMessage.ErrorType.NotValidFault, 64);
            }
        }
    }
}
