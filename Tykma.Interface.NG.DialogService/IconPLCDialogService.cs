﻿// <copyright file="IconPLCDialogService.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Interface.NG.DialogService
{
    using System.Drawing;
    using System.Windows.Forms;

    /// <summary>
    /// A simple message dialog form.
    /// </summary>
    public class IconPLCDialogService : IDialogService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IconPLCDialogService"/> class.
        /// </summary>
        /// <param name="positionString">The size/width position encoded in a string.</param>
        public IconPLCDialogService(string positionString)
        {
            this.Form = new PLCMessage(positionString);
            this.Form.Visible = true;
            this.ShowMessage(string.Empty, "PLC Status", "?", Color.Black.ToString());
        }

        /// <summary>
        /// Gets a value indicaitng the current position and location of the form.
        /// </summary>
        public string CurrentPositionAndLocation
        {
            get
            {
                Point location = this.Form.Location;
                Size size = this.Form.Size;
                if (this.Form.WindowState != FormWindowState.Normal)
                {
                    location = this.Form.RestoreBounds.Location;
                    size = this.Form.RestoreBounds.Size;
                }

                string initLocation = string.Join(",", location.X, location.Y, size.Width, size.Height, this.CurrentMonitor);

                return initLocation;
            }
        }

        /// <summary>
        /// Gets a value indicating the current monitor that the form is visible on.
        /// </summary>
        private string CurrentMonitor
        {
            get
            {
                Screen screen = Screen.FromControl(this.Form);
                return screen.DeviceName;
            }
        }

        /// <summary>
        /// Gets or sets the form.
        /// </summary>
        /// <value>
        /// The form.
        /// </value>
        private PLCMessage Form { get; set; }

        /// <inheritdoc />
        public void ShowMessage(string message, string title)
        {
            this.ShowMessage(message, title, string.Empty, Color.White.ToString());
        }

        /// <summary>
        /// Show a message.
        /// </summary>
        /// <param name="message">Message string.</param>
        /// <param name="title">The title.</param>
        /// <param name="code">A numeric code to show.</param>
        /// <param name="color">Text color.</param>
        public void ShowMessage(string message, string title, string code, string color)
        {
            this.Form.Text = title;

            this.Form.Message = message;

            this.Form.ColorText = color;

            this.Form.StatusCode = code;

            if (this.Form.Visible)
            {
                if (this.Form.WindowState == FormWindowState.Minimized)
                {
                    this.Form.WindowState = FormWindowState.Normal;
                }

                this.Form.BringToFront();
            }
            else
            {
                this.Form.Show();
            }
        }

        /// <inheritdoc />
        public void HideMessage()
        {
            this.Form.Hide();
        }

        /// <inheritdoc />
        public void ShowError(string message, string title)
        {
            this.ShowMessage(message, title, string.Empty, Color.Red.ToString());
        }
    }
}
