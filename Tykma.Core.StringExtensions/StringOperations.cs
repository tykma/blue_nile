﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringOperations.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   String operations helper class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.StringExtensions
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text.RegularExpressions;

    /// <summary>
    /// String operations helper class.
    /// </summary>
    public static class StringOperations
    {
        /// <summary>
        /// Cleans the specified string.
        /// </summary>
        /// <param name="s">The string.</param>
        /// <returns>
        /// Cleaned string.
        /// </returns>
        public static string Clean(this string s)
        {
            return Regex.Replace(s, "(\n|\r)+", " ");
        }

        /// <summary>
        /// A case insensitive replace function.
        /// </summary>
        /// <param name="originalString">The string to examine.(HayStack).</param>
        /// <param name="oldValue">The value to replace.(Needle).</param>
        /// <param name="newValue">The new value to be inserted.</param>
        /// <returns>
        /// A string.
        /// </returns>
        public static string CaseInsensitiveReplace(this string originalString, string oldValue, string newValue)
        {
            var regEx = new Regex(oldValue, RegexOptions.IgnoreCase | RegexOptions.Multiline);
            return regEx.Replace(originalString, newValue);
        }

        /// <summary>
        /// takes a substring between two anchor strings (or the end of the string if that anchor is null).
        /// </summary>
        /// <param name="this">a string.</param>
        /// <param name="from">an optional string to search after.</param>
        /// <param name="until">an optional string to search before.</param>
        /// <param name="comparison">an optional comparison for the search.</param>
        /// <returns>
        /// a substring based on the search.
        /// </returns>
        /// <exception cref="System.ArgumentException">from: Failed to find an instance of the first anchor
        /// or
        /// until: Failed to find an instance of the last anchor.</exception>
        public static string Substring(this string @this, string from = null, string until = null, StringComparison comparison = StringComparison.InvariantCulture)
        {
            var fromLength = (from ?? string.Empty).Length;
            var startIndex = !string.IsNullOrEmpty(from)
                ? @this.IndexOf(from, comparison) + fromLength
                : 0;

            if (startIndex < fromLength)
            {
                throw new ArgumentException("from: Failed to find an instance of the first anchor");
            }

            var endIndex = !string.IsNullOrEmpty(until)
            ? @this.IndexOf(until, startIndex, comparison)
            : @this.Length;

            if (endIndex < 0)
            {
                throw new ArgumentException("until: Failed to find an instance of the last anchor");
            }

            var subString = @this.Substring(startIndex, endIndex - startIndex);
            return subString;
        }

        /// <summary>
        /// Removes the prefix.
        /// </summary>
        /// <param name="o">The o.</param>
        /// <param name="prefix">The prefix.</param>
        /// <returns>
        /// Stripped string.
        /// </returns>
        public static string RemovePrefix(this string o, string prefix)
        {
            if (prefix == null)
            {
                return o;
            }

            return !o.StartsWith(prefix) ? o : o.Remove(0, prefix.Length);
        }

        /// <summary>
        /// Removes the suffix.
        /// </summary>
        /// <param name="o">The o.</param>
        /// <param name="suffix">The suffix.</param>
        /// <returns>
        /// Stripped string.
        /// </returns>
        public static string RemoveSuffix(this string o, string suffix)
        {
            if (suffix == null)
            {
                return o;
            }

            return !o.EndsWith(suffix) ? o : o.Remove(o.Length - suffix.Length, suffix.Length);
        }

        /// <summary>
        /// Tries to get a boolean value.
        /// </summary>
        /// <param name="item">The string.</param>
        /// <returns>
        /// Whether or not item is a boolean.
        /// </returns>
        public static bool TryGetBool(this string item)
        {
            bool b;
            bool.TryParse(item, out b);
            return b;
        }

        /// <summary>
        /// Case insensitive string compare.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="compareString">The compare string.</param>
        /// <returns>
        /// Whether or not the strings are the same.
        /// </returns>
        public static bool CaseInsensitiveCompare(this string item, string compareString)
        {
            return string.Compare(item, compareString, StringComparison.OrdinalIgnoreCase) == 0;
        }

        /// <summary>
        /// Determines whether the specified string to test is numeric.
        /// </summary>
        /// <param name="stringToTest">The string to test.</param>
        /// <returns>
        /// Whether or not the numeric is numeric.
        /// </returns>
        public static bool IsNumeric(this string stringToTest)
        {
            long result;
            return long.TryParse(stringToTest, System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out result);
        }

        /// <summary>
        /// Tests a path for invalid characters.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>
        /// If path has invalid characters.
        /// </returns>
        public static bool FilePathHasInvalidChars(this string path)
        {
            return !string.IsNullOrEmpty(path) && path.IndexOfAny(Path.GetInvalidPathChars()) >= 0;
        }

        /// <summary>
        /// Adds an extension to a file.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="extension">The extension.</param>
        /// <returns>
        /// Path with extension.
        /// </returns>
        public static string AddExtension(this string path, string extension)
        {
            return $"{path}.{extension}";
        }
    }
}