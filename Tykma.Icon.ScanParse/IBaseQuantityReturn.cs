﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBaseQuantityReturn.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.ScanParse
{
    /// <summary>
    /// Scanned code quantity interface.
    /// </summary>
    public interface IBaseQuantityReturn
    {
        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        /// <value>
        /// The quantity.
        /// </value>
        int Quantity { get; set; }
    }
}
