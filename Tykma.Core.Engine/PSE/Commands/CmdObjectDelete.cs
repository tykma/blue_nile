﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdObjectDelete.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// The command to delete an object.
    /// </summary>
    internal class CmdObjectDelete : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdObjectDelete"/> class.
        /// </summary>
        /// <param name="lmcApi">The LMC API.</param>
        /// <param name="objectToDelete">The object to delete.</param>
        public CmdObjectDelete(IntPtr lmcApi, string objectToDelete)
            : base(lmcApi)
        {
            this.ObjectToDelete = objectToDelete;
        }

        /// <summary>
        /// Delete a given entity.
        /// </summary>
        /// <param name="pEntName">Name of the entity.</param>
        /// <returns>Return code.</returns>
        private delegate int LMC1ApiDeleteEnt([MarshalAs(UnmanagedType.LPWStr)] string pEntName);

        /// <summary>
        /// Gets the object to delete.
        /// </summary>
        /// <value>
        /// The object to delete.
        /// </value>
        private string ObjectToDelete { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        /// <exception cref="Tykma.Core.Engine.CustomExceptions.MethodFaultException">Problem with command.</exception>
        internal bool Execute()
        {
            var lmc1DeleteEntity = this.LoadFunction<LMC1ApiDeleteEnt>("lmc1_DeleteEnt");
            if (lmc1DeleteEntity == null)
            {
                throw new EngineNotConnectedException();
            }

            ReturnCodeMessage rm = this.GetMiniLaseReturnCode(lmc1DeleteEntity(this.ObjectToDelete));

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
