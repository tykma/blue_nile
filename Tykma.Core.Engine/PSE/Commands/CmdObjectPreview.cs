﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdDocPreview.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   The command to get a preview.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Runtime.InteropServices;

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Drawing;

    /// <summary>
    /// The command preview.
    /// </summary>
    internal class CmdObjectPreview : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdDocPreview" /> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking card API.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        public CmdObjectPreview(IntPtr lmcApi, int width, int height, string objName)
            : base(lmcApi)
        {
            this.Height = height;
            this.Width = width;
            this.ObjectName = objName;
        }

        /// <summary>
        /// Get a bitmap preview.
        /// </summary>
        /// <param name="nBmpWidth">Width of the bitmap.</param>
        /// <param name="nBmpHeight">Height of the bitmap.</param>
        /// <returns>
        /// An HBITMAP.
        /// </returns>
        private delegate IntPtr GetPrevBitmapByName2([MarshalAs(UnmanagedType.LPWStr)] string EntName, int nBmpWidth, int nBmpHeight);

        private string ObjectName { get; }
        /// <summary>
        /// Gets the preview height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        private int Height { get; }

        /// <summary>
        /// Gets the preview width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        private int Width { get; }

        /// <summary>
        /// The execute.
        /// </summary>
        /// <returns>
        /// The <see cref="Bitmap"/>.
        /// </returns>
        internal Bitmap Execute()
        {
            var bitmap = this.LoadFunction<GetPrevBitmapByName2>("lmc1_GetPrevBitmapByName2");

            if (bitmap != null)
            {
                try
                {
                    var image = bitmap(this.ObjectName, this.Width, this.Height);

                    if (image != IntPtr.Zero)
                    {
                        var bmp = Image.FromHbitmap(image);
                        NativeMethods.DeleteObject(image);
                        return bmp;
                    }
                 }
                catch (Exception)
                {
                    return new Bitmap(this.Width, this.Height);
                }
            }

            return new Bitmap(this.Width, this.Height);
        }
    }
}
