﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UriAxisParse.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.HTTPD
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Icon.AxisInfo;
    using Tykma.Icon.TCPCommand;

    /// <summary>
    /// The uri axis parse.
    /// </summary>
    [TestClass]
    public class UriAxisParse
    {
        /// <summary>
        /// Tests the axis request.
        /// </summary>
        [TestMethod]
        public void TestAxisRequest()
        {
            var returnAxis = new AxisRequest { Axis = new AxisSetting { Number = 1 } };
            Assert.IsTrue(returnAxis.ValidAxis);
            Assert.AreEqual(1, returnAxis.Axis.Number);
        }

        /// <summary>
        /// Tests the null axis request.
        /// </summary>
        [TestMethod]
        public void TestNullAxisRequest()
        {
            var axis = new AxisRequest();
            Assert.IsFalse(axis.ValidAxis);
        }
    }
}
