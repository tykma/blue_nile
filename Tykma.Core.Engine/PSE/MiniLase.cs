﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MiniLase.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;
    using Commands;
    using CustomExceptions;
    using Entity;

    /// <summary>
    /// Laser Engine interface class.
    /// </summary>
    public class MiniLase : ILaserInterface, IDisposable
    {
        /// <summary>
        /// The file extension
        /// </summary>
        private const string ProjectFileExtension = ".pse";

        /// <summary>
        /// The engine type.
        /// </summary>
        private const EngineTypes.EngineType EngineType = EngineTypes.EngineType.PSE;

        /// <summary>
        /// The limits cancellation token source.
        /// </summary>
        private CancellationTokenSource limitsCancellationTokenSource;

        /// <summary>
        /// The LMC1 API pointer.
        /// </summary>
        private IntPtr libLmcapi;

        /// <summary>
        /// The connected state.
        /// </summary>
        private bool connected;

        /// <summary>
        /// The engine is connecting.
        /// </summary>
        private bool connecting;

        /// <summary>
        /// Initializes a new instance of the <see cref="MiniLase"/> class.
        /// </summary>
        /// <param name="settingMainFolder">The setting main folder.</param>
        public MiniLase(string settingMainFolder)
            : this(settingMainFolder, false)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MiniLase" /> class.
        /// </summary>
        /// <param name="settingMainFolder">The setting main folder.</param>
        /// <param name="motf">if set to <c>true</c> [motf].</param>
        public MiniLase(string settingMainFolder, bool motf)
        {
            // Switch into the minilase directory so that all the dependencies can be loaded via DLLsearch.
            Directory.SetCurrentDirectory(settingMainFolder);

            this.InstallFolder = settingMainFolder;

            this.libLmcapi = NativeMethods.LoadLibrary("MarkPSE.dll");

            this.MarkOnTheFlyMode = motf;
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="MiniLase" /> class.
        /// </summary>
        ~MiniLase()
        {
            this.Dispose(false);
        }

        /// <summary>
        /// Occurs when data is ready.
        /// </summary>
        public event EventHandler<LaserStatusMessage> LaserStatusMessage;

        /// <summary>
        /// Occurs when data is ready.
        /// </summary>
        public event EventHandler<ConnStateMessage> ConnectionMessage;

        /// <summary>
        /// Gets the state of the outputs.
        /// </summary>
        /// <value>
        /// The state of the outputs.
        /// </value>
        public OutputsMemory OutputsState { get; private set; } = new OutputsMemory();

        /// <summary>
        /// Gets a value indicating whether this <see cref="ILaserInterface" /> is connected.
        /// </summary>
        /// <value>
        ///   <c>true</c> if connected; otherwise, <c>false</c>.
        /// </value>
        public bool Connected
        {
            get
            {
                return this.connected;
            }

            private set
            {
                if (value)
                {
                    this.connected = true;
                    this.connecting = false;
                }
                else
                {
                    this.connected = false;
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="MiniLase" /> is connecting.
        /// </summary>
        /// <value>
        ///   <c>true</c> if connecting; otherwise, <c>false</c>.
        /// </value>
        public bool Connecting => this.connecting;

        /// <summary>
        /// Gets the file extension.
        /// </summary>
        /// <value>
        /// The file extension.
        /// </value>
        public string FileExtension => ProjectFileExtension;

        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <value>
        /// The engine.
        /// </value>
        public EngineTypes.EngineType Engine => EngineType;

        /// <summary>
        /// Gets a value indicating whether [limits on].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [limits on]; otherwise, <c>false</c>.
        /// </value>
        public bool LimitsOn => this.limitsCancellationTokenSource != null;

        /// <summary>
        /// Gets a value indicating whether [marking is busy].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [marking busy]; otherwise, <c>false</c>.
        /// </value>
        public bool MarkingBusy { get; private set; }

        /// <summary>
        /// Gets a value indicating whether were [limits contoured].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [limits contoured]; otherwise, <c>false</c>.
        /// </value>
        public bool LimitsContoured { get; private set; }

        /// <summary>
        /// Gets a value indicating whether we are going to mark in mark on the fly mode.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [mark on the fly mode]; otherwise, <c>false</c>.
        /// </value>
        private bool MarkOnTheFlyMode { get; }

        /// <summary>
        /// Gets or sets the field rotation parameter.
        /// </summary>
        /// <value>
        /// The rotation parameter.
        /// </value>
        private double RotationParamater { get; set; }

        /// <summary>
        /// Gets the install folder.
        /// </summary>
        /// <value>
        /// The install folder.
        /// </value>
        private string InstallFolder { get; }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task<bool> StartMarkObject(string objectName)
        {
            // Start marking if we aren't busy.
            if (!this.MarkingBusy)
            {
                var argsStarted = new LaserStatusMessage { Status = MarkState.Started };

                this.LaserStatusMessage?.Invoke(this, argsStarted);

                this.MarkingBusy = true;
                var result = await Task.Run(() => this.MarkObject(objectName));
                this.MarkingBusy = false;

                var rm = new LaserStatusMessage();

                if (result)
                {
                    rm.Status = MarkState.Complete;
                }
                else
                {
                    rm.Status = MarkState.Exception;
                }

                this.LaserStatusMessage?.Invoke(this, rm);
            }
            else
            {
                var args = new LaserStatusMessage { Status = MarkState.InProgress };

                this.LaserStatusMessage?.Invoke(this, args);
            }

            return true;
        }


        /// <summary>
        /// Starts the mark.
        /// </summary>
        /// <returns>
        /// Mark attempt success state.
        /// </returns>
        public async Task<bool> StartMark()
        {
            // Start marking if we aren't busy.
            if (!this.MarkingBusy)
            {
                var argsStarted = new LaserStatusMessage { Status = MarkState.Started };

                this.LaserStatusMessage?.Invoke(this, argsStarted);

                this.MarkingBusy = true;
                var result = await Task.Run(() => this.Mark());
                this.MarkingBusy = false;

                var rm = new LaserStatusMessage();

                if (result.Success)
                {
                    rm.Status = MarkState.Complete;
                }
                else
                {
                    rm.Status = MarkState.Exception;
                    rm.ErrorDescription = result.ReturnDescription;
                }

                this.LaserStatusMessage?.Invoke(this, rm);
            }
            else
            {
                var args = new LaserStatusMessage { Status = MarkState.InProgress };

                this.LaserStatusMessage?.Invoke(this, args);
            }

            return true;
        }

        /// <summary>
        /// Starts the limits.
        /// </summary>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        /// <param name="contourMark">if set to <c>true</c> [contour mark].</param>
        public async void StartLimits(bool enable, bool contourMark, string textName = null)
        {
            if (enable)
            {
                if (!this.LimitsOn)
                {
                    this.limitsCancellationTokenSource = new CancellationTokenSource();
                    var token = this.limitsCancellationTokenSource.Token;

                    this.LimitsContoured = contourMark;

                    var argsStarted = new LaserStatusMessage { Status = MarkState.LimitsStarted };

                    this.LaserStatusMessage?.Invoke(this, argsStarted);

                    await Task.Run(
                        () =>
                        {
                            while (true)
                            {
                                if (token.IsCancellationRequested)
                                {
                                    var argsStopped = new LaserStatusMessage { Status = MarkState.LimitsStopped };

                                    this.LaserStatusMessage?.Invoke(this, argsStopped);

                                    this.limitsCancellationTokenSource = null;
                                    break;
                                }

                                try
                                {
                                    bool state;

                                    if (textName != null)
                                    {
                                        state = this.LimitsObject(textName);
                                    }
                                    else
                                    {
                                        state = this.Limits(contourMark);

                                    }
                                    if (!state)
                                    {
                                        this.limitsCancellationTokenSource = null;
                                        break;
                                    }
                                }
                                catch (EngineNotConnectedException)
                                {
                                    var argsStopped = new LaserStatusMessage { Status = MarkState.LimitsStopped, Fault = true, ErrorDescription = "Engine not connected" };

                                    this.LaserStatusMessage?.Invoke(this, argsStopped);

                                    this.limitsCancellationTokenSource = null;
                                    break;
                                }
                                catch (MethodFaultException e)
                                {
                                    var argsStopped = new LaserStatusMessage { Status = MarkState.LimitsStopped, Fault = true, ErrorDescription = e.Message };

                                    this.LaserStatusMessage?.Invoke(this, argsStopped);

                                    this.limitsCancellationTokenSource = null;
                                    break;
                                }
                            }
                        },
                    token);
                }
            }
            else
            {
                if (this.LimitsOn)
                {
                    this.limitsCancellationTokenSource.Cancel();
                }
            }
        }

        /// <summary>
        /// Connects to the card.
        /// </summary>
        /// <returns>
        /// Connection attempt success state.
        /// </returns>
        public async Task<bool> Connect()
        {
            this.connecting = true;

            ReturnCodeMessage result = await Task.Run(() => this.InitializeCard(this.InstallFolder));

            // Fire off a message to our listeners with the state of our connection.
            var args = new ConnStateMessage { Status = result.Success ? ConnectionState.Connected : ConnectionState.NotConnected };
            {
                if (!result.Success)
                {
                    args.Fault = result.ReturnDescription;
                    this.Connected = false;
                }
                else
                {
                    this.SetLaserPower(255, 0);
                    var t = this.AddTextToProject(new TextObjectAttributes("a", " ", 0, 0, 0, 0, 0, 255, false));
                    this.ScaleSize("a", 0, 0, .1, .1);
                    this.Mark();
                    this.DeleteObject("a");
                    this.Connected = true;
                }

                this.ConnectionMessage?.Invoke(this, args);
            }

            return this.Connected;
        }

        /// <summary>
        /// Disconnects from the card.
        /// </summary>
        public void Disconnect()
        {
            this.Connected = false;
            this.connecting = false;
            var close = new CmdConnClose(this.libLmcapi);
            close.Execute();
        }

        /// <summary>
        /// Clears the project.
        /// </summary>
        /// <returns>
        /// Success state.
        /// </returns>
        public async Task<bool> ClearProject()
        {
            return await Task.FromResult(false);
        }

        /// <summary>
        /// Gets the documents list.
        /// </summary>
        /// <returns>
        /// List of documents.
        /// </returns>
        public List<string> GetDocumentsList()
        {
            return new List<string>();
        }

        /// <summary>
        /// Gets the laser power setting.
        /// </summary>
        /// <param name="penNumber">The pen number.</param>
        /// <returns>
        /// Power setting.
        /// </returns>
        public double GetLaserPower(int penNumber)
        {
            var penCommand = new CmdParamPenGet(this.libLmcapi, penNumber);

            PenParameter pen = penCommand.Execute();

            return pen.PowerRatio;
        }

        /// <summary>
        /// Gets the laser frequency.
        /// </summary>
        /// <param name="penNumber">The pen number.</param>
        /// <returns>
        /// Frequency associated with given object.
        /// </returns>
        public int GetLaserFrequency(int penNumber)
        {
            var penCommand = new CmdParamPenGet(this.libLmcapi, penNumber);

            PenParameter pen = penCommand.Execute();

            return pen.Frequency;
        }

        /// <summary>
        /// Sets the laser power.
        /// </summary>
        /// <param name="penNumber">The pen number.</param>
        /// <param name="power">The power to set.</param>
        /// <returns>
        /// Sucess state.
        /// </returns>
        public bool SetLaserPower(int penNumber, double power)
        {
            // Create a command to get pen parameters.
            var penCommand = new CmdParamPenGet(this.libLmcapi, penNumber);

            // Get the pen information.
            PenParameter pen = penCommand.Execute();

            // Change the pen power setting.
            pen.PowerRatio = power;

            // Set the pen number.
            pen.PenNumber = penNumber;

            return this.SetPenParameter(pen);
        }

        /// <summary>
        /// Sets the laser frequency.
        /// </summary>
        /// <param name="penNumber">The pen number.</param>
        /// <param name="frequency">The frequency to set.</param>
        /// <returns>
        /// Success state.
        /// </returns>
        public bool SetLaserFrequency(int penNumber, int frequency)
        {
            // Create a command to get pen parameters.
            var penCommand = new CmdParamPenGet(this.libLmcapi, penNumber);

            // Get the pen information.
            PenParameter pen = penCommand.Execute();

            // Change the pen power setting.
            pen.Frequency = (short)frequency;

            // Set the pen number.
            pen.PenNumber = penNumber;

            return this.SetPenParameter(pen);
        }

        /// <summary>
        /// Adds a text object to the library (marking window).
        /// </summary>
        /// <param name="textObject">The text object.</param>
        /// <returns>
        /// Return Code.
        /// </returns>
        /// <exception cref="MethodFaultException">Method fault.</exception>
        /// <exception cref="EngineNotConnectedException">Engine not connected.</exception>
        public bool AddTextToProject(TextObjectAttributes textObject)
        {
            return new CmdObjectCreateText(this.libLmcapi, textObject).Execute();
        }

        /// <summary>
        /// Homes the specified axis.
        /// </summary>
        /// <param name="whichAxis">Which axis number to home.</param>
        /// <returns>
        /// Return Code message
        /// </returns>
        public async Task<bool> AxisFindHome(int whichAxis)
        {
            return new CmdAxisHome(this.libLmcapi, whichAxis).Execute();
        }

        /// <summary>
        /// Moves the axis to a specified point.
        /// </summary>
        /// <param name="axis">The axis number.</param>
        /// <param name="goalPos">The goal position.</param>
        /// <returns>
        /// Return code message.
        /// </returns>
        public bool AxisMoveToPosition(int axis, double goalPos)
        {
            return new CmdAxisMove(this.libLmcapi, axis, goalPos).Execute();
        }

        /// <summary>
        /// Moves the axis a by specified pulse.
        /// </summary>
        /// <param name="axis">The axis number.</param>
        /// <param name="goalPos">The pulses to send.</param>
        /// <returns>
        /// Return code message.
        /// </returns>
        public bool AxisMoveToPulse(int axis, double goalPos)
        {
            return new CmdAxisPulse(this.libLmcapi, axis, goalPos).Execute();
        }

        /// <summary>
        /// Changes the value of a text field.
        /// </summary>
        /// <param name="textName">Name of the text.</param>
        /// <param name="textValue">The text value.</param>
        /// <returns>
        /// Success state.
        /// </returns>
        /// <exception cref="MethodFaultException">Method fault.</exception>
        /// <exception cref="EngineNotConnectedException">Engine not connected.</exception>
        public async Task<bool> ChangeTextByName(string textName, string textValue)
        {
            return new CmdObjectSetData(this.libLmcapi, textName, textValue).Execute();
        }

        /// <summary>
        /// Gets the axis coordinate.
        /// </summary>
        /// <param name="axis">The axis number.</param>
        /// <returns>
        /// Axis coordinate.
        /// </returns>
        public double GetAxisCoordinate(int axis)
        {
            return new CmdAxisGetCoordinate(this.libLmcapi, axis).Execute();
        }

        /// <summary>
        /// Gets the data of an object by its name
        /// </summary>
        /// <param name="text">The object to look at.</param>
        /// <returns>
        /// Object data.
        /// </returns>
        /// <exception cref="MethodFaultException">Fault occurred while retrieving data.</exception>
        /// <exception cref="EngineNotConnectedException">Engine was not connected.</exception>
        public string GetObjectDataByName(string text)
        {
            return new CmdObjectGetData(this.libLmcapi, text).Execute();
        }

        /// <summary>
        /// Gets the list of objects.
        /// </summary>
        /// <returns>
        /// A list of objects.
        /// </returns>
        public IEnumerable<string> GetListOfObjects()
        {
            // First we'll get the total count of items.
            int totalItems = this.GetTotalAmountOfIDs();

            // List which will hold all found IDs.
            var listOfEntities = new List<string>();

            // Now we will iterate through each item and add it to our list.
            for (int i = 0; i < totalItems; i++)
            {
                // Create the stringbuilder objects which will be passed by reference to the minilase API.
                var entName = this.GetObjectName(i);

                if (!string.IsNullOrEmpty(entName))
                {
                    listOfEntities.Add(entName);
                }
            }

            return listOfEntities;
        }

        /// <summary>
        /// Gets the preview bitmap.
        /// </summary>
        /// <param name="width">The width of the bitmap to return.</param>
        /// <param name="height">The height of the bitmap to return.</param>
        /// <returns>
        /// A bitmap.
        /// </returns>
        public Bitmap GetPreviewBitmap(int width, int height)
        {
            return new CmdDocPreview(this.libLmcapi, width, height).Execute();
        }

        public Bitmap GetPreviewBitmap(int width, int height, string objName)
        {
            return new CmdObjectPreview(this.libLmcapi, width, height, objName).Execute();
        }

        /// <summary>
        /// Loads a PSE file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>
        /// Return code message.
        /// </returns>
        public async Task<bool> LoadDocument(string fileName)
        {
            return new CmdDocLoad(this.libLmcapi, fileName).Execute();
        }

        /// <summary>
        /// Moves given object to specified position.
        /// </summary>
        /// <param name="text">The name of the object..</param>
        /// <param name="moveX">Position in X plane to move to.</param>
        /// <param name="moveY">Position in Y plane to move to.</param>
        /// <returns>
        /// Return code message.
        /// </returns>
        public bool MoveObject(string text, double moveX, double moveY)
        {
            return new CmdObjectMove(this.libLmcapi, text, moveX, moveY).Execute();
        }

        /// <summary>
        /// Reads the I/O port.
        /// </summary>
        /// <returns>
        /// Success state.
        /// </returns>
        public ushort ReadPort()
        {
            return new CmdIORead(this.libLmcapi).Execute();
        }

        /// <summary>
        /// Resets the specified axis.
        /// </summary>
        /// <param name="axis1">if set to <c>true</c> [reset axis 1].</param>
        /// <param name="axis2">if set to <c>true</c> [reset axis 2].</param>
        /// <param name="axis3">if set to <c>true</c> [reset axis 3].</param>
        /// <param name="axis4">if set to <c>true</c> [reset axis 4].</param>
        /// <returns>
        /// Return code message.
        /// </returns>
        public bool ResetAxis(bool axis1, bool axis2, bool axis3, bool axis4)
        {
            return new CmdAxisReset(this.libLmcapi, axis1, axis2).Execute();
        }

        /// <summary>
        /// Rotates a given object.
        /// </summary>
        /// <param name="text">The object name.</param>
        /// <param name="centerX">The center in the X plane.</param>
        /// <param name="centerY">The center in the Y plane.</param>
        /// <param name="angle">The angle to rotate.</param>
        /// <returns>
        /// Success state.
        /// </returns>
        public bool RotateObject(string text, double centerX, double centerY, double angle)
        {
            return new CmdObjectRotate(this.libLmcapi, text, centerX, centerY, angle).Execute();
        }

        /// <summary>
        /// Saves the laser engine file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>
        /// Return code message.
        /// </returns>
        public bool SaveFile(FileInfo fileName)
        {
            return new CmdDocSave(this.libLmcapi, fileName.FullName).Execute();
        }

        /// <summary>
        /// Scales the size of an object.
        /// </summary>
        /// <param name="whichEntity">Which object to scale.</param>
        /// <param name="centerX">The center X.</param>
        /// <param name="centerY">The center Y.</param>
        /// <param name="scaleX">The scale X.</param>
        /// <param name="scaleY">The scale Y.</param>
        /// <returns>
        /// Success state.
        /// </returns>
        public bool ScaleSize(string whichEntity, double centerX, double centerY, double scaleX, double scaleY)
        {
            return new CmdObjectScale(this.libLmcapi, whichEntity, centerX, centerY, scaleX, scaleY).Execute();
        }

        /// <summary>
        /// Deletes an object.
        /// </summary>
        /// <param name="whichEntity">Which object.</param>
        /// <returns>
        /// Return code.
        /// </returns>
        /// <exception cref="Tykma.Core.Engine.CustomExceptions.EngineNotConnectedException">Engine not connected.</exception>
        public bool DeleteObject(string whichEntity)
        {
            return new CmdObjectDelete(this.libLmcapi, whichEntity).Execute();
        }

        /// <summary>
        /// Sets the hatch parameter.
        /// </summary>
        /// <param name="enableContour">if set to <c>true</c> [enable contour].</param>
        /// <param name="hatch1">First hatch object.</param>
        /// <param name="hatch2">Second hatch object.</param>
        /// <returns>
        /// Return code message.
        /// </returns>
        public bool SetHatchParameter(bool enableContour, HatchAttributes hatch1, HatchAttributes hatch2)
        {
            return new CmdParamSetHatch(this.libLmcapi, enableContour, hatch1, hatch2).Execute();
        }

        /// <summary>
        /// Toggles a pen on or off.
        /// </summary>
        /// <param name="whichPen">The which pen.</param>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        /// <returns>
        /// Success state.
        /// </returns>
        public bool SetPenDisableState(int whichPen, bool enable)
        {
            return new CmdParamPenToggle(this.libLmcapi, whichPen, enable).Execute();
        }

        /// <summary>
        /// Sets the pen parameter.
        /// </summary>
        /// <param name="pen">The pen object.</param>
        /// <returns>
        /// Return code message.
        /// </returns>
        public bool SetPenParameter(PenParameter pen)
        {
            return new CmdParamPenSet(this.libLmcapi, pen).Execute();
        }

        /// <summary>
        /// Rotate marking field.
        /// </summary>
        /// <param name="centerX">The center X.</param>
        /// <param name="centerY">The center Y.</param>
        /// <param name="rotateAngle">The rotation angle.</param>
        /// <returns>
        /// Success state.
        /// </returns>
        public bool SetRotationParameter(double centerX, double centerY, double rotateAngle)
        {
            if (new CmdDocRotate(this.libLmcapi, centerX, centerY, rotateAngle).Execute())
            {
                this.RotationParamater = rotateAngle;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the color of a pen.
        /// </summary>
        /// <param name="whichPen">Which pen number.</param>
        /// <returns>
        /// The pen number.
        /// </returns>
        /// <exception cref="MethodFaultException">There was a fault.</exception>
        /// <exception cref="EngineNotConnectedException">The engine is not connected.</exception>
        public int GetPenColor(int whichPen)
        {
            return new CmdParamPenGetColor(this.libLmcapi, whichPen).Execute();
        }

        /// <summary>
        /// Gets then pen number associated with an object.
        /// </summary>
        /// <param name="whichEnt">Object to look at.</param>
        /// <returns>
        /// Pen Number
        /// </returns>
        public int GetPenNumberFromObject(string whichEnt)
        {
            return new CmdObjectGetPen(this.libLmcapi, whichEnt).Execute();
        }

        public int SetPenNumberToObject(string whichEnt, int penNo)
        {
            return new CmdObjectSetPen(this.libLmcapi, whichEnt, penNo).Execute();
        }

        /// <summary>
        /// Hatches an specified object.
        /// </summary>
        /// <param name="whichEnt">Which object to hatch.</param>
        /// <returns>
        /// Return code.
        /// </returns>
        public bool HatchObject(string whichEnt)
        {
            return new CmdObjectSetFill(this.libLmcapi, whichEnt).Execute();
        }

        /// <summary>
        /// Gets the size of an object.
        /// </summary>
        /// <param name="whichEntity">Which entity.</param>
        /// <returns>
        /// An entity object.
        /// </returns>
        public EntityInformation GetObjectSize(string whichEntity)
        {
            return new CmdObjectGetSize(this.libLmcapi, whichEntity).Execute();
        }

        /// <summary>
        /// Stops marking.
        /// </summary>
        /// <returns>
        /// Return code message.
        /// </returns>
        /// <exception cref="EngineNotConnectedException">Engine not connected.</exception>
        /// <exception cref="MethodFaultException">Method fault.</exception>
        public async Task<bool> StopMark()
        {
            // We are no longer marking.
            this.MarkingBusy = false;
            //Task t = new Task(() => new CmdExecStop(this.libLmcapi).Execute());


            //await t.ContinueWith(
            //   c => false, TaskContinuationOptions.OnlyOnFaulted);

            //return true;

            var cmd = new CmdExecStop(this.libLmcapi);
            Task.Factory.StartNew(() =>
                { return cmd.Execute(); }).LogExceptions();

            return true;
        }

        /// <summary>
        /// Sets an output.
        /// </summary>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        /// <param name="whichPort">The port.</param>
        /// <returns>
        /// Success state.
        /// </returns>
        /// <exception cref="EngineNotConnectedException">Engine not connected.</exception>
        /// <exception cref="MethodFaultException">Method fault.</exception>
        public async Task<bool> SetOutput(bool enable, int whichPort)
        {
            // We will generate the proper word to write and write it.
            var iomanip = new OutputManipulation();
            var portToWrite = iomanip.GeneratePortWordToWrite(enable, whichPort, this.OutputsState);
            var rm = this.WritePort(portToWrite);

            // We'll set our output memory.
            this.OutputsState = iomanip.SetOutputMemory(enable, whichPort - 1, this.OutputsState);

            return rm;
        }

        public bool ResetSN(string obj)
        {
            var cmd = new CmdObjectResetSN(this.libLmcapi, obj);
            return cmd.Execute();
        }


        /// <summary>
        /// The bulk of the clean-up code is implemented in Dispose(true).
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.CloseLMC();
            }

            // Free unmanaged resources.
            this.libLmcapi = IntPtr.Zero;
        }

        /// <summary>
        /// Writes the port.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns>
        /// IO Status.
        /// </returns>
        private bool WritePort(ushort data)
        {
            return new CmdIOWrite(this.libLmcapi, data).Execute();
        }

        /// <summary>
        /// Disconnects from the LEC card.
        /// </summary>
        private void CloseLMC()
        {
            new CmdConnClose(this.libLmcapi).Execute();
        }

        /// <summary>
        /// Initialize the card.
        /// </summary>
        /// <param name="dir">The directory.</param>
        /// <returns>
        /// Success state.
        /// </returns>
        private ReturnCodeMessage InitializeCard(string dir)
        {
            return new CmdConnInitialize(this.libLmcapi, dir).Execute();
        }

        /// <summary>
        /// Starts limits (red pointer laser outlining).
        /// </summary>
        /// <param name="contourTrace">if set to <c>true</c> [contour trace].</param>
        /// <returns>
        /// Success state.
        /// </returns>
        private bool Limits(bool contourTrace = false)
        {
            return contourTrace ? this.LimitsContours() : this.LimitsOutline();
        }



        private bool LimitsObject(string objName)
        {
            return new CmdExecMarkLimitsEntity(this.libLmcapi, objName).Execute();
        }
        /// <summary>
        /// Marks contour with a red light.
        /// </summary>
        /// <returns>
        /// Return Code.
        /// </returns>
        private bool LimitsContours()
        {
            return new CmdExecMarkLimitsContour(this.libLmcapi).Execute();
        }

        /// <summary>
        /// Marks with a red light.
        /// </summary>
        /// <returns>
        /// Return Code.
        /// </returns>
        private bool LimitsOutline()
        {
            return new CmdExecMarkLimitsBox(this.libLmcapi).Execute();
        }

        private bool MarkObject(string objecttoMark)
        {
            return new CmdExecMarkObject(this.libLmcapi, objecttoMark).Execute();
        }

        /// <summary>
        /// Perform marking operation.
        /// </summary>
        /// <returns>
        /// Success code.
        /// </returns>
        private ReturnCodeMessage Mark()
        {
            return new CmdExecMark(this.libLmcapi, this.MarkOnTheFlyMode).Execute();
        }

        /// <inheritdoc />
        public bool SplitIntoBoxes(IList<SplitBox> boxes)
        {
            var cmd = new CmdObjectSplitBox(this.libLmcapi, boxes);

            return cmd.Execute();
        }

        /// <summary>
        /// Gets the name of an object by its ID.
        /// </summary>
        /// <param name="count">The numerical ID of the object.</param>
        /// <returns>
        /// Name of the object.
        /// </returns>
        private string GetObjectName(int count)
        {
            return new CmdObjectGetName(this.libLmcapi, count).Execute();
        }

        /// <summary>
        /// Gets the total amount of editable IDs.
        /// </summary>
        /// <returns>
        /// Amount of IDs in the document.
        /// </returns>
        private int GetTotalAmountOfIDs()
        {
            return new CmdObjectCount(this.libLmcapi).Execute();
        }

        public bool SetFontHeight(string entity, double fontHeight)
        {
            var d = new CmdGetTextParameter(this.libLmcapi, entity).Execute();

            var newparams = new TextParameter(
                d.FontName,
                d.FontSpace,
                fontHeight,
                d.CharacterWidthRatio,
                d.CharacterAngle,
                d.CharacterSpace,
                d.LineSpace,
                d.NullCharWidthRatio,
                d.TextAlign,
                d.IsBold,
                d.IsItalic,
                d.TextSpaceMode);

            var x = new CmdSetTextParameter(this.libLmcapi, entity, newparams).Execute();

            return true;
        }

        public bool SetFont(string entity, string fontName)
        {
            var d = new CmdGetTextParameter(this.libLmcapi, entity).Execute();

            var newparams = new TextParameter(
                fontName,
                d.FontSpace,
                d.CharacterHeight,
                d.CharacterWidthRatio,
                d.CharacterAngle,
                d.CharacterSpace,
                d.LineSpace,
                d.NullCharWidthRatio,
                d.TextAlign,
                d.IsBold,
                d.IsItalic,
                d.TextSpaceMode);

            var x = new CmdSetTextParameter(this.libLmcapi, entity, newparams).Execute();

            return true;
        }

        public bool SetVectorPath(string id, string path, string newID, double degree, bool scaling)
        {

            var hatchlist = new List<KeyValuePair<int, HatchEntAttributes>>();

            for (int i = 1; i <= 3; i++)
            {
                try
                {
                    var hatch = new CmdParamGetHatch(this.libLmcapi, id, i).Execute();
                    hatchlist.Add(new KeyValuePair<int, HatchEntAttributes>(i, hatch));
                }
                catch (Exception)
                {
                }
            }

            double getHeight(EntityInformation ent)
            {
                return Math.Sqrt(Math.Pow(ent.MinY - ent.MaxY, 2));
            }

            double getWidth(EntityInformation ent)
            {
                return Math.Sqrt(Math.Pow(ent.MinX - ent.MaxX, 2));
            }

            var sz = this.GetObjectSize(id);
            var pen = this.GetPenNumberFromObject(id);
            var bbCenterX = (sz.MaxX + sz.MinX) / 2;
            var bbCenterY = (sz.MaxY + sz.MinY) / 2;
            //this.DeleteObject(id);

            bool hatchedVector = hatchlist.Count > 0;

            var cmd = new CmdObjectAddVector(this.libLmcapi, path, newID, 0, 0, 0, 0, 1, pen, hatchedVector ? 1 : 0);
            cmd.Execute();

            var rcmd = new CmdObjectRotate(this.libLmcapi, newID, 0, 0, degree);
            rcmd.Execute();

            var x = new CmdObjectSetPen(this.libLmcapi, newID, pen).Execute();

            var originalHeight = getHeight(sz);
            var originalWidth = getWidth(sz);
            var newsize = this.GetObjectSize(newID);
            var newHeight = getHeight(newsize);
            var newWidth = getWidth(newsize);

            var scaleW = originalWidth / newWidth;
            var scaleH = originalHeight / newHeight;

            double scale = scaleW > scaleH ? scaleH : scaleW;

            if (!scaling)
            {
                scale = 1;
            }

            this.ScaleSize(newID, bbCenterX, bbCenterY, scale, scale);

            var posafterscaling = this.GetObjectSize(newID);

            var diff = sz.MinX - posafterscaling.MinX;
            var diffY = sz.MinY - posafterscaling.MinY;

            this.MoveObject(newID, diff, diffY);

            foreach (var hatch in hatchlist)
            {
                var setHatch = new CmdParamSetEntHatch(this.libLmcapi, hatch.Value, newID, hatch.Key).Execute();
            }

            return true;
        }

        public bool SetFontItalic(string entity, bool italic)
        {
            var d = new CmdGetTextParameter(this.libLmcapi, entity).Execute();

            var newparams = new TextParameter(
                d.FontName,
                d.FontSpace,
                d.CharacterHeight,
                d.CharacterWidthRatio,
                d.CharacterAngle,
                d.CharacterSpace,
                d.LineSpace,
                d.NullCharWidthRatio,
                d.TextAlign,
                d.IsBold,
                italic,
                d.TextSpaceMode);

            var x = new CmdSetTextParameter(this.libLmcapi, entity, newparams).Execute();

            return true;
        }

        public bool SetTextSpacing(string entity, double space)
        {
            var d = new CmdGetTextParameter(this.libLmcapi, entity).Execute();

            var newparams = new TextParameter(
                d.FontName,
                d.FontSpace,
                d.CharacterHeight,
                d.CharacterWidthRatio,
                d.CharacterAngle,
                space,
                d.LineSpace,
                d.NullCharWidthRatio,
                d.TextAlign,
                d.IsBold,
                d.IsItalic,
                d.TextSpaceMode);

            var x = new CmdSetTextParameter(this.libLmcapi, entity, newparams).Execute();

            return true;
        }

        public HatchEntAttributes GetHatchParameters(string id, int index)
        {
            var cmd = new CmdParamGetHatch(this.libLmcapi, id, index);
            return cmd.Execute();
        }

        public bool SetHatchParameters(string id, HatchEntAttributes attr, int index)
        {
            var cmd = new CmdParamSetEntHatch(this.libLmcapi, attr, id, index);
            return cmd.Execute();
        }
    }
}
