﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AxisSettingTest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Axis setting unit testing.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.AxisSetting
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Icon.AxisInfo;

    /// <summary>
    /// Unit testing for the axis info class.
    /// </summary>
    [TestClass]
    public class AxisSettingTest
    {
        /// <summary>
        /// Tests the incrementing home counter at zero home frequency.
        /// </summary>
        [TestMethod]
        public void TestIncrementingHomeCounterAtZeroHomeFrequency()
        {
            var axisTest = new AxisSetting { HomeFrequency = 0 };
            axisTest.IncreaseHomeCounter();

            // Verify we do need to be homed.
            Assert.IsFalse(axisTest.NeedsHoming);
        }

        /// <summary>
        /// Tests the incrementing home counter.
        /// </summary>
        [TestMethod]
        public void TestIncrementingHomeCounter()
        {
            var axisTest = new AxisSetting();

            // Let's make sure the axis says it needs to be HOMED on initialization.
            Assert.AreEqual(true, axisTest.NeedsHoming);

            // We'll set the home frequency to 3.
            axisTest.HomeFrequency = 3;

            // Verify we still need to be homed.
            Assert.AreEqual(true, axisTest.NeedsHoming);

            axisTest.IncreaseHomeCounter();

            // Verify we do need to be homed.
            Assert.AreEqual(false, axisTest.NeedsHoming);

            for (int i = 0; i < 45; i++)
            {
                axisTest.IncreaseHomeCounter();
            }

            // Verify we need to be homed.
            Assert.AreEqual(true, axisTest.NeedsHoming);

            axisTest.ResetHomeCounter();

            // Verify we do need to be homed.
            Assert.AreEqual(false, axisTest.NeedsHoming);
        }

        /// <summary>
        /// Tests the axis homed outside of home sequence.
        /// </summary>
        [TestMethod]
        public void TestAxisHomedOutsideOfHomeSequence()
        {
            var axisTest = new AxisSetting();

            // Let's make sure the axis says it needs to be HOMED on initialization.
            Assert.AreEqual(true, axisTest.NeedsHoming);

            // We'll set the home frequency to 5.
            axisTest.HomeFrequency = 5;

            // Verify we still need to be homed.
            Assert.AreEqual(true, axisTest.NeedsHoming);

            // Increment home counter.
            axisTest.IncreaseHomeCounter();

            // Tell axis it is homed.
            axisTest.Homing = false;
            axisTest.Homed = true;
            axisTest.IncreaseHomeCounter();

            // Verify we don't need to be homed.
            Assert.IsFalse(axisTest.NeedsHoming);

            // Increment home counter.
            axisTest.IncreaseHomeCounter();

            // Tell axis it is homed.
            axisTest.Homing = false;
            axisTest.Homed = true;
            axisTest.IncreaseHomeCounter();

            // Verify we don't need to be homed.
            Assert.IsFalse(axisTest.NeedsHoming);
        }
    }
}
