﻿// <copyright file="IconLightDialogService.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Interface.NG.DialogService
{
    using System;
    using System.Windows.Forms;

    /// <summary>
    /// A simple message dialog form.
    /// </summary>
    public class IconLightDialogService : IDialogService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IconLightDialogService"/> class.
        /// </summary>
        public IconLightDialogService()
        {
            this.Form = new SimpleMessage();
            this.Form.Visible = false;
        }

        /// <summary>
        /// Gets or sets the form.
        /// </summary>
        /// <value>
        /// The form.
        /// </value>
        private SimpleMessage Form { get; set; }

        /// <summary>
        /// Shows the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="title">The title.</param>
        public void ShowMessage(string message, string title)
        {
            this.Form.Text = title;

            this.Form.Message = message;

            if (this.Form.Visible)
            {
                if (this.Form.WindowState == FormWindowState.Minimized)
                {
                    this.Form.WindowState = FormWindowState.Normal;
                }

                this.Form.BringToFront();
            }
            else
            {
                this.Form.Show();
            }
        }

        /// <summary>
        /// Hides the message.
        /// </summary>
        public void HideMessage()
        {
            this.Form.Hide();
        }

        /// <summary>
        /// Shows the error.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="title">The title.</param>
        public void ShowError(string message, string title)
        {
            throw new NotImplementedException();
        }
    }
}
