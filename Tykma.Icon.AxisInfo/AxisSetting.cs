﻿// <copyright file="AxisSetting.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Icon.AxisInfo
{
    /// <summary>
    /// Axis setting class.
    /// </summary>
    public class AxisSetting
    {
        /// <summary>
        /// The home counter.
        /// </summary>
        private int homeCounter;

        /// <summary>
        /// Initializes a new instance of the <see cref="AxisSetting" /> class.
        /// </summary>
        public AxisSetting()
        {
            this.NeedsHoming = true;
        }

        /// <summary>
        /// AXIS enumerators.
        /// </summary>
        public enum Axis
        {
            /// <summary>
            /// The Z axis.
            /// </summary>
            Z,

            /// <summary>
            /// The R axis.
            /// </summary>
            R,
        }

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets the letter.
        /// </summary>
        /// <value>
        /// The letter.
        /// </value>
        public string Letter { get; set; }

        /// <summary>
        /// Gets or sets the axis common name.
        /// </summary>
        /// <value>
        /// The common name.
        /// </value>
        public string CommonName { get; set; }

        /// <summary>
        /// Gets or sets which axis it is.
        /// </summary>
        /// <value>
        /// The which axis.
        /// </value>
        public Axis WhichAxis { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="AxisSetting" /> is homed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if homed; otherwise, <c>false</c>.
        /// </value>
        public bool Homed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="AxisSetting" /> is homing.
        /// </summary>
        /// <value>
        ///   <c>true</c> if homing; otherwise, <c>false</c>.
        /// </value>
        public bool Homing { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="AxisSetting" /> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [needs homing].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [needs homing]; otherwise, <c>false</c>.
        /// </value>
        public bool NeedsHoming { get; set; }

        /// <summary>
        /// Sets the home frequency.
        /// </summary>
        /// <value>
        /// The home frequency.
        /// </value>
        public int HomeFrequency { private get; set; }

        /// <summary>
        /// Increases the home counter.
        /// </summary>
        public void IncreaseHomeCounter()
        {
            this.homeCounter += 1;
            if (this.HomeFrequency != 0)
            {
                this.NeedsHoming = this.homeCounter >= this.HomeFrequency;
            }
            else
            {
                this.NeedsHoming = false;
            }
        }

        /// <summary>
        /// Resets the home counter.
        /// </summary>
        public void ResetHomeCounter()
        {
            this.homeCounter = 0;
            this.NeedsHoming = false;
        }
    }
}
