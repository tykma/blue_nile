﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlButtonStart.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    using System.ComponentModel;
    using System.Drawing;

    using Icon_Interface.Properties;

    /// <summary>
    /// The control button for start.
    /// </summary>
    public class ControlButtonStart : ControlButtonBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlButtonStart"/> class.
        /// </summary>
        public ControlButtonStart()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// The initialize component.
        /// </summary>
        private void InitializeComponent()
        {
            ((ISupportInitialize)this.ux_Image_Icon).BeginInit();
            this.SuspendLayout();

            // ux_Image_Icon
            this.ux_Image_Icon.Image = ImageResources.start_cycle;

            // ux_Label_Action
            this.ux_Label_Action.Text = Resources.Main_Start_Button;

            // ControlButtonStart
            this.AutoScaleDimensions = new SizeF(6F, 13F);
            this.Name = "ControlButtonStart";
            ((ISupportInitialize)this.ux_Image_Icon).EndInit();
            this.ResumeLayout(false);
        }
    }
}
