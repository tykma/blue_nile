﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdConnInitialize.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// The command to initialize the library.
    /// </summary>
    internal class CmdConnInitialize : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdConnInitialize"/> class.
        /// </summary>
        /// <param name="lmcApi">The LMC API.</param>
        /// <param name="path">The path.</param>
        public CmdConnInitialize(IntPtr lmcApi, string path)
            : base(lmcApi)
        {
            this.Path = path;
        }

        /// <summary>
        /// Initializes the card.
        /// </summary>
        /// <param name="pathName">Location of the laser engine installation.</param>
        /// <param name="bTestMode">if set to <c>true</c> [b test mode].</param>
        /// <returns>Success code.</returns>
        private delegate int LMC1ApiInitial([MarshalAs(UnmanagedType.LPWStr)] string pathName, bool bTestMode);

        /// <summary>
        /// Gets the path to the software installation.
        /// </summary>
        /// <value>
        /// The path.
        /// </value>
        private string Path { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>A Return Code Message.</returns>
        internal ReturnCodeMessage Execute()
        {
            try
            {
                var lmc1Initialize = this.LoadFunction<LMC1ApiInitial>("lmc1_Initial2");

                if (lmc1Initialize == null)
                {
                   return new ReturnCodeMessage { HardError = true, ReturnDescription = "Problem loading engine.", Status = ReturnCodeMsg.Error };
                }

                int result = lmc1Initialize(this.Path, false);
                var rm = this.GetMiniLaseReturnCode(result);
                return rm;
            }
            catch (NullReferenceException)
            {
                var err = new ReturnCodeMessage { HardError = true, ReturnDescription = "Invalid library", Status = ReturnCodeMsg.Error };
                return err;
            }
        }
    }
}
