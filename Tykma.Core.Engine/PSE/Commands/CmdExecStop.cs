﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdExecStop.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// The command to stop all operations.
    /// </summary>
    internal class CmdExecStop : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdExecStop"/> class.
        /// </summary>
        /// <param name="lmcApi">
        /// The laser marking API.
        /// </param>
        public CmdExecStop(IntPtr lmcApi)
            : base(lmcApi)
        {
        }

        /// <summary>
        /// Stops marking.
        /// </summary>
        /// <returns>Success code.</returns>
        private delegate int LMC1ApiStopMark();

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>
        /// The <see cref="ReturnCodeMessage" />.
        /// </returns>
        /// <exception cref="Tykma.Core.Engine.CustomExceptions.MethodFaultException">Problem executing command.</exception>
        internal bool Execute()
        {
            // Get a pointer to the SE stop mark function.
            var lmc1StopMark = this.LoadFunction<LMC1ApiStopMark>("lmc1_StopMark");

            if (lmc1StopMark == null)
            {
                throw new EngineNotConnectedException();
            }

            // Call the stop mark function and get a result.
            int result = lmc1StopMark();

            // Return success state.
            ReturnCodeMessage rm = this.GetMiniLaseReturnCode(result);

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
