﻿// <copyright file="DimensionMathTests.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.SplitMark.UnitTests
{
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Core.Entity;
    using Tykma.Core.SplitMark;

    /// <summary>
    /// Tests for the dimensioning math class.
    /// </summary>
    [TestClass]
    public class DimensionMathTests
    {
        /// <summary>
        /// Test getting boundaries with just one entity.
        /// </summary>
        [TestMethod]
        public void TestGettingBoundariesWithOneEntity()
        {
            var l = new List<EntityInformation>();

            l.Add(new EntityInformation(0, string.Empty, string.Empty, -25, 25, -10, 10, 0));

            var dimensions = new DimensionMath().GetBoundariesOfObjects(l);

            Assert.AreEqual(-25, dimensions.X1);
            Assert.AreEqual(25, dimensions.X2);
            Assert.AreEqual(-10, dimensions.Y1);
            Assert.AreEqual(10, dimensions.Y2);
        }

        /// <summary>
        /// Test getting boundaries with multiple entities.
        /// </summary>
        [TestMethod]
        public void TestGettingBoundariesWithMultipleEntities()
        {
            var l = new List<EntityInformation>();

            l.Add(new EntityInformation(0, string.Empty, string.Empty, -25, 25, -10, 10, 0));
            l.Add(new EntityInformation(0, string.Empty, string.Empty, -26, 26, -11, 11, 0));
            l.Add(new EntityInformation(0, string.Empty, string.Empty, -30, 24, -5, 12, 0));

            var dimensions = new DimensionMath().GetBoundariesOfObjects(l);

            Assert.AreEqual(-30, dimensions.X1);
            Assert.AreEqual(26, dimensions.X2);
            Assert.AreEqual(-11, dimensions.Y1);
            Assert.AreEqual(12, dimensions.Y2);
        }

        /// <summary>
        /// Test splitting into boxes.
        /// </summary>
        [TestMethod]
        public void TestSplittingIntoBoxes()
        {
            var math = new DimensionMath();

            var dimensionBox = new Box(0, 100, 0, 100);

            var splitBoxes = math.SplitIntoBoxes(25, dimensionBox);

            // Box 1
            Assert.AreEqual(0, splitBoxes[0].X1, "First box x1");
            Assert.AreEqual(25, splitBoxes[0].X2, "First box x2");
            Assert.AreEqual(0, splitBoxes[0].Y1);
            Assert.AreEqual(100, splitBoxes[0].Y2);

            // Box 2
            Assert.AreEqual(25, splitBoxes[1].X1, "2nd box x1");
            Assert.AreEqual(50, splitBoxes[1].X2, "2nd box x2");
            Assert.AreEqual(0, splitBoxes[1].Y1);
            Assert.AreEqual(100, splitBoxes[1].Y2);

            // Box 2
            Assert.AreEqual(50, splitBoxes[2].X1, "3rd box x1");
            Assert.AreEqual(75, splitBoxes[2].X2, "3rd box x2 ");
            Assert.AreEqual(0, splitBoxes[2].Y1);
            Assert.AreEqual(100, splitBoxes[2].Y2);

            // Box 4
            Assert.AreEqual(75, splitBoxes[3].X1, "4th box x1");
            Assert.AreEqual(100, splitBoxes[3].X2, "4th box x2");
            Assert.AreEqual(0, splitBoxes[3].Y1);
            Assert.AreEqual(100, splitBoxes[3].Y2);
        }

        /// <summary>
        /// Test splitting into boxes.
        /// </summary>
        [TestMethod]
        public void TestSplittingIntoBoxes2()
        {
            var math = new DimensionMath();

            var dimensionBox = new Box(-50, 50, -10, 10);

            var splitBoxes = math.SplitIntoBoxes(25, dimensionBox);
            Assert.AreEqual(4, splitBoxes.Count);
        }

        /// <summary>
        /// Test splitting into boxes when split distance is zero.
        /// </summary>
        [TestMethod]
        public void TestSplittingIntoBoxesWithSplitDistanceOfZero()
        {
            var math = new DimensionMath();

            var dimensionBox = new Box(0, 100, 0, 100);

            var splitBoxes = math.SplitIntoBoxes(0, dimensionBox);

            Assert.AreEqual(0, splitBoxes.Count);
        }
    }
}
