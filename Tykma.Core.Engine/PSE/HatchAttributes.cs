﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HatchAttributes.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE
{
    /// <summary>
    /// Hatch attributes.
    /// </summary>
    public class HatchAttributes
    {
        /// <summary>
        /// The hatch attribute.
        /// </summary>
        private int hatchAttrib;

        /// <summary>
        /// Initializes a new instance of the <see cref="HatchAttributes"/> class.
        /// </summary>
        /// <param name="enableHatch">if set to <c>true</c> [enable hatch].</param>
        /// <param name="penNumber">The pen number.</param>
        /// <param name="edgeDistance">The edge distance.</param>
        /// <param name="edgeLineDistance">The edge line distance.</param>
        /// <param name="edgeStartOffset">The edge start offset.</param>
        /// <param name="edgeEndOffset">The edge end offset.</param>
        /// <param name="hatchAngle">The hatch angle.</param>
        /// <param name="hatchAllCalc">if set to <c>true</c> [hatch all calculate].</param>
        /// <param name="hatchBiDir">if set to <c>true</c> [hatch bi direction].</param>
        /// <param name="hatchedge">if set to <c>true</c> [hatch edge].</param>
        /// <param name="hatchloop">if set to <c>true</c> [hatch loop].</param>
        public HatchAttributes(
            bool enableHatch,
            int penNumber,
            double edgeDistance,
            double edgeLineDistance,
            double edgeStartOffset,
            double edgeEndOffset,
            double hatchAngle,
            bool hatchAllCalc,
            bool hatchBiDir,
            bool hatchedge,
            bool hatchloop)
        {
            this.EnableHatch = enableHatch;
            this.PenNumber = penNumber;
            this.HatchEdgeDistance = edgeDistance;
            this.HatchLineDistance = edgeLineDistance;
            this.HatchEndOffset = edgeEndOffset;
            this.HatchStartOffset = edgeStartOffset;
            this.HatchAngle = hatchAngle;
            this.HatchAllCalc = hatchAllCalc;
            this.HatchBiDir = hatchBiDir;
            this.HatchEdge = hatchedge;
            this.HatchLoop = hatchloop;
        }

        /// <summary>
        /// Gets or sets a value indicating whether [enable hatch].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enable hatch]; otherwise, <c>false</c>.
        /// </value>
        internal bool EnableHatch { get; set; }

        /// <summary>
        /// Gets or sets the pen number.
        /// </summary>
        /// <value>
        /// The pen number.
        /// </value>
        internal int PenNumber { get; set; }

        /// <summary>
        /// Gets the hatch attribute.
        /// </summary>
        /// <value>
        /// The hatch attribute.
        /// </value>
        internal int HatchAttrib
        {
            get
            {
                this.hatchAttrib = 0;

                if (this.HatchAllCalc)
                {
                    this.hatchAttrib += 1;
                }

                if (this.HatchEdge)
                {
                    this.hatchAttrib += 2;
                }

                if (this.HatchBiDir)
                {
                    this.hatchAttrib += 8;
                }

                if (this.HatchLoop)
                {
                    this.hatchAttrib += 16;
                }

                return this.hatchAttrib;
            }
        }

        /// <summary>
        /// Gets or sets the hatch edge distance.
        /// </summary>
        /// <value>
        /// The hatch edge distance.
        /// </value>
        internal double HatchEdgeDistance { get; set; }

        /// <summary>
        /// Gets or sets the hatch line distance.
        /// </summary>
        /// <value>
        /// The hatch line distance.
        /// </value>
        internal double HatchLineDistance { get; set; }

        /// <summary>
        /// Gets or sets the hatch start offset.
        /// </summary>
        /// <value>
        /// The hatch start offset.
        /// </value>
        internal double HatchStartOffset { get; set; }

        /// <summary>
        /// Gets or sets the hatch end offset.
        /// </summary>
        /// <value>
        /// The hatch end offset.
        /// </value>
        internal double HatchEndOffset { get; set; }

        /// <summary>
        /// Gets or sets the hatch angle.
        /// </summary>
        /// <value>
        /// The hatch angle.
        /// </value>
        internal double HatchAngle { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [hatch all calculate].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [hatch all calculate]; otherwise, <c>false</c>.
        /// </value>
        internal bool HatchAllCalc { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [reciprocating hatch].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [hatch bi direction]; otherwise, <c>false</c>.
        /// </value>
        internal bool HatchBiDir { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [re-mark edge].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [hatch edge]; otherwise, <c>false</c>.
        /// </value>
        internal bool HatchEdge { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [ring like loop].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [hatch loop]; otherwise, <c>false</c>.
        /// </value>
        internal bool HatchLoop { get; set; }
    }
}
