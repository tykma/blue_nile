﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdObjectGetData.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   The command to get text data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;
    using System.Text;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// The command get text data.
    /// </summary>
    internal class CmdObjectGetData : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdObjectGetData"/> class.
        /// </summary>
        /// <param name="lmcApi">
        /// The laser marking card API.
        /// </param>
        /// <param name="textName">
        /// The text name.
        /// </param>
        public CmdObjectGetData(IntPtr lmcApi, string textName)
            : base(lmcApi)
        {
            this.TextName = textName;
        }

        /// <summary>
        /// Get text of an object.
        /// </summary>
        /// <param name="strTextName">Name of the text.</param>
        /// <param name="strText">The text.</param>
        /// <returns>Success code.</returns>
        private delegate int LMC1ApiGetTextByName([MarshalAs(UnmanagedType.LPWStr)] string strTextName, [MarshalAs(UnmanagedType.LPWStr)] StringBuilder strText);

        /// <summary>
        /// Gets the name of the text object.
        /// </summary>
        /// <value>
        /// The name of the text object.
        /// </value>
        private string TextName { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Data string.</returns>
        /// <exception cref="Tykma.Core.Engine.CustomExceptions.MethodFaultException">
        /// Fault occurred.
        /// </exception>
        public string Execute()
        {
            var sb = new StringBuilder(255);

            // Get pointer to get text by name function.
            var lmc1GetTextByName = this.LoadFunction<LMC1ApiGetTextByName>("lmc1_GetTextByName");

            if (!this.TestEngineAndMethodPresence(lmc1GetTextByName))
            {
                return string.Empty;
            }

            ReturnCodeMessage rm;
            // Return the success state.
            try
            { rm = this.GetMiniLaseReturnCode(lmc1GetTextByName(this.TextName, sb));

                if (rm.Success)
                {
                    return sb.ToString();
                }

            }
            catch (Exception e)
            {
                throw new MethodFaultException(e.Message);
            }

            throw new MethodFaultException(rm.ReturnDescription);

        }
    }
}
