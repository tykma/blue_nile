﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdAxisReset.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// Axis reset command.
    /// </summary>
   internal class CmdAxisReset : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdAxisReset"/> class.
        /// </summary>
        /// <param name="lmcApi">The LMC API.</param>
        /// <param name="axis1Reset">if set to <c>true</c> [axis1reset].</param>
        /// <param name="axis2Reset">if set to <c>true</c> [axis2reset].</param>
        public CmdAxisReset(IntPtr lmcApi, bool axis1Reset, bool axis2Reset)
            : base(lmcApi)
        {
            this.ResetAxis1 = axis1Reset;
            this.ResetAxis2 = axis2Reset;
        }

        /// <summary>
        /// Reset the axis.
        /// </summary>
        /// <param name="bEnAxis0">if set to <c>true</c> [reset axis 1].</param>
        /// <param name="bEnAxis1">if set to <c>true</c> [reset axis 2].</param>
        /// <returns>Return code.</returns>
        private delegate int LMC1ApiReset(bool bEnAxis0, bool bEnAxis1);

        /// <summary>
        /// Gets a value indicating whether to reset axis 1.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [reset axis1]; otherwise, <c>false</c>.
        /// </value>
        private bool ResetAxis1 { get; }

        /// <summary>
        /// Gets a value indicating whether reset axis 2.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [reset axis2]; otherwise, <c>false</c>.
        /// </value>
        private bool ResetAxis2 { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        /// <exception cref="Tykma.Core.Engine.CustomExceptions.EngineNotConnectedException">Engine not connected.</exception>
        /// <exception cref="Tykma.Core.Engine.CustomExceptions.MethodFaultException">Problem executing axis reset command.</exception>
        internal bool Execute()
        {
            var lmc1Reset = this.LoadFunction<LMC1ApiReset>("lmc1_Reset");

            if (lmc1Reset == null)
            {
                throw new EngineNotConnectedException();
            }

            var rm = this.GetMiniLaseReturnCode(lmc1Reset(this.ResetAxis1, this.ResetAxis2));

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
