﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DictionaryExtensions.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.TCPCommand
{
    using System.Collections.Generic;

    /// <summary>
    /// The dictionary extensions.
    /// </summary>
    public static class DictionaryExtensions
    {
        /// <summary>
        /// The get value or default.
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="dict">The dictionary.</param>
        /// <param name="key">The key.</param>
        /// <param name="defaultIfNotFound">The default if not found.</param>
        /// <returns>
        /// Found value - or passed default value.
        /// </returns>
        public static TValue GetValueOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, TValue defaultIfNotFound = default(TValue))
        {
            TValue value;

            if (dict == null)
            {
                return defaultIfNotFound;
            }

            // value will be the result or the default for TValue
            if (!dict.TryGetValue(key, out value))
            {
                value = defaultIfNotFound;
            }

            return value;
        }
    }
}
