﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.LANG.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Translation portion.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using Icon_Interface.Properties;

    /// <summary>
    /// Translation portion of the main interface.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// Sets the translation for scanner controls.
        /// </summary>
        private void SetTranslationForScannerControls()
        {
            // Main screen controls.
            this.ux_ScannerInput.Cue = Resources.Text_Scanner_Input;
            this.ux_Control_Quantity.Text = Resources.Label_Quantity;
        }

        /// <summary>
        /// Sets the translation for tool strip.
        /// </summary>
        private void SetTranslationForToolStrip()
        {
            // Tool strip.
            this.ux_ToolStrip_Settings.Text = Resources.ToolStripSettings;
            this.ux_ToolStrip_Exit.Text = Resources.Main_ExitToolStripMenuItem1_Click_Exit;
            this.ux_ToolStrip_About.Text = Resources.ToolStripAbout;
            this.ux_ToolStrip_Minimize.Text = Resources.ToolStripMinimize;
            this.ux_ToolStrip_Globals.Text = Resources.ToolStripGlobalIDs;
            this.ux_ToolStrip_Card.Text = Resources.ToolStripCard;
            this.ux_ToolStrip_IO.Text = Resources.ToolStripIO;
            this.ux_ToolStrip_Axis.Text = Resources.Label_Axis;
            this.ux_ToolStrip_HomeZ.Text = string.Format("{0} {1}", Resources.Label_HomeAnAxis, Resources.Label_AxisZ);
            this.ux_ToolStrip_HomeR.Text = string.Format("{0} {1}", Resources.Label_HomeAnAxis, Resources.Label_AxisR);
        }

        /// <summary>
        /// Sets the translation for mark in progress.
        /// </summary>
        private void SetTranslationForMarkInProgress()
        {
            // Mark in progress.
            this.ux_MarkInProgressLabel.Text = Resources.Mark_In_Progress;
        }

        /// <summary>
        /// Sets the translation for settings.
        /// </summary>
        private void SetTranslationForSettings()
        {
            this.ux_IO_InputsLabel.Text = Resources.Label_Inputs;
            this.ux_IO_OutputsLabel.Text = Resources.Label_Outputs;
            this.ux_Control_InstallFolder.PathLabel = Resources.Label_Install_Folder;
            this.ux_Control_ProjectsPath.PathLabel = Resources.Label_Project_Folder;
            this.ux_Control_ImagesPath.PathLabel = Resources.Label_Images_Folder;
            this.ux_Control_RTFPath.PathLabel = Resources.Label_RTF_Folder;
            this.ux_Check_PervasiveLimitsEnabled.SettingLabel = Resources.Label_Pervasive_Limits;
            this.ux_Check_LoadLastJobEnabled.SettingLabel = Resources.Label_Load_Last_Job;
            this.ux_Set_NewPassLabel.Text = Resources.Label_New_Password;
        }

        /// <summary>
        /// Sets the translation for general settings.
        /// </summary>
        private void SetTranslationForGeneralSettings()
        {
            // General Settings
            this.ux_Check_InstructionsEnabled.SettingLabel = Resources.Label_Instructions;
            this.ux_Check_AccessControlEnabled.SettingLabel = Resources.Label_Access_Control;
        }

        /// <summary>
        /// Sets the translation for settings screen.
        /// </summary>
        private void SetTranslationForSettingsScreen()
        {
            // Settings screen.
            this.ux_Nav_StopLabel.Text = Resources.Main_Stop_Button;
        }

        /// <summary>
        /// Sets the translation for global variables.
        /// </summary>
        private void SetTranslationForGlobalVariables()
        {
            // Global variables.
            this.ux_label_GVCounters.Text = Resources.Label_Counters;
            this.ux_Button_GVCounterAdd.Text = Resources.Button_Add;
            this.ux_label_GVStrings.Text = Resources.Label_Strings;
            this.ux_Button_GVStringAdd.Text = Resources.Button_Add;
            this.ux_Button_GVCounterDel.Text = Resources.Button_Delete;
            this.ux_Button_GVStringDel.Text = Resources.Button_Delete;
            this.ux_Button_GVStringModify.Text = Resources.Button_Delete;
            this.ux_Button_GVCounterReset.Text = Resources.Button_Reset;
            this.ux_Label_HeaderGlobalVariables.Text = Resources.ToolStripGlobalIDs;
        }

        /// <summary>
        /// Sets the translation for access tool strip.
        /// </summary>
        private void SetTranslationForAccessToolStrip()
        {
            // Access toolstrip.
            this.ux_ToolStrip_Access.Text = Resources.ToolStripAccess;
            this.ux_ToolStrip_Operator.Text = Resources.ToolStripOperator;
            this.ux_ToolStrip_Engineer.Text = Resources.ToolStripEngineer;
        }

        /// <summary>
        /// Sets the translation for theme settings.
        /// </summary>
        private void SetTranslationForThemeSettings()
        {
            // Settings: Theme
            this.ux_Set_BGColorLabel.Text = Resources.LabelBackground;
            this.ux_Set_FontLabel.Text = Resources.LabelFont;
            this.ux_Set_StatusColorLabel.Text = Resources.LabelStatusBar;
            this.ux_Label_Language.Text = Resources.Language;
            this.ux_Set_ToggleFullScreen.Text = Resources.Label_FullScreenMode;
            this.ux_Set_BGPicLabel.Text = Resources.Label_BackgroundPicture;
            this.ux_CheckBox_HideJobList.Text = Resources.Label_Hide_JobList;
        }

        /// <summary>
        /// Sets the translation for import settings.
        /// </summary>
        private void SetTranslationForImportSettings()
        {
            // Settings: Import
            this.ux_Import_PathSettingsLabel.Text = Resources.Label_ImportPathSettings;
            this.ux_Control_ImportPath.PathLabel = Resources.Label_ImportFileLocation;
            this.ux_Control_CompletePath.PathLabel = Resources.Label_ImportCompletedFileLocation;
            this.ux_Control_ErrorPath.PathLabel = Resources.Label_ImportErrorFIlePath;
            this.ux_Set_ImportConfigLabel.Text = Resources.Label_ImportConfiguration;
            this.ux_Import_FilePollRateLabel.Text = Resources.Label_ImportPollRate;
            this.ux_Import_ReadWaitLabel.Text = Resources.Label_ImportWaitToRead;
            this.ux_Import_ToggleJobLoadMode.Text = Resources.Label_ImportJobLoadMode;
            this.ux_Import_ToggleFileImport.Text = Resources.Label_ImportEnabled;
            this.ux_Import_JobLoadDesc.Text = Resources.Label_ImportToolTipJobLoadMode;
            this.ux_Import_ImportDesc.Text = Resources.Label_ImportToolTipRestartRequired;
        }

        /// <summary>
        /// Sets the translation for axis.
        /// </summary>
        private void SetTranslationForAxis()
        {
            // Settings: Axis
            this.ux_CheckBox_AxisZEnabled.Text = Resources.Label_AxisIsUsed;
            this.ux_CheckBox_AxisREnabled.Text = Resources.Label_AxisIsUsed;
            this.ux_Label_ZHomeAxisEveryN.Text = Resources.Label_AxisHomeFrequency;
            this.ux_Label_RHomeAxisEveryN.Text = Resources.Label_AxisHomeFrequency;
            this.ux_Check_HomeAxisOnStartup.Text = Resources.Label_Axis_Home_On_Startup;
        }

        /// <summary>
        /// Sets the translation.
        /// </summary>
        private void SetTranslation()
        {
            this.SetTranslationForScannerControls();
            this.SetTranslationForToolStrip();
            this.SetTranslationForMarkInProgress();
            this.SetTranslationForSettings();
            this.SetTranslationForGeneralSettings();
            this.SetTranslationForSettingsScreen();
            this.SetTranslationForGlobalVariables();
            this.SetTranslationForAccessToolStrip();
            this.SetTranslationForThemeSettings();
            this.SetTranslationForImportSettings();
            this.SetTranslationForAxis();
        }
    }
}
