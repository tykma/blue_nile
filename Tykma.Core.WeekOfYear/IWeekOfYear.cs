﻿// <copyright file="IWeekOfYear.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.WeekOfYear
{
    using System;

    /// <summary>
    /// Week of year interface.
    /// </summary>
    public interface IWeekOfYear
    {
        /// <summary>
        /// Get the week of the year.
        /// </summary>
        /// <param name="time">The date.</param>
        /// <returns>Week of the year.</returns>
        int GetWeek(DateTime time);
    }
}
