﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tykma.Custom.BlueNile.DBLogic
{
    public class BlueNileDataObject
    {
        public string JobNumber { get; }

        public string EngraveText { get; }

        public double EngravingAreaHeight { get; }

        public string EngraveTemplate { get; }

        public string MetalType { get; }

        public string FontName { get; }

        public int EngraveMaxCharacters { get; }

        public bool IsFontInside { get; }

        public string SKU { get; }

        public string FontStyle { get; }

        public string Template { get; }
        public double FontDiameter { get; }

        public BlueNileDataObject(
            string jobnum,
            string eng_text,
            double eng_area_height,
            string eng_template,
            string metal_type,
            int eng_max_chars,
            bool is_font_inside,
            string sku,
            string font_style,
            double font_diam,
            string fontName,
            string template)
        {
            this.JobNumber = jobnum;
            this.EngraveText = eng_text;
            this.EngravingAreaHeight = eng_area_height;
            this.EngraveTemplate = eng_template;
            this.MetalType = metal_type;
            this.EngraveMaxCharacters = eng_max_chars;
            this.IsFontInside = is_font_inside;
            this.SKU = sku;
            this.FontStyle = font_style;
            this.FontDiameter = font_diam;
            this.FontName = fontName;
            this.Template = template;
        }
    }
}
