﻿// <copyright file="GeometryHelpers.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.SplitMark
{
    using System;

    /// <summary>
    /// Geometry methods.
    /// </summary>
    public static class GeometryHelpers
    {
        /// <summary>
        /// Gets the central angle given the arc length and the part radius.
        /// </summary>
        /// <param name="arclength">The arc length.</param>
        /// <param name="radius">The part radius.</param>
        /// <returns>Central angle.</returns>
        public static double GetCentralAngle(double arclength, double radius)
        {
            return (arclength * 360) / (2 * Math.PI * radius);
        }

        /// <summary>
        /// Get the distance between two points.
        /// </summary>
        /// <param name="x1">Min X position.</param>
        /// <param name="y1">Min Y position.</param>
        /// <param name="x2">Max X position.</param>
        /// <param name="y2">Max Y position.</param>
        /// <returns>Distance between the points.</returns>
        public static double GetDistance(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
        }
    }
}
