﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdParamSetHatch.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;
    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// Command to set the hatching/fill settings.
    /// </summary>
    internal class CmdParamSetEntHatch : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdParamSetHatch" /> class.
        /// </summary>
        /// <param name="lmcApi">The LMC API.</param>
        /// <param name="contour">if set to <c>true</c> [contour].</param>
        /// <param name="hatch1">The hatch1.</param>
        /// <param name="hatch2">The hatch2.</param>
        public CmdParamSetEntHatch(IntPtr lmcApi, HatchEntAttributes attrib, string entName, int index)
            : base(lmcApi)
        {
            this.HatchEntAttributes = attrib;
            this.EntName = entName;
            this.Index = index;
        }

        [DllImport("MarkPSE", EntryPoint = "lmc1_SetHatchEntParam2", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern int
            SetHatchEntParam2(string HatchName,
                bool bEnableContour,
                int nParamIndex,
                int bEnableHatch,
                bool bContourFirst,
                int nPenNo,
                int nHatchType,
                bool bHatchAllCalc,
                bool bHatchEdge,
                bool bHatchAverageLine,
                double dHatchAngle,
                double dHatchLineDist,
                double dHatchEdgeDist,
                double dHatchStartOffset,
                double dHatchEndOffset,
                double dHatchLineReduction,//直线缩进
                double dHatchLoopDist,//环间距
                int nEdgeLoop,//环数
                bool nHatchLoopRev,//环形反转
                bool bHatchAutoRotate,//是否自动旋转角度
                double dHatchRotateAngle,
                bool bHatchCrossMode,
                int dCycCount);       

        /// <summary>
        /// Gets  the first hatch.
        /// </summary>
        /// <value>
        /// The hatch1.
        /// </value>
        private HatchEntAttributes HatchEntAttributes { get; }

        private string EntName { get; }

        private int Index { get; }
        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        internal bool Execute()
        {
            int result = SetHatchEntParam2(
                HatchName: this.EntName,
                bEnableContour: this.HatchEntAttributes.EnableContour,
                nParamIndex: this.Index,
                bEnableHatch: this.HatchEntAttributes.EnableHatch,
                bContourFirst: this.HatchEntAttributes.ContourFirst,
                nPenNo: this.HatchEntAttributes.PenNumber,
                nHatchType: this.HatchEntAttributes.HatchType,
                bHatchAllCalc: this.HatchEntAttributes.AllCalc,
                bHatchEdge: this.HatchEntAttributes.HatchEdge,
                bHatchAverageLine: this.HatchEntAttributes.HatchAverageLine,
                dHatchAngle: this.HatchEntAttributes.HatchAngle,
                dHatchLineDist: this.HatchEntAttributes.HatchLineDist,
                dHatchEdgeDist: this.HatchEntAttributes.HatchEdgeDist,
                dHatchStartOffset: this.HatchEntAttributes.HatchStartOffset,
                dHatchEndOffset: this.HatchEntAttributes.HatchEndOffset,
                dHatchLineReduction: this.HatchEntAttributes.HatchLineReduction,
                dHatchLoopDist: this.HatchEntAttributes.HatchLoopDistance,
                nEdgeLoop: this.HatchEntAttributes.EdgeLoop,
                nHatchLoopRev: this.HatchEntAttributes.HatchLoopRev,
                bHatchAutoRotate: this.HatchEntAttributes.HatchAutoRotate,
                dHatchRotateAngle: this.HatchEntAttributes.HatchRotateAngle,
                bHatchCrossMode: this.HatchEntAttributes.HatchCrossMode,
                dCycCount: this.HatchEntAttributes.CycleCount);

            var rm = this.GetMiniLaseReturnCode(returnCode: result);

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(message: rm.ReturnDescription);
        }
    }
}
