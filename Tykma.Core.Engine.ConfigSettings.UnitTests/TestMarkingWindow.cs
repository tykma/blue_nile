﻿// <copyright file="TestMarkingWindow.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.Engine.ConfigSettings.UnitTests
{
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Marking window size tests.
    /// </summary>
    [TestClass]
    public class TestMarkingWindow
    {
        /// <summary>
        /// Basic test for retrieving marking window size.
        /// </summary>
        [TestMethod]
        public void TestBasicMarkingWindowSize()
        {
            IMarkingWindow status = new MarkingWindowFromConfig(new List<string> { "ACTIVEAXIS0=0", "FIELDSIZE=1.250000e+002" });
            Assert.AreEqual(125, status.Size.Width);
            Assert.AreEqual(125, status.Size.Height);
        }

        /// <summary>
        /// Tests the size of the marking window size with missing field size.
        /// </summary>
        [TestMethod]
        public void TestMarkingWindowSizeWithMissingFieldSize()
        {
            IMarkingWindow status = new MarkingWindowFromConfig(new List<string> { "ACTIVEAXIS0=0", "ABC=1.250000e+002" });
            Assert.AreEqual(0, status.Size.Width);
            Assert.AreEqual(0, status.Size.Height);
        }

        /// <summary>
        /// Tests the size of the marking window size with invalid field.
        /// </summary>
        [TestMethod]
        public void TestMarkingWindowSizeWithInvalidFieldSize()
        {
            IMarkingWindow status = new MarkingWindowFromConfig(new List<string> { "ACTIVEAXIS0=0", "FIELDSIZE=a" });
            Assert.AreEqual(0, status.Size.Width);
            Assert.AreEqual(0, status.Size.Height);
        }

        /// <summary>
        /// Tests the mark window size with empty configuration.
        /// </summary>
        [TestMethod]
        public void TestMarkWindowSizeWithEmptyConfig()
        {
            IMarkingWindow status = new MarkingWindowFromConfig(new List<string>());
            Assert.AreEqual(0, status.Size.Width);
            Assert.AreEqual(0, status.Size.Height);
        }

        /// <summary>
        /// Test setting the marking size.
        /// </summary>
        [TestMethod]
        public void TestSettingSize()
        {
            // Get a sample configuration.
            var cfg = ConfigHelper.GetSampleFile();

            // Create a parser with the  configuration.
            IMarkingWindow size = new MarkingWindowFromConfig(cfg);

            // Change the size and get the new config.
            var newconfig = size.SetSize(125);

            // Create a helper with the new config.
            var config = new MarkingWindowFromConfig(newconfig);

            Assert.AreEqual(125, config.Size.Width);
        }

        /// <summary>
        /// Test setting size when size option is not in the config.
        /// </summary>
        [TestMethod]
        public void TestSettingSizeWhenSizeOptionIsMissing()
        {
            IMarkingWindow status = new MarkingWindowFromConfig(new List<string>());
            var cfg = status.SetSize(100);
            Assert.AreEqual(0, cfg.Count);
        }

        /// <summary>
        /// Test retrieving offsets from config.
        /// </summary>
        [TestMethod]
        public void TestGettingOffsets()
        {
            // Get a sample configuration.
            var cfg = new MarkingWindowFromConfig(new List<string> { "FIELDOFFSETX=1.230000e+000", "FIELDOFFSETY=4.250000e+000" });

            Assert.AreEqual(1.23, cfg.OffsetX);
            Assert.AreEqual(4.25, cfg.OffsetY);
        }

        /// <summary>
        /// Test setting offsets.
        /// </summary>
        [TestMethod]
        public void TestSettingOffsets()
        {
            // Get a sample configuration.
            var cfg = ConfigHelper.GetSampleFile();

            // Create a parser with the  configuration.
            IMarkingWindow size = new MarkingWindowFromConfig(cfg);

            var results = size.SetOffset(1.23, 4.25);

            var newCfg = new MarkingWindowFromConfig(results);
            Assert.AreEqual(1.23, newCfg.OffsetX);
            Assert.AreEqual(4.25, newCfg.OffsetY);

            Assert.IsTrue(results.Contains("FIELDOFFSETX=1.230000e+000"), "X offset missing");
            Assert.IsTrue(results.Contains("FIELDOFFSETY=4.250000e+000"), "Y Offset missing");
        }

        /// <summary>
        /// Test getting the field angle.
        /// </summary>
        [TestMethod]
        public void TestGettingFieldAngle()
        {
            // Get a sample configuration.
            var cfg = ConfigHelper.GetSampleFile();

            // Create a parser with the  configuration.
            IMarkingWindow size = new MarkingWindowFromConfig(cfg);

            Assert.AreEqual(45, size.FieldAngle);
        }

        /// <summary>
        /// Test setting the field angle.
        /// </summary>
        [TestMethod]
        public void TestSettingFieldAngle()
        {
            // Get a sample configuration.
            var cfg = new MarkingWindowFromConfig(new List<string> { "FIELDANGLE=0.000000e+000" });

            var results = cfg.SetFieldAngle(45);

            Assert.IsTrue(results.Contains("FIELDANGLE=4.500000e+001"));
        }

        /// <summary>
        /// Test getting galvo 1 as X galvo.
        /// </summary>
        [TestMethod]
        public void TestGettingGalvo1AsX()
        {
            Assert.IsTrue(new MarkingWindowFromConfig(new List<string>() { "GALVOX=0" }).IsGalvo1X);
            Assert.IsFalse(new MarkingWindowFromConfig(new List<string>() { "GALVOX=1" }).IsGalvo1X);
        }

        /// <summary>
        /// Test setting the galvo 1 as the X galvo.
        /// </summary>
        [TestMethod]
        public void TestSettingGalvo1AsX()
        {
            // Get a sample configuration.
            var cfg = new MarkingWindowFromConfig(new List<string>() { "GALVOX=0" });

            Assert.IsTrue(cfg.SetGalvo1AsX(true).Contains("GALVOX=0"));
            Assert.IsTrue(cfg.SetGalvo1AsX(false).Contains("GALVOX=1"));
        }
    }
}
