﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IconScannerParserTest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.ScanParse
{
    using System.Collections.Generic;
    using System.Globalization;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Icon.ScanParse;

    /// <summary>
    /// Unit tests for the icon scanner parser.
    /// </summary>
    [TestClass]
    public class IconScannerParserTest
    {
        /// <summary>
        /// Tests the internal scan commands.
        /// </summary>
        [TestMethod]
        public void TestInternalScanCommands()
        {
            // ReSharper disable once UseObjectOrCollectionInitializer
            var kvp = new Dictionary<string, ScanCommand.InternalCommand>();
            kvp.Add("start", ScanCommand.InternalCommand.Start);
            kvp.Add("clear", ScanCommand.InternalCommand.Clear);
            kvp.Add("limits", ScanCommand.InternalCommand.Limits);
            kvp.Add("qty_up", ScanCommand.InternalCommand.QuantityUp);
            kvp.Add("qty_down", ScanCommand.InternalCommand.QuantityDown);
            kvp.Add("edit", ScanCommand.InternalCommand.Edit);
            kvp.Add("joblist", ScanCommand.InternalCommand.ShowJobList);
            kvp.Add("homez", ScanCommand.InternalCommand.HomeAxisZ);
            kvp.Add("homer", ScanCommand.InternalCommand.HomeAxisR);
            kvp.Add("debug", ScanCommand.InternalCommand.Debug);
            kvp.Add(string.Empty, ScanCommand.InternalCommand.None);
            kvp.Add("!", ScanCommand.InternalCommand.None);
            kvp.Add("FALSE_COMMAND", ScanCommand.InternalCommand.None);

            foreach (var pair in kvp)
            {
                this.TestACommand(pair.Key.ToLower(), pair.Value, true);
                this.TestACommand(pair.Key.ToLower(), pair.Value, false);
                this.TestACommand(pair.Key.ToUpper(), pair.Value, true);
                this.TestACommand(pair.Key.ToUpper(), pair.Value, false);
            }
        }

        /// <summary>
        /// Tests the basic job command.
        /// </summary>
        [TestMethod]
        public void TestBasicJobCommand()
        {
            var listOfJobs = new List<string>
                                 {
                                     "abc123",
                                     "ABC123",
                                     "JOB123",
                                     "zzzzzzz",
                                     string.Empty,
                                     "9123012301230120301203",
                                 };

            foreach (var job in listOfJobs)
            {
                var parsed = new ScannerParser().ParseCode(job, false, false, string.Empty, false, false);

                Assert.IsInstanceOfType(parsed, typeof(ScanJob));

                var jobcmd = (ScanJob)parsed;

                Assert.AreEqual(jobcmd.Data, job);
            }
        }

        /// <summary>
        /// Tests the explicit job command.
        /// </summary>
        [TestMethod]
        public void TestExplicitJobCommand()
        {
            var listOfJobs = new List<string>
                                 {
                                     "abc123",
                                     "ABC123",
                                     "JOB123",
                                     "zzzzzzz",
                                     string.Empty,
                                     "9123012301230120301203",
                                 };

            foreach (var job in listOfJobs)
            {
                foreach (var boolValue in new[] { true, false })
                {
                    var parsed = new ScannerParser().ParseCode($"job:{job}", boolValue, false, string.Empty, false, false);

                    Assert.IsInstanceOfType(parsed, typeof(ScanJob));

                    var jobcmd = (ScanJob)parsed;

                    Assert.AreEqual(jobcmd.Data, job);
                }
            }
        }

        /// <summary>
        /// Tests the quantity command.
        /// </summary>
        [TestMethod]
        public void TestQuantity()
        {
            // Key is the quantity passed, value is the quantity expected.
            var listofQuantities = new Dictionary<string, int>();
            for (int i = 0; i < 501; i++)
            {
                // Add 0-500
                listofQuantities.Add(i.ToString(CultureInfo.InvariantCulture), i);

                // Add 0-500 with a 0 prepended.
                listofQuantities.Add($"0{i}", i);

                // Add 0-500 with a 0 prepended.
                listofQuantities.Add($"00{i}", i);

                // Add 0-500 with a 0 prepended.
                listofQuantities.Add($"00000{i}", i);
            }

            // Even if we give a zero, we expect 1 back.
            listofQuantities["0"] = 1;
            listofQuantities["00"] = 1;
            listofQuantities["000"] = 1;
            listofQuantities["000000"] = 1;
            listofQuantities.Add("7.4", 1);
            listofQuantities.Add("7.6", 1);
            listofQuantities.Add("ZZZ", 1);
            listofQuantities.Add("########", 1);
            listofQuantities.Add(string.Empty, 1);

            foreach (var count in listofQuantities)
            {
                foreach (var boolValue in new[] { true, false })
                {
                    var parsed = new ScannerParser().ParseCode($"#{count.Key}", boolValue, false, string.Empty, false, false);

                    Assert.IsInstanceOfType(parsed, typeof(ScanQuantity));

                    var qtycmd = (ScanQuantity)parsed;

                    Assert.AreEqual(count.Value, qtycmd.Quantity);
                }
            }
        }

        /// <summary>
        /// Tests the SQL command.
        /// </summary>
        [TestMethod]
        public void TestSQLCommand()
        {
            // Key is the quantity passed, value is the quantity expected.
            var listofSQLEntries = new List<string>
                                       {
                                           "123",
                                           string.Empty,
                                           "ZZZZZZZZZZZZZZZZZZZZZZ",
                                           "<1231#@!@!<<@#!",
                                           "ðѓѨێ",
                                       };

            foreach (var sql in listofSQLEntries)
            {
                foreach (var boolValue in new[] { true, false })
                {
                    var parsed = new ScannerParser().ParseCode($"^{sql}", boolValue, false, string.Empty, false, false);

                    Assert.IsInstanceOfType(parsed, typeof(ScanSQL));

                    var qtycmd = (ScanSQL)parsed;

                    Assert.AreEqual(qtycmd.Data, sql);
                }
            }
        }

        /// <summary>
        /// Tests the SQL command.
        /// </summary>
        [TestMethod]
        public void TestSQLCommandNoPrefix()
        {
            // Key is the quantity passed, value is the quantity expected.
            var listofSQLEntries = new List<string>
                                       {
                                           "123",
                                           string.Empty,
                                           "ZZZZZZZZZZZZZZZZZZZZZZ",
                                           "<1231#@!@!<<@#!",
                                           "ðѓѨێ",
                                       };

            foreach (var sql in listofSQLEntries)
            {
                var parsed = new ScannerParser().ParseCode($"{sql}", false, false, string.Empty, false, true);

                Assert.IsInstanceOfType(parsed, typeof(ScanSQL));

                var qtycmd = (ScanSQL)parsed;

                Assert.AreEqual(qtycmd.Data, sql);
            }
        }

        /// <summary>
        /// Tests the set identifier command.
        /// </summary>
        [TestMethod]
        public void TestSetIDCommand()
        {
            var listofIdsToTest = new List<IdToTest>
                                      {
                                          new IdToTest { Name = "SERIAL_NUMBER", Value = "12345" },
                                          new IdToTest { Name = string.Empty, Value = string.Empty },
                                          new IdToTest { Name = "ABC123", Value = string.Empty },
                                          new IdToTest { Name = string.Empty, Value = "ABC123" },
                                          new IdToTest { Name = "MODEL_number", Value = "::::55555::123" },
                                      };

            foreach (var id in listofIdsToTest)
            {
                foreach (var boolValue in new[] { true, false })
                {
                    var parsed = new ScannerParser().ParseCode(
                        $"@{id.Name}:{id.Value}", boolValue, false, string.Empty, false, false);

                    Assert.IsInstanceOfType(parsed, typeof(ScanKeyValuePairs));

                    var kvpcmd = (ScanKeyValuePairs)parsed;

                    if (kvpcmd.IDDataValuePairs.Count == 0)
                    {
                        Assert.IsFalse(kvpcmd.DataPresent);
                    }

                    Assert.IsTrue(kvpcmd.IDDataValuePairs.Count <= 1, "Key value pair return: Count is larger than 1!");

                    if (kvpcmd.IDDataValuePairs.Count == 1)
                    {
                        Assert.IsTrue(kvpcmd.DataPresent);
                        Assert.AreEqual(kvpcmd.IDDataValuePairs[0].Name, id.ExpectedName);
                        Assert.AreEqual(kvpcmd.IDDataValuePairs[0].Value, id.ExpectedValue);
                    }
                }
            }
        }

        /// <summary>
        /// Tests the empty set identifier command.
        /// </summary>
        [TestMethod]
        public void TestEmptySetIDCommand()
        {
            foreach (var boolValue in new[] { true, false })
            {
                var parsed = new ScannerParser().ParseCode("@", boolValue, false, string.Empty, false, false);

                Assert.IsInstanceOfType(parsed, typeof(ScanKeyValuePairs));

                var kvpcmd = (ScanKeyValuePairs)parsed;

                Assert.IsFalse(kvpcmd.DataPresent);
            }
        }

        /// <summary>
        /// Tests the simple mark first identifier without prefix stripping.
        /// </summary>
        [TestMethod]
        public void TestSimpleMarkFirstIDWithoutPrefixStripping()
        {
            var listOfIDs = new List<string> { "*123*", "**", "****" };

            foreach (var item in listOfIDs)
            {
                var parsed = new ScannerParser().ParseCode(item, false, true, "*", false, false);
                Assert.IsInstanceOfType(parsed, typeof(ScanSimpleMark));
                var kvpcmd = (ScanSimpleMark)parsed;

                Assert.IsTrue(kvpcmd.FirstID);

                Assert.AreEqual(item, kvpcmd.Data);
            }
        }

        /// <summary>
        /// Tests the simple mark first identifier without prefix stripping.
        /// </summary>
        [TestMethod]
        public void TestSimpleMarkSecondID()
        {
            var listOfIDs = new List<string> { "123", string.Empty, "*" };

            foreach (var item in listOfIDs)
            {
                var parsed = new ScannerParser().ParseCode(item, false, true, "*", false, false);
                Assert.IsInstanceOfType(parsed, typeof(ScanSimpleMark));
                var kvpcmd = (ScanSimpleMark)parsed;

                Assert.IsFalse(kvpcmd.FirstID);

                Assert.AreEqual(item, kvpcmd.Data);
            }
        }

        /// <summary>
        /// Tests the simple mark first identifier with prefix stripping.
        /// </summary>
        [TestMethod]
        public void TestSimpleMarkFirstIDWithPrefixStripping()
        {
            var listOfIDs = new Dictionary<string, string>
                                {
                                    { "*123*", "123" },
                                    { "**", string.Empty },
                                    { "****", "**" },
                                };

            foreach (var item in listOfIDs)
            {
                var parsed = new ScannerParser().ParseCode(item.Key, false, true, "*", true, false);
                Assert.IsInstanceOfType(parsed, typeof(ScanSimpleMark));
                var kvpcmd = (ScanSimpleMark)parsed;

                Assert.IsTrue(kvpcmd.FirstID);

                Assert.AreEqual(item.Value, kvpcmd.Data);
            }
        }

        /// <summary>
        /// Tests the scanning a queue.
        /// </summary>
        [TestMethod]
        public void TestScanningAQueue()
        {
            var parsed = new ScannerParser().ParseCode("item1", true, false, string.Empty, false, false);
            Assert.IsInstanceOfType(parsed, typeof(ScanQueueInfo));

            var quecmd = (ScanQueueInfo)parsed;

            Assert.AreEqual("item1", quecmd.Data);
        }

        /// <summary>
        /// Tests the scanning a queue with quantity.
        /// </summary>
        [TestMethod]
        public void TestScanningAQueueWithQuantity()
        {
            var parsed = new ScannerParser().ParseCode("item1|55", true, false, string.Empty, false, false);
            Assert.IsInstanceOfType(parsed, typeof(ScanQueueInfo));

            var quecmd = (ScanQueueInfo)parsed;

            Assert.AreEqual("item1", quecmd.Data);
            Assert.AreEqual(55, quecmd.Quantity);
        }

        /// <summary>
        /// Tests a command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="expectedCommand">The expected command.</param>
        /// <param name="queueEnabled">if set to <c>true</c> [queue_enabled].</param>
        private void TestACommand(string command, ScanCommand.InternalCommand expectedCommand, bool queueEnabled)
        {
            // Create an instance of the scanner.
            var scanner = new ScannerParser();

            // Let's test the command.
            var returnedObject = scanner.ParseCode($"!{command}", queueEnabled, false, string.Empty, false, false);

            // Make sure what we get back is a scancommand.
            Assert.IsInstanceOfType(returnedObject, typeof(ScanCommand));

            // Make sure we got the right enum back.
            var cmd = (ScanCommand)returnedObject;
            Assert.AreEqual(cmd.Command, expectedCommand);
        }
    }
}
