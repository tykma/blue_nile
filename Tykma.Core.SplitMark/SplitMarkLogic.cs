﻿// <copyright file="SplitMarkLogic.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.SplitMark
{
    using System;
    using System.Collections.Generic;

    using Tykma.Core.Entity;

    /// <summary>
    /// Split marking logic.
    /// </summary>
    public class SplitMarkLogic
    {
        /// <summary>
        /// Get the list of splitted objects.
        /// </summary>
        /// <param name="splitSize">The size to split the objects by.</param>
        /// <param name="diameter">The part diameters.</param>
        /// <param name="entities">The list of entities.</param>
        /// <returns>A list of split objects.</returns>
        public IList<SplitObject> GetSplitObjects(double splitSize, double diameter, IList<EntityInformation> entities)
        {
            // Create the list of split objects.
            var l = new List<SplitObject>();

            // Let's get the total dimensions of our entities.
            var boundary = new DimensionMath().GetBoundariesOfObjects(entities);

            // Split the entities into boxes.
            var boxes = new DimensionMath().SplitIntoBoxes(splitSize, boundary);

            double maxangle = 0;
            double angle = 0;

            foreach (var item in boxes)
            {
                var w = GeometryHelpers.GetDistance(item.X1, 0, item.X2, 0);
                var centralAngle = GeometryHelpers.GetCentralAngle(w, diameter / 2);

                if (centralAngle > maxangle)
                {
                    maxangle = centralAngle;
                }

                if (centralAngle < maxangle)
                {
                    centralAngle = (maxangle / 2) + (centralAngle / 2);
                }

                if (l.Count == 0)
                {
                    centralAngle = 0;
                }

                angle += centralAngle;

                l.Add(new SplitObject(angle, item));
            }

            return l;
        }
    }
}
