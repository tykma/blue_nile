﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputParseTest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.IO
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Icon.InputParse;

    /// <summary>
    /// The input parse test.
    /// </summary>
    [TestClass]
    public class InputParseTest
    {
        /// <summary>
        /// Tests the input setting.
        /// </summary>
        [TestMethod]
        public void TestInputSetting()
        {
            var iopparse = new InputParser(
                startInput: 0,
                stopInput: 2,
                shutterOpen: 5,
                invertinputs: false);

            Assert.AreEqual(InputMask.IOMasks.Input0, iopparse.StartInput);
            Assert.AreEqual(InputMask.IOMasks.Input2, iopparse.StopInput);
            Assert.AreEqual(InputMask.IOMasks.Input5, iopparse.ShutterInput);
        }

        /// <summary>
        /// Tests the invalid inputs.
        /// </summary>
        [TestMethod]
        public void TestInvalidInputs()
        {
            var iopparse = new InputParser(
                startInput: 17,
                stopInput: -55,
                shutterOpen: 10000,
                invertinputs: false);

            Assert.AreEqual(InputMask.IOMasks.Input0, iopparse.StartInput);
            Assert.AreEqual(InputMask.IOMasks.Input0, iopparse.StopInput);
            Assert.AreEqual(InputMask.IOMasks.Input0, iopparse.ShutterInput);
        }

        /// <summary>
        /// Tests the start input non inverted.
        /// </summary>
        [TestMethod]
        public void TestStartInputNonInverted()
        {
            var iopparse = new InputParser(
                startInput: 0,
                stopInput: 2,
                shutterOpen: 5,
                invertinputs: false);

            var commands = iopparse.GetInputType(1);

            Assert.AreEqual(1, commands.Count);

            Assert.IsTrue(commands.Contains(InputCommand.InputType.Start));
        }

        /// <summary>
        /// Tests the start input inverted.
        /// </summary>
        [TestMethod]
        public void TestStartInputInverted()
        {
            var iopparse = new InputParser(
                startInput: 0,
                stopInput: 2,
                shutterOpen: 5,
                invertinputs: true);

            var commands = iopparse.GetInputType(65534);

            Assert.AreEqual(1, commands.Count);

            Assert.IsTrue(commands.Contains(InputCommand.InputType.Start));
        }

        /// <summary>
        /// Tests the start input inverted.
        /// </summary>
        [TestMethod]
        public void TestStartInputInvertedWithZ()
        {
            var iopparse = new InputParser(
                startInput: 4,
                stopInput: 1,
                shutterOpen: 1,
                invertinputs: true);

            var commands = iopparse.GetInputType(15599);

            Assert.AreEqual(1, commands.Count);

            Assert.IsTrue(commands.Contains(InputCommand.InputType.Start));

            var commands2 = iopparse.GetInputType(48367);

            Assert.AreEqual(1, commands2.Count);

            Assert.IsTrue(commands2.Contains(InputCommand.InputType.Start));
        }

        /// <summary>
        /// Tests the stop input non inverted.
        /// </summary>
        [TestMethod]
        public void TestStopInputNonInverted()
        {
            var iopparse = new InputParser(
                startInput: 0,
                stopInput: 2,
                shutterOpen: 5,
                invertinputs: false);

            var commands = iopparse.GetInputType(4);

            Assert.AreEqual(1, commands.Count);

            Assert.IsTrue(commands.Contains(InputCommand.InputType.Stop));
        }

        /// <summary>
        /// Tests the stop input inverted.
        /// </summary>
        [TestMethod]
        public void TestStopInputInverted()
        {
            var iopparse = new InputParser(
                startInput: 0,
                stopInput: 2,
                shutterOpen: 5,
                invertinputs: true);

            var commands = iopparse.GetInputType(65531);

            Assert.AreEqual(1, commands.Count);

            Assert.IsTrue(commands.Contains(InputCommand.InputType.Stop));
        }

        /// <summary>
        /// Tests the shutter state input non inverted.
        /// </summary>
        [TestMethod]
        public void TestShutterStateInverted()
        {
            var iopparse = new InputParser(
                startInput: 0,
                stopInput: 2,
                shutterOpen: 15,
                invertinputs: false);

            var commands = iopparse.GetInputType(32768);

            Assert.AreEqual(1, commands.Count);

            Assert.IsTrue(commands.Contains(InputCommand.InputType.ShutterState));
        }

        /// <summary>
        /// Tests the shutter state input inverted.
        /// </summary>
        [TestMethod]
        public void TestShutterStateNonInverted()
        {
            var iopparse = new InputParser(
                startInput: 0,
                stopInput: 2,
                shutterOpen: 15,
                invertinputs: true);

            var commands = iopparse.GetInputType(32767);

            Assert.AreEqual(1, commands.Count);

            Assert.IsTrue(commands.Contains(InputCommand.InputType.ShutterState));
        }

        /// <summary>
        /// Tests all inputs non inverted.
        /// </summary>
        [TestMethod]
        public void TestAllInputsNonInverted()
        {
            var iopparse = new InputParser(
         startInput: 0,
         stopInput: 2,
         shutterOpen: 15,
         invertinputs: false);

            var commands = iopparse.GetInputType(32797);

            Assert.AreEqual(3, commands.Count);

            Assert.IsTrue(commands.Contains(InputCommand.InputType.ShutterState));
            Assert.IsTrue(commands.Contains(InputCommand.InputType.Start));
            Assert.IsTrue(commands.Contains(InputCommand.InputType.Stop));
        }

        /// <summary>
        /// Tests all inputs non inverted.
        /// </summary>
        [TestMethod]
        public void TestAllInputsInverted()
        {
            var iopparse = new InputParser(
         startInput: 0,
         stopInput: 2,
         shutterOpen: 15,
         invertinputs: true);

            var commands = iopparse.GetInputType(32738);

            Assert.AreEqual(3, commands.Count);

            Assert.IsTrue(commands.Contains(InputCommand.InputType.ShutterState));
            Assert.IsTrue(commands.Contains(InputCommand.InputType.Start));
            Assert.IsTrue(commands.Contains(InputCommand.InputType.Stop));
        }

        /// <summary>
        /// Tests all inputs non inverted.
        /// </summary>
        [TestMethod]
        public void TestAllButOneInputs()
        {
            var iopparse = new InputParser(
         startInput: 0,
         stopInput: 2,
         shutterOpen: 15,
         invertinputs: false);

            var commands = iopparse.GetInputType(32797);

            Assert.AreEqual(3, commands.Count);

            Assert.IsTrue(commands.Contains(InputCommand.InputType.ShutterState));
            Assert.IsTrue(commands.Contains(InputCommand.InputType.Start));
            Assert.IsTrue(commands.Contains(InputCommand.InputType.Stop));
        }

        /// <summary>
        /// Checks for phantom inputs.
        /// </summary>
        [TestMethod]
        public void CheckForPhantomInputsInverted()
        {
            var iopparse = new InputParser(
         startInput: 0,
         stopInput: 2,
         shutterOpen: 15,
         invertinputs: true);

            var commands = iopparse.GetInputType(32738);

            Assert.AreEqual(3, commands.Count);

            Assert.IsTrue(commands.Contains(InputCommand.InputType.ShutterState));
            Assert.IsTrue(commands.Contains(InputCommand.InputType.Start));
            Assert.IsTrue(commands.Contains(InputCommand.InputType.Stop));
        }

        /// <summary>
        /// Tests all inputs non inverted.
        /// </summary>
        [TestMethod]
        public void TestPhantomAllInputsNonInverted()
        {
            var iopparse = new InputParser(
         startInput: 0,
         stopInput: 2,
         shutterOpen: 15,
         invertinputs: false);

            var commands = iopparse.GetInputType(32797);

            Assert.AreEqual(3, commands.Count);

            Assert.IsTrue(commands.Contains(InputCommand.InputType.ShutterState));
            Assert.IsTrue(commands.Contains(InputCommand.InputType.Start));
            Assert.IsTrue(commands.Contains(InputCommand.InputType.Stop));
        }

        /// <summary>
        /// Tests the is input on all.
        /// </summary>
        [TestMethod]
        public void TestIsInputOnAll()
        {
            var iopparse = new InputParser(
        startInput: 0,
        stopInput: 2,
        shutterOpen: 15,
        invertinputs: false);

            const int Word = 65535;
            Assert.IsTrue(iopparse.IsInputOn(0, Word));
            Assert.IsTrue(iopparse.IsInputOn(1, Word));
            Assert.IsTrue(iopparse.IsInputOn(2, Word));
            Assert.IsTrue(iopparse.IsInputOn(3, Word));
            Assert.IsTrue(iopparse.IsInputOn(4, Word));
            Assert.IsTrue(iopparse.IsInputOn(5, Word));
            Assert.IsTrue(iopparse.IsInputOn(6, Word));
            Assert.IsTrue(iopparse.IsInputOn(7, Word));
            Assert.IsTrue(iopparse.IsInputOn(8, Word));
            Assert.IsTrue(iopparse.IsInputOn(9, Word));
            Assert.IsTrue(iopparse.IsInputOn(10, Word));
            Assert.IsTrue(iopparse.IsInputOn(11, Word));
            Assert.IsTrue(iopparse.IsInputOn(12, Word));
            Assert.IsTrue(iopparse.IsInputOn(13, Word));
            Assert.IsTrue(iopparse.IsInputOn(14, Word));
            Assert.IsTrue(iopparse.IsInputOn(15, Word));
        }

        /// <summary>
        /// Tests the is input on all except one.
        /// </summary>
        [TestMethod]
        public void TestIsInputOnAllExceptOne()
        {
            var iopparse = new InputParser(
        startInput: 0,
        stopInput: 2,
        shutterOpen: 15,
        invertinputs: false);

            const int Word = 65407;
            Assert.IsTrue(iopparse.IsInputOn(0, Word));
            Assert.IsTrue(iopparse.IsInputOn(1, Word));
            Assert.IsTrue(iopparse.IsInputOn(2, Word));
            Assert.IsTrue(iopparse.IsInputOn(3, Word));
            Assert.IsTrue(iopparse.IsInputOn(4, Word));
            Assert.IsTrue(iopparse.IsInputOn(5, Word));
            Assert.IsTrue(iopparse.IsInputOn(6, Word));
            Assert.IsFalse(iopparse.IsInputOn(7, Word));
            Assert.IsTrue(iopparse.IsInputOn(8, Word));
            Assert.IsTrue(iopparse.IsInputOn(9, Word));
            Assert.IsTrue(iopparse.IsInputOn(10, Word));
            Assert.IsTrue(iopparse.IsInputOn(11, Word));
            Assert.IsTrue(iopparse.IsInputOn(12, Word));
            Assert.IsTrue(iopparse.IsInputOn(13, Word));
            Assert.IsTrue(iopparse.IsInputOn(14, Word));
            Assert.IsTrue(iopparse.IsInputOn(15, Word));
        }

        /// <summary>
        /// Tests the non existing input.
        /// </summary>
        [TestMethod]
        public void TestNonExistingInput()
        {
            var iopparse = new InputParser(
       startInput: 0,
       stopInput: 2,
       shutterOpen: 15,
       invertinputs: false);

            Assert.IsFalse(iopparse.IsInputOn(25, 0));
        }

        /// <summary>
        /// Tests the is input on all except one.
        /// </summary>
        [TestMethod]
        public void TestIsInputAllOff()
        {
            var iopparse = new InputParser(
        startInput: 0,
        stopInput: 2,
        shutterOpen: 15,
        invertinputs: false);

            const int Word = 0;
            Assert.IsFalse(iopparse.IsInputOn(0, Word));
            Assert.IsFalse(iopparse.IsInputOn(1, Word));
            Assert.IsFalse(iopparse.IsInputOn(2, Word));
            Assert.IsFalse(iopparse.IsInputOn(3, Word));
            Assert.IsFalse(iopparse.IsInputOn(4, Word));
            Assert.IsFalse(iopparse.IsInputOn(5, Word));
            Assert.IsFalse(iopparse.IsInputOn(6, Word));
            Assert.IsFalse(iopparse.IsInputOn(7, Word));
            Assert.IsFalse(iopparse.IsInputOn(8, Word));
            Assert.IsFalse(iopparse.IsInputOn(9, Word));
            Assert.IsFalse(iopparse.IsInputOn(10, Word));
            Assert.IsFalse(iopparse.IsInputOn(11, Word));
            Assert.IsFalse(iopparse.IsInputOn(12, Word));
            Assert.IsFalse(iopparse.IsInputOn(13, Word));
            Assert.IsFalse(iopparse.IsInputOn(14, Word));
            Assert.IsFalse(iopparse.IsInputOn(15, Word));
        }
    }
}
