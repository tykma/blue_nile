﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// Entry point for the software.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface
{
    using System;
    using System.Configuration;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.IO;
    using System.Threading;
    using System.Windows.Forms;
    using Icon_Interface.Forms;
    using Icon_Interface.General;
    using Icon_Interface.Properties;
    using Microsoft.Win32;

    /// <summary>
    /// The class which runs the icon interface.
    /// </summary>
    internal static class Program
    {
        /// <summary>
        /// The singleton for running application in a single instance.
        /// </summary>
        private static readonly Mutex Singleton = new Mutex(true, "Icon Interface");

        /// <summary>
        /// Handles the SessionEnding event of the SystemEvents control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SessionEndingEventArgs"/> instance containing the event data.</param>
        private static void SystemEventsSessionEnding(object sender, SessionEndingEventArgs e)
        {
            switch (e.Reason)
            {
                case SessionEndReasons.Logoff:
                    Application.Exit();
                    break;

                case SessionEndReasons.SystemShutdown:
                    Application.Exit();
                    break;
            }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        [SuppressMessage("ReSharper", "LocalizableElement", Justification="Can't move strings to resources, as we're trying to check for corrupt resources")]
        private static void Main()
        {
            var settingChecker = new SettingsCheck();

            settingChecker.CheckSettings();

            try
            {
                // ReSharper disable once UnusedVariable
                var cult = Settings.Default.Culture;
            }
            catch (ConfigurationErrorsException)
            {
                File.Delete(
                    ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal)
                        .FilePath);
                MessageBox.Show(
                    "Settings reinitialized. Restart the application to continue.",
                    "Settings reinitialized",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return;
            }

            if (Settings.Default.Culture != null)
            {
                Thread.CurrentThread.CurrentCulture = Settings.Default.Culture;
                Thread.CurrentThread.CurrentUICulture = Settings.Default.Culture;
                CultureInfo.DefaultThreadCurrentCulture = Settings.Default.Culture;
                CultureInfo.DefaultThreadCurrentUICulture = Settings.Default.Culture;
            }

            SystemEvents.SessionEnding += SystemEventsSessionEnding;

            if (!Singleton.WaitOne(TimeSpan.Zero, true))
            {
                // There is already another instance running!
                MessageBox.Show(
                    Resources.MessageBox_Instance_Running,
                    Resources.MessageBox_Instance_Error,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Application.Exit();
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Main());
            }
        }
    }
}