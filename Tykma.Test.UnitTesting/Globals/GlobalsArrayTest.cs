﻿// <copyright file="GlobalsArrayTest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Test.UnitTesting.Globals
{
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Tykma.Icon.Globals;

    /// <summary>
    /// Test array serial incrementing.
    /// </summary>
    [TestClass]
    public class GlobalsArrayTest
    {
        /// <summary>
        /// Array serialization increment test.
        /// </summary>
        [TestMethod]
        public void ArraySerializationIncrementTest()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("SERIAL", "0001", EGlobalTypes.GlobalVariableType.Counter_Integer);

            gv.IncrementUsedArrayCounters(new List<string> { "SERIAL", "SERIAL", "SERIAL" });

            Assert.AreEqual("0004", gv.GetGlobalVariable("SERIAL").Value);
        }

        /// <summary>
        /// Array serialization increment test.
        /// </summary>
        [TestMethod]
        public void ArraySerializationIncrementTestNonCounterValues()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("SERIAL", "0001", EGlobalTypes.GlobalVariableType.String);

            gv.IncrementUsedArrayCounters(new List<string> { "SERIAL", "SERIAL", "SERIAL" });

            Assert.AreEqual("0001", gv.GetGlobalVariable("SERIAL").Value);
        }
    }
}
