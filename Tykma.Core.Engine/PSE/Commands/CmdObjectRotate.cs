﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdObjectRotate.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// The command to rotate an object.
    /// </summary>
    internal class CmdObjectRotate : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdObjectRotate" /> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking API.</param>
        /// <param name="objectToRotate">The object to rotate.</param>
        /// <param name="centerX">The center x.</param>
        /// <param name="centerY">The center y.</param>
        /// <param name="rotateAngle">The rotate angle.</param>
        public CmdObjectRotate(IntPtr lmcApi, string objectToRotate, double centerX, double centerY, double rotateAngle)
            : base(lmcApi)
        {
            this.ObjectToRotate = objectToRotate;
            this.CenterX = centerX;
            this.CenterY = centerY;
            this.RotationAngle = rotateAngle;
        }

        /// <summary>
        /// Rotate a given entity.
        /// </summary>
        /// <param name="pEntName">Name of the entity.</param>
        /// <param name="dCex">The new x center.</param>
        /// <param name="dCeny">The new y center..</param>
        /// <param name="dAngle">The rotation angle.</param>
        /// <returns>>Return code.</returns>
        private delegate int LMC1ApiRotateEnt(
                                                [MarshalAs(UnmanagedType.LPWStr)] string pEntName,
                                                double dCex,
                                                double dCeny,
                                                double dAngle);

        /// <summary>
        /// Gets the object to rotate.
        /// </summary>
        /// <value>
        /// The object to rotate.
        /// </value>
        private string ObjectToRotate { get; }

        /// <summary>
        /// Gets the center x position.
        /// </summary>
        /// <value>
        /// The center x.
        /// </value>
        private double CenterX { get; }

        /// <summary>
        /// Gets the center y position.
        /// </summary>
        /// <value>
        /// The center y position.
        /// </value>
        private double CenterY { get; }

        /// <summary>
        /// Gets the rotation angle.
        /// </summary>
        /// <value>
        /// The rotation angle.
        /// </value>
        private double RotationAngle { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        /// <exception cref="Tykma.Core.Engine.CustomExceptions.MethodFaultException">Problem with command.</exception>
        internal bool Execute()
        {
            var lmc1RotateReturn = this.LoadFunction<LMC1ApiRotateEnt>("lmc1_RotateEnt");

            if (lmc1RotateReturn == null)
            {
                throw new EngineNotConnectedException();
            }

            ReturnCodeMessage rm = this.GetMiniLaseReturnCode(lmc1RotateReturn(this.ObjectToRotate, this.CenterX, this.CenterY, this.RotationAngle));

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
