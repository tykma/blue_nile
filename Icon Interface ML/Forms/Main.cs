﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// Main UI class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Icon_Interface.Custom_Controls;
    using Icon_Interface.File_Management;
    using Icon_Interface.General;
    using Icon_Interface.Properties;
    using Tykma.Core.Engine;
    using Tykma.Core.Entity;
    using Tykma.Icon.Globals;
    using Tykma.Icon.ImageBranding;
    using Tykma.Icon.MarkingJob;
    using Tykma.Icon.RotaryMark.Logic;
    using Tykma.Interface.NG.DialogService;

    /// <summary>
    /// The main form class.
    /// </summary>
    public partial class Main : Form
    {
        /// <summary>
        /// The image brands class.
        /// </summary>
        private readonly ImageBranding imageBrands;

        /// <summary>
        /// The file operations class.
        /// </summary>
        private readonly Files fileOperations;

        /// <summary>
        /// The marking job state.
        /// </summary>
        private readonly MarkingJob markingJobState = new MarkingJob();

        /// <summary>
        /// This will show us which index we are on on the job list sub-list.
        /// </summary>
        private int selectedJobListIndex;

        /// <summary>
        /// Initializes a new instance of the <see cref="Main" /> class.
        /// </summary>
        public Main()
        {
            try
            {
                var settings = TykmaGlobals.INISettings.SETTINGSFOLDER;
            }
            catch (Exception err)
            {
                MessageBox.Show($"Settings Initialization Fault\r\n{err.InnerException}", "Fault", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }

            try
            {
                var sqlsettings = TykmaGlobals.SQLSettings.DatabaseType;
            }
            catch (Exception err)
            {
                MessageBox.Show($"SQL Settings Initialization Fault\r\n{err.InnerException}", "Fault", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }

            try
            {
                var plcsettings = TykmaGlobals.PLCSettings.PLCENABLED;
            }
            catch (Exception err)
            {
                MessageBox.Show($"PLC Settings Initialization Fault\r\n{err.InnerException}", "Fault", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }


            // Initialize custom images.
            this.imageBrands = new ImageBranding(
                                    TykmaGlobals.INISettings.SETTINGSFOLDER,
                                    TykmaGlobals.INISettings.INSTRUCTIONSRTFFOLDER,
                                    TykmaGlobals.INISettings.INSTRUCTIONSIMAGEFOLDER,
                                    Settings.Default.BackColor);

            this.fileOperations = new Files(this.imageBrands);

            this.ParseCommandLineArguments();

            // Initialize the marking processor.
            this.InitializeEngine();

            // Initialize component.
            this.InitializeComponent();

            // Set up the log view window.
            this.InitializeLoggingView();

            // Translate UX.
            this.SetTranslation();

            // Initialize the instruction form.
            this.InitializeInstructionForm();

            // Initialize file import searcher.
            this.InitializeDataImporter();

            // Initialize the PLC.
            this.InitializePLC();

            // Initialize the global variables.
            this.InitializeGlobalVariables();

            // Initialzie the SQL client.
            this.InitializeSQLClient();

            this.InitializeBlueNile();

            // Initialize authorization class.
            this.authorization = new Auth();

            // Configure our external I/O.
            this.InitializeExtIO();

            // Set up the dynamic graphic importer.
            this.InitializeDynamicImporter();

            this.InitializePropertyGrid();

            this.ArrayHelperClass = new Tykma.Icon.ArrayMarkingHelpers.ArrayHelper();

            // Set up our cycle timer.
            this.cycleTimer = new Stopwatch();

            TykmaGlobals.SetDoubleBuffered(this.ux_EventLog);
        }



        /// <summary>
        /// Gets or sets the rotary split marking logic.
        /// </summary>
        private RotaryMark RotaryMarkLogic { get; set; }

        /// <summary>
        /// Gets the array helper class.
        /// </summary>
        private Tykma.Icon.ArrayMarkingHelpers.ArrayHelper ArrayHelperClass { get; }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }

            if (this.gv != null)
            {
                this.gv.Dispose();
            }

            if (this.httpd != null)
            {
                this.httpd.Dispose();
            }

            if (this.cmdFile != null)
            {
                this.cmdFile.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Sets all form colors.
        /// </summary>
        private void SetAllColors()
        {
            this.ux_Layout_Main.BackColor = Settings.Default.BackColor;
            this.BackColor = Settings.Default.BackColor;
            this.ForeColor = Settings.Default.ForeColor;
            this.ux_StatusStrip.BackColor = Settings.Default.StatusColor;
            this.ux_Control_Quantity.ux_Counter_Label.ForeColor = Settings.Default.ForeColor;

            foreach (TabPage tabPage in this.ux_HMI.TabPages)
            {
                tabPage.BackColor = Color.Transparent;
            }

            if (!Settings.Default.UseBgImage)
            {
                this.BackgroundImage = null;
            }
        }

        /// <summary>
        /// Sets the color of the background.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void SetBgColor(object sender, EventArgs e)
        {
            var cntrl = (Control)sender;
            Settings.Default.BackColor = cntrl.BackColor;
            Settings.Default.UseBgImage = false;

            this.SetAllColors();
        }

        /// <summary>
        /// Sets the color of the background.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void SetFgColor(object sender, EventArgs e)
        {
            var cntrl = (Control)sender;
            Settings.Default.ForeColor = cntrl.BackColor;
            this.SetAllColors();
        }

        /// <summary>
        /// Sets the color of the status bar.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void SetStatusBarColor(object sender, EventArgs e)
        {
            var cntrl = (Control)sender;
            Settings.Default.StatusColor = cntrl.BackColor;
            this.SetAllColors();
        }

        /// <summary>
        /// Choose the background color.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void ColorChooserBg(object sender, EventArgs e)
        {
            var myDialog = new ColorDialog { ShowHelp = true };

            if (myDialog.ShowDialog() == DialogResult.OK)
            {
                this.ux_Set_BGCustom.BackColor = myDialog.Color;
                this.SetBgColor(this.ux_Set_BGCustom, e);
            }
        }

        /// <summary>
        /// Choose the foreground color.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void ColorChooserFg(object sender, EventArgs e)
        {
            var myDialog = new ColorDialog { ShowHelp = true };

            if (myDialog.ShowDialog() == DialogResult.OK)
            {
                this.ux_Set_FontCustom.BackColor = myDialog.Color;
                this.SetFgColor(this.ux_Set_FontCustom, e);
            }
        }

        /// <summary>
        /// Choose the Status color.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void ColorChooserStatus(object sender, EventArgs e)
        {
            var myDialog = new ColorDialog { ShowHelp = true };

            if (myDialog.ShowDialog() == DialogResult.OK)
            {
                this.ux_Set_StatusCustom.BackColor = myDialog.Color;
                this.SetStatusBarColor(this.ux_Set_StatusCustom, e);
            }
        }

        /// <summary>
        /// Sets the background image.
        /// </summary>
        private void SetBackgroundImage()
        {
            // Do not conntinue if the chosen bitmap is non-existent.
            if (!File.Exists(Settings.Default.BGImage))
            {
                return;
            }

            this.ux_Layout_Main.BackColor = Color.Transparent;

            var bgImage = new Bitmap(Settings.Default.BGImage);

            this.BackgroundImage = new Bitmap(bgImage, this.RestoreBounds.Size);
        }

        /// <summary>
        /// Sets up setting controls.
        /// </summary>
        private void SetUpSettingControls()
        {
            this.ux_Set_ToggleFullScreen.Checked = Settings.Default.FullScreenMode;
            this.ux_CheckBox_HideJobList.Checked = TykmaGlobals.INISettings.HIDEJOBLIST;
            this.ux_Control_InstallFolder.SelectedPath = this.fileOperations.MainFolder.FullName;
            this.ux_Control_ProjectsPath.SelectedPath = this.fileOperations.ProjectFolder.FullName;
            this.ux_Control_ImagesPath.SelectedPath = this.fileOperations.InstructionsImageFolder;
            this.ux_Control_RTFPath.SelectedPath = this.fileOperations.InstructionsRtfFolder;
            this.ux_Check_AccessControlEnabled.Checked = Settings.Default.EnableAuthentication;
            this.ux_Check_PervasiveLimitsEnabled.Checked = TykmaGlobals.INISettings.ALWAYSONLIMITS;
            this.ux_Check_LoadLastJobEnabled.Checked = TykmaGlobals.INISettings.ENABLELASTJOBLOAD;
            this.ux_Check_HomeAxisOnStartup.Checked = TykmaGlobals.INISettings.HOMEONOPEN;

            // Axis settings.
            if (TykmaGlobals.INISettings.ZHOMEFREQUENCY <= this.ux_Numeric_ZHomeFrequency.Maximum)
            {
                this.ux_Numeric_ZHomeFrequency.Value = TykmaGlobals.INISettings.ZHOMEFREQUENCY;
            }

            if (TykmaGlobals.INISettings.RHOMEFREQUENCY <= this.ux_Numeric_RHomeFrequency.Maximum)
            {
                this.ux_Numeric_RHomeFrequency.Value = TykmaGlobals.INISettings.RHOMEFREQUENCY;
            }

            this.ux_Numeric_RHomeFrequency.Value = TykmaGlobals.INISettings.RHOMEFREQUENCY;
            this.ux_CheckBox_AxisZEnabled.Checked = TykmaGlobals.INISettings.ZAXISPRESENT;
            this.ux_CheckBox_AxisREnabled.Checked = TykmaGlobals.INISettings.RAXISPRESENT;

            // Language controls.
            var listofLanguages = new List<string> { "ru-RU", "en-US", "ko-KR", "de-DE", "fi-FI" };

            foreach (var lang in listofLanguages)
            {
                var checkboxItem = new ComboboxItem
                {
                    Text = new CultureInfo(lang).EnglishName,
                    Value = new CultureInfo(lang),
                };

                this.ux_cbs_LanguageSelect.Items.Add(checkboxItem);
            }

            // ReSharper disable once RedundantNameQualifier
            foreach (
                var checkboxItem in
                    this.ux_cbs_LanguageSelect.Items.Cast<ComboboxItem>()
                        .Where(checkboxItem => object.Equals(checkboxItem.Value, Settings.Default.Culture)))
            {
                this.ux_cbs_LanguageSelect.SelectedItem = checkboxItem;
            }
        }

        /// <summary>
        /// Make the form take over the entire screen.
        /// </summary>
        private void MakeFormFullSize()
        {
            this.WindowState = FormWindowState.Maximized;
            this.TopMost = true;
            this.MinimumSize = this.Size;
            this.MaximumSize = this.Size;
        }

        /// <summary>
        /// Sets up form appearance.
        /// </summary>
        private void SetUpFormAppearance()
        {
            // ReSharper disable once ArrangeStaticMemberQualifier
            if ((Control.ModifierKeys & Keys.Shift) == 0)
            {
                string initLocation = Settings.Default.InitialLocation;
                var il = new Point(0, 0);
                Size sz = this.Size;
                if (!string.IsNullOrWhiteSpace(initLocation))
                {
                    string[] parts = initLocation.Split(',');
                    if (parts.Length >= 2)
                    {
                        il = new Point(int.Parse(parts[0]), int.Parse(parts[1]));
                    }

                    if (parts.Length >= 4)
                    {
                        sz = new Size(int.Parse(parts[2]), int.Parse(parts[3]));
                    }
                }

                this.Size = sz;
                this.Location = il;
            }

            // Focus on the project selection tab.
            this.ux_HMI.SelectedIndex = 0;

            this.SetAllColors();

            // Make the screen consume the entire screen.
            if (Settings.Default.FullScreenMode)
            {
                this.MakeFormFullSize();
            }

            // Set a background image if enabled.
            if (Settings.Default.UseBgImage)
            {
                this.SetBackgroundImage();
            }

            // Hide the start button if required.
            if (TykmaGlobals.INISettings.HIDESTARTBUTTON)
            {
                this.ux_ControlButton_Start.Visible = false;
            }

            // Hide the limits button if required.
            if (TykmaGlobals.INISettings.HIDELIMITSBUTTON)
            {
                this.ux_ControlButton_Limits.Visible = false;
            }

            if (TykmaGlobals.INISettings.HIDEEDITBUTTON)
            {
                this.ux_ControlButton_Edit.Visible = false;
            }

            this.ux_ToolStrip_Axis.Visible = this.axisR.Enabled || this.axisZ.Enabled;
            this.ux_Control_ButtonAxisSetting.Visible = true;
            this.ux_ToolStrip_HomeZ.Visible = this.axisZ.Enabled;
            this.ux_ToolStrip_HomeR.Visible = this.axisR.Enabled;

            // Do not let the settings control overlap to the second monitor.
            this.ux_ToolStrip_Activate.DropDownDirection = ToolStripDropDownDirection.AboveLeft;

            this.DoubleBuffered = true;

            if (!TykmaGlobals.INISettings.TREEJOBLIST)
            {
                this.ux_Layout_JobListNav.ColumnStyles[1] = new ColumnStyle(SizeType.Absolute, 0);
                this.ux_Layout_JobListNav.ColumnStyles[0] = new ColumnStyle(SizeType.Percent, 50);
                this.ux_Layout_JobListNav.ColumnStyles[2] = new ColumnStyle(SizeType.Percent, 50);
            }

            // If the counter option is enabled, show the counter.
            this.ux_Control_Quantity.Visible = TykmaGlobals.INISettings.USECOUNTER;

            if (TykmaGlobals.INISettings.HIDEJOBLIST)
            {
                this.HideJobList();
            }
        }

        /// <summary>
        /// Sets up form events.
        /// </summary>
        private void SetUpFormEvents()
        {
            // Add event handlers for the background panels.
            foreach (
                var pnl in this.ux_Layout_Colors.Controls.OfType<Panel>().Where(pnl => pnl.Name.StartsWith("ux_Set_BG")))
            {
                pnl.Click += this.SetBgColor;
            }

            // Add event handlers for the fore color.
            foreach (var pnl in this.ux_Layout_Colors.Controls.OfType<Panel>().Where(pnl => pnl.Name.StartsWith("ux_Set_Font")))
            {
                pnl.Click += this.SetFgColor;
            }

            // Add event handlers to the status bar color pickers.
            foreach (
                var pnl in
                    this.ux_Layout_Colors.Controls.OfType<Panel>().Where(pnl => pnl.Name.StartsWith("ux_Set_Status")))
            {
                pnl.Click += this.SetStatusBarColor;
            }
        }

        /// <summary>
        /// Saves the form settings.
        /// </summary>
        private void SaveFormSettings()
        {
            // ReSharper disable once ArrangeStaticMemberQualifier
            if ((Control.ModifierKeys & Keys.Shift) == 0)
            {
                Point location = this.Location;
                Size size = this.Size;
                if (this.WindowState != FormWindowState.Normal)
                {
                    location = this.RestoreBounds.Location;
                    size = this.RestoreBounds.Size;
                }

                string initLocation = string.Join(",", location.X, location.Y, size.Width, size.Height);
                Settings.Default.InitialLocation = initLocation;
                Settings.Default.Save();
            }
        }

        /// <summary>
        /// Focuses on the appropriate control.
        /// </summary>
        private void FindFocus()
        {
            this.ux_ScannerInput.Focus();
        }

        /// <summary>
        /// Shows or hides the home button.
        /// </summary>
        private void HandleHomeButton()
        {
            this.ux_ControlButton_Home.Visible = this.ux_HMI.SelectedTab != this.ux_Tab_Main;
        }

        /// <summary>
        /// Updates the edit button visibility.
        /// </summary>
        private void UpdateEditButtonVisibility()
        {
            // Only show the edit button when there is no job loaded and we're not on the edit button screen already.
            if (this.markingJobState.Loaded & this.ux_HMI.SelectedTab != this.ux_Tab_DataEdit & this.ux_DataGridView.Rows.Count > 0 && this.ux_HMI.SelectedTab != this.ux_Tab_MarkInProgress)
            {
                if (!TykmaGlobals.INISettings.HIDEEDITBUTTON)
                {
                    this.ux_ControlButton_Edit.Visible = true;
                }
                else
                {
                    this.ux_ControlButton_Edit.Visible = false;
                }
            }
            else
            {
                this.ux_ControlButton_Edit.Visible = false;
            }
        }

        /// <summary>
        /// Hides the job list.
        /// </summary>
        private void HideJobList()
        {
            this.ux_Layout_MainInner.ColumnStyles[0].SizeType = SizeType.Absolute;
            this.ux_Layout_MainInner.ColumnStyles[0].Width = 0;
        }

        /// <summary>
        /// Shows the shutter open state.
        /// </summary>
        /// <param name="shutterOpen">if set to <c>true</c> [shutter open].</param>
        private void ShowShutterOpenState(bool shutterOpen)
        {
            this.ux_Status_Label_Shutter.Visible = shutterOpen;
        }

        /// <summary>
        /// The main form is loaded.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void MainLoad(object sender, EventArgs e)
        {
            Logger.Info(Resources.Msg_Software_Opened);
            this.ConfigureIniSettings();
            this.SetAccessOptions();
            this.InitiateAxes();
        }

        /// <summary>
        /// The main form is shown on screen.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void MainShown(object sender, EventArgs e)
        {
            this.SetUpFormAppearance();
            this.SetUpFormEvents();
            this.SetUpSettingControls();
            this.ShowHideClearButton();
            this.ConnectToCard(true);
            this.InitializeQueue();
            this.InitializeNudgeForm();

            if (TykmaGlobals.INISettings.SHOWSNINPUT)
            {
                this.ux_Text_StartingSerial.Visible = true;
            }

            // Connect to the PLC if this is a PLC enabled configuration.
            if (TykmaGlobals.PLCSettings.PLCENABLED)
            {
                this.ConnectToPLC();
                this.ux_Timer_ReconnectToPLC.Enabled = true;
            }

            this.FindFocus();

            if (TykmaGlobals.INISettings.NETENABLE)
            {
                this.dlgService = new IconLightDialogService();
                this.TCPOpen(TykmaGlobals.INISettings.NETTCPPORT);
            }

            if (TykmaGlobals.SQLSettings.USESQLSERVER)
            {
                if (this.sql == null)
                {
                    //this.TimeStampEvent("No DB in use");
                }
                else
                {
                    this.TimeStampEvent($"DB in use: {this.sql.GetType().Name}");
                }
            }

            if (TykmaGlobals.PLCSettings.PLCENABLED)
            {
                if (this.tykmaPlc == null)
                {
                    this.TimeStampEvent("No PLC in use");
                }
                else
                {
                    this.TimeStampEvent($"PLC in use: {this.tykmaPlc.GetType().Name}");
                }
            }
        }

        /// <summary>
        /// Form is closed.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="FormClosingEventArgs" /> instance containing the event data.</param>
        private void MainFormClosing(object sender, FormClosingEventArgs e)
        {
            // Close the queue form.
            if (this.queueForm != null)
            {
                this.queueForm.Close();
            }

            // Close the nudger form.
            if (this.nudgerForm != null)
            {
                this.nudgerForm.Close();
            }

            if (this.instructionsForm != null)
            {
                this.instructionsForm.Close();
                this.instructionsForm.Dispose();
            }

            if (this.PLCDialogService != null)
            {
                Settings.Default.PLCDialogInitialLocation = this.PLCDialogService.CurrentPositionAndLocation;
                Settings.Default.Save();
            }

            // Save form size/position etc.
            this.SaveFormSettings();

            // Unsubscribe from all events.
            this.le.LaserStatusMessage -= this.MarkStateEvent;
            this.le.ConnectionMessage -= this.ConnStateEvent;

            if (this.tykmaPlc != null)
            {
                this.tykmaPlc.ConnectionStatusEvent -= this.PlcConnectedEvent;

                // Unsubscribe from the tag change event.
                this.tykmaPlc.TagChangeEvent -= this.PLCTagChangeEvent;

                // Disconnect PLC.
                this.tykmaPlc.Dispose();
            }

            // Disconnect from the card.
            this.le.Dispose();

            // Stop the I/O timer.
            this.ux_Timer_IO.Enabled = false;

            // Stop command file.
            this.cmdFile.Search(false);

            if (TykmaGlobals.INISettings.NETENABLE)
            {
                this.TCPDisconnect();
            }

            // Dispose of global variable class.
            this.gv.Dispose();

            // Stop file searcher.
            this.cmdFile.Dispose();
        }

        /// <summary>
        /// Handles the Click event of the Start Cycle control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void IconStartCycleClick(object sender, EventArgs e)
        {
            this.StartSequence();
        }

        /// <summary>
        /// Stop the cycle.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void PicStopClick(object sender, EventArgs e)
        {
            this.TimeStampEvent("Stop pushed on screen");
            Logger.Info("Stop pushed on screen: {0},{1}", Control.MousePosition.X, Control.MousePosition.Y);

            this.StopSequence();
        }

        /// <summary>
        /// Show the limits.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs" /> instance containing the event data.</param>
        private void IconLimitsClick(object sender, MouseEventArgs e)
        {
            this.PreStartLimits(!this.le.LimitsOn, e.Button == MouseButtons.Right);
        }

        /// <summary>
        /// Begin focusing.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void ImgFocusClick(object sender, EventArgs e)
        {
            // this.StartFocus();
        }

        /// <summary>
        /// Load project when chosen in job list.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void ProjectsListSelectedIndexChanged(object sender, EventArgs e)
        {
            var lb = (Label)sender;

            this.ActiveBlueNileObject = null;

            await this.LoadJobAbstractor(lb.Text);

            this.FindFocus();
        }

        /// <summary>
        /// Handles the NodeMouseClick event of the Job_Tree control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TreeNodeMouseClickEventArgs" /> instance containing the event data.</param>
        private async void JobTreeNodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node != null)
            {
                if (e.Node.Parent == null)
                {
                    // Root level.
                }
                else
                {
                    if (e.Button == MouseButtons.Left)
                    {
                        if (await this.LoadJobAbstractor(e.Node.Text))
                        {
                            await Task.Delay(500);
                            this.ux_HMI.SelectedTab = this.ux_Tab_Main;
                            this.FindFocus();
                        }
                    }
                    else if (e.Button == MouseButtons.Right)
                    {
                        this.AddJobToQueue(e.Node.Text);
                    }
                }
            }
        }

        /// <summary>
        /// Select next list of jobs.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void PicNextListClick(object sender, EventArgs e)
        {
            if (this.selectedJobListIndex + 2 <= this.jobList.ListCount)
            {
                this.selectedJobListIndex += 1;
                this.PopulateJobList();
            }

            this.ShowOrHideJobNavButtons();
            this.FindFocus();
        }

        /// <summary>
        /// Select previous list of jobs.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void PicPrevListClick(object sender, EventArgs e)
        {
            if (this.selectedJobListIndex - 1 >= 0)
            {
                this.selectedJobListIndex -= 1;
                this.PopulateJobList();
            }

            this.ShowOrHideJobNavButtons();
            this.FindFocus();
        }

        /// <summary>
        /// Handles the Click event of the Navigation JobList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void NavJobListClick(object sender, EventArgs e)
        {
            this.ux_HMI.SelectedTab = this.ux_Tab_JobSelect;
            this.ux_Job_Tree.SelectedNode = null;
        }

        /// <summary>
        /// Shows the or hide project navigation buttons.
        /// </summary>
        private void ShowOrHideJobNavButtons()
        {
            // We will always want to know which list index we are on, and never go below 0 or above the max count.

            // If we are on the max, do not show the > arrow.
            this.ux_Nav_NextJobScreen.Visible = this.selectedJobListIndex < this.jobList.ListCount - 1;

            // If we are on the min, do not show the < arrow.
            this.ux_Nav_PrevJobScreen.Visible = this.selectedJobListIndex > 0;

            // Only show the 'view job list' button if there are actual jobs present.
            if (TykmaGlobals.INISettings.TREEJOBLIST)
            {
                this.ux_Nav_JobList.Visible = this.jobList.GetEntireList().Count > 0;
            }
        }

        /// <summary>
        /// Sets up the text data view.
        /// </summary>
        private void InitDataGridView(IEnumerable<EntityInformation> ids = null)
        {
            var theids = ids;
            if (theids == null)
            {
                theids = this.GetListOfIDs();
            }

            this.ux_DataGridView.Use(p =>
            {
                p.DataSource = null;
                p.Columns.Clear();
                p.ColumnCount = 2;
                p.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                p.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                p.Columns[0].DefaultCellStyle.Alignment =
                    DataGridViewContentAlignment.MiddleLeft;
                p.Columns[0].ReadOnly = true;
                p.Columns[1].ReadOnly = false;
                p.Columns[1].DefaultCellStyle.Alignment =
                    DataGridViewContentAlignment.MiddleLeft;
                p.Columns[1].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                p.AllowUserToOrderColumns = false;
                p.ColumnHeadersVisible = true;
                p.Columns[0].HeaderCell.Value = "ID";
                p.Columns[1].HeaderCell.Value = "Value";
                p.RowHeadersVisible = false;
                p.BackgroundColor = this.BackColor;
                p.AllowUserToAddRows = false;
                p.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            });

            if (this.markingJobState.Loaded)
            {
                var l = theids;
                foreach (EntityInformation entity in l)
                {
                    if (entity.Name.StartsWith("*"))
                    {
                        var rowID = this.ux_DataGridView.Rows.Add(entity.Name, entity.Value);
                        this.ux_DataGridView.Rows[rowID].ReadOnly = true;
                        this.ux_DataGridView.Rows[rowID].DefaultCellStyle.BackColor = Color.LightGray;
                    }
                    else if (entity.Name.StartsWith("%"))
                    {

                    }
                    else if (!entity.IsTextObject)
                    {
                    }
                    else
                    {
                        this.ux_DataGridView.Rows.Add(entity.Name, entity.Value);
                    }
                }
            }

            this.FindFocus();
        }

        /// <summary>
        /// Get the Z height in the project.
        /// </summary>
        private void GetZHeight(IEnumerable<EntityInformation> ids = null)
        {
            var l = ids;
            if (l == null)
            {
                l = this.GetListOfIDs();
            }

            foreach (var item in l)
            {
                if (item.Name.Equals("%Z"))
                {
                    double result;
                    if (double.TryParse(item.Value, out result))
                    {
                        this.ZHeight = result;
                        this.TimeStampEvent($"Z set to {result:#.###}");
                    }
                    else
                    {
                        this.ZHeight = null;
                    }
                }
            }
        }

        private void InitializeRotaryMark(IEnumerable<EntityInformation> l)
        {
            var entities = l.Select(item => item.Name).ToList();
            if (entities.Contains("%DIAMETER", StringComparer.OrdinalIgnoreCase)
                && entities.Contains("%SPLIT", StringComparer.OrdinalIgnoreCase))
            {
                Logger.Info("Split Mark Detected");
                this.SplitMarkJob = true;

                var diam = l.FirstOrDefault(x => x.Name.Equals("%DIAMETER"));
                var split = l.FirstOrDefault(x => x.Name.Equals("%SPLIT"));

                var diameterString = diam == null ? "0" : diam.Value;
                var splitString = split == null ? "0" : split.Value;

                double zvalue = 0;
                if (this.ZHeight.HasValue)
                {
                    zvalue = this.ZHeight.Value;
                }

                this.RotaryMarkLogic.Set(diameterString, splitString, zvalue, TykmaGlobals.INISettings.SHOWROTARYSETTINGS);
            }
            else
            {
                this.SplitMarkJob = false;
                this.RotaryMarkLogic.Hide();
            }
        }

        /// <summary>
        /// Handles the KeyDown event of the Data Grid View control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private async void ObjectDataKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back)
            {
                foreach (DataGridViewRow row in this.ux_DataGridView.Rows)
                {
                    if (row.Cells[0].Selected || row.Cells[1].Selected)
                    {
                        string objName = row.Cells[0].Value.ToString();
                        if (await this.ChangeTextByName(objName, string.Empty))
                        {
                            row.Cells[1].Value = string.Empty;
                        }
                    }
                }

                this.UpdatePreview();
            }
        }

        /// <summary>
        /// Data edited, update the data in the project.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataGridViewCellEventArgs" /> instance containing the event data.</param>
        private async void SeqDataCellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            this.BeginInvoke(new Action(async () =>
            {
                // We will only accept an edit if it comes from the 2nd column.
                if (this.ux_DataGridView.Columns[e.ColumnIndex].Index == 1)
                {
                    // What is the name of this row?
                    string objName = this.ux_DataGridView[0, e.RowIndex].Value.ToString();

                    string data = string.Empty;

                    if (this.ux_DataGridView[1, e.RowIndex].Value != null)
                    {
                        data = this.ux_DataGridView[1, e.RowIndex].Value.ToString();
                    }

                    if (await this.ChangeTextByName(objName, data))
                    {
                        this.JobState = JobStatus.Status.DataSet;
                        this.UpdatePreview();
                        this.TimeStampEvent(string.Format("{0} {1}", Resources.Msg_ChangedText, objName));
                        Logger.Debug("{0} {1}", Resources.Msg_ChangedText, objName);

                        for (int i = 0; i <= this.ux_DataGridView.Rows.Count - 1; i++)
                        {
                            if (this.ux_DataGridView[0, i].Value == null)
                            {
                                continue;
                            }

                            if (string.CompareOrdinal(this.ux_DataGridView[0, i].Value.ToString(), objName) == 0)
                            {
                                this.ux_DataGridView[1, i].Value = data;
                            }
                        }
                    }
                }
            }));
        }

        /// <summary>
        /// Show the home tab.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void IconHomeClick(object sender, EventArgs e)
        {
            this.ux_HMI.SelectedTab = this.ux_Tab_Main;
            this.FindFocus();
        }

        /// <summary>
        /// Handles tab entering.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void TabEnter(object sender, EventArgs e)
        {
            this.HandleHomeButton();
            this.UpdateEditButtonVisibility();

            if (this.ux_HMI.SelectedTab == this.ux_Tab_About)
            {
                this.ShowAboutInfo();
            }
            else if (this.ux_HMI.SelectedTab == this.ux_Tab_Moves)
            {
                this.ux_ControlButton_Start.ux_Image_Icon.Image = ImageResources.stop_cycle_active;
                this.ux_ControlButton_Start.Text = Resources.Main_Stop_Button;
            }
            else if (this.ux_HMI.SelectedTab == this.ux_Tab_DataEdit)
            {
                if (this.ux_DataGridView.Rows.Count > 0)
                {
                    this.ux_DataGridView.CurrentCell = this.ux_DataGridView.Rows[0].Cells[1];
                }
            }
            else
            {
                this.ux_ControlButton_Start.ux_Image_Icon.Image = ImageResources.start_cycle;
                this.ux_ControlButton_Start.Text = Resources.Main_Start_Button;
            }
        }

        /// <summary>
        /// Handles the Click event of the PictureEdit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void IconEditDataClick(object sender, EventArgs e)
        {
            this.ux_HMI.SelectedTab = this.ux_Tab_DataEdit;
        }

        /// <summary>
        /// Clear sequence picture.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void IconSequenceClearClick(object sender, EventArgs e)
        {
            this.ClearSequence();
        }

        /// <summary>
        /// Prompt user to exit.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void ExitToolStripMenuItem1Click(object sender, EventArgs e)
        {
            if (
                MessageBox.Show(
                    Resources.Main_ExitToolStripMenuItem1_Click_Are_you_sure_you_want_to_exit_,
                    Resources.Main_ExitToolStripMenuItem1_Click_Exit,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning) != DialogResult.Yes)
            {
                return;
            }

            this.Close();
        }

        /// <summary>
        /// Show the settings screen.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void SettingsToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.ux_HMI.SelectedTab = this.ux_Tab_Settings;
        }

        /// <summary>
        /// Minimize the main form.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void MinimizeToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        /// <summary>
        /// Show the event log.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void ToolStripLaserStatusClick(object sender, EventArgs e)
        {
        }

        private void Ux_StatusStrip_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.ux_HMI.SelectedTab == this.ux_Tab_EventLog)
            {
                this.ux_HMI.SelectedTab = this.ux_Tab_Main;
            }
            else
            {
                this.LockTheDisplay(false);
                this.ux_HMI.SelectedTab = this.ux_Tab_EventLog;
            }
        }

        /// <summary>
        /// Show the I/O Monitor.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void IOToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.ux_HMI.SelectedTab = this.ux_Tab_IO;
        }

        /// <summary>
        /// Bring up the focusing window.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void FocusToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.ux_HMI.SelectedTab = this.ux_Tab_Focus;
        }

        /// <summary>
        /// Handles the Click event of the reconnectToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void ReconnectToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (this.tykmaPlc == null)
            {
                return;
            }

            if (!this.tykmaPlc.Connected)
            {
                this.ConnectToPLC();
            }
        }

        /// <summary>
        /// Handles the Click event of the settingsToolStripMenuItem1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void SettingsToolStripMenuItem1Click(object sender, EventArgs e)
        {
            this.ux_HMI.SelectedTab = this.ux_Tab_PLC;
        }

        /// <summary>
        /// Handles the Click event of the disconnectToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void DisconnectToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.plcManuallyDisconnected = true;
            this.DisconnectFromPLC();
        }

        /// <summary>
        /// Handles the Click event of the engineerToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void EngineerToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.ShowAuthForm();
        }

        /// <summary>
        /// Handles the Click event of the operatorToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void OperatorToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.authorization.BecomeOperator();
            this.SetAccessOptions();
            this.TimeStampEvent(Resources.Msg_Logged_In_Operator);
            Logger.Info(Resources.Msg_Logged_In_Operator);
        }

        /// <summary>
        /// Handles the Click event of the ToolStripLabelPLCStatus control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void ToolStripLabelPlcStatusClick(object sender, EventArgs e)
        {
            if (this.ux_ToolStrip_Settings.Enabled)
            {
                this.ux_HMI.SelectedTab = this.ux_Tab_PLC;
            }
        }

        /// <summary>
        /// Handles the Click event of the ToolStrip global variables control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ToolStripGlobalsClick(object sender, EventArgs e)
        {
            this.ux_HMI.SelectedTab = this.ux_Tab_Global;
        }

        /// <summary>
        /// Handles the Click event of the ToolStripFileSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void ToolStripFileSearchClick(object sender, EventArgs e)
        {
            if (this.ux_ToolStrip_Settings.Enabled)
            {
                this.ux_HMI.SelectedTab = this.ux_Tab_FileImport;
            }
        }

        /// <summary>
        /// Handles the Click event of the ToolStrip_About control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void ToolStripAboutClick(object sender, EventArgs e)
        {
            this.ux_HMI.SelectedTab = this.ux_Tab_About;
        }

        /// <summary>
        /// Handles the HomeZ event of the ToolStrip control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void ToolStripHomeZ(object sender, EventArgs e)
        {
            await this.HomeAnAxis(this.axisZ);
        }

        /// <summary>
        /// Handles the HomeR event of the ToolStrip control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void ToolStripHomeR(object sender, EventArgs e)
        {
            await this.HomeAnAxis(this.axisR);
        }

        /// <summary>
        /// Handles the Click event of the StatusLabel_Connecting control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void StatusLabelConnectingClick(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Choose a background image.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void PbbgClick(object sender, EventArgs e)
        {
            var openFile = new OpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures),
            };

            var result = openFile.ShowDialog();

            if (result == DialogResult.OK)
            {
                if (!string.IsNullOrWhiteSpace(openFile.FileName))
                {
                    Settings.Default.BGImage = openFile.FileName;
                    Settings.Default.UseBgImage = true;
                    this.SetBackgroundImage();
                }
            }
        }

        /// <summary>
        /// Locks and unlocks the display.
        /// </summary>
        /// <param name="enable">The enable.</param>
        private void LockTheDisplay(bool enable)
        {
            // List of controls whose states we will toggle.
            var listOfControls = new List<Control>
            {
                this.ux_ControlButton_Limits,
                this.ux_DataGridView,
                this.ux_ControlButton_Home,
                this.ux_ControlButton_Edit,
                this.ux_ControlButton_Clear,
            };

            // Go through every control and set it as locked/unlocked (visible/enabled).
            foreach (Control cc in listOfControls)
            {
                cc.Enabled = !enable;
                cc.Visible = !enable;
            }

            // If a configuration setting is hiding the start button - hide it.
            // Otherwise only show it if we are unlocking the display.
            if (TykmaGlobals.INISettings.HIDESTARTBUTTON)
            {
                this.ux_ControlButton_Start.Visible = false;
            }
            else
            {
                this.ux_ControlButton_Start.Visible = !enable;
            }

            if (TykmaGlobals.INISettings.HIDELIMITSBUTTON)
            {
                this.ux_ControlButton_Limits.Visible = false;
            }
            else
            {
                this.ux_ControlButton_Limits.Visible = !enable;
            }

            // If there is no job loaded, hide the clear and edit buttons.
            if (!this.markingJobState.Loaded)
            {
                this.ux_ControlButton_Clear.Visible = false;
            }

            // If we are on any tab other than the I/O, show the marking in progress screen on lock.
            // and show the main screen on unlock.
            if (this.ux_HMI.SelectedTab != this.ux_Tab_IO)
            {
                if (enable)
                {
                    this.ux_HMI.SelectedTab = this.ux_Tab_MarkInProgress;
                    this.ux_Layout_Main.BackColor = this.ux_Layout_MarkInProgressMain.BackColor;
                    this.ux_Label_ElapsedTime.Text = string.Empty;
                }
                else
                {
                    this.ux_HMI.SelectedTab = this.ux_Tab_Main;
                    this.SetAllColors();
                }
            }

            this.UpdateEditButtonVisibility();

            this.FindFocus();
        }

        /// <summary>
        /// Go to color settings.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void SettingThemeClick(object sender, EventArgs e)
        {
            this.ux_HMI.SelectedTab = this.ux_Tab_Color;
        }

        /// <summary>
        /// Go to card settings.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void SettingGeneralClick(object sender, EventArgs e)
        {
            this.ux_HMI.SelectedTab = this.ux_Tab_General;
        }

        /// <summary>
        /// Go to PLC settings.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void SettingPLCClick(object sender, EventArgs e)
        {
            this.ux_HMI.SelectedTab = this.ux_Tab_PLC;
        }

        private void ControlButtonAllSettings1_Click(object sender, EventArgs e)
        {
            this.ux_HMI.SelectedTab = this.ux_tab_pg;
        }

        /// <summary>
        /// Go to data import settings.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void SettingFileImportClick(object sender, EventArgs e)
        {
            this.ux_HMI.SelectedTab = this.ux_Tab_FileImport;
        }

        /// <summary>
        /// Handles the Click event of the Pic_Axis control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void SettingAxisClick(object sender, EventArgs e)
        {
            this.ux_HMI.SelectedTab = this.ux_Tab_Axis;
        }

        /// <summary>
        /// Back button on the general settings page.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void BackGeneralSettingsClick(object sender, EventArgs e)
        {
            this.ux_HMI.SelectedTab = this.ux_Tab_Settings;
        }

        /// <summary>
        /// Save the general settings.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void SaveGeneralSettingsClick(object sender, EventArgs e)
        {
            try
            {
                if (Path.IsPathRooted(this.ux_Control_ProjectsPath.SelectedPath))
                {
                    this.fileOperations.ProjectFolder = new DirectoryInfo(this.ux_Control_ProjectsPath.SelectedPath);
                }
                else
                {
                    this.TimeStampEvent("Invalid project Path");
                    return;
                }
            }
            catch (ArgumentException exc)
            {
                this.TimeStampEvent(exc.Message);
                return;
            }

            try
            {
                if (Path.IsPathRooted(this.ux_Control_InstallFolder.SelectedPath))
                {
                    this.fileOperations.MainFolder = new DirectoryInfo(this.ux_Control_InstallFolder.SelectedPath);
                }
                else
                {
                    this.TimeStampEvent("Invalid Install Path");
                    return;
                }
            }
            catch (ArgumentException exc)
            {
                this.TimeStampEvent(exc.Message);
                return;
            }

            this.fileOperations.InstructionsRtfFolder = this.ux_Control_RTFPath.SelectedPath;
            this.fileOperations.InstructionsImageFolder = this.ux_Control_ImagesPath.SelectedPath;

            this.fileOperations.SetPaths();
            this.fileOperations.InitializePaths();
            this.ConfigureFileSystemWatcher();

            Settings.Default.EnableAuthentication = this.ux_Check_AccessControlEnabled.Checked;
            if (!string.IsNullOrWhiteSpace(this.ux_Set_NewPassword.Text))
            {
                this.authorization.ChangeEngineerPassword(this.ux_Set_NewPassword.Text);
            }

            this.ux_Set_NewPassword.Text = string.Empty;

            TykmaGlobals.INISettings.INSTRUCTIONSENABLE = this.ux_Check_InstructionsEnabled.Checked;

            TykmaGlobals.INISettings.ALWAYSONLIMITS = this.ux_Check_PervasiveLimitsEnabled.Checked;

            TykmaGlobals.INISettings.ENABLELASTJOBLOAD = this.ux_Check_LoadLastJobEnabled.Checked;

            Settings.Default.Save();

            this.TimeStampEvent(Resources.Msg_General_Settings_Saved);
            Logger.Info(Resources.Msg_General_Settings_Saved);
            this.GetListOfProjects();
            this.SetAccessOptions();
        }

        /// <summary>
        /// Operator clicks the 'back' button on the theme settings page.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ButtonThemeBackClick(object sender, EventArgs e)
        {
            this.ux_HMI.SelectedTab = this.ux_Tab_Settings;
        }

        /// <summary>
        /// Handles the Click event of the Button_ThemeSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        [SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1126:PrefixCallsCorrectly", Justification = "Reviewed. Suppression is OK here.")]
        private void ButtonThemeSaveClick(object sender, EventArgs e)
        {
            TykmaGlobals.INISettings.HIDEJOBLIST = this.ux_CheckBox_HideJobList.Checked;

            if (this.ux_cbs_LanguageSelect.SelectedItem != null)
            {
                var selectedLanguageItem = (ComboboxItem)this.ux_cbs_LanguageSelect.SelectedItem;

                if (!Equals(Settings.Default.Culture, selectedLanguageItem.Value))
                {
                    Settings.Default.Culture = (CultureInfo)selectedLanguageItem.Value;
                    Settings.Default.Save();
                    this.TimeStampEvent(
                        string.Format(
                            "{0}: {1}",
                            Resources.Msg_Changed_Language_To,
                            ((CultureInfo)selectedLanguageItem.Value).NativeName));
                    Logger.Info(
                        "{0}: {1}",
                        Resources.Msg_Changed_Language_To,
                        ((CultureInfo)selectedLanguageItem.Value).NativeName);
                }
            }

            Settings.Default.FullScreenMode = this.ux_Set_ToggleFullScreen.Checked;
            Settings.Default.Save();

            this.TimeStampEvent(Resources.Msg_Theme_Settings_Saved);
            Logger.Info(Resources.Msg_Theme_Settings_Saved);
        }

        /// <summary>
        /// Handles the Leave event of the tabIOMonitor control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void TabIOMonitorLeave(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Clears the sequence.
        /// </summary>
        private async void ClearSequence()
        {
            this.ux_Status_InnerOrOuterRing.Visible = true;

            supressResize = true;
            this.ux_nud_FontWidth.Value = this.ux_nud_FontWidth.Minimum;
            this.ux_nud_fontHeight.Value = this.ux_nud_fontHeight.Minimum;
            this.ux_cb_lock.Checked = false;
            supressResize = false;

            // We will set the job state as not being loaded - this will be passed through to the httpd interface.
            this.JobState = JobStatus.Status.NotLoaded;

            // If there are instructions loaded - clear them.
            this.ClearInstructions();

            // If any limits are on - turn them off.
            this.PreStartLimits(false, false);

            // Job is not loaded.
            this.markingJobState.Unload();

            // We will turn software ready off - unless we are using a queue and it has available jobs.
            this.SetSoftwareReady(TykmaGlobals.INISettings.USEQUEUE && this.queueForm.JobsAvailable);

            // Re-set the data view.
            this.InitDataGridView();

            // Clear the preview image.
            this.ux_PreviewPic.Image = this.imageBrands.NoJobLoaded;
            this.ux_PreviewPic.SizeMode = PictureBoxSizeMode.Zoom;

            // Log the sequence clear.
            this.TimeStampEvent(Resources.Msg_Sequence_Cleared);
            Logger.Info(Resources.Msg_Sequence_Cleared);

            // No job should be selected/active in the job list.
            this.UnselectAllJobs();

            // Go to the main screen.
            this.ux_HMI.SelectedTab = this.ux_Tab_Main;

            // Hide the clear button.
            this.ShowHideClearButton();

            // Hide edit button.
            this.UpdateEditButtonVisibility();

            // If our engine keeps a project loaded in memory - clear it.
            await this.ClearProject();

            // Reset the quantity.
            this.ux_Control_Quantity.Reset();

            // Start searching for a new job if we are in a data import mode.
            if (TykmaGlobals.INISettings.DATASEARCHENABLED && TykmaGlobals.INISettings.DATAJOBLOADMODE)
            {
                this.cmdFile.Search(true);
            }

            this.SplitMarkJob = false;

            this.ZHeight = null;
            this.RotaryMarkLogic.Hide();

            this.AllIDs = new List<EntityInformation>();

            this.MarkedItemsInArray?.Clear();
            this.ArrayItemsMarked = 0;

            this.ActiveBlueNileObject = null;
        }

        /// <summary>
        /// Shows or hides the clear button.
        /// </summary>
        private void ShowHideClearButton()
        {
            if (this.markingJobState.Loaded || this.ux_Status_Import_State.Text == Resources.Main_ShowHideClearButton_Exception_)
            {
                this.ux_ControlButton_Clear.Visible = true;
            }
            else
            {
                this.ux_ControlButton_Clear.Visible = false;
            }
        }

        /// <summary>
        /// Shows the main screen.
        /// </summary>
        private void ShowMainScreen()
        {
            this.ux_HMI.SelectedTab = this.ux_Tab_Main;
            this.FindFocus();
        }

        /// <summary>
        /// Shows the about info.
        /// </summary>
        private void ShowAboutInfo()
        {
            this.ux_TextBox_About.Text = Environment.NewLine;
            this.ux_TextBox_About.AppendText(string.Join(Environment.NewLine, TykmaGlobals.SoftwareInformation()));
        }

        /// <summary>
        /// Handles the KeyPress event of the HMI control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyPressEventArgs"/> instance containing the event data.</param>
        private void HmiKeyPress(object sender, KeyPressEventArgs e)
        {
            // If we are on the authorization access screen then we will handle
            // keypad entry.
            if (this.ux_HMI.SelectedTab == this.ux_Tab_Auth)
            {
                this.AuthKeyEntered(e);
            }
        }

        /// <summary>
        /// Handles the ResizeEnd event of the Main control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void MainResizeEnd(object sender, EventArgs e)
        {
            if (this.ux_HMI.SelectedTab == this.ux_Tab_Main)
            {
                if (this.markingJobState.Loaded)
                {
                    this.UpdatePreview();
                }
            }
        }

        private void InitializePropertyGrid()
        {
            var view = new INISettingsView(TykmaGlobals.INISettings, TykmaGlobals.SQLSettings);
            this.ux_pg_inisettings.UseCompatibleTextRendering = true;
            this.ux_pg_inisettings.SelectedObject = view;
        }

        const int WM_SYSCOMMAND = 0x0112;
        const int SC_MAXIMIZE = 0xF030;
        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            if (m.Msg == WM_SYSCOMMAND)
            {
                if (m.WParam == (IntPtr)SC_MAXIMIZE)
                {
                    //the window has been maximized
                    this.OnResizeEnd(EventArgs.Empty);
                }
            }
        }

        private void ux_nud_FontWidth_ValueChanged(object sender, EventArgs e)
        {
            this.ResizeObject();
        }

        private void ux_nud_fontHeight_ValueChanged(object sender, EventArgs e)
        {
            this.ResizeHeight();
        }
    }
}