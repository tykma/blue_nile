﻿// <copyright file="DBData.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Integration.ExternalDB
{
    using System.Collections.Generic;

    /// <summary>
    /// Parsed DB data class.
    /// </summary>
    public class DBData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DBData"/> class.
        /// </summary>
        /// <param name="template">The template name.</param>
        /// <param name="dataFields">List of data fields.</param>
        public DBData(string template, Dictionary<string, string> dataFields)
        {
            this.Template = template;
            this.DataFields = dataFields;
        }

        /// <summary>
        /// Gets the template name.
        /// </summary>
        public string Template { get; }

        /// <summary>
        /// Gets the list of data fields.
        /// </summary>
        public Dictionary<string, string> DataFields { get; }
    }
}
