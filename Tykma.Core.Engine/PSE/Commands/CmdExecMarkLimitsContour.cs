﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdExecMarkLimitsContour.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// Command to trace an outer bounding box.
    /// </summary>
    internal class CmdExecMarkLimitsContour : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdExecMarkLimitsContour"/> class.
        /// </summary>
        /// <param name="lmcApi">
        /// The laser mark card API.
        /// </param>
        public CmdExecMarkLimitsContour(IntPtr lmcApi)
            : base(lmcApi)
        {
        }

        /// <summary>
        /// Mark the limits with a red light.
        /// </summary>
        /// <returns>Return code.</returns>
        private delegate int LMC1ApiRedLightMark();

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Return code state.</returns>
        internal bool Execute()
        {
            // Get pointer to our trace function.
            var lmc1RedLightMark = this.LoadFunction<LMC1ApiRedLightMark>("lmc1_RedLightMark");

            var rm = new ReturnCodeMessage();

            // Check that the function exists.
            if (lmc1RedLightMark != null)
            {
                try
                {
                    // Start tracing.
                    rm = this.GetMiniLaseReturnCode(lmc1RedLightMark());

                    if (rm.Success)
                    {
                        return true;
                    }

                    throw new MethodFaultException(rm.ReturnDescription);
                }
                catch (AccessViolationException e)
                {
                    rm.Status = ReturnCodeMsg.Error;
                    rm.ReturnDescription = e.Message;
                    throw new MethodFaultException(rm.ReturnDescription);
                }
            }
            else
            {
                throw new EngineNotConnectedException();
            }
        }
    }
}
