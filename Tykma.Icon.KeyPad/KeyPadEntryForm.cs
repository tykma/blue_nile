﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KeyPadEntryForm.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.KeyPad
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    /// <summary>
    /// The key pad entry form.
    /// </summary>
    public partial class KeyPadEntryForm : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KeyPadEntryForm" /> class.
        /// </summary>
        /// <param name="bg">The background color.</param>
        /// <param name="fg">The foreground color.</param>
        public KeyPadEntryForm(Color bg, Color fg)
        {
            this.ForeColor = fg;
            this.BackColor = bg;

            this.InitializeComponent();

            this.ux_Auth_MaskedKey.Text = string.Empty;

            this.EnteredValue = string.Empty;

            this.ux_Pic_Key0.Click += this.KeyClick;
            this.ux_Pic_Key1.Click += this.KeyClick;
            this.ux_Pic_Key2.Click += this.KeyClick;
            this.ux_Pic_Key3.Click += this.KeyClick;
            this.ux_Pic_Key4.Click += this.KeyClick;
            this.ux_Pic_Key5.Click += this.KeyClick;
            this.ux_Pic_Key6.Click += this.KeyClick;
            this.ux_Pic_Key7.Click += this.KeyClick;
            this.ux_Pic_Key8.Click += this.KeyClick;
            this.ux_Pic_Key9.Click += this.KeyClick;
        }

        /// <summary>
        /// Gets the user entered value.
        /// </summary>
        /// <value>
        /// The user entered value.
        /// </value>
        public int UserEnteredValue { get; private set; }

        /// <summary>
        /// Gets or sets the entered value.
        /// </summary>
        /// <value>
        /// The entered value.
        /// </value>
        private string EnteredValue { get; set; }

        /// <summary>
        /// Keys the click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void KeyClick(object sender, EventArgs e)
        {
            int? keyValue = null;

            if (sender == this.ux_Pic_Key0)
            {
                keyValue = 0;
            }
            else if (sender == this.ux_Pic_Key1)
            {
                keyValue = 1;
            }
            else if (sender == this.ux_Pic_Key2)
            {
                keyValue = 2;
            }
            else if (sender == this.ux_Pic_Key3)
            {
                keyValue = 3;
            }
            else if (sender == this.ux_Pic_Key4)
            {
                keyValue = 4;
            }
            else if (sender == this.ux_Pic_Key5)
            {
                keyValue = 5;
            }
            else if (sender == this.ux_Pic_Key6)
            {
                keyValue = 6;
            }
            else if (sender == this.ux_Pic_Key7)
            {
                keyValue = 7;
            }
            else if (sender == this.ux_Pic_Key8)
            {
                keyValue = 8;
            }
            else if (sender == this.ux_Pic_Key9)
            {
                keyValue = 9;
            }

            if (keyValue != null)
            {
                this.ux_Auth_MaskedKey.Text += keyValue;
                this.EnteredValue += keyValue;
            }
        }

        /// <summary>
        /// Trash icon clicked - reset.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void PicKeyTrashClick(object sender, EventArgs e)
        {
            this.EnteredValue = string.Empty;
            this.ux_Auth_MaskedKey.Text = string.Empty;
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// The "OK" key is clicked.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void PIcKeyOkClick(object sender, EventArgs e)
        {
            int qtyValue;
            bool res = int.TryParse(this.EnteredValue, out qtyValue);
            this.UserEnteredValue = res ? qtyValue : 0;

            this.DialogResult = DialogResult.OK;
        }
    }
}
