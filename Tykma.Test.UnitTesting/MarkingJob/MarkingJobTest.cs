﻿// <copyright file="MarkingJobTest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Test.UnitTesting.MarkingJob
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Test marking job state functionality.
    /// </summary>
    [TestClass]
    public class MarkingJobTest
    {
        /// <summary>
        /// Basic marking job test.
        /// </summary>
        [TestMethod]
        public void BasicMarkingJobTest()
        {
            var job = new Icon.MarkingJob.MarkingJob();

            Assert.IsFalse(job.Authorized);
            Assert.IsFalse(job.Loaded);
            Assert.AreEqual(string.Empty, job.Name);
        }

        /// <summary>
        /// Tests setting marking job name.
        /// </summary>
        [TestMethod]
        public void TestMarkingJobNameSetting()
        {
            var job = new Icon.MarkingJob.MarkingJob();
            job.Name = "ABC123";
            Assert.AreEqual("ABC123", job.Name);
            job.Unload();
            Assert.AreEqual(string.Empty, job.Name);
        }

        /// <summary>
        /// Tests setting marking job loaded state.
        /// </summary>
        [TestMethod]
        public void TestMarkingJobLoadedStateSetting()
        {
            var job = new Icon.MarkingJob.MarkingJob();
            job.Loaded = true;
            Assert.IsTrue(job.Loaded);
            job.Unload();
            Assert.IsFalse(job.Loaded);
        }

        /// <summary>
        /// Tests setting the job authorization.
        /// </summary>
        [TestMethod]
        public void TestSettingJobAuthorization()
        {
            var job = new Icon.MarkingJob.MarkingJob();

            job.Authorized = true;

            Assert.IsTrue(job.Authorized);
            job.Unload();

            Assert.IsFalse(job.Authorized);
        }
    }
}
