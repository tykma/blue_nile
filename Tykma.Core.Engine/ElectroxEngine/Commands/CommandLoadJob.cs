﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandLoadJob.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.ElectroxEngine.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;

    using Electrox.Evolution.Communications;

    /// <summary>
    /// The command to load job.
    /// </summary>
    public class CommandLoadJob : IEvoCommand
    {
        /// <summary>
        /// Command receives data.
        /// </summary>
        private const bool CommandReceivesData = false;

        /// <summary>
        /// The command responds with code
        /// </summary>
        private const bool CommandReceivesCode = true;

        /// <summary>
        /// The send string.
        /// </summary>
        private readonly List<string> sendStrings;

        /// <summary>
        /// The timeout for transmission.
        /// </summary>
        private readonly TimeSpan transmissionTimeout = new TimeSpan(0, 0, 0, 10, 0);

        /// <summary>
        /// The send command
        /// </summary>
        private readonly CommandBase sendCommand;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandLoadJob" /> class.
        /// </summary>
        /// <param name="comms">The communications library.</param>
        /// <param name="jobCommands">The job commands.</param>
        public CommandLoadJob(Communications comms, List<string> jobCommands)
        {
            this.sendStrings = jobCommands;

            this.sendStrings.Add("ECHO R0");
            this.sendCommand = new CommandBase(comms)
                                   {
                                       CommandToSend = this.CommandToSend,
                                       CommandRespondsWithData = this.CommandRespondsWithData,
                                       CommandRespondsWithCode = this.CommandRespondsWithCode,
                                       CommandFinishTimeOut = this.CommandFinishTimeOut,
                                   };
        }

        /// <summary>
        /// Gets the command to send.
        /// </summary>
        public string CommandToSend
        {
            get
            {
                var sb = new StringBuilder();
                foreach (var line in this.sendStrings)
                {
                    sb.Append(string.Format("{0}{1}", line, "\r"));
                }

                return string.Format("{0}", sb);
            }
        }

        /// <summary>
        /// Gets the command finish time out.
        /// </summary>
        /// <value>
        /// The command finish time out.
        /// </value>
        public double CommandFinishTimeOut
        {
            get
            {
                return this.transmissionTimeout.TotalMilliseconds;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [command responds with data].
        /// </summary>
        /// <value>
        /// <c>true</c> if [command responds with data]; otherwise, <c>false</c>.
        /// </value>
        public bool CommandRespondsWithData
        {
            get
            {
                return CommandReceivesData;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [command responds with code].
        /// </summary>
        /// <value>
        /// <c>true</c> if [command responds with code]; otherwise, <c>false</c>.
        /// </value>
        public bool CommandRespondsWithCode
        {
            get
            {
                return CommandReceivesCode;
            }
        }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success st</returns>
        public async Task<DataReceived> Execute()
        {
            return await this.sendCommand.Execute();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.sendCommand.Dispose();
        }
    }
}
