﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.QUEUE.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Queue handling on main UI.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Linq;

    using Icon_Interface.General;

    using Tykma.Icon.Queue;

    /// <summary>
    /// Queue form related code.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// The queue form.
        /// </summary>
        private Queue queueForm;

        /// <summary>
        /// The current queued job.
        /// </summary>
        private QueuedJobInfo currentQueueJob;

        /// <summary>
        /// Queue has sent us an event - react.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private delegate void QueueEventCallBack(object sender, QueueEventMessage e);

        /// <summary>
        /// Loads the next queue job.
        /// </summary>
        private async void LoadNextQueueJob()
        {
            this.currentQueueJob = this.queueForm.NextJob;

            if (this.currentQueueJob.JobName != string.Empty)
            {
                if (!await this.LoadJobAbstractor(this.currentQueueJob.JobName, true))
                {
                    this.TimeStampEvent(string.Format("Removing Job {0} from queue", this.currentQueueJob.JobName));
                    Logger.Error("Removing Job {0} from queue", this.currentQueueJob.JobName);
                    this.currentQueueJob.Failed = true;
                    this.queueForm.RemoveJob(this.currentQueueJob.JobHash);
                    this.currentQueueJob = null;
                }
            }
        }

        /// <summary>
        /// Removes the marked queue job.
        /// </summary>
        private void RemoveMarkedQueueJob()
        {
            if (this.currentQueueJob != null)
            {
                this.queueForm.RemoveJob(this.currentQueueJob.JobHash);
            }
        }

        /// <summary>
        /// Handles a message from the queue form.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void QueueMessageEvent(object sender, QueueEventMessage e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new QueueEventCallBack(this.QueueMessageEvent), sender, e);
            }
            else
            {
                this.SetSoftwareReady(e.JobsAvailable);
            }
        }

        /// <summary>
        /// Initializes the queue.
        /// </summary>
        private void InitializeQueue()
        {
            if (TykmaGlobals.INISettings.USEQUEUE)
            {
                this.queueForm = new Queue();
                this.queueForm.Show();

                if (this.queueForm != null)
                {
                    this.queueForm.QueueAvailabilityChanged += this.QueueMessageEvent;
                }
            }
        }

        /// <summary>
        /// Adds the job to queue.
        /// </summary>
        /// <param name="jobToAdd">The job to add.</param>
        private void AddJobToQueue(string jobToAdd)
        {
            if (
                this.jobList.GetEntireList(true)
                    .Any(job => string.Compare(job, jobToAdd, StringComparison.OrdinalIgnoreCase) == 0))
            {
               // var result = await Task.Run(() => this.queueForm.AddJob(jobToAdd)));
                this.queueForm.AddJob(jobToAdd);
                this.TimeStampEvent(string.Format("Job added to queue: {0}", jobToAdd));
                Logger.Error("Job added to queue: {0}", jobToAdd);
                this.SetSoftwareReady(true);
            }
            else
            {
                this.TimeStampEvent(string.Format("Job not found: {0}", jobToAdd));
                Logger.Error("Job not found: {0}", jobToAdd);
            }
        }
    }
}
