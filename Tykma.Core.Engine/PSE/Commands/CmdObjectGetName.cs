﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdObjectGetName.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;
    using System.Text;

    /// <summary>
    /// The command get object name.
    /// </summary>
    internal class CmdObjectGetName : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdObjectGetName"/> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking card API.</param>
        /// <param name="index">The index of the object.</param>
        public CmdObjectGetName(IntPtr lmcApi, int index)
            : base(lmcApi)
        {
            this.Index = index;
        }

        /// <summary>
        /// Get name of an index.
        /// </summary>
        /// <param name="nEntityIndex">Index of the entity.</param>
        /// <param name="szEntName">Name of the entity.</param>
        /// <returns>Success code.></returns>
        private delegate int LMC1ApiGetEntityName(int nEntityIndex, [MarshalAs(UnmanagedType.LPWStr)] StringBuilder szEntName);

        /// <summary>
        /// Gets the index.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        private int Index { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Name of object.</returns>
        public string Execute()
        {
            var sb = new StringBuilder(255);

            // Get pointer to get text name function.
            var lmc1GetName = this.LoadFunction<LMC1ApiGetEntityName>("lmc1_GetEntityName");

            if (lmc1GetName == null)
            {
                return string.Empty;
            }

            // Call get name function and pass our stringbuilder by reference.
            var rm = this.GetMiniLaseReturnCode(lmc1GetName(this.Index, sb));

            return rm.Success ? sb.ToString() : string.Empty;
        }
    }
}
