﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IFoundFileParserInterface.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.FileParse
{
    using System.IO;

    /// <summary>
    /// File parser interface.
    /// </summary>
    public interface IFoundFileParserInterface
    {
        /// <summary>
        /// Parses the code.
        /// </summary>
        /// <param name="ms">The memory stream.</param>
        /// <returns>A found file class with all the information.</returns>
        FoundFileData ParseJob(MemoryStream ms);
    }
}
