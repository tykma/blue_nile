﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.TCP.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// TCP/IP interface handling in main UI.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Drawing;
    using System.Linq;
    using System.Reflection;
    using System.Web;

    using Icon_Interface.General;
    using Icon_Interface.Properties;

    using Tykma.Core.Engine;
    using Tykma.Core.HTTPD;
    using Tykma.Icon.AlphanumComparator;
    using Tykma.Icon.TCPCommand;
    using Tykma.Interface.NG.DialogService;

    /// <summary>
    /// TCP server/client code.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// The TCP interface.
        /// </summary>
        private Server httpd;

        private IDialogService dlgService;

        /// <summary>
        /// Opens the specified port.
        /// </summary>
        /// <param name="port">The port.</param>
        // ReSharper disable once InconsistentNaming
        private void TCPOpen(int port)
        {
            try
            {
                this.httpd = new Server(port);

                // Subscribe to connection event.
                this.httpd.SocketConnected += this.SocketOpenedEvent;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
            }
            finally
            {
                this.DisplayTcpServerStatus();
            }
        }

        /// <summary>
        /// Gets axis information from a http request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>AxisRequest class.</returns>
        private AxisRequest AxisHttpRequest(string request)
        {
            var returnAxis = new AxisRequest();

            int whichAxis;

            if (int.TryParse(request, out whichAxis))
            {
                if (whichAxis == this.axisZ.Number)
                {
                    returnAxis.Axis = this.axisZ;
                }
                else if (whichAxis == this.axisR.Number)
                {
                    returnAxis.Axis = this.axisR;
                }
            }

            return returnAxis;
        }

        /// <summary>
        /// Gets a job status string for TCP.
        /// </summary>
        /// <returns>Job status string.</returns>
        private string TcpGetJobStatusString()
        {
            string status = "unknown";
            switch (this.JobState)
            {
                case JobStatus.Status.Marked:
                    status = "marked";
                    break;
                case JobStatus.Status.Loaded:
                    status = "loaded";
                    break;
                case JobStatus.Status.NotLoaded:
                    status = "not_loaded";
                    break;
                case JobStatus.Status.DataSet:
                    status = "data_set";
                    break;
                case JobStatus.Status.Stopped:
                    status = "stopped";
                    break;
                case JobStatus.Status.Enabled:
                    status = "enabled";
                    break;
            }

            return status;
        }

        /// <summary>
        /// Gets the job status TCP string.
        /// </summary>
        /// <returns>Job status TCP string.</returns>
        private string TcpJobStatusString()
        {
            return "status:" + this.TcpGetJobStatusString();
        }

        /// <summary>
        /// Socket has been opened event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void SocketOpenedEvent(object sender, SocketEventMessage e)
        {
            this.InvokeIfRequired(
                async () =>
                {
                    string requestedFile = string.Empty;

                    ServerClient client = e.Client;

                    try
                    {
                        requestedFile = await client.Do();
                    }
                    catch (Exception error)
                    {
                        Logger.Fatal(error.ToString());
                    }

                    // Allow spaces when called from a browser.
                    requestedFile = requestedFile.Replace("%20", " ");

                    if (string.IsNullOrWhiteSpace(requestedFile))
                    {
                        return;
                    }

                    this.TimeStampEvent(string.Format("NET client requested string: {0} ", requestedFile));

                    var parser = new TcpCommandParse();

                    var cmdData = parser.ParseCommand(requestedFile);

                    switch (cmdData.Command)
                    {
                        case TcpCommandParse.CommandType.JobStatus:
                            {
                                await client.SendResponse("200", "text", this.TcpJobStatusString());
                                break;
                            }

                        case TcpCommandParse.CommandType.ProjectList:
                            {
                                await
                                    client.SendResponse(
                                        "200",
                                        "text",
                                        "project_list:" + string.Join(",", this.jobList.GetEntireList(true)));
                                break;
                            }

                        case TcpCommandParse.CommandType.ClearJob:
                            {
                                this.ClearSequence();
                                await client.SendResponse("200", "text", "clear:ok");
                                break;
                            }

                        case TcpCommandParse.CommandType.EnableJob:
                            {
                                if (this.JobState != JobStatus.Status.NotLoaded)
                                {
                                    this.JobState = JobStatus.Status.Enabled;
                                    await client.SendResponse("200", "text", "enable_job:ok");
                                    this.CheckStartReady();
                                }
                                else
                                {
                                    await client.SendResponse("200", "text", "enable_job:fault");
                                }

                                break;
                            }

                        case TcpCommandParse.CommandType.LoadJob:
                            {
                                if (await this.LoadJobAbstractor(cmdData.CommandData["job"], true))
                                {
                                    await client.SendResponse("200", "text", "job_load:ok");
                                }
                                else
                                {
                                    await client.SendResponse("200", "text", "job_load:fault");
                                }

                                break;
                            }

                        case TcpCommandParse.CommandType.AxisCoordinate:
                            {
                                var axiscoor = this.AxisHttpRequest(cmdData.CommandData["axis"]);

                                if (axiscoor.ValidAxis)
                                {
                                    var coordinate = this.le.GetAxisCoordinate(axiscoor.Axis.Number);
                                    await
                                        client.SendResponse(
                                            "200",
                                            "text",
                                            "axis_coor:" + axiscoor.Axis.Number + "," + Math.Round(coordinate, 0));
                                }
                                else
                                {
                                    await client.SendResponse("200", "text", "axis_coor:fault");
                                }

                                break;
                            }

                        case TcpCommandParse.CommandType.AxisLimits:
                            {
                                var getAxis = this.AxisHttpRequest(cmdData.CommandData["axis"]);
                                var minmaxvalue = cmdData.CommandData["value"];

                                if (getAxis.ValidAxis)
                                {
                                    bool getMax = minmaxvalue == "max";
                                    double valueToSend;

                                    if (getAxis.Axis.Number == this.axisZ.Number)
                                    {
                                        valueToSend = getMax
                                                          ? TykmaGlobals.INISettings.ZAXISMAX
                                                          : TykmaGlobals.INISettings.ZAXISMIN;
                                    }
                                    else if (getAxis.Axis.Number == this.axisR.Number)
                                    {
                                        valueToSend = getMax
                                                          ? TykmaGlobals.INISettings.RAXISMAX
                                                          : TykmaGlobals.INISettings.RAXISMIN;
                                    }
                                    else
                                    {
                                        await client.SendResponse("200", "text", "get_axis:fault");
                                        break;
                                    }

                                    await
                                        client.SendResponse("200", "text", string.Format("get_axis:{0},{1},{2}", getAxis.Axis.Number, getMax ? "max" : "min", valueToSend));
                                }
                                else
                                {
                                    await client.SendResponse("200", "text", "get_axis:fault");
                                }

                                break;
                            }

                        case TcpCommandParse.CommandType.AxisHome:
                            {
                                var axisHome = this.AxisHttpRequest(cmdData.CommandData["axis"]);

                                if (axisHome.ValidAxis)
                                {
                                    // ReSharper disable once CSharpWarnings::CS4014
                                    this.HomeAnAxis(axisHome.Axis).Forget();
                                    await client.SendResponse("200", "text", "axis_home:ok");
                                }
                                else
                                {
                                    await client.SendResponse("200", "text", "axis_home:fault");
                                }

                                break;
                            }

                        case TcpCommandParse.CommandType.AxisMove:
                            {
                                var whichAxis = this.AxisHttpRequest(cmdData.CommandData["axis"]);

                                string position = HttpUtility.UrlDecode(cmdData.CommandData["position"].TrimEnd());

                                double pos;
                                if (whichAxis.ValidAxis && double.TryParse(position, out pos))
                                {
                                    this.le.AxisMoveToPosition(whichAxis.Axis.Number, pos);
                                    await client.SendResponse("200", "text", "axis_move:ok");
                                }
                                else
                                {
                                    await client.SendResponse("200", "text", "axis_move:fault");
                                }

                                break;
                            }

                        case TcpCommandParse.CommandType.AxisStop:
                            {
                                var axisStop = this.AxisHttpRequest(cmdData.CommandData["axis"]);

                                if (axisStop.ValidAxis)
                                {
                                    if (this.StopAnAxis(axisStop.Axis))
                                    {
                                        await client.SendResponse("200", "text", "stop_axis:ok");
                                    }
                                    else
                                    {
                                        await client.SendResponse("200", "text", "stop_axis:fault");
                                    }
                                }
                                else
                                {
                                    await client.SendResponse("200", "text", "stop_axis:fault");
                                }

                                break;
                            }

                        case TcpCommandParse.CommandType.Version:
                            {
                                await
                                    client.SendResponse("200", "text", string.Format("{0} {1}", Assembly.GetEntryAssembly().GetName().Version, "Tykma Net"));

                                break;
                            }

                        case TcpCommandParse.CommandType.SetID:
                            {
                                string datatoset = HttpUtility.UrlDecode(cmdData.CommandData["data"].TrimEnd());

                                if (this.GetListOfIDs().Any(item => item.Name == cmdData.CommandData["id"]))
                                {
                                    if (await this.ChangeTextByName(cmdData.CommandData["id"], datatoset))
                                    {
                                        this.JobState = JobStatus.Status.DataSet;
                                        await client.SendResponse("200", "text", "set_id:ok");
                                        this.UpdatePreview();
                                        this.TimeStampEvent(
                                            string.Format("Changed text {0}", cmdData.CommandData["id"]));
                                        Logger.Debug("Changed text {0}", cmdData.CommandData["id"]);
                                    }
                                    else
                                    {
                                        await client.SendResponse("200", "text", "set_id:fault");
                                    }

                                    this.InitDataGridView();
                                    this.UpdatePreview();
                                }
                                else
                                {
                                    await client.SendResponse("200", "text", "set_id:id_not_found");
                                }

                                break;
                            }

                        case TcpCommandParse.CommandType.LoadedJob:
                            {
                                await
                                    client.SendResponse("200", "text", "loaded_job:" + this.markingJobState.Name);

                                break;
                            }

                        case TcpCommandParse.CommandType.Preview:
                            {
                                var converter = new ImageConverter();
                                var imgArray = (byte[])converter.ConvertTo(this.ux_PreviewPic.Image, typeof(byte[]));
                                await client.SendFile(imgArray);

                                break;
                            }

                        case TcpCommandParse.CommandType.SystemState:
                            {
                                if (this.CheckStartReady() == Startfault.MarkingBusy)
                                {
                                    await client.SendResponse("200", "text", "state:busy");
                                }
                                else
                                {
                                    await client.SendResponse("200", "text", "state:idle");
                                }

                                break;
                            }

                        case TcpCommandParse.CommandType.LimitsToggle:
                            {
                                this.PreStartLimits(!this.le.LimitsOn, false);

                                await client.SendResponse("200", "text", "limits:OK");

                                break;
                            }

                        case TcpCommandParse.CommandType.LimitsOn:
                            {
                                this.PreStartLimits(true, false);
                                await client.SendResponse("200", "text", "limits_on:OK");

                                break;
                            }

                        case TcpCommandParse.CommandType.LimitsOff:
                            {
                                this.PreStartLimits(false, false);
                                await client.SendResponse("200", "text", "limits_off:OK");

                                break;
                            }

                        case TcpCommandParse.CommandType.Stop:
                            {
                                this.StopSequence();
                                await client.SendResponse("200", "text", "stop:OK");

                                break;
                            }

                        case TcpCommandParse.CommandType.Start:
                            {
                                switch (this.CheckStartReady())
                                {
                                    case Startfault.AxisNotHomed:
                                        await client.SendResponse("404", "text", "start:AXIS NOT HOMED");
                                        return;

                                    case Startfault.OkayToStart:
                                        await client.SendResponse("200", "text", "start:OK");
                                        this.StartSequence();
                                        return;

                                    case Startfault.NoJobLoaded:
                                        await client.SendResponse("200", "text", "start:NO JOB LOADED");
                                        return;

                                    case Startfault.MarkingBusy:
                                        await client.SendResponse("200", "text", "start:MARK IN PROGRESS");
                                        return;

                                    case Startfault.UnAuthorizedJob:
                                        {
                                            await client.SendResponse("200", "text", "start:NOT AUTHORIZED");
                                        }

                                        return;
                                }

                                await client.SendResponse("200", "text", "start:OK");
                                break;
                            }

                        case TcpCommandParse.CommandType.GetAllIds:
                            {
                                string results = string.Empty;

                                var items =
                                    this.GetListOfIDs()
                                        .Select(entity => string.Format("{0}:{1}", entity.Name, entity.Value))
                                        .OrderBy(x => x, new AlphanumComparator())
                                        .ToList();

                                if (items.Count > 0)
                                {
                                    results = items.Aggregate((a, x) => a + "," + x);
                                }

                                await client.SendResponse("200", "text", "all_ids:" + results);

                                break;
                            }

                        case TcpCommandParse.CommandType.NudgeObject:
                            {
                                double resultx;
                                double resulty;

                                if (double.TryParse(cmdData.CommandData["x"], out resultx)
                                    && double.TryParse(cmdData.CommandData["y"], out resulty))
                                {
                                    this.MoveObject(
                                        cmdData.CommandData["id"],
                                        Convert.ToDouble(cmdData.CommandData["x"]),
                                        Convert.ToDouble(cmdData.CommandData["y"]));
                                    await client.SendResponse("200", "text", "nudge_object:ok");
                                    this.UpdatePreview();
                                    break;
                                }

                                await client.SendResponse("200", "text", "nudge_object:fault");

                                break;
                            }

                        case TcpCommandParse.CommandType.MarkObject:
                            {
                                string obj = cmdData.CommandData["id"];
                                await client.SendResponse("200", "text", "mark_object:ok");
                                await this.StartMarkObject(obj);
                                break;
                            }

                        case TcpCommandParse.CommandType.RotateObject:
                            {
                                double result;
                                if (double.TryParse(cmdData.CommandData["angle"], out result))
                                {
                                    this.RotateObject(cmdData.CommandData["id"], 0, 0, Convert.ToDouble(result));
                                    await client.SendResponse("200", "text", "rotate_object:ok");
                                    this.UpdatePreview();
                                }
                                else
                                {
                                    await client.SendResponse("200", "text", "rotate_object:fault");
                                }

                                break;
                            }

                        case TcpCommandParse.CommandType.RotateAll:
                            {
                                double result;
                                if (double.TryParse(cmdData.CommandData["angle"], out result))
                                {
                                    foreach (var ent in this.GetListOfIDs())
                                    {
                                        this.le.RotateObject(ent.Name, 0, 0, Convert.ToDouble(result));
                                    }

                                    await client.SendResponse("200", "text", "rotate_all:ok");
                                    this.UpdatePreview();
                                    break;
                                }

                                await client.SendResponse("200", "text", "rotate_all:fault");
                                break;
                            }

                        case TcpCommandParse.CommandType.GetWeb:
                            {
                                var builder = new Tykma.Core.HTTPD.Web.WebBuild();

                                var page = builder.GetPage(this.jobList.GetEntireList(true));

                                await client.SendResponse("200", "text", page);
                                break;
                            }

                        case TcpCommandParse.CommandType.MoveAll:
                            {
                                double resultx;
                                double resulty;
                                if (double.TryParse(cmdData.CommandData["x"], out resultx)
                                    && double.TryParse(cmdData.CommandData["y"], out resulty))
                                {
                                    foreach (var ent in this.GetListOfIDs())
                                    {
                                        this.MoveObject(
                                            ent.Name,
                                            Convert.ToDouble(cmdData.CommandData["x"]),
                                            Convert.ToDouble(cmdData.CommandData["y"]));
                                    }

                                    await client.SendResponse("200", "text", "nudgeAll:ok");
                                    this.UpdatePreview();
                                    break;
                                }

                                await client.SendResponse("200", "text", "move_all:fault");
                                break;
                            }

                        case TcpCommandParse.CommandType.ShutterState:

                            string shutterState = string.Format("shutter_state:{0}", this.ioShutterstateOneShotOpen ? "open" : "closed");
                            await client.SendResponse("200", "text", shutterState);

                            break;

                        case TcpCommandParse.CommandType.DoorClose:

#pragma warning disable 4014
                            this.PulseDoorOpen(false);
#pragma warning restore 4014

                            await client.SendResponse("200", "text", "door_close:ok");

                            break;

                        case TcpCommandParse.CommandType.DoorOpen:

#pragma warning disable 4014
                            this.PulseDoorOpen(true);
#pragma warning restore 4014

                            await client.SendResponse("200", "text", "door_open:ok");
                            break;

                        case TcpCommandParse.CommandType.GetLaserPower:

                            int penNumber;
                            if (int.TryParse(cmdData.CommandData["pen"], out penNumber))
                            {
                                var result = this.GetLaserPower(penNumber);
                                await client.SendResponse("200", "text", string.Format("{0}:{1}", penNumber, result));
                            }
                            else
                            {
                                await client.SendResponse("404", "text", "fault:invalid pen number");
                            }

                            break;

                        case TcpCommandParse.CommandType.SetLaserPower:

                            int setLaserPenNumber;

                            // Continue if the pen number is a valid integer.
                            if (int.TryParse(cmdData.CommandData["pen"], out setLaserPenNumber))
                            {
                                double setLaserPower;

                                // Continue if the power is a valid double.
                                if (double.TryParse(cmdData.CommandData["power"], out setLaserPower))
                                {
                                    // Check if we were able to set the power.
                                    var result = this.SetLaserPower(setLaserPenNumber, setLaserPower);
                                    if (result)
                                    {
                                        // Send success result.
                                        await
                                            client.SendResponse(
                                                "200",
                                                "text",
                                                string.Format("{0}:{1}", setLaserPenNumber, "ok"));
                                    }
                                    else
                                    {
                                        // Engine could not set power.
                                        await client.SendResponse("404", "text", "fault:can't set power");
                                    }
                                }
                                else
                                {
                                    // Power was not a valid double.
                                    await client.SendResponse("404", "text", "fault:invalid power");
                                }
                            }
                            else
                            {
                                // Pen was not a valid integer.
                                await client.SendResponse("404", "text", "fault:invalid pen number");
                            }

                            break;

                        case TcpCommandParse.CommandType.GetLaserFrequency:

                            int penNumberFrequency;
                            if (int.TryParse(cmdData.CommandData["pen"], out penNumberFrequency))
                            {
                                var result = this.GetLaserFrequency(penNumberFrequency);
                                await client.SendResponse("200", "text", string.Format("{0}:{1}", penNumberFrequency, result));
                            }
                            else
                            {
                                await client.SendResponse("404", "text", "fault:invalid pen number");
                            }

                            break;

                        case TcpCommandParse.CommandType.DeleteObject:

                            if (cmdData.CommandData["object"] != null)
                            {
                                var result = this.DeleteObject(cmdData.CommandData["object"]);

                                if (result)
                                {
                                    await
                                        client.SendResponse(
                                            "200",
                                            "text",
                                            string.Format("{0}:{1}", "delete_object", "ok"));
                                    this.UpdatePreview();
                                    this.InitDataGridView();
                                }
                                else
                                {
                                    await
                                        client.SendResponse(
                                            "200",
                                            "text",
                                            string.Format("{0}:{1}", "delete_object", "fault"));
                                }
                            }
                            else
                            {
                                await
                                    client.SendResponse(
                                        "200",
                                        "text",
                                        string.Format("{0}:{1}", "delete_object", "missing"));
                            }

                            break;

                        case TcpCommandParse.CommandType.ShowUserMessage:

                            var msg = cmdData.CommandData["message"];
                            this.dlgService.ShowMessage(msg, "Message");

                            await client.SendResponse("200", "text", "show_user_message:ok");

                            break;

                        case TcpCommandParse.CommandType.HideUserMessage:

                            this.dlgService.HideMessage();

                            await client.SendResponse("200", "text", "hide_user_message:ok");

                            break;

                        case TcpCommandParse.CommandType.None:
                            {
                                await client.SendResponse("404", "text", "UNKNOWN COMMAND");
                                break;
                            }
                    }
                });
        }

        /// <summary>
        /// Closes this instance.
        /// </summary>
        private void TCPDisconnect()
        {
            // this.tcpInterface.Close();
        }

        /// <summary>
        /// Displays the TCP server status.
        /// </summary>
        private void DisplayTcpServerStatus()
        {
            if (this.httpd != null && this.httpd.IsConnected)
            {
                this.TimeStampEvent(string.Format("{0} {1}", Resources.Msg_Opened_TCP_Port, TykmaGlobals.INISettings.NETTCPPORT));
                Logger.Info("{0} {1}", Resources.Msg_Opened_TCP_Port, TykmaGlobals.INISettings.NETTCPPORT);
            }
            else
            {
                this.TimeStampEvent(string.Format("{0} {1}", Resources.Msg_Could_Not_Open_TCP_Port, TykmaGlobals.INISettings.NETTCPPORT));
                Logger.Info("{0} {1}", Resources.Msg_Could_Not_Open_TCP_Port, TykmaGlobals.INISettings.NETTCPPORT);
            }
        }
    }
}
