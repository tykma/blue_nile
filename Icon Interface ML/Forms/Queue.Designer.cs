﻿namespace Icon_Interface.Forms
{
    partial class Queue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ux_tlp_Queue_Main = new System.Windows.Forms.TableLayoutPanel();
            this.ux_list_Queue = new System.Windows.Forms.ListBox();
            this.ux_tlp_QueueControls = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ux_pic_ok = new System.Windows.Forms.PictureBox();
            this.ux_lbl_run = new System.Windows.Forms.Label();
            this.ux_tlp_QueueDelete = new System.Windows.Forms.TableLayoutPanel();
            this.ux_pic_skip = new System.Windows.Forms.PictureBox();
            this.ux_label_unqueue = new System.Windows.Forms.Label();
            this.ux_tlp_clear = new System.Windows.Forms.TableLayoutPanel();
            this.ux_pic_clear = new System.Windows.Forms.PictureBox();
            this.ux_label_Clear = new System.Windows.Forms.Label();
            this.ux_tlp_Queue_Main.SuspendLayout();
            this.ux_tlp_QueueControls.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pic_ok)).BeginInit();
            this.ux_tlp_QueueDelete.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pic_skip)).BeginInit();
            this.ux_tlp_clear.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pic_clear)).BeginInit();
            this.SuspendLayout();
            // 
            // ux_tlp_Queue_Main
            // 
            this.ux_tlp_Queue_Main.ColumnCount = 1;
            this.ux_tlp_Queue_Main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ux_tlp_Queue_Main.Controls.Add(this.ux_list_Queue, 0, 0);
            this.ux_tlp_Queue_Main.Controls.Add(this.ux_tlp_QueueControls, 0, 1);
            this.ux_tlp_Queue_Main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_tlp_Queue_Main.Location = new System.Drawing.Point(0, 0);
            this.ux_tlp_Queue_Main.Name = "ux_tlp_Queue_Main";
            this.ux_tlp_Queue_Main.RowCount = 2;
            this.ux_tlp_Queue_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ux_tlp_Queue_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.ux_tlp_Queue_Main.Size = new System.Drawing.Size(184, 461);
            this.ux_tlp_Queue_Main.TabIndex = 0;
            // 
            // ux_list_Queue
            // 
            this.ux_list_Queue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_list_Queue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_list_Queue.FormattingEnabled = true;
            this.ux_list_Queue.ItemHeight = 20;
            this.ux_list_Queue.Location = new System.Drawing.Point(3, 3);
            this.ux_list_Queue.Name = "ux_list_Queue";
            this.ux_list_Queue.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.ux_list_Queue.Size = new System.Drawing.Size(178, 395);
            this.ux_list_Queue.TabIndex = 0;
            this.ux_list_Queue.SelectedIndexChanged += new System.EventHandler(this.ListQueueSelectedIndexChanged);
            this.ux_list_Queue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ListQueueKeyDown);
            // 
            // ux_tlp_QueueControls
            // 
            this.ux_tlp_QueueControls.BackColor = System.Drawing.Color.Transparent;
            this.ux_tlp_QueueControls.ColumnCount = 4;
            this.ux_tlp_QueueControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_tlp_QueueControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_tlp_QueueControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_tlp_QueueControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_tlp_QueueControls.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.ux_tlp_QueueControls.Controls.Add(this.ux_tlp_QueueDelete, 0, 0);
            this.ux_tlp_QueueControls.Controls.Add(this.ux_tlp_clear, 3, 0);
            this.ux_tlp_QueueControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_tlp_QueueControls.Location = new System.Drawing.Point(3, 404);
            this.ux_tlp_QueueControls.Name = "ux_tlp_QueueControls";
            this.ux_tlp_QueueControls.RowCount = 1;
            this.ux_tlp_QueueControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_tlp_QueueControls.Size = new System.Drawing.Size(178, 54);
            this.ux_tlp_QueueControls.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.ux_pic_ok, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ux_lbl_run, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(47, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(38, 48);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // ux_pic_ok
            // 
            this.ux_pic_ok.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_pic_ok.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_pic_ok.Image = global::Icon_Interface.Properties.ImageResources.StatusAnnotations_Complete_and_ok_32xLG_color1;
            this.ux_pic_ok.Name = "ux_pic_ok";
            this.ux_pic_ok.Size = new System.Drawing.Size(32, 27);
            this.ux_pic_ok.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_pic_ok.TabIndex = 7;
            this.ux_pic_ok.TabStop = false;
            this.ux_pic_ok.Click += new System.EventHandler(this.PicOkClick);
            // 
            // ux_lbl_run
            // 
            this.ux_lbl_run.AutoSize = true;
            this.ux_lbl_run.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_lbl_run.Location = new System.Drawing.Point(3, 33);
            this.ux_lbl_run.Name = "ux_lbl_run";
            this.ux_lbl_run.Size = new System.Drawing.Size(32, 15);
            this.ux_lbl_run.TabIndex = 8;
            this.ux_lbl_run.Text = "Run";
            this.ux_lbl_run.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ux_tlp_QueueDelete
            // 
            this.ux_tlp_QueueDelete.ColumnCount = 1;
            this.ux_tlp_QueueDelete.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_tlp_QueueDelete.Controls.Add(this.ux_pic_skip, 0, 0);
            this.ux_tlp_QueueDelete.Controls.Add(this.ux_label_unqueue, 0, 1);
            this.ux_tlp_QueueDelete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_tlp_QueueDelete.Location = new System.Drawing.Point(3, 3);
            this.ux_tlp_QueueDelete.Name = "ux_tlp_QueueDelete";
            this.ux_tlp_QueueDelete.RowCount = 2;
            this.ux_tlp_QueueDelete.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.ux_tlp_QueueDelete.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.ux_tlp_QueueDelete.Size = new System.Drawing.Size(38, 48);
            this.ux_tlp_QueueDelete.TabIndex = 0;
            // 
            // ux_pic_skip
            // 
            this.ux_pic_skip.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_pic_skip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_pic_skip.Image = global::Icon_Interface.Properties.ImageResources.StatusAnnotations_Blocked_32xLG1;
            this.ux_pic_skip.Location = new System.Drawing.Point(3, 3);
            this.ux_pic_skip.Name = "ux_pic_skip";
            this.ux_pic_skip.Size = new System.Drawing.Size(32, 27);
            this.ux_pic_skip.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_pic_skip.TabIndex = 7;
            this.ux_pic_skip.TabStop = false;
            this.ux_pic_skip.Click += new System.EventHandler(this.PicSkipClick);
            // 
            // ux_label_unqueue
            // 
            this.ux_label_unqueue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_label_unqueue.Location = new System.Drawing.Point(3, 33);
            this.ux_label_unqueue.Name = "ux_label_unqueue";
            this.ux_label_unqueue.Size = new System.Drawing.Size(32, 15);
            this.ux_label_unqueue.TabIndex = 8;
            this.ux_label_unqueue.Text = "Skip";
            this.ux_label_unqueue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ux_tlp_clear
            // 
            this.ux_tlp_clear.ColumnCount = 1;
            this.ux_tlp_clear.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_tlp_clear.Controls.Add(this.ux_pic_clear, 0, 0);
            this.ux_tlp_clear.Controls.Add(this.ux_label_Clear, 0, 1);
            this.ux_tlp_clear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_tlp_clear.Location = new System.Drawing.Point(135, 3);
            this.ux_tlp_clear.Name = "ux_tlp_clear";
            this.ux_tlp_clear.RowCount = 2;
            this.ux_tlp_clear.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.ux_tlp_clear.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.ux_tlp_clear.Size = new System.Drawing.Size(40, 48);
            this.ux_tlp_clear.TabIndex = 2;
            // 
            // ux_pic_clear
            // 
            this.ux_pic_clear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_pic_clear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_pic_clear.Image = global::Icon_Interface.Properties.ImageResources.StatusAnnotations_Critical_32xLG_color1;
            this.ux_pic_clear.Location = new System.Drawing.Point(3, 3);
            this.ux_pic_clear.Name = "ux_pic_clear";
            this.ux_pic_clear.Size = new System.Drawing.Size(34, 27);
            this.ux_pic_clear.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_pic_clear.TabIndex = 7;
            this.ux_pic_clear.TabStop = false;
            this.ux_pic_clear.Click += new System.EventHandler(this.PicClearClick);
            // 
            // ux_label_Clear
            // 
            this.ux_label_Clear.AutoSize = true;
            this.ux_label_Clear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_label_Clear.Location = new System.Drawing.Point(3, 33);
            this.ux_label_Clear.Name = "ux_label_Clear";
            this.ux_label_Clear.Size = new System.Drawing.Size(34, 15);
            this.ux_label_Clear.TabIndex = 8;
            this.ux_label_Clear.Text = "Clear";
            this.ux_label_Clear.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Queue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(184, 461);
            this.Controls.Add(this.ux_tlp_Queue_Main);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(200, 150);
            this.Name = "Queue";
            this.Text = "Queue";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.QueueFormClosing);
            this.Load += new System.EventHandler(this.QueueLoad);
            this.ux_tlp_Queue_Main.ResumeLayout(false);
            this.ux_tlp_QueueControls.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pic_ok)).EndInit();
            this.ux_tlp_QueueDelete.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_pic_skip)).EndInit();
            this.ux_tlp_clear.ResumeLayout(false);
            this.ux_tlp_clear.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_pic_clear)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel ux_tlp_Queue_Main;
        private System.Windows.Forms.ListBox ux_list_Queue;
        private System.Windows.Forms.TableLayoutPanel ux_tlp_QueueControls;
        private System.Windows.Forms.TableLayoutPanel ux_tlp_QueueDelete;
        private System.Windows.Forms.PictureBox ux_pic_skip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox ux_pic_ok;
        private System.Windows.Forms.Label ux_lbl_run;
        private System.Windows.Forms.Label ux_label_unqueue;
        private System.Windows.Forms.TableLayoutPanel ux_tlp_clear;
        private System.Windows.Forms.PictureBox ux_pic_clear;
        private System.Windows.Forms.Label ux_label_Clear;
    }
}