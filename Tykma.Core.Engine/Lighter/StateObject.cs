﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StateObject.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// State object for receiving data from remote device.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.Lighter
{
    using System.Net.Sockets;
    using System.Text;

    /// <summary>
    /// State object for receiving data from remote device.
    /// </summary>
    internal class StateObject
    {
        /// <summary>
        /// Size of receive buffer.
        /// </summary>
        private const int ReceiveBufferSize = 256;

        /// <summary>
        /// Gets the size of the buffer.
        /// </summary>
        /// <value>
        /// The size of the buffer.
        /// </value>
        internal int BufferSize => ReceiveBufferSize;

        /// <summary>
        /// Gets or sets the work socket.
        /// </summary>
        /// <value>
        /// The work socket.
        /// </value>
        internal Socket WorkSocket { get; set; }

        /// <summary>
        /// Gets the buffer.
        /// </summary>
        /// <value>
        /// The buffer.
        /// </value>
        internal byte[] Buffer { get; } = new byte[ReceiveBufferSize];

        /// <summary>
        /// Gets the string builder..
        /// </summary>
        /// <value>
        /// The string builder.
        /// </value>
        internal StringBuilder Sb { get; } = new StringBuilder();
    }
}
