﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.DYNAMICGFX.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//  Dynamic graphics import portion of the UI.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using Icon_Interface.General;

    using Tykma.Core.DynamicFile;

    /// <summary>
    /// Here we will handle the dynamic file importer library.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// The GFX replacer.
        /// </summary>
        private FileReplacer gfxReplacer;

        /// <summary>
        /// Initializes the dynamic importer.
        /// </summary>
        private void InitializeDynamicImporter()
        {
            if (!TykmaGlobals.INISettings.USEDYNAMICIMPORT)
            {
                return;
            }

            // Let's get a list of available extensions.
            List<string> extensions = TykmaGlobals.INISettings.DYNAMICEXTENSIONS.Split(',').ToList();

            string sourcePath = TykmaGlobals.INISettings.DYNAMICSRCFOLDER;

            FileInfo destFile;

            if (TykmaGlobals.INISettings.DYNAMICDSTFILE != string.Empty)
            {
                destFile = new FileInfo(TykmaGlobals.INISettings.DYNAMICDSTFILE);
            }
            else
            {
                Logger.Error("Import Destination file empty - not continuing");
                return;
            }

            int pollRate = TykmaGlobals.INISettings.DYNAMICIMPORTPOLLRATE;

            if (pollRate == 0)
            {
                pollRate = 500;
            }

            this.gfxReplacer = new FileReplacer(sourcePath, destFile, extensions, pollRate);

            this.gfxReplacer.FileSearchEvent += this.FileFoundEvent;
        }

        /// <summary>
        /// File found event.
        /// </summary>
        /// <param name="o">The object.</param>
        /// <param name="e">The event.</param>
        private void FileFoundEvent(object o, DynamicFileEventState e)
        {
            this.InvokeIfRequired(() =>
                 {
                     switch (e.State)
                     {
                         case DynamicFileEventState.DynamicState.Error:
                             this.EnableGfxReplacer(false);
                             this.TimeStampEvent(string.Format("File copy error: {0}", e.Error), true);
                             Logger.Error("File copy error: {0}", e.Error);
                             break;
                         case DynamicFileEventState.DynamicState.FoundFile:
                             this.TimeStampEvent(string.Format("Found a graphics file: {0}", e.FileName));
                             Logger.Info("Found a graphics file: {0}", e.FileName);
                             break;
                         case DynamicFileEventState.DynamicState.MoveCompleted:
                             this.EnableGfxReplacer(true);
                             this.TimeStampEvent(string.Format("Moved a graphics file: {0}", e.FileName));
                             Logger.Info("Moved a graphics file: {0}", e.FileName);
                             break;
                     }
                 });
        }

        /// <summary>
        /// Handles the Click event of the ToolStrip_GFXiStart control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ToolStripGfXiStartClick(object sender, EventArgs e)
        {
            this.EnableGfxReplacer(true);
        }

        /// <summary>
        /// Handles the Click event of the ToolStrip_GFXiStop control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ToolStripGfXiStopClick(object sender, EventArgs e)
        {
            this.EnableGfxReplacer(false);
        }

        /// <summary>
        /// Enables the GFX replacer.
        /// </summary>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        private void EnableGfxReplacer(bool enable)
        {
            if (this.gfxReplacer != null)
            {
                this.gfxReplacer.Enabled(enable);

                this.TimeStampEvent(enable ? "Graphics replacer started" : "Graphics replacer stopped");
            }
        }
    }
}
