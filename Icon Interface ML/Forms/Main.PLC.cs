﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.PLC.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   A connector between a PLC library (INGEAR) and the user interface form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Reflection;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Icon_Interface.General;
    using Icon_Interface.Properties;
    using Tykma.Core.PLC;
    using Tykma.Core.PLC.AB;
    using Tykma.Core.PLC.Siemens;
    using Tykma.Core.StringExtensions;
    using Tykma.Custom.PLC.Simulator;
    using Tykma.Icon.PLC.MessagesDB;
    using Tykma.Interface.NG.DialogService;

    /// <summary>
    /// PLC code.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// The TYKMA PLC class.
        /// </summary>
        private IPLCInterface tykmaPlc;

        /// <summary>
        /// The PLC was manually disconnected.
        /// </summary>
        private bool plcManuallyDisconnected;

        /// <summary>
        /// Gets or sets the PLC message database.
        /// </summary>
        private MessagesDB PLCMessagesDatabase { get; set; }

        /// <summary>
        /// Gets or sets PLC status form.
        /// </summary>
        private IconPLCDialogService PLCDialogService { get; set; }

        /// <summary>
        /// Shows the PLC initialization failure.
        /// </summary>
        /// <param name="error">The error.</param>
        private void ShowPLCInitializationFailure(string error)
        {
            MessageBox.Show(
              error,
              Resources.Msg_PLC_Error_Initializing,
              MessageBoxButtons.OK,
              MessageBoxIcon.Exclamation,
              MessageBoxDefaultButton.Button1);

            Logger.Error("{0}: {1}", Resources.Msg_PLC_Error_Initializing, error);
        }

        /// <summary>
        /// Initialize the PLC.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void InitializePLC()
        {
            if (TykmaGlobals.PLCSettings.PLCENABLED)
            {
                if (TykmaGlobals.PLCSettings.PLCTYPE == 0)
                {
                    try
                    {
                        this.tykmaPlc = new IngearABLink(
                            TykmaGlobals.PLCSettings.PLCCONNECTIONTIMEOUT,
                            TykmaGlobals.PLCSettings.PLCMAINSCANTIME,
                            TykmaGlobals.PLCSettings.PLCSECONDARYSCANTIME);
                    }
                    catch (Exception exc)
                    {
                        this.ShowPLCInitializationFailure(exc.Message);
                    }
                }
                else if (TykmaGlobals.PLCSettings.PLCTYPE == 1)
                {
                    try
                    {
                        this.tykmaPlc = new IngearLogix(
                            TykmaGlobals.PLCSettings.PLCCONNECTIONTIMEOUT,
                            TykmaGlobals.PLCSettings.PLCMAINSCANTIME,
                            TykmaGlobals.PLCSettings.PLCSECONDARYSCANTIME,
                            EPLCType.ABLogix);
                    }
                    catch (Exception exc)
                    {
                        this.ShowPLCInitializationFailure(exc.Message);
                    }
                }
                else if (TykmaGlobals.PLCSettings.PLCTYPE == 2)
                {
                    try
                    {
                        this.tykmaPlc = new IngearS7(
                            TykmaGlobals.PLCSettings.PLCCONNECTIONTIMEOUT,
                            TykmaGlobals.PLCSettings.PLCMAINSCANTIME,
                            TykmaGlobals.PLCSettings.PLCSECONDARYSCANTIME,
                            EPLCType.SiemensS7_1200);
                    }
                    catch (Exception exc)
                    {
                        this.ShowPLCInitializationFailure(exc.Message);
                    }
                }
                else if (TykmaGlobals.PLCSettings.PLCTYPE == 3)
                {
                    try
                    {
                        this.tykmaPlc = new IngearLogix(
                            TykmaGlobals.PLCSettings.PLCCONNECTIONTIMEOUT,
                            TykmaGlobals.PLCSettings.PLCMAINSCANTIME,
                            TykmaGlobals.PLCSettings.PLCSECONDARYSCANTIME,
                            EPLCType.Micro);
                    }
                    catch (Exception exc)
                    {
                        this.ShowPLCInitializationFailure(exc.Message);
                    }
                }

                if (this.tykmaPlc != null)
                {
                    this.tykmaPlc.ConnectionStatusEvent += this.PlcConnectedEvent;
                    this.tykmaPlc.TagChangeEvent += this.PLCTagChangeEvent;
                    if (TykmaGlobals.PLCSettings.PLCCONNECTIONTIMEOUT <= 0)
                    {
                        TykmaGlobals.PLCSettings.PLCCONNECTIONTIMEOUT = 3000;
                    }

                    this.ux_Timer_ReconnectToPLC.Interval = TykmaGlobals.PLCSettings.PLCCONNECTIONTIMEOUT * 3;

                    if (!string.IsNullOrWhiteSpace(TykmaGlobals.PLCSettings.PLCTAGSTATUS))
                    {
                        this.PLCDialogService = new IconPLCDialogService(Settings.Default.PLCDialogInitialLocation);
                        this.PLCMessagesDatabase = new MessagesDB(@"C:\tykma\custom\tykma_icon\messages.csv");
                        this.PLCMessagesDatabase.LoadDB();
                    }
                }
            }
        }

        /// <summary>
        /// Begin the PLC connection.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void ConnectToPLC()
        {
            this.ConfigureDataTags();

            // Tag view for PLC.
            this.ConfigureTagView();
            this.plcManuallyDisconnected = false;
            if (this.tykmaPlc != null)
            {
                this.tykmaPlc.Connect();
            }
        }

        /// <summary>
        /// Configures the data tags.
        /// </summary>
        private void ConfigureDataTags()
        {
        }

        /// <summary>
        /// Disconnects from PLC.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private void DisconnectFromPLC()
        {
            this.tykmaPlc.Disconnect();
            this.ux_PLC_TagView.Clear();
            this.TimeStampEvent("Disconnected from PLC");
            Logger.Info("Disconnected from PLC");

            this.InvokeIfRequired(
                () =>
                    {
                        this.ShowPLCConnectionStatus(this.tykmaPlc.Connected);
                    });
        }

        /// <summary>
        /// Overloaded timestamp for thread safe reporting.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private async void PlcConnectedEvent(object sender, ConnectionStatusMessage e)
        {
            if (!TykmaGlobals.PLCSettings.PLCENABLED)
            {
                return;
            }

            if (e.Connected)
            {
                this.TimeStampEvent(string.Format("PLC Connected: {0}", e.ConnectedMessage));
                Logger.Info("PLC Connected: {0}", e.ConnectedMessage);
                if (!await this.AddTagToPLC(TykmaGlobals.PLCSettings.PLCTAGSTART, true, typeof(bool)))
                {
                    this.TimeStampEvent(string.Format("Could not add start tag {0}", TykmaGlobals.PLCSettings.PLCTAGSTART), true);
                    Logger.Fatal("Could not add start tag: {0}", TykmaGlobals.PLCSettings.PLCTAGSTART);
                }

                await Task.Delay(100);

                if (!await this.AddTagToPLC(TykmaGlobals.PLCSettings.PLCDATAREADYTAG, true, typeof(bool)))
                {
                    this.TimeStampEvent(string.Format("Could not add data ready tag {0}", TykmaGlobals.PLCSettings.PLCDATAREADYTAG), true);
                    Logger.Fatal("Could not add data ready tag : {0}", TykmaGlobals.PLCSettings.PLCDATAREADYTAG);
                }

                await Task.Delay(100);

                if (TykmaGlobals.PLCSettings.PLCENABLEDATATAGS)
                {
                    var obj1 = new PLCDataObject();
                    obj1.EntityName = TykmaGlobals.PLCSettings.DATATAG1PROJECTID;
                    obj1.EchoTag = TykmaGlobals.PLCSettings.DATATAG1ECHONAME;
                    obj1.Tag = TykmaGlobals.PLCSettings.DATATAG1NAME;

                    var obj2 = new PLCDataObject();
                    obj2.EntityName = TykmaGlobals.PLCSettings.DATATAG2PROJECTID;
                    obj2.EchoTag = TykmaGlobals.PLCSettings.DATATAG2ECHONAME;
                    obj2.Tag = TykmaGlobals.PLCSettings.DATATAG2NAME;

                    var obj3 = new PLCDataObject();
                    obj3.EntityName = TykmaGlobals.PLCSettings.DATATAG3PROJECTID;
                    obj3.EchoTag = TykmaGlobals.PLCSettings.DATATAG3ECHONAME;
                    obj3.Tag = TykmaGlobals.PLCSettings.DATATAG3NAME;

                    var listofItems = new List<PLCDataObject>
                                          {
                                              obj1, obj2, obj3,
                                          };

                    foreach (var item in listofItems)
                    {
                        if (item != null)
                        {
                            await Task.Delay(100);

                            if (!await this.AddTagToPLC(item.Tag, false, typeof(string)))
                            {
                                this.TimeStampEvent(string.Format("Could not add data tag {0}", item.Tag), true);
                                Logger.Fatal("Could not add data tag : {0}", item.Tag);
                            }
                        }
                    }
                }

                await Task.Delay(100);
                if (TykmaGlobals.PLCSettings.TEMPLATE_TAG != null)
                {
                    if (!await this.AddTagToPLC(TykmaGlobals.PLCSettings.TEMPLATE_TAG, false, typeof(string)))
                    {
                        this.TimeStampEvent(
                            string.Format("Could not add data tag {0}", TykmaGlobals.PLCSettings.TEMPLATE_TAG),
                            true);
                        Logger.Fatal("Could not add data tag : {0}", TykmaGlobals.PLCSettings.TEMPLATE_TAG);
                    }
                }

                await Task.Delay(100);

                if (TykmaGlobals.PLCSettings.PLCHEARTBEATTAG != string.Empty)
                {
                    if (!await this.AddTagToPLC(TykmaGlobals.PLCSettings.PLCHEARTBEATTAG, true, typeof(bool)))
                    {
                        this.TimeStampEvent(string.Format("Could not add heartbeat tag {0}", TykmaGlobals.PLCSettings.PLCHEARTBEATTAG), true);
                        Logger.Fatal("Could not add heartbeat tag : {0}", TykmaGlobals.PLCSettings.PLCHEARTBEATTAG);
                    }
                }

                await Task.Delay(100);

                if (!await this.AddTagToPLC(TykmaGlobals.PLCSettings.PLCTAGSTATUS, true, typeof(short)))
                {
                    this.TimeStampEvent(string.Format("Could not add status tag {0}", TykmaGlobals.PLCSettings.PLCTAGSTATUS), true);
                    Logger.Fatal("Could not add status: {0}", TykmaGlobals.PLCSettings.PLCTAGSTATUS);
                }
            }
            else
            {
                this.TimeStampEvent(string.Format("PLC not connected: {0}", e.ConnectedMessage));
                Logger.Fatal("PLC not connected: {0}", e.ConnectedMessage);
            }

            this.InvokeIfRequired(
                () =>
                    {
                        this.ShowPLCConnectionStatus(e.Connected);
                    });
        }

        /// <summary>
        /// Shows the PLC connection status.
        /// </summary>
        /// <param name="online">if set to <c>true</c> [online].</param>
        // ReSharper disable once InconsistentNaming
        private void ShowPLCConnectionStatus(bool online)
        {
            if (online)
            {
                this.ux_Status_PLC.Text = string.Format("PLC: {0}", Resources.Msg_Online);
                this.ux_Status_PLC.ForeColor = Color.Green;
            }
            else
            {
                this.ux_Status_PLC.Text = string.Format("PLC: {0}", Resources.Msg_Offline);
                this.ux_Status_PLC.ForeColor = Color.Red;
                this.ux_PLC_TagView.Clear();
            }
        }

        /// <summary>
        /// Handles the Tick event of the TimerCheckForPLCDisconnect control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void TimerCheckForPLCDisconnectTick(object sender, EventArgs e)
        {
            if (this.tykmaPlc != null)
            {
                if (!this.tykmaPlc.Connected)
                {
                    // We will try to re-connect, unless there was a purposeful disconnection.
                    // Connect to the PLC if this is a PLC enabled configuration.
                    if (TykmaGlobals.PLCSettings.PLCENABLED)
                    {
                        if (!this.plcManuallyDisconnected)
                        {
                            this.TimeStampEvent("Attempting to re-establish connection to PLC");
                            Logger.Info("Attempting to re-establish connection to PLC");
                            this.ConnectToPLC();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Event: PLC tag added.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void PLCTagChangeEvent(object sender, TagValues e)
        {
            if (!TykmaGlobals.PLCSettings.PLCENABLED)
            {
                return;
            }

            this.InvokeIfRequired(
                () =>
                {
                    TagValues theTag = e;

                    var itemToAdd =
                        new ListViewItem(
                            new[]
                                        {
                                            theTag.Name,
                                            theTag.Value,
                                            theTag.NetType,
                                            theTag.QualityString,
                                            theTag.TimeStamp,
                                        })
                        { Name = theTag.Name };

                    bool exists = false;

                    this.ux_PLC_TagView.BeginUpdate();
                    foreach (ListViewItem item in this.ux_PLC_TagView.Items)
                    {
                        if (item.Name == itemToAdd.Name)
                        {
                            exists = true;
                            item.SubItems[1].Text = theTag.Value;
                            item.SubItems[3].Text = theTag.QualityString;
                            item.SubItems[4].Text = theTag.TimeStamp;
                        }
                    }

                    if (!exists)
                    {
                        this.ux_PLC_TagView.Items.Add(itemToAdd);
                        this.SizeTagViewColumns();
                    }

                    this.ux_PLC_TagView.EndUpdate();

                    var tagName = theTag.Name.ToUpper();
                    if (tagName == TykmaGlobals.PLCSettings.PLCTAGSTART.ToUpper())
                    {
                        if (theTag.Value.TryGetBool())
                        {
                            this.StartSequence();
                        }
                    }
                    else if (tagName == TykmaGlobals.PLCSettings.PLCDATAREADYTAG.ToUpper())
                    {
                        if (theTag.Value.TryGetBool())
                        {
                            this.TimeStampEvent("Data ready tag received");
                            Logger.Info("Data ready tag received");
                            this.DataReadyReceived();
                        }
                    }
                    else if (tagName == TykmaGlobals.PLCSettings.PLCHEARTBEATTAG.ToUpper())
                    {
                        if (theTag.Value.TryGetBool())
                        {
                            this.BeatHeartbeat();
                        }
                    }
                    else if (tagName == TykmaGlobals.PLCSettings.PLCTAGSTATUS.ToUpper())
                    {
                        this.TimeStampEvent($"Got status code from PLC: {theTag.Value}");

                        if (int.TryParse(theTag.Value, out int result))
                        {
                            this.HandleStatusTag(result);
                        }
                    }
                });
        }

        private void HandleStatusTag(int statusCode)
        {
            var message = new PLCMessageItem();

            if (this.PLCMessagesDatabase != null)
            {
                message = this.PLCMessagesDatabase.GetMessage(statusCode);
            }
            else
            {
                message.Code = statusCode;
            }

            this.PLCDialogService?.ShowMessage(message.Message, "PLC Status", statusCode.ToString(), message.Color);
        }

        /// <summary>
        /// Beats the heartbeat.
        /// </summary>
        private async void BeatHeartbeat()
        {
            try
            {
                // We'll toggle the heart beat off.
                await this.tykmaPlc.WriteTag<bool>(TykmaGlobals.PLCSettings.PLCHEARTBEATTAG, false);
            }
            catch (Exception err)
            {
                Logger.Error($"Writing heartbeat tag exception: {err.Message}");
                this.TimeStampEvent($"Writing heartbeat tag exception: {err.Message}", true);
            }
        }

        /// <summary>
        /// Handle a data ready received event.
        /// </summary>
        private async void DataReadyReceived()
        {
            var obj1 = new PLCDataObject();
            obj1.EntityName = TykmaGlobals.PLCSettings.DATATAG1PROJECTID;
            obj1.EchoTag = TykmaGlobals.PLCSettings.DATATAG1ECHONAME;
            obj1.Tag = TykmaGlobals.PLCSettings.DATATAG1NAME;

            var obj2 = new PLCDataObject();
            obj2.EntityName = TykmaGlobals.PLCSettings.DATATAG2PROJECTID;
            obj2.EchoTag = TykmaGlobals.PLCSettings.DATATAG2ECHONAME;
            obj2.Tag = TykmaGlobals.PLCSettings.DATATAG2NAME;

            var obj3 = new PLCDataObject();
            obj3.EntityName = TykmaGlobals.PLCSettings.DATATAG3PROJECTID;
            obj3.EchoTag = TykmaGlobals.PLCSettings.DATATAG3ECHONAME;
            obj3.Tag = TykmaGlobals.PLCSettings.DATATAG3NAME;

            var listofItems = new List<PLCDataObject>
                                          {
                                              obj1, obj2, obj3,
                                          };

            // Let's do the job load.
            var tName = TykmaGlobals.PLCSettings.TEMPLATE_TAG;
            string templateToLoad = this.tykmaPlc.TagReadUnimportant(tName).TrimEnd('\0');

            // Let's try to load the job.
            if (await this.LoadJobAbstractor(templateToLoad))
            {
                // Let's echo our job back.
                try
                {
                    await this.tykmaPlc.WriteTag<string>(TykmaGlobals.PLCSettings.TEMPLATE_ECHO_TAG, templateToLoad);
                }
                catch (Exception err)
                {
                    this.TimeStampEvent(err.Message, true);
                }
            }

            if (this.markingJobState.Loaded && TykmaGlobals.PLCSettings.PLCENABLEDATATAGS)
            {
                foreach (var plcdataitem in listofItems)
                {
                    string data = this.tykmaPlc.TagReadUnimportant(plcdataitem.Tag).TrimEnd('\0');

                    if (await this.ChangeTextByName(plcdataitem.EntityName, data))
                    {
                        this.TimeStampEvent(string.Format("Changed {0} to {1}", plcdataitem.Tag, data));
                        try
                        {
                            await this.tykmaPlc.WriteTag<string>(plcdataitem.EchoTag, data);
                        }
                        catch (ArgumentOutOfRangeException e)
                        {
                            Logger.Error(e);
                            this.TimeStampEvent(e.Message, true);
                        }
                    }
                    else
                    {
                        this.TimeStampEvent($"PLC - could not change {plcdataitem.EntityName} to {data}", true);
                        Logger.Error($"PLC - could not change {plcdataitem.EntityName} to {data}");
                    }
                }
            }

            this.UpdatePreview();
            this.InitDataGridView();

            await this.tykmaPlc.WriteTag<bool>(TykmaGlobals.PLCSettings.PLCDATAREADYTAG, false);
        }

        /// <summary>
        /// Adds the tag to PLC.
        /// </summary>
        /// <param name="tagName">Name of the tag.</param>
        /// <param name="importantGroup">if set to <c>true</c> [important group].</param>
        /// <param name="type">Tag type.</param>
        /// <returns>
        /// Success state.
        /// </returns>
        // ReSharper disable once InconsistentNaming
        private async Task<bool> AddTagToPLC(string tagName, bool importantGroup, Type type)
        {
            if (TykmaGlobals.PLCSettings.PLCENABLED)
            {
                try
                {
                    if (type == typeof(bool))
                    {
                        return await this.tykmaPlc.AutoTagAdd<bool>(tagName, importantGroup, TagTypes.ETagType.Boolean);
                    }
                    else if (type == typeof(string))
                    {
                        return await this.tykmaPlc.AutoTagAdd<string>(tagName, importantGroup, TagTypes.ETagType.String);
                    }
                    else if (type == typeof(short))
                    {
                        return await this.tykmaPlc.AutoTagAdd<short>(tagName, importantGroup, TagTypes.ETagType.Integer);
                    }
                    else if (type == typeof(int))
                    {
                        return await this.tykmaPlc.AutoTagAdd<int>(tagName, importantGroup, TagTypes.ETagType.Integer);
                    }
                    else
                    {
                        return await this.tykmaPlc.AutoTagAdd<object>(tagName, importantGroup, TagTypes.ETagType.Boolean);
                    }
                }
                catch (Exception exc)
                {
                    this.TimeStampEvent(string.Format("Tag add fault: {0}, {1}", tagName, exc.Message), true);
                    Logger.Error(string.Format("Tag add fault: {0}, {1}", tagName, exc.Message));
                }
            }

            return false;
        }

        /// <summary>
        /// Configures the tag view.
        /// </summary>
        private void ConfigureTagView()
        {
            if (!TykmaGlobals.PLCSettings.PLCENABLED)
            {
                return;
            }

            this.ux_PLC_TagView.View = View.Details;

            this.ux_PLC_TagView.Columns.Add("Tag Name", 1, HorizontalAlignment.Left);
            this.ux_PLC_TagView.Columns.Add("Value", 1, HorizontalAlignment.Left);
            this.ux_PLC_TagView.Columns.Add("Data Type", 1, HorizontalAlignment.Left);
            this.ux_PLC_TagView.Columns.Add("Quality", 1, HorizontalAlignment.Left);
            this.ux_PLC_TagView.Columns.Add("Time Stamp", 1, HorizontalAlignment.Left);
        }

        /// <summary>
        /// Auto sizes the tag view columns.
        /// </summary>
        private void SizeTagViewColumns()
        {
            this.ux_PLC_TagView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            this.ux_PLC_TagView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        /// <summary>
        /// Write tag button pushed.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void ButtonWriteTagClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(this.ux_PLC_WriteTagName.Text))
            {
                try
                {
                    bool success = false;

                    var si = this.ux_cb_Plc_Tag_Type.SelectedItem;
                    if (si != null)
                    {
                        if (si.Equals("BOOL"))
                        {
                            var val = this.ux_PLC_WriteTagValue.Text.TryGetBool();

                            success = await this.tykmaPlc.WriteTag<bool>(this.ux_PLC_WriteTagName.Text, val);
                        }
                        else if (si.Equals("STRING"))
                        {
                            success = await this.tykmaPlc.WriteTag<string>(this.ux_PLC_WriteTagName.Text, this.ux_PLC_WriteTagValue.Text);
                        }
                        else if (si.Equals("SHORT"))
                        {
                            short val = 0;
                            short.TryParse(this.ux_PLC_WriteTagValue.Text, out val);
                            success = await this.tykmaPlc.WriteTag<short>(this.ux_PLC_WriteTagName.Text, val);
                        }
                        else if (si.Equals("INT"))
                        {
                            int val = 0;
                            int.TryParse(this.ux_PLC_WriteTagValue.Text, out val);
                            success = await this.tykmaPlc.WriteTag<int>(this.ux_PLC_WriteTagName.Text, val);
                        }
                    }

                    if (success)
                    {
                        this.TimeStampEvent("Tag write success");
                        Logger.Trace("Successfully wrote tag");
                    }
                    else
                    {
                        this.TimeStampEvent("Tag write fault", true);
                        Logger.Trace("Could not write tag");
                    }
                }
                catch (Exception exc)
                {
                    this.TimeStampEvent(string.Format("Tag write exception: {0}", exc.Message), true);
                    Logger.Error("Tag write exception: {0}", exc.Message);
                }
            }
        }

        /// <summary>
        /// Selected a tag in the tag view.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void ListTagViewSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ux_PLC_TagView.SelectedItems.Count <= 0)
            {
                return;
            }

            string tagName = this.ux_PLC_TagView.SelectedItems[0].SubItems[0].Text;
            string tagType = this.ux_PLC_TagView.SelectedItems[0].SubItems[2].Text;

            var t = Type.GetType(tagType);

            if (t == typeof(short))
            {
                this.ux_cb_Plc_Tag_Type.SelectedItem = "SHORT";
            }
            else if (t == typeof(bool))
            {
                this.ux_cb_Plc_Tag_Type.SelectedItem = "BOOL";
            }
            else if (t == typeof(int))
            {
                this.ux_cb_Plc_Tag_Type.SelectedItem = "INT";
            }
            else if (t == typeof(string))
            {
                this.ux_cb_Plc_Tag_Type.SelectedItem = "STRING";
            }

            this.ux_PLC_WriteTagName.Text = tagName;
        }

        /// <summary>
        /// Change PLC IP and timeouts.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void ButtonChangeIpClick(object sender, EventArgs e)
        {
            TykmaGlobals.PLCSettings.PLCIP = this.ux_PLC_IP.Text;
            TykmaGlobals.PLCSettings.PLCMAINSCANTIME = (int)this.ux_PLC_ScanTime.Value;
            TykmaGlobals.PLCSettings.PLCSECONDARYSCANTIME = (int)this.ux_PLC_ScanTimeSecondary.Value;
        }

        /// <summary>
        /// Handles the Click event of the ButtonAddTag control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private async void ButtonAddTagClick(object sender, EventArgs e)
        {
            bool tagOk = false;
            var si = this.ux_cb_plc_tag_add_type.SelectedItem;
            if (si != null)
            {
                if (si.Equals("BOOL"))
                {
                    tagOk = await this.AddTagToPLC(this.ux_PLC_AddTagName.Text, false, typeof(bool));
                }
                else if (si.Equals("STRING"))
                {
                    tagOk = await this.AddTagToPLC(this.ux_PLC_AddTagName.Text, false, typeof(string));
                }
                else if (si.Equals("SHORT"))
                {
                    tagOk = await this.AddTagToPLC(this.ux_PLC_AddTagName.Text, false, typeof(short));
                }
                else if (si.Equals("INT"))
                {
                    tagOk = await this.AddTagToPLC(this.ux_PLC_AddTagName.Text, false, typeof(int));
                }
            }
            else
            {
                tagOk = await this.AddTagToPLC(this.ux_PLC_AddTagName.Text, false, null);
            }

            this.TimeStampEvent(
                                tagOk
                                ? string.Format("Succesfully added tag: {0}", this.ux_PLC_AddTagName.Text)
                                : string.Format("Could not add tag: {0}", this.ux_PLC_AddTagName.Text),
                                !tagOk);

            if (tagOk)
            {
                Logger.Debug("Added tag {0}", this.ux_PLC_AddTagName.Text);
            }
            else
            {
                Logger.Fatal("Could not add tag: {0}");
            }
        }
    }
}
