﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlobalsBase26Test.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.Globals
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Icon.Globals;

    /// <summary>
    /// The globals base 26 test.
    /// </summary>
    [TestClass]
    public class GlobalsBase26Test
    {
        /// <summary>
        /// The test base 26.
        /// </summary>
        [TestMethod]
        public void TestBase26()
        {
            ISerializer a = new SerialBase26();

            Assert.AreEqual("A", a.GetValue(1));
            Assert.AreEqual("Y", a.GetValue(25));
            Assert.AreEqual("Z", a.GetValue(26));
            Assert.AreEqual("AA", a.GetValue(27));
            Assert.AreEqual("WIKIPEDIA", a.GetValue(4878821185187));
        }

        /// <summary>
        /// Test global serializer.
        /// </summary>
        [TestMethod]
        public void GlobalSerializerStringTest()
        {
            var serializer = new SerialBase26();

            Assert.AreEqual("A", serializer.GetValue(1));

            Assert.AreEqual("A", serializer.GetValue("1"));

            Assert.AreEqual(string.Empty, serializer.GetValue("9999999999999999999999999999999999"));
        }
    }
}
