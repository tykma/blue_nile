﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdParamPenGet.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Retrieves parameters of a pen from a minilase pro SE projects.
//   The pen parameters include all the laser settings like frequency and power.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;

    using CustomExceptions;

    /// <summary>
    /// The command to get pen parameters.
    /// </summary>
    internal class CmdParamPenGet : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdParamPenGet" /> class.
        /// </summary>
        /// <param name="lmcApi">The API.</param>
        /// <param name="pen">The pen.</param>
        public CmdParamPenGet(IntPtr lmcApi, int pen)
            : base(lmcApi)
        {
            this.Pen = pen;
        }

        private delegate int LMC1ApiGetPenParam(
            int nPenNo,
            ref int nMarkLoop,
            ref double dMarkSpeed,
            ref double powerRatio,
            ref double dCurrent,
            [MarshalAs(UnmanagedType.I2)]ref short nFreq,
            ref int nQPulseWidth,
            ref int nStartTC,
            ref int nLaserOffTC,
            ref int nEndTC,
            ref int nPolyTC,
            ref double dJumpSpeed,
            ref int nJumpPosTC,
            ref int nJumpDistTC,
            ref double dEndComp,
            ref double dAccDist,
            ref double dPointTime,
            ref bool bPulsePointMode,
            ref int nPulseNum,
            ref double dFlySpeed);

        /// <summary>
        /// Gets the pen.
        /// </summary>
        private int Pen { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        /// <exception cref="Tykma.Core.Engine.CustomExceptions.MethodFaultException">Problem executing command.</exception>
        internal PenParameter Execute()
        {
            var lmc1GetPenParam = this.LoadFunction<LMC1ApiGetPenParam>("lmc1_GetPenParam");

            if (lmc1GetPenParam == null)
            {
                throw new EngineNotConnectedException();
            }

            var getPen = new PenParameter();

            var nMarkLoop = getPen.MarkLoop;
            var dMarkSpeed = getPen.MarkSpeed;
            var powerRatio = getPen.PowerRatio;
            var nFreq = getPen.Frequency;
            var dCurrent = getPen.Current;
            var nQPulseWidth = getPen.QPulseWidth;
            var nStartTC = getPen.StartTC;
            var nLaserOffTC = getPen.LaserOffTC;
            var nEndTC = getPen.EndTC;
            var nPolyTC = getPen.PolyTC;
            var dJumpSpeed = getPen.JumpSpeed;
            var nJumpPosTC = getPen.JumpPositionTC;
            var nJumpDistTC = getPen.JumpDistanceTC;
            var dEndComp = getPen.EndCompensate;
            var dAccDist = getPen.AcccelerationDistance;
            var dPointTime = getPen.PointTime;
            var bPulsePointMode = getPen.PulsePointMode;
            var nPulseNum = getPen.PulseCount;
            var dFlySpeed = getPen.FlySpeed;

            int result = lmc1GetPenParam(
                this.Pen,
                ref nMarkLoop,
                ref dMarkSpeed,
                ref powerRatio,
                ref dCurrent,
                ref nFreq,
                ref nQPulseWidth,
                ref nStartTC,
                ref nLaserOffTC,
                ref nEndTC,
                ref nPolyTC,
                ref dJumpSpeed,
                ref nJumpPosTC,
                ref nJumpDistTC,
                ref dEndComp,
                ref dAccDist,
                ref dPointTime,
                ref bPulsePointMode,
                ref nPulseNum,
                ref dFlySpeed);

            var rm = this.GetMiniLaseReturnCode(result);

            if (rm.Success)
            {
                getPen.MarkLoop = nMarkLoop;
                getPen.MarkSpeed = dMarkSpeed;
                getPen.PowerRatio = powerRatio;
                getPen.Current = dCurrent;
                getPen.Frequency = nFreq;
                getPen.QPulseWidth = nQPulseWidth;
                getPen.StartTC = nStartTC;
                getPen.LaserOffTC = nLaserOffTC;
                getPen.EndTC = nEndTC;
                getPen.PolyTC = nPolyTC;
                getPen.JumpSpeed = dJumpSpeed;
                getPen.JumpPositionTC = nJumpPosTC;
                getPen.JumpDistanceTC = nJumpDistTC;
                getPen.EndCompensate = dEndComp;
                getPen.AcccelerationDistance = dAccDist;
                getPen.PointTime = dPointTime;
                getPen.PulsePointMode = bPulsePointMode;
                getPen.PulseCount = nPulseNum;
                getPen.FlySpeed = dFlySpeed;

                return getPen;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
