﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TCPParserTest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.HTTPD
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Icon.TCPCommand;

    /// <summary>
    /// The TCP parser test.
    /// </summary>
    [TestClass]
    public class TcpParserTest
    {
        /// <summary>
        /// Tests the job status.
        /// </summary>
        [TestMethod]
        public void TestJobStatus()
        {
            var listofPossibleCommands = new List<string>
                                             {
                                                 "job_status",
                                                 "job_status?111=0",
                                                 "JOB_STATUS?111=0",
                                                 "job_StAtUs",
                                             };

            var parser = new TcpCommandParse();
            foreach (var item in listofPossibleCommands)
            {
                var cmdType = parser.ParseCommand(item);

                Assert.AreEqual(TcpCommandParse.CommandType.JobStatus, cmdType.Command);
                Assert.AreEqual(0, cmdType.CommandData.Count);
            }
        }

        /// <summary>
        /// Tests the job status.
        /// </summary>
        [TestMethod]
        public void TestJobClear()
        {
            var listofPossibleCommands = new List<string>
                                             {
                                                 "clear_job",
                                                 "clear_job?111=0",
                                                 "CLEAR_JOB?111=0",
                                                 "cleAr_JoB",
                                             };

            var parser = new TcpCommandParse();
            foreach (var item in listofPossibleCommands)
            {
                var cmdType = parser.ParseCommand(item);

                Assert.AreEqual(TcpCommandParse.CommandType.ClearJob, cmdType.Command);
                Assert.AreEqual(0, cmdType.CommandData.Count);
            }
        }

        /// <summary>
        /// Tests the version request.
        /// </summary>
        [TestMethod]
        public void TestVersion()
        {
            var listofPossibleCommands = new List<string> { "version", "VeRSIon?111=0", "VERSION?111=0", "Version " };

            var parser = new TcpCommandParse();
            foreach (var item in listofPossibleCommands)
            {
                var cmdType = parser.ParseCommand(item);

                Assert.AreEqual(TcpCommandParse.CommandType.Version, cmdType.Command);
                Assert.AreEqual(0, cmdType.CommandData.Count);
            }
        }

        /// <summary>
        /// Tests the state.
        /// </summary>
        [TestMethod]
        public void TestState()
        {
            var listofPossibleCommands = new List<string> { "state", "StAtE?111=0", "STATE?111=0", "state " };

            var parser = new TcpCommandParse();
            foreach (var item in listofPossibleCommands)
            {
                var cmdType = parser.ParseCommand(item);

                Assert.AreEqual(TcpCommandParse.CommandType.SystemState, cmdType.Command);
                Assert.AreEqual(0, cmdType.CommandData.Count);
            }
        }

        /// <summary>
        /// Tests the start.
        /// </summary>
        [TestMethod]
        public void TestStart()
        {
            var listofPossibleCommands = new List<string> { "start", "StarT?111=0", "START?111=0", "start " };

            var parser = new TcpCommandParse();
            foreach (var item in listofPossibleCommands)
            {
                var cmdType = parser.ParseCommand(item);

                Assert.AreEqual(TcpCommandParse.CommandType.Start, cmdType.Command);
                Assert.AreEqual(0, cmdType.CommandData.Count);
            }
        }

        /// <summary>
        /// Tests the stop.
        /// </summary>
        [TestMethod]
        public void TestStop()
        {
            var listofPossibleCommands = new List<string> { "stop", "StOP?111=0", "STOP?111=0", "stop " };

            var parser = new TcpCommandParse();
            foreach (var item in listofPossibleCommands)
            {
                var cmdType = parser.ParseCommand(item);

                Assert.AreEqual(TcpCommandParse.CommandType.Stop, cmdType.Command);
                Assert.AreEqual(0, cmdType.CommandData.Count);
            }
        }

        /// <summary>
        /// Tests the getting all ids.
        /// </summary>
        [TestMethod]
        public void TestGettingAllIds()
        {
            var listofPossibleCommands = new List<string> { "all_ids", "alL_iDS?111=0", "ALL_IDS?111=0", "all_ids " };

            var parser = new TcpCommandParse();
            foreach (var item in listofPossibleCommands)
            {
                var cmdType = parser.ParseCommand(item);

                Assert.AreEqual(TcpCommandParse.CommandType.GetAllIds, cmdType.Command);
                Assert.AreEqual(0, cmdType.CommandData.Count);
            }
        }

        /// <summary>
        /// Tests the limit toggle.
        /// </summary>
        [TestMethod]
        public void TestLimitToggle()
        {
            var listofPossibleCommands = new List<string> { "limits", "LimiTs?111=0", "LIMITS?111=0", "limits " };

            var parser = new TcpCommandParse();
            foreach (var item in listofPossibleCommands)
            {
                var cmdType = parser.ParseCommand(item);

                Assert.AreEqual(TcpCommandParse.CommandType.LimitsToggle, cmdType.Command);
                Assert.AreEqual(0, cmdType.CommandData.Count);
            }
        }

        /// <summary>
        /// Tests the limits on.
        /// </summary>
        [TestMethod]
        public void TestLimitsOn()
        {
            var listofPossibleCommands = new List<string>
                                             {
                                                 "limits_on",
                                                 "LimiTs_oN?111=0",
                                                 "LIMITS_ON?111=0",
                                                 "limits_on ",
                                             };

            var parser = new TcpCommandParse();
            foreach (var item in listofPossibleCommands)
            {
                var cmdType = parser.ParseCommand(item);

                Assert.AreEqual(TcpCommandParse.CommandType.LimitsOn, cmdType.Command);
                Assert.AreEqual(0, cmdType.CommandData.Count);
            }
        }

        /// <summary>
        /// Tests the limits off.
        /// </summary>
        [TestMethod]
        public void TestLimitsOff()
        {
            var listofPossibleCommands = new List<string>
                                             {
                                                 "limits_off",
                                                 "LimiTs_oFF?111=0",
                                                 "LIMITS_OFF?111=0",
                                                 "limits_off ",
                                             };

            var parser = new TcpCommandParse();
            foreach (var item in listofPossibleCommands)
            {
                var cmdType = parser.ParseCommand(item);

                Assert.AreEqual(TcpCommandParse.CommandType.LimitsOff, cmdType.Command);
                Assert.AreEqual(0, cmdType.CommandData.Count);
            }
        }

        /// <summary>
        /// Tests getting project list.
        /// </summary>
        [TestMethod]
        public void TestProjectList()
        {
            var listofPossibleCommands = new List<string>
                                             {
                                                 "project_list",
                                                 "ProJEcT_list?1=0",
                                                 "PROJECT_LIST?111=0",
                                                 "PROJECT_LIsT???",
                                             };

            var parser = new TcpCommandParse();
            foreach (var item in listofPossibleCommands)
            {
                var cmdType = parser.ParseCommand(item);

                Assert.AreEqual(TcpCommandParse.CommandType.ProjectList, cmdType.Command);
                Assert.AreEqual(0, cmdType.CommandData.Count);
            }
        }

        /// <summary>
        /// Tests the loaded job.
        /// </summary>
        [TestMethod]
        public void TestLoadedJob()
        {
            var listofPossibleCommands = new List<string>
                                             {
                                                 "loaded_job",
                                                 "LoAdEd_JOB?1=0",
                                                 "LOADED_JOB?111=0",
                                                 "loaded_job ",
                                             };

            var parser = new TcpCommandParse();
            foreach (var item in listofPossibleCommands)
            {
                var cmdType = parser.ParseCommand(item);

                Assert.AreEqual(TcpCommandParse.CommandType.LoadedJob, cmdType.Command);
                Assert.AreEqual(0, cmdType.CommandData.Count);
            }
        }

        /// <summary>
        /// Tests preview.
        /// </summary>
        [TestMethod]
        public void TestPreview()
        {
            var listofPossibleCommands = new List<string>
                                             {
                                                 "get_preview",
                                                 "GET_PREVIEW?1=0",
                                                 "GeT_PreVIew?111=0",
                                                 "get_preview ",
                                             };

            var parser = new TcpCommandParse();
            foreach (var item in listofPossibleCommands)
            {
                var cmdType = parser.ParseCommand(item);

                Assert.AreEqual(TcpCommandParse.CommandType.Preview, cmdType.Command);
                Assert.AreEqual(0, cmdType.CommandData.Count);
            }
        }

        /// <summary>
        /// Tests enabling job.
        /// </summary>
        [TestMethod]
        public void TestEnableJob()
        {
            var listofPossibleCommands = new List<string>
                                             {
                                                 "enable_job",
                                                 "EnABlE_JoB?111=0",
                                                 "ENABLE_JOB?111=0",
                                                 "enAbLe_JoB",
                                             };

            var parser = new TcpCommandParse();
            foreach (var item in listofPossibleCommands)
            {
                var cmdType = parser.ParseCommand(item);

                Assert.AreEqual(TcpCommandParse.CommandType.EnableJob, cmdType.Command);
                Assert.AreEqual(0, cmdType.CommandData.Count);
            }
        }

        /// <summary>
        /// Tests the basic job load.
        /// </summary>
        [TestMethod]
        public void TestBasicJobLoad()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("load_job?job1");

            Assert.AreEqual(TcpCommandParse.CommandType.LoadJob, cmdType.Command);

            Assert.AreEqual("job1", cmdType.CommandData["job"]);
        }

        /// <summary>
        /// Tests the empty job load.
        /// </summary>
        [TestMethod]
        public void TestEmptyJobLoad()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("load_job?");

            Assert.AreEqual(TcpCommandParse.CommandType.LoadJob, cmdType.Command);

            Assert.AreEqual(string.Empty, cmdType.CommandData["job"]);
        }

        /// <summary>
        /// Tests job load without an argument.
        /// </summary>
        [TestMethod]
        public void TestNoArgumentJobLoad()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("load_job");

            Assert.AreEqual(TcpCommandParse.CommandType.LoadJob, cmdType.Command);

            Assert.AreEqual(string.Empty, cmdType.CommandData["job"]);
        }

        /// <summary>
        /// Tests the load job with empty space.
        /// </summary>
        [TestMethod]
        public void TestLoadJobWithEmptySpace()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("load_job?z 1 2");

            Assert.AreEqual(TcpCommandParse.CommandType.LoadJob, cmdType.Command);

            Assert.AreEqual("z 1 2", cmdType.CommandData["job"]);
        }

        /// <summary>
        /// Tests the load job with extra separators.
        /// </summary>
        [TestMethod]
        public void TestLoadJobWithExtraSeparators()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("load_job????z 1 2");

            Assert.AreEqual(TcpCommandParse.CommandType.LoadJob, cmdType.Command);

            Assert.AreEqual("???z 1 2", cmdType.CommandData["job"]);
        }

        /// <summary>
        /// Tests the axis coordinate.
        /// </summary>
        [TestMethod]
        public void TestAxisCoordinate()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("axis_coor?axis=0");

            Assert.AreEqual(TcpCommandParse.CommandType.AxisCoordinate, cmdType.Command);

            Assert.AreEqual("0", cmdType.CommandData["axis"]);
        }

        /// <summary>
        /// Tests the axis coordinate no axis.
        /// </summary>
        [TestMethod]
        public void TestAxisCoordinateNoAxis()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("axis_coor?axis=");

            Assert.AreEqual(TcpCommandParse.CommandType.AxisCoordinate, cmdType.Command);

            Assert.AreEqual(string.Empty, cmdType.CommandData["axis"]);
        }

        /// <summary>
        /// Tests the axis coordinate no axis.
        /// </summary>
        [TestMethod]
        public void TestAxisCoordinateStringAxis()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("axis_coor?axis=zaxis");

            Assert.AreEqual(TcpCommandParse.CommandType.AxisCoordinate, cmdType.Command);

            Assert.AreEqual("zaxis", cmdType.CommandData["axis"]);
        }

        /// <summary>
        /// Tests the axis coordinate no axis argument.
        /// </summary>
        [TestMethod]
        public void TestAxisCoordinateNoAxisArgument()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("axis_coor?axi?axi2");

            Assert.AreEqual(TcpCommandParse.CommandType.AxisCoordinate, cmdType.Command);

            Assert.AreEqual(string.Empty, cmdType.CommandData["axis"]);
        }

        /// <summary>
        /// Tests the axis coordinate no argument.
        /// </summary>
        [TestMethod]
        public void TestAxisCoordinateNoargument()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("axis_coor");

            Assert.AreEqual(TcpCommandParse.CommandType.AxisCoordinate, cmdType.Command);

            Assert.AreEqual(string.Empty, cmdType.CommandData["axis"]);
        }

        /// <summary>
        /// Tests getting axis minimum maximum.
        /// </summary>
        [TestMethod]
        public void TestGettingAxisMinAndMax()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("get_axis?axis=5?value=min");

            Assert.AreEqual(TcpCommandParse.CommandType.AxisLimits, cmdType.Command);

            Assert.AreEqual("5", cmdType.CommandData["axis"]);
            Assert.AreEqual("min", cmdType.CommandData["value"]);
        }

        /// <summary>
        /// Tests getting invalid min/max value on axis.
        /// </summary>
        [TestMethod]
        public void TestGettingAxisInvalidValue()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("get_axis?axis=5?value=m123in");

            Assert.AreEqual(TcpCommandParse.CommandType.AxisLimits, cmdType.Command);

            Assert.AreEqual("5", cmdType.CommandData["axis"]);
            Assert.AreEqual("max", cmdType.CommandData["value"]);
        }

        /// <summary>
        /// Tests getting axis with no arguments.
        /// </summary>
        [TestMethod]
        public void TestGettingAxisInvalid()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("get_axis");

            Assert.AreEqual(TcpCommandParse.CommandType.AxisLimits, cmdType.Command);

            Assert.AreEqual(string.Empty, cmdType.CommandData["axis"]);
            Assert.AreEqual("max", cmdType.CommandData["value"]);
        }

        /// <summary>
        /// Tests the axis home.
        /// </summary>
        [TestMethod]
        public void TestAxisHome()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("axis_home?axis=5");

            Assert.AreEqual(TcpCommandParse.CommandType.AxisHome, cmdType.Command);

            Assert.AreEqual("5", cmdType.CommandData["axis"]);
        }

        /// <summary>
        /// Tests the axis home no axis.
        /// </summary>
        [TestMethod]
        public void TestAxisHomeNoAxis()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("axis_home?axis=");

            Assert.AreEqual(TcpCommandParse.CommandType.AxisHome, cmdType.Command);

            Assert.AreEqual(string.Empty, cmdType.CommandData["axis"]);
        }

        /// <summary>
        /// Tests the axis home no argument.
        /// </summary>
        [TestMethod]
        public void TestAxisHomeNoArgument()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("axis_home");

            Assert.AreEqual(TcpCommandParse.CommandType.AxisHome, cmdType.Command);

            Assert.AreEqual(string.Empty, cmdType.CommandData["axis"]);
        }

        /// <summary>
        /// Tests the axis Stop.
        /// </summary>
        [TestMethod]
        public void TestAxisStop()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("stop_axis?axis=5");

            Assert.AreEqual(TcpCommandParse.CommandType.AxisStop, cmdType.Command);

            Assert.AreEqual("5", cmdType.CommandData["axis"]);
        }

        /// <summary>
        /// Tests the axis home no axis.
        /// </summary>
        [TestMethod]
        public void TestAxisStopNoAxis()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("stop_axis?axis=");

            Assert.AreEqual(TcpCommandParse.CommandType.AxisStop, cmdType.Command);

            Assert.AreEqual(string.Empty, cmdType.CommandData["axis"]);
        }

        /// <summary>
        /// Test axis move command.
        /// </summary>
        [TestMethod]
        public void TestAxisMoving()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("move_axis?axis=0&position=100.51");
            Assert.AreEqual(TcpCommandParse.CommandType.AxisMove, cmdType.Command);
            Assert.AreEqual("0", cmdType.CommandData["axis"]);
            Assert.AreEqual("100.51", cmdType.CommandData["position"]);
        }

        /// <summary>
        /// Tests the axis stop no argument.
        /// </summary>
        [TestMethod]
        public void TestAxisStopNoArgument()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("stop_axis");

            Assert.AreEqual(TcpCommandParse.CommandType.AxisStop, cmdType.Command);

            Assert.AreEqual(string.Empty, cmdType.CommandData["axis"]);
        }

        /// <summary>
        /// Tests setting identifier.
        /// </summary>
        [TestMethod]
        public void TestSettingID()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("set_id?id=123&data=456");

            Assert.AreEqual(TcpCommandParse.CommandType.SetID, cmdType.Command);

            Assert.AreEqual("456", cmdType.CommandData["data"]);
            Assert.AreEqual("123", cmdType.CommandData["id"]);
        }

        /// <summary>
        /// Tests setting identifier reversed.
        /// </summary>
        [TestMethod]
        public void TestSettingIDReversed()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("set_id?data=456&id=123");

            Assert.AreEqual(TcpCommandParse.CommandType.SetID, cmdType.Command);

            Assert.AreEqual("456", cmdType.CommandData["data"]);
            Assert.AreEqual("123", cmdType.CommandData["id"]);
        }

        /// <summary>
        /// Tests the setting identifier empty arguments.
        /// </summary>
        [TestMethod]
        public void TestSettingIDEmptyArguments()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("set_id");

            Assert.AreEqual(TcpCommandParse.CommandType.SetID, cmdType.Command);

            Assert.AreEqual(string.Empty, cmdType.CommandData["data"]);
            Assert.AreEqual(string.Empty, cmdType.CommandData["id"]);
        }

        /// <summary>
        /// Tests the setting identifier unicode.
        /// </summary>
        [TestMethod]
        public void TestSettingIDUnicode()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("set_id?id=ID1&data=тест");

            Assert.AreEqual(TcpCommandParse.CommandType.SetID, cmdType.Command);

            Assert.AreEqual("тест", cmdType.CommandData["data"]);
            Assert.AreEqual("ID1", cmdType.CommandData["id"]);
        }

        /// <summary>
        /// Tests object nudge.
        /// </summary>
        [TestMethod]
        public void TestNudgeObject()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("nudge_object?id=1&x=2.0&y=2.333");

            Assert.AreEqual(TcpCommandParse.CommandType.NudgeObject, cmdType.Command);

            Assert.AreEqual("1", cmdType.CommandData["id"]);
            Assert.AreEqual("2.0", cmdType.CommandData["x"]);
            Assert.AreEqual("2.333", cmdType.CommandData["y"]);
        }

        /// <summary>
        /// Tests object nudge.
        /// </summary>
        [TestMethod]
        public void TestMarkObject()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("mark_object?id=OBJ1");

            Assert.AreEqual(TcpCommandParse.CommandType.MarkObject, cmdType.Command);

            Assert.AreEqual("OBJ1", cmdType.CommandData["id"]);
        }

        /// <summary>
        /// Tests move all.
        /// </summary>
        [TestMethod]
        public void TestMoveAll()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("move_all?x=2.0&y=2.333");

            Assert.AreEqual(TcpCommandParse.CommandType.MoveAll, cmdType.Command);

            Assert.AreEqual("2.0", cmdType.CommandData["x"]);
            Assert.AreEqual("2.333", cmdType.CommandData["y"]);
        }

        /// <summary>
        /// Tests move all with no arguments.
        /// </summary>
        [TestMethod]
        public void TestMoveAllNoArguments()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("move_all");

            Assert.AreEqual(TcpCommandParse.CommandType.MoveAll, cmdType.Command);

            Assert.AreEqual(string.Empty, cmdType.CommandData["x"]);
            Assert.AreEqual(string.Empty, cmdType.CommandData["y"]);
        }

        /// <summary>
        /// Tests object nudge wrong order.
        /// </summary>
        [TestMethod]
        public void TestNudgeObjectWrongOrder()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("nudge_object?x=2.0&y=2.333&id=1");

            Assert.AreEqual(TcpCommandParse.CommandType.NudgeObject, cmdType.Command);

            Assert.AreEqual("1", cmdType.CommandData["id"]);
            Assert.AreEqual("2.0", cmdType.CommandData["x"]);
            Assert.AreEqual("2.333", cmdType.CommandData["y"]);
        }

        /// <summary>
        /// Tests object nudge all empty strings.
        /// </summary>
        [TestMethod]
        public void TestNudgeObjectEmptyStrings()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("nudge_object?x=&y=&id=");

            Assert.AreEqual(TcpCommandParse.CommandType.NudgeObject, cmdType.Command);

            Assert.AreEqual(string.Empty, cmdType.CommandData["id"]);
            Assert.AreEqual(string.Empty, cmdType.CommandData["x"]);
            Assert.AreEqual(string.Empty, cmdType.CommandData["y"]);
        }

        /// <summary>
        /// Tests object rotating.
        /// </summary>
        [TestMethod]
        public void TestRotateObject()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("rotate_object?id=1&angle=37.012");

            Assert.AreEqual(TcpCommandParse.CommandType.RotateObject, cmdType.Command);

            Assert.AreEqual("1", cmdType.CommandData["id"]);
            Assert.AreEqual("37.012", cmdType.CommandData["angle"]);
        }

        /// <summary>
        /// Tests object rotating with empty strings.
        /// </summary>
        [TestMethod]
        public void TestRotateObjectEmptyStrings()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("rotate_object?id=&angle=");

            Assert.AreEqual(TcpCommandParse.CommandType.RotateObject, cmdType.Command);

            Assert.AreEqual(string.Empty, cmdType.CommandData["id"]);
            Assert.AreEqual(string.Empty, cmdType.CommandData["angle"]);
        }

        /// <summary>
        /// Tests object rotating.
        /// </summary>
        [TestMethod]
        public void TestRotateAll()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("rotate_all?angle=37.012");

            Assert.AreEqual(TcpCommandParse.CommandType.RotateAll, cmdType.Command);

            Assert.AreEqual("37.012", cmdType.CommandData["angle"]);
        }

        /// <summary>
        /// Tests object rotating with empty string.
        /// </summary>
        [TestMethod]
        public void TestRotateAllEmptyString()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("rotate_all?angle=");

            Assert.AreEqual(TcpCommandParse.CommandType.RotateAll, cmdType.Command);

            Assert.AreEqual(string.Empty, cmdType.CommandData["angle"]);
        }

        /// <summary>
        /// Tests the state of the shutter.
        /// </summary>
        [TestMethod]
        public void TestShutterState()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("shutter_state");
            Assert.AreEqual(TcpCommandParse.CommandType.ShutterState, cmdType.Command);
            Assert.AreEqual(0, cmdType.CommandData.Count);
        }

        /// <summary>
        /// Tests the door close.
        /// </summary>
        [TestMethod]
        public void TestDoorClose()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("door_close");
            Assert.AreEqual(TcpCommandParse.CommandType.DoorClose, cmdType.Command);
        }

        /// <summary>
        /// Tests the door close.
        /// </summary>
        [TestMethod]
        public void TestDoorOpen()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("door_open");
            Assert.AreEqual(TcpCommandParse.CommandType.DoorOpen, cmdType.Command);
        }

        /// <summary>
        /// Tests object rotating with empty string.
        /// </summary>
        [TestMethod]
        public void TestRotateAllEmptyArgument()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("rotate_all");

            Assert.AreEqual(TcpCommandParse.CommandType.RotateAll, cmdType.Command);

            Assert.AreEqual(string.Empty, cmdType.CommandData["angle"]);
        }

        /// <summary>
        /// Tests the invalid command.
        /// </summary>
        [TestMethod]
        public void TestInvalidCommand()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("invalid_command?data=123");
            Assert.AreEqual(TcpCommandParse.CommandType.None, cmdType.Command);
            Assert.AreEqual(0, cmdType.CommandData.Count);
        }

        /// <summary>
        /// Tests the invalid command empty.
        /// </summary>
        [TestMethod]
        public void TestInvalidCommandEmpty()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand(string.Empty);
            Assert.AreEqual(TcpCommandParse.CommandType.None, cmdType.Command);
            Assert.AreEqual(0, cmdType.CommandData.Count);
        }

        /// <summary>
        /// Tests the invalid command argument separators.
        /// </summary>
        [TestMethod]
        public void TestInvalidCommandArgumentSeparators()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("????");
            Assert.AreEqual(TcpCommandParse.CommandType.None, cmdType.Command);
            Assert.AreEqual(0, cmdType.CommandData.Count);
        }

        /// <summary>
        /// Tests the get power command.
        /// </summary>
        [TestMethod]
        public void TestGetPowerCommand()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("get_power?pen=123");
            Assert.AreEqual(TcpCommandParse.CommandType.GetLaserPower, cmdType.Command);
            Assert.AreEqual(1, cmdType.CommandData.Count);
            Assert.AreEqual("123", cmdType.CommandData["pen"]);
        }

        /// <summary>
        /// Tests the get frequency command.
        /// </summary>
        [TestMethod]
        public void TestGetFrequencyCommand()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("get_frequency?pen=123");
            Assert.AreEqual(TcpCommandParse.CommandType.GetLaserFrequency, cmdType.Command);
            Assert.AreEqual(1, cmdType.CommandData.Count);
            Assert.AreEqual("123", cmdType.CommandData["pen"]);
        }

        /// <summary>
        /// Tests the web command.
        /// </summary>
        [TestMethod]
        public void TestWebCommand()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("web");
            Assert.AreEqual(TcpCommandParse.CommandType.GetWeb, cmdType.Command);
        }

        /// <summary>
        /// Tests the set power command.
        /// </summary>
        [TestMethod]
        public void TestSetPowerCommand()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("set_power?pen=123&power=51");
            Assert.AreEqual(TcpCommandParse.CommandType.SetLaserPower, cmdType.Command);
            Assert.AreEqual(2, cmdType.CommandData.Count);
            Assert.AreEqual("123", cmdType.CommandData["pen"]);
            Assert.AreEqual("51", cmdType.CommandData["power"]);
        }

        /// <summary>
        /// Tests the set frequency command.
        /// </summary>
        [TestMethod]
        public void TestSetFrequencyCommand()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("set_frequency?pen=123&frequency=52000");
            Assert.AreEqual(TcpCommandParse.CommandType.SetLaserFrequency, cmdType.Command);
            Assert.AreEqual(2, cmdType.CommandData.Count);
            Assert.AreEqual("123", cmdType.CommandData["pen"]);
            Assert.AreEqual("52000", cmdType.CommandData["frequency"]);
        }

        /// <summary>
        /// Tests the show user message command.
        /// </summary>
        [TestMethod]
        public void TestShowUserMessageCommand()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("show_user_message?message=this is my message");
            Assert.AreEqual(TcpCommandParse.CommandType.ShowUserMessage, cmdType.Command);
            Assert.AreEqual(1, cmdType.CommandData.Count);
            Assert.AreEqual("this is my message", cmdType.CommandData["message"]);
        }

        /// <summary>
        /// Tests the hide user message command.
        /// </summary>
        [TestMethod]
        public void TestHideUserMessageCommand()
        {
            var cmd = new TcpCommandParse().ParseCommand("hide_user_message");
            Assert.AreEqual(TcpCommandParse.CommandType.HideUserMessage, cmd.Command);
            Assert.AreEqual(0, cmd.CommandData.Count);
        }

        /// <summary>
        /// Tests the object delete command.
        /// </summary>
        [TestMethod]
        public void TestObjectDeleteCommand()
        {
            var parser = new TcpCommandParse();
            var cmdType = parser.ParseCommand("delete_object?object=abc");
            Assert.AreEqual(TcpCommandParse.CommandType.DeleteObject, cmdType.Command);
            var firstItem = cmdType.CommandData.FirstOrDefault();
            Assert.AreEqual("object", firstItem.Key);
            Assert.AreEqual("abc", firstItem.Value);
        }
    }
}
