﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TykmaParse.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.FileParse
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using Tykma.Core.Entity;

    /// <summary>
    /// The TYKMA parser.
    /// </summary>
    public class TykmaParse : IFoundFileParserInterface
    {
        /// <summary>
        /// The delimiters.
        /// </summary>
        private readonly char[] delimiters = { ':' };

        /// <summary>
        /// Parses the code.
        /// </summary>
        /// <param name="ms">The memory stream.</param>
        /// <returns>Found file data class.</returns>
        public FoundFileData ParseJob(MemoryStream ms)
        {
            var listofImportedEntities = new List<EntityInformation>();
            var jobName = string.Empty;
            var qty = 0;

            ms.Position = 0;

            using (var sr = new StreamReader(ms))
            {
                // Iterate through lines.
                while (sr.Peek() > 0)
                {
                    // Read the line into a variable.
                    string line = sr.ReadLine();

                    // If the line is not null.
                    if (line != null)
                    {
                        // Split it into two parts.
                        string[] parts = line.Split(this.delimiters, 2);

                        // Check that there are two parts.
                        if (parts.Length > 1)
                        {
                            // Create new entity.
                            var importedEntity = new EntityInformation { Name = parts[0], Value = parts[1] };

                            // Check that this isn't a double.
                            if (listofImportedEntities.All(ent => ent.Name != importedEntity.Name))
                            {
                                // Add it to our list.
                                listofImportedEntities.Add(importedEntity);
                            }
                        }
                        else
                        {
                            if (parts[0].StartsWith("*"))
                            {
                                jobName = parts[0].Replace("*", string.Empty);
                            }
                            else if (parts[0].StartsWith("#"))
                            {
                                int outInt;
                                qty = int.TryParse(parts[0].Replace("#", string.Empty), out outInt) ? outInt : 1;
                            }
                        }
                    }
                }
            }

            return new FoundFileData(listofImportedEntities, jobName, qty);
        }
    }
}
