﻿//-----------------------------------------------------------------------
// <copyright file="IPLCInterface.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Tykma.Core.PLC
{
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// PLC interface - connect to different PLCs.
    /// </summary>
    public interface IPLCInterface
    {
        /// <summary>
        /// Occurs when the connection state changes.
        /// </summary>
        event EventHandler<ConnectionStatusMessage> ConnectionStatusEvent;

        /// <summary>
        /// Occurs when a tag changes state.
        /// </summary>
        event EventHandler<TagValues> TagChangeEvent;

        /// <summary>
        /// Gets the type of the PLC.
        /// </summary>
        /// <value>
        /// The type of the PLC.
        /// </value>
        EPLCType PLCType { get; }

        /// <summary>
        /// Gets or sets the IP address.
        /// </summary>
        /// <value>
        /// The IP address.
        /// </value>
        string IpAddress { get; set; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="IPLCInterface" /> is connected.
        /// </summary>
        /// <value>
        ///   <c>true</c> if connected; otherwise, <c>false</c>.
        /// </value>
        bool Connected { get; }

        /// <summary>
        /// Connects to the PLC.
        /// </summary>
        void Connect();

        /// <summary>
        /// Disconnects from the PLC.
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Add a tag to be watched.
        /// </summary>
        /// <param name="tagName">Name of the tag.</param>
        /// <param name="importantGroup">if set to <c>true</c> [important group].</param>
        /// <param name="tagType">Type of the data.</param>
        /// <typeparam name="T">The type.</typeparam>
        /// <returns>Success state.</returns>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        Task<bool> AutoTagAdd<T>(string tagName, bool importantGroup, TagTypes.ETagType tagType);

        /// <summary>
        /// Reads tags from the unimportant group.
        /// </summary>
        /// <param name="whichTag">The tag.</param>
        /// <returns>Tag value.</returns>
        string TagReadUnimportant(string whichTag);

        /// <summary>
        /// Writes a PLC tag.
        /// </summary>
        /// <typeparam name="T">Tag type.</typeparam>
        /// <param name="tagName">Tag name.</param>
        /// <param name="value">Tag value.</param>
        /// <returns>Whether or not tag write succeeded.</returns>
        Task<bool> WriteTag<T>(string tagName, object value);

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        void Dispose();
    }
}
