﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NudgerImages.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Form which houses nudger images.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Nudger
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;

    using Icon_Interface.Properties;

    /// <summary>
    /// Form which houses nudge control images.
    /// </summary>
    internal class NudgerImages : IDisposable
    {
        /// <summary>
        /// The image list.
        /// </summary>
        private readonly Dictionary<ArrowTypes, Image> imageList;

        /// <summary>
        /// Initializes a new instance of the <see cref="NudgerImages"/> class.
        /// </summary>
        public NudgerImages()
        {
            // ReSharper disable once UseObjectOrCollectionInitializer
            this.imageList = new Dictionary<ArrowTypes, Image>();

            // ↑
            this.imageList.Add(ArrowTypes.StandardUp, new Bitmap(ImageResources.up_arrow));
            this.imageList.Add(ArrowTypes.ActiveUp, new Bitmap(ImageResources.white_up_arrow));

            // ↓
            this.imageList.Add(ArrowTypes.StandardDown, new Bitmap(ImageResources.down_arrow));
            this.imageList.Add(ArrowTypes.ActiveDown, new Bitmap(ImageResources.white_down_arrow));

            // →
            this.imageList.Add(ArrowTypes.StandardRight, new Bitmap(ImageResources.forwardarrow));
            this.imageList.Add(ArrowTypes.ActiveRight, new Bitmap(ImageResources.white_right_arrow));

            // ←
            this.imageList.Add(ArrowTypes.StandardLeft, new Bitmap(ImageResources.left_arrow));
            this.imageList.Add(ArrowTypes.ActiveLeft, new Bitmap(ImageResources.white_left_arrow));

            // ↷
            this.imageList.Add(ArrowTypes.StandardCw, new Bitmap(ImageResources.rotate_cw1));
            this.imageList.Add(ArrowTypes.ActiveCw, new Bitmap(ImageResources.white_cw_arrow));

            // ↶
            this.imageList.Add(ArrowTypes.StandardCcw, new Bitmap(ImageResources.rotate_ccw1));
            this.imageList.Add(ArrowTypes.ActiveCcw, new Bitmap(ImageResources.white_ccw_arrow));
        }

        /// <summary>
        /// Arrow type enumerator.
        /// </summary>
        private enum ArrowTypes
        {
            /// <summary>
            /// The standard right arrow.
            /// </summary>
            StandardRight,

            /// <summary>
            /// The standard left arrow.
            /// </summary>
            StandardLeft,

            /// <summary>
            /// The standard up arrow.
            /// </summary>
            StandardUp,

            /// <summary>
            /// The standard down arrow.
            /// </summary>
            StandardDown,

            /// <summary>
            /// The standard clockwise arrow.
            /// </summary>
            StandardCw,

            /// <summary>
            /// The standard counter-clockwise arrow.
            /// </summary>
            StandardCcw,

            /// <summary>
            /// The active right arrow.
            /// </summary>
            ActiveRight,

            /// <summary>
            /// The active left arrow.
            /// </summary>
            ActiveLeft,

            /// <summary>
            /// The active up arrow.
            /// </summary>
            ActiveUp,

            /// <summary>
            /// The active down arrow.
            /// </summary>
            ActiveDown,

            /// <summary>
            /// The active clockwise error.
            /// </summary>
            ActiveCw,

            /// <summary>
            /// The active counter-clockwise arrow.
            /// </summary>
            ActiveCcw,
        }

        /// <summary>
        /// Gets the arrow up.
        /// </summary>
        /// <value>
        /// The arrow up.
        /// </value>
        internal Image ArrowUp
        {
            get { return this.imageList[ArrowTypes.StandardUp]; }
        }

        /// <summary>
        /// Gets the arrow down.
        /// </summary>
        /// <value>
        /// The arrow down.
        /// </value>
        internal Image ArrowDown
        {
            get { return this.imageList[ArrowTypes.StandardDown]; }
        }

        /// <summary>
        /// Gets the arrow left.
        /// </summary>
        /// <value>
        /// The arrow left.
        /// </value>
        internal Image ArrowLeft
        {
            get { return this.imageList[ArrowTypes.StandardLeft]; }
        }

        /// <summary>
        /// Gets the arrow right.
        /// </summary>
        /// <value>
        /// The arrow right.
        /// </value>
        internal Image ArrowRight
        {
            get { return this.imageList[ArrowTypes.StandardRight]; }
        }

        /// <summary>
        /// Gets the arrow clock wise.
        /// </summary>
        /// <value>
        /// The arrow clock wise.
        /// </value>
        internal Image ArrowClockWise
        {
            get { return this.imageList[ArrowTypes.StandardCw]; }
        }

        /// <summary>
        /// Gets the arrow counter clockwise.
        /// </summary>
        /// <value>
        /// The arrow counter clockwise.
        /// </value>
        internal Image ArrowCounterClockwise
        {
            get { return this.imageList[ArrowTypes.StandardCcw]; }
        }

        /// <summary>
        /// Gets the activated arrow up.
        /// </summary>
        /// <value>
        /// The activated arrow up.
        /// </value>
        internal Image ArrowActivatedUp
        {
            get { return this.imageList[ArrowTypes.ActiveUp]; }
        }

        /// <summary>
        /// Gets the activated arrow down.
        /// </summary>
        /// <value>
        /// The activated arrow down.
        /// </value>
        internal Image ArrowActivatedDown
        {
            get { return this.imageList[ArrowTypes.ActiveDown]; }
        }

        /// <summary>
        /// Gets the activated arrow left.
        /// </summary>
        /// <value>
        /// The activated arrow left.
        /// </value>
        internal Image ArrowActivatedLeft
        {
            get { return this.imageList[ArrowTypes.ActiveLeft]; }
        }

        /// <summary>
        /// Gets the activated arrow right.
        /// </summary>
        /// <value>
        /// The activated arrow right.
        /// </value>
        internal Image ArrowActivatedRight
        {
            get { return this.imageList[ArrowTypes.ActiveRight]; }
        }

        /// <summary>
        /// Gets the activated arrow clock wise.
        /// </summary>
        /// <value>
        /// The activated arrow clock wise.
        /// </value>
        internal Image ArrowActivatedClockWise
        {
            get { return this.imageList[ArrowTypes.ActiveCw]; }
        }

        /// <summary>
        /// Gets the activated arrow counter clockwise.
        /// </summary>
        /// <value>
        /// The activated arrow counter clockwise.
        /// </value>
        internal Image ArrowActivatedCounterClockwise
        {
            get { return this.imageList[ArrowTypes.ActiveCcw]; }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }
    }
}
