﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdExecMark.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// Command to start a marking cycle.
    /// </summary>
    internal class CmdExecMark : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdExecMark" /> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking API.</param>
        /// <param name="markOnTheFly">if set to <c>true</c> [mark on the fly].</param>
        public CmdExecMark(IntPtr lmcApi, bool markOnTheFly)
            : base(lmcApi)
        {
            this.MOTF = markOnTheFly;
        }

        /// <summary>
        /// Mark selected project.
        /// </summary>
        /// <param name="markOnTheFly">if set to <c>true</c> [mark on the fly].</param>
        /// <returns>Success code.</returns>
        private delegate int LMC1ApiMark(bool markOnTheFly);

        /// <summary>
        /// Gets a value indicating whether or not we are marking in MOTF mode.
        /// </summary>
        /// <value>
        ///   <c>true</c> if motf; otherwise, <c>false</c>.
        /// </value>
        private bool MOTF { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>A return code message.</returns>
        internal ReturnCodeMessage Execute()
        {
            ReturnCodeMessage rm;

            var lmc1Mark = this.LoadFunction<LMC1ApiMark>("lmc1_Mark");
            if (lmc1Mark == null)
            {
                throw new EngineNotConnectedException();
            }

            try
            {
                var result = lmc1Mark(this.MOTF);
                rm = this.GetMiniLaseReturnCode(result);
            }
            catch (AccessViolationException)
            {
                rm = new ReturnCodeMessage { Status = ReturnCodeMsg.Error };
            }

            return rm;
        }
    }
}
