﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomTabControl.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Custom tab control - tab headers are hidden.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    using Icon_Interface.Pinvokes;

    /// <summary>
    /// Custom tab control - remove tab names.
    /// </summary>
    internal class CustomTabControl : TabControl
    {
        /// <summary>
        /// The TCM_ADJUSTRECT.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private const int TCMADJUSTRECT = 0x1328;

        /// <summary>
        /// This member overrides <see cref="M:System.Windows.Forms.Control.WndProc(System.Windows.Forms.Message@)" />.
        /// </summary>
        /// <param name="m">A Windows Message Object.</param>
        protected override void WndProc(ref Message m)
        {
            // Hide the tab headers at run-time
            if (m.Msg == TCMADJUSTRECT && !this.DesignMode)
            {
                m.Result = (IntPtr)1;
                return;
            }

            // call the base class implementation
            base.WndProc(ref m);
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the event data.</param>
        protected override void OnPaint(PaintEventArgs e)
        {
            // var p = this.Parent.Location;
            //  p = new Point(0-p.X, 0-p.Y);
            // e.Graphics.DrawImage(this.FindForm().BackgroundImage, p);
        }

        /// <summary>
        /// Raises the <see>
        ///              <cref>PaintBackground</cref>
        ///            </see>  event.
        /// </summary>
        /// <param name="e">The <see cref="PaintEventArgs" /> instance containing the event data.</param>
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            var hdc = e.Graphics.GetHdc();
            var rec = new Rectangle(e.ClipRectangle.Left, e.ClipRectangle.Top, e.ClipRectangle.Width, e.ClipRectangle.Height);
            NativeMethods.DrawThemeParentBackground(this.Handle, hdc, ref rec);
            e.Graphics.ReleaseHdc(hdc);
        }
    }
}
