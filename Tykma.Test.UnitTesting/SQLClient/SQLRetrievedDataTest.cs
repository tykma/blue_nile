﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SQLRetrievedDataTest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Information class to hold SQL data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.SQLClient
{
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Core.SQL.Client;

    /// <summary>
    /// Testing of the SQL retrieved data class.
    /// </summary>
    [TestClass]
    public class SQLRetrievedDataTest
    {
        /// <summary>
        /// Tests the creating new retrieved data.
        /// </summary>
        [TestMethod]
        public void TestCreatingNewRetrievedData()
        {
            var newsqldata = new SQLRetrievedData();

            Assert.AreEqual(newsqldata.JobName, string.Empty);
            Assert.AreEqual(newsqldata.Fault, false);
            Assert.AreEqual(newsqldata.FaultMessage, string.Empty);

            Assert.AreEqual(newsqldata.RetrievedTags.Count(), 0);
        }

        /// <summary>
        /// Tests the setting of sql quantity.
        /// </summary>
        [TestMethod]
        public void TestSettingRetrievedQuantity()
        {
            Assert.AreEqual(100, new SQLRetrievedData(string.Empty, "100").Quantity);
            Assert.AreEqual(1, new SQLRetrievedData(string.Empty, "0").Quantity);
            Assert.AreEqual(1, new SQLRetrievedData(string.Empty, "z").Quantity);
            Assert.AreEqual(1, new SQLRetrievedData(string.Empty, "-5").Quantity);
            Assert.AreEqual(1, new SQLRetrievedData(string.Empty, string.Empty).Quantity);
            Assert.AreEqual(1, new SQLRetrievedData(string.Empty, null).Quantity);
        }
    }
}
