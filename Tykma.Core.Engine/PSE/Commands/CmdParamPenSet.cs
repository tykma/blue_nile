﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdParamPenSet.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;

    using CustomExceptions;

    /// <summary>
    /// The command to set pen parameters.
    /// </summary>
    internal class CmdParamPenSet : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdParamPenSet" /> class.
        /// </summary>
        /// <param name="lmcApi">The API.</param>
        /// <param name="pen">The pen.</param>
        public CmdParamPenSet(IntPtr lmcApi, PenParameter pen)
            : base(lmcApi)
        {
            this.Pen = pen;
        }

        /// <summary>
        /// Set parameters of a PEN.
        /// </summary>
        /// <param name="nPenNo">The pen number.</param>
        /// <param name="nMarkLoop">The mark loop.</param>
        /// <param name="dMarkSpeed">The mark speed.</param>
        /// <param name="powerRatio">The power ratio.</param>
        /// <param name="dCurrent">The current.</param>
        /// <param name="nFreq">The frequency.</param>
        /// <param name="nQPulseWidth">Width of the Q pulse.</param>
        /// <param name="nStartTC">The start TC.</param>
        /// <param name="nLaserOffTC">The laser off TC.</param>
        /// <param name="nEndTC">The end TC.</param>
        /// <param name="nPolyTC">The poly TC.</param>
        /// <param name="dJumpSpeed">The jump speed.</param>
        /// <param name="nJumpPosTC">The jump position TC.</param>
        /// <param name="nJumpDistTC">The jump distance TC.</param>
        /// <param name="dEndComp">The end compensation.</param>
        /// <param name="dAccDist">The acceleration distance.</param>
        /// <param name="dPointTime">The point time.</param>
        /// <param name="bPulsePointMode">if set to <c>true</c> [pulse point mode].</param>
        /// <param name="nPulseNum">The pulse number.</param>
        /// <param name="dFlySpeed">The fly speed.</param>
        /// <returns>Pointer to function.</returns>
        private delegate int LMC1ApiSetPenParam(
            int nPenNo,
            int nMarkLoop,
            double dMarkSpeed,
            double powerRatio,
            double dCurrent,
            short nFreq,
            int nQPulseWidth,
            int nStartTC,
            int nLaserOffTC,
            int nEndTC,
            int nPolyTC,
            double dJumpSpeed,
            int nJumpPosTC,
            int nJumpDistTC,
            double dEndComp,
            double dAccDist,
            double dPointTime,
            bool bPulsePointMode,
            int nPulseNum,
            double dFlySpeed);

        /// <summary>
        /// Gets the pen.
        /// </summary>
        private PenParameter Pen { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        /// <exception cref="Tykma.Core.Engine.CustomExceptions.MethodFaultException">Problem executing command.</exception>
        internal bool Execute()
        {
            var lmc1SetPenParam = this.LoadFunction<LMC1ApiSetPenParam>("lmc1_SetPenParam");

            if (lmc1SetPenParam == null)
            {
                throw new EngineNotConnectedException();
            }

            int result = lmc1SetPenParam(
                this.Pen.PenNumber,
                this.Pen.MarkLoop,
                this.Pen.MarkSpeed,
                this.Pen.PowerRatio,
                this.Pen.Current,
                this.Pen.Frequency,
                this.Pen.QPulseWidth,
                this.Pen.StartTC,
                this.Pen.LaserOffTC,
                this.Pen.EndTC,
                this.Pen.PolyTC,
                this.Pen.JumpSpeed,
                this.Pen.JumpPositionTC,
                this.Pen.JumpDistanceTC,
                this.Pen.EndCompensate,
                this.Pen.AcccelerationDistance,
                this.Pen.PointTime,
                this.Pen.PulsePointMode,
                this.Pen.PulseCount,
                this.Pen.FlySpeed);

            var rm = this.GetMiniLaseReturnCode(result);

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
