﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.SCANNER.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// Scanner handling in main UI.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.ComponentModel;
    using System.Numerics;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Icon_Interface.General;
    using Tykma.Icon.ScanParse;

    /// <summary>
    /// Partial class which will hold all scanner input logic.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// Handles the Validating event of the ScannerInput control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs" /> instance containing the event data.</param>
        private async void ScannerInputValidating(object sender, CancelEventArgs e)
        {
            var txtBox = (TextBox)sender;

            var parser = new ScannerParser();

            if (string.IsNullOrEmpty(txtBox.Text))
            {
                return;
            }

            string prefix = TykmaGlobals.INISettings.SIMPLEMARKFIRSTIDPREFSUFF;
            bool stripPrefix = TykmaGlobals.INISettings.SIMPLEMARKSTRIPSUFFIX;
            bool simpleMark = TykmaGlobals.INISettings.SIMPLEMARKMODE;
            bool useQueue = TykmaGlobals.INISettings.USEQUEUE;
            bool sqlServer = TykmaGlobals.SQLSettings.USESQLSERVER;

            var parsed = parser.ParseCode(txtBox.Text, useQueue, simpleMark, prefix, stripPrefix, sqlServer);

            txtBox.Clear();

            if (parsed is ScanCommand)
            {
                // An internal command.
                var sc = (ScanCommand)parsed;
                this.HandleScannerInternalCommand(sc.Command);
            }
            else if (parsed is ScanJob)
            {
                // A job to load.
                var sj = (ScanJob)parsed;
                await this.LoadJobAbstractor(sj.Data);
            }
            else if (parsed is ScanQuantity)
            {
                // A quantity to set.
                var sq = (ScanQuantity)parsed;

                if (sq.Quantity <= this.ux_Control_Quantity.Maximum && sq.Quantity >= this.ux_Control_Quantity.Minimum)
                {
                    this.ux_Control_Quantity.Value = sq.Quantity;
                }
            }
            else if (parsed is ScanSQL && TykmaGlobals.SQLSettings.USESQLSERVER)
            {
                // Sql query to make.
                var ss = (ScanSQL)parsed;

                // this.RetrieveSQLData(ss.Data);
                this.HandleBlueNileScannedData(ss.Data);
            }
            else if (parsed is ScanQueueInfo)
            {
                // Queue information with quantity to add.
                var qs = (ScanQueueInfo)parsed;
                for (int i = 0; i < qs.Quantity; i++)
                {
                    await Task.Run(() => this.AddJobToQueue(qs.Data));
                }
            }
            else if (parsed is ScanSimpleMark)
            {
                var smm = (ScanSimpleMark)parsed;

                // ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
                if (smm.FirstID)
                {
                    await this.ChangeTextByName(TykmaGlobals.INISettings.SIMPLEMARKFIRSTID, smm.Data);
                }
                else
                {
                    await this.ChangeTextByName(TykmaGlobals.INISettings.SIMPLEMARKSECONDID, smm.Data);
                }

                this.UpdatePreview();
            }
            else if (parsed is ScanKeyValuePairs)
            {
                // Key value pair information (entity info).
                var skvp = (ScanKeyValuePairs)parsed;
                if (skvp.DataPresent)
                {
                    foreach (var kvp in skvp.IDDataValuePairs)
                    {
                        await this.ChangeTextByName(kvp.Name, kvp.Value);
                    }

                    this.UpdatePreview();
                }
            }

            this.ux_ScannerInput.Focus();
        }

        /// <summary>
        /// Handles the KeyDown event of the ScannerInput control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs" /> instance containing the event data.</param>
        private void ScannerInputKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.Validate();
            }
        }

        /// <summary>
        /// Handles the TextChanged event of the ScannerInput control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void ScannerInputTextChanged(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Handles a scanned internal command.
        /// </summary>
        /// <param name="command">The command.</param>
        private void HandleScannerInternalCommand(ScanCommand.InternalCommand command)
        {
            switch (command)
            {
                case ScanCommand.InternalCommand.Start:
                    this.StartSequence();
                    break;

                case ScanCommand.InternalCommand.Clear:
                    this.ClearSequence();
                    break;

                case ScanCommand.InternalCommand.Limits:
                    this.PreStartLimits(!this.le.LimitsOn, true);
                    break;

                case ScanCommand.InternalCommand.QuantityUp:
                    this.ux_Control_Quantity.Increment();
                    break;

                case ScanCommand.InternalCommand.QuantityDown:
                    this.ux_Control_Quantity.Decrement();
                    break;

                case ScanCommand.InternalCommand.Edit:
                    if (this.markingJobState.Loaded)
                    {
                        this.ux_HMI.SelectedTab = this.ux_Tab_DataEdit;
                    }

                    break;

                case ScanCommand.InternalCommand.ShowJobList:
                    this.ux_HMI.SelectedTab = this.ux_Tab_JobSelect;
                    break;

                case ScanCommand.InternalCommand.HomeAxisZ:
                    // ReSharper disable once CSharpWarnings::CS4014
                    this.HomeAnAxis(this.axisZ).Forget();
                    break;

                case ScanCommand.InternalCommand.HomeAxisR:

                    // ReSharper disable once CSharpWarnings::CS4014
                    this.HomeAnAxis(this.axisR).Forget();
                    break;

                case ScanCommand.InternalCommand.Debug:
                    break;
            }
        }

        /// <summary>
        /// Add starting serial number key down even.t.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void Ux_Text_StartingSerial_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.Validate();
            }
        }

        /// <summary>
        /// Handle the staring serial number validating event.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void Ux_Text_StartingSerial_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var txtBox = (TextBox)sender;

            var scannedValue = txtBox.Text;
            txtBox.Clear();

            if (string.IsNullOrWhiteSpace(scannedValue))
            {
                return;
            }

            var gvserial = this.gv.GetGlobalVariable("SERIAL");

            if (gvserial.Name == null)
            {
                this.gv.AddGlobalVariable("SERIAL", "000", Tykma.Icon.Globals.EGlobalTypes.GlobalVariableType.Counter_Integer);
            }

            if (BigInteger.TryParse(scannedValue, out BigInteger sn))
            {
                if (this.ArrayJobLoaded)
                {
                    sn--;
                }

                var snstring = sn.ToString();
                var paddedSn = snstring.PadLeft(scannedValue.Length, '0');
                this.gv.ModifyCounter("SERIAL", paddedSn);
            }
            else
            {
                this.TimeStampEvent("SERIAL must be numeric", true);
                return;
            }

            this.InitializeGlobalVariables();
            this.SetAllGlobalVariables();
            this.UpdatePreview();
        }
    }
}
