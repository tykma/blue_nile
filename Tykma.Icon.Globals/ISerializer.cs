﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISerializer.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.Globals
{
    /// <summary>
    /// The Serializer interface.
    /// </summary>
    public interface ISerializer
    {
        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns>
        /// Serialized number.
        /// </returns>
        string GetValue(long number);

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        /// Serialized number.
        /// </returns>
        string GetValue(string value);
    }
}
