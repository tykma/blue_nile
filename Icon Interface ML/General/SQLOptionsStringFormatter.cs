﻿// <copyright file="SQLOptionsStringFormatter.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Icon_Interface.General
{
    using System.Collections.Generic;
    using System.ComponentModel;

    public class SQLOptionsStringConverter : StringConverter
    {
        public override bool GetStandardValuesSupported(ITypeDescriptorContext context) {
            return true; }

        public override bool GetStandardValuesExclusive(ITypeDescriptorContext context) { return true; }

        public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            var list = new List<string>();
            list.Add("Excel");
            list.Add("MySQL");
            list.Add("Oracle");
            list.Add("Microsoft SQL Server");
            return new StandardValuesCollection(list);
        }
    }
}
