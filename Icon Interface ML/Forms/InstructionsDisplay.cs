﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstructionsDisplay.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// Form and logic for displaying instructions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;

    using Icon_Interface.General;
    using Icon_Interface.Properties;

    using Tykma.Icon.ImageBranding;

    /// <summary>
    /// Instructions pictures and RTF.
    /// </summary>
    public partial class InstructionsDisplay : Form
    {
        /// <summary>
        /// The image brands class.
        /// </summary>
        private readonly ImageBranding imageBrands;

        /// <summary>
        /// The main form.
        /// </summary>
        private readonly Main mainForm;

        /// <summary>
        /// Initializes a new instance of the <see cref="InstructionsDisplay" /> class.
        /// </summary>
        /// <param name="refForm">The ref form.</param>
        public InstructionsDisplay(Main refForm)
        {
            this.InitializeComponent();
            this.imageBrands = new ImageBranding(
                                            TykmaGlobals.INISettings.SETTINGSFOLDER,
                                            TykmaGlobals.INISettings.INSTRUCTIONSRTFFOLDER,
                                            TykmaGlobals.INISettings.INSTRUCTIONSIMAGEFOLDER,
                                            Settings.Default.BackColor);

            this.mainForm = refForm;

            // Subscribe to the instructions update event.
            this.mainForm.InstructionsLoadEvent += this.ShowInstructions;
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.ResizeEnd" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnResizeEnd(EventArgs e)
        {
            var rightSideOfMainForm = this.mainForm.Location.X + this.mainForm.Size.Width;
            var leftSideOFmainForm = this.mainForm.Location.X;
            var leftSideofRTFForm = this.Location.X;
            var rightSideofRTFForm = this.Location.X + this.Size.Width;

            if (Math.Abs(rightSideOfMainForm - leftSideofRTFForm) < 50)
            {
                this.SetDesktopLocation(this.mainForm.Location.X + this.mainForm.Size.Width, this.mainForm.Location.Y);
                this.Height = this.mainForm.Height;
            }
            else if (Math.Abs(leftSideOFmainForm - rightSideofRTFForm) < 50)
            {
                this.SetDesktopLocation(this.mainForm.Location.X - this.Size.Width, this.mainForm.Location.Y);
                this.Height = this.mainForm.Height;
            }
        }

        /// <summary>
        /// Sets all colors.
        /// </summary>
        private void SetAllColors()
        {
            this.BackColor = Settings.Default.BackColor;
            this.ux_Picture_Instructions.GridColor = Settings.Default.BackColor;
            this.ForeColor = Settings.Default.ForeColor;
        }

        /// <summary>
        /// Shows the instructions.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        /// <exception cref="System.InvalidOperationException">Loading RTF.</exception>
        private void ShowInstructions(object sender, InstructionsEventMessage e)
        {
            if (e.Status == ImageState.LoadInstructions)
            {
                if (!this.Visible)
                {
                    this.Show();
                }

                this.ux_Picture_Instructions.Image = null;

                this.ux_Picture_Instructions.Image = this.imageBrands.GetJobPicture(e.NameOfLoadedJob);

                this.ux_Picture_Instructions.ZoomToFit();

                this.ux_RTF_Instructions.Clear();

                try
                {
                    FileInfo rtfLocation = this.imageBrands.GetRTFLocation(e.NameOfLoadedJob);

                    if (rtfLocation != null && rtfLocation.Exists)
                    {
                        try
                        {
                            this.ux_RTF_Instructions.LoadFile(rtfLocation.FullName);
                        }
                        catch (Exception)
                        {
                            // ReSharper disable once LocalizableElement
                            this.ux_RTF_Instructions.Text = "Problem loading RTF.";
                        }
                    }
                    else
                    {
                        // ReSharper disable once LocalizableElement
                        this.ux_RTF_Instructions.Text = "RTF not found.";
                    }
                }
                catch (FileNotFoundException)
                {
                    // ReSharper disable once LocalizableElement
                    this.ux_RTF_Instructions.Text = "RTF not found.";
                }
            }
            else if (e.Status == ImageState.ClearInstructions)
            {
                this.ux_Picture_Instructions.Image = null;
                this.ux_RTF_Instructions.Clear();
                this.Hide();
            }
        }

        /// <summary>
        /// Handles the Load event of the InstructionsDisplay control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void InstructionsDisplayLoad(object sender, EventArgs e)
        {
            // ReSharper disable once ArrangeStaticMemberQualifier
            if ((Control.ModifierKeys & Keys.Shift) == 0)
            {
                string initLocation = TykmaGlobals.INISettings.INITIALDIMENSIONSINSTRUCTIONS;
                var il = new Point(0, 0);
                Size sz = this.Size;
                if (!string.IsNullOrWhiteSpace(initLocation))
                {
                    string[] parts = initLocation.Split(',');
                    if (parts.Length >= 2)
                    {
                        il = new Point(int.Parse(parts[0]), int.Parse(parts[1]));
                    }

                    if (parts.Length >= 4)
                    {
                        sz = new Size(int.Parse(parts[2]), int.Parse(parts[3]));
                    }
                }

                this.Size = sz;
                this.Location = il;
                this.SetAllColors();
                this.ux_Split_Container.SplitterDistance = TykmaGlobals.INISettings.HORIZONTALSPLITTERINSTRUCTIONS;
            }
        }

        /// <summary>
        /// Handles the FormClosing event of the InstructionsDisplay control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="FormClosingEventArgs" /> instance containing the event data.</param>
        private void InstructionsDisplayFormClosing(object sender, FormClosingEventArgs e)
        {
            // ReSharper disable once ArrangeStaticMemberQualifier
            if ((Control.ModifierKeys & Keys.Shift) == 0)
            {
                Point location = this.Location;
                Size size = this.Size;
                if (this.WindowState != FormWindowState.Normal)
                {
                    location = this.RestoreBounds.Location;
                    size = this.RestoreBounds.Size;
                }

                string initLocation = string.Join(",", location.X, location.Y, size.Width, size.Height);
                TykmaGlobals.INISettings.INITIALDIMENSIONSINSTRUCTIONS = initLocation;
                this.ux_Split_Container.SplitterDistance = TykmaGlobals.INISettings.HORIZONTALSPLITTERINSTRUCTIONS;
            }

            e.Cancel = true;
            this.Hide();
        }

        /// <summary>
        /// Handles the ResizeEnd event of the InstructionsDisplay control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void InstructionsDisplayResizeEnd(object sender, EventArgs e)
        {
            this.ux_Picture_Instructions.ZoomToFit();
        }

        /// <summary>
        /// Handles the SplitterMoved event of the Split_Container control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SplitterEventArgs" /> instance containing the event data.</param>
        private void SplitContainerSplitterMoved(object sender, SplitterEventArgs e)
        {
            this.ux_Picture_Instructions.ZoomToFit();
        }
    }
}
