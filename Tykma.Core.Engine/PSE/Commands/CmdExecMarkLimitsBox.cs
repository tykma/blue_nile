﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdExecMarkLimitsBox.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// Command to trace an outer bounding box.
    /// </summary>
    internal class CmdExecMarkLimitsBox : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdExecMarkLimitsBox"/> class.
        /// </summary>
        /// <param name="lmcApi">
        /// The laser mark card API.
        /// </param>
        public CmdExecMarkLimitsBox(IntPtr lmcApi)
            : base(lmcApi)
        {
        }

        /// <summary>
        /// Trace the objects.
        /// </summary>
        /// <returns>Return code.</returns>
        private delegate int LMC1ApiRedLightMarkContour();

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Return code state.</returns>
        internal bool Execute()
        {
            // Get pointer to our trace function.
            var lmc1RedLightMarkContour = this.LoadFunction<LMC1ApiRedLightMarkContour>("lmc1_RedLightMarkContour");

            var rm = new ReturnCodeMessage();

            // Check that the function exists.
            if (lmc1RedLightMarkContour != null)
            {
                try
                {
                    // Start tracing.
                    rm = this.GetMiniLaseReturnCode(lmc1RedLightMarkContour());

                    if (rm.Success)
                    {
                        return true;
                    }

                    throw new MethodFaultException(rm.ReturnDescription);
                }
                catch (AccessViolationException e)
                {
                    rm.Status = ReturnCodeMsg.Error;
                    rm.ReturnDescription = e.Message;
                    throw new MethodFaultException(rm.ReturnDescription);
                }
            }

            throw new EngineNotConnectedException();
        }
    }
}
