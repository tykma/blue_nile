﻿// <copyright file="ISQLSettings.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Icon_Interface.File_Management
{
    using Config.Net;

    /// <summary>
    /// SQL Settings interface.
    /// </summary>
    public interface ISQLSettings
    {
        /// <summary>
        /// Gets or sets the database server type.
        /// </summary>
        [Option(Alias = "DB_TYPE", DefaultValue = "Oracle")]
        string DatabaseType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether SQL functionality is enabled.
        /// </summary>
        [Option(Alias = "USE_SQL_SERVER", DefaultValue = true)]
        bool USESQLSERVER { get; set; }

        /// <summary>
        /// Gets or sets the SQL table we will search through.
        /// </summary>
        [Option(Alias = "SQL_TABLE", DefaultValue = "XXBN.BYO_ENGRAVING_JOB_INFO_V")]
        string SQLTABLE { get; set; }

        /// <summary>
        /// Gets or sets the primary key.
        /// </summary>
        [Option(Alias = "SQL_PRIMARY_FIELD", DefaultValue = "JOB_NUMBER")]
        string SQLPRIMARYFIELD { get; set; }

        /// <summary>
        /// Gets or sets the SQL column that identifies the template to load.
        /// </summary>
        [Option(Alias = "SQL_JOB_COLUMN", DefaultValue = "TEMPLATE_FILENAME")]
        string SQLJOBCOLUMN { get; set; }

        /// <summary>
        /// Gets or sets the column with the quantity information.
        /// </summary>
        [Option(Alias = "SQL_QTY_COLUMN", DefaultValue = "")]
        string SQLQTYCOLUMN { get; set; }

        /// <summary>
        /// Gets or sets the SQL connection string.
        /// </summary>
        [Option(Alias = "SQL_CONN_STRING", DefaultValue = @"Data Source=104.167.116.102:1521/xe;Persist Security Info=True;User ID=xxbn;Password=bluenile;")]
        string SQLCONNSTRING { get; set; }
    }
}
