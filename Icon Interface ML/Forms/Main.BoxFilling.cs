﻿// <copyright file="Main.BoxFilling.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Icon_Interface.Forms
{
    using System;
    using Tykma.Core.Entity;
    using Tykma.Core.StringExtensions;
    using Tykma.Icon.BoxFill;

    /// <summary>
    /// Boundary box filling code.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// Handle box filling.
        /// </summary>
        public void HandleBoxFilling()
        {
            var ids = this.le.GetListOfObjects();

            foreach (var item in ids)
            {
                if (item.StartsWith("BOUNDARY:") || item.StartsWith("L_BOUNDARY") || item.StartsWith("B_BOUNDARY"))
                {
                    bool leftJustity = false;
                    bool bottomJustify = false;

                    if (item.StartsWith("L_BOUNDARY:"))
                    {
                        leftJustity = true;
                    }
                    else if (item.StartsWith("B_BOUNDARY:"))
                    {
                        bottomJustify = true;
                    }

                    var id = item.RemovePrefix("BOUNDARY:").RemovePrefix("L_BOUNDARY:").RemovePrefix("B_BOUNDARY:");

                    EntityInformation boxSize;

                    EntityInformation entSize;

                    try
                    {
                        boxSize = this.le.GetObjectSize(item);
                        entSize = this.le.GetObjectSize(id);
                    }
                    catch (Exception e)
                    {
                        Logger.Error(e);
                        return;
                    }

                    var bf = new BoundaryBoxFiller(boxSize, entSize);

                    var cntr = bf.GetOffsetToTextObjectCenteredInBoundingBox();
                    var scale = bf.GetScaleSize();

                    try
                    {
                        this.le.MoveObject(id, cntr.X, cntr.Y);
                    }
                    catch (Exception e)
                    {
                        Logger.Error(e);
                        return;
                    }

                    try
                    {
                        this.le.ScaleSize(id, bf.GetCenterOfBoundingBox().X, bf.GetCenterOfBoundingBox().Y, scale.X, scale.Y);
                    }
                    catch (Exception e)
                    {
                        Logger.Error(e);
                        return;
                    }

                    if (leftJustity)
                    {
                        try
                        {
                            var objent = this.le.GetObjectSize(id);

                            var minxox = boxSize.MinX;
                            var minxent = objent.MinX;

                            var diff = minxox - minxent;
                            this.le.MoveObject(id, diff, 0);
                        }
                        catch (Exception e)
                        {
                            Logger.Error(e);
                            return;
                        }
                    }

                    if (bottomJustify)
                    {
                        try
                        {
                            var objent = this.le.GetObjectSize(id);

                            var mintox = boxSize.MinY;
                            var mintent = objent.MinY;

                            var diff = mintox - mintent;
                            this.le.MoveObject(id, 0, diff);
                        }
                        catch (Exception e)
                        {
                            Logger.Error(e);
                            return;
                        }
                    }
                }
            }
        }
    }
}
