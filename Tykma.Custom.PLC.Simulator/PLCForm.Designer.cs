﻿namespace Tykma.Custom.PLC.Simulator
{
    partial class PLCForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IsConnectingLabel = new System.Windows.Forms.Label();
            this.IPLabel = new System.Windows.Forms.Label();
            this.cbDataReady = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.diameter = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.startSeq = new System.Windows.Forms.CheckBox();
            this.cbStartRequest = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // IsConnectingLabel
            // 
            this.IsConnectingLabel.AutoSize = true;
            this.IsConnectingLabel.Location = new System.Drawing.Point(15, 16);
            this.IsConnectingLabel.Name = "IsConnectingLabel";
            this.IsConnectingLabel.Size = new System.Drawing.Size(35, 13);
            this.IsConnectingLabel.TabIndex = 0;
            this.IsConnectingLabel.Text = "label1";
            // 
            // IPLabel
            // 
            this.IPLabel.AutoSize = true;
            this.IPLabel.Location = new System.Drawing.Point(15, 44);
            this.IPLabel.Name = "IPLabel";
            this.IPLabel.Size = new System.Drawing.Size(17, 13);
            this.IPLabel.TabIndex = 1;
            this.IPLabel.Text = "IP";
            // 
            // cbDataReady
            // 
            this.cbDataReady.AutoSize = true;
            this.cbDataReady.Location = new System.Drawing.Point(15, 19);
            this.cbDataReady.Name = "cbDataReady";
            this.cbDataReady.Size = new System.Drawing.Size(102, 17);
            this.cbDataReady.TabIndex = 2;
            this.cbDataReady.Text = "Set Data Ready";
            this.cbDataReady.UseVisualStyleBackColor = true;
            this.cbDataReady.CheckedChanged += new System.EventHandler(this.cbDataReady_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Received Diameter:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbStartRequest);
            this.groupBox1.Controls.Add(this.diameter);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(164, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 206);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Incoming";
            // 
            // diameter
            // 
            this.diameter.AutoSize = true;
            this.diameter.Location = new System.Drawing.Point(113, 28);
            this.diameter.Name = "diameter";
            this.diameter.Size = new System.Drawing.Size(0, 13);
            this.diameter.TabIndex = 4;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.startSeq);
            this.groupBox2.Controls.Add(this.cbDataReady);
            this.groupBox2.Location = new System.Drawing.Point(8, 118);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(150, 100);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Control";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.IsConnectingLabel);
            this.groupBox3.Controls.Add(this.IPLabel);
            this.groupBox3.Location = new System.Drawing.Point(8, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(150, 100);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Info";
            // 
            // startSeq
            // 
            this.startSeq.AutoSize = true;
            this.startSeq.Location = new System.Drawing.Point(15, 42);
            this.startSeq.Name = "startSeq";
            this.startSeq.Size = new System.Drawing.Size(67, 17);
            this.startSeq.TabIndex = 3;
            this.startSeq.Text = "Set Start";
            this.startSeq.UseVisualStyleBackColor = true;
            this.startSeq.CheckedChanged += new System.EventHandler(this.startSeq_CheckedChanged);
            // 
            // cbStartRequest
            // 
            this.cbStartRequest.AutoSize = true;
            this.cbStartRequest.Location = new System.Drawing.Point(9, 56);
            this.cbStartRequest.Name = "cbStartRequest";
            this.cbStartRequest.Size = new System.Drawing.Size(91, 17);
            this.cbStartRequest.TabIndex = 5;
            this.cbStartRequest.Text = "Start Request";
            this.cbStartRequest.UseVisualStyleBackColor = true;
            // 
            // PLCForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 269);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "PLCForm";
            this.Text = "PLCForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label IsConnectingLabel;
        private System.Windows.Forms.Label IPLabel;
        private System.Windows.Forms.CheckBox cbDataReady;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label diameter;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox startSeq;
        private System.Windows.Forms.CheckBox cbStartRequest;
    }
}