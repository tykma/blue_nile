﻿namespace Icon_Interface.Custom_Controls
{
    partial class ControlVerboseCheckBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ux_CheckBox_Check = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Label_Setting = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ux_CheckBox_Check
            // 
            this.ux_CheckBox_Check.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ux_CheckBox_Check.AutoSize = true;
            this.ux_CheckBox_Check.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_CheckBox_Check.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.ux_CheckBox_Check.Location = new System.Drawing.Point(203, 11);
            this.ux_CheckBox_Check.Name = "ux_CheckBox_Check";
            this.ux_CheckBox_Check.Size = new System.Drawing.Size(15, 14);
            this.ux_CheckBox_Check.TabIndex = 39;
            this.ux_CheckBox_Check.UseVisualStyleBackColor = true;
            this.ux_CheckBox_Check.CheckedChanged += new System.EventHandler(this.CheckBoxChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.ux_Label_Setting, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ux_CheckBox_Check, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(372, 36);
            this.tableLayoutPanel1.TabIndex = 40;
            // 
            // ux_Label_Setting
            // 
            this.ux_Label_Setting.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ux_Label_Setting.AutoSize = true;
            this.ux_Label_Setting.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Label_Setting.Location = new System.Drawing.Point(3, 6);
            this.ux_Label_Setting.Name = "ux_Label_Setting";
            this.ux_Label_Setting.Size = new System.Drawing.Size(56, 24);
            this.ux_Label_Setting.TabIndex = 40;
            this.ux_Label_Setting.Text = "Label";
            // 
            // ControlVerboseCheckBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(372, 36);
            this.Name = "ControlVerboseCheckBox";
            this.Size = new System.Drawing.Size(372, 36);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox ux_CheckBox_Check;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label ux_Label_Setting;
    }
}
