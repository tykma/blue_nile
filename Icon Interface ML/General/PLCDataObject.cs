﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PLCDataObject.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Hold information on a PLC 'data' tag.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.General
{
    /// <summary>
    /// A PLC tag object.
    /// </summary>
    public class PLCDataObject
    {
        /// <summary>
        /// The tag.
        /// </summary>
        private string tag;

        /// <summary>
        /// The echo tag.
        /// </summary>
        private string echotag;

        /// <summary>
        /// The entity name.
        /// </summary>
        private string entityname;

        /// <summary>
        /// Gets or sets the tag.
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        public string Tag
        {
            get
            {
                return this.tag;
            }

            set
            {
                if (value != null)
                {
                    this.tag = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the echo tag.
        /// </summary>
        /// <value>
        /// The echo tag.
        /// </value>
        public string EchoTag
        {
            get
            {
                return this.echotag;
            }

            set
            {
                if (value != null)
                {
                    this.echotag = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the name of the entity.
        /// </summary>
        /// <value>
        /// The name of the entity.
        /// </value>
        public string EntityName
        {
            get
            {
                return this.entityname;
            }

            set
            {
                if (value != null)
                {
                    this.entityname = value;
                }
            }
        }
    }
}
