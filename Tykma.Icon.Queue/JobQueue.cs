﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JobQueue.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.Queue
{
    using System.ComponentModel;
    using System.Linq;

    /// <summary>
    /// Class for storing queued marking items.
    /// </summary>
    public class JobQueue
    {
        /// <summary>
        /// The queue of job items.
        /// </summary>
        private readonly BindingList<QueuedJobInfo> queueOfItems;

        /// <summary>
        /// Initializes a new instance of the <see cref="JobQueue"/> class.
        /// </summary>
        public JobQueue()
        {
            this.queueOfItems = new BindingList<QueuedJobInfo>();
        }

        /// <summary>
        /// Gets the full list.
        /// </summary>
        /// <value>
        /// The full list.
        /// </value>
        public BindingList<QueuedJobInfo> FullList => this.queueOfItems;

        /// <summary>
        /// Adds the item to queue.
        /// </summary>
        /// <param name="jobToAdd">The job to add.</param>
        public void AddItemToQueue(string jobToAdd)
        {
            var job = new QueuedJobInfo(jobToAdd);
            this.queueOfItems.Add(job);
        }

        /// <summary>
        /// Removes the job from our list.
        /// </summary>
        /// <param name="whichJobHash">Job hash.</param>
        public void RemoveJob(string whichJobHash)
        {
            // ReSharper disable once ForCanBeConvertedToForeach
            for (int index = 0; index < this.queueOfItems.Count; index++)
            {
                QueuedJobInfo job = this.queueOfItems[index];
                if (job.JobHash == whichJobHash)
                {
                    job.Finished = true;
                }
            }
        }

        /// <summary>
        /// Enables the job.
        /// </summary>
        /// <param name="whichJobHash">Job hash.</param>
        public void EnableJob(string whichJobHash)
        {
            // ReSharper disable once ForCanBeConvertedToForeach
            for (int index = 0; index < this.queueOfItems.Count; index++)
            {
                QueuedJobInfo job = this.queueOfItems[index];
                if (job.JobHash == whichJobHash)
                {
                    job.Finished = false;
                }
            }
        }

        /// <summary>
        /// Determines whether a job with a specified hash has been completed..
        /// </summary>
        /// <param name="whichJobHash">The which job hash.</param>
        /// <returns>Whether or not a job is complete.</returns>
        public bool IsJobComplete(string whichJobHash)
        {
            return (from job in this.queueOfItems where job.JobHash == whichJobHash select job.Finished).FirstOrDefault();
        }

        /// <summary>
        /// Gets the next the job.
        /// </summary>
        /// <returns>The next available job.</returns>
        public QueuedJobInfo NextJob()
        {
            // return this.queueOfItems.Count > 0 ? this.queueOfItems[0] : new QueuedJobInfo(string.Empty);
            foreach (QueuedJobInfo job in this.queueOfItems)
            {
                if (!job.Finished)
                {
                    return job;
                }
            }

            return new QueuedJobInfo(string.Empty);
        }

        /// <summary>
        /// Clears our job list.
        /// </summary>
        public void Clear()
        {
            this.queueOfItems.Clear();
        }

        /// <summary>
        /// Returns count of items in queue.
        /// </summary>
        /// <returns>Total count.</returns>
        public int CountAll()
        {
            return this.queueOfItems.Count;
        }

        /// <summary>
        /// Returns count of unmarked items in queue..
        /// </summary>
        /// <returns>Total count.</returns>
        public int CountAllToDo()
        {
            return this.queueOfItems.Count(job => !job.Finished && !job.Failed);
        }
    }
}
