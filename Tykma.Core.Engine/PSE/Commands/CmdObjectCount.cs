﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdObjectCount.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   The command to get text data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// Get the total object count in a project.
    /// </summary>
    internal class CmdObjectCount : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdObjectCount"/> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking API.</param>
        public CmdObjectCount(IntPtr lmcApi)
            : base(lmcApi)
        {
        }

        /// <summary>
        /// Get total count of objects.
        /// </summary>
        /// <returns>Success code.</returns>
        private delegate int LMC1ApiGetEntityCount();

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Object count.</returns>
        public int Execute()
        {
            // Get pointer to id count function.
            var lmc1GetCount = this.LoadFunction<LMC1ApiGetEntityCount>("lmc1_GetEntityCount");

            if (lmc1GetCount == null)
            {
                throw new EngineNotConnectedException();
            }

            return lmc1GetCount();
        }
    }
}
