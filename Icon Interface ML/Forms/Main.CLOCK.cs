﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.CLOCK.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Clock handling in the UI.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;

    using Icon_Interface.Properties;

    /// <summary>
    /// Main partial class.
    /// We will handle displaying and generating the current time here.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// Gets the clock time.
        /// </summary>
        /// <value>
        /// The clock time.
        /// </value>
        private string ClockTime
        {
            get
            {
                return DateTime.Now.ToString(@"hh\:mm tt");
            }
        }

        /// <summary>
        /// Show the time.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void TimerShowTimeTick(object sender, EventArgs e)
        {
            // Show the time on the status bar.
            this.ux_Status_Time.Text = this.ClockTime;

            // If we have the red marking in progress screen up - then show the marking time.
            if (this.ux_HMI.SelectedTab == this.ux_Tab_MarkInProgress)
            {
                this.ux_Label_ElapsedTime.Text = string.Format(
                    "{0}: {1} {2}",
                    Resources.Label_Time,
                    this.cycleTimer.Elapsed.TotalSeconds.ToString("N0"),
                    Resources.Label_Seconds);
            }
        }
    }
}
