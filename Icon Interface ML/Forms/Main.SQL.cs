﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.SQL.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// SQL class handling from main Ui.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    using Icon_Interface.General;
    using Icon_Interface.Properties;

    using Tykma.Core.Engine;
    using Tykma.Core.Entity;
    using Tykma.Core.SQL.Client;

    /// <summary>
    /// SQL functions.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// The SQL client class.
        /// </summary>
        private ISqlClient sql;

        /// <summary>
        /// Gets or sets the initialize SQL client.
        /// </summary>
        /// <value>
        /// The initialize SQL client.
        /// </value>
        private void InitializeSQLClient()
        {
            return;
            if (TykmaGlobals.SQLSettings.USESQLSERVER)
            {
                if (TykmaGlobals.SQLSettings.DatabaseType.Equals("MySQL", StringComparison.InvariantCultureIgnoreCase))
                {
                    this.sql = new MySQLClient(
                        TykmaGlobals.SQLSettings.SQLCONNSTRING,
                        TykmaGlobals.SQLSettings.SQLTABLE,
                        TykmaGlobals.SQLSettings.SQLPRIMARYFIELD,
                        TykmaGlobals.SQLSettings.SQLJOBCOLUMN,
                        TykmaGlobals.SQLSettings.SQLQTYCOLUMN);
                }
                else if (TykmaGlobals.SQLSettings.DatabaseType.Equals("Oracle", StringComparison.InvariantCultureIgnoreCase))
                {
                    this.sql = new OracleClient(
                        TykmaGlobals.SQLSettings.SQLCONNSTRING,
                        TykmaGlobals.SQLSettings.SQLTABLE,
                        TykmaGlobals.SQLSettings.SQLPRIMARYFIELD,
                        TykmaGlobals.SQLSettings.SQLJOBCOLUMN,
                        TykmaGlobals.SQLSettings.SQLQTYCOLUMN);
                }
                else if (TykmaGlobals.SQLSettings.DatabaseType.Equals("Excel", StringComparison.InvariantCultureIgnoreCase))
                {
                    this.sql = new ExcelCX(
                        TykmaGlobals.SQLSettings.SQLCONNSTRING,
                        TykmaGlobals.SQLSettings.SQLTABLE,
                        TykmaGlobals.SQLSettings.SQLPRIMARYFIELD,
                        TykmaGlobals.SQLSettings.SQLJOBCOLUMN,
                        TykmaGlobals.SQLSettings.SQLQTYCOLUMN);
                }
                else
                {
                    this.sql = new SQLClient(
                        TykmaGlobals.SQLSettings.SQLCONNSTRING,
                        TykmaGlobals.SQLSettings.SQLTABLE,
                        TykmaGlobals.SQLSettings.SQLPRIMARYFIELD,
                        TykmaGlobals.SQLSettings.SQLJOBCOLUMN,
                        TykmaGlobals.SQLSettings.SQLQTYCOLUMN);
                }

                this.ShowSQLState(Resources.Sql_Status_OK);
            }
        }

        /// <summary>
        /// Shows the SQL enabled status.
        /// </summary>
        /// <param name="state">The state.</param>
        private void ShowSQLState(string state)
        {
            this.ux_Status_SQL.Visible = true;
            this.ux_Status_SQL.Text = string.Format("{0}:{1}", "SQL", state);
        }

        /// <summary>
        /// Retrieves the SQL data.
        /// </summary>
        /// <param name="uniq">The unique identifier.</param>
        private async void RetrieveSQLData(string uniq)
        {
            // Show UX notification that we are pulling data and set cursor to busy.
            this.ShowSQLState(Resources.Sql_Status_Pull);
            this.Cursor = Cursors.WaitCursor;

            // Object that will hold our returned data.
            var returnData = new SQLRetrievedData();

            // Get the data.
            await Task.Run(() => returnData = this.sql.GetData(uniq));

            // Set cursor back.
            this.Cursor = Cursors.Default;

            // UX notification on fault.
            if (returnData.Fault)
            {
                this.ShowSQLState(Resources.Sql_Status_Fault);
                this.TimeStampEvent(returnData.FaultMessage, true);
                Logger.Fatal(returnData.FaultMessage);
                return;
            }

            // If the job name is empty, this is a fault.
            if (string.IsNullOrEmpty(returnData.JobName))
            {
                this.ShowSQLState(Resources.Sql_Status_NoJob);
                this.TimeStampEvent(Resources.Msg_Sql_JobFieldEmpty);
                Logger.Error(Resources.Msg_Sql_JobFieldEmpty);
            }
            else
            {
                // Load the job and set the data.
                if (await this.LoadJobAbstractor(returnData.JobName, false))
                {
                    this.ShowSQLState(returnData.JobName);
                    foreach (EntityInformation entity in returnData.RetrievedTags)
                    {
                        EntityInformation entity1 = entity;
                        await this.ChangeTextByName(entity1.Name.ToUpper(), entity1.Value);
                    }

                    if (TykmaGlobals.INISettings.USECOUNTER)
                    {
                        this.ux_Control_Quantity.Value = returnData.Quantity;
                    }

                    this.JobState = JobStatus.Status.DataSet;
                    this.UpdatePreview();
                    this.InitDataGridView();
                }
                else
                {
                    this.ShowSQLState(Resources.Sql_Status_JobNotFound);
                }
            }
        }
    }
}