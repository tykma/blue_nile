﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlFileSelect.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    using System.ComponentModel;
    using System.IO;
    using System.Windows.Forms;

    /// <summary>
    /// The control path select.
    /// </summary>
    public partial class ControlFileSelect : UserControl
    {
        /// <summary>
        /// The selected path.
        /// </summary>
        private FileInfo selectedFilePath;

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlFileSelect" /> class.
        /// </summary>
        public ControlFileSelect()
        {
            this.InitializeComponent();
            this.ux_Label_FilePath.Text = this.SelectedFilePath;
        }

        /// <summary>
        /// Gets or sets the path label.
        /// </summary>
        /// <value>
        /// The path label.
        /// </value>
        [Category("My Properties")]
        [Description("File Label")]
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public string PathLabel
        {
            get
            {
                return this.ux_Label_FilePath.Text;
            }

            set
            {
                this.ux_Label_FilePath.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets the selected path.
        /// </summary>
        /// <value>
        /// The selected path.
        /// </value>
        public string SelectedFilePath
        {
            get
            {
                if (this.selectedFilePath != null)
                {
                    return this.selectedFilePath.FullName;
                }

                return string.Empty;
            }

            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.selectedFilePath = new FileInfo(value);
                    this.OnPropertyChanged("SelectedFilePath");
                }
            }
        }

        /// <summary>
        /// Called when [property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        private void OnPropertyChanged(string propertyName)
        {
            if (propertyName == "SelectedFilePath")
            {
                this.ux_Text_PathInput.Text = this.SelectedFilePath;
            }
        }
    }
}
