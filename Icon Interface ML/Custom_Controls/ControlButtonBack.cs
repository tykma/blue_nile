﻿// <copyright file="ControlButtonBack.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Icon_Interface.Custom_Controls
{
    /// <summary>
    /// A 'back button' control.
    /// </summary>
    public partial class ControlButtonBack : ControlButtonBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlButtonBack"/> class.
        /// </summary>
        public ControlButtonBack()
        {
            this.InitializeComponent();
        }
    }
}
