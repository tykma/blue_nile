﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.AXIS.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// Axis control portion of the GUI.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Icon_Interface.General;
    using Icon_Interface.Properties;

    using Tykma.Core.Engine;
    using Tykma.Core.Engine.CustomExceptions;
    using Tykma.Icon.AxisInfo;
    using Tykma.Icon.InputParse;

    /// <summary>
    /// Status message to pass to events.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// The axis Z.
        /// </summary>
        private AxisSetting axisZ;

        /// <summary>
        /// The axis R.
        /// </summary>
        private AxisSetting axisR;

        /// <summary>
        /// Homes the required axes.
        /// </summary>
        /// <returns>Success state.</returns>
        private async Task<bool> HomeRequiredAxes()
        {
            var axesToHome = new List<AxisSetting>();
            if (this.axisR.Enabled && this.axisR.NeedsHoming)
            {
                axesToHome.Add(this.axisR);
            }

            if (this.axisZ.Enabled && this.axisZ.NeedsHoming)
            {
                axesToHome.Add(this.axisZ);
            }

            return await this.HomeAnAxis(axesToHome);
        }

        /// <summary>
        /// Sets the axis as homed.
        /// </summary>
        /// <param name="whichAxis">Which axis.</param>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        private void SetAxisAsHomed(AxisSetting whichAxis, bool enable)
        {
            if (whichAxis.WhichAxis == AxisSetting.Axis.Z)
            {
                this.axisZ.ResetHomeCounter();
                this.axisZ.Homed = enable;
                this.axisZ.Homing = false;
            }
            else if (whichAxis.WhichAxis == AxisSetting.Axis.R)
            {
                this.axisR.ResetHomeCounter();
                this.axisR.Homed = enable;
                this.axisR.Homing = false;
            }
        }

        /// <summary>
        /// Sets the axis as homing.
        /// </summary>
        /// <param name="whichAxis">Which axis.</param>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        private void SetAxisAsHoming(AxisSetting whichAxis, bool enable)
        {
            if (whichAxis.WhichAxis == AxisSetting.Axis.Z)
            {
                this.axisZ.Homing = enable;
            }
            else if (whichAxis.WhichAxis == AxisSetting.Axis.R)
            {
                this.axisR.Homing = enable;
            }
        }

        /// <summary>
        /// Homes an individual axis.
        /// </summary>
        /// <param name="axisToHome">The axis to home.</param>
        /// <returns>
        /// Success state.
        /// </returns>
        private async Task<bool> HomeAnAxis(AxisSetting axisToHome)
        {
            var listOfAxes = new List<AxisSetting> { axisToHome };
            return await this.HomeAnAxis(listOfAxes);
        }

        /// <summary>
        /// Homes a list of axes.
        /// </summary>
        /// <param name="listOfAxesToHome">The list of axes to home.</param>
        /// <returns>Success state.</returns>
        private async Task<bool> HomeAnAxis(List<AxisSetting> listOfAxesToHome)
        {
            foreach (var axis in listOfAxesToHome)
            {
                // This axis is not already home and will need to be homed.
                this.TimeStampEvent(string.Format("{0}: {1}", Resources.Msg_Homing_Axis, axis.CommonName));
                Logger.Info("{0}: {1}", Resources.Msg_Homing_Axis, axis.CommonName);

                // Set the axis state to 'homing'.
                this.SetAxisAsHoming(axis, true);

                try
                {
                    AxisSetting axis1 = axis;
                    var axisHomeResult = await Task.Run(() => this.le.AxisFindHome(axis1.Number));

                    if (this.le.Engine == EngineTypes.EngineType.PSE)
                    {
                        // Set the axis state to not homing.
                        this.SetAxisAsHoming(axis, false);

                        this.SetAxisAsHomed(axis, axisHomeResult);

                        if (axisHomeResult)
                        {
                            this.TimeStampEvent(string.Format("{0}: {1}", Resources.Msg_Homed_Axis, axis.CommonName));
                            Logger.Info("{0}: {1}", Resources.Msg_Homed_Axis, axis.CommonName);
                        }
                    }
                }
                catch (MethodFaultException e)
                {
                    this.TimeStampEvent(string.Format("{0}: {1} - {2}", Resources.Msg_Home_Axis_Fault, axis.CommonName, e.Message), true);
                    Logger.Fatal("{0} {1}, {2} - {3}", Resources.Msg_Home_Axis_Fault, Resources.Msg_Home_Axis_Error, axis.CommonName, e.Message);
                    this.SetAxisAsHoming(axis, false);
                    return false;
                }
                catch (EngineNotConnectedException)
                {
                    this.TimeStampEvent("Can not home axis - engine not connected");
                    Logger.Fatal("Can not home axis - engine not connected");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Initiates the axes.
        /// </summary>
        private void InitiateAxes()
        {
            this.axisZ = new AxisSetting
            {
                Enabled = TykmaGlobals.INISettings.ZAXISPRESENT,
                Number = TykmaGlobals.INISettings.ZAXISNUMBER,
                Homed = false,
                Homing = false,
                Letter = "Z",
                CommonName = Resources.Label_AxisZ,
                WhichAxis = AxisSetting.Axis.Z,
                NeedsHoming = true,
                HomeFrequency = TykmaGlobals.INISettings.ZHOMEFREQUENCY,
            };

            this.axisR = new AxisSetting
            {
                Enabled = TykmaGlobals.INISettings.RAXISPRESENT,
                Number = TykmaGlobals.INISettings.RAXISNUMBER,
                Homed = false,
                Homing = false,
                Letter = "R",
                CommonName = Resources.Label_AxisR,
                WhichAxis = AxisSetting.Axis.R,
                NeedsHoming = true,
                HomeFrequency = TykmaGlobals.INISettings.RHOMEFREQUENCY,
            };
        }

        /// <summary>
        /// Bumps the axis home counters.
        /// </summary>
        private void BumpAxisHomeCounters()
        {
            if (this.axisZ.Enabled)
            {
                this.axisZ.IncreaseHomeCounter();
            }

            if (this.axisR.Enabled)
            {
                this.axisR.IncreaseHomeCounter();
            }
        }

        /// <summary>
        /// Handles the Click event of the AxisSettingsSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void AxisSettingsSaveClick(object sender, EventArgs e)
        {
            TykmaGlobals.INISettings.HOMEONOPEN = this.ux_Check_HomeAxisOnStartup.Checked;
            TykmaGlobals.INISettings.RHOMEFREQUENCY = (int)this.ux_Numeric_RHomeFrequency.Value;
            TykmaGlobals.INISettings.ZHOMEFREQUENCY = (int)this.ux_Numeric_ZHomeFrequency.Value;
            TykmaGlobals.INISettings.ZAXISPRESENT = this.ux_CheckBox_AxisZEnabled.Checked;
            TykmaGlobals.INISettings.RAXISPRESENT = this.ux_CheckBox_AxisREnabled.Checked;
            this.TimeStampEvent("Axis settings saved.");
            Logger.Info("Axis settings saved");
        }
    }
}
