﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NativeMethods.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// A collection of some native methods.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Pinvokes
{
    using System;
    using System.Drawing;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Native methods holder class.
    /// </summary>
    internal static class NativeMethods
    {
        /// <summary>
        /// Draws the part of a parent control that is covered by a partially-transparent or alpha-blended child control.
        /// </summary>
        /// <param name="hWnd">The child control.</param>
        /// <param name="hdc">The child control's DC.</param>
        /// <param name="pRect">The area to be drawn.</param>
        /// <returns>HRESULT - S_OK or an error code.</returns>
        [DllImport("uxtheme", ExactSpelling = true)]
        public static extern int DrawThemeParentBackground(IntPtr hWnd, IntPtr hdc, ref Rectangle pRect);

        /// <summary>
        /// Sends the specified message to a window or windows.
        /// </summary>
        /// <param name="hWnd">A handle to the window whose window procedure will receive the message.</param>
        /// <param name="msg">The message to be sent.</param>
        /// <param name="wParam">Additional message-specific information.</param>
        /// <param name="lParam">The parameter.</param>
        /// <returns>
        /// LRESULT - result of the message processing.
        /// </returns>
        [DllImport("user32.dll", EntryPoint = "SendMessageW")]
        public static extern IntPtr SendMessageW(IntPtr hWnd, uint msg, IntPtr wParam, [MarshalAs(UnmanagedType.LPWStr)] string lParam);
    }
}
