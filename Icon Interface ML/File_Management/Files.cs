﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Files.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   File creation and management.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.File_Management
{
    using System;
    using System.Drawing;
    using System.IO;

    using Icon_Interface.General;
    using Icon_Interface.Properties;

    using Tykma.Icon.ImageBranding;

    /// <summary>
    /// Initializes a new instance of the <see cref="Files"/> class.
    /// </summary>
    internal class Files
    {
        /// <summary>
        /// The global variable file name.
        /// </summary>
        private const string GlobalVariableFile = "globals.xml";

        /// <summary>
        /// The image branding.
        /// </summary>
        private readonly ImageBranding imageBrands;

        /// <summary>
        /// The project folder.
        /// </summary>
        private DirectoryInfo projectFolder;

        /// <summary>
        /// The main folder.
        /// </summary>
        private DirectoryInfo mainFolder;

        /// <summary>
        /// The RTF folder.
        /// </summary>
        private string instructionsRTFFolder;

        /// <summary>
        /// The image folder.
        /// </summary>
        private string instructionsImageFolder;

        /// <summary>
        /// Initializes a new instance of the <see cref="Files" /> class.
        /// </summary>
        /// <param name="images">The images.</param>
        internal Files(ImageBranding images)
        {
            this.imageBrands = images;
            this.InitializePaths();
            this.CreateAllDirectories();
        }

        /// <summary>
        /// Gets or sets the project folder.
        /// </summary>
        /// <value>
        /// The project folder.
        /// </value>
        internal DirectoryInfo ProjectFolder
        {
            get
            {
                if (this.projectFolder != null)
                {
                    if (this.projectFolder.Exists)
                    {
                        return this.projectFolder;
                    }
                }

                return new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
            }

            set
            {
                this.projectFolder = value;
            }
        }

        /// <summary>
        /// Gets or sets the main folder.
        /// </summary>
        /// <value>
        /// The main folder.
        /// </value>
        internal DirectoryInfo MainFolder
        {
            get
            {
                if (this.mainFolder != null)
                {
                    if (this.mainFolder.Exists)
                    {
                        return this.mainFolder;
                    }
                }

                if (
                    Directory.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), @"Minilase Pro Se")))
                {
                    return
                        new DirectoryInfo(
                            Path.Combine(
                                Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles),
                                @"Minilase Pro Se"));
                }

                return new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles));
            }

            set
            {
                this.mainFolder = value;
            }
        }

        /// <summary>
        /// Gets or sets the instructions RTF folder.
        /// </summary>
        /// <value>
        /// The instructions RTF folder.
        /// </value>
        internal string InstructionsRtfFolder
        {
            get
            {
                return this.instructionsRTFFolder;
            }

            set
            {
                this.instructionsRTFFolder = value;
                TykmaGlobals.INISettings.INSTRUCTIONSRTFFOLDER = this.InstructionsRtfFolder;
            }
        }

        /// <summary>
        /// Gets or sets the instructions image folder.
        /// </summary>
        /// <value>
        /// The instructions image folder.
        /// </value>
        internal string InstructionsImageFolder
        {
            get
            {
                return this.instructionsImageFolder;
            }

            set
            {
                this.instructionsImageFolder = value;
                TykmaGlobals.INISettings.INSTRUCTIONSIMAGEFOLDER = this.InstructionsImageFolder;
            }
        }

        /// <summary>
        /// Created a Unique filename for the given filename.
        /// </summary>
        /// <param name="filename">A full filename.</param>
        /// <returns>A filename with a unique ID at the end.</returns>
        internal static string GetUniqueFilename(string filename)
        {
            if (string.IsNullOrWhiteSpace(filename))
            {
                filename = "null";
            }

            // Replace -(TEXT) with nothing.
            var stringRegex = new System.Text.RegularExpressions.Regex(@"\-\(([^\)]+)\)");
            filename = stringRegex.Replace(filename, string.Empty);

            // ReSharper disable once AssignNullToNotNullAttribute
            string basename = Path.Combine(Path.GetDirectoryName(filename), Path.GetFileNameWithoutExtension(filename));
            return string.Format("{0}-({1}){2}", basename, DateTime.Now.Ticks, Path.GetExtension(filename));
        }

        /// <summary>
        /// Overrides the configuration root path.
        /// </summary>
        /// <param name="path">The path.</param>
        internal void OverrideConfigRootPath(string path)
        {
            this.MainFolder = new DirectoryInfo(path);
        }

        /// <summary>
        /// Saves the paths.
        /// </summary>
        internal void SetPaths()
        {
            TykmaGlobals.INISettings.PROJECTSFOLDER = this.ProjectFolder.FullName;
            TykmaGlobals.INISettings.SEINSTALLDIR = this.MainFolder.FullName;
            TykmaGlobals.INISettings.INSTRUCTIONSRTFFOLDER = this.InstructionsRtfFolder;
            TykmaGlobals.INISettings.INSTRUCTIONSIMAGEFOLDER = this.InstructionsImageFolder;
        }

        /// <summary>
        /// Initializes the paths.
        /// </summary>
        internal void InitializePaths()
        {
            if (Path.IsPathRooted(TykmaGlobals.INISettings.PROJECTSFOLDER))
            {
                this.ProjectFolder = new DirectoryInfo(TykmaGlobals.INISettings.PROJECTSFOLDER);
            }

            if (Path.IsPathRooted(TykmaGlobals.INISettings.SEINSTALLDIR))
            {
                this.MainFolder = new DirectoryInfo(TykmaGlobals.INISettings.SEINSTALLDIR);
            }

            this.InstructionsRtfFolder = TykmaGlobals.INISettings.INSTRUCTIONSRTFFOLDER;
            this.InstructionsImageFolder = TykmaGlobals.INISettings.INSTRUCTIONSIMAGEFOLDER;
        }

        /// <summary>
        /// Gets the global variable file.
        /// </summary>
        /// <returns>A File info.</returns>
        internal FileInfo GetGlobalVariableFile()
        {
            return new FileInfo(Path.Combine(TykmaGlobals.INISettings.SETTINGSFOLDER, GlobalVariableFile));
        }

        /// <summary>
        /// Creates all directories for this folder.
        /// </summary>
        private void CreateAllDirectories()
        {
            var tempFolder = new DirectoryInfo(Path.Combine(Path.GetPathRoot(Environment.SystemDirectory), @"TYKMA", @"Custom", TykmaGlobals.ProjectName));

            if (!tempFolder.Exists)
            {
                tempFolder.Create();
            }

            if (!this.imageBrands.RTFMainFolder.Exists)
            {
                this.imageBrands.RTFMainFolder.Create();
                FileInfo defaultRTFFile = new FileInfo(Path.Combine(this.imageBrands.RTFMainFolder.FullName, "default.RTF"));
                File.WriteAllBytes(defaultRTFFile.FullName, Resources._defaultRTF);
            }

            if (!this.imageBrands.ImagesMainFolder.Exists)
            {
                this.imageBrands.ImagesMainFolder.Create();
                var defaultInstructions = new Bitmap(ImageResources._default);
                var probWithFile = new Bitmap(ImageResources.fileproblem1);
                var fileError = new Bitmap(ImageResources.error);
                var noJobLoaded = new Bitmap(ImageResources.nojobloaded1);
                var searching = new Bitmap(ImageResources.searching1);

                defaultInstructions.Save(Path.Combine(this.imageBrands.ImagesMainFolder.FullName, "default.png"));
                probWithFile.Save(Path.Combine(this.imageBrands.ImagesMainFolder.FullName, "fileproblem.png"));
                fileError.Save(Path.Combine(this.imageBrands.ImagesMainFolder.FullName, "error.png"));
                noJobLoaded.Save(Path.Combine(this.imageBrands.ImagesMainFolder.FullName, "nojobloaded.png"));
                searching.Save(Path.Combine(this.imageBrands.ImagesMainFolder.FullName, "searching.png"));
            }

            TykmaGlobals.INISettings.SETTINGSFOLDER = tempFolder.FullName;
        }
    }
}
