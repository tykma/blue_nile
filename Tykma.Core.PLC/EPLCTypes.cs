﻿// <copyright file="EPLCTypes.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Core.PLC
{
    /// <summary>
    /// PLC Types enumerator.
    /// </summary>
    public enum EPLCType
    {
        /// <summary>
        /// No PLC Configured.
        /// </summary>
        None,

        /// <summary>
        /// Control Logix PLCs.
        /// </summary>
        ABLogix,

        /// <summary>
        /// MIcroLogix PLCs.
        /// </summary>
        ABLink,

        /// <summary>
        /// Allen bradley micro.
        /// </summary>
        Micro,

        /// <summary>
        /// Siemens S7 1200.
        /// </summary>
        SiemensS7_1200,

        /// <summary>
        /// Siemens S7 400
        /// </summary>
        SiemensS7_400,

        /// <summary>
        /// Siemens S7 300
        /// </summary>
        SiemensS7_300,

        /// <summary>
        /// Siemens S7 200
        /// </summary>
        SiemensS7_200,
    }
}
