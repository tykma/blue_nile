﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdObjectGetSize.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;

    using Tykma.Core.Engine.CustomExceptions;
    using Tykma.Core.Entity;

    /// <summary>
    /// The command to get an object's size.
    /// </summary>
    internal class CmdObjectGetSize : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdObjectGetSize"/> class.
        /// </summary>
        /// <param name="lmcApi">The LMC API.</param>
        /// <param name="whichObject">The which object.</param>
        public CmdObjectGetSize(IntPtr lmcApi, string whichObject)
            : base(lmcApi)
        {
            this.WhichObject = whichObject;
        }

        /// <summary>
        /// Get entity size.
        /// </summary>
        /// <param name="pEntName">Name of the entity.</param>
        /// <param name="dMinX">The min X coordinate.</param>
        /// <param name="dMinY">The min Y coordinate</param>
        /// <param name="dMaxX">The max X coordinate..</param>
        /// <param name="dMaxY">The max Y coordinate</param>
        /// <param name="dZ">The coordinate Z.</param>
        /// <returns>Sizing information on an entity.</returns>
        private delegate int LMC1ApiGetEntSize(
                                                [MarshalAs(UnmanagedType.LPWStr)] string pEntName,
                                                out double dMinX,
                                                out double dMinY,
                                                out double dMaxX,
                                                out double dMaxY,
                                                out double dZ);

        /// <summary>
        /// Gets the object sizing.
        /// </summary>
        /// <value>
        /// Which object to get sizing from.
        /// </value>
        private string WhichObject { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Entity information.</returns>
        /// <exception cref="EngineNotConnectedException">Engine not connected.</exception>
        /// <exception cref="MethodFaultException">Problem executing command.</exception>
        internal EntityInformation Execute()
        {
            var entityInfo = new EntityInformation { Name = this.WhichObject };

            var lmc1GetEntSize = this.LoadFunction<LMC1ApiGetEntSize>("lmc1_GetEntSize");

            if (lmc1GetEntSize == null)
            {
                throw new EngineNotConnectedException();
            }

            double minx;
            double miny;
            double maxx;
            double maxy;
            double z;

            int result = lmc1GetEntSize(entityInfo.Name, out minx, out miny, out maxx, out maxy, out z);
            entityInfo.MaxX = maxx;
            entityInfo.MaxY = maxy;
            entityInfo.MinX = minx;
            entityInfo.MinY = miny;
            entityInfo.Z = z;

            ReturnCodeMessage rm = this.GetMiniLaseReturnCode(result);

            if (rm.Success)
            {
                return entityInfo;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
