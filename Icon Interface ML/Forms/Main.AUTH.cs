﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.AUTH.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// UI Authorizaton code.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Globalization;
    using System.Windows.Forms;

    using Icon_Interface.General;
    using Icon_Interface.Properties;

    /// <summary>
    /// AUTH partial class.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// The authorization class.
        /// </summary>
        private readonly Auth authorization;

        /// <summary>
        /// Boolean flag used to determine when a character other than a number is entered in password change.
        /// </summary>
        private bool nonNumberEntered;

        /// <summary>
        /// Handles the Click event of the PicKeyClear control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void PicKeyClearClick(object sender, EventArgs e)
        {
            this.ShowAuthForm();
        }

        /// <summary>
        /// Handles the Click event of the PicKeyOK control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void PicKeyOkClick(object sender, EventArgs e)
        {
            this.AttemptAuthorization(this.ux_Auth_MaskedKey.Tag.ToString());
        }

        /// <summary>
        /// Attempts the authorization.
        /// </summary>
        /// <param name="password">The password.</param>
        private void AttemptAuthorization(string password)
        {
            this.authorization.VerifyEngineerPassword(password);

            if (this.authorization.EngineerAuthorized)
            {
                this.TimeStampEvent(Resources.Msg_Engineer_Authorized);
                Logger.Info(Resources.Msg_Engineer_Authorized);
                this.ux_HMI.SelectedTab = this.ux_Tab_Main;
            }
            else
            {
                this.TimeStampEvent(Resources.Msg_Engineer_Not_Authorized, true);
                Logger.Info(Resources.Msg_Engineer_Not_Authorized);
                this.ShowAuthForm();
            }

            this.SetAccessOptions();
        }

        /// <summary>
        /// Shows the authorization form.
        /// </summary>
        private void ShowAuthForm()
        {
            this.ux_Auth_MaskedKey.Text = string.Empty;
            this.ux_Auth_MaskedKey.Tag = string.Empty;
            this.ux_HMI.SelectedTab = this.ux_Tab_Auth;

            this.ux_Auth_Level.Text = string.Format(
                                    "{0}: {1}",
                                    Resources.ToolStripAccess,
                                    this.authorization.EngineerAuthorized ? Resources.ToolStripEngineer : Resources.ToolStripOperator);

            this.SetAccessOptions();
        }

        /// <summary>
        /// Sets the access options.
        /// </summary>
        private void SetAccessOptions()
        {
            if (Settings.Default.EnableAuthentication)
            {
                this.ux_ToolStrip_Operator.Checked = !this.authorization.EngineerAuthorized;
                this.ux_ToolStrip_Engineer.Checked = this.authorization.EngineerAuthorized;
                this.ux_ToolStrip_PLCSettings.Enabled = this.authorization.EngineerAuthorized;
                this.ux_ToolStrip_Settings.Enabled = this.authorization.EngineerAuthorized;
                this.ux_ToolStrip_Card.Enabled = this.authorization.EngineerAuthorized;
                this.ux_Set_NewPassLabel.Visible = true;
                this.ux_Set_NewPassword.Visible = true;
                this.ux_ToolStrip_Access.Visible = true;
            }
            else
            {
                this.ux_ToolStrip_Access.Visible = false;
                this.ux_Set_NewPassLabel.Visible = false;
                this.ux_Set_NewPassword.Visible = false;
                this.ux_ToolStrip_PLCSettings.Enabled = true;
                this.ux_ToolStrip_Settings.Enabled = true;
            }
        }

        /// <summary>
        /// Handles the KeyDown event of the TextNewPassword control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs" /> instance containing the event data.</param>
        private void TextNewPasswordKeyDown(object sender, KeyEventArgs e)
        {
            // Initialize the flag to false.
            this.nonNumberEntered = false;

            // Determine whether the keystroke is a number from the top of the keyboard.
            if (e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9)
            {
                // Determine whether the keystroke is a number from the keypad.
                if (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9)
                {
                    // Determine whether the keystroke is a backspace.
                    if (e.KeyCode != Keys.Back)
                    {
                        // A non-numerical keystroke was pressed.
                        // Set the flag to true and evaluate in KeyPress event.
                        this.nonNumberEntered = true;
                    }
                }
            }
        }

        /// <summary>
        /// Handles the KeyPress event of the TextNewPassword control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyPressEventArgs" /> instance containing the event data.</param>
        private void TextNewPasswordKeyPress(object sender, KeyPressEventArgs e)
        {
            if (this.nonNumberEntered)
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Handles the Click event of the PicKey0 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void PicKey0Click(object sender, EventArgs e)
        {
            this.ux_Auth_MaskedKey.Text += Resources.Masked_Password_Key;
            this.ux_Auth_MaskedKey.Tag += "0";
        }

        /// <summary>
        /// Handles the Click event of the PicKey1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void PicKey1Click(object sender, EventArgs e)
        {
            this.ux_Auth_MaskedKey.Text += Resources.Masked_Password_Key;
            this.ux_Auth_MaskedKey.Tag += "1";
        }

        /// <summary>
        /// Handles the Click event of the PicKey2 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void PicKey2Click(object sender, EventArgs e)
        {
            this.ux_Auth_MaskedKey.Text += Resources.Masked_Password_Key;
            this.ux_Auth_MaskedKey.Tag += "2";
        }

        /// <summary>
        /// Handles the Click event of the PicKey3 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void PicKey3Click(object sender, EventArgs e)
        {
            this.ux_Auth_MaskedKey.Text += Resources.Masked_Password_Key;
            this.ux_Auth_MaskedKey.Tag += "3";
        }

        /// <summary>
        /// Handles the Click event of the PicKey4 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void PicKey4Click(object sender, EventArgs e)
        {
            this.ux_Auth_MaskedKey.Text += Resources.Masked_Password_Key;
            this.ux_Auth_MaskedKey.Tag += "4";
        }

        /// <summary>
        /// Handles the Click event of the PicKey5 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void PicKey5Click(object sender, EventArgs e)
        {
            this.ux_Auth_MaskedKey.Text += Resources.Masked_Password_Key;
            this.ux_Auth_MaskedKey.Tag += "5";
        }

        /// <summary>
        /// Handles the Click event of the PicKey6 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void PicKey6Click(object sender, EventArgs e)
        {
            this.ux_Auth_MaskedKey.Text += Resources.Masked_Password_Key;
            this.ux_Auth_MaskedKey.Tag += "6";
        }

        /// <summary>
        /// Handles the Click event of the PicKey7 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void PicKey7Click(object sender, EventArgs e)
        {
            this.ux_Auth_MaskedKey.Text += Resources.Masked_Password_Key;
            this.ux_Auth_MaskedKey.Tag += "7";
        }

        /// <summary>
        /// Handles the Click event of the PicKey8 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void PicKey8Click(object sender, EventArgs e)
        {
            this.ux_Auth_MaskedKey.Text += Resources.Masked_Password_Key;
            this.ux_Auth_MaskedKey.Tag += "8";
        }

        /// <summary>
        /// Handles the Click event of the PicKey9 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void PicKey9Click(object sender, EventArgs e)
        {
            this.ux_Auth_MaskedKey.Text += Resources.Masked_Password_Key;
            this.ux_Auth_MaskedKey.Tag += "9";
        }

        /// <summary>
        /// Authentications the key entered.
        /// </summary>
        /// <param name="e">The <see cref="KeyPressEventArgs" /> instance containing the event data.</param>
        private void AuthKeyEntered(KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar))
            {
                this.ux_Auth_MaskedKey.Text += Resources.Masked_Password_Key;
                this.ux_Auth_MaskedKey.Tag += e.KeyChar.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                if (e.KeyChar == Convert.ToChar(Keys.Return))
                {
                    this.AttemptAuthorization(this.ux_Auth_MaskedKey.Tag.ToString());
                }
                else if (e.KeyChar == Convert.ToChar(Keys.Back))
                {
                    string currentTag = this.ux_Auth_MaskedKey.Tag.ToString();
                    if (currentTag.Length > 0)
                    {
                        this.ux_Auth_MaskedKey.Tag = currentTag.Remove(currentTag.Length - 1);
                        this.ux_Auth_MaskedKey.Text = this.ux_Auth_MaskedKey.Text.Remove(this.ux_Auth_MaskedKey.Text.Length - 1);
                    }
                }
            }
        }
    }
}
