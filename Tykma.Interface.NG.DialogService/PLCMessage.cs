﻿// <copyright file="PLCMessage.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Interface.NG.DialogService
{
    using System;
    using System.Drawing;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    /// <summary>
    /// A PLC Message form.
    /// </summary>
    public sealed partial class PLCMessage : Form
    {
#pragma warning disable SA1600 // Missing XML comment for publicly visible type or member

#pragma warning disable SA1310 // Field names should not contain underscore

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        public const int WM_NCLBUTTONDOWN = 0xA1;

        public const int HT_CAPTION = 0x2;

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member

#pragma warning restore SA1310 // Field names should not contain underscore

        private const int HTLEFT = 10;

        private const int HTRIGHT = 11;

        private const int HTTOP = 12;

        private const int HTTOPLEFT = 13;

        private const int HTTOPRIGHT = 14;

        private const int HTBOTTOM = 15;

        private const int HTBOTTOMLEFT = 16;

        private const int HTBOTTOMRIGHT = 17;

#pragma warning restore SA1600 // Missing XML comment for publicly visible type or member

        /// <summary>
        /// Form padding.
        /// </summary>
        private const int PaddingValue = 10;

        /// <summary>
        /// Initializes a new instance of the <see cref="PLCMessage"/> class.
        /// </summary>
        /// <param name="initLocation">Desired dimensions..</param>
        public PLCMessage(string initLocation)
        {
            var desiredMonitor = string.Empty;
            var il = new Point(0, 0);
            Size sz = this.Size;
            if (!string.IsNullOrWhiteSpace(initLocation))
            {
                string[] parts = initLocation.Split(',');
                if (parts.Length >= 2)
                {
                    il = new Point(int.Parse(parts[0]), int.Parse(parts[1]));
                }

                if (parts.Length >= 4)
                {
                    sz = new Size(int.Parse(parts[2]), int.Parse(parts[3]));
                }

                if (parts.Length >= 5)
                {
                    desiredMonitor = parts[4];
                }
            }

            this.DesiredSize = sz;
            this.DesiredLocation = il;

            foreach (var screen in Screen.AllScreens)
            {
                if (screen.DeviceName.Equals(desiredMonitor))
                {
                    var workingArea = screen.WorkingArea;
                    this.Left = workingArea.Left;
                    this.Top = workingArea.Top;
                    this.Width = workingArea.Width;
                    this.Height = workingArea.Height;
                    break;
                }
            }

            this.FormBorderStyle = FormBorderStyle.None;
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.ResizeRedraw, true);

            this.InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the message to display.
        /// </summary>
        public string Message
        {
            get => this.ux_Message.Text;

            set => this.ux_Message.Text = value;
        }

        /// <summary>
        /// Gets or sets the status code number.
        /// </summary>
        public string StatusCode
        {
            get => this.ux_label_Status_Code.Text;

            set => this.ux_label_Status_Code.Text = value;
        }

        /// <summary>
        /// Gets or sets the color to display the status meessag ein.
        /// </summary>
        public string ColorText
        {
            get => this.ux_Message.ForeColor.ToString();

            set
            {
                Color color = Color.White;

                if (value != null)
                {
                    color = Color.FromName(value);

                    if (color == Color.Empty)
                    {
                        color = Color.White;
                    }
                }

                this.ux_Message.ForeColor = color;
            }
        }

        private Rectangle TopS => new Rectangle(0, 0, this.ClientSize.Width, PaddingValue);

        private Rectangle LeftS => new Rectangle(0, 0, PaddingValue, this.ClientSize.Height);

        private new Rectangle Bottom => new Rectangle(0, this.ClientSize.Height - PaddingValue, this.ClientSize.Width, PaddingValue);

        private Rectangle RightS => new Rectangle(this.ClientSize.Width - PaddingValue, 0, PaddingValue, this.ClientSize.Height);

        private Rectangle TopLeft => new Rectangle(0, 0, PaddingValue, PaddingValue);

        private Rectangle TopRight => new Rectangle(this.ClientSize.Width - PaddingValue, 0, PaddingValue, PaddingValue);

        private Rectangle BottomLeft => new Rectangle(0, this.ClientSize.Height - PaddingValue, PaddingValue, PaddingValue);

        private Rectangle BottomRight => new Rectangle(this.ClientSize.Width - PaddingValue, this.ClientSize.Height - PaddingValue, PaddingValue, PaddingValue);

        /// <summary>
        /// Gets the desired form size.
        /// </summary>
        private Size DesiredSize { get; }

        /// <summary>
        /// Gets the desired form location.
        /// </summary>
        private Point DesiredLocation { get; }

        /// <summary>
        /// Sends the specified message to a window or windows.
        /// The SendMessage function calls the window procedure for the specified window and does not return until the window procedure has processed the message.
        /// </summary>
        /// <param name="hWnd">The window handle to send the message to.</param>
        /// <param name="msg">The id of the message to send.</param>
        /// <param name="wParam">The w parameter.</param>
        /// <param name="lParam">The l parameter.</param>
        /// <returns>
        /// The return value specifies the result of the message processing; it depends on the message sent.
        /// </returns>
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int msg, int wParam, int lParam);

        /// <summary>
        /// Release mouse capture.
        /// </summary>
        /// <returns>Return value.</returns>
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        /// <inheritdoc />
        protected override void WndProc(ref Message message)
        {
            base.WndProc(ref message);

            if (message.Msg == 0x84)
            {
                var cursor = this.PointToClient(Cursor.Position);

                if (this.TopLeft.Contains(cursor))
                {
                    message.Result = (IntPtr)HTTOPLEFT;
                }
                else if (this.TopRight.Contains(cursor))
                {
                    message.Result = (IntPtr)HTTOPRIGHT;
                }
                else if (this.BottomLeft.Contains(cursor))
                {
                    message.Result = (IntPtr)HTBOTTOMLEFT;
                }
                else if (this.BottomRight.Contains(cursor))
                {
                    message.Result = (IntPtr)HTBOTTOMRIGHT;
                }
                else if (this.TopS.Contains(cursor))
                {
                    message.Result = (IntPtr)HTTOP;
                }
                else if (this.LeftS.Contains(cursor))
                {
                    message.Result = (IntPtr)HTLEFT;
                }
                else if (this.RightS.Contains(cursor))
                {
                    message.Result = (IntPtr)HTRIGHT;
                }
                else if (this.Bottom.Contains(cursor))
                {
                    message.Result = (IntPtr)HTBOTTOM;
                }
            }
        }

        /// <inheritdoc/>
        protected override void OnPaint(PaintEventArgs e) // you can safely omit this method if you want
        {
            e.Graphics.FillRectangle(Brushes.White, this.TopS);
            e.Graphics.FillRectangle(Brushes.White, this.LeftS);
            e.Graphics.FillRectangle(Brushes.White, this.RightS);
            e.Graphics.FillRectangle(Brushes.White, this.Bottom);
        }

        /// <summary>
        /// Handle the form closing event.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SimpleMessage_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        /// <summary>
        /// Handle form loading events.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void SimpleMessage_Load(object sender, EventArgs e)
        {
            this.Location = this.DesiredLocation;
            this.Size = this.DesiredSize;

            // this.Location = this.Pos;
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(this.Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
    }
}