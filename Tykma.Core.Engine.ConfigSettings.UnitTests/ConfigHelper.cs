﻿// <copyright file="ConfigHelper.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.Engine.ConfigSettings.UnitTests
{
    using System.Collections.Generic;
    using Tykma.Core.Engine.ConfigSettings.UnitTests.Properties;

    /// <summary>
    /// Configuration test helpers.
    /// </summary>
    internal static class ConfigHelper
    {
        /// <summary>
        /// Get a sample file.
        /// </summary>
        /// <returns>A sample markcfg file.</returns>
        internal static IList<string> GetSampleFile()
        {
            var file = Resources.markcfg7;
            var str = System.Text.Encoding.Default.GetString(file);

            var l = new List<string>(str.Replace("\n", string.Empty).Split('\r'));

            return l;
        }
    }
}
