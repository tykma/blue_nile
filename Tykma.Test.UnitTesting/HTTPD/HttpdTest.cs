﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HttpdTest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.HTTPD
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Net;
    using System.Threading;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Core.HTTPD;

    /// <summary>
    /// Testing for the TYKMA HTTPD class.
    /// </summary>
    [TestClass]
    public class HttpdTest
    {
        /// <summary>
        /// Test that an invalid port throws an exception.
        /// </summary>
        [TestMethod]
        [ExcludeFromCodeCoverage]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestInvalidPortException()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new Server(-500);
        }

        /// <summary>
        /// Tests the disconnection.
        /// </summary>
        [TestMethod]
        public void TestDisconnect()
        {
            var server = new Server(80);
            server.Disconnect();
            Assert.IsFalse(server.IsConnected);
        }

        /// <summary>
        /// Tests the connection.
        /// </summary>
        [TestMethod]
        public void TestConnection()
        {
            var server = new Server(80);
            Assert.AreEqual(true, server.IsConnected);
            server.Disconnect();
        }

        /// <summary>
        /// Tests the client connection event.
        /// </summary>
        [TestMethod]
        public void TestClientConnectionEvent()
        {
            var server = new Server(80);
            ManualResetEvent connectionUpdateEvent = new ManualResetEvent(false);

            bool eventReceived = false;

            // We will need to verify that we get an event back and that the 'status' of this event is connected.
            server.SocketConnected += (sender, e) =>
                {
                    ServerClient theclient = e.Client;

                    _ = theclient.SendResponse("200", "text", "sent");

                    eventReceived = true;
                    connectionUpdateEvent.Set();
                };

            var http = (HttpWebRequest)WebRequest.Create("http://127.0.0.1:80/version");
            var response = http.GetResponse();
            response.GetResponseStream();

            // Wait for the event.
            connectionUpdateEvent.WaitOne(500, false);

            server.Disconnect();
            Assert.IsTrue(eventReceived);
        }

        /// <summary>
        /// Tests the that server sends proper data.
        /// </summary>
        [TestMethod]
        public void TestThatServerSendsProperData()
        {
            var server = new Server(80);
            ManualResetEvent connectionUpdateEvent = new ManualResetEvent(false);

            // We will need to verify that we get an event back and that the 'status' of this event is connected.
            server.SocketConnected += (sender, e) =>
                {
                    ServerClient theclient = e.Client;

                    // ReSharper disable once UnusedVariable
                    var line = theclient.Do().Result;

#pragma warning disable 4014
                    theclient.SendResponse("200", "text", "CONFIRM");
#pragma warning restore 4014

                    connectionUpdateEvent.Set();
                };

            var client = new WebClient();

            string result = client.DownloadString("http://localhost:80/version");

            connectionUpdateEvent.WaitOne(500, false);

            server.Disconnect();
            Assert.AreEqual("CONFIRM", result);
        }
    }
}
