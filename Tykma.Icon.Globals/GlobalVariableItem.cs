﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlobalVariableItem.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Tykma.Icon.Globals
{
    using Tykma.Core.Entity;

    /// <summary>
    /// A global variable item.
    /// </summary>
    public class GlobalVariableItem : EntityInformation, System.ICloneable
    {
        /// <summary>
        /// The name.
        /// </summary>
        private string name;

        /// <summary>
        /// Gets or sets a value indicating whether [counter].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [counter]; otherwise, <c>false</c>.
        /// </value>
        public bool Counter { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [alpha numeric].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [alpha numeric]; otherwise, <c>false</c>.
        /// </value>
        public bool AlphaNumeric { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has been set.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has been set; otherwise, <c>false</c>.
        /// </value>
        public bool HasBeenSet { get; set; }

        /// <summary>
        /// Gets or sets the name of the object.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public new string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
                this.HasBeenSet = true;
            }
        }

        /// <summary>
        /// Gets the display value.
        /// </summary>
        /// <value>
        /// The display value.
        /// </value>
        public string DisplayValue
        {
            get
            {
                if (this.Counter)
                {
                    if (this.AlphaNumeric)
                    {
                        ISerializer serializer = new SerialBase26();
                        return serializer.GetValue(this.Value);
                    }
                }

                return this.Value;
            }
        }

        /// <inheritdoc/>
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
