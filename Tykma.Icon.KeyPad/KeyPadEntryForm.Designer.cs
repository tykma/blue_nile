﻿namespace Tykma.Icon.KeyPad
{
    sealed partial class KeyPadEntryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KeyPadEntryForm));
            this.ux_Layout_AuthMain = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Layout_KeyPadKeys = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Pic_KeyOk = new System.Windows.Forms.PictureBox();
            this.ux_Pic_Key0 = new System.Windows.Forms.PictureBox();
            this.ux_Pic_KeyTrash = new System.Windows.Forms.PictureBox();
            this.ux_Pic_Key9 = new System.Windows.Forms.PictureBox();
            this.ux_Pic_Key8 = new System.Windows.Forms.PictureBox();
            this.ux_Pic_Key7 = new System.Windows.Forms.PictureBox();
            this.ux_Pic_Key6 = new System.Windows.Forms.PictureBox();
            this.ux_Pic_Key5 = new System.Windows.Forms.PictureBox();
            this.ux_Pic_Key4 = new System.Windows.Forms.PictureBox();
            this.ux_Pic_Key3 = new System.Windows.Forms.PictureBox();
            this.ux_Pic_Key2 = new System.Windows.Forms.PictureBox();
            this.ux_Pic_Key1 = new System.Windows.Forms.PictureBox();
            this.ux_Auth_MaskedKey = new System.Windows.Forms.Label();
            this.ux_Auth_Level = new System.Windows.Forms.Label();
            this.ux_Layout_AuthMain.SuspendLayout();
            this.ux_Layout_KeyPadKeys.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_KeyOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_KeyTrash)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key1)).BeginInit();
            this.SuspendLayout();
            // 
            // ux_Layout_AuthMain
            // 
            this.ux_Layout_AuthMain.ColumnCount = 3;
            this.ux_Layout_AuthMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.ux_Layout_AuthMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.ux_Layout_AuthMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.ux_Layout_AuthMain.Controls.Add(this.ux_Layout_KeyPadKeys, 1, 2);
            this.ux_Layout_AuthMain.Controls.Add(this.ux_Auth_MaskedKey, 1, 1);
            this.ux_Layout_AuthMain.Controls.Add(this.ux_Auth_Level, 1, 0);
            this.ux_Layout_AuthMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_AuthMain.Location = new System.Drawing.Point(0, 0);
            this.ux_Layout_AuthMain.Name = "ux_Layout_AuthMain";
            this.ux_Layout_AuthMain.RowCount = 3;
            this.ux_Layout_AuthMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.ux_Layout_AuthMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.ux_Layout_AuthMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_Layout_AuthMain.Size = new System.Drawing.Size(384, 461);
            this.ux_Layout_AuthMain.TabIndex = 2;
            // 
            // ux_Layout_KeyPadKeys
            // 
            this.ux_Layout_KeyPadKeys.ColumnCount = 3;
            this.ux_Layout_KeyPadKeys.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_Layout_KeyPadKeys.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.ux_Layout_KeyPadKeys.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Pic_KeyOk, 2, 3);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Pic_Key0, 1, 3);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Pic_KeyTrash, 0, 3);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Pic_Key9, 2, 2);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Pic_Key8, 1, 2);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Pic_Key7, 0, 2);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Pic_Key6, 2, 1);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Pic_Key5, 1, 1);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Pic_Key4, 0, 1);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Pic_Key3, 2, 0);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Pic_Key2, 1, 0);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Pic_Key1, 0, 0);
            this.ux_Layout_KeyPadKeys.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_KeyPadKeys.Location = new System.Drawing.Point(41, 69);
            this.ux_Layout_KeyPadKeys.Name = "ux_Layout_KeyPadKeys";
            this.ux_Layout_KeyPadKeys.RowCount = 4;
            this.ux_Layout_KeyPadKeys.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_Layout_KeyPadKeys.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_Layout_KeyPadKeys.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_Layout_KeyPadKeys.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_Layout_KeyPadKeys.Size = new System.Drawing.Size(301, 389);
            this.ux_Layout_KeyPadKeys.TabIndex = 0;
            // 
            // ux_Pic_KeyOk
            // 
            this.ux_Pic_KeyOk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Pic_KeyOk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Pic_KeyOk.Image = global::Tykma.Icon.KeyPad.Properties.Resources.key_ok;
            this.ux_Pic_KeyOk.Location = new System.Drawing.Point(203, 294);
            this.ux_Pic_KeyOk.Name = "ux_Pic_KeyOk";
            this.ux_Pic_KeyOk.Size = new System.Drawing.Size(95, 92);
            this.ux_Pic_KeyOk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Pic_KeyOk.TabIndex = 11;
            this.ux_Pic_KeyOk.TabStop = false;
            this.ux_Pic_KeyOk.Click += new System.EventHandler(this.PIcKeyOkClick);
            // 
            // ux_Pic_Key0
            // 
            this.ux_Pic_Key0.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Pic_Key0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Pic_Key0.Image = ((System.Drawing.Image)(resources.GetObject("ux_Pic_Key0.Image")));
            this.ux_Pic_Key0.Location = new System.Drawing.Point(103, 294);
            this.ux_Pic_Key0.Name = "ux_Pic_Key0";
            this.ux_Pic_Key0.Size = new System.Drawing.Size(94, 92);
            this.ux_Pic_Key0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ux_Pic_Key0.TabIndex = 10;
            this.ux_Pic_Key0.TabStop = false;
            // 
            // ux_Pic_KeyTrash
            // 
            this.ux_Pic_KeyTrash.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Pic_KeyTrash.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Pic_KeyTrash.Image = global::Tykma.Icon.KeyPad.Properties.Resources.key_delete;
            this.ux_Pic_KeyTrash.Location = new System.Drawing.Point(3, 294);
            this.ux_Pic_KeyTrash.Name = "ux_Pic_KeyTrash";
            this.ux_Pic_KeyTrash.Size = new System.Drawing.Size(94, 92);
            this.ux_Pic_KeyTrash.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Pic_KeyTrash.TabIndex = 9;
            this.ux_Pic_KeyTrash.TabStop = false;
            this.ux_Pic_KeyTrash.Click += new System.EventHandler(this.PicKeyTrashClick);
            // 
            // ux_Pic_Key9
            // 
            this.ux_Pic_Key9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Pic_Key9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Pic_Key9.Image = ((System.Drawing.Image)(resources.GetObject("ux_Pic_Key9.Image")));
            this.ux_Pic_Key9.Location = new System.Drawing.Point(203, 197);
            this.ux_Pic_Key9.Name = "ux_Pic_Key9";
            this.ux_Pic_Key9.Size = new System.Drawing.Size(95, 91);
            this.ux_Pic_Key9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Pic_Key9.TabIndex = 8;
            this.ux_Pic_Key9.TabStop = false;
            // 
            // ux_Pic_Key8
            // 
            this.ux_Pic_Key8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Pic_Key8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Pic_Key8.Image = ((System.Drawing.Image)(resources.GetObject("ux_Pic_Key8.Image")));
            this.ux_Pic_Key8.Location = new System.Drawing.Point(103, 197);
            this.ux_Pic_Key8.Name = "ux_Pic_Key8";
            this.ux_Pic_Key8.Size = new System.Drawing.Size(94, 91);
            this.ux_Pic_Key8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Pic_Key8.TabIndex = 7;
            this.ux_Pic_Key8.TabStop = false;
            // 
            // ux_Pic_Key7
            // 
            this.ux_Pic_Key7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Pic_Key7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Pic_Key7.Image = ((System.Drawing.Image)(resources.GetObject("ux_Pic_Key7.Image")));
            this.ux_Pic_Key7.Location = new System.Drawing.Point(3, 197);
            this.ux_Pic_Key7.Name = "ux_Pic_Key7";
            this.ux_Pic_Key7.Size = new System.Drawing.Size(94, 91);
            this.ux_Pic_Key7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Pic_Key7.TabIndex = 6;
            this.ux_Pic_Key7.TabStop = false;
            // 
            // ux_Pic_Key6
            // 
            this.ux_Pic_Key6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Pic_Key6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Pic_Key6.ErrorImage = null;
            this.ux_Pic_Key6.Image = ((System.Drawing.Image)(resources.GetObject("ux_Pic_Key6.Image")));
            this.ux_Pic_Key6.Location = new System.Drawing.Point(203, 100);
            this.ux_Pic_Key6.Name = "ux_Pic_Key6";
            this.ux_Pic_Key6.Size = new System.Drawing.Size(95, 91);
            this.ux_Pic_Key6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Pic_Key6.TabIndex = 5;
            this.ux_Pic_Key6.TabStop = false;
            // 
            // ux_Pic_Key5
            // 
            this.ux_Pic_Key5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Pic_Key5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Pic_Key5.Image = ((System.Drawing.Image)(resources.GetObject("ux_Pic_Key5.Image")));
            this.ux_Pic_Key5.Location = new System.Drawing.Point(103, 100);
            this.ux_Pic_Key5.Name = "ux_Pic_Key5";
            this.ux_Pic_Key5.Size = new System.Drawing.Size(94, 91);
            this.ux_Pic_Key5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Pic_Key5.TabIndex = 4;
            this.ux_Pic_Key5.TabStop = false;
            // 
            // ux_Pic_Key4
            // 
            this.ux_Pic_Key4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Pic_Key4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Pic_Key4.Image = ((System.Drawing.Image)(resources.GetObject("ux_Pic_Key4.Image")));
            this.ux_Pic_Key4.Location = new System.Drawing.Point(3, 100);
            this.ux_Pic_Key4.Name = "ux_Pic_Key4";
            this.ux_Pic_Key4.Size = new System.Drawing.Size(94, 91);
            this.ux_Pic_Key4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Pic_Key4.TabIndex = 3;
            this.ux_Pic_Key4.TabStop = false;
            // 
            // ux_Pic_Key3
            // 
            this.ux_Pic_Key3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Pic_Key3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Pic_Key3.Image = ((System.Drawing.Image)(resources.GetObject("ux_Pic_Key3.Image")));
            this.ux_Pic_Key3.Location = new System.Drawing.Point(203, 3);
            this.ux_Pic_Key3.Name = "ux_Pic_Key3";
            this.ux_Pic_Key3.Size = new System.Drawing.Size(95, 91);
            this.ux_Pic_Key3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Pic_Key3.TabIndex = 2;
            this.ux_Pic_Key3.TabStop = false;
            // 
            // ux_Pic_Key2
            // 
            this.ux_Pic_Key2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Pic_Key2.Image = ((System.Drawing.Image)(resources.GetObject("ux_Pic_Key2.Image")));
            this.ux_Pic_Key2.Location = new System.Drawing.Point(103, 3);
            this.ux_Pic_Key2.Name = "ux_Pic_Key2";
            this.ux_Pic_Key2.Size = new System.Drawing.Size(94, 91);
            this.ux_Pic_Key2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Pic_Key2.TabIndex = 1;
            this.ux_Pic_Key2.TabStop = false;
            // 
            // ux_Pic_Key1
            // 
            this.ux_Pic_Key1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Pic_Key1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Pic_Key1.Image = ((System.Drawing.Image)(resources.GetObject("ux_Pic_Key1.Image")));
            this.ux_Pic_Key1.Location = new System.Drawing.Point(3, 3);
            this.ux_Pic_Key1.Name = "ux_Pic_Key1";
            this.ux_Pic_Key1.Size = new System.Drawing.Size(94, 91);
            this.ux_Pic_Key1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Pic_Key1.TabIndex = 0;
            this.ux_Pic_Key1.TabStop = false;
            // 
            // ux_Auth_MaskedKey
            // 
            this.ux_Auth_MaskedKey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Auth_MaskedKey.AutoSize = true;
            this.ux_Auth_MaskedKey.BackColor = System.Drawing.Color.LemonChiffon;
            this.ux_Auth_MaskedKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Auth_MaskedKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Auth_MaskedKey.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.ux_Auth_MaskedKey.Location = new System.Drawing.Point(38, 34);
            this.ux_Auth_MaskedKey.Margin = new System.Windows.Forms.Padding(0);
            this.ux_Auth_MaskedKey.Name = "ux_Auth_MaskedKey";
            this.ux_Auth_MaskedKey.Size = new System.Drawing.Size(307, 32);
            this.ux_Auth_MaskedKey.TabIndex = 1;
            this.ux_Auth_MaskedKey.Text = "12345";
            this.ux_Auth_MaskedKey.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ux_Auth_Level
            // 
            this.ux_Auth_Level.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Auth_Level.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Auth_Level.ForeColor = System.Drawing.Color.SteelBlue;
            this.ux_Auth_Level.Location = new System.Drawing.Point(41, 0);
            this.ux_Auth_Level.Name = "ux_Auth_Level";
            this.ux_Auth_Level.Size = new System.Drawing.Size(301, 34);
            this.ux_Auth_Level.TabIndex = 2;
            this.ux_Auth_Level.Text = "Input Quantity";
            this.ux_Auth_Level.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // KeyPadEntryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 461);
            this.Controls.Add(this.ux_Layout_AuthMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "KeyPadEntryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Keypad Entry";
            this.ux_Layout_AuthMain.ResumeLayout(false);
            this.ux_Layout_AuthMain.PerformLayout();
            this.ux_Layout_KeyPadKeys.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_KeyOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_KeyTrash)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Pic_Key1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel ux_Layout_AuthMain;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_KeyPadKeys;
        private System.Windows.Forms.Label ux_Auth_MaskedKey;
        private System.Windows.Forms.Label ux_Auth_Level;
        private System.Windows.Forms.PictureBox ux_Pic_KeyOk;
        private System.Windows.Forms.PictureBox ux_Pic_Key0;
        private System.Windows.Forms.PictureBox ux_Pic_KeyTrash;
        private System.Windows.Forms.PictureBox ux_Pic_Key9;
        private System.Windows.Forms.PictureBox ux_Pic_Key8;
        private System.Windows.Forms.PictureBox ux_Pic_Key7;
        private System.Windows.Forms.PictureBox ux_Pic_Key6;
        private System.Windows.Forms.PictureBox ux_Pic_Key5;
        private System.Windows.Forms.PictureBox ux_Pic_Key4;
        private System.Windows.Forms.PictureBox ux_Pic_Key3;
        private System.Windows.Forms.PictureBox ux_Pic_Key2;
        private System.Windows.Forms.PictureBox ux_Pic_Key1;
    }
}