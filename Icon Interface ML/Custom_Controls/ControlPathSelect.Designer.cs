﻿namespace Icon_Interface.Custom_Controls
{
    partial class ControlPathSelect
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ux_tlp_Design = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Button_ChooseFolder = new System.Windows.Forms.PictureBox();
            this.ux_Text_PathInput = new System.Windows.Forms.TextBox();
            this.ux_Label_Path = new System.Windows.Forms.Label();
            this.ux_tlp_Design.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Button_ChooseFolder)).BeginInit();
            this.SuspendLayout();
            // 
            // ux_tlp_Design
            // 
            this.ux_tlp_Design.ColumnCount = 3;
            this.ux_tlp_Design.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.ux_tlp_Design.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 86.17747F));
            this.ux_tlp_Design.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.82253F));
            this.ux_tlp_Design.Controls.Add(this.ux_Button_ChooseFolder, 0, 0);
            this.ux_tlp_Design.Controls.Add(this.ux_Text_PathInput, 0, 0);
            this.ux_tlp_Design.Controls.Add(this.ux_Label_Path, 0, 0);
            this.ux_tlp_Design.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_tlp_Design.Location = new System.Drawing.Point(0, 0);
            this.ux_tlp_Design.Name = "ux_tlp_Design";
            this.ux_tlp_Design.RowCount = 1;
            this.ux_tlp_Design.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_tlp_Design.Size = new System.Drawing.Size(758, 36);
            this.ux_tlp_Design.TabIndex = 0;
            // 
            // ux_Button_ChooseFolder
            // 
            this.ux_Button_ChooseFolder.BackgroundImage = global::Icon_Interface.Properties.ImageResources.folder_open;
            this.ux_Button_ChooseFolder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ux_Button_ChooseFolder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Button_ChooseFolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Button_ChooseFolder.ErrorImage = null;
            this.ux_Button_ChooseFolder.Location = new System.Drawing.Point(683, 3);
            this.ux_Button_ChooseFolder.Name = "ux_Button_ChooseFolder";
            this.ux_Button_ChooseFolder.Size = new System.Drawing.Size(72, 30);
            this.ux_Button_ChooseFolder.TabIndex = 35;
            this.ux_Button_ChooseFolder.TabStop = false;
            this.ux_Button_ChooseFolder.Click += new System.EventHandler(this.UxButtonChooseFolderClick);
            // 
            // ux_Text_PathInput
            // 
            this.ux_Text_PathInput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Text_PathInput.Location = new System.Drawing.Point(203, 8);
            this.ux_Text_PathInput.Name = "ux_Text_PathInput";
            this.ux_Text_PathInput.Size = new System.Drawing.Size(474, 20);
            this.ux_Text_PathInput.TabIndex = 34;
            this.ux_Text_PathInput.TextChanged += new System.EventHandler(this.TextChangedEvent);
            // 
            // ux_Label_Path
            // 
            this.ux_Label_Path.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Label_Path.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Label_Path.Location = new System.Drawing.Point(3, 6);
            this.ux_Label_Path.Name = "ux_Label_Path";
            this.ux_Label_Path.Size = new System.Drawing.Size(194, 24);
            this.ux_Label_Path.TabIndex = 1;
            this.ux_Label_Path.Text = "Folder Path";
            // 
            // ControlPathSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ux_tlp_Design);
            this.MinimumSize = new System.Drawing.Size(465, 36);
            this.Name = "ControlPathSelect";
            this.Size = new System.Drawing.Size(758, 36);
            this.ux_tlp_Design.ResumeLayout(false);
            this.ux_tlp_Design.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Button_ChooseFolder)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel ux_tlp_Design;
        private System.Windows.Forms.Label ux_Label_Path;
        private System.Windows.Forms.TextBox ux_Text_PathInput;
        private System.Windows.Forms.PictureBox ux_Button_ChooseFolder;
    }
}
