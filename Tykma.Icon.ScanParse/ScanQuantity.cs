﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScanQuantity.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.ScanParse
{
    /// <summary>
    /// A quantity setting command.
    /// </summary>
    public class ScanQuantity : IBaseQuantityReturn
    {
        /// <summary>
        /// The quantity.
        /// </summary>
        private int qty;

        /// <summary>
        /// Initializes a new instance of the <see cref="ScanQuantity"/> class.
        /// </summary>
        public ScanQuantity()
        {
            this.qty = 1;
        }

        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        /// <value>
        /// The quantity.
        /// </value>
        public int Quantity
        {
            get
            {
                return this.qty;
            }

            set
            {
                if (value != 0)
                {
                    this.qty = value;
                }
            }
        }
    }
}
