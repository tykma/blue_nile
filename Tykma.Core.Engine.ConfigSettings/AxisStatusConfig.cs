﻿// <copyright file="AxisStatusConfig.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.Engine.ConfigSettings
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    /// <summary>
    /// Axis status.
    /// </summary>
    public class AxisStatusConfig : IAxisStatus
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AxisStatusConfig"/> class.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public AxisStatusConfig(IList<string> config)
        {
            this.Config = config ?? new List<string>();
        }

        private IList<string> Config { get; }

        /// <summary>
        /// Determines whether specified axis is enabled in configuration file..
        /// </summary>
        /// <param name="whichAxis">Axis to check.</param>
        /// <returns>hether the axis is enabled.</returns>
        public bool IsAxisOn(int whichAxis)
        {
            if (this.Config.Where(line => line.StartsWith($"ACTIVEAXIS{whichAxis}")).Any(line => line.EndsWith("=1")))
            {
                return true;
            }

            return false;
        }

        /// <inheritdoc />
        public IList<string> SetHomeTimeOut(int whichAxis, int timeout)
        {
            var newConfig = new List<string>();

            var str = $"AXISTIMEOUT{whichAxis}=";

            foreach (var line in this.Config)
            {
                newConfig.Add(line.StartsWith(str) ? $"{str}{timeout}" : line);
            }

            return newConfig;
        }

        /// <inheritdoc />
        public int GetAxisTimeOut(int whichAxis)
        {
            var timeout = this.Config.Where(item => item.StartsWith($"AXISTIMEOUT{whichAxis}"))
                .Select(item => item.Split('=')[1])
                .FirstOrDefault(data => data != null);

            return int.TryParse(timeout, out int result) ? result : 0;
        }

        /// <inheritdoc />
        public int GetAxisHomeSpeed(int whichAxis)
        {
            foreach (var item in this.Config)
            {
                if (item.StartsWith($"AXISZEROSPD{whichAxis}"))
                {
                    var speed = item.Split('=')[1];

                    if (speed != null)
                    {
                        if (decimal.TryParse(speed, NumberStyles.Any, CultureInfo.InvariantCulture, out decimal value))
                        {
                            return (int)value;
                        }
                    }
                }
            }

            return 0;
        }

        /// <inheritdoc />
        public IList<string> SetAxisMinPosition(int whichAxis, int min)
        {
            var str = $"AXISMINPOS{whichAxis}=";

            return this.Config.Select(line => line.StartsWith(str) ? $"{str}{min}" : line).ToList();
        }

        /// <inheritdoc />
        public IList<string> SetAxisMaximumPosition(int whichAxis, int max)
        {
            var str = $"AXISMAXPOS{whichAxis}=";

            return this.Config.Select(line => line.StartsWith(str) ? $"{str}{max}" : line).ToList();
        }

        /// <inheritdoc />
        public int GetAxisMinimumPosition(int whichAxis)
        {
            var pos = this.Config.Where(item => item.StartsWith($"AXISMINPOS{whichAxis}"))
                .Select(item => item.Split('=')[1])
                .FirstOrDefault(data => data != null);

            if (decimal.TryParse(pos, NumberStyles.Any, CultureInfo.InvariantCulture, out decimal value))
            {
                return (int)value;
            }

            return 0;
        }

        /// <inheritdoc />
        public int GetAxisMaximumPosition(int whichAxis)
        {
            var pos = this.Config.Where(item => item.StartsWith($"AXISMAXPOS{whichAxis}"))
                .Select(item => item.Split('=')[1])
                .FirstOrDefault(data => data != null);

            if (decimal.TryParse(pos, NumberStyles.Any, CultureInfo.InvariantCulture, out decimal value))
            {
                return (int)value;
            }

            return 0;
        }

        /// <inheritdoc />
        public int GetPulsesPerRound(int whichAxis)
        {
            foreach (var item in this.Config)
            {
                if (item.StartsWith($"AXISSTEPPERRO{whichAxis}"))
                {
                    var val = item.Split('=')[1];

                    if (val != null)
                    {
                        if (decimal.TryParse(val, NumberStyles.Any, CultureInfo.InvariantCulture, out decimal value))
                        {
                            return (int)value;
                        }
                    }
                }
            }

            return 0;
        }

        /// <inheritdoc />
        public int GetDistancePerRound(int whichAxis)
        {
            foreach (var item in this.Config)
            {
                if (item.StartsWith($"AXISDISTPERRO{whichAxis}"))
                {
                    var val = item.Split('=')[1];

                    if (val != null)
                    {
                        if (decimal.TryParse(val, NumberStyles.Any, CultureInfo.InvariantCulture, out decimal value))
                        {
                            return (int)value;
                        }
                    }
                }
            }

            return 0;
        }

        /// <inheritdoc />
        public List<string> SetAxisHomeSpeed(int whichAxis, int speed)
        {
            var newConfig = new List<string>();

            foreach (var line in this.Config)
            {
                if (line.StartsWith($"AXISZEROSPD{whichAxis}"))
                {
                    var speedtoset = speed.ToString("E").ToLower();
                    newConfig.Add($"AXISZEROSPD{whichAxis}={speedtoset}");
                }
                else
                {
                    newConfig.Add(line);
                }
            }

            return newConfig;
        }

        /// <summary>
        /// Toggle an axis on.
        /// </summary>
        /// <param name="whichAxis">The axis to toggle.</param>
        /// <param name="enabled">Whether to enable the axis or disable it.</param>
        /// <returns>Success state.</returns>
        public List<string> ToggleAxisEnabled(int whichAxis, bool enabled)
        {
            var newConfig = new List<string>();

            foreach (var line in this.Config)
            {
                if (line.StartsWith($"ACTIVEAXIS{whichAxis}"))
                {
                    string en = enabled ? "1" : "0";
                    newConfig.Add($"ACTIVEAXIS{whichAxis}={en}");
                }
                else
                {
                    newConfig.Add(line);
                }
            }

            return newConfig;
        }
    }
}
