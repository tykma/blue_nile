//-----------------------------------------------------------------------
// <copyright file="Main.Designer.cs" company="TYKMA">
//     Copyright TYKMA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using Icon_Interface.Custom_Controls;
    using Icon_Interface.Custom_Controls.IOLeds;

    /// <summary>
    /// The main form.
    /// </summary>
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ux_StatusStrip = new System.Windows.Forms.StatusStrip();
            this.ux_Status_Events = new System.Windows.Forms.ToolStripStatusLabel();
            this.ux_Status_Empty = new System.Windows.Forms.ToolStripStatusLabel();
            this.ux_Status_Import_State = new System.Windows.Forms.ToolStripStatusLabel();
            this.ux_Status_SQL = new System.Windows.Forms.ToolStripStatusLabel();
            this.ux_Status_PLC = new System.Windows.Forms.ToolStripStatusLabel();
            this.ux_Status_Label_Shutter = new System.Windows.Forms.ToolStripStatusLabel();
            this.ux_Status_InnerOrOuterRing = new System.Windows.Forms.ToolStripStatusLabel();
            this.ux_StatusLabel_Connecting = new System.Windows.Forms.ToolStripStatusLabel();
            this.ux_Status_Time = new System.Windows.Forms.ToolStripStatusLabel();
            this.ux_ToolStrip_Activate = new System.Windows.Forms.ToolStripDropDownButton();
            this.ux_ToolStrip_Settings = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_Card = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_IO = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_Focus = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_GFXReplacer = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_GFXiStart = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_GFXiStop = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_Axis = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_HomeZ = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_HomeR = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_Globals = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_PLC = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_PLCReconnect = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_PLCDisconnect = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_PLCSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_Access = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_Operator = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_Engineer = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_Minimize = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_About = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_ToolStrip_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_Timer_ShowTime = new System.Windows.Forms.Timer(this.components);
            this.ux_Timer_CardConnection = new System.Windows.Forms.Timer(this.components);
            this.ux_Layout_MainControls = new System.Windows.Forms.TableLayoutPanel();
            this.ux_ControlButton_Start = new Icon_Interface.Custom_Controls.ControlButtonStart();
            this.ux_ControlButton_Limits = new Icon_Interface.Custom_Controls.ControlButtonLimits();
            this.ux_ControlButton_Home = new Icon_Interface.Custom_Controls.ControlButtonHome();
            this.ux_ControlButton_Edit = new Icon_Interface.Custom_Controls.ControlButtonEdit();
            this.ux_ControlButton_Clear = new Icon_Interface.Custom_Controls.ControlButtonClear();
            this.ux_Layout_Main = new System.Windows.Forms.TableLayoutPanel();
            this.ux_HMI = new Icon_Interface.Custom_Controls.CustomTabControl();
            this.ux_Tab_Main = new System.Windows.Forms.TabPage();
            this.ux_Layout_MainInner = new System.Windows.Forms.TableLayoutPanel();
            this.ux_JobList = new System.Windows.Forms.FlowLayoutPanel();
            this.ux_Layout_JobListNav = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Nav_JobList = new System.Windows.Forms.PictureBox();
            this.ux_Nav_NextJobScreen = new System.Windows.Forms.PictureBox();
            this.ux_Nav_PrevJobScreen = new System.Windows.Forms.PictureBox();
            this.ux_Layout_ScanInput = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Layout_ScannerInput = new System.Windows.Forms.TableLayoutPanel();
            this.ux_ScannerInput = new Icon_Interface.Custom_Controls.CueTextBox();
            this.ux_Control_Quantity = new Icon_Interface.Custom_Controls.ControlQuantity();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.ux_nud_fontHeight = new System.Windows.Forms.NumericUpDown();
            this.ux_Text_StartingSerial = new Icon_Interface.Custom_Controls.CueTextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.ux_nud_FontWidth = new System.Windows.Forms.NumericUpDown();
            this.ux_cb_lock = new System.Windows.Forms.CheckBox();
            this.ux_PreviewPic = new System.Windows.Forms.PictureBox();
            this.ux_Tab_Color = new System.Windows.Forms.TabPage();
            this.ux_tlp_Theme_Main = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Layout_Colors = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Set_BGRed = new System.Windows.Forms.Panel();
            this.ux_Set_BGOrange = new System.Windows.Forms.Panel();
            this.ux_Set_StatusCustom = new System.Windows.Forms.Panel();
            this.ux_Set_StatusColorLabel = new System.Windows.Forms.Label();
            this.ux_Set_StatusBlack = new System.Windows.Forms.Panel();
            this.ux_Set_FontLabel = new System.Windows.Forms.Label();
            this.ux_Set_StatusViolet = new System.Windows.Forms.Panel();
            this.ux_Set_BGColorLabel = new System.Windows.Forms.Label();
            this.ux_Set_StatusLightBlue = new System.Windows.Forms.Panel();
            this.ux_Set_StatusBlue = new System.Windows.Forms.Panel();
            this.ux_Set_StatusGreen = new System.Windows.Forms.Panel();
            this.ux_Set_StatusYellow = new System.Windows.Forms.Panel();
            this.ux_Set_StatusOrange = new System.Windows.Forms.Panel();
            this.ux_Set_StatusRed = new System.Windows.Forms.Panel();
            this.ux_Set_BGYellow = new System.Windows.Forms.Panel();
            this.ux_Set_BGViolet = new System.Windows.Forms.Panel();
            this.ux_Set_FontViolet = new System.Windows.Forms.Panel();
            this.ux_Set_BGGreen = new System.Windows.Forms.Panel();
            this.ux_Set_FontBlack = new System.Windows.Forms.Panel();
            this.ux_Set_BGBlue = new System.Windows.Forms.Panel();
            this.ux_Set_FontCustom = new System.Windows.Forms.Panel();
            this.ux_Set_BGWhite = new System.Windows.Forms.Panel();
            this.ux_Set_BGLightBlue = new System.Windows.Forms.Panel();
            this.ux_Set_BGCustom = new System.Windows.Forms.Panel();
            this.ux_Set_FontRed = new System.Windows.Forms.Panel();
            this.ux_Set_FontOrange = new System.Windows.Forms.Panel();
            this.ux_Set_FontYellow = new System.Windows.Forms.Panel();
            this.ux_Set_FontGreen = new System.Windows.Forms.Panel();
            this.ux_Set_FontBlue = new System.Windows.Forms.Panel();
            this.ux_Set_FontLightBlue = new System.Windows.Forms.Panel();
            this.ux_Layout_FullScreenMode = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Theme_SpellCheck = new System.Windows.Forms.TableLayoutPanel();
            this.ux_CheckBox_HideJobList = new System.Windows.Forms.CheckBox();
            this.ux_Label_Language = new System.Windows.Forms.Label();
            this.ux_cbs_LanguageSelect = new System.Windows.Forms.ComboBox();
            this.ux_Set_ToggleFullScreen = new System.Windows.Forms.CheckBox();
            this.ux_tlp_BGImage = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Set_BackgroundImage = new System.Windows.Forms.PictureBox();
            this.ux_Set_BGPicLabel = new System.Windows.Forms.Label();
            this.ux_Button_BackColors = new Icon_Interface.Custom_Controls.ControlButtonBack();
            this.ux_ControlButtonSave_Theme = new Icon_Interface.Custom_Controls.ControlButtonSave();
            this.ux_Tab_EventLog = new System.Windows.Forms.TabPage();
            this.ux_EventLog = new System.Windows.Forms.ListView();
            this.ux_Context_EventLog = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ux_Context_EventLogCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_Context_EventClear = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_Tab_IO = new System.Windows.Forms.TabPage();
            this.ux_Layout_IO = new System.Windows.Forms.TableLayoutPanel();
            this.ux_LED_Output16 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Input16 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Output15 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Input15 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Output14 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_IO_OutputsLabel = new System.Windows.Forms.Label();
            this.ux_LED_Output13 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Input14 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Output12 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Input13 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Output11 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Input12 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Output10 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Input11 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Output9 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Input10 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Output8 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Input9 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Output7 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Input8 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Output6 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Input7 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Output5 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Input6 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Output4 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Input5 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Output3 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Input4 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Output2 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Input3 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Output1 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Input2 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_LED_Input1 = new Icon_Interface.Custom_Controls.IOLeds.ControlIOLEDBase();
            this.ux_IO_InputsLabel = new System.Windows.Forms.Label();
            this.ux_Tab_General = new System.Windows.Forms.TabPage();
            this.ux_Layout_GeneralMain = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Control_InstallFolder = new Icon_Interface.Custom_Controls.ControlPathSelect();
            this.ux_Set_NewPassLabel = new System.Windows.Forms.Label();
            this.ux_Set_NewPassword = new System.Windows.Forms.TextBox();
            this.ux_ControlButtonSave_General = new Icon_Interface.Custom_Controls.ControlButtonSave();
            this.ux_Check_PervasiveLimitsEnabled = new Icon_Interface.Custom_Controls.ControlVerboseCheckBox();
            this.ux_Check_InstructionsEnabled = new Icon_Interface.Custom_Controls.ControlVerboseCheckBox();
            this.ux_Check_LoadLastJobEnabled = new Icon_Interface.Custom_Controls.ControlVerboseCheckBox();
            this.ux_Check_AccessControlEnabled = new Icon_Interface.Custom_Controls.ControlVerboseCheckBox();
            this.ux_Control_ProjectsPath = new Icon_Interface.Custom_Controls.ControlPathSelect();
            this.ux_Control_ImagesPath = new Icon_Interface.Custom_Controls.ControlPathSelect();
            this.ux_Control_RTFPath = new Icon_Interface.Custom_Controls.ControlPathSelect();
            this.ux_Button_Back_Generals = new Icon_Interface.Custom_Controls.ControlButtonBack();
            this.ux_Tab_Settings = new System.Windows.Forms.TabPage();
            this.ux_flp_Settings = new System.Windows.Forms.FlowLayoutPanel();
            this.ux_Control_ButtonGeneral = new Icon_Interface.Custom_Controls.ControlButtonGeneral();
            this.Control_Button_Theme = new Icon_Interface.Custom_Controls.ControlButtonTheme();
            this.ux_Control_SettingImport = new Icon_Interface.Custom_Controls.ControlButtonImport();
            this.ux_Control_ButtonAxisSetting = new Icon_Interface.Custom_Controls.ControlButtonAxis();
            this.ux_Control_PLCSetting = new Icon_Interface.Custom_Controls.ControlButtonPLC();
            this.controlButtonAllSettings1 = new Icon_Interface.Custom_Controls.ControlButtonAllSettings();
            this.ux_Nav_Stop = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Nav_StopLabel = new System.Windows.Forms.Label();
            this.ux_Nav_StopPic = new System.Windows.Forms.PictureBox();
            this.ux_Tab_MarkInProgress = new System.Windows.Forms.TabPage();
            this.ux_Layout_MarkInProgressMain = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Label_ElapsedTime = new System.Windows.Forms.Label();
            this.ux_MarkInProgressLabel = new System.Windows.Forms.Label();
            this.ux_Label_MarkingObject = new System.Windows.Forms.Label();
            this.ux_Tab_Focus = new System.Windows.Forms.TabPage();
            this.ux_Layout_FocusMain = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Nav_Focus = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Nav_FocusPic = new System.Windows.Forms.PictureBox();
            this.ux_Nav_FocusLabel = new System.Windows.Forms.Label();
            this.ux_Layout_FocusSize = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Focus_SizeLabel = new System.Windows.Forms.Label();
            this.ux_FocusSize = new System.Windows.Forms.HScrollBar();
            this.ux_Tab_DataEdit = new System.Windows.Forms.TabPage();
            this.ux_DataGridView = new System.Windows.Forms.DataGridView();
            this.ux_Tab_PLC = new System.Windows.Forms.TabPage();
            this.ux_PLC_Layout_TagAdd = new System.Windows.Forms.GroupBox();
            this.ux_cb_plc_tag_add_type = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ux_PLC_AddTag = new System.Windows.Forms.Button();
            this.ux_PLC_AddTagName = new System.Windows.Forms.TextBox();
            this.ux_PLC_AddTagNameLabel = new System.Windows.Forms.Label();
            this.ux_PLC_Layout_TagWrite = new System.Windows.Forms.GroupBox();
            this.ux_cb_Plc_Tag_Type = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ux_PLC_WriteTag = new System.Windows.Forms.Button();
            this.ux_PLC_WriteTagValue = new System.Windows.Forms.TextBox();
            this.ux_PLC_WriteTagName = new System.Windows.Forms.TextBox();
            this.ux_PLC_TagWriteValueLabel = new System.Windows.Forms.Label();
            this.ux_PLC_TagWriteNameLabel = new System.Windows.Forms.Label();
            this.ux_PLC_Layout_TagView = new System.Windows.Forms.GroupBox();
            this.ux_PLC_TagView = new Icon_Interface.Custom_Controls.MyListView();
            this.ux_PLC_Layout_Config = new System.Windows.Forms.GroupBox();
            this.ux_PLC_ChangeIP = new System.Windows.Forms.Button();
            this.ux_PLC_ScanTimeSecondaryMSLabel = new System.Windows.Forms.Label();
            this.ux_PLC_ScanTimeMSLabel = new System.Windows.Forms.Label();
            this.ux_PLC_ScanTimeSecondary = new System.Windows.Forms.NumericUpDown();
            this.ux_PLC_ScanTime = new System.Windows.Forms.NumericUpDown();
            this.ux_PLC_ScanSecondaryLabel = new System.Windows.Forms.Label();
            this.ux_PLC_MainScanLabel = new System.Windows.Forms.Label();
            this.ux_PLC_IP = new System.Windows.Forms.TextBox();
            this.ux_PLC_IPLabel = new System.Windows.Forms.Label();
            this.ux_Tab_FileImport = new System.Windows.Forms.TabPage();
            this.ux_Import_Layout_Main = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Import_PathSettingsLabel = new System.Windows.Forms.Label();
            this.ux_Set_ImportConfigLabel = new System.Windows.Forms.Label();
            this.ux_Layout_ImportConfig = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Import_ReadWait = new System.Windows.Forms.NumericUpDown();
            this.ux_Import_ReadWaitLabel = new System.Windows.Forms.Label();
            this.ux_Import_FilePollRateLabel = new System.Windows.Forms.Label();
            this.ux_Import_PollRate = new System.Windows.Forms.NumericUpDown();
            this.ux_Import_ToggleJobLoadMode = new System.Windows.Forms.CheckBox();
            this.ux_Import_JobLoadDesc = new System.Windows.Forms.Label();
            this.ux_Import_ToggleFileImport = new System.Windows.Forms.CheckBox();
            this.ux_Import_ImportDesc = new System.Windows.Forms.Label();
            this.ux_flp_ImportSaveControls = new System.Windows.Forms.FlowLayoutPanel();
            this.ux_ControlButtonSave_Paths = new Icon_Interface.Custom_Controls.ControlButtonSave();
            this.ux_Button_BackFromImportPaths = new Icon_Interface.Custom_Controls.ControlButtonBack();
            this.ux_flp_ImportPaths = new System.Windows.Forms.FlowLayoutPanel();
            this.ux_Control_ImportPath = new Icon_Interface.Custom_Controls.ControlPathSelect();
            this.ux_Control_CompletePath = new Icon_Interface.Custom_Controls.ControlPathSelect();
            this.ux_Control_ErrorPath = new Icon_Interface.Custom_Controls.ControlPathSelect();
            this.ux_Tab_Auth = new System.Windows.Forms.TabPage();
            this.ux_Layout_AuthMain = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Layout_KeyPadKeys = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Auth_Key6 = new System.Windows.Forms.PictureBox();
            this.ux_Auth_Key5 = new System.Windows.Forms.PictureBox();
            this.ux_Auth_Key4 = new System.Windows.Forms.PictureBox();
            this.ux_Auth_Key0 = new System.Windows.Forms.PictureBox();
            this.ux_Auth_KeyClear = new System.Windows.Forms.PictureBox();
            this.ux_Auth_KeyOK = new System.Windows.Forms.PictureBox();
            this.ux_Auth_Key7 = new System.Windows.Forms.PictureBox();
            this.ux_Auth_Key1 = new System.Windows.Forms.PictureBox();
            this.ux_Auth_Key8 = new System.Windows.Forms.PictureBox();
            this.ux_Auth_Key2 = new System.Windows.Forms.PictureBox();
            this.ux_Auth_Key9 = new System.Windows.Forms.PictureBox();
            this.ux_Auth_Key3 = new System.Windows.Forms.PictureBox();
            this.ux_Auth_MaskedKey = new System.Windows.Forms.Label();
            this.ux_Auth_Level = new System.Windows.Forms.Label();
            this.ux_Tab_About = new System.Windows.Forms.TabPage();
            this.ux_TextBox_About = new System.Windows.Forms.TextBox();
            this.ux_Tab_JobSelect = new System.Windows.Forms.TabPage();
            this.ux_Job_Tree = new System.Windows.Forms.TreeView();
            this.ux_Tab_Axis = new System.Windows.Forms.TabPage();
            this.ux_Layout_AxisMain = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Layout_RAxis = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Label_RHomeAxisEveryN = new System.Windows.Forms.Label();
            this.ux_CheckBox_AxisREnabled = new System.Windows.Forms.CheckBox();
            this.ux_Numeric_RHomeFrequency = new System.Windows.Forms.NumericUpDown();
            this.ux_Label_AxisR = new System.Windows.Forms.Label();
            this.ux_Label_AxisX = new System.Windows.Forms.Label();
            this.ux_Label_AxisZ = new System.Windows.Forms.Label();
            this.ux_Layout_ZAxis = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Label_ZHomeAxisEveryN = new System.Windows.Forms.Label();
            this.ux_CheckBox_AxisZEnabled = new System.Windows.Forms.CheckBox();
            this.ux_Numeric_ZHomeFrequency = new System.Windows.Forms.NumericUpDown();
            this.ux_tlp_LowerAxisSave = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Check_HomeAxisOnStartup = new System.Windows.Forms.CheckBox();
            this.ux_ControlButtonSave_Axis = new Icon_Interface.Custom_Controls.ControlButtonSave();
            this.ux_Tab_Global = new System.Windows.Forms.TabPage();
            this.ux_Layout_GVMain = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Label_HeaderGlobalVariables = new System.Windows.Forms.Label();
            this.ux_Layout_GlobalVariables = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Layout_GVCounters = new System.Windows.Forms.TableLayoutPanel();
            this.ux_dgv_GVCounters = new System.Windows.Forms.DataGridView();
            this.ux_Layout_GVCounterControls = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Button_GVCounterAdd = new System.Windows.Forms.Button();
            this.ux_Button_GVCounterDel = new System.Windows.Forms.Button();
            this.ux_Button_GVCounterReset = new System.Windows.Forms.Button();
            this.ux_label_GVCounters = new System.Windows.Forms.Label();
            this.ux_Layout_GVStrings = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Layout_GVCounterStrings = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Button_GVStringAdd = new System.Windows.Forms.Button();
            this.ux_Button_GVStringDel = new System.Windows.Forms.Button();
            this.ux_Button_GVStringModify = new System.Windows.Forms.Button();
            this.ux_dgv_Strings = new System.Windows.Forms.DataGridView();
            this.ux_label_GVStrings = new System.Windows.Forms.Label();
            this.ux_Tab_Moves = new System.Windows.Forms.TabPage();
            this.ux_Table_Axes = new System.Windows.Forms.TableLayoutPanel();
            this.ux_ProgressBarR = new System.Windows.Forms.ProgressBar();
            this.ux_Label_MoveR = new System.Windows.Forms.Label();
            this.ux_Label_MoveZ = new System.Windows.Forms.Label();
            this.ux_Label_MoveX = new System.Windows.Forms.Label();
            this.ux_Label_MoveY = new System.Windows.Forms.Label();
            this.ux_ProgressBarZ = new System.Windows.Forms.ProgressBar();
            this.ux_tab_pg = new System.Windows.Forms.TabPage();
            this.ux_pg_inisettings = new System.Windows.Forms.PropertyGrid();
            this.ux_Timer_IO = new System.Windows.Forms.Timer(this.components);
            this.ux_Timer_AnimateFileSearch = new System.Windows.Forms.Timer(this.components);
            this.ux_Timer_ReconnectToPLC = new System.Windows.Forms.Timer(this.components);
            this.ux_fsw_JobList = new System.IO.FileSystemWatcher();
            this.ux_Timer_ReconnectToEngine = new System.Windows.Forms.Timer(this.components);
            this.ux_StatusStrip.SuspendLayout();
            this.ux_Layout_MainControls.SuspendLayout();
            this.ux_Layout_Main.SuspendLayout();
            this.ux_HMI.SuspendLayout();
            this.ux_Tab_Main.SuspendLayout();
            this.ux_Layout_MainInner.SuspendLayout();
            this.ux_Layout_JobListNav.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Nav_JobList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Nav_NextJobScreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Nav_PrevJobScreen)).BeginInit();
            this.ux_Layout_ScanInput.SuspendLayout();
            this.ux_Layout_ScannerInput.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_nud_fontHeight)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_nud_FontWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_PreviewPic)).BeginInit();
            this.ux_Tab_Color.SuspendLayout();
            this.ux_tlp_Theme_Main.SuspendLayout();
            this.ux_Layout_Colors.SuspendLayout();
            this.ux_Layout_FullScreenMode.SuspendLayout();
            this.ux_Theme_SpellCheck.SuspendLayout();
            this.ux_tlp_BGImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Set_BackgroundImage)).BeginInit();
            this.ux_Tab_EventLog.SuspendLayout();
            this.ux_Context_EventLog.SuspendLayout();
            this.ux_Tab_IO.SuspendLayout();
            this.ux_Layout_IO.SuspendLayout();
            this.ux_Tab_General.SuspendLayout();
            this.ux_Layout_GeneralMain.SuspendLayout();
            this.ux_Tab_Settings.SuspendLayout();
            this.ux_flp_Settings.SuspendLayout();
            this.ux_Nav_Stop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Nav_StopPic)).BeginInit();
            this.ux_Tab_MarkInProgress.SuspendLayout();
            this.ux_Layout_MarkInProgressMain.SuspendLayout();
            this.ux_Tab_Focus.SuspendLayout();
            this.ux_Layout_FocusMain.SuspendLayout();
            this.ux_Nav_Focus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Nav_FocusPic)).BeginInit();
            this.ux_Layout_FocusSize.SuspendLayout();
            this.ux_Tab_DataEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_DataGridView)).BeginInit();
            this.ux_Tab_PLC.SuspendLayout();
            this.ux_PLC_Layout_TagAdd.SuspendLayout();
            this.ux_PLC_Layout_TagWrite.SuspendLayout();
            this.ux_PLC_Layout_TagView.SuspendLayout();
            this.ux_PLC_Layout_Config.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_PLC_ScanTimeSecondary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_PLC_ScanTime)).BeginInit();
            this.ux_Tab_FileImport.SuspendLayout();
            this.ux_Import_Layout_Main.SuspendLayout();
            this.ux_Layout_ImportConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Import_ReadWait)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Import_PollRate)).BeginInit();
            this.ux_flp_ImportSaveControls.SuspendLayout();
            this.ux_flp_ImportPaths.SuspendLayout();
            this.ux_Tab_Auth.SuspendLayout();
            this.ux_Layout_AuthMain.SuspendLayout();
            this.ux_Layout_KeyPadKeys.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_KeyClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_KeyOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key3)).BeginInit();
            this.ux_Tab_About.SuspendLayout();
            this.ux_Tab_JobSelect.SuspendLayout();
            this.ux_Tab_Axis.SuspendLayout();
            this.ux_Layout_AxisMain.SuspendLayout();
            this.ux_Layout_RAxis.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Numeric_RHomeFrequency)).BeginInit();
            this.ux_Layout_ZAxis.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Numeric_ZHomeFrequency)).BeginInit();
            this.ux_tlp_LowerAxisSave.SuspendLayout();
            this.ux_Tab_Global.SuspendLayout();
            this.ux_Layout_GVMain.SuspendLayout();
            this.ux_Layout_GlobalVariables.SuspendLayout();
            this.ux_Layout_GVCounters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_dgv_GVCounters)).BeginInit();
            this.ux_Layout_GVCounterControls.SuspendLayout();
            this.ux_Layout_GVStrings.SuspendLayout();
            this.ux_Layout_GVCounterStrings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_dgv_Strings)).BeginInit();
            this.ux_Tab_Moves.SuspendLayout();
            this.ux_Table_Axes.SuspendLayout();
            this.ux_tab_pg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_fsw_JobList)).BeginInit();
            this.SuspendLayout();
            // 
            // ux_StatusStrip
            // 
            this.ux_StatusStrip.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ux_Status_Events,
            this.ux_Status_Empty,
            this.ux_Status_Import_State,
            this.ux_Status_SQL,
            this.ux_Status_PLC,
            this.ux_Status_Label_Shutter,
            this.ux_Status_InnerOrOuterRing,
            this.ux_StatusLabel_Connecting,
            this.ux_Status_Time,
            this.ux_ToolStrip_Activate});
            this.ux_StatusStrip.Location = new System.Drawing.Point(0, 541);
            this.ux_StatusStrip.Name = "ux_StatusStrip";
            this.ux_StatusStrip.Size = new System.Drawing.Size(784, 22);
            this.ux_StatusStrip.SizingGrip = false;
            this.ux_StatusStrip.TabIndex = 5;
            this.ux_StatusStrip.Text = " ";
            this.ux_StatusStrip.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Ux_StatusStrip_MouseDoubleClick);
            // 
            // ux_Status_Events
            // 
            this.ux_Status_Events.Name = "ux_Status_Events";
            this.ux_Status_Events.Size = new System.Drawing.Size(60, 17);
            this.ux_Status_Events.Text = "Welcome!";
            this.ux_Status_Events.Click += new System.EventHandler(this.ToolStripLaserStatusClick);
            // 
            // ux_Status_Empty
            // 
            this.ux_Status_Empty.Name = "ux_Status_Empty";
            this.ux_Status_Empty.Size = new System.Drawing.Size(640, 17);
            this.ux_Status_Empty.Spring = true;
            // 
            // ux_Status_Import_State
            // 
            this.ux_Status_Import_State.Image = ((System.Drawing.Image)(resources.GetObject("ux_Status_Import_State.Image")));
            this.ux_Status_Import_State.ImageTransparentColor = System.Drawing.Color.White;
            this.ux_Status_Import_State.Name = "ux_Status_Import_State";
            this.ux_Status_Import_State.Size = new System.Drawing.Size(93, 17);
            this.ux_Status_Import_State.Text = "Search Status";
            this.ux_Status_Import_State.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.ux_Status_Import_State.Visible = false;
            this.ux_Status_Import_State.Click += new System.EventHandler(this.ToolStripFileSearchClick);
            // 
            // ux_Status_SQL
            // 
            this.ux_Status_SQL.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Status_SQL.Name = "ux_Status_SQL";
            this.ux_Status_SQL.Size = new System.Drawing.Size(29, 17);
            this.ux_Status_SQL.Text = "SQL";
            this.ux_Status_SQL.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.ux_Status_SQL.Visible = false;
            // 
            // ux_Status_PLC
            // 
            this.ux_Status_PLC.Name = "ux_Status_PLC";
            this.ux_Status_PLC.Size = new System.Drawing.Size(28, 17);
            this.ux_Status_PLC.Text = "PLC";
            this.ux_Status_PLC.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.ux_Status_PLC.Visible = false;
            this.ux_Status_PLC.Click += new System.EventHandler(this.ToolStripLabelPlcStatusClick);
            // 
            // ux_Status_Label_Shutter
            // 
            this.ux_Status_Label_Shutter.BackColor = System.Drawing.Color.Red;
            this.ux_Status_Label_Shutter.BorderStyle = System.Windows.Forms.Border3DStyle.Bump;
            this.ux_Status_Label_Shutter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ux_Status_Label_Shutter.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Status_Label_Shutter.ForeColor = System.Drawing.Color.White;
            this.ux_Status_Label_Shutter.Name = "ux_Status_Label_Shutter";
            this.ux_Status_Label_Shutter.Size = new System.Drawing.Size(40, 17);
            this.ux_Status_Label_Shutter.Text = " OPEN";
            this.ux_Status_Label_Shutter.Visible = false;
            // 
            // ux_Status_InnerOrOuterRing
            // 
            this.ux_Status_InnerOrOuterRing.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedInner;
            this.ux_Status_InnerOrOuterRing.Name = "ux_Status_InnerOrOuterRing";
            this.ux_Status_InnerOrOuterRing.Size = new System.Drawing.Size(31, 17);
            this.ux_Status_InnerOrOuterRing.Text = "Ring";
            this.ux_Status_InnerOrOuterRing.Visible = false;
            // 
            // ux_StatusLabel_Connecting
            // 
            this.ux_StatusLabel_Connecting.Name = "ux_StatusLabel_Connecting";
            this.ux_StatusLabel_Connecting.Size = new System.Drawing.Size(69, 17);
            this.ux_StatusLabel_Connecting.Text = "Connecting";
            this.ux_StatusLabel_Connecting.Visible = false;
            this.ux_StatusLabel_Connecting.Click += new System.EventHandler(this.StatusLabelConnectingClick);
            // 
            // ux_Status_Time
            // 
            this.ux_Status_Time.Name = "ux_Status_Time";
            this.ux_Status_Time.Size = new System.Drawing.Size(49, 17);
            this.ux_Status_Time.Text = "12:00:00";
            // 
            // ux_ToolStrip_Activate
            // 
            this.ux_ToolStrip_Activate.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ux_ToolStrip_Activate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ux_ToolStrip_Activate.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ux_ToolStrip_Settings,
            this.ux_ToolStrip_Card,
            this.ux_ToolStrip_GFXReplacer,
            this.ux_ToolStrip_Axis,
            this.ux_ToolStrip_Globals,
            this.ux_ToolStrip_PLC,
            this.ux_ToolStrip_Access,
            this.ux_ToolStrip_Minimize,
            this.ux_ToolStrip_About,
            this.ux_ToolStrip_Exit});
            this.ux_ToolStrip_Activate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_ToolStrip_Activate.Image = ((System.Drawing.Image)(resources.GetObject("ux_ToolStrip_Activate.Image")));
            this.ux_ToolStrip_Activate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ux_ToolStrip_Activate.Name = "ux_ToolStrip_Activate";
            this.ux_ToolStrip_Activate.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.ux_ToolStrip_Activate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ux_ToolStrip_Activate.ShowDropDownArrow = false;
            this.ux_ToolStrip_Activate.Size = new System.Drawing.Size(20, 20);
            this.ux_ToolStrip_Activate.Text = "toolStripDropDownButton1";
            this.ux_ToolStrip_Activate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ux_ToolStrip_Activate.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical270;
            this.ux_ToolStrip_Activate.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            // 
            // ux_ToolStrip_Settings
            // 
            this.ux_ToolStrip_Settings.Image = ((System.Drawing.Image)(resources.GetObject("ux_ToolStrip_Settings.Image")));
            this.ux_ToolStrip_Settings.Name = "ux_ToolStrip_Settings";
            this.ux_ToolStrip_Settings.Size = new System.Drawing.Size(172, 26);
            this.ux_ToolStrip_Settings.Text = "Settings";
            this.ux_ToolStrip_Settings.Click += new System.EventHandler(this.SettingsToolStripMenuItemClick);
            // 
            // ux_ToolStrip_Card
            // 
            this.ux_ToolStrip_Card.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ux_ToolStrip_IO,
            this.ux_ToolStrip_Focus});
            this.ux_ToolStrip_Card.Image = ((System.Drawing.Image)(resources.GetObject("ux_ToolStrip_Card.Image")));
            this.ux_ToolStrip_Card.Name = "ux_ToolStrip_Card";
            this.ux_ToolStrip_Card.Size = new System.Drawing.Size(172, 26);
            this.ux_ToolStrip_Card.Text = "Card";
            // 
            // ux_ToolStrip_IO
            // 
            this.ux_ToolStrip_IO.Name = "ux_ToolStrip_IO";
            this.ux_ToolStrip_IO.Size = new System.Drawing.Size(120, 26);
            this.ux_ToolStrip_IO.Text = "I/O";
            this.ux_ToolStrip_IO.Click += new System.EventHandler(this.IOToolStripMenuItemClick);
            // 
            // ux_ToolStrip_Focus
            // 
            this.ux_ToolStrip_Focus.Name = "ux_ToolStrip_Focus";
            this.ux_ToolStrip_Focus.Size = new System.Drawing.Size(120, 26);
            this.ux_ToolStrip_Focus.Text = "Focus";
            this.ux_ToolStrip_Focus.Visible = false;
            this.ux_ToolStrip_Focus.Click += new System.EventHandler(this.FocusToolStripMenuItemClick);
            // 
            // ux_ToolStrip_GFXReplacer
            // 
            this.ux_ToolStrip_GFXReplacer.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ux_ToolStrip_GFXiStart,
            this.ux_ToolStrip_GFXiStop});
            this.ux_ToolStrip_GFXReplacer.Image = ((System.Drawing.Image)(resources.GetObject("ux_ToolStrip_GFXReplacer.Image")));
            this.ux_ToolStrip_GFXReplacer.Name = "ux_ToolStrip_GFXReplacer";
            this.ux_ToolStrip_GFXReplacer.Size = new System.Drawing.Size(172, 26);
            this.ux_ToolStrip_GFXReplacer.Text = "GFX Replacer";
            // 
            // ux_ToolStrip_GFXiStart
            // 
            this.ux_ToolStrip_GFXiStart.Name = "ux_ToolStrip_GFXiStart";
            this.ux_ToolStrip_GFXiStart.Size = new System.Drawing.Size(112, 26);
            this.ux_ToolStrip_GFXiStart.Text = "Start";
            this.ux_ToolStrip_GFXiStart.Click += new System.EventHandler(this.ToolStripGfXiStartClick);
            // 
            // ux_ToolStrip_GFXiStop
            // 
            this.ux_ToolStrip_GFXiStop.Name = "ux_ToolStrip_GFXiStop";
            this.ux_ToolStrip_GFXiStop.Size = new System.Drawing.Size(112, 26);
            this.ux_ToolStrip_GFXiStop.Text = "Stop";
            this.ux_ToolStrip_GFXiStop.Click += new System.EventHandler(this.ToolStripGfXiStopClick);
            // 
            // ux_ToolStrip_Axis
            // 
            this.ux_ToolStrip_Axis.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ux_ToolStrip_HomeZ,
            this.ux_ToolStrip_HomeR});
            this.ux_ToolStrip_Axis.Name = "ux_ToolStrip_Axis";
            this.ux_ToolStrip_Axis.Size = new System.Drawing.Size(172, 26);
            this.ux_ToolStrip_Axis.Text = "Axis";
            // 
            // ux_ToolStrip_HomeZ
            // 
            this.ux_ToolStrip_HomeZ.Name = "ux_ToolStrip_HomeZ";
            this.ux_ToolStrip_HomeZ.Size = new System.Drawing.Size(136, 26);
            this.ux_ToolStrip_HomeZ.Text = "Home Z";
            this.ux_ToolStrip_HomeZ.Click += new System.EventHandler(this.ToolStripHomeZ);
            // 
            // ux_ToolStrip_HomeR
            // 
            this.ux_ToolStrip_HomeR.Name = "ux_ToolStrip_HomeR";
            this.ux_ToolStrip_HomeR.Size = new System.Drawing.Size(136, 26);
            this.ux_ToolStrip_HomeR.Text = "Home R";
            this.ux_ToolStrip_HomeR.Click += new System.EventHandler(this.ToolStripHomeR);
            // 
            // ux_ToolStrip_Globals
            // 
            this.ux_ToolStrip_Globals.Image = ((System.Drawing.Image)(resources.GetObject("ux_ToolStrip_Globals.Image")));
            this.ux_ToolStrip_Globals.Name = "ux_ToolStrip_Globals";
            this.ux_ToolStrip_Globals.Size = new System.Drawing.Size(172, 26);
            this.ux_ToolStrip_Globals.Text = "Global IDs";
            this.ux_ToolStrip_Globals.Click += new System.EventHandler(this.ToolStripGlobalsClick);
            // 
            // ux_ToolStrip_PLC
            // 
            this.ux_ToolStrip_PLC.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ux_ToolStrip_PLCReconnect,
            this.ux_ToolStrip_PLCDisconnect,
            this.ux_ToolStrip_PLCSettings});
            this.ux_ToolStrip_PLC.Name = "ux_ToolStrip_PLC";
            this.ux_ToolStrip_PLC.Size = new System.Drawing.Size(172, 26);
            this.ux_ToolStrip_PLC.Text = "PLC";
            this.ux_ToolStrip_PLC.Visible = false;
            // 
            // ux_ToolStrip_PLCReconnect
            // 
            this.ux_ToolStrip_PLCReconnect.Name = "ux_ToolStrip_PLCReconnect";
            this.ux_ToolStrip_PLCReconnect.Size = new System.Drawing.Size(156, 26);
            this.ux_ToolStrip_PLCReconnect.Text = "Reconnect";
            this.ux_ToolStrip_PLCReconnect.Click += new System.EventHandler(this.ReconnectToolStripMenuItemClick);
            // 
            // ux_ToolStrip_PLCDisconnect
            // 
            this.ux_ToolStrip_PLCDisconnect.Name = "ux_ToolStrip_PLCDisconnect";
            this.ux_ToolStrip_PLCDisconnect.Size = new System.Drawing.Size(156, 26);
            this.ux_ToolStrip_PLCDisconnect.Text = "Disconnect";
            this.ux_ToolStrip_PLCDisconnect.Click += new System.EventHandler(this.DisconnectToolStripMenuItemClick);
            // 
            // ux_ToolStrip_PLCSettings
            // 
            this.ux_ToolStrip_PLCSettings.Name = "ux_ToolStrip_PLCSettings";
            this.ux_ToolStrip_PLCSettings.Size = new System.Drawing.Size(156, 26);
            this.ux_ToolStrip_PLCSettings.Text = "Settings";
            this.ux_ToolStrip_PLCSettings.Click += new System.EventHandler(this.SettingsToolStripMenuItem1Click);
            // 
            // ux_ToolStrip_Access
            // 
            this.ux_ToolStrip_Access.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ux_ToolStrip_Operator,
            this.ux_ToolStrip_Engineer});
            this.ux_ToolStrip_Access.Image = ((System.Drawing.Image)(resources.GetObject("ux_ToolStrip_Access.Image")));
            this.ux_ToolStrip_Access.Name = "ux_ToolStrip_Access";
            this.ux_ToolStrip_Access.Size = new System.Drawing.Size(172, 26);
            this.ux_ToolStrip_Access.Text = "Access";
            // 
            // ux_ToolStrip_Operator
            // 
            this.ux_ToolStrip_Operator.Name = "ux_ToolStrip_Operator";
            this.ux_ToolStrip_Operator.Size = new System.Drawing.Size(143, 26);
            this.ux_ToolStrip_Operator.Text = "Operator";
            this.ux_ToolStrip_Operator.Click += new System.EventHandler(this.OperatorToolStripMenuItemClick);
            // 
            // ux_ToolStrip_Engineer
            // 
            this.ux_ToolStrip_Engineer.Name = "ux_ToolStrip_Engineer";
            this.ux_ToolStrip_Engineer.Size = new System.Drawing.Size(143, 26);
            this.ux_ToolStrip_Engineer.Text = "Engineer";
            this.ux_ToolStrip_Engineer.Click += new System.EventHandler(this.EngineerToolStripMenuItemClick);
            // 
            // ux_ToolStrip_Minimize
            // 
            this.ux_ToolStrip_Minimize.Image = ((System.Drawing.Image)(resources.GetObject("ux_ToolStrip_Minimize.Image")));
            this.ux_ToolStrip_Minimize.Name = "ux_ToolStrip_Minimize";
            this.ux_ToolStrip_Minimize.Size = new System.Drawing.Size(172, 26);
            this.ux_ToolStrip_Minimize.Text = "Minimize";
            this.ux_ToolStrip_Minimize.Click += new System.EventHandler(this.MinimizeToolStripMenuItemClick);
            // 
            // ux_ToolStrip_About
            // 
            this.ux_ToolStrip_About.Image = ((System.Drawing.Image)(resources.GetObject("ux_ToolStrip_About.Image")));
            this.ux_ToolStrip_About.Name = "ux_ToolStrip_About";
            this.ux_ToolStrip_About.Size = new System.Drawing.Size(172, 26);
            this.ux_ToolStrip_About.Text = "About";
            this.ux_ToolStrip_About.Click += new System.EventHandler(this.ToolStripAboutClick);
            // 
            // ux_ToolStrip_Exit
            // 
            this.ux_ToolStrip_Exit.Image = ((System.Drawing.Image)(resources.GetObject("ux_ToolStrip_Exit.Image")));
            this.ux_ToolStrip_Exit.Name = "ux_ToolStrip_Exit";
            this.ux_ToolStrip_Exit.Size = new System.Drawing.Size(172, 26);
            this.ux_ToolStrip_Exit.Text = "Exit";
            this.ux_ToolStrip_Exit.Click += new System.EventHandler(this.ExitToolStripMenuItem1Click);
            // 
            // ux_Timer_ShowTime
            // 
            this.ux_Timer_ShowTime.Enabled = true;
            this.ux_Timer_ShowTime.Interval = 1000;
            this.ux_Timer_ShowTime.Tick += new System.EventHandler(this.TimerShowTimeTick);
            // 
            // ux_Timer_CardConnection
            // 
            this.ux_Timer_CardConnection.Interval = 2500;
            this.ux_Timer_CardConnection.Tick += new System.EventHandler(this.TimerConnectionToCardTick);
            // 
            // ux_Layout_MainControls
            // 
            this.ux_Layout_MainControls.ColumnCount = 1;
            this.ux_Layout_MainControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_Layout_MainControls.Controls.Add(this.ux_ControlButton_Start, 0, 0);
            this.ux_Layout_MainControls.Controls.Add(this.ux_ControlButton_Limits, 0, 1);
            this.ux_Layout_MainControls.Controls.Add(this.ux_ControlButton_Home, 0, 2);
            this.ux_Layout_MainControls.Controls.Add(this.ux_ControlButton_Edit, 0, 3);
            this.ux_Layout_MainControls.Controls.Add(this.ux_ControlButton_Clear, 0, 4);
            this.ux_Layout_MainControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_MainControls.Location = new System.Drawing.Point(702, 3);
            this.ux_Layout_MainControls.Name = "ux_Layout_MainControls";
            this.ux_Layout_MainControls.RowCount = 5;
            this.ux_Layout_MainControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.ux_Layout_MainControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.ux_Layout_MainControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.ux_Layout_MainControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.ux_Layout_MainControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.ux_Layout_MainControls.Size = new System.Drawing.Size(79, 535);
            this.ux_Layout_MainControls.TabIndex = 66;
            // 
            // ux_ControlButton_Start
            // 
            this.ux_ControlButton_Start.Location = new System.Drawing.Point(3, 3);
            this.ux_ControlButton_Start.MinimumSize = new System.Drawing.Size(75, 100);
            this.ux_ControlButton_Start.Name = "ux_ControlButton_Start";
            this.ux_ControlButton_Start.Size = new System.Drawing.Size(75, 100);
            this.ux_ControlButton_Start.TabIndex = 17;
            this.ux_ControlButton_Start.Click += new System.EventHandler(this.IconStartCycleClick);
            // 
            // ux_ControlButton_Limits
            // 
            this.ux_ControlButton_Limits.Location = new System.Drawing.Point(3, 110);
            this.ux_ControlButton_Limits.MinimumSize = new System.Drawing.Size(75, 100);
            this.ux_ControlButton_Limits.Name = "ux_ControlButton_Limits";
            this.ux_ControlButton_Limits.Size = new System.Drawing.Size(75, 100);
            this.ux_ControlButton_Limits.TabIndex = 18;
            this.ux_ControlButton_Limits.MouseClick += new System.Windows.Forms.MouseEventHandler(this.IconLimitsClick);
            // 
            // ux_ControlButton_Home
            // 
            this.ux_ControlButton_Home.Location = new System.Drawing.Point(3, 217);
            this.ux_ControlButton_Home.MinimumSize = new System.Drawing.Size(75, 100);
            this.ux_ControlButton_Home.Name = "ux_ControlButton_Home";
            this.ux_ControlButton_Home.Size = new System.Drawing.Size(75, 100);
            this.ux_ControlButton_Home.TabIndex = 19;
            this.ux_ControlButton_Home.Click += new System.EventHandler(this.IconHomeClick);
            // 
            // ux_ControlButton_Edit
            // 
            this.ux_ControlButton_Edit.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_ControlButton_Edit.Location = new System.Drawing.Point(3, 324);
            this.ux_ControlButton_Edit.MinimumSize = new System.Drawing.Size(75, 100);
            this.ux_ControlButton_Edit.Name = "ux_ControlButton_Edit";
            this.ux_ControlButton_Edit.Size = new System.Drawing.Size(75, 100);
            this.ux_ControlButton_Edit.TabIndex = 20;
            this.ux_ControlButton_Edit.Click += new System.EventHandler(this.IconEditDataClick);
            // 
            // ux_ControlButton_Clear
            // 
            this.ux_ControlButton_Clear.Location = new System.Drawing.Point(3, 431);
            this.ux_ControlButton_Clear.MinimumSize = new System.Drawing.Size(75, 100);
            this.ux_ControlButton_Clear.Name = "ux_ControlButton_Clear";
            this.ux_ControlButton_Clear.Size = new System.Drawing.Size(75, 100);
            this.ux_ControlButton_Clear.TabIndex = 21;
            this.ux_ControlButton_Clear.Click += new System.EventHandler(this.IconSequenceClearClick);
            // 
            // ux_Layout_Main
            // 
            this.ux_Layout_Main.BackColor = System.Drawing.Color.Transparent;
            this.ux_Layout_Main.ColumnCount = 2;
            this.ux_Layout_Main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_Layout_Main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.ux_Layout_Main.Controls.Add(this.ux_HMI, 0, 0);
            this.ux_Layout_Main.Controls.Add(this.ux_Layout_MainControls, 1, 0);
            this.ux_Layout_Main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_Main.Location = new System.Drawing.Point(0, 0);
            this.ux_Layout_Main.Name = "ux_Layout_Main";
            this.ux_Layout_Main.RowCount = 1;
            this.ux_Layout_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_Layout_Main.Size = new System.Drawing.Size(784, 541);
            this.ux_Layout_Main.TabIndex = 67;
            // 
            // ux_HMI
            // 
            this.ux_HMI.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.ux_HMI.Controls.Add(this.ux_Tab_Main);
            this.ux_HMI.Controls.Add(this.ux_Tab_Color);
            this.ux_HMI.Controls.Add(this.ux_Tab_EventLog);
            this.ux_HMI.Controls.Add(this.ux_Tab_IO);
            this.ux_HMI.Controls.Add(this.ux_Tab_General);
            this.ux_HMI.Controls.Add(this.ux_Tab_Settings);
            this.ux_HMI.Controls.Add(this.ux_Tab_MarkInProgress);
            this.ux_HMI.Controls.Add(this.ux_Tab_Focus);
            this.ux_HMI.Controls.Add(this.ux_Tab_DataEdit);
            this.ux_HMI.Controls.Add(this.ux_Tab_PLC);
            this.ux_HMI.Controls.Add(this.ux_Tab_FileImport);
            this.ux_HMI.Controls.Add(this.ux_Tab_Auth);
            this.ux_HMI.Controls.Add(this.ux_Tab_About);
            this.ux_HMI.Controls.Add(this.ux_Tab_JobSelect);
            this.ux_HMI.Controls.Add(this.ux_Tab_Axis);
            this.ux_HMI.Controls.Add(this.ux_Tab_Global);
            this.ux_HMI.Controls.Add(this.ux_Tab_Moves);
            this.ux_HMI.Controls.Add(this.ux_tab_pg);
            this.ux_HMI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_HMI.Location = new System.Drawing.Point(3, 3);
            this.ux_HMI.Multiline = true;
            this.ux_HMI.Name = "ux_HMI";
            this.ux_HMI.SelectedIndex = 0;
            this.ux_HMI.Size = new System.Drawing.Size(693, 535);
            this.ux_HMI.TabIndex = 10;
            this.ux_HMI.TabStop = false;
            this.ux_HMI.Enter += new System.EventHandler(this.TabEnter);
            this.ux_HMI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HmiKeyPress);
            // 
            // ux_Tab_Main
            // 
            this.ux_Tab_Main.Controls.Add(this.ux_Layout_MainInner);
            this.ux_Tab_Main.Location = new System.Drawing.Point(4, 4);
            this.ux_Tab_Main.Name = "ux_Tab_Main";
            this.ux_Tab_Main.Padding = new System.Windows.Forms.Padding(3);
            this.ux_Tab_Main.Size = new System.Drawing.Size(685, 491);
            this.ux_Tab_Main.TabIndex = 0;
            this.ux_Tab_Main.Text = "main";
            this.ux_Tab_Main.UseVisualStyleBackColor = true;
            this.ux_Tab_Main.Enter += new System.EventHandler(this.TabEnter);
            // 
            // ux_Layout_MainInner
            // 
            this.ux_Layout_MainInner.ColumnCount = 2;
            this.ux_Layout_MainInner.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_MainInner.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_MainInner.Controls.Add(this.ux_JobList, 0, 0);
            this.ux_Layout_MainInner.Controls.Add(this.ux_Layout_JobListNav, 0, 1);
            this.ux_Layout_MainInner.Controls.Add(this.ux_Layout_ScanInput, 1, 1);
            this.ux_Layout_MainInner.Controls.Add(this.ux_PreviewPic, 1, 0);
            this.ux_Layout_MainInner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_MainInner.Location = new System.Drawing.Point(3, 3);
            this.ux_Layout_MainInner.Name = "ux_Layout_MainInner";
            this.ux_Layout_MainInner.RowCount = 2;
            this.ux_Layout_MainInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_Layout_MainInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.ux_Layout_MainInner.Size = new System.Drawing.Size(679, 485);
            this.ux_Layout_MainInner.TabIndex = 7;
            // 
            // ux_JobList
            // 
            this.ux_JobList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_JobList.Location = new System.Drawing.Point(3, 3);
            this.ux_JobList.Name = "ux_JobList";
            this.ux_JobList.Size = new System.Drawing.Size(179, 412);
            this.ux_JobList.TabIndex = 5;
            // 
            // ux_Layout_JobListNav
            // 
            this.ux_Layout_JobListNav.ColumnCount = 3;
            this.ux_Layout_JobListNav.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_Layout_JobListNav.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_Layout_JobListNav.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_Layout_JobListNav.Controls.Add(this.ux_Nav_JobList, 0, 0);
            this.ux_Layout_JobListNav.Controls.Add(this.ux_Nav_NextJobScreen, 2, 0);
            this.ux_Layout_JobListNav.Controls.Add(this.ux_Nav_PrevJobScreen, 0, 0);
            this.ux_Layout_JobListNav.Location = new System.Drawing.Point(3, 421);
            this.ux_Layout_JobListNav.Name = "ux_Layout_JobListNav";
            this.ux_Layout_JobListNav.RowCount = 1;
            this.ux_Layout_JobListNav.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_Layout_JobListNav.Size = new System.Drawing.Size(179, 61);
            this.ux_Layout_JobListNav.TabIndex = 66;
            // 
            // ux_Nav_JobList
            // 
            this.ux_Nav_JobList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Nav_JobList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Nav_JobList.Image = ((System.Drawing.Image)(resources.GetObject("ux_Nav_JobList.Image")));
            this.ux_Nav_JobList.Location = new System.Drawing.Point(62, 3);
            this.ux_Nav_JobList.Name = "ux_Nav_JobList";
            this.ux_Nav_JobList.Size = new System.Drawing.Size(53, 55);
            this.ux_Nav_JobList.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Nav_JobList.TabIndex = 12;
            this.ux_Nav_JobList.TabStop = false;
            this.ux_Nav_JobList.Visible = false;
            this.ux_Nav_JobList.Click += new System.EventHandler(this.NavJobListClick);
            // 
            // ux_Nav_NextJobScreen
            // 
            this.ux_Nav_NextJobScreen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Nav_NextJobScreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Nav_NextJobScreen.Image = ((System.Drawing.Image)(resources.GetObject("ux_Nav_NextJobScreen.Image")));
            this.ux_Nav_NextJobScreen.Location = new System.Drawing.Point(121, 3);
            this.ux_Nav_NextJobScreen.Name = "ux_Nav_NextJobScreen";
            this.ux_Nav_NextJobScreen.Size = new System.Drawing.Size(55, 55);
            this.ux_Nav_NextJobScreen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Nav_NextJobScreen.TabIndex = 9;
            this.ux_Nav_NextJobScreen.TabStop = false;
            this.ux_Nav_NextJobScreen.Visible = false;
            this.ux_Nav_NextJobScreen.Click += new System.EventHandler(this.PicNextListClick);
            // 
            // ux_Nav_PrevJobScreen
            // 
            this.ux_Nav_PrevJobScreen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Nav_PrevJobScreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Nav_PrevJobScreen.Image = ((System.Drawing.Image)(resources.GetObject("ux_Nav_PrevJobScreen.Image")));
            this.ux_Nav_PrevJobScreen.Location = new System.Drawing.Point(3, 3);
            this.ux_Nav_PrevJobScreen.Name = "ux_Nav_PrevJobScreen";
            this.ux_Nav_PrevJobScreen.Size = new System.Drawing.Size(53, 55);
            this.ux_Nav_PrevJobScreen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Nav_PrevJobScreen.TabIndex = 8;
            this.ux_Nav_PrevJobScreen.TabStop = false;
            this.ux_Nav_PrevJobScreen.Visible = false;
            this.ux_Nav_PrevJobScreen.Click += new System.EventHandler(this.PicPrevListClick);
            // 
            // ux_Layout_ScanInput
            // 
            this.ux_Layout_ScanInput.ColumnCount = 1;
            this.ux_Layout_ScanInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_ScanInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.ux_Layout_ScanInput.Controls.Add(this.ux_Layout_ScannerInput, 0, 0);
            this.ux_Layout_ScanInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_ScanInput.Location = new System.Drawing.Point(188, 421);
            this.ux_Layout_ScanInput.Name = "ux_Layout_ScanInput";
            this.ux_Layout_ScanInput.RowCount = 1;
            this.ux_Layout_ScanInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_Layout_ScanInput.Size = new System.Drawing.Size(498, 61);
            this.ux_Layout_ScanInput.TabIndex = 67;
            // 
            // ux_Layout_ScannerInput
            // 
            this.ux_Layout_ScannerInput.ColumnCount = 6;
            this.ux_Layout_ScannerInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_Layout_ScannerInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_ScannerInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_ScannerInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_ScannerInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_ScannerInput.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_ScannerInput.Controls.Add(this.ux_ScannerInput, 0, 0);
            this.ux_Layout_ScannerInput.Controls.Add(this.flowLayoutPanel2, 4, 0);
            this.ux_Layout_ScannerInput.Controls.Add(this.flowLayoutPanel1, 3, 0);
            this.ux_Layout_ScannerInput.Controls.Add(this.ux_cb_lock, 5, 0);
            this.ux_Layout_ScannerInput.Controls.Add(this.ux_Control_Quantity, 2, 0);
            this.ux_Layout_ScannerInput.Controls.Add(this.ux_Text_StartingSerial, 1, 0);
            this.ux_Layout_ScannerInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_ScannerInput.Location = new System.Drawing.Point(3, 3);
            this.ux_Layout_ScannerInput.Name = "ux_Layout_ScannerInput";
            this.ux_Layout_ScannerInput.RowCount = 1;
            this.ux_Layout_ScannerInput.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_Layout_ScannerInput.Size = new System.Drawing.Size(492, 55);
            this.ux_Layout_ScannerInput.TabIndex = 67;
            // 
            // ux_ScannerInput
            // 
            this.ux_ScannerInput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_ScannerInput.Cue = "Scanner Input";
            this.ux_ScannerInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_ScannerInput.Location = new System.Drawing.Point(3, 13);
            this.ux_ScannerInput.Name = "ux_ScannerInput";
            this.ux_ScannerInput.Size = new System.Drawing.Size(1, 29);
            this.ux_ScannerInput.TabIndex = 69;
            this.ux_ScannerInput.TextChanged += new System.EventHandler(this.ScannerInputTextChanged);
            this.ux_ScannerInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ScannerInputKeyDown);
            this.ux_ScannerInput.Validating += new System.ComponentModel.CancelEventHandler(this.ScannerInputValidating);
            // 
            // ux_Control_Quantity
            // 
            this.ux_Control_Quantity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ux_Control_Quantity.Location = new System.Drawing.Point(67, 3);
            this.ux_Control_Quantity.MinimumSize = new System.Drawing.Size(209, 47);
            this.ux_Control_Quantity.Name = "ux_Control_Quantity";
            this.ux_Control_Quantity.Size = new System.Drawing.Size(209, 49);
            this.ux_Control_Quantity.TabIndex = 70;
            this.ux_Control_Quantity.Value = 1;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.Controls.Add(this.label4);
            this.flowLayoutPanel2.Controls.Add(this.ux_nud_fontHeight);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(368, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(80, 49);
            this.flowLayoutPanel2.TabIndex = 73;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Height";
            // 
            // ux_nud_fontHeight
            // 
            this.ux_nud_fontHeight.AutoSize = true;
            this.ux_nud_fontHeight.DecimalPlaces = 2;
            this.ux_nud_fontHeight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_nud_fontHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_nud_fontHeight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.ux_nud_fontHeight.Location = new System.Drawing.Point(3, 16);
            this.ux_nud_fontHeight.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.ux_nud_fontHeight.Name = "ux_nud_fontHeight";
            this.ux_nud_fontHeight.Size = new System.Drawing.Size(74, 26);
            this.ux_nud_fontHeight.TabIndex = 1;
            this.ux_nud_fontHeight.ValueChanged += new System.EventHandler(this.ux_nud_fontHeight_ValueChanged);
            // 
            // ux_Text_StartingSerial
            // 
            this.ux_Text_StartingSerial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Text_StartingSerial.Cue = "Serial";
            this.ux_Text_StartingSerial.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Text_StartingSerial.Location = new System.Drawing.Point(-39, 13);
            this.ux_Text_StartingSerial.Name = "ux_Text_StartingSerial";
            this.ux_Text_StartingSerial.Size = new System.Drawing.Size(100, 29);
            this.ux_Text_StartingSerial.TabIndex = 71;
            this.ux_Text_StartingSerial.Visible = false;
            this.ux_Text_StartingSerial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Ux_Text_StartingSerial_KeyDown);
            this.ux_Text_StartingSerial.Validating += new System.ComponentModel.CancelEventHandler(this.Ux_Text_StartingSerial_Validating);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.label3);
            this.flowLayoutPanel1.Controls.Add(this.ux_nud_FontWidth);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(282, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(80, 49);
            this.flowLayoutPanel1.TabIndex = 72;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Width";
            // 
            // ux_nud_FontWidth
            // 
            this.ux_nud_FontWidth.AutoSize = true;
            this.ux_nud_FontWidth.DecimalPlaces = 2;
            this.ux_nud_FontWidth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_nud_FontWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_nud_FontWidth.Location = new System.Drawing.Point(3, 16);
            this.ux_nud_FontWidth.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.ux_nud_FontWidth.Name = "ux_nud_FontWidth";
            this.ux_nud_FontWidth.Size = new System.Drawing.Size(74, 26);
            this.ux_nud_FontWidth.TabIndex = 1;
            this.ux_nud_FontWidth.ValueChanged += new System.EventHandler(this.ux_nud_FontWidth_ValueChanged);
            // 
            // ux_cb_lock
            // 
            this.ux_cb_lock.AutoSize = true;
            this.ux_cb_lock.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ux_cb_lock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_cb_lock.Location = new System.Drawing.Point(454, 3);
            this.ux_cb_lock.Name = "ux_cb_lock";
            this.ux_cb_lock.Size = new System.Drawing.Size(35, 49);
            this.ux_cb_lock.TabIndex = 74;
            this.ux_cb_lock.Text = "Lock";
            this.ux_cb_lock.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ux_cb_lock.UseVisualStyleBackColor = true;
            // 
            // ux_PreviewPic
            // 
            this.ux_PreviewPic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_PreviewPic.Location = new System.Drawing.Point(188, 3);
            this.ux_PreviewPic.Name = "ux_PreviewPic";
            this.ux_PreviewPic.Size = new System.Drawing.Size(498, 412);
            this.ux_PreviewPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.ux_PreviewPic.TabIndex = 68;
            this.ux_PreviewPic.TabStop = false;
            // 
            // ux_Tab_Color
            // 
            this.ux_Tab_Color.Controls.Add(this.ux_tlp_Theme_Main);
            this.ux_Tab_Color.Location = new System.Drawing.Point(4, 4);
            this.ux_Tab_Color.Name = "ux_Tab_Color";
            this.ux_Tab_Color.Padding = new System.Windows.Forms.Padding(3);
            this.ux_Tab_Color.Size = new System.Drawing.Size(685, 491);
            this.ux_Tab_Color.TabIndex = 2;
            this.ux_Tab_Color.Text = "clr";
            this.ux_Tab_Color.UseVisualStyleBackColor = true;
            this.ux_Tab_Color.Enter += new System.EventHandler(this.TabEnter);
            // 
            // ux_tlp_Theme_Main
            // 
            this.ux_tlp_Theme_Main.ColumnCount = 1;
            this.ux_tlp_Theme_Main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ux_tlp_Theme_Main.Controls.Add(this.ux_Layout_Colors, 0, 0);
            this.ux_tlp_Theme_Main.Controls.Add(this.ux_Layout_FullScreenMode, 0, 1);
            this.ux_tlp_Theme_Main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_tlp_Theme_Main.Location = new System.Drawing.Point(3, 3);
            this.ux_tlp_Theme_Main.Name = "ux_tlp_Theme_Main";
            this.ux_tlp_Theme_Main.RowCount = 2;
            this.ux_tlp_Theme_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67.39563F));
            this.ux_tlp_Theme_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.60437F));
            this.ux_tlp_Theme_Main.Size = new System.Drawing.Size(679, 485);
            this.ux_tlp_Theme_Main.TabIndex = 85;
            // 
            // ux_Layout_Colors
            // 
            this.ux_Layout_Colors.ColumnCount = 11;
            this.ux_Layout_Colors.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_Colors.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_Colors.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_Colors.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_Colors.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_Colors.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_Colors.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_Colors.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_Colors.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_Colors.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.ux_Layout_Colors.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_BGRed, 1, 1);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_BGOrange, 2, 1);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_StatusCustom, 10, 5);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_StatusColorLabel, 0, 5);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_StatusBlack, 8, 5);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_FontLabel, 0, 3);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_StatusViolet, 7, 5);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_BGColorLabel, 0, 1);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_StatusLightBlue, 6, 5);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_StatusBlue, 5, 5);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_StatusGreen, 4, 5);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_StatusYellow, 3, 5);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_StatusOrange, 2, 5);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_StatusRed, 1, 5);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_BGYellow, 3, 1);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_BGViolet, 7, 1);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_FontViolet, 7, 3);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_BGGreen, 4, 1);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_FontBlack, 8, 3);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_BGBlue, 5, 1);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_FontCustom, 10, 3);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_BGWhite, 8, 1);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_BGLightBlue, 6, 1);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_BGCustom, 10, 1);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_FontRed, 1, 3);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_FontOrange, 2, 3);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_FontYellow, 3, 3);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_FontGreen, 4, 3);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_FontBlue, 5, 3);
            this.ux_Layout_Colors.Controls.Add(this.ux_Set_FontLightBlue, 6, 3);
            this.ux_Layout_Colors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_Colors.Location = new System.Drawing.Point(3, 3);
            this.ux_Layout_Colors.Name = "ux_Layout_Colors";
            this.ux_Layout_Colors.RowCount = 6;
            this.ux_Layout_Colors.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555554F));
            this.ux_Layout_Colors.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.77778F));
            this.ux_Layout_Colors.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555554F));
            this.ux_Layout_Colors.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.77778F));
            this.ux_Layout_Colors.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555554F));
            this.ux_Layout_Colors.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.77778F));
            this.ux_Layout_Colors.Size = new System.Drawing.Size(673, 320);
            this.ux_Layout_Colors.TabIndex = 83;
            // 
            // ux_Set_BGRed
            // 
            this.ux_Set_BGRed.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_BGRed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ux_Set_BGRed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_BGRed.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_BGRed.ForeColor = System.Drawing.Color.Red;
            this.ux_Set_BGRed.Location = new System.Drawing.Point(74, 38);
            this.ux_Set_BGRed.Name = "ux_Set_BGRed";
            this.ux_Set_BGRed.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_BGRed.TabIndex = 52;
            // 
            // ux_Set_BGOrange
            // 
            this.ux_Set_BGOrange.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_BGOrange.BackColor = System.Drawing.Color.BurlyWood;
            this.ux_Set_BGOrange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_BGOrange.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_BGOrange.Location = new System.Drawing.Point(125, 38);
            this.ux_Set_BGOrange.Name = "ux_Set_BGOrange";
            this.ux_Set_BGOrange.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_BGOrange.TabIndex = 57;
            // 
            // ux_Set_StatusCustom
            // 
            this.ux_Set_StatusCustom.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_StatusCustom.BackColor = System.Drawing.Color.White;
            this.ux_Set_StatusCustom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_StatusCustom.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_StatusCustom.Location = new System.Drawing.Point(563, 251);
            this.ux_Set_StatusCustom.Name = "ux_Set_StatusCustom";
            this.ux_Set_StatusCustom.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_StatusCustom.TabIndex = 73;
            this.ux_Set_StatusCustom.Click += new System.EventHandler(this.ColorChooserStatus);
            // 
            // ux_Set_StatusColorLabel
            // 
            this.ux_Set_StatusColorLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Set_StatusColorLabel.AutoSize = true;
            this.ux_Set_StatusColorLabel.Location = new System.Drawing.Point(3, 267);
            this.ux_Set_StatusColorLabel.Name = "ux_Set_StatusColorLabel";
            this.ux_Set_StatusColorLabel.Size = new System.Drawing.Size(65, 13);
            this.ux_Set_StatusColorLabel.TabIndex = 71;
            this.ux_Set_StatusColorLabel.Text = "Status Bar";
            // 
            // ux_Set_StatusBlack
            // 
            this.ux_Set_StatusBlack.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_StatusBlack.BackColor = System.Drawing.Color.Black;
            this.ux_Set_StatusBlack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_StatusBlack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_StatusBlack.Location = new System.Drawing.Point(431, 251);
            this.ux_Set_StatusBlack.Name = "ux_Set_StatusBlack";
            this.ux_Set_StatusBlack.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_StatusBlack.TabIndex = 77;
            // 
            // ux_Set_FontLabel
            // 
            this.ux_Set_FontLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Set_FontLabel.AutoSize = true;
            this.ux_Set_FontLabel.BackColor = System.Drawing.Color.Transparent;
            this.ux_Set_FontLabel.Location = new System.Drawing.Point(3, 159);
            this.ux_Set_FontLabel.Name = "ux_Set_FontLabel";
            this.ux_Set_FontLabel.Size = new System.Drawing.Size(65, 13);
            this.ux_Set_FontLabel.TabIndex = 60;
            this.ux_Set_FontLabel.Text = "Font";
            // 
            // ux_Set_StatusViolet
            // 
            this.ux_Set_StatusViolet.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_StatusViolet.BackColor = System.Drawing.Color.Thistle;
            this.ux_Set_StatusViolet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_StatusViolet.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_StatusViolet.Location = new System.Drawing.Point(380, 251);
            this.ux_Set_StatusViolet.Name = "ux_Set_StatusViolet";
            this.ux_Set_StatusViolet.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_StatusViolet.TabIndex = 75;
            // 
            // ux_Set_BGColorLabel
            // 
            this.ux_Set_BGColorLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Set_BGColorLabel.AutoSize = true;
            this.ux_Set_BGColorLabel.Location = new System.Drawing.Point(3, 54);
            this.ux_Set_BGColorLabel.Name = "ux_Set_BGColorLabel";
            this.ux_Set_BGColorLabel.Size = new System.Drawing.Size(65, 13);
            this.ux_Set_BGColorLabel.TabIndex = 51;
            this.ux_Set_BGColorLabel.Text = "Background";
            // 
            // ux_Set_StatusLightBlue
            // 
            this.ux_Set_StatusLightBlue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_StatusLightBlue.BackColor = System.Drawing.Color.PaleTurquoise;
            this.ux_Set_StatusLightBlue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_StatusLightBlue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_StatusLightBlue.Location = new System.Drawing.Point(329, 251);
            this.ux_Set_StatusLightBlue.Name = "ux_Set_StatusLightBlue";
            this.ux_Set_StatusLightBlue.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_StatusLightBlue.TabIndex = 78;
            // 
            // ux_Set_StatusBlue
            // 
            this.ux_Set_StatusBlue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_StatusBlue.BackColor = System.Drawing.Color.SteelBlue;
            this.ux_Set_StatusBlue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_StatusBlue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_StatusBlue.Location = new System.Drawing.Point(278, 251);
            this.ux_Set_StatusBlue.Name = "ux_Set_StatusBlue";
            this.ux_Set_StatusBlue.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_StatusBlue.TabIndex = 79;
            // 
            // ux_Set_StatusGreen
            // 
            this.ux_Set_StatusGreen.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_StatusGreen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ux_Set_StatusGreen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_StatusGreen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_StatusGreen.Location = new System.Drawing.Point(227, 251);
            this.ux_Set_StatusGreen.Name = "ux_Set_StatusGreen";
            this.ux_Set_StatusGreen.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_StatusGreen.TabIndex = 80;
            // 
            // ux_Set_StatusYellow
            // 
            this.ux_Set_StatusYellow.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_StatusYellow.BackColor = System.Drawing.Color.LemonChiffon;
            this.ux_Set_StatusYellow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_StatusYellow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_StatusYellow.Location = new System.Drawing.Point(176, 251);
            this.ux_Set_StatusYellow.Name = "ux_Set_StatusYellow";
            this.ux_Set_StatusYellow.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_StatusYellow.TabIndex = 76;
            // 
            // ux_Set_StatusOrange
            // 
            this.ux_Set_StatusOrange.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_StatusOrange.BackColor = System.Drawing.Color.BurlyWood;
            this.ux_Set_StatusOrange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_StatusOrange.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_StatusOrange.Location = new System.Drawing.Point(125, 251);
            this.ux_Set_StatusOrange.Name = "ux_Set_StatusOrange";
            this.ux_Set_StatusOrange.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_StatusOrange.TabIndex = 74;
            // 
            // ux_Set_StatusRed
            // 
            this.ux_Set_StatusRed.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_StatusRed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ux_Set_StatusRed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_StatusRed.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_StatusRed.ForeColor = System.Drawing.Color.Red;
            this.ux_Set_StatusRed.Location = new System.Drawing.Point(74, 251);
            this.ux_Set_StatusRed.Name = "ux_Set_StatusRed";
            this.ux_Set_StatusRed.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_StatusRed.TabIndex = 72;
            // 
            // ux_Set_BGYellow
            // 
            this.ux_Set_BGYellow.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_BGYellow.BackColor = System.Drawing.Color.LemonChiffon;
            this.ux_Set_BGYellow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_BGYellow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_BGYellow.Location = new System.Drawing.Point(176, 38);
            this.ux_Set_BGYellow.Name = "ux_Set_BGYellow";
            this.ux_Set_BGYellow.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_BGYellow.TabIndex = 54;
            // 
            // ux_Set_BGViolet
            // 
            this.ux_Set_BGViolet.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_BGViolet.BackColor = System.Drawing.Color.Thistle;
            this.ux_Set_BGViolet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_BGViolet.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_BGViolet.Location = new System.Drawing.Point(380, 38);
            this.ux_Set_BGViolet.Name = "ux_Set_BGViolet";
            this.ux_Set_BGViolet.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_BGViolet.TabIndex = 56;
            // 
            // ux_Set_FontViolet
            // 
            this.ux_Set_FontViolet.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_FontViolet.BackColor = System.Drawing.Color.Thistle;
            this.ux_Set_FontViolet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_FontViolet.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_FontViolet.Location = new System.Drawing.Point(380, 143);
            this.ux_Set_FontViolet.Name = "ux_Set_FontViolet";
            this.ux_Set_FontViolet.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_FontViolet.TabIndex = 65;
            // 
            // ux_Set_BGGreen
            // 
            this.ux_Set_BGGreen.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_BGGreen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ux_Set_BGGreen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_BGGreen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_BGGreen.Location = new System.Drawing.Point(227, 38);
            this.ux_Set_BGGreen.Name = "ux_Set_BGGreen";
            this.ux_Set_BGGreen.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_BGGreen.TabIndex = 55;
            // 
            // ux_Set_FontBlack
            // 
            this.ux_Set_FontBlack.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_FontBlack.BackColor = System.Drawing.Color.Black;
            this.ux_Set_FontBlack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_FontBlack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_FontBlack.Location = new System.Drawing.Point(431, 143);
            this.ux_Set_FontBlack.Name = "ux_Set_FontBlack";
            this.ux_Set_FontBlack.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_FontBlack.TabIndex = 66;
            // 
            // ux_Set_BGBlue
            // 
            this.ux_Set_BGBlue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_BGBlue.BackColor = System.Drawing.Color.SteelBlue;
            this.ux_Set_BGBlue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_BGBlue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_BGBlue.Location = new System.Drawing.Point(278, 38);
            this.ux_Set_BGBlue.Name = "ux_Set_BGBlue";
            this.ux_Set_BGBlue.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_BGBlue.TabIndex = 58;
            // 
            // ux_Set_FontCustom
            // 
            this.ux_Set_FontCustom.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_FontCustom.BackColor = System.Drawing.Color.White;
            this.ux_Set_FontCustom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_FontCustom.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_FontCustom.Location = new System.Drawing.Point(563, 143);
            this.ux_Set_FontCustom.Name = "ux_Set_FontCustom";
            this.ux_Set_FontCustom.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_FontCustom.TabIndex = 63;
            this.ux_Set_FontCustom.Click += new System.EventHandler(this.ColorChooserFg);
            // 
            // ux_Set_BGWhite
            // 
            this.ux_Set_BGWhite.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_BGWhite.BackColor = System.Drawing.Color.White;
            this.ux_Set_BGWhite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_BGWhite.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_BGWhite.Location = new System.Drawing.Point(431, 38);
            this.ux_Set_BGWhite.Name = "ux_Set_BGWhite";
            this.ux_Set_BGWhite.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_BGWhite.TabIndex = 59;
            // 
            // ux_Set_BGLightBlue
            // 
            this.ux_Set_BGLightBlue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_BGLightBlue.BackColor = System.Drawing.Color.PaleTurquoise;
            this.ux_Set_BGLightBlue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_BGLightBlue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_BGLightBlue.Location = new System.Drawing.Point(329, 38);
            this.ux_Set_BGLightBlue.Name = "ux_Set_BGLightBlue";
            this.ux_Set_BGLightBlue.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_BGLightBlue.TabIndex = 53;
            // 
            // ux_Set_BGCustom
            // 
            this.ux_Set_BGCustom.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_BGCustom.BackColor = System.Drawing.Color.White;
            this.ux_Set_BGCustom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_BGCustom.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_BGCustom.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.ux_Set_BGCustom.Location = new System.Drawing.Point(563, 38);
            this.ux_Set_BGCustom.Name = "ux_Set_BGCustom";
            this.ux_Set_BGCustom.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_BGCustom.TabIndex = 61;
            this.ux_Set_BGCustom.Click += new System.EventHandler(this.ColorChooserBg);
            // 
            // ux_Set_FontRed
            // 
            this.ux_Set_FontRed.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_FontRed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ux_Set_FontRed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_FontRed.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_FontRed.ForeColor = System.Drawing.Color.Red;
            this.ux_Set_FontRed.Location = new System.Drawing.Point(74, 143);
            this.ux_Set_FontRed.Name = "ux_Set_FontRed";
            this.ux_Set_FontRed.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_FontRed.TabIndex = 62;
            // 
            // ux_Set_FontOrange
            // 
            this.ux_Set_FontOrange.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_FontOrange.BackColor = System.Drawing.Color.BurlyWood;
            this.ux_Set_FontOrange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_FontOrange.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_FontOrange.Location = new System.Drawing.Point(125, 143);
            this.ux_Set_FontOrange.Name = "ux_Set_FontOrange";
            this.ux_Set_FontOrange.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_FontOrange.TabIndex = 64;
            // 
            // ux_Set_FontYellow
            // 
            this.ux_Set_FontYellow.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_FontYellow.BackColor = System.Drawing.Color.LemonChiffon;
            this.ux_Set_FontYellow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_FontYellow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_FontYellow.Location = new System.Drawing.Point(176, 143);
            this.ux_Set_FontYellow.Name = "ux_Set_FontYellow";
            this.ux_Set_FontYellow.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_FontYellow.TabIndex = 67;
            // 
            // ux_Set_FontGreen
            // 
            this.ux_Set_FontGreen.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_FontGreen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ux_Set_FontGreen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_FontGreen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_FontGreen.Location = new System.Drawing.Point(227, 143);
            this.ux_Set_FontGreen.Name = "ux_Set_FontGreen";
            this.ux_Set_FontGreen.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_FontGreen.TabIndex = 70;
            // 
            // ux_Set_FontBlue
            // 
            this.ux_Set_FontBlue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_FontBlue.BackColor = System.Drawing.Color.SteelBlue;
            this.ux_Set_FontBlue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_FontBlue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_FontBlue.Location = new System.Drawing.Point(278, 143);
            this.ux_Set_FontBlue.Name = "ux_Set_FontBlue";
            this.ux_Set_FontBlue.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_FontBlue.TabIndex = 69;
            // 
            // ux_Set_FontLightBlue
            // 
            this.ux_Set_FontLightBlue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_FontLightBlue.BackColor = System.Drawing.Color.PaleTurquoise;
            this.ux_Set_FontLightBlue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Set_FontLightBlue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_FontLightBlue.Location = new System.Drawing.Point(329, 143);
            this.ux_Set_FontLightBlue.Name = "ux_Set_FontLightBlue";
            this.ux_Set_FontLightBlue.Size = new System.Drawing.Size(45, 45);
            this.ux_Set_FontLightBlue.TabIndex = 68;
            // 
            // ux_Layout_FullScreenMode
            // 
            this.ux_Layout_FullScreenMode.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetPartial;
            this.ux_Layout_FullScreenMode.ColumnCount = 4;
            this.ux_Layout_FullScreenMode.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 99.0099F));
            this.ux_Layout_FullScreenMode.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_FullScreenMode.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_FullScreenMode.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_FullScreenMode.Controls.Add(this.ux_Theme_SpellCheck, 0, 0);
            this.ux_Layout_FullScreenMode.Controls.Add(this.ux_tlp_BGImage, 1, 0);
            this.ux_Layout_FullScreenMode.Controls.Add(this.ux_Button_BackColors, 2, 0);
            this.ux_Layout_FullScreenMode.Controls.Add(this.ux_ControlButtonSave_Theme, 3, 0);
            this.ux_Layout_FullScreenMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_FullScreenMode.Location = new System.Drawing.Point(3, 329);
            this.ux_Layout_FullScreenMode.Name = "ux_Layout_FullScreenMode";
            this.ux_Layout_FullScreenMode.RowCount = 1;
            this.ux_Layout_FullScreenMode.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_Layout_FullScreenMode.Size = new System.Drawing.Size(673, 153);
            this.ux_Layout_FullScreenMode.TabIndex = 84;
            // 
            // ux_Theme_SpellCheck
            // 
            this.ux_Theme_SpellCheck.ColumnCount = 3;
            this.ux_Theme_SpellCheck.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_Theme_SpellCheck.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.18182F));
            this.ux_Theme_SpellCheck.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.07071F));
            this.ux_Theme_SpellCheck.Controls.Add(this.ux_CheckBox_HideJobList, 0, 2);
            this.ux_Theme_SpellCheck.Controls.Add(this.ux_Label_Language, 0, 0);
            this.ux_Theme_SpellCheck.Controls.Add(this.ux_cbs_LanguageSelect, 1, 0);
            this.ux_Theme_SpellCheck.Controls.Add(this.ux_Set_ToggleFullScreen, 0, 1);
            this.ux_Theme_SpellCheck.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Theme_SpellCheck.Location = new System.Drawing.Point(6, 6);
            this.ux_Theme_SpellCheck.Name = "ux_Theme_SpellCheck";
            this.ux_Theme_SpellCheck.RowCount = 3;
            this.ux_Theme_SpellCheck.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_Theme_SpellCheck.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_Theme_SpellCheck.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_Theme_SpellCheck.Size = new System.Drawing.Size(308, 141);
            this.ux_Theme_SpellCheck.TabIndex = 1;
            // 
            // ux_CheckBox_HideJobList
            // 
            this.ux_CheckBox_HideJobList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_CheckBox_HideJobList.AutoSize = true;
            this.ux_Theme_SpellCheck.SetColumnSpan(this.ux_CheckBox_HideJobList, 3);
            this.ux_CheckBox_HideJobList.Location = new System.Drawing.Point(3, 109);
            this.ux_CheckBox_HideJobList.Name = "ux_CheckBox_HideJobList";
            this.ux_CheckBox_HideJobList.Size = new System.Drawing.Size(302, 17);
            this.ux_CheckBox_HideJobList.TabIndex = 3;
            this.ux_CheckBox_HideJobList.Text = "Hide Job List";
            this.ux_CheckBox_HideJobList.UseVisualStyleBackColor = true;
            // 
            // ux_Label_Language
            // 
            this.ux_Label_Language.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Label_Language.AutoSize = true;
            this.ux_Label_Language.Location = new System.Drawing.Point(3, 17);
            this.ux_Label_Language.Name = "ux_Label_Language";
            this.ux_Label_Language.Size = new System.Drawing.Size(70, 13);
            this.ux_Label_Language.TabIndex = 0;
            this.ux_Label_Language.Text = "Language";
            // 
            // ux_cbs_LanguageSelect
            // 
            this.ux_cbs_LanguageSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_cbs_LanguageSelect.FormattingEnabled = true;
            this.ux_cbs_LanguageSelect.Location = new System.Drawing.Point(79, 13);
            this.ux_cbs_LanguageSelect.Name = "ux_cbs_LanguageSelect";
            this.ux_cbs_LanguageSelect.Size = new System.Drawing.Size(126, 21);
            this.ux_cbs_LanguageSelect.TabIndex = 1;
            // 
            // ux_Set_ToggleFullScreen
            // 
            this.ux_Set_ToggleFullScreen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Set_ToggleFullScreen.AutoSize = true;
            this.ux_Theme_SpellCheck.SetColumnSpan(this.ux_Set_ToggleFullScreen, 3);
            this.ux_Set_ToggleFullScreen.Location = new System.Drawing.Point(3, 62);
            this.ux_Set_ToggleFullScreen.Name = "ux_Set_ToggleFullScreen";
            this.ux_Set_ToggleFullScreen.Size = new System.Drawing.Size(302, 17);
            this.ux_Set_ToggleFullScreen.TabIndex = 0;
            this.ux_Set_ToggleFullScreen.Text = "Use Full Screen Mode";
            this.ux_Set_ToggleFullScreen.UseVisualStyleBackColor = true;
            // 
            // ux_tlp_BGImage
            // 
            this.ux_tlp_BGImage.ColumnCount = 1;
            this.ux_tlp_BGImage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ux_tlp_BGImage.Controls.Add(this.ux_Set_BackgroundImage, 0, 1);
            this.ux_tlp_BGImage.Controls.Add(this.ux_Set_BGPicLabel, 0, 0);
            this.ux_tlp_BGImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_tlp_BGImage.Location = new System.Drawing.Point(323, 6);
            this.ux_tlp_BGImage.Name = "ux_tlp_BGImage";
            this.ux_tlp_BGImage.RowCount = 2;
            this.ux_tlp_BGImage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.33333F));
            this.ux_tlp_BGImage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 84.66666F));
            this.ux_tlp_BGImage.Size = new System.Drawing.Size(156, 141);
            this.ux_tlp_BGImage.TabIndex = 83;
            // 
            // ux_Set_BackgroundImage
            // 
            this.ux_Set_BackgroundImage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Set_BackgroundImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Set_BackgroundImage.Image = ((System.Drawing.Image)(resources.GetObject("ux_Set_BackgroundImage.Image")));
            this.ux_Set_BackgroundImage.Location = new System.Drawing.Point(3, 24);
            this.ux_Set_BackgroundImage.Name = "ux_Set_BackgroundImage";
            this.ux_Set_BackgroundImage.Size = new System.Drawing.Size(150, 114);
            this.ux_Set_BackgroundImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Set_BackgroundImage.TabIndex = 81;
            this.ux_Set_BackgroundImage.TabStop = false;
            this.ux_Set_BackgroundImage.Click += new System.EventHandler(this.PbbgClick);
            // 
            // ux_Set_BGPicLabel
            // 
            this.ux_Set_BGPicLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Set_BGPicLabel.AutoSize = true;
            this.ux_Set_BGPicLabel.Location = new System.Drawing.Point(27, 4);
            this.ux_Set_BGPicLabel.Name = "ux_Set_BGPicLabel";
            this.ux_Set_BGPicLabel.Size = new System.Drawing.Size(101, 13);
            this.ux_Set_BGPicLabel.TabIndex = 82;
            this.ux_Set_BGPicLabel.Text = "Background Picture";
            // 
            // ux_Button_BackColors
            // 
            this.ux_Button_BackColors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Button_BackColors.Location = new System.Drawing.Point(488, 47);
            this.ux_Button_BackColors.MinimumSize = new System.Drawing.Size(85, 100);
            this.ux_Button_BackColors.Name = "ux_Button_BackColors";
            this.ux_Button_BackColors.Size = new System.Drawing.Size(85, 100);
            this.ux_Button_BackColors.TabIndex = 85;
            this.ux_Button_BackColors.Click += new System.EventHandler(this.ButtonThemeBackClick);
            // 
            // ux_ControlButtonSave_Theme
            // 
            this.ux_ControlButtonSave_Theme.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_ControlButtonSave_Theme.Location = new System.Drawing.Point(582, 47);
            this.ux_ControlButtonSave_Theme.MinimumSize = new System.Drawing.Size(75, 100);
            this.ux_ControlButtonSave_Theme.Name = "ux_ControlButtonSave_Theme";
            this.ux_ControlButtonSave_Theme.Size = new System.Drawing.Size(85, 100);
            this.ux_ControlButtonSave_Theme.TabIndex = 84;
            this.ux_ControlButtonSave_Theme.Click += new System.EventHandler(this.ButtonThemeSaveClick);
            // 
            // ux_Tab_EventLog
            // 
            this.ux_Tab_EventLog.Controls.Add(this.ux_EventLog);
            this.ux_Tab_EventLog.Location = new System.Drawing.Point(4, 4);
            this.ux_Tab_EventLog.Name = "ux_Tab_EventLog";
            this.ux_Tab_EventLog.Padding = new System.Windows.Forms.Padding(3);
            this.ux_Tab_EventLog.Size = new System.Drawing.Size(685, 491);
            this.ux_Tab_EventLog.TabIndex = 3;
            this.ux_Tab_EventLog.Text = "log";
            this.ux_Tab_EventLog.UseVisualStyleBackColor = true;
            this.ux_Tab_EventLog.Enter += new System.EventHandler(this.TabEnter);
            // 
            // ux_EventLog
            // 
            this.ux_EventLog.ContextMenuStrip = this.ux_Context_EventLog;
            this.ux_EventLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_EventLog.FullRowSelect = true;
            this.ux_EventLog.HideSelection = false;
            this.ux_EventLog.Location = new System.Drawing.Point(3, 3);
            this.ux_EventLog.Name = "ux_EventLog";
            this.ux_EventLog.Size = new System.Drawing.Size(679, 485);
            this.ux_EventLog.TabIndex = 0;
            this.ux_EventLog.UseCompatibleStateImageBehavior = false;
            this.ux_EventLog.View = System.Windows.Forms.View.Details;
            // 
            // ux_Context_EventLog
            // 
            this.ux_Context_EventLog.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ux_Context_EventLogCopy,
            this.ux_Context_EventClear});
            this.ux_Context_EventLog.Name = "ux_Context_EventLog";
            this.ux_Context_EventLog.Size = new System.Drawing.Size(103, 48);
            // 
            // ux_Context_EventLogCopy
            // 
            this.ux_Context_EventLogCopy.Name = "ux_Context_EventLogCopy";
            this.ux_Context_EventLogCopy.Size = new System.Drawing.Size(102, 22);
            this.ux_Context_EventLogCopy.Text = "Copy";
            this.ux_Context_EventLogCopy.Click += new System.EventHandler(this.ContextEventLogCopyClick);
            // 
            // ux_Context_EventClear
            // 
            this.ux_Context_EventClear.Name = "ux_Context_EventClear";
            this.ux_Context_EventClear.Size = new System.Drawing.Size(102, 22);
            this.ux_Context_EventClear.Text = "Clear";
            this.ux_Context_EventClear.Click += new System.EventHandler(this.ContextEventClearClick);
            // 
            // ux_Tab_IO
            // 
            this.ux_Tab_IO.Controls.Add(this.ux_Layout_IO);
            this.ux_Tab_IO.Location = new System.Drawing.Point(4, 4);
            this.ux_Tab_IO.Name = "ux_Tab_IO";
            this.ux_Tab_IO.Padding = new System.Windows.Forms.Padding(3);
            this.ux_Tab_IO.Size = new System.Drawing.Size(685, 491);
            this.ux_Tab_IO.TabIndex = 4;
            this.ux_Tab_IO.Text = "io";
            this.ux_Tab_IO.UseVisualStyleBackColor = true;
            this.ux_Tab_IO.Enter += new System.EventHandler(this.TabEnter);
            this.ux_Tab_IO.Leave += new System.EventHandler(this.TabIOMonitorLeave);
            // 
            // ux_Layout_IO
            // 
            this.ux_Layout_IO.ColumnCount = 16;
            this.ux_Layout_IO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_IO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.ux_Layout_IO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.ux_Layout_IO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.ux_Layout_IO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.ux_Layout_IO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.ux_Layout_IO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.ux_Layout_IO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.ux_Layout_IO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.ux_Layout_IO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.ux_Layout_IO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.ux_Layout_IO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.ux_Layout_IO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.ux_Layout_IO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.ux_Layout_IO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.ux_Layout_IO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Output16, 15, 3);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Input16, 15, 1);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Output15, 14, 3);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Input15, 14, 1);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Output14, 13, 3);
            this.ux_Layout_IO.Controls.Add(this.ux_IO_OutputsLabel, 0, 2);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Output13, 12, 3);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Input14, 13, 1);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Output12, 11, 3);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Input13, 12, 1);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Output11, 10, 3);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Input12, 11, 1);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Output10, 9, 3);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Input11, 10, 1);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Output9, 8, 3);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Input10, 9, 1);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Output8, 7, 3);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Input9, 8, 1);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Output7, 6, 3);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Input8, 7, 1);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Output6, 5, 3);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Input7, 6, 1);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Output5, 4, 3);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Input6, 5, 1);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Output4, 3, 3);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Input5, 4, 1);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Output3, 2, 3);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Input4, 3, 1);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Output2, 1, 3);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Input3, 2, 1);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Output1, 0, 3);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Input2, 1, 1);
            this.ux_Layout_IO.Controls.Add(this.ux_LED_Input1, 0, 1);
            this.ux_Layout_IO.Controls.Add(this.ux_IO_InputsLabel, 0, 0);
            this.ux_Layout_IO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_IO.Location = new System.Drawing.Point(3, 3);
            this.ux_Layout_IO.Name = "ux_Layout_IO";
            this.ux_Layout_IO.RowCount = 4;
            this.ux_Layout_IO.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.ux_Layout_IO.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_Layout_IO.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.ux_Layout_IO.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.ux_Layout_IO.Size = new System.Drawing.Size(679, 485);
            this.ux_Layout_IO.TabIndex = 2;
            // 
            // ux_LED_Output16
            // 
            this.ux_LED_Output16.IsEnabled = false;
            this.ux_LED_Output16.LEDLabel = "16";
            this.ux_LED_Output16.Location = new System.Drawing.Point(634, 138);
            this.ux_LED_Output16.Name = "ux_LED_Output16";
            this.ux_LED_Output16.Size = new System.Drawing.Size(42, 73);
            this.ux_LED_Output16.TabIndex = 15;
            this.ux_LED_Output16.Click += new System.EventHandler(this.Output16LedClick);
            // 
            // ux_LED_Input16
            // 
            this.ux_LED_Input16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_LED_Input16.IsEnabled = false;
            this.ux_LED_Input16.LEDLabel = "16";
            this.ux_LED_Input16.Location = new System.Drawing.Point(634, 34);
            this.ux_LED_Input16.Name = "ux_LED_Input16";
            this.ux_LED_Input16.Size = new System.Drawing.Size(42, 68);
            this.ux_LED_Input16.TabIndex = 9;
            // 
            // ux_LED_Output15
            // 
            this.ux_LED_Output15.IsEnabled = false;
            this.ux_LED_Output15.LEDLabel = "15";
            this.ux_LED_Output15.Location = new System.Drawing.Point(592, 138);
            this.ux_LED_Output15.Name = "ux_LED_Output15";
            this.ux_LED_Output15.Size = new System.Drawing.Size(36, 73);
            this.ux_LED_Output15.TabIndex = 14;
            this.ux_LED_Output15.Click += new System.EventHandler(this.Output15LedClick);
            // 
            // ux_LED_Input15
            // 
            this.ux_LED_Input15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_LED_Input15.IsEnabled = false;
            this.ux_LED_Input15.LEDLabel = "15";
            this.ux_LED_Input15.Location = new System.Drawing.Point(592, 34);
            this.ux_LED_Input15.Name = "ux_LED_Input15";
            this.ux_LED_Input15.Size = new System.Drawing.Size(36, 68);
            this.ux_LED_Input15.TabIndex = 37;
            // 
            // ux_LED_Output14
            // 
            this.ux_LED_Output14.IsEnabled = false;
            this.ux_LED_Output14.LEDLabel = "14";
            this.ux_LED_Output14.Location = new System.Drawing.Point(550, 138);
            this.ux_LED_Output14.Name = "ux_LED_Output14";
            this.ux_LED_Output14.Size = new System.Drawing.Size(35, 73);
            this.ux_LED_Output14.TabIndex = 13;
            this.ux_LED_Output14.Click += new System.EventHandler(this.Output14LedClick);
            // 
            // ux_IO_OutputsLabel
            // 
            this.ux_IO_OutputsLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_IO_OutputsLabel.AutoSize = true;
            this.ux_Layout_IO.SetColumnSpan(this.ux_IO_OutputsLabel, 16);
            this.ux_IO_OutputsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_IO_OutputsLabel.ForeColor = System.Drawing.Color.Black;
            this.ux_IO_OutputsLabel.Location = new System.Drawing.Point(298, 108);
            this.ux_IO_OutputsLabel.Name = "ux_IO_OutputsLabel";
            this.ux_IO_OutputsLabel.Size = new System.Drawing.Size(82, 24);
            this.ux_IO_OutputsLabel.TabIndex = 1;
            this.ux_IO_OutputsLabel.Text = "Outputs";
            // 
            // ux_LED_Output13
            // 
            this.ux_LED_Output13.IsEnabled = false;
            this.ux_LED_Output13.LEDLabel = "13";
            this.ux_LED_Output13.Location = new System.Drawing.Point(508, 138);
            this.ux_LED_Output13.Name = "ux_LED_Output13";
            this.ux_LED_Output13.Size = new System.Drawing.Size(35, 73);
            this.ux_LED_Output13.TabIndex = 12;
            this.ux_LED_Output13.Click += new System.EventHandler(this.Output13LedClick);
            // 
            // ux_LED_Input14
            // 
            this.ux_LED_Input14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_LED_Input14.IsEnabled = false;
            this.ux_LED_Input14.LEDLabel = "14";
            this.ux_LED_Input14.Location = new System.Drawing.Point(550, 34);
            this.ux_LED_Input14.Name = "ux_LED_Input14";
            this.ux_LED_Input14.Size = new System.Drawing.Size(36, 68);
            this.ux_LED_Input14.TabIndex = 36;
            // 
            // ux_LED_Output12
            // 
            this.ux_LED_Output12.IsEnabled = false;
            this.ux_LED_Output12.LEDLabel = "12";
            this.ux_LED_Output12.Location = new System.Drawing.Point(466, 138);
            this.ux_LED_Output12.Name = "ux_LED_Output12";
            this.ux_LED_Output12.Size = new System.Drawing.Size(35, 73);
            this.ux_LED_Output12.TabIndex = 11;
            this.ux_LED_Output12.Click += new System.EventHandler(this.Output12LedClick);
            // 
            // ux_LED_Input13
            // 
            this.ux_LED_Input13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_LED_Input13.IsEnabled = false;
            this.ux_LED_Input13.LEDLabel = "13";
            this.ux_LED_Input13.Location = new System.Drawing.Point(508, 34);
            this.ux_LED_Input13.Name = "ux_LED_Input13";
            this.ux_LED_Input13.Size = new System.Drawing.Size(36, 68);
            this.ux_LED_Input13.TabIndex = 35;
            // 
            // ux_LED_Output11
            // 
            this.ux_LED_Output11.IsEnabled = false;
            this.ux_LED_Output11.LEDLabel = "11";
            this.ux_LED_Output11.Location = new System.Drawing.Point(424, 138);
            this.ux_LED_Output11.Name = "ux_LED_Output11";
            this.ux_LED_Output11.Size = new System.Drawing.Size(35, 73);
            this.ux_LED_Output11.TabIndex = 10;
            this.ux_LED_Output11.Click += new System.EventHandler(this.Output11LedClick);
            // 
            // ux_LED_Input12
            // 
            this.ux_LED_Input12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_LED_Input12.IsEnabled = false;
            this.ux_LED_Input12.LEDLabel = "12";
            this.ux_LED_Input12.Location = new System.Drawing.Point(466, 34);
            this.ux_LED_Input12.Name = "ux_LED_Input12";
            this.ux_LED_Input12.Size = new System.Drawing.Size(36, 68);
            this.ux_LED_Input12.TabIndex = 34;
            // 
            // ux_LED_Output10
            // 
            this.ux_LED_Output10.IsEnabled = false;
            this.ux_LED_Output10.LEDLabel = "10";
            this.ux_LED_Output10.Location = new System.Drawing.Point(382, 138);
            this.ux_LED_Output10.Name = "ux_LED_Output10";
            this.ux_LED_Output10.Size = new System.Drawing.Size(35, 73);
            this.ux_LED_Output10.TabIndex = 9;
            this.ux_LED_Output10.Click += new System.EventHandler(this.Output10LedClick);
            // 
            // ux_LED_Input11
            // 
            this.ux_LED_Input11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_LED_Input11.IsEnabled = false;
            this.ux_LED_Input11.LEDLabel = "11";
            this.ux_LED_Input11.Location = new System.Drawing.Point(424, 34);
            this.ux_LED_Input11.Name = "ux_LED_Input11";
            this.ux_LED_Input11.Size = new System.Drawing.Size(36, 68);
            this.ux_LED_Input11.TabIndex = 33;
            // 
            // ux_LED_Output9
            // 
            this.ux_LED_Output9.IsEnabled = false;
            this.ux_LED_Output9.LEDLabel = "9";
            this.ux_LED_Output9.Location = new System.Drawing.Point(340, 138);
            this.ux_LED_Output9.Name = "ux_LED_Output9";
            this.ux_LED_Output9.Size = new System.Drawing.Size(35, 73);
            this.ux_LED_Output9.TabIndex = 8;
            this.ux_LED_Output9.Click += new System.EventHandler(this.Output9LedClick);
            // 
            // ux_LED_Input10
            // 
            this.ux_LED_Input10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_LED_Input10.IsEnabled = false;
            this.ux_LED_Input10.LEDLabel = "10";
            this.ux_LED_Input10.Location = new System.Drawing.Point(382, 34);
            this.ux_LED_Input10.Name = "ux_LED_Input10";
            this.ux_LED_Input10.Size = new System.Drawing.Size(36, 68);
            this.ux_LED_Input10.TabIndex = 32;
            // 
            // ux_LED_Output8
            // 
            this.ux_LED_Output8.IsEnabled = false;
            this.ux_LED_Output8.LEDLabel = "8";
            this.ux_LED_Output8.Location = new System.Drawing.Point(298, 138);
            this.ux_LED_Output8.Name = "ux_LED_Output8";
            this.ux_LED_Output8.Size = new System.Drawing.Size(35, 73);
            this.ux_LED_Output8.TabIndex = 7;
            this.ux_LED_Output8.Click += new System.EventHandler(this.Output8LedClick);
            // 
            // ux_LED_Input9
            // 
            this.ux_LED_Input9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_LED_Input9.IsEnabled = false;
            this.ux_LED_Input9.LEDLabel = "9";
            this.ux_LED_Input9.Location = new System.Drawing.Point(340, 34);
            this.ux_LED_Input9.Name = "ux_LED_Input9";
            this.ux_LED_Input9.Size = new System.Drawing.Size(36, 68);
            this.ux_LED_Input9.TabIndex = 31;
            // 
            // ux_LED_Output7
            // 
            this.ux_LED_Output7.IsEnabled = false;
            this.ux_LED_Output7.LEDLabel = "7";
            this.ux_LED_Output7.Location = new System.Drawing.Point(256, 138);
            this.ux_LED_Output7.Name = "ux_LED_Output7";
            this.ux_LED_Output7.Size = new System.Drawing.Size(35, 73);
            this.ux_LED_Output7.TabIndex = 6;
            this.ux_LED_Output7.Click += new System.EventHandler(this.Output7LedClick);
            // 
            // ux_LED_Input8
            // 
            this.ux_LED_Input8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_LED_Input8.IsEnabled = false;
            this.ux_LED_Input8.LEDLabel = "8";
            this.ux_LED_Input8.Location = new System.Drawing.Point(298, 34);
            this.ux_LED_Input8.Name = "ux_LED_Input8";
            this.ux_LED_Input8.Size = new System.Drawing.Size(36, 68);
            this.ux_LED_Input8.TabIndex = 30;
            // 
            // ux_LED_Output6
            // 
            this.ux_LED_Output6.IsEnabled = false;
            this.ux_LED_Output6.LEDLabel = "6";
            this.ux_LED_Output6.Location = new System.Drawing.Point(214, 138);
            this.ux_LED_Output6.Name = "ux_LED_Output6";
            this.ux_LED_Output6.Size = new System.Drawing.Size(35, 73);
            this.ux_LED_Output6.TabIndex = 5;
            this.ux_LED_Output6.Click += new System.EventHandler(this.Output6LedClick);
            // 
            // ux_LED_Input7
            // 
            this.ux_LED_Input7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_LED_Input7.IsEnabled = false;
            this.ux_LED_Input7.LEDLabel = "7";
            this.ux_LED_Input7.Location = new System.Drawing.Point(256, 34);
            this.ux_LED_Input7.Name = "ux_LED_Input7";
            this.ux_LED_Input7.Size = new System.Drawing.Size(36, 68);
            this.ux_LED_Input7.TabIndex = 29;
            // 
            // ux_LED_Output5
            // 
            this.ux_LED_Output5.IsEnabled = false;
            this.ux_LED_Output5.LEDLabel = "5";
            this.ux_LED_Output5.Location = new System.Drawing.Point(172, 138);
            this.ux_LED_Output5.Name = "ux_LED_Output5";
            this.ux_LED_Output5.Size = new System.Drawing.Size(35, 73);
            this.ux_LED_Output5.TabIndex = 4;
            this.ux_LED_Output5.Click += new System.EventHandler(this.Output5LedClick);
            // 
            // ux_LED_Input6
            // 
            this.ux_LED_Input6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_LED_Input6.IsEnabled = false;
            this.ux_LED_Input6.LEDLabel = "6";
            this.ux_LED_Input6.Location = new System.Drawing.Point(214, 34);
            this.ux_LED_Input6.Name = "ux_LED_Input6";
            this.ux_LED_Input6.Size = new System.Drawing.Size(36, 68);
            this.ux_LED_Input6.TabIndex = 28;
            // 
            // ux_LED_Output4
            // 
            this.ux_LED_Output4.IsEnabled = false;
            this.ux_LED_Output4.LEDLabel = "4";
            this.ux_LED_Output4.Location = new System.Drawing.Point(130, 138);
            this.ux_LED_Output4.Name = "ux_LED_Output4";
            this.ux_LED_Output4.Size = new System.Drawing.Size(35, 73);
            this.ux_LED_Output4.TabIndex = 3;
            this.ux_LED_Output4.Click += new System.EventHandler(this.Output4LedClick);
            // 
            // ux_LED_Input5
            // 
            this.ux_LED_Input5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_LED_Input5.IsEnabled = false;
            this.ux_LED_Input5.LEDLabel = "5";
            this.ux_LED_Input5.Location = new System.Drawing.Point(172, 34);
            this.ux_LED_Input5.Name = "ux_LED_Input5";
            this.ux_LED_Input5.Size = new System.Drawing.Size(36, 68);
            this.ux_LED_Input5.TabIndex = 27;
            // 
            // ux_LED_Output3
            // 
            this.ux_LED_Output3.IsEnabled = false;
            this.ux_LED_Output3.LEDLabel = "3";
            this.ux_LED_Output3.Location = new System.Drawing.Point(88, 138);
            this.ux_LED_Output3.Name = "ux_LED_Output3";
            this.ux_LED_Output3.Size = new System.Drawing.Size(35, 73);
            this.ux_LED_Output3.TabIndex = 2;
            this.ux_LED_Output3.Click += new System.EventHandler(this.Output3LedClick);
            // 
            // ux_LED_Input4
            // 
            this.ux_LED_Input4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_LED_Input4.IsEnabled = false;
            this.ux_LED_Input4.LEDLabel = "4";
            this.ux_LED_Input4.Location = new System.Drawing.Point(130, 34);
            this.ux_LED_Input4.Name = "ux_LED_Input4";
            this.ux_LED_Input4.Size = new System.Drawing.Size(36, 68);
            this.ux_LED_Input4.TabIndex = 26;
            // 
            // ux_LED_Output2
            // 
            this.ux_LED_Output2.IsEnabled = false;
            this.ux_LED_Output2.LEDLabel = "2";
            this.ux_LED_Output2.Location = new System.Drawing.Point(46, 138);
            this.ux_LED_Output2.Name = "ux_LED_Output2";
            this.ux_LED_Output2.Size = new System.Drawing.Size(35, 73);
            this.ux_LED_Output2.TabIndex = 1;
            this.ux_LED_Output2.Click += new System.EventHandler(this.Output2LedClick);
            // 
            // ux_LED_Input3
            // 
            this.ux_LED_Input3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_LED_Input3.IsEnabled = false;
            this.ux_LED_Input3.LEDLabel = "3";
            this.ux_LED_Input3.Location = new System.Drawing.Point(88, 34);
            this.ux_LED_Input3.Name = "ux_LED_Input3";
            this.ux_LED_Input3.Size = new System.Drawing.Size(36, 68);
            this.ux_LED_Input3.TabIndex = 25;
            // 
            // ux_LED_Output1
            // 
            this.ux_LED_Output1.IsEnabled = false;
            this.ux_LED_Output1.LEDLabel = "1";
            this.ux_LED_Output1.Location = new System.Drawing.Point(3, 138);
            this.ux_LED_Output1.Name = "ux_LED_Output1";
            this.ux_LED_Output1.Size = new System.Drawing.Size(35, 73);
            this.ux_LED_Output1.TabIndex = 0;
            this.ux_LED_Output1.Click += new System.EventHandler(this.Output1LedClick);
            // 
            // ux_LED_Input2
            // 
            this.ux_LED_Input2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_LED_Input2.IsEnabled = false;
            this.ux_LED_Input2.LEDLabel = "2";
            this.ux_LED_Input2.Location = new System.Drawing.Point(46, 34);
            this.ux_LED_Input2.Name = "ux_LED_Input2";
            this.ux_LED_Input2.Size = new System.Drawing.Size(36, 68);
            this.ux_LED_Input2.TabIndex = 24;
            // 
            // ux_LED_Input1
            // 
            this.ux_LED_Input1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_LED_Input1.IsEnabled = false;
            this.ux_LED_Input1.LEDLabel = "1";
            this.ux_LED_Input1.Location = new System.Drawing.Point(3, 34);
            this.ux_LED_Input1.Name = "ux_LED_Input1";
            this.ux_LED_Input1.Size = new System.Drawing.Size(37, 68);
            this.ux_LED_Input1.TabIndex = 8;
            // 
            // ux_IO_InputsLabel
            // 
            this.ux_IO_InputsLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_IO_InputsLabel.AutoSize = true;
            this.ux_Layout_IO.SetColumnSpan(this.ux_IO_InputsLabel, 16);
            this.ux_IO_InputsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_IO_InputsLabel.ForeColor = System.Drawing.Color.Black;
            this.ux_IO_InputsLabel.Location = new System.Drawing.Point(306, 3);
            this.ux_IO_InputsLabel.Name = "ux_IO_InputsLabel";
            this.ux_IO_InputsLabel.Size = new System.Drawing.Size(66, 24);
            this.ux_IO_InputsLabel.TabIndex = 0;
            this.ux_IO_InputsLabel.Text = "Inputs";
            // 
            // ux_Tab_General
            // 
            this.ux_Tab_General.Controls.Add(this.ux_Layout_GeneralMain);
            this.ux_Tab_General.Location = new System.Drawing.Point(4, 4);
            this.ux_Tab_General.Name = "ux_Tab_General";
            this.ux_Tab_General.Padding = new System.Windows.Forms.Padding(3);
            this.ux_Tab_General.Size = new System.Drawing.Size(685, 491);
            this.ux_Tab_General.TabIndex = 5;
            this.ux_Tab_General.Text = "gnrl";
            this.ux_Tab_General.UseVisualStyleBackColor = true;
            this.ux_Tab_General.Enter += new System.EventHandler(this.TabEnter);
            // 
            // ux_Layout_GeneralMain
            // 
            this.ux_Layout_GeneralMain.ColumnCount = 3;
            this.ux_Layout_GeneralMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.26549F));
            this.ux_Layout_GeneralMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.65781F));
            this.ux_Layout_GeneralMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.07669F));
            this.ux_Layout_GeneralMain.Controls.Add(this.ux_Control_InstallFolder, 0, 0);
            this.ux_Layout_GeneralMain.Controls.Add(this.ux_Set_NewPassLabel, 0, 8);
            this.ux_Layout_GeneralMain.Controls.Add(this.ux_Set_NewPassword, 1, 8);
            this.ux_Layout_GeneralMain.Controls.Add(this.ux_ControlButtonSave_General, 2, 10);
            this.ux_Layout_GeneralMain.Controls.Add(this.ux_Check_PervasiveLimitsEnabled, 0, 4);
            this.ux_Layout_GeneralMain.Controls.Add(this.ux_Check_InstructionsEnabled, 0, 5);
            this.ux_Layout_GeneralMain.Controls.Add(this.ux_Check_LoadLastJobEnabled, 0, 6);
            this.ux_Layout_GeneralMain.Controls.Add(this.ux_Check_AccessControlEnabled, 0, 7);
            this.ux_Layout_GeneralMain.Controls.Add(this.ux_Control_ProjectsPath, 0, 1);
            this.ux_Layout_GeneralMain.Controls.Add(this.ux_Control_ImagesPath, 0, 2);
            this.ux_Layout_GeneralMain.Controls.Add(this.ux_Control_RTFPath, 0, 3);
            this.ux_Layout_GeneralMain.Controls.Add(this.ux_Button_Back_Generals, 1, 10);
            this.ux_Layout_GeneralMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_GeneralMain.Location = new System.Drawing.Point(3, 3);
            this.ux_Layout_GeneralMain.Name = "ux_Layout_GeneralMain";
            this.ux_Layout_GeneralMain.RowCount = 11;
            this.ux_Layout_GeneralMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.843137F));
            this.ux_Layout_GeneralMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.843137F));
            this.ux_Layout_GeneralMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.843137F));
            this.ux_Layout_GeneralMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.843137F));
            this.ux_Layout_GeneralMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.843137F));
            this.ux_Layout_GeneralMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.843137F));
            this.ux_Layout_GeneralMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.843137F));
            this.ux_Layout_GeneralMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.843137F));
            this.ux_Layout_GeneralMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.843137F));
            this.ux_Layout_GeneralMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.843137F));
            this.ux_Layout_GeneralMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.56863F));
            this.ux_Layout_GeneralMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.ux_Layout_GeneralMain.Size = new System.Drawing.Size(679, 485);
            this.ux_Layout_GeneralMain.TabIndex = 0;
            // 
            // ux_Control_InstallFolder
            // 
            this.ux_Layout_GeneralMain.SetColumnSpan(this.ux_Control_InstallFolder, 3);
            this.ux_Control_InstallFolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Control_InstallFolder.Location = new System.Drawing.Point(3, 3);
            this.ux_Control_InstallFolder.MinimumSize = new System.Drawing.Size(465, 36);
            this.ux_Control_InstallFolder.Name = "ux_Control_InstallFolder";
            this.ux_Control_InstallFolder.PathLabel = "Install Folder";
            this.ux_Control_InstallFolder.SelectedPath = "";
            this.ux_Control_InstallFolder.Size = new System.Drawing.Size(673, 36);
            this.ux_Control_InstallFolder.TabIndex = 46;
            // 
            // ux_Set_NewPassLabel
            // 
            this.ux_Set_NewPassLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ux_Set_NewPassLabel.AutoSize = true;
            this.ux_Set_NewPassLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Set_NewPassLabel.Location = new System.Drawing.Point(3, 311);
            this.ux_Set_NewPassLabel.Name = "ux_Set_NewPassLabel";
            this.ux_Set_NewPassLabel.Size = new System.Drawing.Size(136, 24);
            this.ux_Set_NewPassLabel.TabIndex = 18;
            this.ux_Set_NewPassLabel.Text = "New Password";
            // 
            // ux_Set_NewPassword
            // 
            this.ux_Set_NewPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Set_NewPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.ux_Set_NewPassword.Location = new System.Drawing.Point(276, 308);
            this.ux_Set_NewPassword.Name = "ux_Set_NewPassword";
            this.ux_Set_NewPassword.PasswordChar = '*';
            this.ux_Set_NewPassword.Size = new System.Drawing.Size(290, 29);
            this.ux_Set_NewPassword.TabIndex = 19;
            this.ux_Set_NewPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextNewPasswordKeyDown);
            this.ux_Set_NewPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextNewPasswordKeyPress);
            // 
            // ux_ControlButtonSave_General
            // 
            this.ux_ControlButtonSave_General.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_ControlButtonSave_General.Location = new System.Drawing.Point(572, 383);
            this.ux_ControlButtonSave_General.MinimumSize = new System.Drawing.Size(75, 100);
            this.ux_ControlButtonSave_General.Name = "ux_ControlButtonSave_General";
            this.ux_ControlButtonSave_General.Size = new System.Drawing.Size(104, 100);
            this.ux_ControlButtonSave_General.TabIndex = 41;
            this.ux_ControlButtonSave_General.Click += new System.EventHandler(this.SaveGeneralSettingsClick);
            // 
            // ux_Check_PervasiveLimitsEnabled
            // 
            this.ux_Check_PervasiveLimitsEnabled.Checked = false;
            this.ux_Layout_GeneralMain.SetColumnSpan(this.ux_Check_PervasiveLimitsEnabled, 3);
            this.ux_Check_PervasiveLimitsEnabled.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Check_PervasiveLimitsEnabled.Location = new System.Drawing.Point(3, 155);
            this.ux_Check_PervasiveLimitsEnabled.MinimumSize = new System.Drawing.Size(372, 36);
            this.ux_Check_PervasiveLimitsEnabled.Name = "ux_Check_PervasiveLimitsEnabled";
            this.ux_Check_PervasiveLimitsEnabled.SettingLabel = "Pervasive Limits";
            this.ux_Check_PervasiveLimitsEnabled.Size = new System.Drawing.Size(673, 36);
            this.ux_Check_PervasiveLimitsEnabled.TabIndex = 42;
            // 
            // ux_Check_InstructionsEnabled
            // 
            this.ux_Check_InstructionsEnabled.Checked = false;
            this.ux_Layout_GeneralMain.SetColumnSpan(this.ux_Check_InstructionsEnabled, 3);
            this.ux_Check_InstructionsEnabled.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Check_InstructionsEnabled.Location = new System.Drawing.Point(3, 193);
            this.ux_Check_InstructionsEnabled.MinimumSize = new System.Drawing.Size(372, 36);
            this.ux_Check_InstructionsEnabled.Name = "ux_Check_InstructionsEnabled";
            this.ux_Check_InstructionsEnabled.SettingLabel = "Instructions";
            this.ux_Check_InstructionsEnabled.Size = new System.Drawing.Size(673, 36);
            this.ux_Check_InstructionsEnabled.TabIndex = 43;
            // 
            // ux_Check_LoadLastJobEnabled
            // 
            this.ux_Check_LoadLastJobEnabled.Checked = false;
            this.ux_Layout_GeneralMain.SetColumnSpan(this.ux_Check_LoadLastJobEnabled, 3);
            this.ux_Check_LoadLastJobEnabled.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Check_LoadLastJobEnabled.Location = new System.Drawing.Point(3, 231);
            this.ux_Check_LoadLastJobEnabled.MinimumSize = new System.Drawing.Size(372, 36);
            this.ux_Check_LoadLastJobEnabled.Name = "ux_Check_LoadLastJobEnabled";
            this.ux_Check_LoadLastJobEnabled.SettingLabel = "Load Last Job";
            this.ux_Check_LoadLastJobEnabled.Size = new System.Drawing.Size(673, 36);
            this.ux_Check_LoadLastJobEnabled.TabIndex = 44;
            // 
            // ux_Check_AccessControlEnabled
            // 
            this.ux_Check_AccessControlEnabled.Checked = false;
            this.ux_Layout_GeneralMain.SetColumnSpan(this.ux_Check_AccessControlEnabled, 3);
            this.ux_Check_AccessControlEnabled.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Check_AccessControlEnabled.Location = new System.Drawing.Point(3, 269);
            this.ux_Check_AccessControlEnabled.MinimumSize = new System.Drawing.Size(372, 36);
            this.ux_Check_AccessControlEnabled.Name = "ux_Check_AccessControlEnabled";
            this.ux_Check_AccessControlEnabled.SettingLabel = "Access Control";
            this.ux_Check_AccessControlEnabled.Size = new System.Drawing.Size(673, 36);
            this.ux_Check_AccessControlEnabled.TabIndex = 45;
            // 
            // ux_Control_ProjectsPath
            // 
            this.ux_Layout_GeneralMain.SetColumnSpan(this.ux_Control_ProjectsPath, 3);
            this.ux_Control_ProjectsPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Control_ProjectsPath.Location = new System.Drawing.Point(3, 41);
            this.ux_Control_ProjectsPath.MinimumSize = new System.Drawing.Size(465, 36);
            this.ux_Control_ProjectsPath.Name = "ux_Control_ProjectsPath";
            this.ux_Control_ProjectsPath.PathLabel = "Projects Folder";
            this.ux_Control_ProjectsPath.SelectedPath = "";
            this.ux_Control_ProjectsPath.Size = new System.Drawing.Size(673, 36);
            this.ux_Control_ProjectsPath.TabIndex = 47;
            // 
            // ux_Control_ImagesPath
            // 
            this.ux_Layout_GeneralMain.SetColumnSpan(this.ux_Control_ImagesPath, 3);
            this.ux_Control_ImagesPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Control_ImagesPath.Location = new System.Drawing.Point(3, 79);
            this.ux_Control_ImagesPath.MinimumSize = new System.Drawing.Size(465, 36);
            this.ux_Control_ImagesPath.Name = "ux_Control_ImagesPath";
            this.ux_Control_ImagesPath.PathLabel = "Images Folder";
            this.ux_Control_ImagesPath.SelectedPath = "";
            this.ux_Control_ImagesPath.Size = new System.Drawing.Size(673, 36);
            this.ux_Control_ImagesPath.TabIndex = 48;
            // 
            // ux_Control_RTFPath
            // 
            this.ux_Layout_GeneralMain.SetColumnSpan(this.ux_Control_RTFPath, 3);
            this.ux_Control_RTFPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Control_RTFPath.Location = new System.Drawing.Point(3, 117);
            this.ux_Control_RTFPath.MinimumSize = new System.Drawing.Size(465, 36);
            this.ux_Control_RTFPath.Name = "ux_Control_RTFPath";
            this.ux_Control_RTFPath.PathLabel = "RTF Folder";
            this.ux_Control_RTFPath.SelectedPath = "";
            this.ux_Control_RTFPath.Size = new System.Drawing.Size(673, 36);
            this.ux_Control_RTFPath.TabIndex = 49;
            // 
            // ux_Button_Back_Generals
            // 
            this.ux_Button_Back_Generals.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Button_Back_Generals.Location = new System.Drawing.Point(481, 383);
            this.ux_Button_Back_Generals.MinimumSize = new System.Drawing.Size(85, 100);
            this.ux_Button_Back_Generals.Name = "ux_Button_Back_Generals";
            this.ux_Button_Back_Generals.Size = new System.Drawing.Size(85, 100);
            this.ux_Button_Back_Generals.TabIndex = 50;
            this.ux_Button_Back_Generals.Click += new System.EventHandler(this.BackGeneralSettingsClick);
            // 
            // ux_Tab_Settings
            // 
            this.ux_Tab_Settings.Controls.Add(this.ux_flp_Settings);
            this.ux_Tab_Settings.Location = new System.Drawing.Point(4, 4);
            this.ux_Tab_Settings.Name = "ux_Tab_Settings";
            this.ux_Tab_Settings.Padding = new System.Windows.Forms.Padding(3);
            this.ux_Tab_Settings.Size = new System.Drawing.Size(685, 491);
            this.ux_Tab_Settings.TabIndex = 6;
            this.ux_Tab_Settings.Text = "set";
            this.ux_Tab_Settings.UseVisualStyleBackColor = true;
            this.ux_Tab_Settings.Enter += new System.EventHandler(this.TabEnter);
            // 
            // ux_flp_Settings
            // 
            this.ux_flp_Settings.Controls.Add(this.ux_Control_ButtonGeneral);
            this.ux_flp_Settings.Controls.Add(this.Control_Button_Theme);
            this.ux_flp_Settings.Controls.Add(this.ux_Control_SettingImport);
            this.ux_flp_Settings.Controls.Add(this.ux_Control_ButtonAxisSetting);
            this.ux_flp_Settings.Controls.Add(this.ux_Control_PLCSetting);
            this.ux_flp_Settings.Controls.Add(this.controlButtonAllSettings1);
            this.ux_flp_Settings.Controls.Add(this.ux_Nav_Stop);
            this.ux_flp_Settings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_flp_Settings.Location = new System.Drawing.Point(3, 3);
            this.ux_flp_Settings.Name = "ux_flp_Settings";
            this.ux_flp_Settings.Size = new System.Drawing.Size(679, 485);
            this.ux_flp_Settings.TabIndex = 13;
            // 
            // ux_Control_ButtonGeneral
            // 
            this.ux_Control_ButtonGeneral.Location = new System.Drawing.Point(3, 3);
            this.ux_Control_ButtonGeneral.MinimumSize = new System.Drawing.Size(85, 100);
            this.ux_Control_ButtonGeneral.Name = "ux_Control_ButtonGeneral";
            this.ux_Control_ButtonGeneral.Size = new System.Drawing.Size(125, 125);
            this.ux_Control_ButtonGeneral.TabIndex = 18;
            this.ux_Control_ButtonGeneral.Click += new System.EventHandler(this.SettingGeneralClick);
            // 
            // Control_Button_Theme
            // 
            this.Control_Button_Theme.Location = new System.Drawing.Point(134, 3);
            this.Control_Button_Theme.MinimumSize = new System.Drawing.Size(85, 100);
            this.Control_Button_Theme.Name = "Control_Button_Theme";
            this.Control_Button_Theme.Size = new System.Drawing.Size(125, 125);
            this.Control_Button_Theme.TabIndex = 17;
            this.Control_Button_Theme.Click += new System.EventHandler(this.SettingThemeClick);
            // 
            // ux_Control_SettingImport
            // 
            this.ux_Control_SettingImport.Location = new System.Drawing.Point(265, 3);
            this.ux_Control_SettingImport.MinimumSize = new System.Drawing.Size(85, 100);
            this.ux_Control_SettingImport.Name = "ux_Control_SettingImport";
            this.ux_Control_SettingImport.Size = new System.Drawing.Size(125, 125);
            this.ux_Control_SettingImport.TabIndex = 19;
            this.ux_Control_SettingImport.Click += new System.EventHandler(this.SettingFileImportClick);
            // 
            // ux_Control_ButtonAxisSetting
            // 
            this.ux_Control_ButtonAxisSetting.Location = new System.Drawing.Point(396, 3);
            this.ux_Control_ButtonAxisSetting.MinimumSize = new System.Drawing.Size(85, 100);
            this.ux_Control_ButtonAxisSetting.Name = "ux_Control_ButtonAxisSetting";
            this.ux_Control_ButtonAxisSetting.Size = new System.Drawing.Size(125, 125);
            this.ux_Control_ButtonAxisSetting.TabIndex = 20;
            this.ux_Control_ButtonAxisSetting.Visible = false;
            this.ux_Control_ButtonAxisSetting.Click += new System.EventHandler(this.SettingAxisClick);
            // 
            // ux_Control_PLCSetting
            // 
            this.ux_Control_PLCSetting.Location = new System.Drawing.Point(527, 3);
            this.ux_Control_PLCSetting.MinimumSize = new System.Drawing.Size(85, 100);
            this.ux_Control_PLCSetting.Name = "ux_Control_PLCSetting";
            this.ux_Control_PLCSetting.Size = new System.Drawing.Size(125, 125);
            this.ux_Control_PLCSetting.TabIndex = 21;
            this.ux_Control_PLCSetting.Visible = false;
            this.ux_Control_PLCSetting.Click += new System.EventHandler(this.SettingPLCClick);
            // 
            // controlButtonAllSettings1
            // 
            this.controlButtonAllSettings1.Location = new System.Drawing.Point(3, 134);
            this.controlButtonAllSettings1.MinimumSize = new System.Drawing.Size(125, 125);
            this.controlButtonAllSettings1.Name = "controlButtonAllSettings1";
            this.controlButtonAllSettings1.Size = new System.Drawing.Size(125, 125);
            this.controlButtonAllSettings1.TabIndex = 22;
            this.controlButtonAllSettings1.Click += new System.EventHandler(this.ControlButtonAllSettings1_Click);
            // 
            // ux_Nav_Stop
            // 
            this.ux_Nav_Stop.ColumnCount = 1;
            this.ux_Nav_Stop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ux_Nav_Stop.Controls.Add(this.ux_Nav_StopLabel, 0, 1);
            this.ux_Nav_Stop.Controls.Add(this.ux_Nav_StopPic, 0, 0);
            this.ux_Nav_Stop.Location = new System.Drawing.Point(134, 134);
            this.ux_Nav_Stop.Name = "ux_Nav_Stop";
            this.ux_Nav_Stop.RowCount = 2;
            this.ux_Nav_Stop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.ux_Nav_Stop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.ux_Nav_Stop.Size = new System.Drawing.Size(87, 90);
            this.ux_Nav_Stop.TabIndex = 14;
            this.ux_Nav_Stop.Visible = false;
            // 
            // ux_Nav_StopLabel
            // 
            this.ux_Nav_StopLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Nav_StopLabel.AutoSize = true;
            this.ux_Nav_StopLabel.BackColor = System.Drawing.Color.Transparent;
            this.ux_Nav_StopLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Nav_StopLabel.Location = new System.Drawing.Point(22, 66);
            this.ux_Nav_StopLabel.Name = "ux_Nav_StopLabel";
            this.ux_Nav_StopLabel.Size = new System.Drawing.Size(43, 20);
            this.ux_Nav_StopLabel.TabIndex = 7;
            this.ux_Nav_StopLabel.Text = "Stop";
            // 
            // ux_Nav_StopPic
            // 
            this.ux_Nav_StopPic.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Nav_StopPic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Nav_StopPic.Location = new System.Drawing.Point(3, 3);
            this.ux_Nav_StopPic.Name = "ux_Nav_StopPic";
            this.ux_Nav_StopPic.Size = new System.Drawing.Size(81, 57);
            this.ux_Nav_StopPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Nav_StopPic.TabIndex = 6;
            this.ux_Nav_StopPic.TabStop = false;
            // 
            // ux_Tab_MarkInProgress
            // 
            this.ux_Tab_MarkInProgress.Controls.Add(this.ux_Layout_MarkInProgressMain);
            this.ux_Tab_MarkInProgress.Location = new System.Drawing.Point(4, 4);
            this.ux_Tab_MarkInProgress.Name = "ux_Tab_MarkInProgress";
            this.ux_Tab_MarkInProgress.Padding = new System.Windows.Forms.Padding(3);
            this.ux_Tab_MarkInProgress.Size = new System.Drawing.Size(685, 491);
            this.ux_Tab_MarkInProgress.TabIndex = 7;
            this.ux_Tab_MarkInProgress.Text = "mip";
            this.ux_Tab_MarkInProgress.UseVisualStyleBackColor = true;
            // 
            // ux_Layout_MarkInProgressMain
            // 
            this.ux_Layout_MarkInProgressMain.BackColor = System.Drawing.Color.Red;
            this.ux_Layout_MarkInProgressMain.ColumnCount = 1;
            this.ux_Layout_MarkInProgressMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_Layout_MarkInProgressMain.Controls.Add(this.ux_Label_ElapsedTime, 0, 3);
            this.ux_Layout_MarkInProgressMain.Controls.Add(this.ux_MarkInProgressLabel, 0, 1);
            this.ux_Layout_MarkInProgressMain.Controls.Add(this.ux_Label_MarkingObject, 0, 2);
            this.ux_Layout_MarkInProgressMain.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Layout_MarkInProgressMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_MarkInProgressMain.Location = new System.Drawing.Point(3, 3);
            this.ux_Layout_MarkInProgressMain.Name = "ux_Layout_MarkInProgressMain";
            this.ux_Layout_MarkInProgressMain.RowCount = 4;
            this.ux_Layout_MarkInProgressMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_Layout_MarkInProgressMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_Layout_MarkInProgressMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_Layout_MarkInProgressMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_Layout_MarkInProgressMain.Size = new System.Drawing.Size(679, 485);
            this.ux_Layout_MarkInProgressMain.TabIndex = 0;
            this.ux_Layout_MarkInProgressMain.Click += new System.EventHandler(this.PicStopClick);
            // 
            // ux_Label_ElapsedTime
            // 
            this.ux_Label_ElapsedTime.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Label_ElapsedTime.AutoSize = true;
            this.ux_Label_ElapsedTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Label_ElapsedTime.ForeColor = System.Drawing.Color.White;
            this.ux_Label_ElapsedTime.Location = new System.Drawing.Point(329, 394);
            this.ux_Label_ElapsedTime.Name = "ux_Label_ElapsedTime";
            this.ux_Label_ElapsedTime.Size = new System.Drawing.Size(20, 24);
            this.ux_Label_ElapsedTime.TabIndex = 1;
            this.ux_Label_ElapsedTime.Text = "0";
            // 
            // ux_MarkInProgressLabel
            // 
            this.ux_MarkInProgressLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_MarkInProgressLabel.AutoSize = true;
            this.ux_MarkInProgressLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_MarkInProgressLabel.ForeColor = System.Drawing.Color.White;
            this.ux_MarkInProgressLabel.Location = new System.Drawing.Point(82, 199);
            this.ux_MarkInProgressLabel.Name = "ux_MarkInProgressLabel";
            this.ux_MarkInProgressLabel.Size = new System.Drawing.Size(515, 73);
            this.ux_MarkInProgressLabel.TabIndex = 0;
            this.ux_MarkInProgressLabel.Text = "Mark in Progress";
            this.ux_MarkInProgressLabel.Click += new System.EventHandler(this.PicStopClick);
            // 
            // ux_Label_MarkingObject
            // 
            this.ux_Label_MarkingObject.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Label_MarkingObject.AutoSize = true;
            this.ux_Label_MarkingObject.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Label_MarkingObject.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ux_Label_MarkingObject.Location = new System.Drawing.Point(339, 314);
            this.ux_Label_MarkingObject.Name = "ux_Label_MarkingObject";
            this.ux_Label_MarkingObject.Size = new System.Drawing.Size(0, 13);
            this.ux_Label_MarkingObject.TabIndex = 2;
            // 
            // ux_Tab_Focus
            // 
            this.ux_Tab_Focus.Controls.Add(this.ux_Layout_FocusMain);
            this.ux_Tab_Focus.Location = new System.Drawing.Point(4, 4);
            this.ux_Tab_Focus.Name = "ux_Tab_Focus";
            this.ux_Tab_Focus.Padding = new System.Windows.Forms.Padding(3);
            this.ux_Tab_Focus.Size = new System.Drawing.Size(685, 491);
            this.ux_Tab_Focus.TabIndex = 9;
            this.ux_Tab_Focus.Text = "fcs";
            this.ux_Tab_Focus.UseVisualStyleBackColor = true;
            // 
            // ux_Layout_FocusMain
            // 
            this.ux_Layout_FocusMain.ColumnCount = 2;
            this.ux_Layout_FocusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.97041F));
            this.ux_Layout_FocusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55.02959F));
            this.ux_Layout_FocusMain.Controls.Add(this.ux_Nav_Focus, 1, 1);
            this.ux_Layout_FocusMain.Controls.Add(this.ux_Layout_FocusSize, 0, 0);
            this.ux_Layout_FocusMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_FocusMain.Location = new System.Drawing.Point(3, 3);
            this.ux_Layout_FocusMain.Name = "ux_Layout_FocusMain";
            this.ux_Layout_FocusMain.RowCount = 2;
            this.ux_Layout_FocusMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 47.47899F));
            this.ux_Layout_FocusMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 52.52101F));
            this.ux_Layout_FocusMain.Size = new System.Drawing.Size(679, 485);
            this.ux_Layout_FocusMain.TabIndex = 0;
            // 
            // ux_Nav_Focus
            // 
            this.ux_Nav_Focus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Nav_Focus.ColumnCount = 1;
            this.ux_Nav_Focus.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ux_Nav_Focus.Controls.Add(this.ux_Nav_FocusPic, 0, 0);
            this.ux_Nav_Focus.Controls.Add(this.ux_Nav_FocusLabel, 0, 1);
            this.ux_Nav_Focus.Location = new System.Drawing.Point(595, 367);
            this.ux_Nav_Focus.Name = "ux_Nav_Focus";
            this.ux_Nav_Focus.RowCount = 2;
            this.ux_Nav_Focus.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.ux_Nav_Focus.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.ux_Nav_Focus.Size = new System.Drawing.Size(81, 115);
            this.ux_Nav_Focus.TabIndex = 14;
            // 
            // ux_Nav_FocusPic
            // 
            this.ux_Nav_FocusPic.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Nav_FocusPic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Nav_FocusPic.Image = ((System.Drawing.Image)(resources.GetObject("ux_Nav_FocusPic.Image")));
            this.ux_Nav_FocusPic.Location = new System.Drawing.Point(3, 3);
            this.ux_Nav_FocusPic.Name = "ux_Nav_FocusPic";
            this.ux_Nav_FocusPic.Size = new System.Drawing.Size(75, 74);
            this.ux_Nav_FocusPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Nav_FocusPic.TabIndex = 8;
            this.ux_Nav_FocusPic.TabStop = false;
            this.ux_Nav_FocusPic.Click += new System.EventHandler(this.ImgFocusClick);
            // 
            // ux_Nav_FocusLabel
            // 
            this.ux_Nav_FocusLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Nav_FocusLabel.AutoSize = true;
            this.ux_Nav_FocusLabel.BackColor = System.Drawing.Color.Transparent;
            this.ux_Nav_FocusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Nav_FocusLabel.Location = new System.Drawing.Point(9, 85);
            this.ux_Nav_FocusLabel.Name = "ux_Nav_FocusLabel";
            this.ux_Nav_FocusLabel.Size = new System.Drawing.Size(63, 24);
            this.ux_Nav_FocusLabel.TabIndex = 7;
            this.ux_Nav_FocusLabel.Text = "Focus";
            // 
            // ux_Layout_FocusSize
            // 
            this.ux_Layout_FocusSize.ColumnCount = 2;
            this.ux_Layout_FocusSize.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.22222F));
            this.ux_Layout_FocusSize.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 77.77778F));
            this.ux_Layout_FocusSize.Controls.Add(this.ux_Focus_SizeLabel, 0, 0);
            this.ux_Layout_FocusSize.Controls.Add(this.ux_FocusSize, 1, 0);
            this.ux_Layout_FocusSize.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_FocusSize.Location = new System.Drawing.Point(3, 3);
            this.ux_Layout_FocusSize.Name = "ux_Layout_FocusSize";
            this.ux_Layout_FocusSize.RowCount = 2;
            this.ux_Layout_FocusSize.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.43836F));
            this.ux_Layout_FocusSize.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 83.56165F));
            this.ux_Layout_FocusSize.Size = new System.Drawing.Size(299, 224);
            this.ux_Layout_FocusSize.TabIndex = 16;
            // 
            // ux_Focus_SizeLabel
            // 
            this.ux_Focus_SizeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Focus_SizeLabel.AutoSize = true;
            this.ux_Focus_SizeLabel.Location = new System.Drawing.Point(3, 11);
            this.ux_Focus_SizeLabel.Name = "ux_Focus_SizeLabel";
            this.ux_Focus_SizeLabel.Size = new System.Drawing.Size(60, 13);
            this.ux_Focus_SizeLabel.TabIndex = 16;
            this.ux_Focus_SizeLabel.Text = "Size";
            // 
            // ux_FocusSize
            // 
            this.ux_FocusSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_FocusSize.Location = new System.Drawing.Point(66, 3);
            this.ux_FocusSize.Name = "ux_FocusSize";
            this.ux_FocusSize.Size = new System.Drawing.Size(233, 30);
            this.ux_FocusSize.TabIndex = 17;
            this.ux_FocusSize.Value = 50;
            // 
            // ux_Tab_DataEdit
            // 
            this.ux_Tab_DataEdit.Controls.Add(this.ux_DataGridView);
            this.ux_Tab_DataEdit.Location = new System.Drawing.Point(4, 4);
            this.ux_Tab_DataEdit.Name = "ux_Tab_DataEdit";
            this.ux_Tab_DataEdit.Padding = new System.Windows.Forms.Padding(3);
            this.ux_Tab_DataEdit.Size = new System.Drawing.Size(685, 491);
            this.ux_Tab_DataEdit.TabIndex = 10;
            this.ux_Tab_DataEdit.Text = "data";
            this.ux_Tab_DataEdit.UseVisualStyleBackColor = true;
            this.ux_Tab_DataEdit.Enter += new System.EventHandler(this.TabEnter);
            // 
            // ux_DataGridView
            // 
            this.ux_DataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ux_DataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ux_DataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.ux_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_DataGridView.Cursor = System.Windows.Forms.Cursors.IBeam;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ux_DataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.ux_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_DataGridView.Location = new System.Drawing.Point(3, 3);
            this.ux_DataGridView.Name = "ux_DataGridView";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ux_DataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.ux_DataGridView.Size = new System.Drawing.Size(679, 485);
            this.ux_DataGridView.TabIndex = 7;
            this.ux_DataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.SeqDataCellEndEdit);
            this.ux_DataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ObjectDataKeyDown);
            // 
            // ux_Tab_PLC
            // 
            this.ux_Tab_PLC.Controls.Add(this.ux_PLC_Layout_TagAdd);
            this.ux_Tab_PLC.Controls.Add(this.ux_PLC_Layout_TagWrite);
            this.ux_Tab_PLC.Controls.Add(this.ux_PLC_Layout_TagView);
            this.ux_Tab_PLC.Controls.Add(this.ux_PLC_Layout_Config);
            this.ux_Tab_PLC.Location = new System.Drawing.Point(4, 4);
            this.ux_Tab_PLC.Name = "ux_Tab_PLC";
            this.ux_Tab_PLC.Padding = new System.Windows.Forms.Padding(3);
            this.ux_Tab_PLC.Size = new System.Drawing.Size(685, 491);
            this.ux_Tab_PLC.TabIndex = 11;
            this.ux_Tab_PLC.Text = "plc";
            this.ux_Tab_PLC.UseVisualStyleBackColor = true;
            this.ux_Tab_PLC.Enter += new System.EventHandler(this.TabEnter);
            // 
            // ux_PLC_Layout_TagAdd
            // 
            this.ux_PLC_Layout_TagAdd.Controls.Add(this.ux_cb_plc_tag_add_type);
            this.ux_PLC_Layout_TagAdd.Controls.Add(this.label2);
            this.ux_PLC_Layout_TagAdd.Controls.Add(this.ux_PLC_AddTag);
            this.ux_PLC_Layout_TagAdd.Controls.Add(this.ux_PLC_AddTagName);
            this.ux_PLC_Layout_TagAdd.Controls.Add(this.ux_PLC_AddTagNameLabel);
            this.ux_PLC_Layout_TagAdd.Location = new System.Drawing.Point(256, 329);
            this.ux_PLC_Layout_TagAdd.Name = "ux_PLC_Layout_TagAdd";
            this.ux_PLC_Layout_TagAdd.Size = new System.Drawing.Size(222, 147);
            this.ux_PLC_Layout_TagAdd.TabIndex = 5;
            this.ux_PLC_Layout_TagAdd.TabStop = false;
            this.ux_PLC_Layout_TagAdd.Text = "Add Tag";
            // 
            // ux_cb_plc_tag_add_type
            // 
            this.ux_cb_plc_tag_add_type.FormattingEnabled = true;
            this.ux_cb_plc_tag_add_type.Items.AddRange(new object[] {
            "BOOL",
            "SHORT",
            "STRING",
            "INT"});
            this.ux_cb_plc_tag_add_type.Location = new System.Drawing.Point(65, 57);
            this.ux_cb_plc_tag_add_type.Name = "ux_cb_plc_tag_add_type";
            this.ux_cb_plc_tag_add_type.Size = new System.Drawing.Size(122, 21);
            this.ux_cb_plc_tag_add_type.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Type";
            // 
            // ux_PLC_AddTag
            // 
            this.ux_PLC_AddTag.Location = new System.Drawing.Point(112, 110);
            this.ux_PLC_AddTag.Name = "ux_PLC_AddTag";
            this.ux_PLC_AddTag.Size = new System.Drawing.Size(75, 23);
            this.ux_PLC_AddTag.TabIndex = 4;
            this.ux_PLC_AddTag.Text = "Add";
            this.ux_PLC_AddTag.UseVisualStyleBackColor = true;
            this.ux_PLC_AddTag.Click += new System.EventHandler(this.ButtonAddTagClick);
            // 
            // ux_PLC_AddTagName
            // 
            this.ux_PLC_AddTagName.Location = new System.Drawing.Point(65, 31);
            this.ux_PLC_AddTagName.Name = "ux_PLC_AddTagName";
            this.ux_PLC_AddTagName.Size = new System.Drawing.Size(122, 20);
            this.ux_PLC_AddTagName.TabIndex = 2;
            // 
            // ux_PLC_AddTagNameLabel
            // 
            this.ux_PLC_AddTagNameLabel.AutoSize = true;
            this.ux_PLC_AddTagNameLabel.Location = new System.Drawing.Point(17, 34);
            this.ux_PLC_AddTagNameLabel.Name = "ux_PLC_AddTagNameLabel";
            this.ux_PLC_AddTagNameLabel.Size = new System.Drawing.Size(35, 13);
            this.ux_PLC_AddTagNameLabel.TabIndex = 0;
            this.ux_PLC_AddTagNameLabel.Text = "Name";
            // 
            // ux_PLC_Layout_TagWrite
            // 
            this.ux_PLC_Layout_TagWrite.Controls.Add(this.ux_cb_Plc_Tag_Type);
            this.ux_PLC_Layout_TagWrite.Controls.Add(this.label1);
            this.ux_PLC_Layout_TagWrite.Controls.Add(this.ux_PLC_WriteTag);
            this.ux_PLC_Layout_TagWrite.Controls.Add(this.ux_PLC_WriteTagValue);
            this.ux_PLC_Layout_TagWrite.Controls.Add(this.ux_PLC_WriteTagName);
            this.ux_PLC_Layout_TagWrite.Controls.Add(this.ux_PLC_TagWriteValueLabel);
            this.ux_PLC_Layout_TagWrite.Controls.Add(this.ux_PLC_TagWriteNameLabel);
            this.ux_PLC_Layout_TagWrite.Location = new System.Drawing.Point(9, 329);
            this.ux_PLC_Layout_TagWrite.Name = "ux_PLC_Layout_TagWrite";
            this.ux_PLC_Layout_TagWrite.Size = new System.Drawing.Size(222, 147);
            this.ux_PLC_Layout_TagWrite.TabIndex = 3;
            this.ux_PLC_Layout_TagWrite.TabStop = false;
            this.ux_PLC_Layout_TagWrite.Text = "Tag Write";
            // 
            // ux_cb_Plc_Tag_Type
            // 
            this.ux_cb_Plc_Tag_Type.FormattingEnabled = true;
            this.ux_cb_Plc_Tag_Type.Items.AddRange(new object[] {
            "BOOL",
            "SHORT",
            "STRING",
            "INT"});
            this.ux_cb_Plc_Tag_Type.Location = new System.Drawing.Point(65, 77);
            this.ux_cb_Plc_Tag_Type.Name = "ux_cb_Plc_Tag_Type";
            this.ux_cb_Plc_Tag_Type.Size = new System.Drawing.Size(122, 21);
            this.ux_cb_Plc_Tag_Type.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Type";
            // 
            // ux_PLC_WriteTag
            // 
            this.ux_PLC_WriteTag.Location = new System.Drawing.Point(141, 110);
            this.ux_PLC_WriteTag.Name = "ux_PLC_WriteTag";
            this.ux_PLC_WriteTag.Size = new System.Drawing.Size(75, 23);
            this.ux_PLC_WriteTag.TabIndex = 4;
            this.ux_PLC_WriteTag.Text = "Write";
            this.ux_PLC_WriteTag.UseVisualStyleBackColor = true;
            this.ux_PLC_WriteTag.Click += new System.EventHandler(this.ButtonWriteTagClick);
            // 
            // ux_PLC_WriteTagValue
            // 
            this.ux_PLC_WriteTagValue.Location = new System.Drawing.Point(65, 49);
            this.ux_PLC_WriteTagValue.Name = "ux_PLC_WriteTagValue";
            this.ux_PLC_WriteTagValue.Size = new System.Drawing.Size(122, 20);
            this.ux_PLC_WriteTagValue.TabIndex = 3;
            // 
            // ux_PLC_WriteTagName
            // 
            this.ux_PLC_WriteTagName.Location = new System.Drawing.Point(65, 21);
            this.ux_PLC_WriteTagName.Name = "ux_PLC_WriteTagName";
            this.ux_PLC_WriteTagName.Size = new System.Drawing.Size(122, 20);
            this.ux_PLC_WriteTagName.TabIndex = 2;
            // 
            // ux_PLC_TagWriteValueLabel
            // 
            this.ux_PLC_TagWriteValueLabel.AutoSize = true;
            this.ux_PLC_TagWriteValueLabel.Location = new System.Drawing.Point(17, 54);
            this.ux_PLC_TagWriteValueLabel.Name = "ux_PLC_TagWriteValueLabel";
            this.ux_PLC_TagWriteValueLabel.Size = new System.Drawing.Size(34, 13);
            this.ux_PLC_TagWriteValueLabel.TabIndex = 1;
            this.ux_PLC_TagWriteValueLabel.Text = "Value";
            // 
            // ux_PLC_TagWriteNameLabel
            // 
            this.ux_PLC_TagWriteNameLabel.AutoSize = true;
            this.ux_PLC_TagWriteNameLabel.Location = new System.Drawing.Point(17, 24);
            this.ux_PLC_TagWriteNameLabel.Name = "ux_PLC_TagWriteNameLabel";
            this.ux_PLC_TagWriteNameLabel.Size = new System.Drawing.Size(35, 13);
            this.ux_PLC_TagWriteNameLabel.TabIndex = 0;
            this.ux_PLC_TagWriteNameLabel.Text = "Name";
            // 
            // ux_PLC_Layout_TagView
            // 
            this.ux_PLC_Layout_TagView.Controls.Add(this.ux_PLC_TagView);
            this.ux_PLC_Layout_TagView.Location = new System.Drawing.Point(9, 87);
            this.ux_PLC_Layout_TagView.Name = "ux_PLC_Layout_TagView";
            this.ux_PLC_Layout_TagView.Size = new System.Drawing.Size(670, 227);
            this.ux_PLC_Layout_TagView.TabIndex = 1;
            this.ux_PLC_Layout_TagView.TabStop = false;
            this.ux_PLC_Layout_TagView.Text = "Tag View";
            // 
            // ux_PLC_TagView
            // 
            this.ux_PLC_TagView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_PLC_TagView.HideSelection = false;
            this.ux_PLC_TagView.Location = new System.Drawing.Point(3, 16);
            this.ux_PLC_TagView.Name = "ux_PLC_TagView";
            this.ux_PLC_TagView.Size = new System.Drawing.Size(664, 208);
            this.ux_PLC_TagView.TabIndex = 0;
            this.ux_PLC_TagView.UseCompatibleStateImageBehavior = false;
            this.ux_PLC_TagView.SelectedIndexChanged += new System.EventHandler(this.ListTagViewSelectedIndexChanged);
            // 
            // ux_PLC_Layout_Config
            // 
            this.ux_PLC_Layout_Config.Controls.Add(this.ux_PLC_ChangeIP);
            this.ux_PLC_Layout_Config.Controls.Add(this.ux_PLC_ScanTimeSecondaryMSLabel);
            this.ux_PLC_Layout_Config.Controls.Add(this.ux_PLC_ScanTimeMSLabel);
            this.ux_PLC_Layout_Config.Controls.Add(this.ux_PLC_ScanTimeSecondary);
            this.ux_PLC_Layout_Config.Controls.Add(this.ux_PLC_ScanTime);
            this.ux_PLC_Layout_Config.Controls.Add(this.ux_PLC_ScanSecondaryLabel);
            this.ux_PLC_Layout_Config.Controls.Add(this.ux_PLC_MainScanLabel);
            this.ux_PLC_Layout_Config.Controls.Add(this.ux_PLC_IP);
            this.ux_PLC_Layout_Config.Controls.Add(this.ux_PLC_IPLabel);
            this.ux_PLC_Layout_Config.Location = new System.Drawing.Point(9, 13);
            this.ux_PLC_Layout_Config.Name = "ux_PLC_Layout_Config";
            this.ux_PLC_Layout_Config.Size = new System.Drawing.Size(667, 65);
            this.ux_PLC_Layout_Config.TabIndex = 0;
            this.ux_PLC_Layout_Config.TabStop = false;
            this.ux_PLC_Layout_Config.Text = "Config";
            // 
            // ux_PLC_ChangeIP
            // 
            this.ux_PLC_ChangeIP.Location = new System.Drawing.Point(574, 32);
            this.ux_PLC_ChangeIP.Name = "ux_PLC_ChangeIP";
            this.ux_PLC_ChangeIP.Size = new System.Drawing.Size(75, 23);
            this.ux_PLC_ChangeIP.TabIndex = 8;
            this.ux_PLC_ChangeIP.Text = "Change";
            this.ux_PLC_ChangeIP.UseVisualStyleBackColor = true;
            this.ux_PLC_ChangeIP.Click += new System.EventHandler(this.ButtonChangeIpClick);
            // 
            // ux_PLC_ScanTimeSecondaryMSLabel
            // 
            this.ux_PLC_ScanTimeSecondaryMSLabel.AutoSize = true;
            this.ux_PLC_ScanTimeSecondaryMSLabel.Location = new System.Drawing.Point(475, 37);
            this.ux_PLC_ScanTimeSecondaryMSLabel.Name = "ux_PLC_ScanTimeSecondaryMSLabel";
            this.ux_PLC_ScanTimeSecondaryMSLabel.Size = new System.Drawing.Size(20, 13);
            this.ux_PLC_ScanTimeSecondaryMSLabel.TabIndex = 7;
            this.ux_PLC_ScanTimeSecondaryMSLabel.Text = "ms";
            // 
            // ux_PLC_ScanTimeMSLabel
            // 
            this.ux_PLC_ScanTimeMSLabel.AutoSize = true;
            this.ux_PLC_ScanTimeMSLabel.Location = new System.Drawing.Point(275, 37);
            this.ux_PLC_ScanTimeMSLabel.Name = "ux_PLC_ScanTimeMSLabel";
            this.ux_PLC_ScanTimeMSLabel.Size = new System.Drawing.Size(20, 13);
            this.ux_PLC_ScanTimeMSLabel.TabIndex = 6;
            this.ux_PLC_ScanTimeMSLabel.Text = "ms";
            // 
            // ux_PLC_ScanTimeSecondary
            // 
            this.ux_PLC_ScanTimeSecondary.Location = new System.Drawing.Point(412, 35);
            this.ux_PLC_ScanTimeSecondary.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.ux_PLC_ScanTimeSecondary.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ux_PLC_ScanTimeSecondary.Name = "ux_PLC_ScanTimeSecondary";
            this.ux_PLC_ScanTimeSecondary.Size = new System.Drawing.Size(57, 20);
            this.ux_PLC_ScanTimeSecondary.TabIndex = 5;
            this.ux_PLC_ScanTimeSecondary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ux_PLC_ScanTimeSecondary.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ux_PLC_ScanTime
            // 
            this.ux_PLC_ScanTime.Location = new System.Drawing.Point(212, 34);
            this.ux_PLC_ScanTime.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.ux_PLC_ScanTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ux_PLC_ScanTime.Name = "ux_PLC_ScanTime";
            this.ux_PLC_ScanTime.Size = new System.Drawing.Size(57, 20);
            this.ux_PLC_ScanTime.TabIndex = 4;
            this.ux_PLC_ScanTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ux_PLC_ScanTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ux_PLC_ScanSecondaryLabel
            // 
            this.ux_PLC_ScanSecondaryLabel.AutoSize = true;
            this.ux_PLC_ScanSecondaryLabel.Location = new System.Drawing.Point(377, 16);
            this.ux_PLC_ScanSecondaryLabel.Name = "ux_PLC_ScanSecondaryLabel";
            this.ux_PLC_ScanSecondaryLabel.Size = new System.Drawing.Size(118, 13);
            this.ux_PLC_ScanSecondaryLabel.TabIndex = 3;
            this.ux_PLC_ScanSecondaryLabel.Text = "Scan Time (Secondary)";
            // 
            // ux_PLC_MainScanLabel
            // 
            this.ux_PLC_MainScanLabel.AutoSize = true;
            this.ux_PLC_MainScanLabel.Location = new System.Drawing.Point(194, 16);
            this.ux_PLC_MainScanLabel.Name = "ux_PLC_MainScanLabel";
            this.ux_PLC_MainScanLabel.Size = new System.Drawing.Size(90, 13);
            this.ux_PLC_MainScanLabel.TabIndex = 2;
            this.ux_PLC_MainScanLabel.Text = "Scan Time (Main)";
            // 
            // ux_PLC_IP
            // 
            this.ux_PLC_IP.Location = new System.Drawing.Point(20, 34);
            this.ux_PLC_IP.Name = "ux_PLC_IP";
            this.ux_PLC_IP.Size = new System.Drawing.Size(130, 20);
            this.ux_PLC_IP.TabIndex = 1;
            // 
            // ux_PLC_IPLabel
            // 
            this.ux_PLC_IPLabel.AutoSize = true;
            this.ux_PLC_IPLabel.Location = new System.Drawing.Point(46, 16);
            this.ux_PLC_IPLabel.Name = "ux_PLC_IPLabel";
            this.ux_PLC_IPLabel.Size = new System.Drawing.Size(58, 13);
            this.ux_PLC_IPLabel.TabIndex = 0;
            this.ux_PLC_IPLabel.Text = "IP Address";
            // 
            // ux_Tab_FileImport
            // 
            this.ux_Tab_FileImport.Controls.Add(this.ux_Import_Layout_Main);
            this.ux_Tab_FileImport.Location = new System.Drawing.Point(4, 4);
            this.ux_Tab_FileImport.Name = "ux_Tab_FileImport";
            this.ux_Tab_FileImport.Padding = new System.Windows.Forms.Padding(3);
            this.ux_Tab_FileImport.Size = new System.Drawing.Size(685, 491);
            this.ux_Tab_FileImport.TabIndex = 12;
            this.ux_Tab_FileImport.Text = "imp";
            this.ux_Tab_FileImport.Enter += new System.EventHandler(this.TabEnter);
            // 
            // ux_Import_Layout_Main
            // 
            this.ux_Import_Layout_Main.ColumnCount = 1;
            this.ux_Import_Layout_Main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_Import_Layout_Main.Controls.Add(this.ux_Import_PathSettingsLabel, 0, 0);
            this.ux_Import_Layout_Main.Controls.Add(this.ux_Set_ImportConfigLabel, 0, 2);
            this.ux_Import_Layout_Main.Controls.Add(this.ux_Layout_ImportConfig, 0, 3);
            this.ux_Import_Layout_Main.Controls.Add(this.ux_flp_ImportPaths, 0, 1);
            this.ux_Import_Layout_Main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Import_Layout_Main.Location = new System.Drawing.Point(3, 3);
            this.ux_Import_Layout_Main.Name = "ux_Import_Layout_Main";
            this.ux_Import_Layout_Main.RowCount = 4;
            this.ux_Import_Layout_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.ux_Import_Layout_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 82.07547F));
            this.ux_Import_Layout_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.92453F));
            this.ux_Import_Layout_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 251F));
            this.ux_Import_Layout_Main.Size = new System.Drawing.Size(679, 485);
            this.ux_Import_Layout_Main.TabIndex = 0;
            // 
            // ux_Import_PathSettingsLabel
            // 
            this.ux_Import_PathSettingsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Import_PathSettingsLabel.AutoSize = true;
            this.ux_Import_PathSettingsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Import_PathSettingsLabel.Location = new System.Drawing.Point(3, 9);
            this.ux_Import_PathSettingsLabel.Name = "ux_Import_PathSettingsLabel";
            this.ux_Import_PathSettingsLabel.Size = new System.Drawing.Size(673, 25);
            this.ux_Import_PathSettingsLabel.TabIndex = 1;
            this.ux_Import_PathSettingsLabel.Text = "Path Settings";
            // 
            // ux_Set_ImportConfigLabel
            // 
            this.ux_Set_ImportConfigLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Set_ImportConfigLabel.AutoSize = true;
            this.ux_Set_ImportConfigLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Set_ImportConfigLabel.Location = new System.Drawing.Point(3, 208);
            this.ux_Set_ImportConfigLabel.Name = "ux_Set_ImportConfigLabel";
            this.ux_Set_ImportConfigLabel.Size = new System.Drawing.Size(673, 25);
            this.ux_Set_ImportConfigLabel.TabIndex = 2;
            this.ux_Set_ImportConfigLabel.Text = "Configuration";
            // 
            // ux_Layout_ImportConfig
            // 
            this.ux_Layout_ImportConfig.ColumnCount = 3;
            this.ux_Layout_ImportConfig.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_ImportConfig.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_ImportConfig.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_ImportConfig.Controls.Add(this.ux_Import_ReadWait, 1, 1);
            this.ux_Layout_ImportConfig.Controls.Add(this.ux_Import_ReadWaitLabel, 0, 1);
            this.ux_Layout_ImportConfig.Controls.Add(this.ux_Import_FilePollRateLabel, 0, 0);
            this.ux_Layout_ImportConfig.Controls.Add(this.ux_Import_PollRate, 1, 0);
            this.ux_Layout_ImportConfig.Controls.Add(this.ux_Import_ToggleJobLoadMode, 1, 2);
            this.ux_Layout_ImportConfig.Controls.Add(this.ux_Import_JobLoadDesc, 2, 2);
            this.ux_Layout_ImportConfig.Controls.Add(this.ux_Import_ToggleFileImport, 1, 3);
            this.ux_Layout_ImportConfig.Controls.Add(this.ux_Import_ImportDesc, 2, 3);
            this.ux_Layout_ImportConfig.Controls.Add(this.ux_flp_ImportSaveControls, 2, 4);
            this.ux_Layout_ImportConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_ImportConfig.Location = new System.Drawing.Point(3, 236);
            this.ux_Layout_ImportConfig.Name = "ux_Layout_ImportConfig";
            this.ux_Layout_ImportConfig.RowCount = 5;
            this.ux_Layout_ImportConfig.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_Layout_ImportConfig.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_Layout_ImportConfig.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_Layout_ImportConfig.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_Layout_ImportConfig.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_Layout_ImportConfig.Size = new System.Drawing.Size(673, 246);
            this.ux_Layout_ImportConfig.TabIndex = 3;
            // 
            // ux_Import_ReadWait
            // 
            this.ux_Import_ReadWait.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ux_Import_ReadWait.Location = new System.Drawing.Point(79, 29);
            this.ux_Import_ReadWait.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.ux_Import_ReadWait.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ux_Import_ReadWait.Name = "ux_Import_ReadWait";
            this.ux_Import_ReadWait.Size = new System.Drawing.Size(79, 20);
            this.ux_Import_ReadWait.TabIndex = 4;
            this.ux_Import_ReadWait.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ux_Import_ReadWaitLabel
            // 
            this.ux_Import_ReadWaitLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Import_ReadWaitLabel.AutoSize = true;
            this.ux_Import_ReadWaitLabel.Location = new System.Drawing.Point(3, 32);
            this.ux_Import_ReadWaitLabel.Name = "ux_Import_ReadWaitLabel";
            this.ux_Import_ReadWaitLabel.Size = new System.Drawing.Size(70, 13);
            this.ux_Import_ReadWaitLabel.TabIndex = 3;
            this.ux_Import_ReadWaitLabel.Text = "Wait to Read";
            // 
            // ux_Import_FilePollRateLabel
            // 
            this.ux_Import_FilePollRateLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Import_FilePollRateLabel.AutoSize = true;
            this.ux_Import_FilePollRateLabel.Location = new System.Drawing.Point(3, 6);
            this.ux_Import_FilePollRateLabel.Name = "ux_Import_FilePollRateLabel";
            this.ux_Import_FilePollRateLabel.Size = new System.Drawing.Size(70, 13);
            this.ux_Import_FilePollRateLabel.TabIndex = 1;
            this.ux_Import_FilePollRateLabel.Text = "File Poll Rate";
            // 
            // ux_Import_PollRate
            // 
            this.ux_Import_PollRate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ux_Import_PollRate.Location = new System.Drawing.Point(79, 3);
            this.ux_Import_PollRate.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.ux_Import_PollRate.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ux_Import_PollRate.Name = "ux_Import_PollRate";
            this.ux_Import_PollRate.Size = new System.Drawing.Size(79, 20);
            this.ux_Import_PollRate.TabIndex = 2;
            this.ux_Import_PollRate.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ux_Import_ToggleJobLoadMode
            // 
            this.ux_Import_ToggleJobLoadMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Import_ToggleJobLoadMode.AutoSize = true;
            this.ux_Import_ToggleJobLoadMode.Location = new System.Drawing.Point(79, 55);
            this.ux_Import_ToggleJobLoadMode.Name = "ux_Import_ToggleJobLoadMode";
            this.ux_Import_ToggleJobLoadMode.Size = new System.Drawing.Size(116, 17);
            this.ux_Import_ToggleJobLoadMode.TabIndex = 12;
            this.ux_Import_ToggleJobLoadMode.Text = "Job Load Mode";
            this.ux_Import_ToggleJobLoadMode.UseVisualStyleBackColor = true;
            // 
            // ux_Import_JobLoadDesc
            // 
            this.ux_Import_JobLoadDesc.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ux_Import_JobLoadDesc.AutoSize = true;
            this.ux_Import_JobLoadDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Import_JobLoadDesc.Location = new System.Drawing.Point(201, 57);
            this.ux_Import_JobLoadDesc.Name = "ux_Import_JobLoadDesc";
            this.ux_Import_JobLoadDesc.Size = new System.Drawing.Size(207, 13);
            this.ux_Import_JobLoadDesc.TabIndex = 13;
            this.ux_Import_JobLoadDesc.Text = "First line with a * prefix denotes job to load.";
            // 
            // ux_Import_ToggleFileImport
            // 
            this.ux_Import_ToggleFileImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Import_ToggleFileImport.AutoSize = true;
            this.ux_Import_ToggleFileImport.Location = new System.Drawing.Point(79, 78);
            this.ux_Import_ToggleFileImport.Name = "ux_Import_ToggleFileImport";
            this.ux_Import_ToggleFileImport.Size = new System.Drawing.Size(116, 17);
            this.ux_Import_ToggleFileImport.TabIndex = 14;
            this.ux_Import_ToggleFileImport.Text = "File Import Enabled";
            this.ux_Import_ToggleFileImport.UseVisualStyleBackColor = true;
            // 
            // ux_Import_ImportDesc
            // 
            this.ux_Import_ImportDesc.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ux_Import_ImportDesc.AutoSize = true;
            this.ux_Import_ImportDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Import_ImportDesc.Location = new System.Drawing.Point(201, 80);
            this.ux_Import_ImportDesc.Name = "ux_Import_ImportDesc";
            this.ux_Import_ImportDesc.Size = new System.Drawing.Size(85, 13);
            this.ux_Import_ImportDesc.TabIndex = 15;
            this.ux_Import_ImportDesc.Text = "Restart required.";
            // 
            // ux_flp_ImportSaveControls
            // 
            this.ux_flp_ImportSaveControls.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_flp_ImportSaveControls.Controls.Add(this.ux_ControlButtonSave_Paths);
            this.ux_flp_ImportSaveControls.Controls.Add(this.ux_Button_BackFromImportPaths);
            this.ux_flp_ImportSaveControls.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.ux_flp_ImportSaveControls.Location = new System.Drawing.Point(201, 143);
            this.ux_flp_ImportSaveControls.Name = "ux_flp_ImportSaveControls";
            this.ux_flp_ImportSaveControls.Size = new System.Drawing.Size(469, 100);
            this.ux_flp_ImportSaveControls.TabIndex = 17;
            // 
            // ux_ControlButtonSave_Paths
            // 
            this.ux_ControlButtonSave_Paths.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_ControlButtonSave_Paths.Location = new System.Drawing.Point(391, 3);
            this.ux_ControlButtonSave_Paths.MinimumSize = new System.Drawing.Size(75, 100);
            this.ux_ControlButtonSave_Paths.Name = "ux_ControlButtonSave_Paths";
            this.ux_ControlButtonSave_Paths.Size = new System.Drawing.Size(75, 100);
            this.ux_ControlButtonSave_Paths.TabIndex = 16;
            this.ux_ControlButtonSave_Paths.Click += new System.EventHandler(this.ButtonImportSaveClick);
            // 
            // ux_Button_BackFromImportPaths
            // 
            this.ux_Button_BackFromImportPaths.Location = new System.Drawing.Point(300, 3);
            this.ux_Button_BackFromImportPaths.MinimumSize = new System.Drawing.Size(85, 100);
            this.ux_Button_BackFromImportPaths.Name = "ux_Button_BackFromImportPaths";
            this.ux_Button_BackFromImportPaths.Size = new System.Drawing.Size(85, 100);
            this.ux_Button_BackFromImportPaths.TabIndex = 18;
            this.ux_Button_BackFromImportPaths.Click += new System.EventHandler(this.ButtonImportBackClick);
            // 
            // ux_flp_ImportPaths
            // 
            this.ux_flp_ImportPaths.Controls.Add(this.ux_Control_ImportPath);
            this.ux_flp_ImportPaths.Controls.Add(this.ux_Control_CompletePath);
            this.ux_flp_ImportPaths.Controls.Add(this.ux_Control_ErrorPath);
            this.ux_flp_ImportPaths.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_flp_ImportPaths.Location = new System.Drawing.Point(3, 37);
            this.ux_flp_ImportPaths.Name = "ux_flp_ImportPaths";
            this.ux_flp_ImportPaths.Size = new System.Drawing.Size(673, 158);
            this.ux_flp_ImportPaths.TabIndex = 17;
            // 
            // ux_Control_ImportPath
            // 
            this.ux_Control_ImportPath.Location = new System.Drawing.Point(3, 3);
            this.ux_Control_ImportPath.MinimumSize = new System.Drawing.Size(465, 36);
            this.ux_Control_ImportPath.Name = "ux_Control_ImportPath";
            this.ux_Control_ImportPath.PathLabel = "File Location";
            this.ux_Control_ImportPath.SelectedPath = "C:\\temp\\";
            this.ux_Control_ImportPath.Size = new System.Drawing.Size(667, 36);
            this.ux_Control_ImportPath.TabIndex = 10;
            // 
            // ux_Control_CompletePath
            // 
            this.ux_Control_CompletePath.Location = new System.Drawing.Point(3, 45);
            this.ux_Control_CompletePath.MinimumSize = new System.Drawing.Size(465, 36);
            this.ux_Control_CompletePath.Name = "ux_Control_CompletePath";
            this.ux_Control_CompletePath.PathLabel = "Completed Location";
            this.ux_Control_CompletePath.SelectedPath = "";
            this.ux_Control_CompletePath.Size = new System.Drawing.Size(667, 36);
            this.ux_Control_CompletePath.TabIndex = 11;
            // 
            // ux_Control_ErrorPath
            // 
            this.ux_Control_ErrorPath.Location = new System.Drawing.Point(3, 87);
            this.ux_Control_ErrorPath.MinimumSize = new System.Drawing.Size(465, 36);
            this.ux_Control_ErrorPath.Name = "ux_Control_ErrorPath";
            this.ux_Control_ErrorPath.PathLabel = "Error Location";
            this.ux_Control_ErrorPath.SelectedPath = "";
            this.ux_Control_ErrorPath.Size = new System.Drawing.Size(667, 36);
            this.ux_Control_ErrorPath.TabIndex = 12;
            // 
            // ux_Tab_Auth
            // 
            this.ux_Tab_Auth.Controls.Add(this.ux_Layout_AuthMain);
            this.ux_Tab_Auth.Location = new System.Drawing.Point(4, 4);
            this.ux_Tab_Auth.Name = "ux_Tab_Auth";
            this.ux_Tab_Auth.Padding = new System.Windows.Forms.Padding(3);
            this.ux_Tab_Auth.Size = new System.Drawing.Size(685, 491);
            this.ux_Tab_Auth.TabIndex = 13;
            this.ux_Tab_Auth.Text = "auth";
            this.ux_Tab_Auth.UseVisualStyleBackColor = true;
            this.ux_Tab_Auth.Enter += new System.EventHandler(this.TabEnter);
            // 
            // ux_Layout_AuthMain
            // 
            this.ux_Layout_AuthMain.ColumnCount = 3;
            this.ux_Layout_AuthMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.05355F));
            this.ux_Layout_AuthMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.89289F));
            this.ux_Layout_AuthMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.05355F));
            this.ux_Layout_AuthMain.Controls.Add(this.ux_Layout_KeyPadKeys, 1, 2);
            this.ux_Layout_AuthMain.Controls.Add(this.ux_Auth_MaskedKey, 1, 1);
            this.ux_Layout_AuthMain.Controls.Add(this.ux_Auth_Level, 1, 0);
            this.ux_Layout_AuthMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_AuthMain.Location = new System.Drawing.Point(3, 3);
            this.ux_Layout_AuthMain.Name = "ux_Layout_AuthMain";
            this.ux_Layout_AuthMain.RowCount = 3;
            this.ux_Layout_AuthMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 38.13559F));
            this.ux_Layout_AuthMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 61.86441F));
            this.ux_Layout_AuthMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 379F));
            this.ux_Layout_AuthMain.Size = new System.Drawing.Size(679, 485);
            this.ux_Layout_AuthMain.TabIndex = 0;
            // 
            // ux_Layout_KeyPadKeys
            // 
            this.ux_Layout_KeyPadKeys.ColumnCount = 3;
            this.ux_Layout_KeyPadKeys.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_Layout_KeyPadKeys.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.ux_Layout_KeyPadKeys.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Auth_Key6, 2, 1);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Auth_Key5, 1, 1);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Auth_Key4, 0, 1);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Auth_Key0, 1, 3);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Auth_KeyClear, 0, 3);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Auth_KeyOK, 2, 3);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Auth_Key7, 0, 2);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Auth_Key1, 0, 0);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Auth_Key8, 1, 2);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Auth_Key2, 1, 0);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Auth_Key9, 2, 2);
            this.ux_Layout_KeyPadKeys.Controls.Add(this.ux_Auth_Key3, 2, 0);
            this.ux_Layout_KeyPadKeys.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_KeyPadKeys.Location = new System.Drawing.Point(91, 108);
            this.ux_Layout_KeyPadKeys.Name = "ux_Layout_KeyPadKeys";
            this.ux_Layout_KeyPadKeys.RowCount = 4;
            this.ux_Layout_KeyPadKeys.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_Layout_KeyPadKeys.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_Layout_KeyPadKeys.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_Layout_KeyPadKeys.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_Layout_KeyPadKeys.Size = new System.Drawing.Size(495, 374);
            this.ux_Layout_KeyPadKeys.TabIndex = 0;
            // 
            // ux_Auth_Key6
            // 
            this.ux_Auth_Key6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ux_Auth_Key6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Auth_Key6.Image = ((System.Drawing.Image)(resources.GetObject("ux_Auth_Key6.Image")));
            this.ux_Auth_Key6.Location = new System.Drawing.Point(332, 96);
            this.ux_Auth_Key6.Name = "ux_Auth_Key6";
            this.ux_Auth_Key6.Size = new System.Drawing.Size(120, 87);
            this.ux_Auth_Key6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Auth_Key6.TabIndex = 9;
            this.ux_Auth_Key6.TabStop = false;
            this.ux_Auth_Key6.Click += new System.EventHandler(this.PicKey6Click);
            // 
            // ux_Auth_Key5
            // 
            this.ux_Auth_Key5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Auth_Key5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Auth_Key5.Image = ((System.Drawing.Image)(resources.GetObject("ux_Auth_Key5.Image")));
            this.ux_Auth_Key5.Location = new System.Drawing.Point(186, 96);
            this.ux_Auth_Key5.Name = "ux_Auth_Key5";
            this.ux_Auth_Key5.Size = new System.Drawing.Size(120, 87);
            this.ux_Auth_Key5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Auth_Key5.TabIndex = 8;
            this.ux_Auth_Key5.TabStop = false;
            this.ux_Auth_Key5.Click += new System.EventHandler(this.PicKey5Click);
            // 
            // ux_Auth_Key4
            // 
            this.ux_Auth_Key4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ux_Auth_Key4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Auth_Key4.Image = ((System.Drawing.Image)(resources.GetObject("ux_Auth_Key4.Image")));
            this.ux_Auth_Key4.Location = new System.Drawing.Point(41, 96);
            this.ux_Auth_Key4.Name = "ux_Auth_Key4";
            this.ux_Auth_Key4.Size = new System.Drawing.Size(120, 87);
            this.ux_Auth_Key4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Auth_Key4.TabIndex = 7;
            this.ux_Auth_Key4.TabStop = false;
            this.ux_Auth_Key4.Click += new System.EventHandler(this.PicKey4Click);
            // 
            // ux_Auth_Key0
            // 
            this.ux_Auth_Key0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Auth_Key0.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Auth_Key0.Image = ((System.Drawing.Image)(resources.GetObject("ux_Auth_Key0.Image")));
            this.ux_Auth_Key0.Location = new System.Drawing.Point(186, 282);
            this.ux_Auth_Key0.Name = "ux_Auth_Key0";
            this.ux_Auth_Key0.Size = new System.Drawing.Size(120, 89);
            this.ux_Auth_Key0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Auth_Key0.TabIndex = 0;
            this.ux_Auth_Key0.TabStop = false;
            this.ux_Auth_Key0.Click += new System.EventHandler(this.PicKey0Click);
            // 
            // ux_Auth_KeyClear
            // 
            this.ux_Auth_KeyClear.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ux_Auth_KeyClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Auth_KeyClear.Image = ((System.Drawing.Image)(resources.GetObject("ux_Auth_KeyClear.Image")));
            this.ux_Auth_KeyClear.Location = new System.Drawing.Point(41, 282);
            this.ux_Auth_KeyClear.Name = "ux_Auth_KeyClear";
            this.ux_Auth_KeyClear.Size = new System.Drawing.Size(120, 89);
            this.ux_Auth_KeyClear.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Auth_KeyClear.TabIndex = 10;
            this.ux_Auth_KeyClear.TabStop = false;
            this.ux_Auth_KeyClear.Click += new System.EventHandler(this.PicKeyClearClick);
            // 
            // ux_Auth_KeyOK
            // 
            this.ux_Auth_KeyOK.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ux_Auth_KeyOK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Auth_KeyOK.Image = ((System.Drawing.Image)(resources.GetObject("ux_Auth_KeyOK.Image")));
            this.ux_Auth_KeyOK.Location = new System.Drawing.Point(332, 282);
            this.ux_Auth_KeyOK.Name = "ux_Auth_KeyOK";
            this.ux_Auth_KeyOK.Size = new System.Drawing.Size(120, 89);
            this.ux_Auth_KeyOK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Auth_KeyOK.TabIndex = 11;
            this.ux_Auth_KeyOK.TabStop = false;
            this.ux_Auth_KeyOK.Click += new System.EventHandler(this.PicKeyOkClick);
            // 
            // ux_Auth_Key7
            // 
            this.ux_Auth_Key7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ux_Auth_Key7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Auth_Key7.Image = ((System.Drawing.Image)(resources.GetObject("ux_Auth_Key7.Image")));
            this.ux_Auth_Key7.Location = new System.Drawing.Point(41, 189);
            this.ux_Auth_Key7.Name = "ux_Auth_Key7";
            this.ux_Auth_Key7.Size = new System.Drawing.Size(120, 87);
            this.ux_Auth_Key7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Auth_Key7.TabIndex = 4;
            this.ux_Auth_Key7.TabStop = false;
            this.ux_Auth_Key7.Click += new System.EventHandler(this.PicKey7Click);
            // 
            // ux_Auth_Key1
            // 
            this.ux_Auth_Key1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ux_Auth_Key1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Auth_Key1.Image = ((System.Drawing.Image)(resources.GetObject("ux_Auth_Key1.Image")));
            this.ux_Auth_Key1.Location = new System.Drawing.Point(41, 3);
            this.ux_Auth_Key1.Name = "ux_Auth_Key1";
            this.ux_Auth_Key1.Size = new System.Drawing.Size(120, 87);
            this.ux_Auth_Key1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Auth_Key1.TabIndex = 1;
            this.ux_Auth_Key1.TabStop = false;
            this.ux_Auth_Key1.Click += new System.EventHandler(this.PicKey1Click);
            // 
            // ux_Auth_Key8
            // 
            this.ux_Auth_Key8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Auth_Key8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Auth_Key8.Image = ((System.Drawing.Image)(resources.GetObject("ux_Auth_Key8.Image")));
            this.ux_Auth_Key8.Location = new System.Drawing.Point(186, 189);
            this.ux_Auth_Key8.Name = "ux_Auth_Key8";
            this.ux_Auth_Key8.Size = new System.Drawing.Size(120, 87);
            this.ux_Auth_Key8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Auth_Key8.TabIndex = 5;
            this.ux_Auth_Key8.TabStop = false;
            this.ux_Auth_Key8.Click += new System.EventHandler(this.PicKey8Click);
            // 
            // ux_Auth_Key2
            // 
            this.ux_Auth_Key2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Auth_Key2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Auth_Key2.Image = ((System.Drawing.Image)(resources.GetObject("ux_Auth_Key2.Image")));
            this.ux_Auth_Key2.Location = new System.Drawing.Point(186, 3);
            this.ux_Auth_Key2.Name = "ux_Auth_Key2";
            this.ux_Auth_Key2.Size = new System.Drawing.Size(120, 87);
            this.ux_Auth_Key2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Auth_Key2.TabIndex = 2;
            this.ux_Auth_Key2.TabStop = false;
            this.ux_Auth_Key2.Click += new System.EventHandler(this.PicKey2Click);
            // 
            // ux_Auth_Key9
            // 
            this.ux_Auth_Key9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ux_Auth_Key9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Auth_Key9.Image = ((System.Drawing.Image)(resources.GetObject("ux_Auth_Key9.Image")));
            this.ux_Auth_Key9.Location = new System.Drawing.Point(332, 189);
            this.ux_Auth_Key9.Name = "ux_Auth_Key9";
            this.ux_Auth_Key9.Size = new System.Drawing.Size(120, 87);
            this.ux_Auth_Key9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Auth_Key9.TabIndex = 6;
            this.ux_Auth_Key9.TabStop = false;
            this.ux_Auth_Key9.Click += new System.EventHandler(this.PicKey9Click);
            // 
            // ux_Auth_Key3
            // 
            this.ux_Auth_Key3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ux_Auth_Key3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Auth_Key3.Image = ((System.Drawing.Image)(resources.GetObject("ux_Auth_Key3.Image")));
            this.ux_Auth_Key3.Location = new System.Drawing.Point(332, 3);
            this.ux_Auth_Key3.Name = "ux_Auth_Key3";
            this.ux_Auth_Key3.Size = new System.Drawing.Size(120, 87);
            this.ux_Auth_Key3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Auth_Key3.TabIndex = 3;
            this.ux_Auth_Key3.TabStop = false;
            this.ux_Auth_Key3.Click += new System.EventHandler(this.PicKey3Click);
            // 
            // ux_Auth_MaskedKey
            // 
            this.ux_Auth_MaskedKey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Auth_MaskedKey.AutoSize = true;
            this.ux_Auth_MaskedKey.BackColor = System.Drawing.Color.LemonChiffon;
            this.ux_Auth_MaskedKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ux_Auth_MaskedKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Auth_MaskedKey.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.ux_Auth_MaskedKey.Location = new System.Drawing.Point(88, 40);
            this.ux_Auth_MaskedKey.Margin = new System.Windows.Forms.Padding(0);
            this.ux_Auth_MaskedKey.Name = "ux_Auth_MaskedKey";
            this.ux_Auth_MaskedKey.Size = new System.Drawing.Size(501, 65);
            this.ux_Auth_MaskedKey.TabIndex = 1;
            this.ux_Auth_MaskedKey.Text = "12345";
            this.ux_Auth_MaskedKey.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ux_Auth_Level
            // 
            this.ux_Auth_Level.AutoSize = true;
            this.ux_Auth_Level.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Auth_Level.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Auth_Level.ForeColor = System.Drawing.Color.SteelBlue;
            this.ux_Auth_Level.Location = new System.Drawing.Point(91, 0);
            this.ux_Auth_Level.Name = "ux_Auth_Level";
            this.ux_Auth_Level.Size = new System.Drawing.Size(495, 40);
            this.ux_Auth_Level.TabIndex = 2;
            this.ux_Auth_Level.Text = "Authorization: Operator";
            this.ux_Auth_Level.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ux_Tab_About
            // 
            this.ux_Tab_About.Controls.Add(this.ux_TextBox_About);
            this.ux_Tab_About.Location = new System.Drawing.Point(4, 4);
            this.ux_Tab_About.Name = "ux_Tab_About";
            this.ux_Tab_About.Padding = new System.Windows.Forms.Padding(3);
            this.ux_Tab_About.Size = new System.Drawing.Size(685, 491);
            this.ux_Tab_About.TabIndex = 14;
            this.ux_Tab_About.Text = "abt";
            this.ux_Tab_About.UseVisualStyleBackColor = true;
            this.ux_Tab_About.Enter += new System.EventHandler(this.TabEnter);
            // 
            // ux_TextBox_About
            // 
            this.ux_TextBox_About.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_TextBox_About.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_TextBox_About.Location = new System.Drawing.Point(3, 3);
            this.ux_TextBox_About.Margin = new System.Windows.Forms.Padding(10, 60, 10, 10);
            this.ux_TextBox_About.Multiline = true;
            this.ux_TextBox_About.Name = "ux_TextBox_About";
            this.ux_TextBox_About.Size = new System.Drawing.Size(679, 485);
            this.ux_TextBox_About.TabIndex = 1;
            this.ux_TextBox_About.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ux_Tab_JobSelect
            // 
            this.ux_Tab_JobSelect.Controls.Add(this.ux_Job_Tree);
            this.ux_Tab_JobSelect.Location = new System.Drawing.Point(4, 4);
            this.ux_Tab_JobSelect.Name = "ux_Tab_JobSelect";
            this.ux_Tab_JobSelect.Padding = new System.Windows.Forms.Padding(3);
            this.ux_Tab_JobSelect.Size = new System.Drawing.Size(685, 491);
            this.ux_Tab_JobSelect.TabIndex = 15;
            this.ux_Tab_JobSelect.Text = "jb";
            this.ux_Tab_JobSelect.UseVisualStyleBackColor = true;
            this.ux_Tab_JobSelect.Enter += new System.EventHandler(this.TabEnter);
            // 
            // ux_Job_Tree
            // 
            this.ux_Job_Tree.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ux_Job_Tree.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Job_Tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Job_Tree.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Job_Tree.FullRowSelect = true;
            this.ux_Job_Tree.Indent = 20;
            this.ux_Job_Tree.ItemHeight = 35;
            this.ux_Job_Tree.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.ux_Job_Tree.Location = new System.Drawing.Point(3, 3);
            this.ux_Job_Tree.Name = "ux_Job_Tree";
            this.ux_Job_Tree.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ux_Job_Tree.ShowLines = false;
            this.ux_Job_Tree.Size = new System.Drawing.Size(679, 485);
            this.ux_Job_Tree.TabIndex = 0;
            this.ux_Job_Tree.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.JobTreeNodeMouseClick);
            // 
            // ux_Tab_Axis
            // 
            this.ux_Tab_Axis.Controls.Add(this.ux_Layout_AxisMain);
            this.ux_Tab_Axis.Location = new System.Drawing.Point(4, 4);
            this.ux_Tab_Axis.Name = "ux_Tab_Axis";
            this.ux_Tab_Axis.Padding = new System.Windows.Forms.Padding(3);
            this.ux_Tab_Axis.Size = new System.Drawing.Size(685, 491);
            this.ux_Tab_Axis.TabIndex = 16;
            this.ux_Tab_Axis.Text = "axs";
            this.ux_Tab_Axis.UseVisualStyleBackColor = true;
            // 
            // ux_Layout_AxisMain
            // 
            this.ux_Layout_AxisMain.ColumnCount = 3;
            this.ux_Layout_AxisMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_Layout_AxisMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.84241F));
            this.ux_Layout_AxisMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.87334F));
            this.ux_Layout_AxisMain.Controls.Add(this.ux_Layout_RAxis, 1, 1);
            this.ux_Layout_AxisMain.Controls.Add(this.ux_Label_AxisR, 1, 0);
            this.ux_Layout_AxisMain.Controls.Add(this.ux_Label_AxisX, 2, 0);
            this.ux_Layout_AxisMain.Controls.Add(this.ux_Label_AxisZ, 0, 0);
            this.ux_Layout_AxisMain.Controls.Add(this.ux_Layout_ZAxis, 0, 1);
            this.ux_Layout_AxisMain.Controls.Add(this.ux_tlp_LowerAxisSave, 0, 2);
            this.ux_Layout_AxisMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_AxisMain.Location = new System.Drawing.Point(3, 3);
            this.ux_Layout_AxisMain.Name = "ux_Layout_AxisMain";
            this.ux_Layout_AxisMain.RowCount = 3;
            this.ux_Layout_AxisMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.695652F));
            this.ux_Layout_AxisMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 91.30434F));
            this.ux_Layout_AxisMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 155F));
            this.ux_Layout_AxisMain.Size = new System.Drawing.Size(679, 485);
            this.ux_Layout_AxisMain.TabIndex = 0;
            // 
            // ux_Layout_RAxis
            // 
            this.ux_Layout_RAxis.ColumnCount = 2;
            this.ux_Layout_RAxis.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_RAxis.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_RAxis.Controls.Add(this.ux_Label_RHomeAxisEveryN, 0, 2);
            this.ux_Layout_RAxis.Controls.Add(this.ux_CheckBox_AxisREnabled, 0, 1);
            this.ux_Layout_RAxis.Controls.Add(this.ux_Numeric_RHomeFrequency, 1, 2);
            this.ux_Layout_RAxis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_RAxis.Location = new System.Drawing.Point(229, 31);
            this.ux_Layout_RAxis.Name = "ux_Layout_RAxis";
            this.ux_Layout_RAxis.RowCount = 4;
            this.ux_Layout_RAxis.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.ux_Layout_RAxis.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_Layout_RAxis.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_Layout_RAxis.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_Layout_RAxis.Size = new System.Drawing.Size(216, 295);
            this.ux_Layout_RAxis.TabIndex = 5;
            // 
            // ux_Label_RHomeAxisEveryN
            // 
            this.ux_Label_RHomeAxisEveryN.AutoSize = true;
            this.ux_Label_RHomeAxisEveryN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Label_RHomeAxisEveryN.Location = new System.Drawing.Point(3, 43);
            this.ux_Label_RHomeAxisEveryN.Name = "ux_Label_RHomeAxisEveryN";
            this.ux_Label_RHomeAxisEveryN.Size = new System.Drawing.Size(113, 32);
            this.ux_Label_RHomeAxisEveryN.TabIndex = 14;
            this.ux_Label_RHomeAxisEveryN.Text = "Home Axis Frequency:";
            this.ux_Label_RHomeAxisEveryN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ux_CheckBox_AxisREnabled
            // 
            this.ux_CheckBox_AxisREnabled.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_CheckBox_AxisREnabled.AutoSize = true;
            this.ux_Layout_RAxis.SetColumnSpan(this.ux_CheckBox_AxisREnabled, 2);
            this.ux_CheckBox_AxisREnabled.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ux_CheckBox_AxisREnabled.Location = new System.Drawing.Point(75, 23);
            this.ux_CheckBox_AxisREnabled.Name = "ux_CheckBox_AxisREnabled";
            this.ux_CheckBox_AxisREnabled.Size = new System.Drawing.Size(65, 17);
            this.ux_CheckBox_AxisREnabled.TabIndex = 15;
            this.ux_CheckBox_AxisREnabled.Text = "Enabled";
            this.ux_CheckBox_AxisREnabled.UseVisualStyleBackColor = true;
            // 
            // ux_Numeric_RHomeFrequency
            // 
            this.ux_Numeric_RHomeFrequency.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ux_Numeric_RHomeFrequency.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Numeric_RHomeFrequency.Location = new System.Drawing.Point(151, 46);
            this.ux_Numeric_RHomeFrequency.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.ux_Numeric_RHomeFrequency.Name = "ux_Numeric_RHomeFrequency";
            this.ux_Numeric_RHomeFrequency.Size = new System.Drawing.Size(62, 26);
            this.ux_Numeric_RHomeFrequency.TabIndex = 14;
            this.ux_Numeric_RHomeFrequency.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ux_Label_AxisR
            // 
            this.ux_Label_AxisR.AutoSize = true;
            this.ux_Label_AxisR.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ux_Label_AxisR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Label_AxisR.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Label_AxisR.Location = new System.Drawing.Point(229, 0);
            this.ux_Label_AxisR.Name = "ux_Label_AxisR";
            this.ux_Label_AxisR.Size = new System.Drawing.Size(216, 28);
            this.ux_Label_AxisR.TabIndex = 1;
            this.ux_Label_AxisR.Text = "R";
            this.ux_Label_AxisR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ux_Label_AxisX
            // 
            this.ux_Label_AxisX.AutoSize = true;
            this.ux_Label_AxisX.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ux_Label_AxisX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Label_AxisX.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Label_AxisX.Location = new System.Drawing.Point(451, 0);
            this.ux_Label_AxisX.Name = "ux_Label_AxisX";
            this.ux_Label_AxisX.Size = new System.Drawing.Size(225, 28);
            this.ux_Label_AxisX.TabIndex = 0;
            this.ux_Label_AxisX.Text = "X";
            this.ux_Label_AxisX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ux_Label_AxisZ
            // 
            this.ux_Label_AxisZ.AutoSize = true;
            this.ux_Label_AxisZ.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ux_Label_AxisZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Label_AxisZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Label_AxisZ.Location = new System.Drawing.Point(3, 0);
            this.ux_Label_AxisZ.Name = "ux_Label_AxisZ";
            this.ux_Label_AxisZ.Size = new System.Drawing.Size(220, 28);
            this.ux_Label_AxisZ.TabIndex = 2;
            this.ux_Label_AxisZ.Text = "Z";
            this.ux_Label_AxisZ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ux_Layout_ZAxis
            // 
            this.ux_Layout_ZAxis.ColumnCount = 2;
            this.ux_Layout_ZAxis.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_ZAxis.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ux_Layout_ZAxis.Controls.Add(this.ux_Label_ZHomeAxisEveryN, 0, 2);
            this.ux_Layout_ZAxis.Controls.Add(this.ux_CheckBox_AxisZEnabled, 0, 1);
            this.ux_Layout_ZAxis.Controls.Add(this.ux_Numeric_ZHomeFrequency, 1, 2);
            this.ux_Layout_ZAxis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_ZAxis.Location = new System.Drawing.Point(3, 31);
            this.ux_Layout_ZAxis.Name = "ux_Layout_ZAxis";
            this.ux_Layout_ZAxis.RowCount = 4;
            this.ux_Layout_ZAxis.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.ux_Layout_ZAxis.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_Layout_ZAxis.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_Layout_ZAxis.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ux_Layout_ZAxis.Size = new System.Drawing.Size(220, 295);
            this.ux_Layout_ZAxis.TabIndex = 4;
            // 
            // ux_Label_ZHomeAxisEveryN
            // 
            this.ux_Label_ZHomeAxisEveryN.AutoSize = true;
            this.ux_Label_ZHomeAxisEveryN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Label_ZHomeAxisEveryN.Location = new System.Drawing.Point(3, 43);
            this.ux_Label_ZHomeAxisEveryN.Name = "ux_Label_ZHomeAxisEveryN";
            this.ux_Label_ZHomeAxisEveryN.Size = new System.Drawing.Size(113, 32);
            this.ux_Label_ZHomeAxisEveryN.TabIndex = 13;
            this.ux_Label_ZHomeAxisEveryN.Text = "Home Axis Frequency:";
            this.ux_Label_ZHomeAxisEveryN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ux_CheckBox_AxisZEnabled
            // 
            this.ux_CheckBox_AxisZEnabled.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_CheckBox_AxisZEnabled.AutoSize = true;
            this.ux_Layout_ZAxis.SetColumnSpan(this.ux_CheckBox_AxisZEnabled, 2);
            this.ux_CheckBox_AxisZEnabled.Location = new System.Drawing.Point(77, 23);
            this.ux_CheckBox_AxisZEnabled.Name = "ux_CheckBox_AxisZEnabled";
            this.ux_CheckBox_AxisZEnabled.Size = new System.Drawing.Size(65, 17);
            this.ux_CheckBox_AxisZEnabled.TabIndex = 14;
            this.ux_CheckBox_AxisZEnabled.Text = "Enabled";
            this.ux_CheckBox_AxisZEnabled.UseVisualStyleBackColor = true;
            // 
            // ux_Numeric_ZHomeFrequency
            // 
            this.ux_Numeric_ZHomeFrequency.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ux_Numeric_ZHomeFrequency.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Numeric_ZHomeFrequency.Location = new System.Drawing.Point(155, 46);
            this.ux_Numeric_ZHomeFrequency.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.ux_Numeric_ZHomeFrequency.Name = "ux_Numeric_ZHomeFrequency";
            this.ux_Numeric_ZHomeFrequency.Size = new System.Drawing.Size(62, 26);
            this.ux_Numeric_ZHomeFrequency.TabIndex = 12;
            this.ux_Numeric_ZHomeFrequency.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ux_tlp_LowerAxisSave
            // 
            this.ux_tlp_LowerAxisSave.ColumnCount = 2;
            this.ux_Layout_AxisMain.SetColumnSpan(this.ux_tlp_LowerAxisSave, 3);
            this.ux_tlp_LowerAxisSave.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.20951F));
            this.ux_tlp_LowerAxisSave.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.79049F));
            this.ux_tlp_LowerAxisSave.Controls.Add(this.ux_Check_HomeAxisOnStartup, 0, 0);
            this.ux_tlp_LowerAxisSave.Controls.Add(this.ux_ControlButtonSave_Axis, 1, 1);
            this.ux_tlp_LowerAxisSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_tlp_LowerAxisSave.Location = new System.Drawing.Point(3, 332);
            this.ux_tlp_LowerAxisSave.Name = "ux_tlp_LowerAxisSave";
            this.ux_tlp_LowerAxisSave.RowCount = 2;
            this.ux_tlp_LowerAxisSave.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.34884F));
            this.ux_tlp_LowerAxisSave.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 84.65116F));
            this.ux_tlp_LowerAxisSave.Size = new System.Drawing.Size(673, 150);
            this.ux_tlp_LowerAxisSave.TabIndex = 6;
            // 
            // ux_Check_HomeAxisOnStartup
            // 
            this.ux_Check_HomeAxisOnStartup.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ux_Check_HomeAxisOnStartup.AutoSize = true;
            this.ux_Check_HomeAxisOnStartup.Location = new System.Drawing.Point(3, 3);
            this.ux_Check_HomeAxisOnStartup.Name = "ux_Check_HomeAxisOnStartup";
            this.ux_Check_HomeAxisOnStartup.Size = new System.Drawing.Size(134, 17);
            this.ux_Check_HomeAxisOnStartup.TabIndex = 0;
            this.ux_Check_HomeAxisOnStartup.Text = "Home Axes On Startup";
            this.ux_Check_HomeAxisOnStartup.UseVisualStyleBackColor = true;
            // 
            // ux_ControlButtonSave_Axis
            // 
            this.ux_ControlButtonSave_Axis.Location = new System.Drawing.Point(563, 26);
            this.ux_ControlButtonSave_Axis.MinimumSize = new System.Drawing.Size(75, 100);
            this.ux_ControlButtonSave_Axis.Name = "ux_ControlButtonSave_Axis";
            this.ux_ControlButtonSave_Axis.Size = new System.Drawing.Size(75, 100);
            this.ux_ControlButtonSave_Axis.TabIndex = 1;
            this.ux_ControlButtonSave_Axis.Click += new System.EventHandler(this.AxisSettingsSaveClick);
            // 
            // ux_Tab_Global
            // 
            this.ux_Tab_Global.Controls.Add(this.ux_Layout_GVMain);
            this.ux_Tab_Global.Location = new System.Drawing.Point(4, 4);
            this.ux_Tab_Global.Name = "ux_Tab_Global";
            this.ux_Tab_Global.Padding = new System.Windows.Forms.Padding(3);
            this.ux_Tab_Global.Size = new System.Drawing.Size(685, 491);
            this.ux_Tab_Global.TabIndex = 17;
            this.ux_Tab_Global.Text = "gv";
            this.ux_Tab_Global.UseVisualStyleBackColor = true;
            this.ux_Tab_Global.Enter += new System.EventHandler(this.TabEnter);
            // 
            // ux_Layout_GVMain
            // 
            this.ux_Layout_GVMain.ColumnCount = 1;
            this.ux_Layout_GVMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_Layout_GVMain.Controls.Add(this.ux_Label_HeaderGlobalVariables, 0, 0);
            this.ux_Layout_GVMain.Controls.Add(this.ux_Layout_GlobalVariables, 0, 1);
            this.ux_Layout_GVMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_GVMain.Location = new System.Drawing.Point(3, 3);
            this.ux_Layout_GVMain.Name = "ux_Layout_GVMain";
            this.ux_Layout_GVMain.RowCount = 2;
            this.ux_Layout_GVMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.ux_Layout_GVMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 95F));
            this.ux_Layout_GVMain.Size = new System.Drawing.Size(679, 485);
            this.ux_Layout_GVMain.TabIndex = 0;
            // 
            // ux_Label_HeaderGlobalVariables
            // 
            this.ux_Label_HeaderGlobalVariables.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Label_HeaderGlobalVariables.AutoSize = true;
            this.ux_Label_HeaderGlobalVariables.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Label_HeaderGlobalVariables.Location = new System.Drawing.Point(3, 0);
            this.ux_Label_HeaderGlobalVariables.Name = "ux_Label_HeaderGlobalVariables";
            this.ux_Label_HeaderGlobalVariables.Size = new System.Drawing.Size(673, 24);
            this.ux_Label_HeaderGlobalVariables.TabIndex = 2;
            this.ux_Label_HeaderGlobalVariables.Text = "Global IDs";
            this.ux_Label_HeaderGlobalVariables.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ux_Layout_GlobalVariables
            // 
            this.ux_Layout_GlobalVariables.ColumnCount = 2;
            this.ux_Layout_GlobalVariables.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ux_Layout_GlobalVariables.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ux_Layout_GlobalVariables.Controls.Add(this.ux_Layout_GVCounters, 0, 0);
            this.ux_Layout_GlobalVariables.Controls.Add(this.ux_Layout_GVStrings, 1, 0);
            this.ux_Layout_GlobalVariables.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_GlobalVariables.Location = new System.Drawing.Point(3, 27);
            this.ux_Layout_GlobalVariables.Name = "ux_Layout_GlobalVariables";
            this.ux_Layout_GlobalVariables.RowCount = 1;
            this.ux_Layout_GlobalVariables.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ux_Layout_GlobalVariables.Size = new System.Drawing.Size(673, 455);
            this.ux_Layout_GlobalVariables.TabIndex = 3;
            // 
            // ux_Layout_GVCounters
            // 
            this.ux_Layout_GVCounters.ColumnCount = 2;
            this.ux_Layout_GVCounters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.ux_Layout_GVCounters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.ux_Layout_GVCounters.Controls.Add(this.ux_dgv_GVCounters, 0, 1);
            this.ux_Layout_GVCounters.Controls.Add(this.ux_Layout_GVCounterControls, 1, 1);
            this.ux_Layout_GVCounters.Controls.Add(this.ux_label_GVCounters, 0, 0);
            this.ux_Layout_GVCounters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_GVCounters.Location = new System.Drawing.Point(3, 3);
            this.ux_Layout_GVCounters.Name = "ux_Layout_GVCounters";
            this.ux_Layout_GVCounters.RowCount = 2;
            this.ux_Layout_GVCounters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.ux_Layout_GVCounters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 95F));
            this.ux_Layout_GVCounters.Size = new System.Drawing.Size(330, 449);
            this.ux_Layout_GVCounters.TabIndex = 0;
            // 
            // ux_dgv_GVCounters
            // 
            this.ux_dgv_GVCounters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_dgv_GVCounters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_dgv_GVCounters.Location = new System.Drawing.Point(3, 25);
            this.ux_dgv_GVCounters.Name = "ux_dgv_GVCounters";
            this.ux_dgv_GVCounters.Size = new System.Drawing.Size(208, 421);
            this.ux_dgv_GVCounters.TabIndex = 0;
            this.ux_dgv_GVCounters.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.GvCountersCellValidating);
            // 
            // ux_Layout_GVCounterControls
            // 
            this.ux_Layout_GVCounterControls.ColumnCount = 1;
            this.ux_Layout_GVCounterControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_Layout_GVCounterControls.Controls.Add(this.ux_Button_GVCounterAdd, 0, 0);
            this.ux_Layout_GVCounterControls.Controls.Add(this.ux_Button_GVCounterDel, 0, 1);
            this.ux_Layout_GVCounterControls.Controls.Add(this.ux_Button_GVCounterReset, 0, 2);
            this.ux_Layout_GVCounterControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_GVCounterControls.Location = new System.Drawing.Point(217, 25);
            this.ux_Layout_GVCounterControls.Name = "ux_Layout_GVCounterControls";
            this.ux_Layout_GVCounterControls.RowCount = 4;
            this.ux_Layout_GVCounterControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.ux_Layout_GVCounterControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.ux_Layout_GVCounterControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.ux_Layout_GVCounterControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.ux_Layout_GVCounterControls.Size = new System.Drawing.Size(110, 421);
            this.ux_Layout_GVCounterControls.TabIndex = 1;
            // 
            // ux_Button_GVCounterAdd
            // 
            this.ux_Button_GVCounterAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Button_GVCounterAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Button_GVCounterAdd.Image = ((System.Drawing.Image)(resources.GetObject("ux_Button_GVCounterAdd.Image")));
            this.ux_Button_GVCounterAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ux_Button_GVCounterAdd.Location = new System.Drawing.Point(3, 3);
            this.ux_Button_GVCounterAdd.Name = "ux_Button_GVCounterAdd";
            this.ux_Button_GVCounterAdd.Size = new System.Drawing.Size(104, 36);
            this.ux_Button_GVCounterAdd.TabIndex = 0;
            this.ux_Button_GVCounterAdd.Text = "Add";
            this.ux_Button_GVCounterAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ux_Button_GVCounterAdd.UseVisualStyleBackColor = true;
            this.ux_Button_GVCounterAdd.Click += new System.EventHandler(this.ButtonGvCounterAddClick);
            // 
            // ux_Button_GVCounterDel
            // 
            this.ux_Button_GVCounterDel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Button_GVCounterDel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Button_GVCounterDel.Image = ((System.Drawing.Image)(resources.GetObject("ux_Button_GVCounterDel.Image")));
            this.ux_Button_GVCounterDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ux_Button_GVCounterDel.Location = new System.Drawing.Point(3, 45);
            this.ux_Button_GVCounterDel.Name = "ux_Button_GVCounterDel";
            this.ux_Button_GVCounterDel.Size = new System.Drawing.Size(104, 36);
            this.ux_Button_GVCounterDel.TabIndex = 1;
            this.ux_Button_GVCounterDel.Text = "Delete";
            this.ux_Button_GVCounterDel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ux_Button_GVCounterDel.UseVisualStyleBackColor = true;
            this.ux_Button_GVCounterDel.Click += new System.EventHandler(this.ButtonGvCounterDelClick);
            // 
            // ux_Button_GVCounterReset
            // 
            this.ux_Button_GVCounterReset.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Button_GVCounterReset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Button_GVCounterReset.Image = ((System.Drawing.Image)(resources.GetObject("ux_Button_GVCounterReset.Image")));
            this.ux_Button_GVCounterReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ux_Button_GVCounterReset.Location = new System.Drawing.Point(3, 87);
            this.ux_Button_GVCounterReset.Name = "ux_Button_GVCounterReset";
            this.ux_Button_GVCounterReset.Size = new System.Drawing.Size(104, 36);
            this.ux_Button_GVCounterReset.TabIndex = 2;
            this.ux_Button_GVCounterReset.Text = "Reset";
            this.ux_Button_GVCounterReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ux_Button_GVCounterReset.UseVisualStyleBackColor = true;
            this.ux_Button_GVCounterReset.Click += new System.EventHandler(this.ButtonGvCounterResetClick);
            // 
            // ux_label_GVCounters
            // 
            this.ux_label_GVCounters.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_label_GVCounters.AutoSize = true;
            this.ux_label_GVCounters.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_label_GVCounters.Location = new System.Drawing.Point(3, 1);
            this.ux_label_GVCounters.Name = "ux_label_GVCounters";
            this.ux_label_GVCounters.Size = new System.Drawing.Size(208, 20);
            this.ux_label_GVCounters.TabIndex = 2;
            this.ux_label_GVCounters.Text = "Counters";
            this.ux_label_GVCounters.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ux_Layout_GVStrings
            // 
            this.ux_Layout_GVStrings.ColumnCount = 2;
            this.ux_Layout_GVStrings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.ux_Layout_GVStrings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.ux_Layout_GVStrings.Controls.Add(this.ux_Layout_GVCounterStrings, 1, 1);
            this.ux_Layout_GVStrings.Controls.Add(this.ux_dgv_Strings, 0, 1);
            this.ux_Layout_GVStrings.Controls.Add(this.ux_label_GVStrings, 0, 0);
            this.ux_Layout_GVStrings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_GVStrings.Location = new System.Drawing.Point(339, 3);
            this.ux_Layout_GVStrings.Name = "ux_Layout_GVStrings";
            this.ux_Layout_GVStrings.RowCount = 2;
            this.ux_Layout_GVStrings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.ux_Layout_GVStrings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 95F));
            this.ux_Layout_GVStrings.Size = new System.Drawing.Size(331, 449);
            this.ux_Layout_GVStrings.TabIndex = 1;
            // 
            // ux_Layout_GVCounterStrings
            // 
            this.ux_Layout_GVCounterStrings.ColumnCount = 1;
            this.ux_Layout_GVCounterStrings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_Layout_GVCounterStrings.Controls.Add(this.ux_Button_GVStringAdd, 0, 0);
            this.ux_Layout_GVCounterStrings.Controls.Add(this.ux_Button_GVStringDel, 0, 1);
            this.ux_Layout_GVCounterStrings.Controls.Add(this.ux_Button_GVStringModify, 0, 2);
            this.ux_Layout_GVCounterStrings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Layout_GVCounterStrings.Location = new System.Drawing.Point(218, 25);
            this.ux_Layout_GVCounterStrings.Name = "ux_Layout_GVCounterStrings";
            this.ux_Layout_GVCounterStrings.RowCount = 4;
            this.ux_Layout_GVCounterStrings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.ux_Layout_GVCounterStrings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.ux_Layout_GVCounterStrings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.ux_Layout_GVCounterStrings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.ux_Layout_GVCounterStrings.Size = new System.Drawing.Size(110, 421);
            this.ux_Layout_GVCounterStrings.TabIndex = 2;
            // 
            // ux_Button_GVStringAdd
            // 
            this.ux_Button_GVStringAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Button_GVStringAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Button_GVStringAdd.Image = ((System.Drawing.Image)(resources.GetObject("ux_Button_GVStringAdd.Image")));
            this.ux_Button_GVStringAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ux_Button_GVStringAdd.Location = new System.Drawing.Point(3, 3);
            this.ux_Button_GVStringAdd.Name = "ux_Button_GVStringAdd";
            this.ux_Button_GVStringAdd.Size = new System.Drawing.Size(104, 36);
            this.ux_Button_GVStringAdd.TabIndex = 1;
            this.ux_Button_GVStringAdd.Text = "Add";
            this.ux_Button_GVStringAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ux_Button_GVStringAdd.UseVisualStyleBackColor = true;
            this.ux_Button_GVStringAdd.Click += new System.EventHandler(this.ButtonGvStringAddClick);
            // 
            // ux_Button_GVStringDel
            // 
            this.ux_Button_GVStringDel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Button_GVStringDel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Button_GVStringDel.Image = ((System.Drawing.Image)(resources.GetObject("ux_Button_GVStringDel.Image")));
            this.ux_Button_GVStringDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ux_Button_GVStringDel.Location = new System.Drawing.Point(3, 45);
            this.ux_Button_GVStringDel.Name = "ux_Button_GVStringDel";
            this.ux_Button_GVStringDel.Size = new System.Drawing.Size(104, 36);
            this.ux_Button_GVStringDel.TabIndex = 2;
            this.ux_Button_GVStringDel.Text = "Delete";
            this.ux_Button_GVStringDel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ux_Button_GVStringDel.UseVisualStyleBackColor = true;
            this.ux_Button_GVStringDel.Click += new System.EventHandler(this.ButtonGvStringDelClick);
            // 
            // ux_Button_GVStringModify
            // 
            this.ux_Button_GVStringModify.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Button_GVStringModify.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Button_GVStringModify.Image = ((System.Drawing.Image)(resources.GetObject("ux_Button_GVStringModify.Image")));
            this.ux_Button_GVStringModify.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ux_Button_GVStringModify.Location = new System.Drawing.Point(3, 87);
            this.ux_Button_GVStringModify.Name = "ux_Button_GVStringModify";
            this.ux_Button_GVStringModify.Size = new System.Drawing.Size(104, 36);
            this.ux_Button_GVStringModify.TabIndex = 3;
            this.ux_Button_GVStringModify.Text = "Edit";
            this.ux_Button_GVStringModify.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ux_Button_GVStringModify.UseVisualStyleBackColor = true;
            this.ux_Button_GVStringModify.Visible = false;
            this.ux_Button_GVStringModify.Click += new System.EventHandler(this.ButtonGvStringModifyClick);
            // 
            // ux_dgv_Strings
            // 
            this.ux_dgv_Strings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_dgv_Strings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_dgv_Strings.Location = new System.Drawing.Point(3, 25);
            this.ux_dgv_Strings.Name = "ux_dgv_Strings";
            this.ux_dgv_Strings.Size = new System.Drawing.Size(209, 421);
            this.ux_dgv_Strings.TabIndex = 0;
            this.ux_dgv_Strings.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.StringsCellValidating);
            // 
            // ux_label_GVStrings
            // 
            this.ux_label_GVStrings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_label_GVStrings.AutoSize = true;
            this.ux_label_GVStrings.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_label_GVStrings.Location = new System.Drawing.Point(3, 1);
            this.ux_label_GVStrings.Name = "ux_label_GVStrings";
            this.ux_label_GVStrings.Size = new System.Drawing.Size(209, 20);
            this.ux_label_GVStrings.TabIndex = 3;
            this.ux_label_GVStrings.Text = "Strings";
            this.ux_label_GVStrings.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ux_Tab_Moves
            // 
            this.ux_Tab_Moves.Controls.Add(this.ux_Table_Axes);
            this.ux_Tab_Moves.Location = new System.Drawing.Point(4, 4);
            this.ux_Tab_Moves.Name = "ux_Tab_Moves";
            this.ux_Tab_Moves.Padding = new System.Windows.Forms.Padding(3);
            this.ux_Tab_Moves.Size = new System.Drawing.Size(685, 491);
            this.ux_Tab_Moves.TabIndex = 18;
            this.ux_Tab_Moves.Text = "mv";
            this.ux_Tab_Moves.UseVisualStyleBackColor = true;
            this.ux_Tab_Moves.Enter += new System.EventHandler(this.TabEnter);
            // 
            // ux_Table_Axes
            // 
            this.ux_Table_Axes.ColumnCount = 2;
            this.ux_Table_Axes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.13844F));
            this.ux_Table_Axes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85.86156F));
            this.ux_Table_Axes.Controls.Add(this.ux_ProgressBarR, 1, 1);
            this.ux_Table_Axes.Controls.Add(this.ux_Label_MoveR, 0, 1);
            this.ux_Table_Axes.Controls.Add(this.ux_Label_MoveZ, 0, 0);
            this.ux_Table_Axes.Controls.Add(this.ux_Label_MoveX, 0, 2);
            this.ux_Table_Axes.Controls.Add(this.ux_Label_MoveY, 0, 3);
            this.ux_Table_Axes.Controls.Add(this.ux_ProgressBarZ, 1, 0);
            this.ux_Table_Axes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Table_Axes.Location = new System.Drawing.Point(3, 3);
            this.ux_Table_Axes.Name = "ux_Table_Axes";
            this.ux_Table_Axes.RowCount = 4;
            this.ux_Table_Axes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_Table_Axes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_Table_Axes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_Table_Axes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_Table_Axes.Size = new System.Drawing.Size(679, 485);
            this.ux_Table_Axes.TabIndex = 0;
            // 
            // ux_ProgressBarR
            // 
            this.ux_ProgressBarR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_ProgressBarR.Location = new System.Drawing.Point(99, 124);
            this.ux_ProgressBarR.Name = "ux_ProgressBarR";
            this.ux_ProgressBarR.Size = new System.Drawing.Size(577, 115);
            this.ux_ProgressBarR.TabIndex = 5;
            // 
            // ux_Label_MoveR
            // 
            this.ux_Label_MoveR.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Label_MoveR.AutoSize = true;
            this.ux_Label_MoveR.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Label_MoveR.Location = new System.Drawing.Point(34, 169);
            this.ux_Label_MoveR.Name = "ux_Label_MoveR";
            this.ux_Label_MoveR.Size = new System.Drawing.Size(27, 25);
            this.ux_Label_MoveR.TabIndex = 2;
            this.ux_Label_MoveR.Text = "R";
            // 
            // ux_Label_MoveZ
            // 
            this.ux_Label_MoveZ.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Label_MoveZ.AutoSize = true;
            this.ux_Label_MoveZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Label_MoveZ.Location = new System.Drawing.Point(35, 48);
            this.ux_Label_MoveZ.Name = "ux_Label_MoveZ";
            this.ux_Label_MoveZ.Size = new System.Drawing.Size(25, 25);
            this.ux_Label_MoveZ.TabIndex = 0;
            this.ux_Label_MoveZ.Text = "Z";
            // 
            // ux_Label_MoveX
            // 
            this.ux_Label_MoveX.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Label_MoveX.AutoSize = true;
            this.ux_Label_MoveX.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Label_MoveX.Location = new System.Drawing.Point(35, 290);
            this.ux_Label_MoveX.Name = "ux_Label_MoveX";
            this.ux_Label_MoveX.Size = new System.Drawing.Size(26, 25);
            this.ux_Label_MoveX.TabIndex = 3;
            this.ux_Label_MoveX.Text = "X";
            // 
            // ux_Label_MoveY
            // 
            this.ux_Label_MoveY.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ux_Label_MoveY.AutoSize = true;
            this.ux_Label_MoveY.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Label_MoveY.Location = new System.Drawing.Point(34, 411);
            this.ux_Label_MoveY.Name = "ux_Label_MoveY";
            this.ux_Label_MoveY.Size = new System.Drawing.Size(27, 25);
            this.ux_Label_MoveY.TabIndex = 1;
            this.ux_Label_MoveY.Text = "Y";
            // 
            // ux_ProgressBarZ
            // 
            this.ux_ProgressBarZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_ProgressBarZ.Location = new System.Drawing.Point(99, 3);
            this.ux_ProgressBarZ.Name = "ux_ProgressBarZ";
            this.ux_ProgressBarZ.Size = new System.Drawing.Size(577, 115);
            this.ux_ProgressBarZ.TabIndex = 4;
            // 
            // ux_tab_pg
            // 
            this.ux_tab_pg.Controls.Add(this.ux_pg_inisettings);
            this.ux_tab_pg.Location = new System.Drawing.Point(4, 4);
            this.ux_tab_pg.Name = "ux_tab_pg";
            this.ux_tab_pg.Padding = new System.Windows.Forms.Padding(3);
            this.ux_tab_pg.Size = new System.Drawing.Size(685, 491);
            this.ux_tab_pg.TabIndex = 19;
            this.ux_tab_pg.Text = "tabPage1";
            this.ux_tab_pg.UseVisualStyleBackColor = true;
            // 
            // ux_pg_inisettings
            // 
            this.ux_pg_inisettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_pg_inisettings.Location = new System.Drawing.Point(3, 3);
            this.ux_pg_inisettings.Name = "ux_pg_inisettings";
            this.ux_pg_inisettings.Size = new System.Drawing.Size(679, 485);
            this.ux_pg_inisettings.TabIndex = 0;
            // 
            // ux_Timer_IO
            // 
            this.ux_Timer_IO.Interval = 200;
            this.ux_Timer_IO.Tick += new System.EventHandler(this.TimerIOTick);
            // 
            // ux_Timer_AnimateFileSearch
            // 
            this.ux_Timer_AnimateFileSearch.Interval = 350;
            this.ux_Timer_AnimateFileSearch.Tick += new System.EventHandler(this.TimerAnimateFileSearchTick);
            // 
            // ux_Timer_ReconnectToPLC
            // 
            this.ux_Timer_ReconnectToPLC.Interval = 5000;
            this.ux_Timer_ReconnectToPLC.Tick += new System.EventHandler(this.TimerCheckForPLCDisconnectTick);
            // 
            // ux_fsw_JobList
            // 
            this.ux_fsw_JobList.EnableRaisingEvents = true;
            this.ux_fsw_JobList.SynchronizingObject = this;
            this.ux_fsw_JobList.Created += new System.IO.FileSystemEventHandler(this.JobListOnChange);
            this.ux_fsw_JobList.Deleted += new System.IO.FileSystemEventHandler(this.JobListOnChange);
            this.ux_fsw_JobList.Renamed += new System.IO.RenamedEventHandler(this.JobListOnChange);
            // 
            // ux_Timer_ReconnectToEngine
            // 
            this.ux_Timer_ReconnectToEngine.Interval = 1000;
            this.ux_Timer_ReconnectToEngine.Tick += new System.EventHandler(this.TimerReconnectToEngineTick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 563);
            this.Controls.Add(this.ux_Layout_Main);
            this.Controls.Add(this.ux_StatusStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "Main";
            this.Text = "Icon Interface";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFormClosing);
            this.Load += new System.EventHandler(this.MainLoad);
            this.Shown += new System.EventHandler(this.MainShown);
            this.ResizeEnd += new System.EventHandler(this.MainResizeEnd);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Main_KeyDown);
            this.ux_StatusStrip.ResumeLayout(false);
            this.ux_StatusStrip.PerformLayout();
            this.ux_Layout_MainControls.ResumeLayout(false);
            this.ux_Layout_Main.ResumeLayout(false);
            this.ux_HMI.ResumeLayout(false);
            this.ux_Tab_Main.ResumeLayout(false);
            this.ux_Layout_MainInner.ResumeLayout(false);
            this.ux_Layout_MainInner.PerformLayout();
            this.ux_Layout_JobListNav.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_Nav_JobList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Nav_NextJobScreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Nav_PrevJobScreen)).EndInit();
            this.ux_Layout_ScanInput.ResumeLayout(false);
            this.ux_Layout_ScannerInput.ResumeLayout(false);
            this.ux_Layout_ScannerInput.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_nud_fontHeight)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_nud_FontWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_PreviewPic)).EndInit();
            this.ux_Tab_Color.ResumeLayout(false);
            this.ux_tlp_Theme_Main.ResumeLayout(false);
            this.ux_Layout_Colors.ResumeLayout(false);
            this.ux_Layout_Colors.PerformLayout();
            this.ux_Layout_FullScreenMode.ResumeLayout(false);
            this.ux_Theme_SpellCheck.ResumeLayout(false);
            this.ux_Theme_SpellCheck.PerformLayout();
            this.ux_tlp_BGImage.ResumeLayout(false);
            this.ux_tlp_BGImage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Set_BackgroundImage)).EndInit();
            this.ux_Tab_EventLog.ResumeLayout(false);
            this.ux_Context_EventLog.ResumeLayout(false);
            this.ux_Tab_IO.ResumeLayout(false);
            this.ux_Layout_IO.ResumeLayout(false);
            this.ux_Layout_IO.PerformLayout();
            this.ux_Tab_General.ResumeLayout(false);
            this.ux_Layout_GeneralMain.ResumeLayout(false);
            this.ux_Layout_GeneralMain.PerformLayout();
            this.ux_Tab_Settings.ResumeLayout(false);
            this.ux_flp_Settings.ResumeLayout(false);
            this.ux_Nav_Stop.ResumeLayout(false);
            this.ux_Nav_Stop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Nav_StopPic)).EndInit();
            this.ux_Tab_MarkInProgress.ResumeLayout(false);
            this.ux_Layout_MarkInProgressMain.ResumeLayout(false);
            this.ux_Layout_MarkInProgressMain.PerformLayout();
            this.ux_Tab_Focus.ResumeLayout(false);
            this.ux_Layout_FocusMain.ResumeLayout(false);
            this.ux_Nav_Focus.ResumeLayout(false);
            this.ux_Nav_Focus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Nav_FocusPic)).EndInit();
            this.ux_Layout_FocusSize.ResumeLayout(false);
            this.ux_Layout_FocusSize.PerformLayout();
            this.ux_Tab_DataEdit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_DataGridView)).EndInit();
            this.ux_Tab_PLC.ResumeLayout(false);
            this.ux_PLC_Layout_TagAdd.ResumeLayout(false);
            this.ux_PLC_Layout_TagAdd.PerformLayout();
            this.ux_PLC_Layout_TagWrite.ResumeLayout(false);
            this.ux_PLC_Layout_TagWrite.PerformLayout();
            this.ux_PLC_Layout_TagView.ResumeLayout(false);
            this.ux_PLC_Layout_Config.ResumeLayout(false);
            this.ux_PLC_Layout_Config.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_PLC_ScanTimeSecondary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_PLC_ScanTime)).EndInit();
            this.ux_Tab_FileImport.ResumeLayout(false);
            this.ux_Import_Layout_Main.ResumeLayout(false);
            this.ux_Import_Layout_Main.PerformLayout();
            this.ux_Layout_ImportConfig.ResumeLayout(false);
            this.ux_Layout_ImportConfig.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Import_ReadWait)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Import_PollRate)).EndInit();
            this.ux_flp_ImportSaveControls.ResumeLayout(false);
            this.ux_flp_ImportPaths.ResumeLayout(false);
            this.ux_Tab_Auth.ResumeLayout(false);
            this.ux_Layout_AuthMain.ResumeLayout(false);
            this.ux_Layout_AuthMain.PerformLayout();
            this.ux_Layout_KeyPadKeys.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_KeyClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_KeyOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Auth_Key3)).EndInit();
            this.ux_Tab_About.ResumeLayout(false);
            this.ux_Tab_About.PerformLayout();
            this.ux_Tab_JobSelect.ResumeLayout(false);
            this.ux_Tab_Axis.ResumeLayout(false);
            this.ux_Layout_AxisMain.ResumeLayout(false);
            this.ux_Layout_AxisMain.PerformLayout();
            this.ux_Layout_RAxis.ResumeLayout(false);
            this.ux_Layout_RAxis.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Numeric_RHomeFrequency)).EndInit();
            this.ux_Layout_ZAxis.ResumeLayout(false);
            this.ux_Layout_ZAxis.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Numeric_ZHomeFrequency)).EndInit();
            this.ux_tlp_LowerAxisSave.ResumeLayout(false);
            this.ux_tlp_LowerAxisSave.PerformLayout();
            this.ux_Tab_Global.ResumeLayout(false);
            this.ux_Layout_GVMain.ResumeLayout(false);
            this.ux_Layout_GVMain.PerformLayout();
            this.ux_Layout_GlobalVariables.ResumeLayout(false);
            this.ux_Layout_GVCounters.ResumeLayout(false);
            this.ux_Layout_GVCounters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_dgv_GVCounters)).EndInit();
            this.ux_Layout_GVCounterControls.ResumeLayout(false);
            this.ux_Layout_GVStrings.ResumeLayout(false);
            this.ux_Layout_GVStrings.PerformLayout();
            this.ux_Layout_GVCounterStrings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_dgv_Strings)).EndInit();
            this.ux_Tab_Moves.ResumeLayout(false);
            this.ux_Table_Axes.ResumeLayout(false);
            this.ux_Table_Axes.PerformLayout();
            this.ux_tab_pg.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_fsw_JobList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void TimerShowHomingStatusTick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void TimerRAxisOverTimeTick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void TimerZAxisOverTimeTick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.StatusStrip ux_StatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel ux_Status_Events;
        private System.Windows.Forms.ToolStripStatusLabel ux_Status_Empty;
        private System.Windows.Forms.ToolStripDropDownButton ux_ToolStrip_Activate;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_Settings;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_Minimize;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_Exit;
        private System.Windows.Forms.ToolStripStatusLabel ux_Status_Time;
        private System.Windows.Forms.Timer ux_Timer_ShowTime;
        private System.Windows.Forms.TabPage ux_Tab_EventLog;
        private System.Windows.Forms.TabPage ux_Tab_Color;
        internal System.Windows.Forms.Label ux_Set_BGPicLabel;
        internal System.Windows.Forms.Panel ux_Set_StatusCustom;
        internal System.Windows.Forms.Panel ux_Set_StatusBlack;
        internal System.Windows.Forms.Panel ux_Set_StatusViolet;
        internal System.Windows.Forms.Panel ux_Set_FontCustom;
        internal System.Windows.Forms.Panel ux_Set_StatusLightBlue;
        internal System.Windows.Forms.Panel ux_Set_BGCustom;
        internal System.Windows.Forms.Panel ux_Set_StatusBlue;
        internal System.Windows.Forms.Panel ux_Set_FontBlack;
        internal System.Windows.Forms.Panel ux_Set_StatusGreen;
        internal System.Windows.Forms.Panel ux_Set_BGWhite;
        internal System.Windows.Forms.Panel ux_Set_StatusYellow;
        internal System.Windows.Forms.Panel ux_Set_FontViolet;
        internal System.Windows.Forms.Panel ux_Set_StatusOrange;
        internal System.Windows.Forms.Panel ux_Set_BGViolet;
        internal System.Windows.Forms.Panel ux_Set_StatusRed;
        internal System.Windows.Forms.Label ux_Set_StatusColorLabel;
        internal System.Windows.Forms.Panel ux_Set_FontLightBlue;
        internal System.Windows.Forms.Panel ux_Set_BGLightBlue;
        internal System.Windows.Forms.Panel ux_Set_FontBlue;
        internal System.Windows.Forms.Panel ux_Set_BGBlue;
        internal System.Windows.Forms.Panel ux_Set_FontGreen;
        internal System.Windows.Forms.Panel ux_Set_BGGreen;
        internal System.Windows.Forms.Panel ux_Set_FontYellow;
        internal System.Windows.Forms.Panel ux_Set_FontOrange;
        internal System.Windows.Forms.Panel ux_Set_BGYellow;
        internal System.Windows.Forms.Panel ux_Set_FontRed;
        internal System.Windows.Forms.Panel ux_Set_BGOrange;
        internal System.Windows.Forms.Label ux_Set_FontLabel;
        internal System.Windows.Forms.Panel ux_Set_BGRed;
        internal System.Windows.Forms.Label ux_Set_BGColorLabel;
        internal System.Windows.Forms.PictureBox ux_Set_BackgroundImage;
        private System.Windows.Forms.TabPage ux_Tab_Main;
        private System.Windows.Forms.FlowLayoutPanel ux_JobList;
        private CustomTabControl ux_HMI;
        private System.Windows.Forms.Timer ux_Timer_CardConnection;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_MainControls;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_Main;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_MainInner;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_Card;
        private System.Windows.Forms.TabPage ux_Tab_IO;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_IO;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_Colors;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_FullScreenMode;
        private System.Windows.Forms.CheckBox ux_Set_ToggleFullScreen;
        private System.Windows.Forms.TabPage ux_Tab_General;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_GeneralMain;
        private System.Windows.Forms.TabPage ux_Tab_Settings;
        private System.Windows.Forms.TabPage ux_Tab_MarkInProgress;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_MarkInProgressMain;
        private System.Windows.Forms.Label ux_MarkInProgressLabel;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_JobListNav;
        private System.Windows.Forms.PictureBox ux_Nav_NextJobScreen;
        private System.Windows.Forms.PictureBox ux_Nav_PrevJobScreen;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_ScanInput;
        private System.Windows.Forms.TableLayoutPanel ux_Nav_Stop;
        private System.Windows.Forms.Label ux_Nav_StopLabel;
        private System.Windows.Forms.PictureBox ux_Nav_StopPic;
        private System.Windows.Forms.TabPage ux_Tab_Focus;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_FocusMain;
        private System.Windows.Forms.TableLayoutPanel ux_Nav_Focus;
        private System.Windows.Forms.PictureBox ux_Nav_FocusPic;
        private System.Windows.Forms.Label ux_Nav_FocusLabel;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_FocusSize;
        private System.Windows.Forms.Label ux_Focus_SizeLabel;
        private System.Windows.Forms.HScrollBar ux_FocusSize;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_Focus;
        private System.Windows.Forms.Timer ux_Timer_IO;
        private System.Windows.Forms.TabPage ux_Tab_DataEdit;
        private System.Windows.Forms.DataGridView ux_DataGridView;
        private System.Windows.Forms.PictureBox ux_PreviewPic;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_ScannerInput;
        private System.Windows.Forms.Label ux_IO_InputsLabel;
        private System.Windows.Forms.Label ux_IO_OutputsLabel;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_IO;
        private System.Windows.Forms.TabPage ux_Tab_PLC;
        private System.Windows.Forms.GroupBox ux_PLC_Layout_Config;
        private System.Windows.Forms.Label ux_PLC_IPLabel;
        private System.Windows.Forms.TextBox ux_PLC_IP;
        private System.Windows.Forms.GroupBox ux_PLC_Layout_TagView;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_PLC;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_PLCReconnect;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_PLCDisconnect;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_PLCSettings;
        private System.Windows.Forms.GroupBox ux_PLC_Layout_TagWrite;
        private MyListView ux_PLC_TagView;
        private System.Windows.Forms.TextBox ux_PLC_WriteTagValue;
        private System.Windows.Forms.TextBox ux_PLC_WriteTagName;
        private System.Windows.Forms.Label ux_PLC_TagWriteValueLabel;
        private System.Windows.Forms.Label ux_PLC_TagWriteNameLabel;
        private System.Windows.Forms.Button ux_PLC_WriteTag;
        private System.Windows.Forms.Label ux_PLC_ScanSecondaryLabel;
        private System.Windows.Forms.Label ux_PLC_MainScanLabel;
        private System.Windows.Forms.NumericUpDown ux_PLC_ScanTime;
        private System.Windows.Forms.NumericUpDown ux_PLC_ScanTimeSecondary;
        private System.Windows.Forms.Label ux_PLC_ScanTimeSecondaryMSLabel;
        private System.Windows.Forms.Label ux_PLC_ScanTimeMSLabel;
        private System.Windows.Forms.Button ux_PLC_ChangeIP;
        private System.Windows.Forms.ToolStripStatusLabel ux_Status_PLC;
        private System.Windows.Forms.TabPage ux_Tab_FileImport;
        private System.Windows.Forms.TableLayoutPanel ux_Import_Layout_Main;
        private System.Windows.Forms.Label ux_Import_PathSettingsLabel;
        private System.Windows.Forms.ToolStripStatusLabel ux_Status_Import_State;
        private System.Windows.Forms.Label ux_Set_ImportConfigLabel;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_ImportConfig;
        private System.Windows.Forms.NumericUpDown ux_Import_ReadWait;
        private System.Windows.Forms.Label ux_Import_ReadWaitLabel;
        private System.Windows.Forms.Label ux_Import_FilePollRateLabel;
        private System.Windows.Forms.NumericUpDown ux_Import_PollRate;
        private System.Windows.Forms.Timer ux_Timer_AnimateFileSearch;
        private System.Windows.Forms.CheckBox ux_Import_ToggleJobLoadMode;
        private System.Windows.Forms.Label ux_Import_JobLoadDesc;
        private System.Windows.Forms.CheckBox ux_Import_ToggleFileImport;
        private System.Windows.Forms.Label ux_Import_ImportDesc;
        private System.Windows.Forms.TabPage ux_Tab_Auth;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_AuthMain;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_KeyPadKeys;
        private System.Windows.Forms.PictureBox ux_Auth_Key0;
        private System.Windows.Forms.PictureBox ux_Auth_Key6;
        private System.Windows.Forms.PictureBox ux_Auth_Key5;
        private System.Windows.Forms.PictureBox ux_Auth_Key4;
        private System.Windows.Forms.PictureBox ux_Auth_Key9;
        private System.Windows.Forms.PictureBox ux_Auth_Key8;
        private System.Windows.Forms.PictureBox ux_Auth_Key1;
        private System.Windows.Forms.PictureBox ux_Auth_Key2;
        private System.Windows.Forms.PictureBox ux_Auth_Key3;
        private System.Windows.Forms.PictureBox ux_Auth_Key7;
        private System.Windows.Forms.Label ux_Auth_MaskedKey;
        private System.Windows.Forms.Label ux_Auth_Level;
        private System.Windows.Forms.PictureBox ux_Auth_KeyClear;
        private System.Windows.Forms.PictureBox ux_Auth_KeyOK;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_Access;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_Operator;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_Engineer;
        private System.Windows.Forms.GroupBox ux_PLC_Layout_TagAdd;
        private System.Windows.Forms.Button ux_PLC_AddTag;
        private System.Windows.Forms.TextBox ux_PLC_AddTagName;
        private System.Windows.Forms.Label ux_PLC_AddTagNameLabel;
        private System.Windows.Forms.Timer ux_Timer_ReconnectToPLC;
        private System.Windows.Forms.Label ux_Set_NewPassLabel;
        private System.Windows.Forms.TextBox ux_Set_NewPassword;
        private System.Windows.Forms.TabPage ux_Tab_About;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_About;
        private System.Windows.Forms.TextBox ux_TextBox_About;
        private System.Windows.Forms.TabPage ux_Tab_JobSelect;
        private System.Windows.Forms.TreeView ux_Job_Tree;
        private System.Windows.Forms.PictureBox ux_Nav_JobList;
        private CueTextBox ux_ScannerInput;
        private System.Windows.Forms.Label ux_Label_ElapsedTime;
        private System.Windows.Forms.TabPage ux_Tab_Axis;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_AxisMain;
        private System.Windows.Forms.Label ux_Label_AxisZ;
        private System.Windows.Forms.Label ux_Label_AxisR;
        private System.Windows.Forms.Label ux_Label_AxisX;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_Axis;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_HomeZ;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_ZAxis;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_HomeR;
        private System.Windows.Forms.CheckBox ux_Check_HomeAxisOnStartup;
        private System.Windows.Forms.NumericUpDown ux_Numeric_ZHomeFrequency;
        private System.Windows.Forms.Label ux_Label_ZHomeAxisEveryN;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_RAxis;
        private System.Windows.Forms.Label ux_Label_RHomeAxisEveryN;
        private System.Windows.Forms.NumericUpDown ux_Numeric_RHomeFrequency;
        private System.Windows.Forms.CheckBox ux_CheckBox_AxisREnabled;
        private System.Windows.Forms.CheckBox ux_CheckBox_AxisZEnabled;
        private System.IO.FileSystemWatcher ux_fsw_JobList;
        private System.Windows.Forms.TabPage ux_Tab_Global;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_GVMain;
        private System.Windows.Forms.Label ux_Label_HeaderGlobalVariables;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_GlobalVariables;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_GVCounters;
        private System.Windows.Forms.DataGridView ux_dgv_GVCounters;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_GVStrings;
        private System.Windows.Forms.DataGridView ux_dgv_Strings;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_GVCounterControls;
        private System.Windows.Forms.TableLayoutPanel ux_Layout_GVCounterStrings;
        private System.Windows.Forms.Button ux_Button_GVCounterAdd;
        private System.Windows.Forms.Button ux_Button_GVStringAdd;
        private System.Windows.Forms.Button ux_Button_GVCounterDel;
        private System.Windows.Forms.Button ux_Button_GVStringDel;
        private System.Windows.Forms.Button ux_Button_GVCounterReset;
        private System.Windows.Forms.Button ux_Button_GVStringModify;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_Globals;
        private System.Windows.Forms.Label ux_label_GVCounters;
        private System.Windows.Forms.Label ux_label_GVStrings;
        private System.Windows.Forms.ToolStripStatusLabel ux_Status_SQL;
        private System.Windows.Forms.TableLayoutPanel ux_Theme_SpellCheck;
        private System.Windows.Forms.Label ux_Label_Language;
        private System.Windows.Forms.ComboBox ux_cbs_LanguageSelect;
        private System.Windows.Forms.TableLayoutPanel ux_tlp_Theme_Main;
        private System.Windows.Forms.ToolStripStatusLabel ux_StatusLabel_Connecting;
        private System.Windows.Forms.TableLayoutPanel ux_tlp_BGImage;
        private System.Windows.Forms.CheckBox ux_CheckBox_HideJobList;
        private System.Windows.Forms.ListView ux_EventLog;
        private System.Windows.Forms.ContextMenuStrip ux_Context_EventLog;
        private System.Windows.Forms.ToolStripMenuItem ux_Context_EventLogCopy;
        private System.Windows.Forms.ToolStripMenuItem ux_Context_EventClear;
        private System.Windows.Forms.Timer ux_Timer_ReconnectToEngine;
        private System.Windows.Forms.TabPage ux_Tab_Moves;
        private System.Windows.Forms.TableLayoutPanel ux_Table_Axes;
        private System.Windows.Forms.Label ux_Label_MoveR;
        private System.Windows.Forms.Label ux_Label_MoveZ;
        private System.Windows.Forms.Label ux_Label_MoveX;
        private System.Windows.Forms.Label ux_Label_MoveY;
        private System.Windows.Forms.ProgressBar ux_ProgressBarR;
        private System.Windows.Forms.ProgressBar ux_ProgressBarZ;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_GFXReplacer;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_GFXiStart;
        private System.Windows.Forms.ToolStripMenuItem ux_ToolStrip_GFXiStop;
        private System.Windows.Forms.ToolStripStatusLabel ux_Status_Label_Shutter;
        private System.Windows.Forms.TableLayoutPanel ux_tlp_LowerAxisSave;
        private ControlButtonSave ux_ControlButtonSave_General;
        private ControlButtonSave ux_ControlButtonSave_Paths;
        private ControlButtonSave ux_ControlButtonSave_Theme;
        private ControlButtonSave ux_ControlButtonSave_Axis;
        private ControlButtonStart ux_ControlButton_Start;
        private ControlButtonLimits ux_ControlButton_Limits;
        private ControlButtonHome ux_ControlButton_Home;
        private ControlButtonEdit ux_ControlButton_Edit;
        private ControlButtonClear ux_ControlButton_Clear;
        private ControlQuantity ux_Control_Quantity;
        private ControlVerboseCheckBox ux_Check_PervasiveLimitsEnabled;
        private ControlVerboseCheckBox ux_Check_InstructionsEnabled;
        private ControlVerboseCheckBox ux_Check_LoadLastJobEnabled;
        private ControlVerboseCheckBox ux_Check_AccessControlEnabled;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Input1;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Input16;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Input15;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Input14;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Input13;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Input12;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Input11;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Input10;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Input9;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Input8;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Input7;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Input6;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Input5;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Input4;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Input3;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Input2;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Output16;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Output15;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Output14;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Output13;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Output12;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Output11;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Output10;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Output9;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Output8;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Output7;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Output6;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Output5;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Output4;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Output3;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Output2;
        private Custom_Controls.IOLeds.ControlIOLEDBase ux_LED_Output1;
        private ControlPathSelect ux_Control_InstallFolder;
        private ControlPathSelect ux_Control_ProjectsPath;
        private ControlPathSelect ux_Control_ImagesPath;
        private ControlPathSelect ux_Control_RTFPath;
        private System.Windows.Forms.FlowLayoutPanel ux_flp_ImportPaths;
        private ControlPathSelect ux_Control_ImportPath;
        private ControlPathSelect ux_Control_CompletePath;
        private ControlPathSelect ux_Control_ErrorPath;
        private ControlButtonTheme Control_Button_Theme;
        private ControlButtonGeneral ux_Control_ButtonGeneral;
        private ControlButtonImport ux_Control_SettingImport;
        private ControlButtonAxis ux_Control_ButtonAxisSetting;
        private ControlButtonPLC ux_Control_PLCSetting;
        private System.Windows.Forms.FlowLayoutPanel ux_flp_Settings;
        private ControlButtonBack ux_Button_Back_Generals;
        private System.Windows.Forms.FlowLayoutPanel ux_flp_ImportSaveControls;
        private ControlButtonBack ux_Button_BackFromImportPaths;
        private ControlButtonBack ux_Button_BackColors;
        private System.Windows.Forms.ComboBox ux_cb_Plc_Tag_Type;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ux_cb_plc_tag_add_type;
        private System.Windows.Forms.Label label2;
        private CueTextBox ux_Text_StartingSerial;
        private System.Windows.Forms.TabPage ux_tab_pg;
        private System.Windows.Forms.PropertyGrid ux_pg_inisettings;
        private ControlButtonAllSettings controlButtonAllSettings1;
        private System.Windows.Forms.Label ux_Label_MarkingObject;
        private System.Windows.Forms.ToolStripStatusLabel ux_Status_InnerOrOuterRing;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown ux_nud_FontWidth;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown ux_nud_fontHeight;
        private System.Windows.Forms.CheckBox ux_cb_lock;
    }
}
