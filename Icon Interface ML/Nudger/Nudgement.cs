﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Nudgement.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   A class for holding information about a entity nudge request.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Nudger
{
    /// <summary>
    /// A class for holding information about a entity nudge request.
    /// </summary>
    internal class Nudgement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Nudgement"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="nudgedistance">The nudge distance.</param>
        internal Nudgement(NudgeType type, double nudgedistance)
        {
            this.NudgeDirection = type;
            this.NudgeDistance = nudgedistance;
        }

        /// <summary>
        /// Whether this is a rotational or position nudge.
        /// </summary>
        internal enum NudgeType
        {
            /// <summary>
            /// Move right.
            /// </summary>
            Right,

            /// <summary>
            /// Move left
            /// </summary>
            Left,

            /// <summary>
            /// Move Down
            /// </summary>
            Down,

            /// <summary>
            /// Move Up
            /// </summary>
            Up,

            /// <summary>
            /// Move clock wise
            /// </summary>
            ClockWise,

            /// <summary>
            /// Move counter clockwise
            /// </summary>
            CounterClockwise,
        }

        /// <summary>
        /// Gets the nudge direction.
        /// </summary>
        /// <value>
        /// The nudge direction.
        /// </value>
        internal NudgeType NudgeDirection { get; private set; }

        /// <summary>
        /// Gets or sets the nudge distance.
        /// </summary>
        /// <value>
        /// The nudge distance.
        /// </value>
        internal double NudgeDistance { get; set; }
    }
}
