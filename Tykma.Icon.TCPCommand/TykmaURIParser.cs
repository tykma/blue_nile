﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TykmaURIParser.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Parser.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.TCPCommand
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Parser for a custom URI.
    /// </summary>
    public static class TykmaUriParser
    {
        /// <summary>
        /// Decodes the query parameters.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <returns>Parsed dictionary.</returns>
        public static Dictionary<string, string> DecodeQueryParameters(string uri)
        {
            Dictionary<string, string> returnDict = new Dictionary<string, string>();

            if (uri == null)
            {
                return returnDict;
            }

            // We will get something similar to axis_coor?axis=0  First, let's split everything by question mark.
            // We can also get get_axis?axis=0&value=min
            var parts = uri.Split('?', '&');

            // If a part contains an = sign, then it is a kvp pair.
            foreach (var item in parts)
            {
                if (item.Contains('='))
                {
                    var splitItem = item.Split('=');
                    if (!string.IsNullOrEmpty(splitItem[0]))
                    {
                        if (!returnDict.ContainsKey(splitItem[0].ToLower()))
                        {
                            returnDict.Add(splitItem[0].ToLower(), splitItem[1].Trim());
                        }
                    }
                }
            }

            return returnDict;
        }
    }
}
