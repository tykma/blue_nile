﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlobalVariables.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.Globals
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Serialization;

    using Tykma.Core.StringExtensions;

    /// <summary>
    /// Global variables storage/retrieval from xml file.
    /// </summary>
    public class GlobalVariables : IDisposable
    {
        /// <summary>
        /// The list of global variables.
        /// </summary>
        private readonly List<GlobalVariableItem> listOfGlobalVariables;

        /// <summary>
        /// The file operations class.
        /// </summary>
        private readonly FileInfo globalVariableFile;

        /// <summary>
        /// Initializes a new instance of the <see cref="GlobalVariables" /> class.
        /// </summary>
        /// <param name="globalFile">The global file.</param>
        public GlobalVariables(FileInfo globalFile)
        {
            this.listOfGlobalVariables = new List<GlobalVariableItem>();

            this.globalVariableFile = globalFile;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.SaveGlobalVariableData();
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Adds the global value to our dataset.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="value">The value.</param>
        /// <param name="type">The type.</param>
        public void AddGlobalVariable(string id, string value, EGlobalTypes.GlobalVariableType type)
        {
            GlobalVariableItem newItem = null;

            if (type == EGlobalTypes.GlobalVariableType.Counter_Integer)
            {
                long result;

                newItem = long.TryParse(value, out result) ? new GlobalVariableItem { Counter = true, Name = id, Value = value } : new GlobalVariableItem { Counter = true, Name = id, Value = "0" };
            }
            else if (type == EGlobalTypes.GlobalVariableType.Counter_Alpha)
            {
                newItem = new GlobalVariableItem { Counter = true, AlphaNumeric = true, Name = id, Value = value };
            }
            else if (type == EGlobalTypes.GlobalVariableType.String)
            {
                newItem = new GlobalVariableItem { Counter = false, AlphaNumeric = false, Name = id, Value = value };
            }

            // ReSharper disable once LoopCanBeConvertedToQuery
            if (newItem != null)
            {
                bool itemExists = false;

                foreach (GlobalVariableItem existingItem in this.listOfGlobalVariables)
                {
                    if (existingItem.Name == newItem.Name)
                    {
                        if (existingItem.Counter == newItem.Counter)
                        {
                            itemExists = true;
                            break;
                        }
                    }
                }

                if (!itemExists)
                {
                    this.listOfGlobalVariables.Add(newItem);
                    this.SaveGlobalVariableData();
                }
            }
        }

        /// <summary>
        /// Deletes global variables.
        /// </summary>
        /// <param name="deleteList">
        /// The delete list.
        /// </param>
        /// <param name="counter">
        /// if set to <c>true</c> [counter].
        /// </param>
        public void DeleteVariables(List<string> deleteList, bool counter)
        {
            foreach (GlobalVariableItem item in this.listOfGlobalVariables.ToList())
            {
                if (deleteList.Contains(item.Name) && (item.Counter == counter))
                {
                    this.listOfGlobalVariables.Remove(item);
                }
            }

            this.SaveGlobalVariableData();
        }

        /// <summary>
        /// Resets the counters.
        /// </summary>
        /// <param name="resetList">
        /// The reset list.
        /// </param>
        public void ResetCounters(IEnumerable<string> resetList)
        {
            if (resetList == null)
            {
                return;
            }

            foreach (var gvitem in resetList.SelectMany(itemToReset => this.listOfGlobalVariables.Where(x => x.Name == itemToReset).Where(x => x.Counter)))
            {
                gvitem.Value = gvitem.AlphaNumeric ? "1" : "0".PadLeft(gvitem.Value.Length, '0');
            }

            this.SaveGlobalVariableData();
        }

        /// <summary>
        /// Increments the used counters.
        /// </summary>
        /// <param name="usedList">
        /// The used list.
        /// </param>
        public void IncrementUsedCounters(List<string> usedList)
        {
            bool counterIncremented = false;
            foreach (GlobalVariableItem item in this.listOfGlobalVariables.ToList())
            {
                if (item.Counter)
                {
                    if (usedList.Contains(item.Name))
                    {
                        counterIncremented = true;
                        item.Increment();
                    }
                }
            }

            if (counterIncremented)
            {
                this.SaveGlobalVariableData();
            }
        }

        /// <summary>
        /// Increments used array counters.
        /// </summary>
        /// <param name="usedList">The used list.</param>
        public void IncrementUsedArrayCounters(List<string> usedList)
        {
            bool counterIncremented = false;
            foreach (var item in usedList)
            {
                GlobalVariableItem globalvariablecounter = null;

                foreach (var gvItem in this.listOfGlobalVariables.Where(gvItem => gvItem.Name == item).Where(gvItem => gvItem.Counter))
                {
                    globalvariablecounter = gvItem;
                }

                if (globalvariablecounter != null)
                {
                    counterIncremented = true;
                    globalvariablecounter.Increment();
                }
            }

            if (counterIncremented)
            {
                this.SaveGlobalVariableData();
            }
        }

        /// <summary>
        /// Modifies the counter.
        /// </summary>
        /// <param name="counterIdentifier">The counter identifier.</param>
        /// <param name="newCounterValue">The new counter value.</param>
        /// <returns>
        /// Success state.
        /// </returns>
        public bool ModifyCounter(string counterIdentifier, string newCounterValue)
        {
            if (!newCounterValue.IsNumeric())
            {
                return false;
            }

            foreach (GlobalVariableItem item in this.listOfGlobalVariables.ToList())
            {
                if (item.Counter &&
                    string.Compare(item.Name, counterIdentifier, StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    item.Value = newCounterValue;
                    this.SaveGlobalVariableData();
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Modifies the string.
        /// </summary>
        /// <param name="stringIdentifier">The string identifier.</param>
        /// <param name="newStringValue">The new string value.</param>
        /// <returns>
        /// Success state.
        /// </returns>
        public bool ModifyString(string stringIdentifier, string newStringValue)
        {
            foreach (GlobalVariableItem item in this.listOfGlobalVariables.ToList())
            {
                if (!item.Counter &&
                    string.Compare(item.Name, stringIdentifier, StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    item.Value = newStringValue;
                    this.SaveGlobalVariableData();
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets all counters.
        /// </summary>
        /// <returns>A list with key value pairs containing the counters.</returns>
        public IEnumerable<GlobalVariableItem> GetAllCounters()
        {
            return this.listOfGlobalVariables.Where(item => item.Counter).ToList();
        }

        /// <summary>
        /// Gets all strings.
        /// </summary>
        /// <returns>A list with key value pairs containing the strings.</returns>
        public IEnumerable<GlobalVariableItem> GetAllStrings()
        {
            return this.listOfGlobalVariables.Where(item => !item.Counter).ToList();
        }

        /// <summary>
        /// Loads the global variable data.
        /// </summary>
        public void LoadGlobalVariableData()
        {
            this.listOfGlobalVariables.Clear();

            if (this.globalVariableFile.Exists)
            {
                using (var fs = new StreamReader(this.globalVariableFile.FullName))
                {
                    XmlReader reader = new XmlTextReader(fs);
                    var xmlserializer = new XmlSerializer(typeof(List<GlobalVariableItem>));
                    var other = (List<GlobalVariableItem>)xmlserializer.Deserialize(reader);
                    this.listOfGlobalVariables.AddRange(other);
                }
            }
            else
            {
                throw new FileNotFoundException("Globals file does not exist");
            }
        }

        /// <summary>
        /// Gets the global variable.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>A global variable item.</returns>
        public GlobalVariableItem GetGlobalVariable(string id)
        {
            foreach (var gv in this.listOfGlobalVariables)
            {
                if (string.Compare(gv.Name, id, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    return gv;
                }
            }

            return new GlobalVariableItem();
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        /// <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Dispose managed resources
            }
        }

        /// <summary>
        /// Saves the global variable data.
        /// </summary>
        private void SaveGlobalVariableData()
        {
            try
            {
                using (var writer = new StreamWriter(this.globalVariableFile.FullName))
                {
                    var xmlserializer = new XmlSerializer(typeof(List<GlobalVariableItem>));
                    xmlserializer.Serialize(writer, this.listOfGlobalVariables);
                    writer.Flush();
                }
            }

            // ReSharper disable once EmptyGeneralCatchClause
            catch (Exception)
            {
                // We will just silently ignore this for now.
                // If we can't save our global variable file
                // it means we also probably can't save our config file
                // and the software will not function correctly anyway.
                // If it's some transient/lock issue
                // then save will be ran on the next increment anyway.
            }
        }
    }
}
