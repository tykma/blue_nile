﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlPathSelect.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Windows.Forms;

    /// <summary>
    /// The control path select.
    /// </summary>
    public partial class ControlPathSelect : UserControl
    {
        /// <summary>
        /// The selected path.
        /// </summary>
        private DirectoryInfo selectedPath;

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlPathSelect"/> class.
        /// </summary>
        public ControlPathSelect()
        {
            this.InitializeComponent();
            this.ux_Label_Path.Text = this.SelectedPath;
        }

        /// <summary>
        /// Gets or sets the path label.
        /// </summary>
        /// <value>
        /// The path label.
        /// </value>
        [Category("My Properties")]
        [Description("Path Label")]
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public string PathLabel
        {
            get
            {
                return this.ux_Label_Path.Text;
            }

            set
            {
                this.ux_Label_Path.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets the selected path.
        /// </summary>
        /// <value>
        /// The selected path.
        /// </value>
        public string SelectedPath
        {
            get
            {
                if (this.selectedPath != null)
                {
                    return this.selectedPath.FullName;
                }

                return string.Empty;
            }

            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    try
                    {
                        this.selectedPath = new DirectoryInfo(value);
                        this.OnPropertyChanged(nameof(this.SelectedPath));
                    }
                    catch (NotSupportedException)
                    {
                        // Do nothing.
                    }
                }
            }
        }

        /// <summary>
        /// Called when [property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        private void OnPropertyChanged(string propertyName)
        {
            if (propertyName == nameof(this.SelectedPath))
            {
                this.ux_Text_PathInput.Text = this.SelectedPath;
            }
        }

        /// <summary>
        /// Choose folder button click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void UxButtonChooseFolderClick(object sender, System.EventArgs e)
        {
            var openDir = new FolderBrowserDialog();

            if (openDir.ShowDialog() == DialogResult.OK)
            {
                if (!string.IsNullOrWhiteSpace(openDir.SelectedPath))
                {
                    this.SelectedPath = openDir.SelectedPath;
                }
            }
        }

        /// <summary>
        /// Selected path changed event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void TextChangedEvent(object sender, System.EventArgs e)
        {
            this.SelectedPath = this.ux_Text_PathInput.Text;
        }
    }
}
