﻿// <copyright file="IPLCSettings.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Icon_Interface.File_Management
{
    using Config.Net;

    /// <summary>
    /// PLC Settings class.
    /// </summary>
    public interface IPLCSettings
    {
        /// <summary>
        /// Gets or sets a value indicating whether or not PLC connectivity is enabled.
        /// </summary>
        [Option(Alias = "PLCENABLED", DefaultValue = "False")]
        bool PLCENABLED { get; set; }

        /// <summary>
        /// Gets or sets the IP Address of the PLC.
        /// </summary>
        [Option(Alias = "PLCIP", DefaultValue = "192.168.1.100")]
        string PLCIP { get; set; }

        /// <summary>
        /// Gets or sets the PLC Main Group Scan Time.
        /// </summary>
        [Option(Alias = "PLC_MAIN_SCANTIME", DefaultValue = "200")]
        int PLCMAINSCANTIME { get; set; }

        /// <summary>
        /// Gets or sets the secondary group scan time.
        /// </summary>
        [Option(Alias = "PLC_SECOND_SCANTIME", DefaultValue = "1000")]
        int PLCSECONDARYSCANTIME { get; set; }

        /// <summary>
        /// Gets or sets the connection timeout.
        /// </summary>
        [Option(Alias = "PLC_CONNECTION_TIMEOUT", DefaultValue = "3000")]
        int PLCCONNECTIONTIMEOUT { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not we'll read data tags.
        /// </summary>
        [Option(Alias = "PLC_ENABLE_DATATAGS", DefaultValue = "True")]
        bool PLCENABLEDATATAGS { get; set; }

        /// <summary>
        /// Gets or sets the PLC start tag name.
        /// </summary>
        [Option(Alias = "PLC_TAG_START", DefaultValue = "B3:0/0")]
        string PLCTAGSTART { get; set; }

        /// <summary>
        /// Gets or sets the PLC data is ready tag name.
        /// </summary>
        [Option(Alias = "PLC_DATA_READY_TAG", DefaultValue = "Data_Ready_Tag")]
        string PLCDATAREADYTAG { get; set; }

        /// <summary>
        /// Gets or sets thet he PLC heartbeat tag name.
        /// </summary>
        [Option(Alias = "PLC_HEARTBEAT_TAG", DefaultValue = "Heart_Beat_Tag")]
        string PLCHEARTBEATTAG { get; set; }

        /// <summary>
        /// Gets or sets the start tag name.
        /// </summary>
        [Option(Alias = "PLC_STATUS_TAG", DefaultValue = "Status_Tag")]
        string PLCTAGSTATUS { get; set; }

        /// <summary>
        /// Gets or sets the PLC type.
        /// </summary>
        [Option(Alias = "PLC_TYPE", DefaultValue = "1")]
        int PLCTYPE { get; set; }

        /// <summary>
        /// Gets or sets the name of data tag 1.
        /// </summary>
        [Option(Alias = "DATA_TAG_1_NAME", DefaultValue = "DataTag1")]
        string DATATAG1NAME { get; set; }

        /// <summary>
        /// Gets or sets the name of data tag 1 echo.
        /// </summary>
        [Option(Alias = "DATA_TAG_1_ECHO_NAME", DefaultValue = "DataTag1_Echo")]
        string DATATAG1ECHONAME { get; set; }

        /// <summary>
        /// Gets or sets the project ID mapping of data tag 1.
        /// </summary>
        [Option(Alias = "DATA_TAG_1_PROJECT_ID", DefaultValue = "ID1")]
        string DATATAG1PROJECTID { get; set; }

        /// <summary>
        /// Gets or sets the name of data tag 2.
        /// </summary>
        [Option(Alias = "DATA_TAG_2_NAME", DefaultValue = "DataTag2")]
        string DATATAG2NAME { get; set; }

        /// <summary>
        /// Gets or sets the project ID mapping of data tag 2.
        /// </summary>
        [Option(Alias = "DATA_TAG_2_PROJECT_ID", DefaultValue = "ID2")]
        string DATATAG2PROJECTID { get; set; }

        /// <summary>
        /// Gets or sets the name of data tag 2 echo.
        /// </summary>
        [Option(Alias = "DATA_TAG_2_ECHO_NAME", DefaultValue = "DataTag2_Echo")]
        string DATATAG2ECHONAME { get; set; }

        /// <summary>
        /// Gets or sets the name of data tag 3.
        /// </summary>
        [Option(Alias = "DATA_TAG_3_NAME", DefaultValue = "DataTag3")]
        string DATATAG3NAME { get; set; }

        /// <summary>
        /// Gets or sets the project ID mapping of data tag 3.
        /// </summary>
        [Option(Alias = "DATA_TAG_3_PROJECT_ID", DefaultValue = "ID3")]
        string DATATAG3PROJECTID { get; set; }

        /// <summary>
        /// Gets or sets the name of data tag 3 echo.
        /// </summary>
        [Option(Alias = "DATA_TAG_3_ECHO_NAME", DefaultValue = "DataTag3_Echo")]
        string DATATAG3ECHONAME { get; set; }

        /// <summary>
        /// Gets or sets the name of the template tag.
        /// </summary>
        [Option(Alias = "TEMPLATE_TAG_NAME", DefaultValue = "TemplateTag")]
        string TEMPLATE_TAG { get; set; }

        /// <summary>
        /// Gets or sets the name of the template tag echo name.
        /// </summary>
        [Option(Alias = "TEMPLATETAG_ECHO_NAME", DefaultValue = "TemplateTagECho")]
        string TEMPLATE_ECHO_TAG { get; set; }
    }
}
