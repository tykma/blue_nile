﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlobalsTest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.Globals
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Icon.Globals;

    /// <summary>
    /// The global variables test class.
    /// </summary>
    [TestClass]
    public class GlobalsTest
    {
        /// <summary>
        /// The test data without initializing file.
        /// </summary>
        [TestMethod]
        public void TestDataWithoutInitializingFile()
        {
            var file = new FileInfo("C:/temp/blah.xml");

            var gv = new GlobalVariables(file);

            Assert.AreEqual(0, gv.GetAllCounters().Count());
            Assert.AreEqual(0, gv.GetAllStrings().Count());
        }

        /// <summary>
        /// Tests the loading empty file.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        [ExcludeFromCodeCoverage]
        public void TestLoadinNonExistentFile()
        {
            var file = new FileInfo("C:/temp/blah.xml");
            file.Delete();

            var gv = new GlobalVariables(file);
            gv.LoadGlobalVariableData();
        }

        /// <summary>
        /// Tests loading corrupt file.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        [ExcludeFromCodeCoverage]
        public void TestLoadingCorruptFile()
        {
            var file = new FileInfo("C:/temp/blah.xml");
            using (StreamWriter writer = new StreamWriter(file.FullName))
            {
                writer.WriteLine("COrrupt xml file");
                writer.WriteLine("<>");
            }

            var gv = new GlobalVariables(file);
            gv.LoadGlobalVariableData();
        }

        /// <summary>
        /// Tests loading empty file.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        [ExcludeFromCodeCoverage]
        public void TestLoadingEmptyFile()
        {
            var file = new FileInfo("C:/temp/blah.xml");
            file.Delete();
            using (StreamWriter writer = new StreamWriter(file.FullName))
            {
                writer.WriteLine();
            }

            var gv = new GlobalVariables(file);
            gv.LoadGlobalVariableData();
        }

        /// <summary>
        /// Invalid counter adding test.
        /// </summary>
        [TestMethod]
        public void InvalidCounterTest()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("ID1", "abc123", EGlobalTypes.GlobalVariableType.Counter_Integer);

            Assert.AreEqual(0, gv.GetAllStrings().Count());
            Assert.AreEqual(1, gv.GetAllCounters().Count());
        }

        /// <summary>
        /// Tests the adding duplicate variable.
        /// </summary>
        [TestMethod]
        public void TestAddingDuplicateVariable()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("ID1", "00001", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID1", "00002", EGlobalTypes.GlobalVariableType.Counter_Integer);
            Assert.AreEqual("00001", gv.GetGlobalVariable("ID1").Value);
        }

        /// <summary>
        /// Tests adding valid global variables.
        /// </summary>
        [TestMethod]
        public void ValidCounterTest()
        {
            var gv = GlobalHelpers.GetGlobalVariables();
            gv.AddGlobalVariable("ID1", "00001", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID2", "12345678901234", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID3", "0", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID4", "55", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID5", "1001", EGlobalTypes.GlobalVariableType.Counter_Integer);

            Assert.AreEqual("00001", gv.GetGlobalVariable("ID1").Value);
            Assert.AreEqual("12345678901234", gv.GetGlobalVariable("ID2").Value);
            Assert.AreEqual("0", gv.GetGlobalVariable("ID3").Value);
            Assert.AreEqual("55", gv.GetGlobalVariable("ID4").Value);
            Assert.AreEqual("1001", gv.GetGlobalVariable("ID5").Value);

            Assert.AreEqual(0, gv.GetAllStrings().Count());
        }

        /// <summary>
        /// Counters the has been set test.
        /// </summary>
        [TestMethod]
        public void CounterHasBeenSetTest()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("ID1", "00001", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID2", "12345678901234", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID3", "0", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID4", "55", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID5", "1001", EGlobalTypes.GlobalVariableType.Counter_Integer);

            gv.IncrementUsedCounters(new List<string> { "ID1", "ID2", "ID5" });

            Assert.IsTrue(gv.GetGlobalVariable("ID1").HasBeenSet);
            Assert.IsTrue(gv.GetGlobalVariable("ID2").HasBeenSet);
            Assert.IsTrue(gv.GetGlobalVariable("ID3").HasBeenSet);

            Assert.IsTrue(gv.GetGlobalVariable("ID4").HasBeenSet);
            Assert.IsTrue(gv.GetGlobalVariable("ID5").HasBeenSet);

            Assert.AreEqual(0, gv.GetAllStrings().Count());
        }

        /// <summary>
        /// Tests incrementing valid global variables.
        /// </summary>
        [TestMethod]
        public void ValidCounterIncrementTest()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("ID1", "00001", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID2", "12345678901234", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID3", "0", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID4", "55", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID5", "1001", EGlobalTypes.GlobalVariableType.Counter_Integer);

            gv.IncrementUsedCounters(new List<string> { "ID1", "ID2", "ID5" });

            Assert.AreEqual("00002", gv.GetGlobalVariable("ID1").Value);
            Assert.AreEqual("12345678901235", gv.GetGlobalVariable("ID2").Value);
            Assert.AreEqual("0", gv.GetGlobalVariable("ID3").Value);
            Assert.AreEqual("55", gv.GetGlobalVariable("ID4").Value);
            Assert.AreEqual("1002", gv.GetGlobalVariable("ID5").Value);

            Assert.AreEqual(0, gv.GetAllStrings().Count());
        }

        /// <summary>
        /// Tests the saving and reloading.
        /// </summary>
        [TestMethod]
        public void TestSavingAndReloading()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("ID1", "00001", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID2", "12345678901234", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID3", "0", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID4", "55", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID5", "1001", EGlobalTypes.GlobalVariableType.Counter_Integer);

            gv.AddGlobalVariable("string1", "abc123", EGlobalTypes.GlobalVariableType.String);
            gv.AddGlobalVariable("string2", "qwerty", EGlobalTypes.GlobalVariableType.String);
            gv.AddGlobalVariable("string3", "serial", EGlobalTypes.GlobalVariableType.String);
            gv.AddGlobalVariable("string4", "ca-AZ", EGlobalTypes.GlobalVariableType.String);
            gv.AddGlobalVariable("string5", "test1", EGlobalTypes.GlobalVariableType.String);

            gv.Dispose();

            var gv2 = new GlobalVariables(new FileInfo("C:/temp/blah.xml"));
            gv2.LoadGlobalVariableData();

            Assert.AreEqual("00001", gv2.GetGlobalVariable("ID1").Value);
            Assert.AreEqual("12345678901234", gv2.GetGlobalVariable("ID2").Value);
            Assert.AreEqual("0", gv2.GetGlobalVariable("ID3").Value);
            Assert.AreEqual("55", gv2.GetGlobalVariable("ID4").Value);
            Assert.AreEqual("1001", gv2.GetGlobalVariable("ID5").Value);

            Assert.AreEqual("abc123", gv2.GetGlobalVariable("string1").Value);
            Assert.AreEqual("qwerty", gv2.GetGlobalVariable("string2").Value);
            Assert.AreEqual("serial", gv2.GetGlobalVariable("string3").Value);
            Assert.AreEqual("ca-AZ", gv2.GetGlobalVariable("string4").Value);
            Assert.AreEqual("test1", gv2.GetGlobalVariable("string5").Value);

            gv2.Dispose();
        }

        /// <summary>
        /// Tests the duplicate strings.
        /// </summary>
        [TestMethod]
        public void TestDuplicateStrings()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("string1", "value1", EGlobalTypes.GlobalVariableType.String);
            gv.AddGlobalVariable("string1", "value2", EGlobalTypes.GlobalVariableType.String);

            Assert.AreEqual("value1", gv.GetGlobalVariable("string1").Value);
        }

        /// <summary>
        /// Tests the resetting empty counter list.
        /// </summary>
        [TestMethod]
        public void TestResettingEmptyCounterList()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.ResetCounters(new List<string>());
        }

        /// <summary>
        /// Tests  resetting null list.
        /// </summary>
        [TestMethod]
        public void TestResettingNullList()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("ID1", "00001", EGlobalTypes.GlobalVariableType.Counter_Integer);

            gv.ResetCounters(null);

            Assert.AreEqual("00001", gv.GetGlobalVariable("ID1").Value);
        }

        /// <summary>
        /// Tests the resetting alpha numeric counters.
        /// </summary>
        [TestMethod]
        public void TestResettingAlphaNumericCounters()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("ID1", "00001", EGlobalTypes.GlobalVariableType.Counter_Alpha);

            var listtoreset = new List<string> { "ID1", "ID2", "ID5" };

            gv.ResetCounters(listtoreset);
            Assert.AreEqual("1", gv.GetGlobalVariable("ID1").Value);
        }

        /// <summary>
        /// Tests resetting counters.
        /// </summary>
        [TestMethod]
        public void TestResettingCounters()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("ID1", "00001", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID2", "12345678901234", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID3", "0", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID4", "55", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID5", "1001", EGlobalTypes.GlobalVariableType.Counter_Integer);

            var listtoreset = new List<string> { "ID1", "ID2", "ID5" };

            gv.ResetCounters(listtoreset);
            Assert.AreEqual("00000", gv.GetGlobalVariable("ID1").Value);
            Assert.AreEqual("00000000000000", gv.GetGlobalVariable("ID2").Value);
            Assert.AreEqual("0", gv.GetGlobalVariable("ID3").Value);
            Assert.AreEqual("55", gv.GetGlobalVariable("ID4").Value);
            Assert.AreEqual("0000", gv.GetGlobalVariable("ID5").Value);
        }

        /// <summary>
        /// Tests the deleting variables.
        /// </summary>
        [TestMethod]
        public void TestDeletingVariables()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("ID1", "00001", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("ID2", "12345678901234", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("string1", "stringvalue", EGlobalTypes.GlobalVariableType.String);

            var countersDelete = new List<string> { "ID1", "ID2", "ID5" };
            var stringsDelete = new List<string> { "string1" };

            gv.DeleteVariables(countersDelete, true);
            gv.DeleteVariables(stringsDelete, false);

            Assert.AreEqual(0, gv.GetAllCounters().Count());
            Assert.AreEqual(0, gv.GetAllStrings().Count());
        }

        /// <summary>
        /// Tests the modifying string.
        /// </summary>
        [TestMethod]
        public void TestModifyingString()
        {
            var file = new FileInfo("C:/temp/blah.xml");
            file.Delete();

            var gv = new GlobalVariables(file);

            gv.AddGlobalVariable("string1", "value", EGlobalTypes.GlobalVariableType.String);

            gv.ModifyString("string1", "newvalue");

            Assert.AreEqual("newvalue", gv.GetGlobalVariable("string1").Value);
        }

        /// <summary>
        /// Tests the modifying string that's a counter.
        /// </summary>
        [TestMethod]
        public void TestModifyingStringThatIsACounter()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("count1", "0", EGlobalTypes.GlobalVariableType.Counter_Integer);

            gv.ModifyString("count", "newvalue");

            Assert.AreEqual("0", gv.GetGlobalVariable("count1").Value);
        }

        /// <summary>
        /// Tests the modifying  a counter.
        /// </summary>
        [TestMethod]
        public void TestModifyingCounter()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("count1", "0", EGlobalTypes.GlobalVariableType.Counter_Integer);

            gv.ModifyCounter("count1", "123");

            Assert.AreEqual("123", gv.GetGlobalVariable("count1").Value);
        }

        /// <summary>
        /// Tests modifying a counter to a non numeric value.
        /// </summary>
        [TestMethod]
        public void TestModifyingCounterToInvalidNumber()
        {
            var gv = GlobalHelpers.GetGlobalVariables();
            gv.AddGlobalVariable("count1", "012", EGlobalTypes.GlobalVariableType.Counter_Integer);

            var state = gv.ModifyCounter("count1", "a123");

            Assert.IsFalse(state);

            Assert.AreEqual("012", gv.GetGlobalVariable("count1").Value);
        }

        /// <summary>
        /// Tests modifying counter that is a string.
        /// </summary>
        [TestMethod]
        public void TestModifyingCounterThatIsAString()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("count1", "abc", EGlobalTypes.GlobalVariableType.String);

            gv.ModifyCounter("count1", "123");

            Assert.AreEqual("abc", gv.GetGlobalVariable("count1").Value);
        }

        /// <summary>
        /// Tests the get non existing global variable.
        /// </summary>
        [TestMethod]
        public void TestGetNonExistingGlobalVariable()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            var myvar = gv.GetGlobalVariable("abc123");

            Assert.AreEqual(null, myvar.Name);
        }

        /// <summary>
        /// Tests the many strings.
        /// </summary>
        [TestMethod]
        public void TestManyStrings()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            for (int i = 0; i < 500; i++)
            {
                gv.AddGlobalVariable(i.ToString(), i.ToString(), EGlobalTypes.GlobalVariableType.String);
            }

            gv.Dispose();

            var gv2 = new GlobalVariables(new FileInfo("C:/temp/blah.xml"));
            gv2.LoadGlobalVariableData();
            Assert.AreEqual(500, gv2.GetAllStrings().Count());
        }

        /// <summary>
        /// Tests adding duplicate strings.
        /// </summary>
        [TestMethod]
        public void TestAddingDuplicateStrings()
        {
            var gv = GlobalHelpers.GetGlobalVariables();
            gv.AddGlobalVariable("id1", "value1", EGlobalTypes.GlobalVariableType.String);
            gv.AddGlobalVariable("id1", "value1", EGlobalTypes.GlobalVariableType.String);
            Assert.AreEqual(1, gv.GetAllStrings().Count());
        }

        /// <summary>
        /// Tests adding duplicate counters.
        /// </summary>
        [TestMethod]
        public void TestAddingDuplicateCounter()
        {
            var gv = GlobalHelpers.GetGlobalVariables();
            gv.AddGlobalVariable("id1", "1", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.AddGlobalVariable("id1", "2", EGlobalTypes.GlobalVariableType.Counter_Integer);
            Assert.AreEqual(1, gv.GetAllCounters().Count());
        }

        /// <summary>
        /// Tests adding duplicate counters.
        /// </summary>
        [TestMethod]
        public void TestAddingSameNameVariables()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("id1", "1", EGlobalTypes.GlobalVariableType.String);
            gv.AddGlobalVariable("id1", "2", EGlobalTypes.GlobalVariableType.Counter_Integer);
            Assert.AreEqual(1, gv.GetAllCounters().Count());
            Assert.AreEqual(1, gv.GetAllStrings().Count());
        }

        /// <summary>
        /// Tests the saving to bad location.
        /// </summary>
        [TestMethod]
        public void TestSavingToBadLocation()
        {
            var gv = new GlobalVariables(new FileInfo(@"D:\file.xml"));

            gv.DeleteVariables(new List<string> { "Id1" }, true);
        }

        /// <summary>
        /// Tests incrementing a non long value.
        /// </summary>
        [TestMethod]
        public void TestInvalidLargeCounterValueIncrementing()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("ID1", "1", EGlobalTypes.GlobalVariableType.Counter_Alpha);
            gv.AddGlobalVariable("ID2", "999999999999999999999999999999999999999999", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.IncrementUsedCounters(new List<string> { "ID1", "ID2" });

            Assert.AreEqual("B", gv.GetGlobalVariable("ID1").DisplayValue);
            Assert.AreEqual("1", gv.GetGlobalVariable("ID2").DisplayValue);

            Assert.AreEqual(0, gv.GetAllStrings().Count());
        }

        /// <summary>
        /// Tests incrementing alpha numeric counters.
        /// </summary>
        [TestMethod]
        public void ValidAlphaCountersIncrementTest()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("ID1", "1", EGlobalTypes.GlobalVariableType.Counter_Alpha);
            gv.AddGlobalVariable("ID2", "1", EGlobalTypes.GlobalVariableType.Counter_Integer);
            gv.IncrementUsedCounters(new List<string> { "ID1", "ID2" });

            Assert.AreEqual("B", gv.GetGlobalVariable("ID1").DisplayValue);
            Assert.AreEqual("2", gv.GetGlobalVariable("ID2").DisplayValue);

            Assert.AreEqual(0, gv.GetAllStrings().Count());
        }

        /// <summary>
        /// Tests the display value on non counter.
        /// </summary>
        [TestMethod]
        public void TestDisplayValueOnNonCounter()
        {
            var gv = GlobalHelpers.GetGlobalVariables();

            gv.AddGlobalVariable("ID1", "1", EGlobalTypes.GlobalVariableType.String);
            gv.AddGlobalVariable("ID2", "2", EGlobalTypes.GlobalVariableType.String);

            Assert.AreEqual("1", gv.GetGlobalVariable("ID1").DisplayValue);
            Assert.AreEqual("2", gv.GetGlobalVariable("ID2").DisplayValue);

            Assert.AreEqual(2, gv.GetAllStrings().Count());
        }
    }
}
