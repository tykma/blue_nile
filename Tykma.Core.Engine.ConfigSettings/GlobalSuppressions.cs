﻿// <copyright file="GlobalSuppressions.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.SpacingRules", "SA1008:Opening parenthesis must be spaced correctly", Justification = "C#7.0 tuples", Scope = "member", Target = "~P:Tykma.Core.Engine.ConfigSettings.IEngineConfigurationHandler.MarkingOffset")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.SpacingRules", "SA1008:Opening parenthesis must be spaced correctly", Justification = "C#7.0 tuples", Scope = "member", Target = "~P:Tykma.Core.Engine.ConfigSettings.SEEngineConfigurationHandler.MarkingOffset")]