﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdObjectGetPen.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// Command to get a pen number from an object.
    /// </summary>
    internal class CmdObjectGetPen : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdObjectGetPen"/> class.
        /// </summary>
        /// <param name="lmcApi">
        /// The laser marking API.
        /// </param>
        /// <param name="objectToGetPen">
        /// The object to get pen.
        /// </param>
        public CmdObjectGetPen(IntPtr lmcApi, string objectToGetPen)
            : base(lmcApi)
        {
            this.ObjectToGetPenFrom = objectToGetPen;
        }

        /// <summary>
        /// Returns pen number associated with an entity.
        /// </summary>
        /// <param name="whichEnt">Entity to get pen number from.</param>
        /// <returns>Pointer to function.</returns>
        private delegate int LMC1ApiGetPenNumberFromEnt([MarshalAs(UnmanagedType.LPWStr)] string whichEnt);

        /// <summary>
        /// Gets the object to get pen from.
        /// </summary>
        /// <value>
        /// The object to get pen from.
        /// </value>
        private string ObjectToGetPenFrom { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Pen number.</returns>
        /// <exception cref="Tykma.Core.Engine.CustomExceptions.EngineNotConnectedException">Engine not connected.</exception>
        internal int Execute()
        {
            var lmc1PenNo = this.LoadFunction<LMC1ApiGetPenNumberFromEnt>("lmc1_GetPenNumberFromEnt");
            if (lmc1PenNo == null)
            {
                throw new EngineNotConnectedException();
            }

            return lmc1PenNo(this.ObjectToGetPenFrom);
        }
    }
}
