﻿namespace Icon_Interface.Forms
{
    partial class GlobalVariableAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ux_layout_AddVariable = new System.Windows.Forms.TableLayoutPanel();
            this.ux_label_AddGlobalHeader = new System.Windows.Forms.Label();
            this.ux_layout_AddVariableInner = new System.Windows.Forms.TableLayoutPanel();
            this.ux_label_GvName = new System.Windows.Forms.Label();
            this.ux_label_variableValue = new System.Windows.Forms.Label();
            this.ux_Text_AddVarID = new System.Windows.Forms.TextBox();
            this.ux_Text_AddVarValue = new System.Windows.Forms.TextBox();
            this.ux_layout_AddVariableControls = new System.Windows.Forms.TableLayoutPanel();
            this.ux_Button_CancelGV = new System.Windows.Forms.Button();
            this.ux_Button_OKAddGV = new System.Windows.Forms.Button();
            this.ux_Check_ALphaGlobal = new System.Windows.Forms.CheckBox();
            this.ux_layout_AddVariable.SuspendLayout();
            this.ux_layout_AddVariableInner.SuspendLayout();
            this.ux_layout_AddVariableControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // ux_layout_AddVariable
            // 
            this.ux_layout_AddVariable.ColumnCount = 1;
            this.ux_layout_AddVariable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ux_layout_AddVariable.Controls.Add(this.ux_label_AddGlobalHeader, 0, 0);
            this.ux_layout_AddVariable.Controls.Add(this.ux_layout_AddVariableInner, 0, 1);
            this.ux_layout_AddVariable.Controls.Add(this.ux_layout_AddVariableControls, 0, 2);
            this.ux_layout_AddVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_layout_AddVariable.Location = new System.Drawing.Point(0, 0);
            this.ux_layout_AddVariable.Name = "ux_layout_AddVariable";
            this.ux_layout_AddVariable.RowCount = 3;
            this.ux_layout_AddVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29.03226F));
            this.ux_layout_AddVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70.96774F));
            this.ux_layout_AddVariable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.ux_layout_AddVariable.Size = new System.Drawing.Size(375, 174);
            this.ux_layout_AddVariable.TabIndex = 0;
            // 
            // ux_label_AddGlobalHeader
            // 
            this.ux_label_AddGlobalHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_label_AddGlobalHeader.AutoSize = true;
            this.ux_label_AddGlobalHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_label_AddGlobalHeader.Location = new System.Drawing.Point(3, 10);
            this.ux_label_AddGlobalHeader.Name = "ux_label_AddGlobalHeader";
            this.ux_label_AddGlobalHeader.Size = new System.Drawing.Size(369, 24);
            this.ux_label_AddGlobalHeader.TabIndex = 0;
            this.ux_label_AddGlobalHeader.Text = "Add a Global Variable";
            this.ux_label_AddGlobalHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ux_layout_AddVariableInner
            // 
            this.ux_layout_AddVariableInner.ColumnCount = 3;
            this.ux_layout_AddVariableInner.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_layout_AddVariableInner.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ux_layout_AddVariableInner.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ux_layout_AddVariableInner.Controls.Add(this.ux_label_GvName, 0, 0);
            this.ux_layout_AddVariableInner.Controls.Add(this.ux_label_variableValue, 0, 1);
            this.ux_layout_AddVariableInner.Controls.Add(this.ux_Text_AddVarID, 1, 0);
            this.ux_layout_AddVariableInner.Controls.Add(this.ux_Text_AddVarValue, 1, 1);
            this.ux_layout_AddVariableInner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_layout_AddVariableInner.Location = new System.Drawing.Point(3, 37);
            this.ux_layout_AddVariableInner.Name = "ux_layout_AddVariableInner";
            this.ux_layout_AddVariableInner.RowCount = 2;
            this.ux_layout_AddVariableInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ux_layout_AddVariableInner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ux_layout_AddVariableInner.Size = new System.Drawing.Size(369, 79);
            this.ux_layout_AddVariableInner.TabIndex = 1;
            // 
            // ux_label_GvName
            // 
            this.ux_label_GvName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_label_GvName.AutoSize = true;
            this.ux_label_GvName.Location = new System.Drawing.Point(3, 13);
            this.ux_label_GvName.Name = "ux_label_GvName";
            this.ux_label_GvName.Size = new System.Drawing.Size(86, 13);
            this.ux_label_GvName.TabIndex = 0;
            this.ux_label_GvName.Text = "ID";
            this.ux_label_GvName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ux_label_variableValue
            // 
            this.ux_label_variableValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_label_variableValue.AutoSize = true;
            this.ux_label_variableValue.Location = new System.Drawing.Point(3, 52);
            this.ux_label_variableValue.Name = "ux_label_variableValue";
            this.ux_label_variableValue.Size = new System.Drawing.Size(86, 13);
            this.ux_label_variableValue.TabIndex = 1;
            this.ux_label_variableValue.Text = "Value";
            this.ux_label_variableValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ux_Text_AddVarID
            // 
            this.ux_Text_AddVarID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Text_AddVarID.Location = new System.Drawing.Point(95, 9);
            this.ux_Text_AddVarID.Name = "ux_Text_AddVarID";
            this.ux_Text_AddVarID.Size = new System.Drawing.Size(178, 20);
            this.ux_Text_AddVarID.TabIndex = 2;
            // 
            // ux_Text_AddVarValue
            // 
            this.ux_Text_AddVarValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Text_AddVarValue.Location = new System.Drawing.Point(95, 49);
            this.ux_Text_AddVarValue.Name = "ux_Text_AddVarValue";
            this.ux_Text_AddVarValue.Size = new System.Drawing.Size(178, 20);
            this.ux_Text_AddVarValue.TabIndex = 3;
            this.ux_Text_AddVarValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextAddVarValueKeyPress);
            // 
            // ux_layout_AddVariableControls
            // 
            this.ux_layout_AddVariableControls.ColumnCount = 3;
            this.ux_layout_AddVariableControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_layout_AddVariableControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_layout_AddVariableControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ux_layout_AddVariableControls.Controls.Add(this.ux_Button_CancelGV, 0, 0);
            this.ux_layout_AddVariableControls.Controls.Add(this.ux_Button_OKAddGV, 2, 0);
            this.ux_layout_AddVariableControls.Controls.Add(this.ux_Check_ALphaGlobal, 1, 0);
            this.ux_layout_AddVariableControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_layout_AddVariableControls.Location = new System.Drawing.Point(3, 122);
            this.ux_layout_AddVariableControls.Name = "ux_layout_AddVariableControls";
            this.ux_layout_AddVariableControls.RowCount = 1;
            this.ux_layout_AddVariableControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ux_layout_AddVariableControls.Size = new System.Drawing.Size(369, 49);
            this.ux_layout_AddVariableControls.TabIndex = 2;
            // 
            // ux_Button_CancelGV
            // 
            this.ux_Button_CancelGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Button_CancelGV.Image = global::Icon_Interface.Properties.ImageResources.StatusAnnotations_Critical_32xLG_color1;
            this.ux_Button_CancelGV.Location = new System.Drawing.Point(3, 3);
            this.ux_Button_CancelGV.Name = "ux_Button_CancelGV";
            this.ux_Button_CancelGV.Size = new System.Drawing.Size(117, 43);
            this.ux_Button_CancelGV.TabIndex = 0;
            this.ux_Button_CancelGV.Text = "Cancel";
            this.ux_Button_CancelGV.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ux_Button_CancelGV.UseVisualStyleBackColor = true;
            this.ux_Button_CancelGV.Click += new System.EventHandler(this.ButtonCancelGvClick);
            // 
            // ux_Button_OKAddGV
            // 
            this.ux_Button_OKAddGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Button_OKAddGV.Image = global::Icon_Interface.Properties.ImageResources.StatusAnnotations_Complete_and_ok_32xLG_color1;
            this.ux_Button_OKAddGV.Location = new System.Drawing.Point(249, 3);
            this.ux_Button_OKAddGV.Name = "ux_Button_OKAddGV";
            this.ux_Button_OKAddGV.Size = new System.Drawing.Size(117, 43);
            this.ux_Button_OKAddGV.TabIndex = 1;
            this.ux_Button_OKAddGV.Text = "Add";
            this.ux_Button_OKAddGV.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ux_Button_OKAddGV.UseVisualStyleBackColor = true;
            this.ux_Button_OKAddGV.Click += new System.EventHandler(this.ButtonOkAddGvClick);
            // 
            // ux_Check_ALphaGlobal
            // 
            this.ux_Check_ALphaGlobal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_Check_ALphaGlobal.AutoSize = true;
            this.ux_Check_ALphaGlobal.Location = new System.Drawing.Point(126, 16);
            this.ux_Check_ALphaGlobal.Name = "ux_Check_ALphaGlobal";
            this.ux_Check_ALphaGlobal.Size = new System.Drawing.Size(117, 17);
            this.ux_Check_ALphaGlobal.TabIndex = 2;
            this.ux_Check_ALphaGlobal.Text = "Alpha";
            this.ux_Check_ALphaGlobal.UseVisualStyleBackColor = true;
            // 
            // GlobalVariableAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(375, 174);
            this.Controls.Add(this.ux_layout_AddVariable);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "GlobalVariableAdd";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add a Variable";
            this.ux_layout_AddVariable.ResumeLayout(false);
            this.ux_layout_AddVariable.PerformLayout();
            this.ux_layout_AddVariableInner.ResumeLayout(false);
            this.ux_layout_AddVariableInner.PerformLayout();
            this.ux_layout_AddVariableControls.ResumeLayout(false);
            this.ux_layout_AddVariableControls.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel ux_layout_AddVariable;
        private System.Windows.Forms.Label ux_label_AddGlobalHeader;
        private System.Windows.Forms.TableLayoutPanel ux_layout_AddVariableInner;
        private System.Windows.Forms.Label ux_label_GvName;
        private System.Windows.Forms.Label ux_label_variableValue;
        private System.Windows.Forms.TextBox ux_Text_AddVarID;
        private System.Windows.Forms.TextBox ux_Text_AddVarValue;
        private System.Windows.Forms.TableLayoutPanel ux_layout_AddVariableControls;
        private System.Windows.Forms.Button ux_Button_CancelGV;
        private System.Windows.Forms.Button ux_Button_OKAddGV;
        private System.Windows.Forms.CheckBox ux_Check_ALphaGlobal;
    }
}