﻿// <copyright file="WebBuild.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Core.HTTPD.Web
{
    using System.Collections.Generic;

    /// <summary>
    /// Web builder.
    /// </summary>
    public class WebBuild
    {
        /// <summary>
        /// The builder.
        /// </summary>
        private readonly IWebBuilder builder;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebBuild"/> class.
        /// </summary>
        public WebBuild()
        {
            this.builder = new BasicBuilder();
        }

        /// <summary>
        /// Gets the generated page.
        /// </summary>
        /// <param name="jobList">The job list.</param>
        /// <returns>The entire page as one long string.</returns>
        public string GetPage(List<string> jobList)
        {
            return this.builder.GetPage(jobList);
        }
    }
}
