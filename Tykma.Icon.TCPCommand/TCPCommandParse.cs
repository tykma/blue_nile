﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TcpCommandParse.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.TCPCommand
{
    using System.Collections.Generic;

    using Tykma.Core.StringExtensions;

    /// <summary>
    /// The tcp command parse.
    /// </summary>
    public class TcpCommandParse
    {
        /// <summary>
        /// Command type.
        /// </summary>
        public enum CommandType
        {
            /// <summary>
            /// The job status
            /// </summary>
            JobStatus,

            /// <summary>
            /// Clear_ job
            /// </summary>
            ClearJob,

            /// <summary>
            /// Enable job
            /// </summary>
            EnableJob,

            /// <summary>
            /// Load job.
            /// </summary>
            LoadJob,

            /// <summary>
            /// The axis coordinate
            /// </summary>
            AxisCoordinate,

            /// <summary>
            /// Axis limits.
            /// </summary>
            AxisLimits,

            /// <summary>
            /// Axis home request.
            /// </summary>
            AxisHome,

            /// <summary>
            /// Axis move request.
            /// </summary>
            AxisMove,

            /// <summary>
            /// Axis stop.
            /// </summary>
            AxisStop,

            /// <summary>
            /// Project list.
            /// </summary>
            ProjectList,

            /// <summary>
            /// Version request.
            /// </summary>
            Version,

            /// <summary>
            /// Set an ID.
            /// </summary>
            SetID,

            /// <summary>
            /// Loaded job.
            /// </summary>
            LoadedJob,

            /// <summary>
            /// Preview request.
            /// </summary>
            Preview,

            /// <summary>
            /// Get the system state.
            /// </summary>
            SystemState,

            /// <summary>
            /// Toggle the limits.
            /// </summary>
            LimitsToggle,

            /// <summary>
            /// The limits on.
            /// </summary>
            LimitsOn,

            /// <summary>
            /// Stop request.
            /// </summary>
            Stop,

            /// <summary>
            /// Start request.
            /// </summary>
            Start,

            /// <summary>
            /// Get all ids request.
            /// </summary>
            GetAllIds,

            /// <summary>
            /// Nudge object request.
            /// </summary>
            NudgeObject,

            /// <summary>
            /// Mark an object request.
            /// </summary>
            MarkObject,

            /// <summary>
            /// Rotate object.
            /// </summary>
            RotateObject,

            /// <summary>
            /// The limits off.
            /// </summary>
            LimitsOff,

            /// <summary>
            /// Request for a web interface.
            /// </summary>
            GetWeb,

            /// <summary>
            /// Rotate all request.
            /// </summary>
            MoveAll,

            /// <summary>
            /// Rotate all objects request.
            /// </summary>
            RotateAll,

            /// <summary>
            /// The shutter state.
            /// </summary>
            ShutterState,

            /// <summary>
            /// Door open request.
            /// </summary>
            DoorOpen,

            /// <summary>
            /// Door close request.
            /// </summary>
            DoorClose,

            /// <summary>
            /// Get laser power
            /// </summary>
            GetLaserPower,

            /// <summary>
            /// Set laser power
            /// </summary>
            SetLaserPower,

            /// <summary>
            /// Get laser frequency
            /// </summary>
            GetLaserFrequency,

            /// <summary>
            /// Set laser frequency
            /// </summary>
            SetLaserFrequency,

            /// <summary>
            /// Show a user message.
            /// </summary>
            ShowUserMessage,

            /// <summary>
            /// Delete an object.
            /// </summary>
            DeleteObject,

            /// <summary>
            /// Hide user message
            /// </summary>
            HideUserMessage,

            /// <summary>
            /// None request.
            /// </summary>
            None,
        }

        /// <summary>
        /// Parses the command.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <returns>
        /// A command type.
        /// </returns>
        public CommandReturn ParseCommand(string command)
        {
            var typeofcommand = command.Split('?')[0].TrimEnd();

            // Job status.
            if (typeofcommand.CaseInsensitiveCompare("job_status"))
            {
                return new CommandReturn(CommandType.JobStatus);
            }

            // Project list.
            if (typeofcommand.CaseInsensitiveCompare("project_list"))
            {
                return new CommandReturn(CommandType.ProjectList);
            }

            if (typeofcommand.CaseInsensitiveCompare("all_ids"))
            {
                return new CommandReturn(CommandType.GetAllIds);
            }

            // Preview.
            if (typeofcommand.CaseInsensitiveCompare("get_preview"))
            {
                return new CommandReturn(CommandType.Preview);
            }

            // Version request.
            if (typeofcommand.CaseInsensitiveCompare("version"))
            {
                return new CommandReturn(CommandType.Version);
            }

            // Limits toggle.
            if (typeofcommand.CaseInsensitiveCompare("limits"))
            {
                return new CommandReturn(CommandType.LimitsToggle);
            }

            // Start.
            if (typeofcommand.CaseInsensitiveCompare("start"))
            {
                return new CommandReturn(CommandType.Start);
            }

            // Stop.
            if (typeofcommand.CaseInsensitiveCompare("stop"))
            {
                return new CommandReturn(CommandType.Stop);
            }

            // Limits on.
            if (typeofcommand.CaseInsensitiveCompare("limits_on"))
            {
                return new CommandReturn(CommandType.LimitsOn);
            }

            // Limits off.
            if (typeofcommand.CaseInsensitiveCompare("limits_off"))
            {
                return new CommandReturn(CommandType.LimitsOff);
            }

            // Version request.
            if (typeofcommand.CaseInsensitiveCompare("state"))
            {
                return new CommandReturn(CommandType.SystemState);
            }

            // Loaded job request.
            if (typeofcommand.CaseInsensitiveCompare("loaded_job"))
            {
                return new CommandReturn(CommandType.LoadedJob);
            }

            // Clear job.
            if (typeofcommand.CaseInsensitiveCompare("clear_job"))
            {
                return new CommandReturn(CommandType.ClearJob);
            }

            // Enable job.
            if (typeofcommand.CaseInsensitiveCompare("enable_job"))
            {
                return new CommandReturn(CommandType.EnableJob);
            }

            // Load job.
            if (typeofcommand.CaseInsensitiveCompare("load_job"))
            {
                string[] jobToLoad = command.Split(new[] { '?' }, 2);

                var kvp = new Dictionary<string, string>();

                string jobRequest = string.Empty;

                if (jobToLoad.Length > 1)
                {
                    jobRequest = jobToLoad[1].Trim();
                }

                kvp.Add("job", jobRequest);

                return new CommandReturn(CommandType.LoadJob, kvp);
            }

            // Get web page.
            if (typeofcommand.CaseInsensitiveCompare("web"))
            {
                return new CommandReturn(CommandType.GetWeb);
            }

            // Axis Coordinate.
            if (typeofcommand.CaseInsensitiveCompare("axis_coor"))
            {
                var parts = TykmaUriParser.DecodeQueryParameters(command);
                string whichAxis = parts.GetValueOrDefault("axis", string.Empty);

                return new CommandReturn(
                    CommandType.AxisCoordinate,
                    new Dictionary<string, string> { { "axis", whichAxis } });
            }

            // Get axis min/max.
            if (typeofcommand.CaseInsensitiveCompare("get_axis"))
            {
                var parts = TykmaUriParser.DecodeQueryParameters(command);
                string whichAxis = parts.GetValueOrDefault("axis", string.Empty);
                string minormax = "max";

                if (parts.ContainsKey("value"))
                {
                    if (parts["value"].CaseInsensitiveCompare("min") || parts["value"].CaseInsensitiveCompare("max"))
                    {
                        minormax = parts["value"].ToLower();
                    }
                }

                var kvp = new Dictionary<string, string> { { "axis", whichAxis }, { "value", minormax } };

                return new CommandReturn(CommandType.AxisLimits, kvp);
            }

            // Axis home.
            if (typeofcommand.CaseInsensitiveCompare("axis_home"))
            {
                string whichAxis = TykmaUriParser.DecodeQueryParameters(command).GetValueOrDefault("axis", string.Empty);

                return new CommandReturn(CommandType.AxisHome, new Dictionary<string, string> { { "axis", whichAxis } });
            }

            // A move axis command.
            if (typeofcommand.CaseInsensitiveCompare("move_axis"))
            {
                string whichAxis = TykmaUriParser.DecodeQueryParameters(command).GetValueOrDefault("axis", string.Empty);
                string location = TykmaUriParser.DecodeQueryParameters(command).GetValueOrDefault("position", string.Empty);

                var kvp = new Dictionary<string, string> { { "axis", whichAxis }, { "position", location } };
                return new CommandReturn(CommandType.AxisMove, kvp);
            }

            // Axis stop.
            if (typeofcommand.CaseInsensitiveCompare("stop_axis"))
            {
                string whichAxis = TykmaUriParser.DecodeQueryParameters(command).GetValueOrDefault("axis", string.Empty);

                return new CommandReturn(CommandType.AxisStop, new Dictionary<string, string> { { "axis", whichAxis } });
            }

            // Set an ID.
            if (typeofcommand.CaseInsensitiveCompare("set_id"))
            {
                var parts = TykmaUriParser.DecodeQueryParameters(command);
                string whichID = parts.GetValueOrDefault("id", string.Empty);
                string whichData = parts.GetValueOrDefault("data", string.Empty);

                var kvp = new Dictionary<string, string> { { "id", whichID }, { "data", whichData } };

                var returncmd = new CommandReturn(CommandType.SetID, kvp);

                return returncmd;
            }

            // Nudge an object.
            if (typeofcommand.CaseInsensitiveCompare("nudge_object"))
            {
                var parts = TykmaUriParser.DecodeQueryParameters(command);
                string whichID = parts.GetValueOrDefault("id", string.Empty);
                string nudgeX = parts.GetValueOrDefault("x", string.Empty);
                string nudgeY = parts.GetValueOrDefault("y", string.Empty);

                var kvp = new Dictionary<string, string> { { "id", whichID }, { "x", nudgeX }, { "y", nudgeY } };

                var returncmd = new CommandReturn(CommandType.NudgeObject, kvp);

                return returncmd;
            }

            // Mark an individual object.
            if (typeofcommand.CaseInsensitiveCompare("mark_object"))
            {
                var parts = TykmaUriParser.DecodeQueryParameters(command);
                string whichID = parts.GetValueOrDefault("id", string.Empty);
                var kvp = new Dictionary<string, string> { { "id", whichID } };
                return new CommandReturn(CommandType.MarkObject, kvp);
            }

            if (typeofcommand.CaseInsensitiveCompare("rotate_object"))
            {
                var parts = TykmaUriParser.DecodeQueryParameters(command);
                string whichID = parts.GetValueOrDefault("id", string.Empty);
                string angle = parts.GetValueOrDefault("angle", string.Empty);

                var kvp = new Dictionary<string, string> { { "id", whichID }, { "angle", angle } };

                var returncmd = new CommandReturn(CommandType.RotateObject, kvp);

                return returncmd;
            }

            // Move all comand.
            if (typeofcommand.CaseInsensitiveCompare("move_all"))
            {
                var parts = TykmaUriParser.DecodeQueryParameters(command);
                string nudgeX = parts.GetValueOrDefault("x", string.Empty);
                string nudgeY = parts.GetValueOrDefault("y", string.Empty);

                var kvp = new Dictionary<string, string> { { "x", nudgeX }, { "y", nudgeY } };

                var returncmd = new CommandReturn(CommandType.MoveAll, kvp);

                return returncmd;
            }

            // Rotate all command.
            if (typeofcommand.CaseInsensitiveCompare("rotate_all"))
            {
                string angle = TykmaUriParser.DecodeQueryParameters(command).GetValueOrDefault("angle", string.Empty);

                var returncmd = new CommandReturn(CommandType.RotateAll, new Dictionary<string, string> { { "angle", angle } });

                return returncmd;
            }

            // Door close request.
            if (typeofcommand.CaseInsensitiveCompare("door_close"))
            {
                return new CommandReturn(CommandType.DoorClose);
            }

            // Door close request.
            if (typeofcommand.CaseInsensitiveCompare("door_open"))
            {
                return new CommandReturn(CommandType.DoorOpen);
            }

            // Shutter state request.
            if (typeofcommand.CaseInsensitiveCompare("shutter_state"))
            {
                return new CommandReturn(CommandType.ShutterState);
            }

            // Get laser power request.
            if (typeofcommand.CaseInsensitiveCompare("get_power"))
            {
                string pen = TykmaUriParser.DecodeQueryParameters(command).GetValueOrDefault("pen", string.Empty);
                return new CommandReturn(CommandType.GetLaserPower, new Dictionary<string, string> { { "pen", pen } });
            }

            // Set laser power request.
            if (typeofcommand.CaseInsensitiveCompare("set_power"))
            {
                string pen = TykmaUriParser.DecodeQueryParameters(command).GetValueOrDefault("pen", string.Empty);
                string power = TykmaUriParser.DecodeQueryParameters(command).GetValueOrDefault("power", string.Empty);

                var dict = new Dictionary<string, string> { { "pen", pen }, { "power", power } };

                return new CommandReturn(CommandType.SetLaserPower, dict);
            }

            // Get laser frequency request.
            if (typeofcommand.CaseInsensitiveCompare("get_frequency"))
            {
                string pen = TykmaUriParser.DecodeQueryParameters(command).GetValueOrDefault("pen", string.Empty);
                return new CommandReturn(CommandType.GetLaserFrequency, new Dictionary<string, string> { { "pen", pen } });
            }

            // Set laser frequency request.
            if (typeofcommand.CaseInsensitiveCompare("set_frequency"))
            {
                string pen = TykmaUriParser.DecodeQueryParameters(command).GetValueOrDefault("pen", string.Empty);
                string freq = TykmaUriParser.DecodeQueryParameters(command).GetValueOrDefault("frequency", string.Empty);

                var dict = new Dictionary<string, string> { { "pen", pen }, { "frequency", freq } };

                return new CommandReturn(CommandType.SetLaserFrequency, dict);
            }

            // Delete an object.
            if (typeofcommand.CaseInsensitiveCompare("delete_object"))
            {
                var parts = TykmaUriParser.DecodeQueryParameters(command);
                string whichObj = parts.GetValueOrDefault("object", string.Empty);
                var kvp = new Dictionary<string, string> { { "object", whichObj } };

                return new CommandReturn(CommandType.DeleteObject, kvp);
            }

            // Show a user message.
            if (typeofcommand.CaseInsensitiveCompare("show_user_message"))
            {
                string message = TykmaUriParser.DecodeQueryParameters(command).GetValueOrDefault("message", "Alert");
                return new CommandReturn(CommandType.ShowUserMessage, new Dictionary<string, string> { { "message", message } });
            }

            // Hide user message.
            if (typeofcommand.CaseInsensitiveCompare("hide_user_message"))
            {
                return new CommandReturn(CommandType.HideUserMessage);
            }

            return new CommandReturn(CommandType.None);
        }
    }
}
