﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ErrorMessage.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.ElectroxEngine
{
    /// <summary>
    /// The error message.
    /// </summary>
    public class ErrorMessage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorMessage" /> class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="code">The code.</param>
        public ErrorMessage(ErrorType type, int code)
        {
            this.Error = type;
            this.Code = code;
        }

        /// <summary>
        /// Error messages.
        /// </summary>
        public enum ErrorType
        {
            /// <summary>
            /// The interlock fault.
            /// </summary>
            InterlockFault,

            /// <summary>
            /// Other fault.
            /// </summary>
            Other,

            /// <summary>
            /// Not a valid fault.
            /// </summary>
            NotValidFault
        }

        /// <summary>
        /// Gets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        public ErrorType Error { get; private set; }

        /// <summary>
        /// Gets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        public int Code { get; private set; }
    }
}
