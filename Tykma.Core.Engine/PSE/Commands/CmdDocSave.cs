﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdDocSave.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// The command to save a document.
    /// </summary>
    internal class CmdDocSave : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdDocSave"/> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking card API.</param>
        /// <param name="fileName">Name of the file.</param>
        public CmdDocSave(IntPtr lmcApi, string fileName)
            : base(lmcApi)
        {
            this.FilenameToSave = fileName;
        }

        /// <summary>
        /// Save to file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>Return Code.</returns>
        private delegate int LMC1ApiSaveEntLibToFile([MarshalAs(UnmanagedType.LPWStr)] string fileName);

        /// <summary>
        /// Gets the filename to save.
        /// </summary>
        /// <value>
        /// The filename to save.
        /// </value>
        private string FilenameToSave { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        /// <exception cref="EngineNotConnectedException">Engine not connected.</exception>
        /// <exception cref="MethodFaultException">Problem saving the document.</exception>
        internal bool Execute()
        {
            var lmc1SaveFile = this.LoadFunction<LMC1ApiSaveEntLibToFile>("lmc1_SaveEntLibToFile");

            if (lmc1SaveFile == null)
            {
                throw new EngineNotConnectedException();
            }

            var rm = this.GetMiniLaseReturnCode(lmc1SaveFile(this.FilenameToSave));

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
