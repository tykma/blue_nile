﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SQLRetrievedDataTestable.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   IO testing.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.SQLClient
{
    using System.Collections.Generic;

    using Tykma.Core.Entity;
    using Tykma.Core.SQL.Client;

    /// <summary>
    /// Override SQL retrieved data class to allow setting of properties.
    /// </summary>
    public class SQLRetrievedDataTestable : SQLRetrievedData
    {
        /// <summary>
        /// Gets or sets the name of the job.
        /// </summary>
        /// <value>
        /// The name of the job.
        /// </value>
        public new string JobName { get; set; }

        /// <summary>
        /// Gets or sets the primary key.
        /// </summary>
        /// <value>
        /// The prim key.
        /// </value>
        public string PrimKey { get; set; }

        /// <summary>
        /// Gets or sets the retrieved tags.
        /// </summary>
        /// <value>
        /// The retrieved tags.
        /// </value>
        public new IEnumerable<EntityInformation> RetrievedTags { get; set; }
    }
}
