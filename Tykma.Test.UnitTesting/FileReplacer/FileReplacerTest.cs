﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileReplacerTest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.FileReplacer
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Threading;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Core.DynamicFile;

    /// <summary>
    /// File replace tests.
    /// </summary>
    [TestClass]
    public class FileReplacerTest
    {
        /// <summary>
        /// Test adding multiple extensions.
        /// </summary>
        [TestMethod]
        public void AddingMultipleExtensions()
        {
            var newdir = new DirectoryInfo(@"C:\temp\frunittest3");
            var destFile = new FileInfo(@"C:\temp\TestLogo1.dxf");
            var extensions = new List<string> { "dxf", "plt", "dxf" };

            var filereplacer = new FileReplacer(newdir.FullName, destFile, extensions, 500);
            Assert.AreEqual(2, filereplacer.ExtensionsSearched.Count);
        }

        /// <summary>
        /// Test basic functionality.
        /// </summary>
        [TestMethod]
        public void TestFileReplacerFunctionality()
        {
            var newdir = this.CreateDirectory(@"C:\temp\frunittest");

            newdir.Create();

            var destFile = new FileInfo(@"C:\temp\TestLogo.dxf");
            if (destFile.Exists)
            {
                destFile.Delete();
            }

            var extensions = new List<string> { "dxf", "plt" };

            var filereplacer = new FileReplacer(newdir.FullName, destFile, extensions, 500);

            bool eventReceived = false;
            bool foundFileMsg = false;
            bool fileFoundReceived = false;

            ManualResetEvent foundFileReceived = new ManualResetEvent(false);
            ManualResetEvent movedFileReceived = new ManualResetEvent(false);

            filereplacer.FileSearchEvent += (sender, e) =>
                {
                    if (foundFileMsg)
                    {
                        DynamicFileEventState fileState = e;
                        Assert.AreEqual(DynamicFileEventState.DynamicState.MoveCompleted, fileState.State);
                        eventReceived = true;
                        movedFileReceived.Set();
                        Debug.Print("Move message received.");
                    }
                    else
                    {
                        fileFoundReceived = true;
                        foundFileMsg = true;
                        foundFileReceived.Set();
                        DynamicFileEventState fileState = e;
                        Debug.Print("Found file message received.");
                        Assert.AreEqual("fileName.dxf", fileState.FileName);
                        Assert.AreEqual(DynamicFileEventState.DynamicState.FoundFile, fileState.State);
                    }
                };

            // Let's enable the file replacer.
            filereplacer.Enabled(true);

            // Let's create a temporary file.
            var filetocreate = new FileInfo(Path.Combine(newdir.FullName, "fileName.dxf"));
            File.Create(filetocreate.FullName).Dispose();

            // Wait for the event.
            foundFileReceived.WaitOne(2000, true);
            Debug.Print("See if file found is true");
            Assert.AreEqual(true, fileFoundReceived);

            movedFileReceived.WaitOne(5000, true);
            Debug.Print("See if move received is true");
            Assert.IsTrue(eventReceived);

            Debug.Print("Checking if file exists");
            Assert.IsTrue(File.Exists(destFile.FullName));
        }

        /// <summary>
        /// Tests the file replacer disable.
        /// </summary>
        [TestMethod]
        public void TestFileReplacerDisable()
        {
            var newdir = new DirectoryInfo(@"C:\temp\frunittestd");
            var destFile = new FileInfo(@"C:\temp\TestLogo.dxf1");
            var extensions = new List<string> { "dxf", "plt" };

            var filereplacer = new FileReplacer(newdir.FullName, destFile, extensions, 500);

            filereplacer.Enabled(false);

            Assert.IsFalse(filereplacer.IsEnabled);
        }

        /// <summary>
        /// Tests the locked file.
        /// </summary>
        [TestMethod]
        public void TestLockedFile()
        {
            var newdir = this.CreateDirectory(@"C:\temp\frunittest2");

            newdir.Create();

            var destFile = new FileInfo(@"C:\temp\frunittest2\TestLogoLocked.dxf");

            var extensions = new List<string> { "dxf", "plt" };

            var filereplacer = new FileReplacer(newdir.FullName, destFile, extensions, 500);

            bool eventReceived = false;
            bool foundFileMsg = false;
            bool fileFoundReceived = false;
            string errormessage = string.Empty;

            ManualResetEvent foundFileReceived = new ManualResetEvent(false);
            ManualResetEvent movedFileReceived = new ManualResetEvent(false);

            filereplacer.FileSearchEvent += (sender, e) =>
                {
                    if (foundFileMsg)
                    {
                        DynamicFileEventState fileState = e;
                        Assert.AreEqual(DynamicFileEventState.DynamicState.Error, fileState.State);
                        errormessage = fileState.Error;
                        eventReceived = true;
                        movedFileReceived.Set();
                        Debug.Print("Move message received.");
                    }
                    else
                    {
                        fileFoundReceived = true;
                        foundFileMsg = true;
                        foundFileReceived.Set();
                        DynamicFileEventState fileState = e;
                        Debug.Print("Found file message received.");
                        Assert.AreEqual("fileName.dxf", fileState.FileName);
                        Assert.AreEqual(DynamicFileEventState.DynamicState.FoundFile, fileState.State);
                    }
                };

            // Let's enable the file replacer.
            filereplacer.Enabled(true);

            // Let's create a temporary file.
            var filetocreate = new FileInfo(Path.Combine(newdir.FullName, "fileName.dxf"));
            File.Create(filetocreate.FullName).Dispose();
            var fs = new FileStream(filetocreate.FullName, FileMode.Open, FileAccess.ReadWrite, FileShare.None);

            // Wait for the event.
            foundFileReceived.WaitOne(2000, true);
            Debug.Print("See if file found is true");
            Assert.AreEqual(true, fileFoundReceived);

            movedFileReceived.WaitOne(11000, true);
            Debug.Print("See if move received is true");
            Assert.AreEqual("Could not move file", errormessage);
            Assert.IsTrue(eventReceived);
            fs.Close();
        }

        /// <summary>
        /// Tests the invalid destination directory.
        /// </summary>
        [TestMethod]
        public void TestInvalidDestinationDirectory()
        {
            var newdir = this.CreateDirectory(@"C:\temp\frunittest5");

            var destFile = new FileInfo(@"D:\temp\TestLogoLocked.dxf");

            var extensions = new List<string> { "dxf", "plt" };

            var filereplacer = new FileReplacer(newdir.FullName, destFile, extensions, 500);

            bool eventReceived = false;
            bool foundFileMsg = false;
            bool fileFoundReceived = false;
            string errormessage = string.Empty;

            ManualResetEvent foundFileReceived = new ManualResetEvent(false);
            ManualResetEvent movedFileReceived = new ManualResetEvent(false);

            filereplacer.FileSearchEvent += (sender, e) =>
                {
                    if (foundFileMsg)
                    {
                        DynamicFileEventState fileState = e;
                        Assert.AreEqual(DynamicFileEventState.DynamicState.Error, fileState.State);
                        errormessage = fileState.Error;
                        eventReceived = true;
                        movedFileReceived.Set();
                        Debug.Print("Move message received.");
                    }
                    else
                    {
                        fileFoundReceived = true;
                        foundFileMsg = true;
                        foundFileReceived.Set();
                        DynamicFileEventState fileState = e;
                        Debug.Print("Found file message received.");
                        Assert.AreEqual("fileName.dxf", fileState.FileName);
                        Assert.AreEqual(DynamicFileEventState.DynamicState.FoundFile, fileState.State);
                    }
                };

            // Let's enable the file replacer.
            filereplacer.Enabled(true);

            // Let's create a temporary file.
            var filetocreate = new FileInfo(Path.Combine(newdir.FullName, "fileName.dxf"));
            File.Create(filetocreate.FullName).Dispose();

            // Wait for the event.
            foundFileReceived.WaitOne(2000, true);
            Debug.Print("See if file found is true");
            Assert.AreEqual(true, fileFoundReceived);

            movedFileReceived.WaitOne(11000, true);
            Debug.Print("See if move received is true");
            Assert.AreEqual("Could not create destination directory", errormessage);
            Assert.IsTrue(eventReceived);
        }

        /// <summary>
        /// Tests the invalid destination directory.
        /// </summary>
        [TestMethod]
        public void TestNullDestinationDirectory()
        {
            var newdir = this.CreateDirectory(@"C:\temp\frunittest4");

            var extensions = new List<string> { "dxf", "plt" };

            var filereplacer = new FileReplacer(newdir.FullName, null, extensions, 500);

            bool eventReceived = false;
            bool foundFileMsg = false;
            bool fileFoundReceived = false;
            string errormessage = string.Empty;

            ManualResetEvent foundFileReceived = new ManualResetEvent(false);
            ManualResetEvent movedFileReceived = new ManualResetEvent(false);

            filereplacer.FileSearchEvent += (sender, e) =>
                {
                    if (foundFileMsg)
                    {
                        DynamicFileEventState fileState = e;
                        Assert.AreEqual(DynamicFileEventState.DynamicState.Error, fileState.State);
                        errormessage = fileState.Error;
                        eventReceived = true;
                        movedFileReceived.Set();
                        Debug.Print("Move message received.");
                    }
                    else
                    {
                        fileFoundReceived = true;
                        foundFileMsg = true;
                        foundFileReceived.Set();
                        DynamicFileEventState fileState = e;
                        Debug.Print("Found file message received.");
                        Assert.AreEqual("fileName.dxf", fileState.FileName);
                        Assert.AreEqual(DynamicFileEventState.DynamicState.FoundFile, fileState.State);
                    }
                };

            // Let's enable the file replacer.
            filereplacer.Enabled(true);

            // Let's create a temporary file.
            var filetocreate = new FileInfo(Path.Combine(newdir.FullName, "fileName.dxf"));
            File.Create(filetocreate.FullName).Dispose();

            // Wait for the event.
            foundFileReceived.WaitOne(2000, true);
            Debug.Print("See if file found is true");
            Assert.AreEqual(true, fileFoundReceived);

            movedFileReceived.WaitOne(11000, true);
            Debug.Print("See if move received is true");
            Assert.AreEqual("Destination path invalid", errormessage);
            Assert.IsTrue(eventReceived);
        }

        /// <summary>
        /// Creates the directory.
        /// </summary>
        /// <param name="directoryToCreate">The directory to create.</param>
        /// <returns>Created director object.</returns>
        private DirectoryInfo CreateDirectory(string directoryToCreate)
        {
            var newdir = new DirectoryInfo(directoryToCreate);

            // Create a directory.
            if (newdir.Exists)
            {
                newdir.Delete(true);
            }

            newdir.Create();

            return newdir;
        }
    }
}
