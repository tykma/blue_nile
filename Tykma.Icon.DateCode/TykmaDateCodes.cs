﻿// <copyright file="TykmaDateCodes.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Icon.DateCode
{
    using System;

    /// <summary>
    /// Tykma date code generation.
    /// </summary>
    public class TykmaDateCodes : IDateCode
    {
        /// <summary>
        /// Gets the date code.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="time">The time.</param>
        /// <returns>Parsed date code.</returns>
        public string GetDateCode(string format, DateTime time)
        {
            string codes = format;

            if (codes.Contains("%week"))
            {
                codes = codes.Replace("%week", new Core.WeekOfYear.Iso8601().GetWeek(time).ToString().PadLeft(2, '0'));
            }

            foreach (var item in format.Split(new[] { '%' }, StringSplitOptions.RemoveEmptyEntries))
            {
                var parsedTime = time.ToString(item);

                if (!string.IsNullOrEmpty(parsedTime))
                {
                    codes = codes.Replace($"%{item}", parsedTime);
                }
            }

            return codes;
        }

        /// <summary>
        /// Gets the date code.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <returns>Parsed date code.</returns>
        public string GetDateCode(string format)
        {
            return this.GetDateCode(format, DateTime.Now);
        }
    }
}
