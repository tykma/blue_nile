﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileSearchEventMessage.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//  An enum that contains possible states of file import process.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.File_Management.CmdFileImporter
{
    /// <summary>
    /// File import state.
    /// </summary>
    internal enum FileImportState
    {
        /// <summary>
        /// File search has completed.
        /// </summary>
        Complete,

        /// <summary>
        /// File search is ready.
        /// </summary>
        Ready,

        /// <summary>
        /// File search is disabled.
        /// </summary>
        Disabled,

        /// <summary>
        /// Error in file search.
        /// </summary>
        Error,

        /// <summary>
        /// Parsing command file.
        /// </summary>
        Parsing,

        /// <summary>
        /// Searching for a file.
        /// </summary>
        Searching,
    }

    /// <summary>
    /// Status message to pass to events.
    /// </summary>
    internal class FileSearchEventMessage
    {
        /// <summary>
        /// Gets or sets the STATUS string.
        /// </summary>
        /// <value>
        /// The STATUS string.
        /// </value>
        public FileImportState Status { get; set; }
    }
}
