﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdObjectSetData.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   The command to set the data on a text object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;

    using CustomExceptions;

    /// <summary>
    /// Change the text of a string.
    /// </summary>
    internal class CmdObjectAddVector : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdObjectSetData"/> class.
        /// </summary>
        /// <param name="lmcApi">The LMC API.</param>
        /// <param name="textName">Name of the text.</param>
        /// <param name="data">The data.</param>
        public CmdObjectAddVector(IntPtr lmcApi, string fileName, string entName, double dPosX, double dPosY, double dPosZ, int nAlign, double dRatio, int pen, int bHatchFile)
            : base(lmcApi)
        {
            this.FileName = fileName;
            this.EntName = entName;
            this.PosX = dPosX;
            this.PosY = dPosY;
            this.PosZ = dPosZ;
            this.Align = nAlign;
            this.Ratio = dRatio;
            this.Hatch = bHatchFile;
            this.Pen = pen;
        }

        [DllImport("MarkPSE", EntryPoint = "lmc1_AddFileToLib", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern int AddFileToLib(string strFileName, string strEntName, double dPosX, double dPosY, double dPosZ, int nAlign, double dRatio, int nPenNo, int bHatchFile);


        private string FileName { get; }

        private string EntName { get; }

        private double PosX { get; }

        private double PosY { get; }

        private double PosZ { get; }

        private int Align { get; }

        private double Ratio { get; }

        public int Hatch { get; }

        public int Pen { get; }
        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        internal bool Execute()
        {


            // Call change text.
            int result = AddFileToLib(this.FileName, this.EntName, this.PosX, this.PosY, this.PosZ, this.Align,
                this.Ratio, this.Pen, this.Hatch);


            // Get success state.
            ReturnCodeMessage rm = this.GetMiniLaseReturnCode(result);

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
