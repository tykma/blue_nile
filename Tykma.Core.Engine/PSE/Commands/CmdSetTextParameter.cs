﻿namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;
    using Tykma.Core.Engine.CustomExceptions;

    internal class CmdSetTextParameter : CmdBase
    {
        private TextParameter TextParameters { get; }

        private string WhichEntity { get; }

        public CmdSetTextParameter(IntPtr lmcApi, string ent, TextParameter textParam) : base(lmcApi)
        {
            this.TextParameters = textParam;
            this.WhichEntity = ent;
        }

        private delegate int lmc1_SetTextEntParam4(
            [MarshalAs(UnmanagedType.LPWStr)] string EntName,
            [MarshalAs(UnmanagedType.LPWStr)] string fontname,
            int nTextSpaceMode,
            double dTextSpace,
            double CharHeight,
            double CharWidthRatio,
            double CharAngle,
            double CharSpace,
            double LineSpace,
            double spaceWidthRatio,
            int nTextAlign,
            bool bBold,
            bool bItalic);

        internal bool Execute()
        {
            var cmd = this.LoadFunction<lmc1_SetTextEntParam4>("lmc1_SetTextEntParam4");

            if (cmd == null)
            {
                throw new EngineNotConnectedException();
            }


            int result = cmd(
                this.WhichEntity,
                this.TextParameters.FontName,
                this.TextParameters.TextSpaceMode,
                this.TextParameters.FontSpace,
                this.TextParameters.CharacterHeight,
                this.TextParameters.CharacterWidthRatio,
                this.TextParameters.CharacterAngle,
                this.TextParameters.CharacterSpace,
                this.TextParameters.LineSpace,
                this.TextParameters.NullCharWidthRatio,
                this.TextParameters.TextAlign,
                this.TextParameters.IsBold,
                this.TextParameters.IsItalic);

            return true;

        }
    }
}
