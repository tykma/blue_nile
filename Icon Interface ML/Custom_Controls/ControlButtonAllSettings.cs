﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlButtonAllSettings.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    using Icon_Interface.Properties;

    /// <summary>
    /// The control button for a PLC.
    /// </summary>
    public partial class ControlButtonAllSettings : ControlButtonBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlButtonAllSettings"/> class.
        /// </summary>
        public ControlButtonAllSettings()
        {
            this.InitializeComponent();
            this.Customize();
        }

        /// <summary>
        /// The customization method..
        /// </summary>
        private void Customize()
        {
            this.Text = "View All";
        }
    }
}
