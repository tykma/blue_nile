﻿// <copyright file="Iso8601Verbose.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.WeekOfYear
{
    using System;

    /// <summary>
    /// ISO8601 compliant week of year calculator.
    /// </summary>
    public class Iso8601Verbose : IWeekOfYear
    {
        /// <summary>
        /// Get the week given a date.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns>Week of the year.</returns>
        public int GetWeek(DateTime date)
        {
            int a;
            int b;
            int c;
            int s;
            int e;
            int f;

            if (date.Month <= 2)
            {
                a = date.Year - 1;
                b = (a / 4) - (a / 100) + (a / 400);
                c = ((a - 1) / 4) - ((a - 1) / 100) + ((a - 1) / 400);
                s = b - c;
                e = 0;
                f = date.Day - 1 + (31 * (date.Month - 1));
            }
            else
            {
                a = date.Year;
                b = (a / 4) - (a / 100) + (a / 400);
                c = ((a - 1) / 4) - ((a - 1) / 100) + ((a - 1) / 400);
                s = b - c;
                e = s + 1;
                f = date.Day + (((153 * (date.Month - 3)) + 2) / 5) + 58 + s;
            }

            int g = (a + b) % 7;
            int d = (f + g - e) % 7;
            int n = f + 3 - d;

            if (n < 0)
            {
                return 53 - ((g - s) / 5);
            }

            if (n > (364 + s))
            {
                return 1;
            }

            return (n / 7) + 1;
        }
    }
}
