﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandReturn.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.TCPCommand
{
    using System.Collections.Generic;

    /// <summary>
    /// The command return.
    /// </summary>
    public class CommandReturn
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandReturn" /> class.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="kvp">The dictionary of data.</param>
        public CommandReturn(TcpCommandParse.CommandType command, Dictionary<string, string> kvp = null)
        {
            this.Command = command;
            this.CommandData = kvp ?? new Dictionary<string, string>();
        }

        /// <summary>
        /// Gets the command.
        /// </summary>
        public TcpCommandParse.CommandType Command { get; private set; }

        /// <summary>
        /// Gets the command data.
        /// </summary>
        public Dictionary<string, string> CommandData { get; private set; }
    }
}
