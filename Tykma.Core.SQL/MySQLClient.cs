﻿//-----------------------------------------------------------------------
// <copyright file="MySQLClient.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Tykma.Core.SQL.Client
{
    using System;
    using System.Data.SqlClient;
    using MySql.Data.MySqlClient;

    /// <summary>
    /// SQL Server class.
    /// </summary>
    public class MySQLClient : SQLClientBase, ISqlClient
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MySQLClient" /> class.
        /// </summary>
        /// <param name="connString">The connection string.</param>
        /// <param name="sqlTable">The SQL table.</param>
        /// <param name="sqlPrimaryField">The SQL primary field.</param>
        /// <param name="sqlJobColumn">The SQL job column.</param>
        /// <param name="sqlQuantityColumn">The SQL quantity column.</param>
        public MySQLClient(string connString, string sqlTable, string sqlPrimaryField, string sqlJobColumn, string sqlQuantityColumn)
        {
            this.ConnectionString = connString;
            this.SQLTable = sqlTable;
            this.SQLPrimaryField = sqlPrimaryField;
            this.SQLJobColumn = sqlJobColumn;
            this.SQLQuantityColumn = sqlQuantityColumn;
            this.ConnectionString = connString;
        }

        /// <summary>
        /// Writes the data.
        /// </summary>
        /// <param name="primkey">The primary key.</param>
        /// <param name="data">The data.</param>
        /// <param name="column">The column.</param>
        /// <returns>
        /// Success state.
        /// </returns>
        public bool SetData(string primkey, string data, string column)
        {
            using (var con = new MySqlConnection(this.ConnectionString))
            {
                con.Open();
                var builder = new MySqlCommandBuilder();
                string escapedTableName = builder.QuoteIdentifier(this.SQLTable);
                string escapedPrimaryField = builder.QuoteIdentifier(this.SQLPrimaryField);

                var command =
                    $"Update {escapedTableName} set {column}='{data}' where {escapedPrimaryField} LIKE '{primkey}'";
                using (var cmd = new MySqlCommand(command, con))
                {
                    cmd.ExecuteReader();
                }
            }

            return true;
        }

        /// <summary>
        /// Gets the data.
        /// </summary>
        /// <param name="uniqID">The unique identifier.</param>
        /// <returns>
        /// A SQL retrieved data class.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage(
            "Microsoft.Security",
            "CA2100:Review SQL queries for security vulnerabilities",
            Justification = "Table and primary field are set in config and are sanitized. Can't set table or field as a parameter.")]
        public SQLRetrievedData GetData(string uniqID)
        {
            var returnData = new SQLRetrievedData();
            try
            {
                using (var con = new MySqlConnection(this.ConnectionString))
                {
                    con.Open();

                    var builder = new MySqlCommandBuilder();
                    string escapedTableName = builder.QuoteIdentifier(this.SQLTable);
                    string escapedPrimaryField = builder.QuoteIdentifier(this.SQLPrimaryField);

                    var command = $"Select * FROM {escapedTableName} WHERE {escapedPrimaryField} LIKE @uniqField LIMIT 1";

                    using (var cmd = new MySqlCommand(command, con))
                    {
                        cmd.Parameters.AddWithValue("@uniqField", uniqID);

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    if (string.Compare(
                                        reader.GetName(i),
                                        this.SQLJobColumn,
                                        StringComparison.OrdinalIgnoreCase) == 0)
                                    {
                                        returnData.SetSQLJob(reader[i].ToString());
                                    }
                                    else if (string.Compare(reader.GetName(i), this.SQLQuantityColumn, StringComparison.OrdinalIgnoreCase) == 0)
                                    {
                                        returnData.SetQuantity(reader[i].ToString());
                                    }
                                    else
                                    {
                                        returnData.AddItemToList(reader.GetName(i), reader[i].ToString());
                                    }
                                }
                            }
                            else
                            {
                                returnData.SetFault("Value not found");
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                returnData.SetFault(e.Message);
            }

            return returnData;
        }
    }
}