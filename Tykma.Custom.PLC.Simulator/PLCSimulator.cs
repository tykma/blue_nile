﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tykma.Custom.PLC.Simulator
{
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Timers;
    using System.Windows.Forms;

    using Tykma.Core.PLC;

    using Timer = System.Timers.Timer;


    public class PLCSimulator : IPLCInterface
    {
        private Timer ConnectTimer;

        private Dictionary<string, Type> tagList;

        private string NameDataReadyTag = "Data_Ready_Tag";

        private string NameStartSequenceTag = "Start_Tag";

        private string NameStartRequest = "Start_Sequence_Tag";

        private PLCForm view;
        public PLCSimulator()
        {
            this.view = new PLCForm();
            this.view.Show();

            this.ConnectTimer = new Timer();
            this.ConnectTimer.Interval = 500;
            this.ConnectTimer.Elapsed += ConnectTimerOnElapsed;

            this.tagList = new Dictionary<string, Type>();

            this.view.PropertyChanged += ViewOnPropertyChanged;

        }


        private void ViewOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(nameof(this.view.DataReady)))
            {
                var tag = new TagValues(this.NameDataReadyTag, this.view.DataReady.ToString(), "Blah", "GOOD", DateTime.Now.ToString());

                this.TagChangeEvent?.Invoke(this, tag);
            }
            else if (e.PropertyName.Equals(nameof(this.view.StartSequence)))
            {
                var tag = new TagValues(this.NameStartSequenceTag, this.view.StartSequence.ToString(), "Blah", "GOOD", DateTime.Now.ToString());
                this.TagChangeEvent?.Invoke(this, tag);

            }
        }

        private void ConnectTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            var msg = new ConnectionStatusMessage();

            msg.Connected = true;

            this.ConnectionStatusEvent?.Invoke(this, msg);

            this.ConnectTimer.Stop();
            this.view.Connecting = "Connected";

        }

        public event EventHandler<ConnectionStatusMessage> ConnectionStatusEvent;

        public event EventHandler<TagValues> TagChangeEvent;

        public EPLCType PLCType
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public string IpAddress
        {
            get
            {
                return this.view.IP;
            }
            set
            {
                this.view.IP = value;
            }
        }

        private bool isConnected;

        public bool Connected
        {
            get
            {
                return this.isConnected;
            }
        }


        public bool Simulation { get; set; }

        public void Connect()
        {

            this.isConnected = true;
            this.view.Connecting = "Connecting";


            this.ConnectTimer.Start();

            //throw new NotImplementedException();
        }

        public void Disconnect()
        {
            //  throw new NotImplementedException();
        }



        public Task<bool> BoolWrite(string tagName, bool value)
        {
            if (tagName.Equals(this.NameDataReadyTag))
            {
                this.view.DataReady = value;
            }
            else if (tagName.Equals(this.NameStartRequest))
            {
                this.view.StartRequest = value;
            }
            else if (tagName.Equals(this.NameStartSequenceTag))
            {
                this.view.StartSequence = value;
            }

            return Task.FromResult(true);
            // throw new NotImplementedException();
        }

        public Task<bool> AutoTagAdd<T>(string tagName, bool importantGroup, TagTypes.ETagType tagType)
        {
            if (!this.tagList.ContainsKey(tagName))
            {
                this.tagList.Add(tagName, typeof(T).GetType());
            }
            return Task.FromResult(true);
        }

        public void ScanImportantGroup(bool enable)
        {
            //throw new NotImplementedException();
        }

        public void ScanUnimportantGroup(bool enable)
        {
            // throw new NotImplementedException();
        }

        public string TagReadUnimportant(string whichTag)
        {
            return "A";
            //throw new NotImplementedException();
        }

        public async Task<bool> WriteTag<T>(string tagName, object value)
        {
            if (typeof(T) == typeof(bool))
            {
                await this.BoolWrite(tagName, (bool)value);
            }
            else if (typeof(T) == typeof(string))
            {
                await this.GenericTagWrite(tagName, (string)value);
            }
            else if (typeof(T) == typeof(int))
            {
                await this.WriteInteger(tagName, (int)value);
            }

            return true;
        }

        public Task<bool> GenericTagWrite(string tagName, string tagValue)
        {
            return Task.FromResult(true);

            // throw new NotImplementedException();
        }

        public Task<bool> WriteInteger(string tagName, int value)
        {
            this.view.Diameter = value.ToString();
            Debug.Print(tagName + " " + value);
            return Task.FromResult(true);
        }

        public void ChangeScanTimes(int mainInterval, int secondaryInterval)
        {
            //throw new NotImplementedException();
        }

        public void Dispose()
        {
            // throw new NotImplementedException();
        }


    }
}
