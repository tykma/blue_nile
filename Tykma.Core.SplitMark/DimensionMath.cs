﻿// <copyright file="DimensionMath.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.SplitMark
{
    using System;
    using System.Collections.Generic;

    using Tykma.Core.Entity;

    /// <summary>
    /// Split mark dimensioning math.
    /// </summary>
    public class DimensionMath
    {
        /// <summary>
        /// Gets the boundaries of a list of objects.
        /// </summary>
        /// <param name="entities">The list of entities.</param>
        /// <returns>Box dimensions.</returns>
        public Box GetBoundariesOfObjects(IList<EntityInformation> entities)
        {
            double? maxX = null;
            double? minX = null;

            double? minY = null;
            double? maxY = null;

            foreach (var ent in entities)
            {
                if (maxX == null)
                {
                    maxX = ent.MaxX;
                }
                else
                {
                    if (maxX.Value < ent.MaxX)
                    {
                        maxX = ent.MaxX;
                    }
                }

                if (maxY == null)
                {
                    maxY = ent.MaxY;
                }
                else
                {
                    if (maxY.Value < ent.MaxY)
                    {
                        maxY = ent.MaxY;
                    }
                }

                if (minX == null)
                {
                    minX = ent.MinX;
                }
                else
                {
                    if (minX.Value > ent.MinX)
                    {
                        minX = ent.MinX;
                    }
                }

                if (minY == null)
                {
                    minY = ent.MinY;
                }
                else
                {
                    if (minY.Value > ent.MinY)
                    {
                        minY = ent.MinY;
                    }
                }
            }

            return new Box(minX.GetValueOrDefault(0), maxX.GetValueOrDefault(0), minY.GetValueOrDefault(0), maxY.GetValueOrDefault(0));
        }

        /// <summary>
        /// Split a box into multiple boxes by the split distance.
        /// </summary>
        /// <param name="splitDistance">The width of each resulting box.</param>
        /// <param name="dimensions">The box dimensions.</param>
        /// <returns>A list of boxes.</returns>
        public IList<Box> SplitIntoBoxes(double splitDistance, Box dimensions)
        {
            var boxlist = new List<Box>();

            if (Equals(splitDistance, 0.0))
            {
                return boxlist;
            }

            var xSplits = GeometryHelpers.GetDistance(dimensions.X1, 0, dimensions.X2, 0) / splitDistance;

            var minX = dimensions.X1;
            var maxX = dimensions.X2;

            for (int i = 0; i < xSplits; i++)
            {
                var x1 = minX + (i * splitDistance);
                var x2 = minX + ((i + 1) * splitDistance);

                if (x2 > maxX)
                {
                    x2 = maxX;
                }

                boxlist.Add(new Box(x1, x2, dimensions.Y1, dimensions.Y2));
            }

            return boxlist;
        }
    }
}
