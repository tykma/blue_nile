﻿// <copyright file="INISettingsView.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Icon_Interface.General
{
    using System;
    using System.ComponentModel;
    using Icon_Interface.Custom_Controls;
    using Icon_Interface.File_Management;

    /// <summary>
    /// Settings view model.
    /// </summary>
    [TypeConverter(typeof(PropertySorter))]
    public class INISettingsView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="INISettingsView"/> class.
        /// </summary>
        /// <param name="settings">Application settings.</param>
        /// <param name="sqlSettings">SQL settings.</param>
        public INISettingsView(IApplicationSettings settings, ISQLSettings sqlSettings)
        {
            this.Settings = settings;
            this.SQLSettings = sqlSettings;
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not data import is on.
        /// </summary>
        [Category("Data Import")]
        [DisplayName("Enabled")]
        [DescriptionAttribute("Whether data file import functionality is on or off.")]
        public bool DataImportEnabled
        {
            get { return this.Settings.DATASEARCHENABLED; }
            set { this.Settings.DATASEARCHENABLED = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether data import job load mode is on.
        /// </summary>
        [Category("Data Import")]
        [DisplayName("Load Job")]
        [DescriptionAttribute("Whether template is specified in file - or only field data.")]
        public bool DataImportJobLoad
        {
            get { return this.Settings.DATAJOBLOADMODE; }
            set { this.Settings.DATAJOBLOADMODE = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether clear on finish is enabled.
        /// </summary>
        [Category("Data Import")]
        [DisplayName("Clear on Finish")]
        [DescriptionAttribute("Whether to reset the sequence after marking one imported file.")]
        public bool DataImportSequenceReset
        {
            get { return this.Settings.CLEARAFTERFINISH; }
            set { this.Settings.CLEARAFTERFINISH = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether data import reset is enabled.
        /// </summary>
        [Category("Data Import")]
        [DisplayName("Clear Data on Finish")]
        [DescriptionAttribute("Whether to reset the imported data after marking one imported file.")]
        public bool DataImportDataReset
        {
            get { return this.Settings.CLEARDATAAFTERFINISH; }
            set { this.Settings.CLEARDATAAFTERFINISH = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether start marking on import is enabled.
        /// </summary>
        [Category("Data Import")]
        [DisplayName("Mark on Import")]
        [DescriptionAttribute("Whether to start marking after importing a file.")]
        public bool DataImportMarkOnImport
        {
            get { return this.Settings.DATAIMPORTSTARTONLOAD; }
            set { this.Settings.DATAIMPORTSTARTONLOAD = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to search after data has been imported.
        /// </summary>
        [Category("Data Import")]
        [DisplayName("Continue Search After Import")]
        [DescriptionAttribute("Continue search for a file even after a file is imported (disables queueing)")]
        public bool DataImportSearchAfterComplete
        {
            get { return this.Settings.DATAIMPORTSEARCHAFTERCOMPLETE; }
            set { this.Settings.DATAIMPORTSEARCHAFTERCOMPLETE = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the data import search path.
        /// </summary>
        [Category("Data Import")]
        [DisplayName("Search Folder")]
        [DescriptionAttribute("Directory with data files to import.")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string DataImportPath
        {
            get { return this.Settings.DATASEARCHFOLDER; }
            set { this.Settings.DATASEARCHFOLDER = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the data import complete path.
        /// </summary>
        [Category("Data Import")]
        [DisplayName("Done Folder")]
        [DescriptionAttribute("Directory that will contain completed files.")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string DataImportDonePath
        {
            get { return this.Settings.DATADONEFOLDER; }
            set { this.Settings.DATADONEFOLDER = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the data import error path.
        /// </summary>
        [Category("Data Import")]
        [DisplayName("Error Folder")]
        [DescriptionAttribute("Directory that will contain files that had import errors.")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string DataImportErrorePath
        {
            get { return this.Settings.DATAERRORFOLDER; }
            set { this.Settings.DATAERRORFOLDER = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the data import timer period.
        /// </summary>
        [Category("Data Import")]
        [DisplayName("Search Speed")]
        [DescriptionAttribute("Interval to poll for new file.")]
        public int DataSearchInterval
        {
            get { return this.Settings.DATASEARCHSPEED; }
            set { this.Settings.DATASEARCHSPEED = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the data import delay before reading a file.
        /// </summary>
        [Category("Data Import")]
        [DisplayName("Wait Delay")]
        [DescriptionAttribute("Delay before reading a file after it has been found.")]
        public int DataSearchReadDelay
        {
            get { return this.Settings.DATAWAITFORREAD; }
            set { this.Settings.DATAWAITFORREAD = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to manually set the complete output.
        /// </summary>
        [Category("Inputs and Outputs")]
        [DisplayName("Set Complete Bit")]
        [DescriptionAttribute("Use icon to set cycle complete bit (For rotary or array marking).")]
        public bool SetCompleteBit
        {
            get { return this.Settings.IOSETCOMPLETE; }
            set { this.Settings.IOSETCOMPLETE = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the duration of the cycle complete pulse.
        /// </summary>
        [Category("Inputs and Outputs")]
        [DisplayName("Length of Cycle Complete")]
        [DescriptionAttribute("Length of cycle complete output.")]
        public int CompleteLength
        {
            get { return this.Settings.IOCOMPLETLENGTH; }
            set { this.Settings.IOCOMPLETLENGTH = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the instructions form is enabled.
        /// </summary>
        [CustomSortedCategoryAttribute("Instructions", 4, 15)]
        [DisplayName("Enable Instructions")]
        [DescriptionAttribute("Whether instructions form is enabled.")]
        [PropertyOrder(1)]
        public bool EnableInstructions
        {
            get { return this.Settings.INSTRUCTIONSENABLE; }
            set { this.Settings.INSTRUCTIONSENABLE = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the instructions RTF path.
        /// </summary>
        [CustomSortedCategoryAttribute("Instructions", 4, 15)]
        [DisplayName("RTF Folder")]
        [DescriptionAttribute("Directory that will contain RTF files.")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [PropertyOrder(2)]
        public string RTFPath
        {
            get { return this.Settings.INSTRUCTIONSRTFFOLDER; }

            set { this.Settings.INSTRUCTIONSRTFFOLDER = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the instructions images path.
        /// </summary>
        [CustomSortedCategoryAttribute("Instructions", 4, 15)]
        [DisplayName("Images Folder")]
        [DescriptionAttribute("Directory that will contain image files.")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [PropertyOrder(3)]
        public string ImagesPath
        {
            get { return this.Settings.INSTRUCTIONSIMAGEFOLDER; }

            set { this.Settings.INSTRUCTIONSIMAGEFOLDER = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the expanded job list is enabled.
        /// </summary>
        [CustomSortedCategoryAttribute("UI", 2, 15)]
        [DisplayName("Expanded Job List")]
        [DescriptionAttribute("Whether option to view expanded job list is enabled.")]
        public bool ExpandedJobList
        {
            get { return this.Settings.TREEJOBLIST; }
            set { this.Settings.TREEJOBLIST = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the counter is enabled.
        /// </summary>
        [CustomSortedCategoryAttribute("UI", 2, 15)]
        [DisplayName("Enable Counter")]
        [DescriptionAttribute("Whether cycle counter is enabled.")]
        public bool EnableCounter
        {
            get { return this.Settings.USECOUNTER; }
            set { this.Settings.USECOUNTER = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the counter is not decremented.
        /// </summary>
        [CustomSortedCategoryAttribute("UI", 2, 15)]
        [DisplayName("Non Decrementing Counter")]
        [DescriptionAttribute("If enabled the counter does not decrement. (Infinite array mode)")]
        public bool NonDecrementingCounter
        {
            get { return this.Settings.COUNTERNODECREMENT; }
            set { this.Settings.COUNTERNODECREMENT = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the start button is hiddne.
        /// </summary>
        [CustomSortedCategoryAttribute("UI", 2, 15)]
        [DisplayName("Hide Start Button")]
        [DescriptionAttribute("Whether the start icon is hidden.")]
        public bool HideStart
        {
            get { return this.Settings.HIDESTARTBUTTON; }
            set { this.Settings.HIDESTARTBUTTON = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the edit button is hidden.
        /// </summary>
        [CustomSortedCategoryAttribute("UI", 2, 15)]
        [DisplayName("Hide Edit Button")]
        [DescriptionAttribute("Whether the edit icon is hidden.")]
        public bool HideEdit
        {
            get { return this.Settings.HIDEEDITBUTTON; }
            set { this.Settings.HIDEEDITBUTTON = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the limits button is hidden.
        /// </summary>
        [CustomSortedCategoryAttribute("UI", 2, 15)]
        [DisplayName("Hide Limits Button")]
        [DescriptionAttribute("Whether the limits icon is hidden.")]
        public bool HideLimits
        {
            get { return this.Settings.HIDELIMITSBUTTON; }
            set { this.Settings.HIDELIMITSBUTTON = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to hide the job list or not.
        /// </summary>
        [CustomSortedCategoryAttribute("UI", 2, 15)]
        [DisplayName("Hide Job List")]
        [DescriptionAttribute("Whether the job list is hidden.")]
        public bool HideJobList
        {
            get { return this.Settings.HIDEJOBLIST; }
            set { this.Settings.HIDEJOBLIST = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the serial entry input is enabled.
        /// </summary>
        [CustomSortedCategoryAttribute("UI", 2, 15)]
        [DisplayName("Show Serial Input Control")]
        [DescriptionAttribute("Whether to show the serial input control on main screen.")]
        public bool ShowSerialInput
        {
            get { return this.Settings.SHOWSNINPUT; }
            set { this.Settings.SHOWSNINPUT = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to show the rotary mark settings.
        /// </summary>
        [Category("Rotary Mark")]
        [DisplayName("Show Rotary Mark Settings")]
        [DescriptionAttribute("Whether to show the rotary mark dialog (Z-height/diameter).")]
        public bool ShowRotaryMarkSettings
        {
            get { return this.Settings.SHOWROTARYSETTINGS; }
            set { this.Settings.SHOWROTARYSETTINGS = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the system is in tag feeder mode.
        /// </summary>
        [CustomSortedCategoryAttribute("Advanced", 15, 15)]
        [DisplayName("Tag Feeder Mode")]
        [DescriptionAttribute("Whether to run system in tag feeder mode.")]
        public bool TagFeederMode
        {
            get { return this.Settings.TAGFEEDER; }
            set { this.Settings.TAGFEEDER = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether data will be cleared on cycle finish.
        /// </summary>
        [CustomSortedCategoryAttribute("Advanced", 15, 15)]
        [DisplayName("Clear Data on Finish")]
        [DescriptionAttribute("If enabled data will be cleared after a sequence is ran.")]
        public bool ClearData
        {
            get { return this.Settings.DATACLEARONFINISH; }
            set { this.Settings.DATACLEARONFINISH = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether limits are always turned on.
        /// </summary>
        [CustomSortedCategoryAttribute("Advanced", 15, 15)]
        [DisplayName("Always on Limits")]
        [DescriptionAttribute("Whether the limits/trace will always turn on")]
        public bool LimitsAlwaysOn
        {
            get { return this.Settings.ALWAYSONLIMITS; }
            set { this.Settings.ALWAYSONLIMITS = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the queue functionality is enabled.
        /// </summary>
        [CustomSortedCategoryAttribute("Advanced", 15, 15)]
        [DisplayName("Queue Enabled")]
        [DescriptionAttribute("Whether to enable the queue form.")]
        public bool QueueEnabled
        {
            get { return this.Settings.USEQUEUE; }
            set { this.Settings.USEQUEUE = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the preview image angle.
        /// </summary>
        [CustomSortedCategoryAttribute("Advanced", 15, 15)]
        [DisplayName("Preview Image Angle")]
        [DescriptionAttribute("Angle to rotate the preview image.")]
        public int PreviewAngle
        {
            get { return this.Settings.PREVIEWANGLE; }
            set { this.Settings.PREVIEWANGLE = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the system is in mark on the fly mode.
        /// </summary>
        [CustomSortedCategoryAttribute("Advanced", 15, 15)]
        [DisplayName("Mark on the Fly Mode")]
        [DescriptionAttribute("Whether to enable mark on the fly mode.")]
        public bool MOTF
        {
            get { return this.Settings.MOTFENABLED; }
            set { this.Settings.MOTFENABLED = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show the nudge form.
        /// </summary>
        [Category("Nudge Form")]
        [DisplayName("Enabled")]
        [DescriptionAttribute("Whether the nudge form is enabled.")]
        public bool UseNudgeForm
        {
            get { return this.Settings.USENUDGEFORM; }
            set { this.Settings.USENUDGEFORM = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the nudge form distance value.
        /// </summary>
        [Category("Nudge Form")]
        [DisplayName("Distance")]
        [DescriptionAttribute("Nudging distance.")]
        public double NudgeDistance
        {
            get { return this.Settings.NUDGEDISTANCE; }
            set { this.Settings.NUDGEDISTANCE = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the TCP/IP API is enabled.
        /// </summary>
        [Category("TCP/IP API")]
        [DisplayName("Enabled")]
        [DescriptionAttribute("Whether the TCP/IP API is enabled.")]
        public bool EnableTCP
        {
            get { return this.Settings.NETENABLE; }
            set { this.Settings.NETENABLE = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the job needs to be enabled via tcp/ip.
        /// </summary>
        [Category("TCP/IP API")]
        [DisplayName("Job Enables via API")]
        [DescriptionAttribute("Whether a job must be enabled via TCP/IP before it can run.")]
        public bool NetJobEnable
        {
            get { return this.Settings.NETENABLESJOB; }
            set { this.Settings.NETENABLESJOB = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the TCP/IP API port.
        /// </summary>
        [Category("TCP/IP API")]
        [DisplayName("Port")]
        [DescriptionAttribute("Port to use for the TCP/IP API.")]
        public int TCPIPPort
        {
            get { return this.Settings.NETTCPPORT; }
            set { this.Settings.NETTCPPORT = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to home the axes on startup.
        /// </summary>
        [CustomSortedCategoryAttribute("Axis", 3, 15)]
        [DisplayName("Home Axes on Startup")]
        [DescriptionAttribute("Whether the axes will home on software load.")]
        [PropertyOrder(1)]
        public bool HomeAxesStartup
        {
            get { return this.Settings.HOMEONOPEN; }
            set { this.Settings.HOMEONOPEN = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the Z axis is present.
        /// </summary>
        [CustomSortedCategoryAttribute("Axis", 3, 15)]
        [DisplayName("Z Axis Present")]
        [DescriptionAttribute("Whether the Z axis is present.")]
        [PropertyOrder(2)]
        public bool AxisZPresent
        {
            get { return this.Settings.ZAXISPRESENT; }
            set { this.Settings.ZAXISPRESENT = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the Z axis home frequency.
        /// </summary>
        [CustomSortedCategoryAttribute("Axis", 3, 15)]
        [DisplayName("Z Axis Home Frequency")]
        [DescriptionAttribute("After how many cycles the Z axis will be forced to home.")]
        [PropertyOrder(3)]
        public int AxisZHomeFrequency
        {
            get { return this.Settings.ZHOMEFREQUENCY; }
            set { this.Settings.ZHOMEFREQUENCY = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to home the Z on every new job.
        /// </summary>
        [CustomSortedCategoryAttribute("Axis", 3, 15)]
        [DisplayName("Z Axis Home on New Job")]
        [DescriptionAttribute("Whether the Z axis will be homed on every new job that is loaded.")]
        [PropertyOrder(4)]
        public bool HomeZOnNewJob
        {
            get { return this.Settings.ZAXISHOMEONNEWJOB; }
            set { this.Settings.ZAXISHOMEONNEWJOB = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the R axis is present.
        /// </summary>
        [CustomSortedCategoryAttribute("Axis", 3, 15)]
        [DisplayName("R Axis Present")]
        [DescriptionAttribute("Whether the R axis is present.")]
        [PropertyOrder(5)]
        public bool AxisRPresent
        {
            get { return this.Settings.RAXISPRESENT; }
            set { this.Settings.RAXISPRESENT = value; }
        }

        /// <summary>
        /// Gets or sets a value indicaitng the R axis home frequency.
        /// </summary>
        [CustomSortedCategoryAttribute("Axis", 3, 15)]
        [DisplayName("R Axis Home Frequency")]
        [DescriptionAttribute("After how many cycles the R axis will be forced to home.")]
        [PropertyOrder(6)]
        public int AxisRHomeFrequency
        {
            get { return this.Settings.RHOMEFREQUENCY; }
            set { this.Settings.RHOMEFREQUENCY = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the operator is able to select jobs.
        /// </summary>
        [Category("Job Loading")]
        [DisplayName("Operator Can Load Job")]
        [DescriptionAttribute("Whether or not an operator can select and load a job.")]
        public bool OperatorCanLoadJob
        {
            get { return this.Settings.OPERATORCANLOADJOB; }
            set { this.Settings.OPERATORCANLOADJOB = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to reload the previously loaded job on startup.
        /// </summary>
        [Category("Job Loading")]
        [DisplayName("Reload Last Job on Startup")]
        [DescriptionAttribute("If enabled the last loaded job will be loaded when software reopens.")]
        public bool ReloadLastJob
        {
            get { return this.Settings.ENABLELASTJOBLOAD; }
            set { this.Settings.ENABLELASTJOBLOAD = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the custom datecode ID.
        /// </summary>
        [Category("Custom Datecode")]
        [DisplayName("ID of Custom DateCode")]
        [DescriptionAttribute("Name of the field that will have a custom datecode set.")]
        public string CustomDateCodeID
        {
            get { return this.Settings.DATECODEID; }
            set { this.Settings.DATECODEID = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the custom datecode formatting.
        /// </summary>
        [Category("Custom Datecode")]
        [DisplayName("Datecode Format")]
        [DescriptionAttribute("Custom datecode format.")]
        public string CustomDateCodeFormat
        {
            get { return this.Settings.DATECODEFORMAT; }
            set { this.Settings.DATECODEFORMAT = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the settings path.
        /// </summary>
        [CustomSortedCategoryAttribute("Paths", 1, 15)]
        [DisplayName("Settings Folder")]
        [DescriptionAttribute("Directory that contains the settings files")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string SettingsPath
        {
            get { return this.Settings.SETTINGSFOLDER; }
            set { this.Settings.SETTINGSFOLDER = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the minilase pro se installation path.
        /// </summary>
        [CustomSortedCategoryAttribute("Paths", 1, 15)]
        [DisplayName("Minilase Pro SE Folder")]
        [DescriptionAttribute("Directory that contains Minilase Pro SE.")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string SEPath
        {
            get { return this.Settings.SEINSTALLDIR; }
            set { this.Settings.SEINSTALLDIR = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the path to the projects.
        /// </summary>
        [CustomSortedCategoryAttribute("Paths", 1, 15)]
        [DisplayName("Projects Path")]
        [DescriptionAttribute("Directory that contains laser project files.")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string ProjectsPath
        {
            get { return this.Settings.PROJECTSFOLDER; }
            set { this.Settings.PROJECTSFOLDER = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether simple mark mode is enabled.
        /// </summary>
        [Category("Simple Mark Mode")]
        [DisplayName("Enable Simple Mark Mode")]
        [DescriptionAttribute("If simple mark mode is enabled - ID scanning directly into scanner input box.")]
        public bool SimpleMarkMode
        {
            get { return this.Settings.SIMPLEMARKMODE; }
            set { this.Settings.SIMPLEMARKMODE = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the prefix and suffix of the first simple mark ID.
        /// </summary>
        [Category("Simple Mark Mode")]
        [DisplayName("Prefix and Suffix of First Simple Mark ID")]
        [DescriptionAttribute("A scanned value surrounded by this value will be treated as the first ID.")]
        public string SimpleMarkModeFirstPrefSuf
        {
            get { return this.Settings.SIMPLEMARKFIRSTIDPREFSUFF; }
            set { this.Settings.SIMPLEMARKFIRSTIDPREFSUFF = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to strip the prefix and suffix in simple mark mode.
        /// </summary>
        [Category("Simple Mark Mode")]
        [DisplayName("Strip Prefix and Suffix")]
        [DescriptionAttribute("Whether the surrounding characters will be stripped.")]
        public bool SimpleMarkModeStripPrefSuf
        {
            get { return this.Settings.SIMPLEMARKSTRIPSUFFIX; }
            set { this.Settings.SIMPLEMARKSTRIPSUFFIX = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the first ID in simple mark mode.
        /// </summary>
        [Category("Simple Mark Mode")]
        [DisplayName("First ID")]
        [DescriptionAttribute("First ID Name.")]
        public string SimpleMarkModeFirstID
        {
            get { return this.Settings.SIMPLEMARKFIRSTID; }
            set { this.Settings.SIMPLEMARKFIRSTID = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the second ID in simple mark mode.
        /// </summary>
        [Category("Simple Mark Mode")]
        [DisplayName("Second ID")]
        [DescriptionAttribute("Second ID Name.")]
        public string SimpleMarkModeSecondID
        {
            get { return this.Settings.SIMPLEMARKSECONDID; }
            set { this.Settings.SIMPLEMARKSECONDID = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether dynamic file import is enabled.
        /// </summary>
        [Category("Dynamic File Import")]
        [DisplayName("Enable Dynamic File Import")]
        [DescriptionAttribute("If enabled files will be moved from a source to a destination if found at source.")]
        public bool DynamicFileImport
        {
            get { return this.Settings.USEDYNAMICIMPORT; }
            set { this.Settings.USEDYNAMICIMPORT = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the search interval for dynamic file import.
        /// </summary>
        [Category("Dynamic File Import")]
        [DisplayName("Search Speed")]
        [DescriptionAttribute("Interval to poll for new file.")]
        public int DynamicFileImportInterval
        {
            get { return this.Settings.DYNAMICIMPORTPOLLRATE; }
            set { this.Settings.DYNAMICIMPORTPOLLRATE = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the source path for dynamic file import.
        /// </summary>
        [Category("Dynamic File Import")]
        [DisplayName("Source Path")]
        [DescriptionAttribute("Directory that contains vectors/PLTs.")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string SourcePathDynamic
        {
            get { return this.Settings.DYNAMICSRCFOLDER; }
            set { this.Settings.DYNAMICSRCFOLDER = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the destination path for dynamic file import.
        /// </summary>
        [Category("Dynamic File Import")]
        [DisplayName("Destination File Path")]
        [DescriptionAttribute("Vector file output name.")]
        [EditorAttribute(typeof(System.Windows.Forms.Design.FileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string DestPathDynamic
        {
            get { return this.Settings.DYNAMICDSTFILE; }
            set { this.Settings.DYNAMICDSTFILE = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the list of extensions in dynamic file import.
        /// </summary>
        [Category("Dynamic File Import")]
        [DisplayName("File Extensions")]
        [DescriptionAttribute("Comma separated list of file types that will be imported.")]
        public string DestFileTypes
        {
            get { return this.Settings.DYNAMICEXTENSIONS; }
            set { this.Settings.DYNAMICEXTENSIONS = value; }
        }

        /// <summary>
        /// Gets a value indicating whether or not SQL server functionality is enabled.
        /// </summary>
        [Category("Database")]
        [DisplayName("SQL Enabled")]
        [PropertyOrder(1)]
        [DescriptionAttribute("If SQL server connectivity is enabled.")]
        public bool SQLServerEnabled
        {
            get
            {
                return this.SQLSettings.USESQLSERVER;
            }

            set
            {
                this.SQLSettings.USESQLSERVER = value;
            }
        }

        /// <summary>
        /// Gets or sets the database type.
        /// </summary>
        [Category("Database")]
        [DisplayName("Provider")]
        [DescriptionAttribute("The database type.")]
        [TypeConverter(typeof(SQLOptionsStringConverter))]
        [PropertyOrder(2)]
        public string SQLDBProvider
        {
            get => this.SQLSettings.DatabaseType;

            set => this.SQLSettings.DatabaseType = value;
        }

        /// <summary>
        /// Gets or sets the SQL connection string.
        /// </summary>
        [Category("Database")]
        [DisplayName("Connection String")]
        [DescriptionAttribute("The SQL server connection string.")]
        [PropertyOrder(3)]
        public string SQLConnectionString
        {
            get => this.SQLSettings.SQLCONNSTRING;

            set => this.SQLSettings.SQLCONNSTRING = value;
        }

        /// <summary>
        /// Gets or sets the SQL table.
        /// </summary>
        [Category("Database")]
        [DisplayName("Table")]
        [DescriptionAttribute("The database table.")]
        [PropertyOrder(4)]
        public string SQLTable
        {
            get => this.SQLSettings.SQLTABLE;

            set => this.SQLSettings.SQLTABLE = value;
        }

        /// <summary>
        /// Gets or sets the SQL primary key.
        /// </summary>
        [Category("Database")]
        [DisplayName("Primary Key Column")]
        [DescriptionAttribute("Primary key column name.")]
        [PropertyOrder(5)]
        public string SQLPrimaryKey
        {
            get => this.SQLSettings.SQLPRIMARYFIELD;

            set => this.SQLSettings.SQLPRIMARYFIELD = value;
        }

        /// <summary>
        /// Gets or sets a value for the job column name.
        /// </summary>
        [PropertyOrder(6)]
        [Category("Database")]
        [DisplayName("Template Column")]
        [DescriptionAttribute("Column that identifies the template to load.")]
        public string SQLJobColumn
        {
            get => this.SQLSettings.SQLJOBCOLUMN;

            set => this.SQLSettings.SQLJOBCOLUMN = value;
        }

        /// <summary>
        /// Gets or sets a value for the quantity column name.
        /// </summary>
        [Category("Database")]
        [DisplayName("Quantity Column")]
        [DescriptionAttribute("Column that identifies the quantity to set.")]
        [PropertyOrder(7)]
        public string SQLQuantityColumn
        {
            get => this.SQLSettings.SQLQTYCOLUMN;

            set => this.SQLSettings.SQLQTYCOLUMN = value;
        }

        [Category("Custom")]
        [DisplayName("Font Height Multiplier.")]
        [DescriptionAttribute("Font height multiplier")]
        public double FontHeightMultiplier
        {
            get { return this.Settings.FONTSIZEMULTIPLIER; }

            set
            {
                this.Settings.FONTSIZEMULTIPLIER = value;
            }
        }


        [Category("Custom")]
        [DisplayName("Font Height Maintain aspect")]
        [DescriptionAttribute("Maintain aspect ratio when changing font height")]
        public bool FontHeightMaintainAspect
        {
            get { return this.Settings.HEIGHTKEEPASPECT; }

            set
            {
                this.Settings.HEIGHTKEEPASPECT = value;
            }
        }

        [Category("Custom")]
        [DisplayName("Nudge amount.")]
        [DescriptionAttribute("Nudge amount")]
        public double NudgeAmount
        {
            get { return (double)this.Settings.WIDTHNUDGESIZE; }

            set
            {
                this.Settings.WIDTHNUDGESIZE = (decimal)value;
            }
        }


        /// <summary>
        /// Gets our settings class.
        /// </summary>
        private IApplicationSettings Settings { get; }

        /// <summary>
        /// Gets our SQL settings class.
        /// </summary>
        private ISQLSettings SQLSettings { get; }
    }
}
