﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EGlobalTypes.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Types of global variables.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.Globals
{
    /// <summary>
    /// Class that holds available global variable type enums.
    /// </summary>
    public class EGlobalTypes
    {
        /// <summary>
        /// The type of global variable.
        /// </summary>
        public enum GlobalVariableType
       {
            /// <summary>
            /// The variable is an integer
            /// </summary>
            Counter_Integer,

            /// <summary>
            /// The variable is a string
            /// </summary>
            String,

            /// <summary>
            /// The vairable is an alphanumeric counter.
            /// </summary>
            Counter_Alpha,
        }
    }
}
