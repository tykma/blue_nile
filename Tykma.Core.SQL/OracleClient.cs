﻿// <copyright file="OracleClient.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.SQL.Client
{
    using System;

    using System.Data;

    using Oracle.ManagedDataAccess.Client;

    /// <summary>
    /// Oracle client using ODP.net (managed).
    /// </summary>
    public class OracleClient : SQLClientBase, ISqlClient
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OracleClient"/> class.
        /// </summary>
        /// <param name="connString">The connection string.</param>
        /// <param name="sqlTable">The SQL table.</param>
        /// <param name="sqlPrimaryField">The SQL primary field.</param>
        /// <param name="sqlJobColumn">The SQL job column.</param>
        /// <param name="sqlQuantityColumn">Quantity column name.</param>
        public OracleClient(string connString, string sqlTable, string sqlPrimaryField, string sqlJobColumn, string sqlQuantityColumn)
        {
            this.ConnectionString = connString;
            this.SQLPrimaryField = sqlPrimaryField;
            this.SQLTable = sqlTable;
            this.SQLJobColumn = sqlJobColumn;
            this.SQLQuantityColumn = sqlQuantityColumn;
        }

        /// <summary>
        /// Gets the data.
        /// </summary>
        /// <param name="uniqID">The unique identifier.</param>
        /// <returns>A data object with retrieved sql information.</returns>
        public SQLRetrievedData GetData(string uniqID)
        {
            var returnData = new SQLRetrievedData();

            try
            {
                using (var con = new OracleConnection { ConnectionString = this.ConnectionString })
                {
                    // Open connection.
                    con.Open();
                    string selectCommand = $"SELECT * from {this.SQLTable} where {this.SQLPrimaryField} LIKE :uniqID";

                    OracleCommand cmd = new OracleCommand(selectCommand, con)
                    {
                        BindByName = true,
                        CommandType = CommandType.Text,
                    };

                    cmd.Parameters.Add(new OracleParameter("uniqID", uniqID));

                    var oraReader = cmd.ExecuteReader();

                    if (oraReader.HasRows)
                    {
                        if (oraReader.Read())
                        {
                            for (int i = 0; i < oraReader.FieldCount; i++)
                            {
                                if (string.Compare(
                                    oraReader.GetName(i),
                                    this.SQLJobColumn,
                                    StringComparison.OrdinalIgnoreCase) == 0)
                                {
                                    returnData.SetSQLJob(oraReader[i].ToString());
                                }
                                else if (string.Compare(oraReader.GetName(i), this.SQLQuantityColumn, StringComparison.OrdinalIgnoreCase) == 0)
                                {
                                    returnData.SetQuantity(oraReader[i].ToString());
                                }
                                else
                                {
                                    returnData.AddItemToList(oraReader.GetName(i), oraReader[i].ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                returnData.SetFault(e.Message);
            }

            return returnData;
        }

        /// <summary>
        /// Writes data to an oracle database.
        /// </summary>
        /// <param name="primkey">The primary key.</param>
        /// <param name="data">The data.</param>
        /// <param name="column">The column.</param>
        /// <returns>
        /// Sucess state.
        /// </returns>
        /// <exception cref="System.NotImplementedException">Not implmented.</exception>
        public bool SetData(string primkey, string data, string column)
        {
            throw new NotImplementedException();
        }
    }
}
