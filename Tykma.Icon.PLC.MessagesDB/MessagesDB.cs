﻿using System.IO.Abstractions;

namespace Tykma.Icon.PLC.MessagesDB
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using CsvHelper;

    public class MessagesDB
    {
        /// <summary>
        /// The database location.
        /// </summary>
        private readonly FileInfo dbLocation;

        private IFileSystem FS { get; }
        private List<PLCMessageItem> StatusCodes { get; set; }

        public PLCMessageItem GetMessage(int code)
        {
            foreach (var item in this.StatusCodes)
            {
                if (item.Code.Equals(code))
                {
                    return item;
                }
            }

            return new PLCMessageItem();
        }

        public MessagesDB(string filePath) : this(filePath, new FileSystem())
        {

        }

        public MessagesDB(string filePath, IFileSystem fs)
        {
            this.FS = fs;

            if (filePath != null)
            {
                this.dbLocation = new FileInfo(filePath);
            }

            if (!this.FS.File.Exists(this.dbLocation.FullName))
            {
                this.CreateDB();
            }

            this.StatusCodes = new List<PLCMessageItem>();
        }

        private void CreateDB()
        {
            if (this.dbLocation == null)
            {
                throw new InvalidOperationException("Path is null.");
            }

            using (var writer = this.FS.File.CreateText(this.dbLocation.FullName))
            {
                var csv = new CsvWriter(writer);

                var msg = new PLCMessageItem();
                msg.Code = 0;
                msg.Color = "White";
                msg.Message = "Ready";

                csv.WriteHeader<PLCMessageItem>();
                csv.NextRecord();
                csv.WriteRecord<PLCMessageItem>(msg);
                csv.NextRecord();
            }
        }

        public void LoadDB()
        {
            if (this.dbLocation == null)
            {
                throw new InvalidOperationException("Path is null.");
            }

            if (!this.FS.File.Exists(this.dbLocation.FullName))
            {
                throw new FileNotFoundException();
            }

            using (TextReader reader = this.FS.File.OpenText(this.dbLocation.FullName))
            {
                var csv = new CsvReader(reader);

                // csv.Configuration.RegisterClassMap(map);

                var map = new Factory().CreateClassMapBuilder<PLCMessageItem>()
                    .Map(m => m.Code).Index(0)
                    .Map(m => m.Color).Index(1)
                    .Map(m => m.Message).Index(2)
                    .Build();

                csv.Configuration.RegisterClassMap(map);
                csv.Configuration.HeaderValidated = null;
                csv.Configuration.HasHeaderRecord = true;

                var records = csv.GetRecords<PLCMessageItem>();

                foreach (var item in records)
                {
                    this.StatusCodes.Add(item);
                }
            }
        }
    }
}
