﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tykma.Icon.RotaryMark.Logic
{
    using System.ComponentModel;

    public class RotaryMark
    {
        public event EventHandler<bool> ParametersUpdated;

        private RotaryForm form;

        public RotaryMark()
        {
            this.form = new RotaryForm();
            this.form.PropertyChanged += FormOnPropertyChanged;
        }

        private void FormOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            this.ParametersUpdated?.Invoke(new object(), true);
        }

        public double Diameter => (double)this.form.Diameter;

        public double SplitSize => (double)this.form.SplitSize;

        public double Z => (double)this.form.Z;

        public bool IsVisible { get
            {
                return this.form.Visible;
            }
        }
        public void Set(string diameter, string splitSize, double z, bool show)
        {
            decimal res;
            this.form.Diameter = decimal.TryParse(diameter, out res) ? res : 0;
            this.form.SplitSize = decimal.TryParse(splitSize, out res) ? res : 0;
            this.form.Z = (decimal)z;

            if (show)
            {
                this.form.Show();
            }
        }

        public void Hide()
        {
            this.form.Hide();
        }
    }
}
