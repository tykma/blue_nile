﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlButtonGeneral.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    using Icon_Interface.Properties;

    /// <summary>
    /// The control button: general.
    /// </summary>
    public partial class ControlButtonGeneral : ControlButtonBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlButtonGeneral"/> class.
        /// </summary>
        public ControlButtonGeneral()
        {
            this.InitializeComponent();
            this.Customization();
        }

        /// <summary>
        /// The customization.
        /// </summary>
        private void Customization()
        {
            this.Text = Resources.Label_General;
        }
    }
}
