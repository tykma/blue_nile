﻿using System;
using Tykma.Integration.ExternalDB;

namespace Tykma.Custom.BlueNile.DBLogic
{
    public class BlueNileDBLogic
    {
        public IDBLogic Database { get; }
        private string TemplateColumn { get; }
        public BlueNileDBLogic(string connString, string table, string templatecolumn)
        {
            string primarycolumn = "JOB_NUMBER";
            this.TemplateColumn = templatecolumn;
            this.Database = new Tykma.Integration.ExternalDB.DBLogic(connString, "Oracle.ManagedDataAccess.Client", primarycolumn, "", table, false);
        }


        public BlueNileDataObject GetBlueNileData(string primkey)
        {
            var data = this.Database.GetData(primkey);

            if (data.DataFields.Count == 0)
            {
                throw new MissingFieldException($"Data for {primkey} not found in database.");
            }

            data.DataFields.TryGetValue("JOB_NUMBER", out string jobNumber);
            data.DataFields.TryGetValue("ENGRAVE_TEXT", out string engravetext);
            data.DataFields.TryGetValue("ENGRAVING_AREA_HEIGHT", out string engraveheight);
            double.TryParse(engraveheight, out double dengraveheight);
            data.DataFields.TryGetValue("ENGRAVE_TEMPLATE", out string engrave_template);
            data.DataFields.TryGetValue("METAL_TYPE", out string metal_type);
            data.DataFields.TryGetValue("ENGRAVE_MAX_CHARACTERS", out string engm);
            int.TryParse(engm, out int engravemaxchars);
            data.DataFields.TryGetValue("FONT_LAYOUT", out string fl);
            data.DataFields.TryGetValue("FONT_NAME", out string fn);
            data.DataFields.TryGetValue(this.TemplateColumn, out string template);

            bool insidefont = false;
            if (fl != null)
            {
                if (fl.Equals("Inside", StringComparison.InvariantCultureIgnoreCase))
                {
                    insidefont = true;
                }
            }

            data.DataFields.TryGetValue("SKU", out string sku);
            data.DataFields.TryGetValue("FONT_STYLE", out string font_style);
            data.DataFields.TryGetValue("SKU_DIAMETER", out string sku_diam);

            double.TryParse(sku_diam, out double skudiam);


            var bndo = new BlueNileDataObject(
                jobNumber,
                engravetext,
                dengraveheight,
                engrave_template,
                metal_type,
                engravemaxchars,
                insidefont,
                sku,
                font_style,
                skudiam,
                fn,
                template
                );

            return bndo;

        }

    }
}
