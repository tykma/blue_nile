# README #

An HTTPD class library - originally to allow remote control of the tykma icon interface.

### What is this repository for? ###

* HTTPD DLL
* Version 1

### How do I get set up? ###

* Pull source.
* Add project as reference to solution.
* Insantiate a new server instance: *Server httpd = new Server(80)*
* Subscribe to socket connected event *SocketConnected*.
* Socket connected will send a *SocketEventMessage* with a Client instance.
* Run Client.Do and get requested string.


### Who do I talk to? ###

* Youri Karpeev youri@permanentmarking.com