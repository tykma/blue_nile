﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdParamPenGetColor.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// The command to get pen color.
    /// </summary>
    internal class CmdParamPenGetColor : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdParamPenGetColor"/> class.
        /// </summary>
        /// <param name="lmcApi">
        /// The API.
        /// </param>
        /// <param name="whichpen">
        /// Which pen.
        /// </param>
        public CmdParamPenGetColor(IntPtr lmcApi, int whichpen)
            : base(lmcApi)
        {
            this.WhichPen = whichpen;
        }

        /// <summary>
        /// Returns decimal value of a pen color.
        /// </summary>
        /// <param name="penNumber">Pen number.</param>
        /// <param name="color">The color.</param>
        /// <returns>Pointer to function.</returns>
        private delegate int LMC1ApiGetPenColor(int penNumber, out int color);

        /// <summary>
        /// Gets or sets the which pen.
        /// </summary>
        /// <value>
        /// The which pen.
        /// </value>
        internal int WhichPen { get; set; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="Tykma.Core.Engine.CustomExceptions.MethodFaultException">
        /// There was a problem.
        /// </exception>
        internal int Execute()
        {
            int penNo;
            var lmc1GetPenColor = this.LoadFunction<LMC1ApiGetPenColor>("lmc1_GetPenColor");

            if (lmc1GetPenColor == null)
            {
                throw new EngineNotConnectedException();
            }

            var rm = this.GetMiniLaseReturnCode(lmc1GetPenColor(this.WhichPen, out penNo));

            if (rm.Success)
            {
                return penNo;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
