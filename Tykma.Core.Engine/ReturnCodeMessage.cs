﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReturnCodeMessage.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine
{
    using System;

    /// <summary>
    /// Return code type.
    /// </summary>
    public enum ReturnCodeMsg
    {
        /// <summary>
        /// The return code is an error.
        /// </summary>
        Error,

        /// <summary>
        /// The return code is informative.
        /// </summary>
        Info,

        /// <summary>
        /// The return code did not get properly built.
        /// </summary>
        NoMessage
    }

    /// <summary>
    /// Status message to pass to events.
    /// </summary>
    public class ReturnCodeMessage : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReturnCodeMessage"/> class.
        /// </summary>
        public ReturnCodeMessage()
        {
            this.Status = ReturnCodeMsg.NoMessage;
        }

        /// <summary>
        /// Gets or sets the STATUS string.
        /// </summary>
        /// <value>
        /// The STATUS string.
        /// </value>
        public ReturnCodeMsg Status { get; set; }

        /// <summary>
        /// Gets or sets the return code..
        /// </summary>
        /// <value>
        /// The error string.
        /// </value>
        public int ReturnCode { get; set; }

        /// <summary>
        /// Gets or sets the return description.
        /// </summary>
        /// <value>
        /// The return description.
        /// </value>
        public string ReturnDescription { get; set; }

        /// <summary>
        /// Gets or sets the return where the return code originated..
        /// </summary>
        /// <value>
        /// The return where.
        /// </value>
        public string ReturnOrigin { get; set; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="ReturnCodeMessage"/> is success.
        /// </summary>
        /// <value>
        ///   <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        public bool Success => this.Status != ReturnCodeMsg.Error;

        /// <summary>
        /// Gets or sets a value indicating whether this is a [hard error].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [hard error]; otherwise, <c>false</c>.
        /// </value>
        public bool HardError { get; set; }
    }
}
