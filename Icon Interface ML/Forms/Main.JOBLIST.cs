﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.JOBLIST.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Job list generation/presentation portion of the UX.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;

    using Icon_Interface.General;
    using Icon_Interface.Properties;

    using Tykma.Core.Engine;
    using Tykma.Core.StringExtensions;
    using Tykma.Icon.JobList;

    /// <summary>
    /// Job list related code for the user interface.
    /// We will retrieve available projects from our engines
    /// populate job lists and handle job list changes.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// This will host a lists (cut into smaller lists) of the retrieved jobs.
        /// </summary>
        private readonly JobList jobList = new JobList(8);

        /// <summary>
        /// Gets the list of projects.
        /// </summary>
        private void GetListOfProjects()
        {
            // Create a new list to hold all the jobs.
            var allJobs = new List<string>();

            /* For the PSE engine the jobs will be stored on disc
             * for the Lighter engine the jobs will be retrieved through the
             * API interface */
            if (this.le.Engine == EngineTypes.EngineType.PSE)
            {
                // If the requested project folder exists - continue.
                if (this.fileOperations.ProjectFolder.Exists)
                {
                    // Add all the folders in our directory with the specified exception to our job list.
                    try
                    {
                        allJobs.AddRange(Directory.EnumerateFiles(this.fileOperations.ProjectFolder.FullName).Where(file => file.ToLower().EndsWith(this.le.FileExtension)).ToList());
                    }
                    catch (Exception err)
                    {
                        Logger.Error(err.Message);
                    }
                }
            }

            // Create a new list of lists from the available jobs.
            this.jobList.ListToParse = allJobs;

            // Show the jobs on the user interface.
            this.PopulateJobList();
        }

        /// <summary>
        /// Unselects all jobs.
        /// </summary>
        private void UnselectAllJobs()
        {
            // Go through every label and make sure the image is an unselected button.
            foreach (Label lb in this.ux_JobList.Controls)
            {
                lb.Image = new Bitmap(ImageResources.button_blue);
            }
        }

        /// <summary>
        /// Gets the job list label.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>A label.</returns>
        private Label GetJobListLabel(string text)
        {
            var buttonImage = new Bitmap(ImageResources.button_blue);

            var lb = new Label
            {
                Text = text.CaseInsensitiveReplace(this.le.FileExtension, string.Empty),
                Size = buttonImage.Size,
                TextAlign = ContentAlignment.MiddleCenter,
                Image = buttonImage,
                Margin = new Padding(0, 2, 0, 2),
                Cursor = Cursors.Hand,
                Font = new Font(this.Font.FontFamily, 12, FontStyle.Regular),
                Tag = "flash",
            };

            return lb;
        }

        /// <summary>
        /// Clears the job list.
        /// </summary>
        private void ClearJobList()
        {
            while (this.ux_JobList.Controls.Count > 0)
            {
                var controltoremove = this.ux_JobList.Controls[0];
                this.ux_JobList.Controls.Remove(controltoremove);
                controltoremove.Dispose();
            }
        }

        /// <summary>
        /// Populates the job list.
        /// </summary>
        private void PopulateJobList()
        {
            // Remove all the exists job labels from our job list.
            this.ClearJobList();

            // Get a sublist from our total job list.
            var listToIterate = this.jobList.GetList(this.selectedJobListIndex);

            // Create labels if the joblist functionality is enabled.
            if (!TykmaGlobals.INISettings.HIDEJOBLIST)
            {
                // Iterate through the list and handle the file.
                foreach (var projName in listToIterate)
                {
                    var fileName = Path.GetFileName(projName);
                    if (fileName != null)
                    {
                        // Create a label.
                        var lb = this.GetJobListLabel(fileName);

                        // Set it's image to active if it is the loaded job.
                        if (lb.Text == this.markingJobState.Name)
                        {
                            lb.Image = new Bitmap(ImageResources.button_glow);
                        }

                        // Create a click event handler.
                        lb.Click += this.ProjectsListSelectedIndexChanged;

                        // Add it to our listbox.
                        this.ux_JobList.Controls.Add(lb);
                    }

                    this.ux_JobList.VerticalScroll.Maximum += 30;
                }
            }

            // Node filler.
            this.ux_Job_Tree.Nodes.Clear();
            foreach (var projName in this.jobList.GetEntireList())
            {
                var name = Path.GetFileName(projName);
                if (name == null)
                {
                    continue;
                }

                string fileName = name;

                if (!string.IsNullOrEmpty(this.le.FileExtension))
                {
                    fileName = name.Replace(this.le.FileExtension, string.Empty);
                }

                if (string.IsNullOrEmpty(fileName))
                {
                    continue;
                }

                string firstChar = fileName[0].ToString(CultureInfo.InvariantCulture).ToUpper();

                TreeNode[] treeNodes = this.ux_Job_Tree.Nodes
                    .Cast<TreeNode>()
                    .Where(r => r.Text == firstChar)
                    .ToArray();

                if (treeNodes.Length > 0)
                {
                    treeNodes[0].Nodes.Add(fileName);
                }
                else
                {
                    TreeNode addNode = this.ux_Job_Tree.Nodes.Add(firstChar);
                    addNode.NodeFont = new Font(this.ux_Job_Tree.Font, FontStyle.Bold);
                    addNode.Nodes.Add(fileName);
                }
            }

            this.ux_Job_Tree.Sort();

            this.ShowOrHideJobNavButtons();
        }

        /// <summary>
        /// Configures the file system watcher.
        /// </summary>
        private void ConfigureFileSystemWatcher()
        {
            if (this.fileOperations.ProjectFolder.Exists)
            {
                try
                {
                    this.ux_fsw_JobList.Path = this.fileOperations.ProjectFolder.FullName;
                }
                catch (Exception e)
                {
                    Logger.Error(e.Message);
                }

                this.ux_fsw_JobList.EnableRaisingEvents = true;
            }
        }

        /// <summary>
        /// Handles the onChange event of the Job List control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="FileSystemEventArgs"/> instance containing the event data.</param>
        private void JobListOnChange(object sender, FileSystemEventArgs e)
        {
            this.GetListOfProjects();
        }
    }
}
