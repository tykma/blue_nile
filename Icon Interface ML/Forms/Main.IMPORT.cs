﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.IMPORT.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// Data import handling on the main UI.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Threading.Tasks;

    using Icon_Interface.File_Management.CmdFileImporter;
    using Icon_Interface.General;
    using Icon_Interface.Properties;

    using Tykma.Core.Engine;
    using Tykma.Core.Entity;

    /// <summary>
    /// Data import partial class.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// The command file class.
        /// </summary>
        private DataFileSearcher cmdFile;

        /// <summary>
        /// A list of file search animation images.
        /// </summary>
        private List<Image> listofFileSearchImages;

        /// <summary>
        /// The file search image animation iterator.
        /// </summary>
        private int? fileSearchImageIterator;

        /// <summary>
        /// Initializes the data importer class.
        /// </summary>
        private void InitializeDataImporter()
        {
            // We'll instantiate our command file class and subscribe to its data file found event.
            this.cmdFile = new DataFileSearcher(TykmaGlobals.INISettings);
            this.cmdFile.FileSearchEvent += this.DataFileEvent;

            // List of images that will be iterated through to create a file searching animation.
            this.listofFileSearchImages = new List<Image>
                                              {
                                                  ImageResources.file_search,
                                                  ImageResources.file_search2,
                                                  ImageResources.file_search3,
                                              };
        }

        /// <summary>
        /// Data file ready event triggered.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void DataFileEvent(object sender, FileSearchEventMessage e)
        {
            this.InvokeIfRequired(
                async () =>
                {
                    // If the limits are on we will wait them out.
                    while (this.le.LimitsOn)
                    {
                        await Task.Delay(50);
                    }

                    // We'll wait out a marking cycle as well.
                    while (this.le.MarkingBusy)
                    {
                        await Task.Delay(50);
                    }

                    // Our file event message will be in 'e'.
                    FileSearchEventMessage fileMessage = e;

                    if (fileMessage.Status == FileImportState.Complete)
                    {
                        // If we're in job load mode, we will first load the job.
                        if (TykmaGlobals.INISettings.DATAJOBLOADMODE)
                        {
                            if (!await this.LoadJobAbstractor(this.cmdFile.JobName, true))
                            {
                                this.ShowSearchStatus(FileImportState.Searching);
                                this.cmdFile.MoveNoMarkFile();
                                this.cmdFile.Search(true);
                                this.ClearSequence();
                                return;
                            }

                            // If we want to keep searching after we've loaded something - we will do so here.
                            if (TykmaGlobals.INISettings.DATAIMPORTSEARCHAFTERCOMPLETE)
                            {
                                this.ShowSearchStatus(FileImportState.Searching);
                                this.cmdFile.MoveLastFile();
                                this.cmdFile.Search(true);
                            }
                        }

                        // We will get a list of all data and set it here.
                        foreach (EntityInformation entity in this.cmdFile.DataList)
                        {
                            await this.ChangeTextByName(entity.Name, entity.Value);
                            Logger.Info("Discovered IDs {0}:{1}", entity.Name, entity.Value);
                        }

                        // If there is a quantity involved, we'll set it here.
                        if (TykmaGlobals.INISettings.USECOUNTER)
                        {
                            if (this.cmdFile.Quantity > this.ux_Control_Quantity.Minimum && this.cmdFile.Quantity < this.ux_Control_Quantity.Maximum)
                            {
                                this.ux_Control_Quantity.Value = this.cmdFile.Quantity;
                            }
                        }

                        this.JobState = JobStatus.Status.DataSet;
                        this.UpdatePreview();
                        this.InitDataGridView();
                        this.markingJobState.Authorized = true;
                        this.TimeStampEvent(Resources.Msg_Data_Import_Completed);
                        Logger.Debug(Resources.Msg_Data_Import_Completed);
                        this.SetSoftwareReady(true);
                        if (!TykmaGlobals.INISettings.DATAIMPORTSEARCHAFTERCOMPLETE)
                        {
                            this.ShowSearchStatus(FileImportState.Complete);
                        }

                        if (TykmaGlobals.INISettings.DATAIMPORTSTARTONLOAD)
                        {
                            this.StartSequence();
                        }
                    }
                    else if (fileMessage.Status == FileImportState.Parsing)
                    {
                        this.ShowSearchStatus(FileImportState.Parsing);
                        this.SetSoftwareReady(false);
                    }
                    else if (fileMessage.Status == FileImportState.Error)
                    {
                        this.ShowSearchStatus(FileImportState.Error);
                        this.SetSoftwareReady(false);
                    }
                    else if (fileMessage.Status == FileImportState.Searching)
                    {
                        this.ShowSearchStatus(FileImportState.Searching);
                        this.SetSoftwareReady(false);
                    }
                });
        }

        /// <summary>
        /// Show's the file search status in the toolbar.
        /// </summary>
        /// <param name="searchState">State of the search.</param>
        private void ShowSearchStatus(FileImportState searchState)
        {
            this.ux_Timer_AnimateFileSearch.Stop();

            this.ux_Status_Import_State.ForeColor = Color.Black;
            switch (searchState)
            {
                case FileImportState.Searching:
                    this.ux_Status_Import_State.Visible = true;
                    this.ux_Timer_AnimateFileSearch.Start();
                    this.ux_Status_Import_State.Text = Resources.Main_Show_Search_Status_File_Search_;
                    if (TykmaGlobals.INISettings.CLEARAFTERFINISH && !this.markingJobState.Loaded)
                    {
                        this.ux_PreviewPic.Image = this.imageBrands.FileSearching;
                    }

                    break;

                case FileImportState.Complete:
                    this.ux_Status_Import_State.Visible = true;
                    this.ux_Status_Import_State.ForeColor = Color.DarkGreen;
                    this.ux_Status_Import_State.Text = Resources.Main_Show_Search_Status_Complete_;
                    break;

                case FileImportState.Error:
                    this.ux_Status_Import_State.Visible = true;
                    this.ux_Status_Import_State.ForeColor = Color.Red;
                    this.ux_Status_Import_State.Text = Resources.Main_ShowHideClearButton_Exception_;
                    this.ux_PreviewPic.Image = this.imageBrands.FileFault;
                    this.ShowHideClearButton();
                    break;

                case FileImportState.Ready:
                    this.ux_Status_Import_State.Visible = true;
                    this.ux_Status_Import_State.Text = Resources.Main_Show_Search_Status_Ready_;
                    break;

                case FileImportState.Parsing:
                    this.ux_Status_Import_State.Visible = true;
                    this.ux_Status_Import_State.Text = Resources.Main_Show_Search_Status_Parsing_;
                    break;

                case FileImportState.Disabled:
                    this.ux_Status_Import_State.Visible = false;
                    break;
            }
        }

        /// <summary>
        /// Handles the Tick event of the TimerAnimateFileSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void TimerAnimateFileSearchTick(object sender, EventArgs e)
        {
            if (!this.fileSearchImageIterator.HasValue || this.fileSearchImageIterator.Value > 3)
            {
                this.fileSearchImageIterator = 1;
            }

            this.ux_Status_Import_State.Image = this.listofFileSearchImages[this.fileSearchImageIterator.Value - 1];
            this.fileSearchImageIterator += 1;
        }

        /// <summary>
        /// Back button is clicked on the import path settings screen.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ButtonImportBackClick(object sender, EventArgs e)
        {
            this.ux_HMI.SelectedTab = this.ux_Tab_Settings;
        }

        /// <summary>
        /// Save data import settings.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void ButtonImportSaveClick(object sender, EventArgs e)
        {
            TykmaGlobals.INISettings.DATASEARCHFOLDER = this.ux_Control_ImportPath.SelectedPath;
            TykmaGlobals.INISettings.DATADONEFOLDER = this.ux_Control_CompletePath.SelectedPath;
            TykmaGlobals.INISettings.DATAERRORFOLDER = this.ux_Control_ErrorPath.SelectedPath;
            TykmaGlobals.INISettings.DATASEARCHSPEED = (int)this.ux_Import_PollRate.Value;
            TykmaGlobals.INISettings.DATAWAITFORREAD = (int)this.ux_Import_ReadWait.Value;
            TykmaGlobals.INISettings.DATAJOBLOADMODE = this.ux_Import_ToggleJobLoadMode.Checked;
            TykmaGlobals.INISettings.DATASEARCHENABLED = this.ux_Import_ToggleFileImport.Checked;
            this.TimeStampEvent("Saved import configuration settings.");
            Logger.Info("File import configuration setting saved");
        }
    }
}
