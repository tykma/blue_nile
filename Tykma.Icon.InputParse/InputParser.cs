﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputParser.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Tykma.Icon.InputParse
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Parse retrieved inputs.
    /// </summary>
    public class InputParser
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InputParser" /> class.
        /// </summary>
        /// <param name="startInput">The start input.</param>
        /// <param name="stopInput">The stop input.</param>
        /// <param name="shutterOpen">The shutter open.</param>
        /// <param name="invertinputs">if set to <c>true</c> [invert inputs].</param>
        public InputParser(int startInput, int stopInput, int shutterOpen, bool invertinputs)
        {
            this.StartInput = (InputMask.IOMasks)Math.Pow(2, this.GetValidInputNumber(startInput));
            this.StopInput = (InputMask.IOMasks)Math.Pow(2, this.GetValidInputNumber(stopInput));
            this.ShutterInput = (InputMask.IOMasks)Math.Pow(2, this.GetValidInputNumber(shutterOpen));

            this.InvertInputs = invertinputs;
        }

        /// <summary>
        /// Gets the start input.
        /// </summary>
        /// <value>
        /// The start input.
        /// </value>
        public InputMask.IOMasks StartInput { get; }

        /// <summary>
        /// Gets the stop input.
        /// </summary>
        /// <value>
        /// The stop input.
        /// </value>
        public InputMask.IOMasks StopInput { get; }

        /// <summary>
        /// Gets the shutter input.
        /// </summary>
        /// <value>
        /// The shutter input.
        /// </value>
        public InputMask.IOMasks ShutterInput { get; }

        /// <summary>
        /// Gets a value indicating whether to invert inputs.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [invert inputs]; otherwise, <c>false</c>.
        /// </value>
        private bool InvertInputs { get; }

        /// <summary>
        /// Gets the type of the input.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns>
        /// List of commands given.
        /// </returns>
        public List<InputCommand.InputType> GetInputType(int word)
        {
            var masks = (InputMask.IOMasks)word;

            var returnCommands = new List<InputCommand.InputType>();

            if (this.InvertInputs)
            {
                masks = ~masks;
            }

            if (masks.HasFlag(this.StartInput))
            {
                returnCommands.Add(InputCommand.InputType.Start);
            }

            if (masks.HasFlag(this.StopInput))
            {
                returnCommands.Add(InputCommand.InputType.Stop);
            }

            if (masks.HasFlag(this.ShutterInput))
            {
                returnCommands.Add(InputCommand.InputType.ShutterState);
            }

            return returnCommands;
        }

        /// <summary>
        /// Determines whether [is input on] [the specified input].
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="dword">The word.</param>
        /// <returns>Whether or not an input is on.</returns>
        public bool IsInputOn(int input, int dword)
        {
            var masks = (InputMask.IOMasks)dword;

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (InputMask.IOMasks ioinput in Enum.GetValues(typeof(InputMask.IOMasks)))
            {
                var index = Array.IndexOf(Enum.GetValues(typeof(InputMask.IOMasks)), ioinput);

                if (Math.Abs(index) == input)
                {
                    return masks.HasFlag(ioinput);
                }
            }

            return false;
        }

        /// <summary>
        /// Gets a valid input number.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns>
        /// Parsed value.
        /// </returns>
        private int GetValidInputNumber(int number)
        {
            var enumLength = Enum.GetValues(typeof(InputMask.IOMasks)).Length;

            if (number > enumLength || number < 0)
            {
                return 0;
            }

            return number;
        }
    }
}
