﻿// <copyright file="GlobalHelpers.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Test.UnitTesting.Globals
{
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;

    using Tykma.Icon.Globals;

    /// <summary>
    /// Global unit test helpers.
    /// </summary>
    public static class GlobalHelpers
    {
        /// <summary>
        /// Gets the global variables.
        /// </summary>
        /// <returns>Global variables instance.</returns>
        [ExcludeFromCodeCoverage]
        public static GlobalVariables GetGlobalVariables()
        {
            var file = new FileInfo("C:/temp/blah.xml");
            if (file.Exists)
            {
                file.Delete();
            }

            var gv = new GlobalVariables(file);

            try
            {
                gv.LoadGlobalVariableData();
            }
            catch (FileNotFoundException)
            {
                Debug.Print("New global variable file created");
            }

            return gv;
        }
    }
}
