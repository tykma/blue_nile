﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.NUDGER.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//  Nudger form handling class on main UI.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;

    using Icon_Interface.General;
    using Icon_Interface.Nudger;

    using Tykma.Core.Entity;

    /// <summary>
    /// Main partial for nudging form handling.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// The queue form.
        /// </summary>
        private Nudger nudgerForm;

        /// <summary>
        /// Nudge has sent us a stopped event - react.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private delegate void NudgeEventStopCallBack(object sender, bool e);

        /// <summary>
        /// Gets or sets a value indicating whether [limits were stopped].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [limits were stopped]; otherwise, <c>false</c>.
        /// </value>
        private bool LimitsWereStopped { get; set; }

        /// <summary>
        /// Initializes the nudge form.
        /// </summary>
        private void InitializeNudgeForm()
        {
            if (TykmaGlobals.INISettings.USENUDGEFORM)
            {
                this.nudgerForm = new Nudger();
                this.nudgerForm.Show();

                if (this.nudgerForm != null)
                {
                    this.nudgerForm.NudgeEvent += this.NudgeMessageEvent;
                    this.nudgerForm.NudgeStopEvent += this.NudgeMessageStopEvent;
                }
            }
        }

        /// <summary>
        /// Handles a message from the queue form.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void NudgeMessageEvent(object sender, Nudgement e)
        {
            this.InvokeIfRequired(() =>
                    {
                        this.NudgeAllEntities(e.NudgeDirection, e.NudgeDistance);
                        this.UpdatePreview();
                    });
        }

        /// <summary>
        /// Nudge form has sent a stop event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void NudgeMessageStopEvent(object sender, bool e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new NudgeEventStopCallBack(this.NudgeMessageStopEvent), sender, e);
            }
            else
            {
                if (this.LimitsWereStopped)
                {
                    this.TimeStampEvent("Resuming limits");
                    this.LimitsWereStopped = false;
                    this.le.StartLimits(true, this.le.LimitsContoured);
                }
            }
        }

        /// <summary>
        /// Nudges all entities.
        /// </summary>
        /// <param name="dir">The direction.</param>
        /// <param name="nudgeDistance">The nudge distance.</param>
        private void NudgeAllEntities(Nudgement.NudgeType dir, double nudgeDistance)
        {
            if (!this.markingJobState.Loaded)
            {
                return;
            }

            if (this.le.LimitsOn)
            {
                this.TimeStampEvent("Pausing limits");
                this.LimitsWereStopped = true;
                this.le.StartLimits(false, false);
            }

            if (dir != Nudgement.NudgeType.ClockWise && dir != Nudgement.NudgeType.CounterClockwise)
            {
                foreach (EntityInformation ent in this.GetListOfIDs())
                {
                    if (dir == Nudgement.NudgeType.Right)
                    {
                        this.le.MoveObject(ent.Name, nudgeDistance, 0);
                    }
                    else if (dir == Nudgement.NudgeType.Left)
                    {
                        this.le.MoveObject(ent.Name, -nudgeDistance, 0);
                    }
                    else if (dir == Nudgement.NudgeType.Up)
                    {
                        this.le.MoveObject(ent.Name, 0, nudgeDistance);
                    }
                    else if (dir == Nudgement.NudgeType.Down)
                    {
                        this.le.MoveObject(ent.Name, 0, -nudgeDistance);
                    }
                }
            }
            else
            {
                foreach (var ent in this.GetListOfIDs())
                {
                    if (dir == Nudgement.NudgeType.ClockWise)
                    {
                        this.RotateObject(ent.Name, 0, 0, -nudgeDistance);
                    }
                    else
                    {
                        this.RotateObject(ent.Name, 0, 0, nudgeDistance);
                    }
                }
            }
        }
    }
}
