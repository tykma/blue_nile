﻿namespace Icon_Interface.Custom_Controls.IOLeds
{
    using System.Windows.Forms;

    partial class ControlIOLEDBase
    {
        /// <summary>
        /// The LED Picture.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private PictureBox ux_Picture_IOLED;

        /// <summary>
        /// The panel
        /// </summary>
        private Panel panel1;

        /// <summary>
        /// The split container.
        /// </summary>
        private SplitContainer splitContainer1;

        /// <summary>
        /// The LED number.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private Label ux_Label_IONum;


        /// <summary>
        /// Initializes the component.
        /// </summary>
        private void InitializeComponent()
        {
            this.ux_Picture_IOLED = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ux_Label_IONum = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ux_Picture_IOLED)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ux_Picture_IOLED
            // 
            this.ux_Picture_IOLED.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ux_Picture_IOLED.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Picture_IOLED.Image = global::Icon_Interface.Properties.ImageResources.epb_green_led1;
            this.ux_Picture_IOLED.Location = new System.Drawing.Point(0, 0);
            this.ux_Picture_IOLED.Name = "ux_Picture_IOLED";
            this.ux_Picture_IOLED.Size = new System.Drawing.Size(56, 49);
            this.ux_Picture_IOLED.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ux_Picture_IOLED.TabIndex = 2;
            this.ux_Picture_IOLED.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(56, 78);
            this.panel1.TabIndex = 3;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ux_Picture_IOLED);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.ux_Label_IONum);
            this.splitContainer1.Size = new System.Drawing.Size(56, 78);
            this.splitContainer1.SplitterDistance = 49;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 0;
            // 
            // ux_Label_IONum
            // 
            this.ux_Label_IONum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_Label_IONum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_Label_IONum.Location = new System.Drawing.Point(0, 0);
            this.ux_Label_IONum.Name = "ux_Label_IONum";
            this.ux_Label_IONum.Size = new System.Drawing.Size(56, 28);
            this.ux_Label_IONum.TabIndex = 0;
            this.ux_Label_IONum.Text = "1";
            this.ux_Label_IONum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ControlIOLEDBase
            // 
            this.Controls.Add(this.panel1);
            this.Name = "ControlIOLEDBase";
            this.Size = new System.Drawing.Size(56, 78);
            ((System.ComponentModel.ISupportInitialize)(this.ux_Picture_IOLED)).EndInit();
            this.panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }
    }
}
