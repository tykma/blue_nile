﻿// <copyright file="DatabaseUtil.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Integration.SQL
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;

    /// <summary>
    /// Low level database access utility.
    /// </summary>
    public class DatabaseUtil : IDatabaseUtil
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseUtil"/> class.
        /// </summary>
        /// <param name="connString">The connection string.</param>
        /// <param name="providerName">Name of the DB provider.</param>
        public DatabaseUtil(string connString, string providerName)
            : this(connString, providerName, new MyDbProviderFactories())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseUtil"/> class.
        /// </summary>
        /// <param name="connString">The connection string.</param>
        /// <param name="providerName">Name of the DB provider.</param>
        /// <param name="factoryProvider">The factory provider.</param>
        public DatabaseUtil(string connString, string providerName, IDbProviderFactories factoryProvider)
        {
            this.ConnString = connString;
            this.ProviderName = providerName;
            this.FactoryProvider = factoryProvider;
        }

        /// <summary>
        /// Gets the Connection string.
        /// </summary>
        public string ConnString { get; }

        /// <summary>
        /// Gets the db provider name.
        /// </summary>
        public string ProviderName { get; }

        /// <summary>
        /// Gets the DB factory provider.
        /// </summary>
        private IDbProviderFactories FactoryProvider { get; }

        /// <summary>
        /// Executes the gives SQL query.
        /// </summary>
        /// <param name="query">The SQL query to be executed.</param>
        /// <param name="parameters">Query parameters and their values.</param>
        /// <returns>Number of rows affected.</returns>
        public int ExecuteCommand(string query, Dictionary<string, object> parameters = null)
        {
            return this.ExecuteCommand(this.ConnString, this.ProviderName, query, parameters);
        }

        /// <summary>
        /// Exeute a stored procedure.
        /// </summary>
        /// <param name="procedurename">Stored procedure name.</param>
        /// <param name="parameters">Parameters to pass to stored procedure.</param>
        /// <returns>Return datatable.</returns>
        public DataTable ExecuteStoredProcedure(string procedurename, Dictionary<string, object> parameters = null)
        {
            return this.ExecuteStoredProcedure(this.ConnString, this.ProviderName, procedurename, parameters);
        }

        /// <inheritdoc />
        public DataTable Select(string query, Dictionary<string, object> parameters = null)
        {
            return this.Select(this.ConnString, this.ProviderName, query, parameters);
        }

        /// <summary>
        /// Executes the given SQL query.
        /// </summary>
        /// <param name="connString">Connection string to the database.</param>
        /// <param name="providerName">Db provider invariant name.</param>
        /// <param name="query">The SQL query to be executed.</param>
        /// <param name="parameters">Query parameters and their values.</param>
        /// <returns>Number of rows affected.</returns>
        private int ExecuteCommand(string connString, string providerName, string query, Dictionary<string, object> parameters = null)
        {
            DbProviderFactory factory = this.FactoryProvider.GetFactory(providerName);

            // Create Query
            using (IDbConnection conn = factory.CreateConnection())
            {
                conn.ConnectionString = connString;
                using (IDbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;

                    // Add Parameters
                    if (parameters != null)
                    {
                        foreach (KeyValuePair<string, object> kvp in parameters)
                        {
                            DbParameter parameter = factory.CreateParameter();
                            parameter.ParameterName = kvp.Key;
                            parameter.Value = kvp.Value;
                            cmd.Parameters.Add(parameter);
                        }
                    }

                    // Execute Query
                    conn.Open();
                    return cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Query an SQL database.
        /// </summary>
        /// <param name="connString">Connection string to the database.</param>
        /// <param name="providerName">DB provider invariant name.</param>
        /// <param name="query">Select query that returns a data table.</param>
        /// <param name="parameters">Query parameters with their values.</param>
        /// <returns>Query results as a DataTable.</returns>
        private DataTable Select(string connString, string providerName, string query, Dictionary<string, object> parameters = null)
        {
            if (this.FactoryProvider == null)
            {
                throw new Exception("Factory provider is null");
            }

            DbProviderFactory factory = this.FactoryProvider.GetFactory(providerName);

            if (factory == null)
            {
                throw new Exception("Factory is null");
            }

            DataTable dt = new DataTable();

            // Create Query
            using (DbConnection conn = factory.CreateConnection())
            {
                if (conn == null)
                {
                    throw new Exception("Connection is null");
                }

                conn.ConnectionString = connString;
                using (DbCommand cmd = conn.CreateCommand())
                using (DbDataAdapter da = factory.CreateDataAdapter())
                {
                    if (da == null)
                    {
                        throw new Exception("Null data adapter.");
                    }

                    cmd.CommandText = query;
                    da.SelectCommand = cmd;

                    // Add Parameters
                    if (parameters != null)
                    {
                        foreach (KeyValuePair<string, object> kvp in parameters)
                        {
                            DbParameter parameter = cmd.CreateParameter();
                            parameter.ParameterName = kvp.Key;
                            parameter.Value = kvp.Value;
                            cmd.Parameters.Add(parameter);
                        }
                    }

                    // Execute Query
                    conn.Open();
                    da.Fill(dt);
                    return dt;
                }
            }
        }

        /// <summary>
        /// Execute stored procedure.
        /// </summary>
        /// <param name="connString">The connection string.</param>
        /// <param name="providerName">The provider name.</param>
        /// <param name="procedurename">Name of the stored procedure.</param>
        /// <param name="parameters">Collection of parameters.</param>
        /// <returns>Return datatable.</returns>
        private DataTable ExecuteStoredProcedure(
            string connString,
            string providerName,
            string procedurename,
            Dictionary<string, object> parameters = null)
        {
            DbProviderFactory factory = DbProviderFactories.GetFactory(providerName);
            DataTable dt = new DataTable();

            // Create Query
            using (DbConnection conn = factory.CreateConnection())
            {
                conn.ConnectionString = connString;
                using (var cmd = new SqlCommand(procedurename))
                using (DbDataAdapter da = factory.CreateDataAdapter())
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Add Parameters
                    if (parameters != null)
                    {
                        foreach (KeyValuePair<string, object> kvp in parameters)
                        {
                            DbParameter parameter = cmd.CreateParameter();
                            parameter.ParameterName = kvp.Key;
                            parameter.Value = kvp.Value;
                            cmd.Parameters.Add(parameter);
                        }
                    }

                    da.SelectCommand = cmd;
                    da.SelectCommand.Connection = conn;

                    conn.Open();
                    da.Fill(dt);
                }
            }

            return dt;
        }
    }
}