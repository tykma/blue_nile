﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JobListTest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Test.UnitTesting.JobList
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Icon.JobList;

    /// <summary>
    /// Job list unit testing class.
    /// </summary>
    [TestClass]
    public class JobListTest
    {
        /// <summary>
        /// Test job list count.
        /// </summary>
        [TestMethod]
        public void TestJobListListCount()
        {
            var joblist = new JobList(3);

            var listofjobs = new List<string>();

            for (int i = 0; i < 10; i++)
            {
                listofjobs.Add($"job{i}");
            }

            joblist.ListToParse = listofjobs;

            Assert.AreEqual(4, joblist.ListCount);
        }

        /// <summary>
        /// Tests the job list total count.
        /// </summary>
        [TestMethod]
        public void TestJobListTotalCount()
        {
            var joblist = new JobList(3);

            var listofjobs = new List<string>();

            for (int i = 0; i < 10; i++)
            {
                listofjobs.Add($"job{i}");
            }

            joblist.ListToParse = listofjobs;

            Assert.AreEqual(10, joblist.GetEntireList().Count);
        }

        /// <summary>
        /// Tests the list splitting accuracy.
        /// </summary>
        [TestMethod]
        public void TestListSplittingAccuracy()
        {
            var joblist = new JobList(3);

            var listofjobs = new List<string>();

            for (int i = 0; i < 10; i++)
            {
                listofjobs.Add($"job{i}");
            }

            joblist.ListToParse = listofjobs;

            Assert.AreEqual(3, joblist.GetList(0).Count());
            Assert.AreEqual(3, joblist.GetList(1).Count());
            Assert.AreEqual(3, joblist.GetList(2).Count());
            Assert.AreEqual(1, joblist.GetList(3).Count());
        }

        /// <summary>
        /// Tests an empty list.
        /// </summary>
        [TestMethod]
        public void TestEmptyList()
        {
            var joblist = new JobList(3);

            var listofjobs = new List<string>();

            joblist.ListToParse = listofjobs;

            Assert.AreEqual(1, joblist.ListCount);
            Assert.AreEqual(0, joblist.GetEntireList().Count);
        }

        /// <summary>
        /// Tests the duplicate jobs.
        /// </summary>
        [TestMethod]
        public void TestDuplicateJobs()
        {
            var joblist = new JobList(3);

            var listofjobs = new List<string>();

            for (int i = 0; i < 10; i++)
            {
                listofjobs.Add($"job{1}");
            }

            joblist.ListToParse = listofjobs;

            Assert.AreEqual(10, joblist.GetEntireList().Count);
        }

        /// <summary>
        /// Tests the getting list without extensions.
        /// </summary>
        [TestMethod]
        public void TestGettingListWithExtensions()
        {
            var joblist = new JobList(3);

            var listofjobs = new List<string>();

            for (int i = 0; i < 10; i++)
            {
                listofjobs.Add($"job{i}.ext");
            }

            joblist.ListToParse = listofjobs;

            Assert.AreEqual("job0.ext", joblist.GetList(0).ToList()[0]);
            Assert.AreEqual("job1.ext", joblist.GetList(0).ToList()[1]);
            Assert.AreEqual("job2.ext", joblist.GetList(0).ToList()[2]);
            Assert.AreEqual("job3.ext", joblist.GetList(1).ToList()[0]);
            Assert.AreEqual("job4.ext", joblist.GetList(1).ToList()[1]);
            Assert.AreEqual("job5.ext", joblist.GetList(1).ToList()[2]);
            Assert.AreEqual("job6.ext", joblist.GetList(2).ToList()[0]);
            Assert.AreEqual("job7.ext", joblist.GetList(2).ToList()[1]);
            Assert.AreEqual("job8.ext", joblist.GetList(2).ToList()[2]);
            Assert.AreEqual("job9.ext", joblist.GetList(3).ToList()[0]);
        }

        /// <summary>
        /// Tests the getting full list cutting extensions.
        /// </summary>
        [TestMethod]
        public void TestGettingFullListCuttingExtensions()
        {
            var joblist = new JobList(3);

            var listofjobs = new List<string>();

            for (int i = 0; i < 10; i++)
            {
                listofjobs.Add($"job{i}.ext");
            }

            joblist.ListToParse = listofjobs;

            var entirelist = joblist.GetEntireList(true);

            Assert.AreEqual("job0", entirelist[0]);
            Assert.AreEqual("job1", entirelist[1]);
            Assert.AreEqual("job2", entirelist[2]);
            Assert.AreEqual("job3", entirelist[3]);
            Assert.AreEqual("job4", entirelist[4]);
            Assert.AreEqual("job5", entirelist[5]);
            Assert.AreEqual("job6", entirelist[6]);
            Assert.AreEqual("job7", entirelist[7]);
            Assert.AreEqual("job8", entirelist[8]);
            Assert.AreEqual("job9", entirelist[9]);
        }

        /// <summary>
        /// Tests the getting full list without cutting extensions.
        /// </summary>
        [TestMethod]
        public void TestGettingFullListWithoutCuttingExtensions()
        {
            var joblist = new JobList(3);

            var listofjobs = new List<string>();

            for (int i = 0; i < 10; i++)
            {
                listofjobs.Add($"job{i}.ext");
            }

            joblist.ListToParse = listofjobs;

            var entirelist = joblist.GetEntireList();

            Assert.AreEqual("job0.ext", entirelist[0]);
            Assert.AreEqual("job1.ext", entirelist[1]);
            Assert.AreEqual("job2.ext", entirelist[2]);
            Assert.AreEqual("job3.ext", entirelist[3]);
            Assert.AreEqual("job4.ext", entirelist[4]);
            Assert.AreEqual("job5.ext", entirelist[5]);
            Assert.AreEqual("job6.ext", entirelist[6]);
            Assert.AreEqual("job7.ext", entirelist[7]);
            Assert.AreEqual("job8.ext", entirelist[8]);
            Assert.AreEqual("job9.ext", entirelist[9]);
        }

        /// <summary>
        /// Tests the stripping extension on non extension item.
        /// </summary>
        [TestMethod]
        public void TestStrippingExtensionOnNonExtensionItem()
        {
            var joblist = new JobList(3);

            var listofjobs = new List<string> { "noextension" };

            joblist.ListToParse = listofjobs;

            Assert.AreEqual("noextension", joblist.GetEntireList(true)[0]);
        }

        /// <summary>
        /// Tests the stripping extension on double extension item.
        /// </summary>
        [TestMethod]
        public void TestStrippingExtensionOnDoubleExtensionItem()
        {
            var joblist = new JobList(3);

            var listofjobs = new List<string> { "double.ext.ension" };

            joblist.ListToParse = listofjobs;

            Assert.AreEqual("double.ext", joblist.GetEntireList(true)[0]);
        }

        /// <summary>
        /// Tests the getting a negative list.
        /// </summary>
        [TestMethod]
        public void TestGettingANegativeList()
        {
            var joblist = new JobList(3);
            var list = joblist.GetList(-1);
            Assert.AreEqual(0, list.Count());
        }

        /// <summary>
        /// Tests getting an out of bounds list.
        /// </summary>
        [TestMethod]
        public void TestGettingOutOfBoundsList()
        {
            var joblist = new JobList(3);
            var list = joblist.GetList(55);
            Assert.AreEqual(0, list.Count());
        }

        /// <summary>
        /// Test getting a list.
        /// </summary>
        [TestMethod]
        public void TestGettingAList()
        {
            var joblist = new JobList(3);
            var listofjobs = new List<string>();
            for (int i = 0; i < 10; i++)
            {
                listofjobs.Add($"job{i}.ext");
            }

            joblist.ListToParse = listofjobs;

            var returnedList = joblist.GetList(0);
            Assert.AreEqual(3, returnedList.Count());
        }

        /// <summary>
        /// Tests the null lists.
        /// </summary>
        [TestMethod]
        public void TestNullLists()
        {
            var joblist = new JobList(0) { ListToParse = null };
            var returnedList = joblist.GetList(0);
            Assert.AreEqual(0, returnedList.Count());
        }

        /// <summary>
        /// Tests the negative master list.
        /// </summary>
        [TestMethod]
        public void TestNegativeMasterListGrouping()
        {
            var joblist = new JobList(-5);

            var listofjobs = new List<string>();

            for (int i = 0; i < 10; i++)
            {
                listofjobs.Add($"job{i}.ext");
            }

            joblist.ListToParse = listofjobs;

            var returnedList = joblist.GetList(0);
            Assert.AreEqual(10, returnedList.Count());
        }
    }
}
