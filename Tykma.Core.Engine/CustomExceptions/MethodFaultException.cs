﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MethodFaultException.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.CustomExceptions
{
    using System;

    /// <summary>
    /// An exception that occurs when an engine is not
    /// connected and we attempt to interact with it.
    /// </summary>
    public class MethodFaultException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MethodFaultException"/> class.
        /// </summary>
        public MethodFaultException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MethodFaultException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public MethodFaultException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MethodFaultException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="hardException">if set to <c>true</c> [hard exception].</param>
        public MethodFaultException(string message, bool hardException)
            : base(message)
        {
            this.ShowStopping = hardException;
        }

        /// <summary>
        /// Gets a value indicating whether or not this fault is a hard fault.
        /// </summary>
        /// <value>
        /// This exception is a show stopping one.
        /// </value>
        public bool ShowStopping { get; private set; }
    }
}
