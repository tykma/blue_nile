﻿// <copyright file="DBUtilTests.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Integration.SQL.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Diagnostics.CodeAnalysis;

    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NSubstitute;

    /// <summary>
    /// Test for the database utility class.
    /// </summary>
    [TestClass]
    public class DBUtilTests
    {
        /// <summary>
        /// Test that the connection string is set correctly.
        /// </summary>
        [TestMethod]
        public void TestConnectionStringProperty()
        {
            string conn = "Server=64.137.213.235;Database=ICON;User Id=youri;Password=youri85;";
            var dbutil = new DatabaseUtil(conn, "System.Data.SQL");
            Assert.AreEqual(conn, dbutil.ConnString);
        }

        /// <summary>
        /// Test that the provider property is set correctly.
        /// </summary>
        [TestMethod]
        public void TestProviderProperty()
        {
            string conn = "Server=64.137.213.235;Database=ICON;User Id=youri;Password=youri85;";
            var dbutil = new DatabaseUtil(conn, "System.Data.SQL");
            Assert.AreEqual("System.Data.SQL", dbutil.ProviderName);
        }

        /// <summary>
        /// Test executing a query command.
        /// </summary>
        [TestMethod]
        public void TestExecuteQueryCommand()
        {
            // We'll create a command which returns values.
            var command = Substitute.For<DbCommand>();
            IDataParameterCollection collection = Substitute.For<DbParameterCollection>();
            command.Parameters.Returns(collection);
            command.Parameters.Add(Arg.Any<DbParameter>()).Returns(0);

            // We'll return a command from our connection..
            var connection = Substitute.For<DbConnection>();
            connection.CreateCommand().Returns(command);

            // This is the factory we will return when asked for a factory.
            var factory = Substitute.For<DbProviderFactory>();
            factory.CreateConnection().Returns(connection);

            // Let's create the provider factory mock.
            var idb = Substitute.For<IDbProviderFactories>();
            idb.GetFactory("System.Data.UnitTest").Returns(factory);

            // Create the DB utility.
            var db = new DatabaseUtil(string.Empty, "System.Data.UnitTest", idb);

            // Execute a query command.
            var r = db.ExecuteCommand("Select TOP 1 from DB");

            // Confirm we tried to retrieve a factory.
            idb.Received().GetFactory("System.Data.UnitTest");

            // Command we set the proper command query.
            command.Received().CommandText = "Select TOP 1 from DB";

            // Confirm we opened the connection.
            connection.Received().Open();

            // Confirm we executed the query.
            command.Received().ExecuteNonQuery();

            // Confirm our databae connection ended up closed.
            connection.Received().Dispose();
        }

        /// <summary>
        /// Test executing query command with parameters.
        /// </summary>
        [TestMethod]
        public void TestExecuteQueryCommandParameters()
        {
            var idb = Substitute.For<IDbProviderFactories>();
            var factory = Substitute.For<DbProviderFactory>();
            var connection = Substitute.For<DbConnection>();

            // A command parameter object.
            var parameterObject = Substitute.For<DbParameter>();

            var command = Substitute.For<DbCommand>();

            // We will return a factory when called our provider.
            idb.GetFactory("System.Data.UnitTest").Returns(factory);

            // We will return connections and parameters from our factory.
            factory.CreateConnection().Returns(connection);
            factory.CreateParameter().Returns(parameterObject);

            // We'll return a command from our connection..
            connection.CreateCommand().Returns(command);

            // We will return parameters from our command.
            IDataParameterCollection collection = Substitute.For<DbParameterCollection>();
            command.Parameters.Returns(collection);
            command.Parameters.Add(Arg.Any<DbParameter>()).Returns(0);

            // Create the DB utility.
            var db = new DatabaseUtil(string.Empty, "System.Data.UnitTest", idb);

            // capture any command parameters set.
            DbParameter result = null;
            command.Parameters.Add(Arg.Do<DbParameter>(arg => result = arg));

            var dict = new Dictionary<string, object> { { "Serial", "123" } };

            // Execute a query command.
            var r = db.ExecuteCommand("Select TOP 1 from DB", dict);

            // Should be command parameters set.
            Assert.IsNotNull(result);
            Assert.AreEqual("Serial", result.ParameterName);
            Assert.AreEqual("123", result.Value);

            // Confirm we tried to retrieve a factory.
            idb.Received().GetFactory("System.Data.UnitTest");

            // Command we set the proper command query.
            command.Received().CommandText = "Select TOP 1 from DB";

            // Confirm we opened the connection.
            connection.Received().Open();

            // Confirm we executed the query.
            command.Received().ExecuteNonQuery();

            // Confirm our databae connection ended up closed.
            connection.Received().Dispose();
        }

        /// <summary>
        /// Test the select command.
        /// </summary>
        [TestMethod]
        public void TestSelectCommand()
        {
            var idb = Substitute.For<IDbProviderFactories>();
            var factory = Substitute.For<DbProviderFactory>();
            var connection = Substitute.For<DbConnection>();
            var dataadapter = Substitute.For<DbDataAdapter>();

            // A command parameter object.
            var parameterObject = Substitute.For<DbParameter>();

            var command = Substitute.For<DbCommand>();

            // We will return a factory when called our provider.
            idb.GetFactory("System.Data.UnitTest").Returns(factory);

            // We will return connections and parameters from our factory.
            factory.CreateConnection().Returns(connection);
            factory.CreateParameter().Returns(parameterObject);
            factory.CreateDataAdapter().Returns(dataadapter);

            // We'll return a command from our connection..
            connection.CreateCommand().Returns(command);

            // We will return parameters from our command.
            IDataParameterCollection collection = Substitute.For<DbParameterCollection>();
            command.Parameters.Returns(collection);
            command.Parameters.Add(Arg.Any<DbParameter>()).Returns(0);

            // Create the DB utility.
            var db = new DatabaseUtil(string.Empty, "System.Data.UnitTest", idb);

            var r = db.Select("Select TOP 1 from Table1");

            // Confirm we tried to retrieve a factory.
            idb.Received().GetFactory("System.Data.UnitTest");

            // Command we set the proper command query.
            command.Received().CommandText = "Select TOP 1 from Table1";

            // Confirm we opened the connection.
            connection.Received().Open();

            // Confirm we executed the query.
            dataadapter.ReceivedWithAnyArgs().Fill(null);

            // Confirm our databae connection ended up closed.
            connection.Received().Dispose();
        }

        /// <summary>
        /// Test the select command.
        /// </summary>
        [TestMethod]
        public void TestSelectCommandWithParameters()
        {
            var idb = Substitute.For<IDbProviderFactories>();
            var factory = Substitute.For<DbProviderFactory>();
            var connection = Substitute.For<DbConnection>();
            var dataadapter = Substitute.For<DbDataAdapter>();

            // A command parameter object.
            var parameterObject = Substitute.For<DbParameter>();

            var command = Substitute.For<DbCommand>();

            // We will return a factory when called our provider.
            idb.GetFactory("System.Data.UnitTest").Returns(factory);

            // We will return connections and parameters from our factory.
            factory.CreateConnection().Returns(connection);
            factory.CreateDataAdapter().Returns(dataadapter);

            // We'll return a command from our connection..
            connection.CreateCommand().Returns(command);

            // We will return parameters from our command.
            IDataParameterCollection collection = Substitute.For<DbParameterCollection>();
            command.Parameters.Returns(collection);
            command.Parameters.Add(Arg.Any<DbParameter>()).Returns(0);
            command.CreateParameter().Returns(parameterObject);

            // We'll return a command from our connection..
            connection.CreateCommand().Returns(command);

            // Create the DB utility.
            var db = new DatabaseUtil(string.Empty, "System.Data.UnitTest", idb);

            // capture any command parameters set.
            DbParameter result = null;
            command.Parameters.Add(Arg.Do<DbParameter>(arg => result = arg));

            var r = db.Select("Select TOP 1 from Table1", new Dictionary<string, object> { { "Serial", "123" } });

            // Should be command parameters set.
            Assert.IsNotNull(result);
            Assert.AreEqual("Serial", result.ParameterName);
            Assert.AreEqual("123", result.Value);

            // Confirm we tried to retrieve a factory.
            idb.Received().GetFactory("System.Data.UnitTest");

            // Command we set the proper command query.
            command.Received().CommandText = "Select TOP 1 from Table1";

            // Confirm we opened the connection.
            connection.Received().Open();

            // Confirm we executed the query.
            dataadapter.ReceivedWithAnyArgs().Fill(null);

            // Confirm our databae connection ended up closed.
            connection.Received().Dispose();
        }

        /// <summary>
        /// Test the select command with a null factory provider.
        /// </summary>
        [TestMethod]
        [ExcludeFromCodeCoverage]
        public void TestSelectCommandWithNullFactoryProvider()
        {
            // Create the DB utility.
            var db = new DatabaseUtil(string.Empty, "System.Data.UnitTest", null);

            var ex = Assert.ThrowsException<Exception>(() => db.Select("Select ALL"));
            Assert.AreEqual("Factory provider is null", ex.Message);
        }

        /// <summary>
        /// Test select command with a null factory.
        /// </summary>
        [TestMethod]
        [ExcludeFromCodeCoverage]
        public void TestSelectCommandWithNullFactory()
        {
            var idb = Substitute.For<IDbProviderFactories>();

            // We will return a factory when called our provider.
            idb.GetFactory("System.Data.UnitTest").Returns((DbProviderFactory)null);

            // Create the DB utility.
            var db = new DatabaseUtil(string.Empty, "System.Data.UnitTest", idb);

            var ex = Assert.ThrowsException<Exception>(() => db.Select("Select ALL"));
            Assert.AreEqual("Factory is null", ex.Message);
        }

        /// <summary>
        /// Test select command with a null connection.
        /// </summary>
        [TestMethod]
        [ExcludeFromCodeCoverage]

        public void TestSelectCommandWithNullConnectiony()
        {
            var idb = Substitute.For<IDbProviderFactories>();
            var factory = Substitute.For<DbProviderFactory>();

            idb.GetFactory(Arg.Any<string>()).Returns(factory);
            factory.CreateConnection().Returns((DbConnection)null);

            // We will return a factory when called our provider.
            idb.GetFactory("System.Data.UnitTest").Returns(factory);

            // Create the DB utility.
            var db = new DatabaseUtil(string.Empty, "System.Data.UnitTest", idb);

            var ex = Assert.ThrowsException<Exception>(() => db.Select("Select ALL"));
            Assert.AreEqual("Connection is null", ex.Message);
        }
    }
}
