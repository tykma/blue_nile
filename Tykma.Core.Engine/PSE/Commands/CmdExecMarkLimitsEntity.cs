﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdExecMarkObject.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.ExceptionServices;
    using System.Runtime.InteropServices;
    using Tykma.Core.Engine.CustomExceptions;
    using Tykma.Core.Engine.PSE.Commands;

    /// <summary>
    /// Command to  trace an individual object.
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal class CmdExecMarkLimitsEntity : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdExecMarkObject" /> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking API.</param>
        /// <param name="entName">Entity name.</param>
        public CmdExecMarkLimitsEntity(IntPtr lmcApi, string entName)
            : base(lmcApi)
        {
            this.EntityName = entName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CmdExecMarkObject"/> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking API.</param>
        public CmdExecMarkLimitsEntity(IntPtr lmcApi)
            : base(lmcApi)
        {
            this.EntityName = string.Empty;
        }



        /// <summary>
        /// Gets or sets the entity name.
        /// </summary>
        public string EntityName { get; set; }


        [DllImport("MarkPSE", EntryPoint = "lmc1_RedLightMarkByEnt", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern int TraceEntity(string EntName);

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>A return code message.</returns>
        [HandleProcessCorruptedStateExceptions]
        public bool Execute()
        {
            try
            {
                int result = TraceEntity(this.EntityName);

                if (result != 0)
                {
                    var rm = this.GetMiniLaseReturnCode(result);
                    throw new MethodFaultException(rm.ReturnDescription);
                }
                else
                {
                    return true;
                }
            }
            catch (AccessViolationException)
            {
                return false;
            }
        }
    }
}
