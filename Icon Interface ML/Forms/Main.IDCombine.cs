﻿// <copyright file="Main.IDCombine.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Icon_Interface.Forms
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using Tykma.Core.StringExtensions;

    /// <summary>
    /// ID combination form.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// Find and compose any 'combination IDs'.
        /// </summary>
        public void SetCombinationIDs()
        {
            bool combinationIDExists = false;

            foreach (DataGridViewRow row in this.ux_DataGridView.Rows)
            {
                var kvp = new KeyValuePair<string, string>(row.Cells[0].Value.ToString(), row.Cells[1].Value.ToString());

                if (kvp.Key.StartsWith("@"))
                {
                    combinationIDExists = true;
                    break;
                }
            }

            if (combinationIDExists)
            {
                foreach (DataGridViewRow row in this.ux_DataGridView.Rows)
                {
                    this.le.ResetSN(row.Cells[0].Value.ToString());
                }

                var ids = this.GetListOfIDs().ToList();

                List<string> combinationIDs = (from item in ids where item.Name.StartsWith("@") select item.Name).ToList();

                foreach (var item in combinationIDs)
                {
                    var parts = item.RemovePrefix("@").Split(',');

                    var sb = new StringBuilder();

                    foreach (var part in parts)
                    {
                        var id = ids.FirstOrDefault(x => x.Name.Equals(part));

                        if (id != null)
                        {
                            sb.Append(id.Value);
                        }
                    }

                    _ = this.ChangeTextByName(item, sb.ToString(), false);
                }

                this.UpdatePreview();
                this.InitDataGridView();
            }
        }
    }
}
