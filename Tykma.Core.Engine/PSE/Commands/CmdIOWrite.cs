﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdIOWrite.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// The command to write the I/O port.
    /// </summary>
    internal class CmdIOWrite : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdIOWrite"/> class.
        /// </summary>
        /// <param name="lmcApi">The LMC API.</param>
        /// <param name="portToWrite">The port to write.</param>
        public CmdIOWrite(IntPtr lmcApi, ushort portToWrite)
            : base(lmcApi)
        {
            this.PortToWrite = portToWrite;
        }

        /// <summary>
        /// Write the output port on the card.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns>Return code.</returns>
        private delegate int LMC1ApiWritePort([MarshalAs(UnmanagedType.U2)] ushort data);

        /// <summary>
        /// Gets the port to write.
        /// </summary>
        /// <value>
        /// The port to write.
        /// </value>
        private ushort PortToWrite { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        /// <exception cref="EngineNotConnectedException">Engine not connected.</exception>
        /// <exception cref="MethodFaultException">Problem setting outputs.</exception>
        internal bool Execute()
        {
            var writePort = this.LoadFunction<LMC1ApiWritePort>("lmc1_WritePort");

            if (writePort == null)
            {
                throw new EngineNotConnectedException();
            }

            var rm = this.GetMiniLaseReturnCode(writePort(this.PortToWrite));

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}