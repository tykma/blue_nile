﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AlphanumComparator.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Tykma.Icon.AlphanumComparator
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Natural sort comparer.
    /// </summary>
    public class AlphanumComparator : IComparer<string>
    {
        /// <summary>
        /// The regex.
        /// </summary>
        private static readonly Regex RegExp = new Regex(@"(?<=\D)(?=\d)|(?<=\d)(?=\D)", RegexOptions.Compiled);

        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        /// <returns>
        /// A signed integer that indicates the relative values of <paramref name="x" />
        /// and <paramref name="y" />, as shown in the following table.
        /// Value Meaning Less than zero<paramref name="x" /> is less than <paramref name="y" />.
        /// Zero<paramref name="x" /> equals <paramref name="y" />.
        /// Greater than zero<paramref name="x" /> is greater than <paramref name="y" />.
        /// </returns>
        public int Compare(string x, string y)
        {
            x = x.ToLower();
            y = y.ToLower();
            if (string.Compare(x, 0, y, 0, Math.Min(x.Length, y.Length)) == 0)
            {
                if (x.Length == y.Length)
                {
                    return 0;
                }

                return x.Length < y.Length ? -1 : 1;
            }

            var a = RegExp.Split(x);
            var b = RegExp.Split(y);
            int i = 0;
            while (true)
            {
                int r = PartCompare(a[i], b[i]);
                if (r != 0)
                {
                    return r;
                }

                ++i;
            }
        }

        /// <summary>
        /// Compares to parts.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <returns>
        /// Larger string.
        /// </returns>
        private static int PartCompare(string x, string y)
        {
            int a, b;
            if (int.TryParse(x, out a) && int.TryParse(y, out b))
            {
                return a.CompareTo(b);
            }

            return string.Compare(x, y, StringComparison.Ordinal);
        }
    }
}
