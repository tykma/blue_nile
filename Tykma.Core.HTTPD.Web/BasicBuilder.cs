﻿// <copyright file="BasicBuilder.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Core.HTTPD.Web
{
    using System.Collections.Generic;
    using System.IO;
    using System.Web.UI;

    /// <summary>
    /// A builder that creates a basic web page that allows the
    /// operator to interact with the tcp/ip httpd interface.
    /// </summary>
    public class BasicBuilder : IWebBuilder
    {
        /// <summary>
        /// Gets or sets the job list.
        /// </summary>
        /// <value>
        /// The job list.
        /// </value>
        private List<string> JobList { get; set; }

        /// <summary>
        /// Gets the page.
        /// </summary>
        /// <param name="jobList">The job list.</param>
        /// <returns>A long HTML string that builds the entire page.</returns>
        public string GetPage(List<string> jobList)
        {
            this.JobList = jobList;
            return this.Build();
        }

        /// <summary>
        /// Builds the page.
        /// </summary>
        /// <returns>A long HTML string that builds the entire page.</returns>
        private string Build()
        {
            StringWriter stringWriter = new StringWriter();

            using (HtmlTextWriter writer = new HtmlTextWriter(stringWriter))
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Html);
                writer.RenderBeginTag(HtmlTextWriterTag.Head);
                writer.RenderBeginTag(HtmlTextWriterTag.Body);
                writer.WriteBeginTag("p");
                writer.Write(HtmlTextWriter.TagRightChar);
                writer.Write("Icon Interface - Web API");
                writer.WriteEndTag("p");

                writer.WriteBreak();
                writer.WriteBreak();

                foreach (var item in this.JobList)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, $"./load_job?{item}");
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(item);
                    writer.RenderEndTag();
                    writer.WriteBreak();
                }

                writer.AddAttribute(HtmlTextWriterAttribute.Src, "./get_preview");
                writer.RenderBeginTag(HtmlTextWriterTag.Img);
                writer.RenderEndTag();
                writer.WriteBreak();

                // Project list command.
                this.AddCommandLink(writer, "project_list", "Job List", "Retrieve a list of available projects");

                // Preview command.
                this.AddCommandLink(writer, "get_preview", "Get Preview", "An image preview of current project");

                // Set ID Command.
                this.AddCommandLink(writer, "./set_id?id=serial&data=12345", "Set ID", "Set a dynamic ID within the current project");

                // Start
                this.AddCommandLink(writer, "start", "Start", "Start a marking cycle");

                // Stop
                this.AddCommandLink(writer, "stop", "Stop", "Stop a marking cycle");

                // Status
                this.AddCommandLink(writer, "state", "Status", "Get the current status of the cycle - idle or busy");

                // Get job status.
                this.AddCommandLink(writer, "job_status", "Job Status", "Get the current status of the job - loaded, marked, data set");

                // Job clear.
                this.AddCommandLink(writer, "clear_job", "Job Clear", "Clears out the loaded project and all data");

                // Version
                this.AddCommandLink(writer, "version", "Version", "Get the interface version");

                // All ids.
                this.AddCommandLink(writer, "all_ids", "Available IDs", "Get a list of all ids in the currently loaded template.");

                // Limits.
                this.AddCommandLink(writer, "limits", "Outline Trace", "Toggle contour tracing");

                // Limits on.
                this.AddCommandLink(writer, "limits_on", "Turn on limits", "Turn on contour tracing");

                // Limits off.
                this.AddCommandLink(writer, "limits_off", "Turn off limits", "Turn off contour tracing");

                // Get loaded job.
                this.AddCommandLink(writer, "loaded_job", "Current job", "Gets the currently selected template");

                // Enable a job.
                this.AddCommandLink(writer, "enable_job", "Job enable", "Enables a job - (NET_ENABLE job setting)");

                // Axis coordinate.
                this.AddCommandLink(writer, "axis_coor?axis=0", "Axis Position", "Get the current position of an axis");

                // Axis min max.
                this.AddCommandLink(writer, "get_axis?axis=0&value=min", "Axis Minimum", "Get the axis minimum position");

                // Axis min max.
                this.AddCommandLink(writer, "get_axis?axis=0&value=max", "Axis Maximum", "Get the axis maximum position");

                // Axis home.
                this.AddCommandLink(writer, "axis_home?axis=0", "Home Axis 0", "Reference axis 0");

                // Axis home.
                this.AddCommandLink(writer, "axis_home?axis=1", "Home Axis 1", "Reference axis 1");

                // Stop axis.
                this.AddCommandLink(writer, "stop_axis?axis=0", "Stop Axis", "Stop axis moevement");

                // Move axis
                this.AddCommandLink(writer, "move_axis?axis=0&position=100", "Move Axis", "Move Axis to a specified position");

                // Nudge object.
                this.AddCommandLink(writer, "nudge_object?id=1&x=2.0&y=2.33", "Nudge Object", "Nudge an object");

                // Nudge all objects.
                this.AddCommandLink(writer, "move_all?x=2&y=2", "Nudge All", "Nudge all available objects");

                // Rotate object.
                this.AddCommandLink(writer, "rotate_object?id=1&angle=35", "Rotate Object", "Rotate an object");

                // Rotate all.
                this.AddCommandLink(writer, "rotate_all?angle=35", "Rotate All Objects", "Rotate all objects");

                // Get Shutter State.
                this.AddCommandLink(writer, "shutter_state", "Shutter State", "Retrieve the state of the shutter");

                // Close door.
                this.AddCommandLink(writer, "door_close", "Close Door", "Close an automatic door");

                // Open door.
                this.AddCommandLink(writer, "door_open", "Open Door", "Open an automatic door");

                writer.RenderEndTag();
                writer.RenderEndTag();
                writer.RenderEndTag();
            }

            return stringWriter.ToString();
        }

        /// <summary>
        /// Adds a command link.
        /// </summary>
        /// <param name="writer">The html writer.</param>
        /// <param name="url">The URL.</param>
        /// <param name="header">The header.</param>
        /// <param name="commanddesc">The commanddesc.</param>
        private void AddCommandLink(HtmlTextWriter writer, string url, string header, string commanddesc)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Href, $"./{url}");
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write($"*** {header} ***");
            writer.RenderEndTag();
            writer.Write($" : {commanddesc}");
            writer.WriteBreak();
        }
    }
}
