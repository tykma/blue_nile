﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MarkingJob.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.MarkingJob
{
    /// <summary>
    /// Marking job class - it will hold job states.
    /// Whether job is loaded, authorized and the name of the job.
    /// </summary>
    public class MarkingJob
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MarkingJob"/> class.
        /// </summary>
        public MarkingJob()
        {
            this.Unload();
        }

        /// <summary>
        /// Gets or sets the name of the job.
        /// </summary>
        /// <value>
        /// The name of the job.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [job loaded].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [job loaded]; otherwise, <c>false</c>.
        /// </value>
        public bool Loaded { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [job authorized].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [job authorized]; otherwise, <c>false</c>.
        /// </value>
        public bool Authorized { get; set; }

        /// <summary>
        /// Unloads the job.
        /// </summary>
        public void Unload()
        {
            this.Name = string.Empty;
            this.Loaded = false;
            this.Authorized = false;
        }
    }
}
