﻿// <copyright file="IDBLogic.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Integration.ExternalDB
{
    /// <summary>
    /// DB Logic interface.
    /// </summary>
    public interface IDBLogic
    {
        /// <summary>
        /// Get data from a database.
        /// </summary>
        /// <param name="value">Unique key to look at.</param>
        /// <returns>DBData class.</returns>
        DBData GetData(string value);
    }
}
