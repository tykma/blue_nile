﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.GLOBALS.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//  Global variable handling of the main UI.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Forms
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;
    using System.Xml;

    using Icon_Interface.General;
    using Icon_Interface.Properties;

    using Tykma.Core.Entity;
    using Tykma.Icon.Globals;

    /// <summary>
    /// Partial class of main - handle interaction with our global variables class and the UX.
    /// </summary>
    public partial class Main
    {
        /// <summary>
        /// The list of counters consumed.
        /// </summary>
        private readonly List<string> listOfCountersConsumed = new List<string>();

        /// <summary>
        /// The list of array counters consumed.
        /// </summary>
        private readonly List<string> listOfArrayCountersConsumed = new List<string>();

        /// <summary>
        /// The global variable class.
        /// </summary>
        private GlobalVariables gv;

        /// <summary>
        /// Initializes the global variables.
        /// </summary>
        private void InitializeGlobalVariables()
        {
            this.gv = new GlobalVariables(this.fileOperations.GetGlobalVariableFile());

            try
            {
                this.gv.LoadGlobalVariableData();
            }
            catch (XmlException)
            {
                Logger.Error("Problem with globals XML file");
            }
            catch (InvalidOperationException)
            {
                Logger.Error("Problem with reading globals XML file");
            }
            catch (FileNotFoundException)
            {
                Logger.Error("Missing globals XML file");
            }

            this.ConfigureGlobalViews();
        }

        /// <summary>
        /// Sets all global variables.
        /// </summary>
        private async void SetAllGlobalVariables(IEnumerable<EntityInformation> ids = null)
        {
            var theids = ids;
            if (theids == null)
            {
                theids = this.GetListOfIDs();
            }

            this.listOfCountersConsumed.Clear();

            foreach (var entity in theids)
            {
                var gvitem = this.gv.GetGlobalVariable(entity.Name);
                if (gvitem.HasBeenSet)
                {
                    EntityInformation entity1 = entity;
                    await this.ChangeTextByName(entity1.Name, gvitem.DisplayValue);

                    if (gvitem.Counter)
                    {
                        this.listOfCountersConsumed.Add(gvitem.Name);
                        this.TimeStampEvent(string.Format("{0} - {1}:{2}", Resources.Msg_Set_Global_Counter, entity.Name, gvitem.Value));
                        Logger.Info("{0} - {1}:{2}", Resources.Msg_Set_Global_Counter, entity.Name, gvitem.Value);
                    }
                    else
                    {
                        this.TimeStampEvent(string.Format("{0} - {1}:{2}", Resources.Msg_Set_Global_String, entity.Name, gvitem.Value));
                        Logger.Info("{0} - {1}:{2}", Resources.Msg_Set_Global_String, entity.Name, gvitem.Value);
                    }
                }
            }

            if (this.ArrayJobLoaded)
            {
                this.SetMarkArraySerializationPreMark();
            }
        }

        private async void SetMarkArraySerializationPreMark()
        {
            var uniq = this.AllIDs.Select(x => x.NameNoIndex).Distinct();

            // Let's get all of our global variables.
            foreach (var item in this.gv.GetAllCounters())
            {
                if (uniq.Contains(item.Name))
                {
                    // This is our starting serial number
                    var sn = (GlobalVariableItem)this.gv.GetGlobalVariable(item.Name).Clone();

                    // Let's get all our array items.
                    var arrayitems = this.AllIDs.Where(x => x.NameNoIndex.Equals(item.Name)).OrderBy(f => f.SerializationIndex);

                    foreach (var snarrayitem in arrayitems)
                    {
                        sn = sn.Increment();
                        await this.ChangeTextByName(snarrayitem.Name, sn.DisplayValue);
                    }
                }
            }
        }

        private void SetMarkArraySerializationPostMark(IEnumerable<EntityInformation> ids = null)
        {
            var allids = ids;

            // Let's get all the IDs.
            if (allids == null)
            {
                allids = this.GetListOfIDs();
            }

            if (this.MarkedItemsInArray == null)
            {
                return;
            }

            // Let's find all the unique marked items.
            var unique = this.MarkedItemsInArray.Select(x => x.NameNoIndex).Distinct();

            var markedglobals = new List<string>();

            // Now let's find all that are global variables.
            foreach (var item in this.gv.GetAllCounters())
            {
                if (unique.Contains(item.Name))
                {
                    markedglobals.Add(item.Name);
                }
            }

            // Now let's get the last item for each marked global and set the gv to that.
            foreach (var m in markedglobals)
            {
                var last = this.MarkedItemsInArray.Where(x => x.NameNoIndex.Equals(m)).OrderBy(f => f.SerializationIndex).Last();

                string value = allids.Where(x => x.Name.Equals(last.Name)).Select(x => x.Value).FirstOrDefault();
                if (value != null)
                {
                    this.gv.ModifyCounter(m, value);
                }
            }
        }

        /// <summary>
        /// Increments the used counters.
        /// </summary>
        private void IncrementConsumedCounters(IEnumerable<EntityInformation> ids = null)
        {
            var allids = ids;
            if (allids == null)
            {
                allids = this.GetListOfIDs();
            }

            this.gv.IncrementUsedCounters(this.listOfCountersConsumed);
            this.gv.IncrementUsedArrayCounters(this.listOfArrayCountersConsumed);
            this.PopulateCounterView();
            this.SetAllGlobalVariables(allids);
            this.InitDataGridView(allids);
            this.UpdatePreview();
        }

        /// <summary>
        /// Configures the data grid view for global variables.
        /// </summary>
        private void ConfigureGlobalViews()
        {
            foreach (var dgv in new List<DataGridView> { this.ux_dgv_GVCounters, this.ux_dgv_Strings })
            {
                dgv.Use(p =>
                {
                    p.DataSource = null;
                    p.Columns.Clear();
                    p.ColumnCount = 2;
                    p.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    p.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    p.Columns[0].DefaultCellStyle.Alignment =
                        DataGridViewContentAlignment.MiddleLeft;
                    p.Columns[0].ReadOnly = true;
                    p.Columns[1].ReadOnly = false;
                    p.Columns[1].DefaultCellStyle.Alignment =
                        DataGridViewContentAlignment.MiddleLeft;
                    p.Columns[1].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                    p.AllowUserToOrderColumns = false;
                    p.ColumnHeadersVisible = true;
                    p.RowHeadersVisible = false;
                    p.Columns[0].HeaderCell.Value = Resources.GridViewId;
                    p.Columns[1].HeaderCell.Value = Resources.GridViewValue;
                    p.BackgroundColor = this.BackColor;
                    p.AllowUserToAddRows = false;
                    p.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                });
            }

            this.PopulateGlobalViews();
        }

        /// <summary>
        /// Populates the global views.
        /// </summary>
        private void PopulateGlobalViews()
        {
            this.PopulateCounterView();
            this.PopulateStringsView();
        }

        /// <summary>
        /// Populates the counter view.
        /// </summary>
        private void PopulateCounterView()
        {
            this.ux_dgv_GVCounters.CellValidating -= this.GvCountersCellValidating;
            this.ux_dgv_GVCounters.Rows.Clear();
            foreach (var kvp in this.gv.GetAllCounters())
            {
                // ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
                if (kvp.AlphaNumeric)
                {
                    this.ux_dgv_GVCounters.Rows.Add(kvp.Name, string.Format("{0} ({1})", kvp.Value, kvp.DisplayValue));
                }
                else
                {
                    this.ux_dgv_GVCounters.Rows.Add(kvp.Name, kvp.Value);
                }
            }

            this.ux_dgv_GVCounters.CellValidating += this.GvCountersCellValidating;
        }

        /// <summary>
        /// Populates the strings view.
        /// </summary>
        private void PopulateStringsView()
        {
            while (this.ux_dgv_Strings.CurrentRow != null)
            {
                this.ux_dgv_Strings.Rows.Remove(this.ux_dgv_Strings.CurrentRow);
            }

            foreach (GlobalVariableItem ent in this.gv.GetAllStrings())
            {
                this.ux_dgv_Strings.Rows.Add(ent.Name, ent.Value);
            }
        }

        /// <summary>
        /// Handles the Click event of the Button_GVCounterAdd control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ButtonGvCounterAddClick(object sender, EventArgs e)
        {
            // Tell the form we're asking for a counter - this will filter out non-numeric values.
            var addForm = new GlobalVariableAdd { IsCounter = true };

            if (addForm.ShowDialog(this) == DialogResult.OK)
            {
                this.gv.AddGlobalVariable(addForm.IDName, addForm.IDValue, addForm.SelectedVariableType);
            }

            this.PopulateCounterView();
        }

        /// <summary>
        /// Handles the Click event of the Button_GVCounterDel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ButtonGvCounterDelClick(object sender, EventArgs e)
        {
            if (MessageBox.Show(
                Resources.Main_Button_GVCounterDel_Click_Are_you_sure_you_want_to_delete_global_IDs,
                Resources.Main_Button_GVStringDel_Click_Delete_Global_IDs,
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning) != DialogResult.Yes)
            {
                return;
            }

            // Look for any selected cell, get its row and add it to our rows list.
            var rows = this.ux_dgv_GVCounters.SelectedCells.Cast<DataGridViewCell>()
                                     .Select(x => x.RowIndex).Distinct()
                                     .ToList();

            // Go through every row and add column 1 (the id) to the selected ids list.
            var selectedIdentifiers = rows.Select(i => this.ux_dgv_GVCounters.Rows[i].Cells[0].Value.ToString()).ToList();

            // Call the delete counters method.
            this.gv.DeleteVariables(selectedIdentifiers, true);

            // Repopulate the data grid view.
            this.PopulateCounterView();
        }

        /// <summary>
        /// Handles the Click event of the Button_GVCounterReset control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ButtonGvCounterResetClick(object sender, EventArgs e)
        {
            // Look for any selected cell, get its row and add it to our rows list.
            var rows = this.ux_dgv_GVCounters.SelectedCells.Cast<DataGridViewCell>()
                                     .Select(x => x.RowIndex).Distinct()
                                     .ToList();

            // Go through every row and add column 1 (the id) to the selected ids list.
            var selectedIdentifiers = rows.Select(i => this.ux_dgv_GVCounters.Rows[i].Cells[0].Value.ToString()).ToList();

            // Call the delete counters method.
            this.gv.ResetCounters(selectedIdentifiers);

            // Repopulate the data grid view.
            this.PopulateCounterView();
        }

        /// <summary>
        /// Handles the Click event of the Button_GVStringAdd control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ButtonGvStringAddClick(object sender, EventArgs e)
        {
            // Tell the form we're asking for a string - this will not filter out non-numeric values.
            var addForm = new GlobalVariableAdd { IsCounter = false };

            if (addForm.ShowDialog(this) == DialogResult.OK)
            {
                this.gv.AddGlobalVariable(addForm.IDName, addForm.IDValue, addForm.SelectedVariableType);
            }

            this.PopulateStringsView();
        }

        /// <summary>
        /// Handles the Click event of the Button_GVStringDel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ButtonGvStringDelClick(object sender, EventArgs e)
        {
            if (
                MessageBox.Show(
                Resources.Main_Button_GVCounterDel_Click_Are_you_sure_you_want_to_delete_global_IDs,
                Resources.Main_Button_GVStringDel_Click_Delete_Global_IDs,
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning) != DialogResult.Yes)
            {
                return;
            }

            // Look for any selected cell, get its row and add it to our rows list.
            var rows = this.ux_dgv_Strings.SelectedCells.Cast<DataGridViewCell>()
                                     .Select(x => x.RowIndex).Distinct()
                                     .ToList();

            // Go through every row and add column 1 (the id) to the selected ids list.
            var selectedIdentifiers = rows.Select(i => this.ux_dgv_Strings.Rows[i].Cells[0].Value.ToString()).ToList();

            // Call the delete counters method.
            this.gv.DeleteVariables(selectedIdentifiers, false);

            // Repopulate the data grid view.
            this.PopulateStringsView();
        }

        /// <summary>
        /// Handles the Click event of the Button_GVStringModify control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ButtonGvStringModifyClick(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Handles the CellValidating event of the GVCounters control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataGridViewCellValidatingEventArgs"/> instance containing the event data.</param>
        private void GvCountersCellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (this.ux_DataGridView.Columns.Count == 0)
            {
                return;
            }

            if (this.ux_DataGridView.Columns[e.ColumnIndex].Index != 1)
            {
                return;
            }

            if ((string)e.FormattedValue == this.ux_dgv_GVCounters[1, e.RowIndex].Value.ToString())
            {
                return;
            }

            string idName = this.ux_dgv_GVCounters[0, e.RowIndex].Value.ToString();

            bool result = this.gv.ModifyCounter(idName, e.FormattedValue.ToString());

            if (result)
            {
                this.TimeStampEvent(string.Format("{0} {1}", Resources.Msg_Changed_Global_Variable, e.FormattedValue));

                Logger.Debug("{0} {1}", Resources.Msg_Changed_Global_Variable, e.FormattedValue);
            }
            else
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Handles the CellValidating event of the Strings control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataGridViewCellValidatingEventArgs"/> instance containing the event data.</param>
        private void StringsCellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (this.ux_dgv_Strings.Columns[e.ColumnIndex].Index != 1)
            {
                return;
            }

            string idName = this.ux_dgv_Strings[0, e.RowIndex].Value.ToString();
            bool result = this.gv.ModifyString(idName, e.FormattedValue.ToString());

            if (result)
            {
                this.TimeStampEvent(string.Format("{0} {1}", Resources.Msg_Changed_Global_Variable, e.FormattedValue));
                Logger.Debug("{0} {1}", Resources.Msg_Changed_Global_Variable, e.FormattedValue);
            }
            else
            {
                e.Cancel = true;
            }
        }
    }
}
