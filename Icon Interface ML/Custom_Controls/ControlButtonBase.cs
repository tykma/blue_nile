﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlButtonBase.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.Custom_Controls
{
    using System;
    using System.Windows.Forms;

    /// <summary>
    /// The control button base.
    /// </summary>
    public partial class ControlButtonBase : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlButtonBase" /> class.
        /// </summary>
        protected ControlButtonBase()
        {
            this.InitializeComponent();
            this.WireAllControls(this);
        }

        /// <summary>
        /// Sets the label text - exposed to hook into our translation routines.
        /// </summary>
        public override string Text
        {
            set
            {
                this.ux_Label_Action.Text = value;
            }
        }

        /// <summary>
        /// Wires all controls to a mouse click event.
        /// </summary>
        /// <param name="cont">The control.</param>
        private void WireAllControls(Control cont)
        {
            foreach (Control ctl in cont.Controls)
            {
                ctl.Click += this.CtlClick;
                ctl.MouseClick += this.ControlMouseClick;
                if (ctl.HasChildren)
                {
                    this.WireAllControls(ctl);
                }
            }
        }

        /// <summary>
        /// Set up click control.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void CtlClick(object sender, EventArgs e)
        {
            this.InvokeOnClick(this, EventArgs.Empty);
        }

        /// <summary>
        /// Controls the mouse click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void ControlMouseClick(object sender, MouseEventArgs e)
        {
            this.OnMouseClick(e);
        }
    }
}
