﻿// <copyright file="IDateCode.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Icon.DateCode
{
    using System;

    /// <summary>
    /// Date code interface.
    /// </summary>
    public interface IDateCode
    {
        /// <summary>
        /// Get a date string given a format - for the current time.
        /// </summary>
        /// <param name="format">Datecode format.</param>
        /// <returns>A datecode string.</returns>
        string GetDateCode(string format);

        /// <summary>
        /// Get a date string given a format - for the provided time.
        /// </summary>
        /// <param name="format">Datecode format.</param>
        /// <param name="date">The date.</param>
        /// <returns>A datecode string.</returns>
        string GetDateCode(string format, DateTime date);
    }
}
