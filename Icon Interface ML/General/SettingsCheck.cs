﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SettingsCheck.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// CHeck application settings for corruption and fix them if possible.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.General
{
    using System.Configuration;

    /// <summary>
    /// Settings checker class.
    /// </summary>
    internal class SettingsCheck
    {
        /// <summary>
        /// Checks the settings and recovers if there is a fault.
        /// </summary>
        /// <returns>Whether or not the config file was reset.</returns>
        internal bool CheckSettings()
        {
            var isReset = false;

            /* Let's load our configuration file.If we get an
             exception we will handle the failure by recreating the file. */
            try
            {
                ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);
            }
            catch (ConfigurationErrorsException ex)
            {
                // Let's identify the configuration file which we are getting a corruption on.
                string filename = string.Empty;
                if (!string.IsNullOrEmpty(ex.Filename))
                {
                    filename = ex.Filename;
                }
                else
                {
                    var innerEx = ex.InnerException as ConfigurationErrorsException;
                    if (innerEx != null && !string.IsNullOrEmpty(innerEx.Filename))
                    {
                        filename = innerEx.Filename;
                    }
                }

                // Delete the file and wait until it is actually deleted.
                // The framework will recreate the file itself.
                if (!string.IsNullOrEmpty(filename))
                {
                    if (System.IO.File.Exists(filename))
                    {
                        var fileInfo = new System.IO.FileInfo(filename);
                        if (fileInfo.Directory != null)
                        {
                            var watcher = new System.IO.FileSystemWatcher(fileInfo.Directory.FullName, fileInfo.Name);
                            System.IO.File.Delete(filename);
                            isReset = true;
                            if (System.IO.File.Exists(filename))
                            {
                                watcher.WaitForChanged(System.IO.WatcherChangeTypes.Deleted);
                            }
                        }
                    }
                }
            }

            return isReset;
        }
    }
}