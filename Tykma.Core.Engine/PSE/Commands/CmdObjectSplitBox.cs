﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdObjectSplitBox.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   The command to split a window into boxes.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;

    using CustomExceptions;


    /// <summary>
    /// Split the box.
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal class CmdObjectSplitBox : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdObjectSplitBox"/> class.
        /// </summary>
        /// <param name="lmcApi">The LMC API.</param>
        /// <param name="splitBoxes">Split box list..</param>
        public CmdObjectSplitBox(IntPtr lmcApi, IList<SplitBox> splitBoxes)
            : base(lmcApi)
        {
            this.SplitBoxes = splitBoxes;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CmdObjectSplitBox"/> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking API.</param>
        public CmdObjectSplitBox(IntPtr lmcApi)
            : base(lmcApi)
        {
        }

        /// <summary>
        /// Split a marking window by boxes.
        /// </summary>
        /// <param name="pBoxs">Box array./</param>
        /// <param name="nBox">Number of boxes.</param>
        /// <param name="dBoxErr">Result.</param>
        /// <returns>Return code.</returns>
        private delegate int SplitAllCurveEntByBox(SplitBox[] pBoxs, int nBox, double dBoxErr);

        /// <inheritdoc />
        public IList<SplitBox> SplitBoxes { get; set; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        public bool Execute()
        {
            // Get a pointer to the change text function.
            var func = this.LoadFunction<SplitAllCurveEntByBox>("lmc1_SplitAllCurveEntByBox");

            if (func == null)
            {
                throw new EngineNotConnectedException();
            }

            double err = 0;
            int result = func(this.SplitBoxes.ToArray(), this.SplitBoxes.Count, err);

            //// Get success state.
            ReturnCodeMessage rm = this.GetMiniLaseReturnCode(result);

            if (rm.Success)
            {
                return true;
            }
            else
            {
                throw new Exception(rm.ReturnDescription);
            }
        }
    }
}
