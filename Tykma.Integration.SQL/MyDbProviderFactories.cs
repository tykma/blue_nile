﻿// <copyright file="MyDbProviderFactories.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Integration.SQL
{
    using System.Data.Common;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Provider for DB factories.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class MyDbProviderFactories : IDbProviderFactories
    {
        /// <summary>
        /// Get the factory provider.
        /// </summary>
        /// <param name="name">Name of the provider.</param>
        /// <returns>A DB factory provider.</returns>
        public DbProviderFactory GetFactory(string name)
        {
            return DbProviderFactories.GetFactory(name);
        }
    }
}