﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReturnStateParse.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.ElectroxEngine
{
    /// <summary>
    /// Parse a return state.
    /// </summary>
    public class ReturnStateParse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReturnStateParse"/> class.
        /// </summary>
        /// <param name="response">The response.</param>
        public ReturnStateParse(string response)
        {
            this.ResponseString = response;
        }

        /// <summary>
        /// Possible response states from the electrox card.
        /// </summary>
        public enum EStatResponses
        {
            /// <summary>
            /// OK command..
            /// </summary>
            Ok,

            /// <summary>
            /// Unknown command.
            /// </summary>
            UnknownCommand,

            /// <summary>
            /// Bad command format.
            /// </summary>
            BadCommandFormat,

            /// <summary>
            /// Program not found.
            /// </summary>
            ProgramNotFound,

            /// <summary>
            /// Laser busy.
            /// </summary>
            Busy,

            /// <summary>
            /// The shutter not enabled or program not selected.
            /// </summary>
            ShutterNotEnabledOrProgramNotSelected,

            /// <summary>
            /// The laser not on.
            /// </summary>
            LaserNotOn,

            /// <summary>
            /// The quit or stop.
            /// </summary>
            QuitOrStop,

            /// <summary>
            /// The mark aborted.
            /// </summary>
            MarkAborted,

            /// <summary>
            /// The mark finished.
            /// </summary>
            MarkFinished,

            /// <summary>
            /// The estop active.
            /// </summary>
            EstopActive,

            /// <summary>
            /// The user interlock.
            /// </summary>
            UserInterlock,

            /// <summary>
            /// The marking shutter not enabled.
            /// </summary>
            MarkingShutterNotEnabled,

            /// <summary>
            /// The marking laser not on.
            /// </summary>
            MarkingLaserNotOn,

            /// <summary>
            /// The galvos are tuning.
            /// </summary>
            GalvosTuning
        }

        /// <summary>
        /// Gets the response string.
        /// </summary>
        /// <value>
        /// The response string.
        /// </value>
        private string ResponseString { get; }

        /// <summary>
        /// Parses a command and returns an enumerator.
        /// </summary>
        /// <returns>
        /// An enumerator - laser status.
        /// </returns>
        public EStatResponses CommandParse()
        {
            if (this.ResponseString.StartsWith("<R16"))
            {
                return EStatResponses.MarkingLaserNotOn;
            }
            else if (this.ResponseString.StartsWith("<R15"))
            {
                return EStatResponses.MarkingShutterNotEnabled;
            }
            else if (this.ResponseString.StartsWith("<R12"))
            {
                return EStatResponses.GalvosTuning;
            }
            else if (this.ResponseString.StartsWith("<R11"))
            {
                return EStatResponses.UserInterlock;
            }
            else if (this.ResponseString.StartsWith("<R10"))
            {
                return EStatResponses.EstopActive;
            }
            else if (this.ResponseString.StartsWith("<R9"))
            {
                return EStatResponses.MarkFinished;
            }
            else if (this.ResponseString.StartsWith("<R8"))
            {
                return EStatResponses.MarkAborted;
            }
            else if (this.ResponseString.StartsWith("<R7"))
            {
                return EStatResponses.QuitOrStop;
            }
            else if (this.ResponseString.StartsWith("<R6"))
            {
                return EStatResponses.LaserNotOn;
            }
            else if (this.ResponseString.StartsWith("<R5"))
            {
                return EStatResponses.ShutterNotEnabledOrProgramNotSelected;
            }
            else if (this.ResponseString.StartsWith("<R4"))
            {
                return EStatResponses.Busy;
            }
            else if (this.ResponseString.StartsWith("<R3"))
            {
                return EStatResponses.ProgramNotFound;
            }
            else if (this.ResponseString.StartsWith("<R2"))
            {
                return EStatResponses.BadCommandFormat;
            }
            else if (this.ResponseString.StartsWith("<R1"))
            {
                return EStatResponses.UnknownCommand;
            }
            else if (this.ResponseString.StartsWith("<R0"))
            {
                return EStatResponses.Ok;
            }
            else
            {
                return EStatResponses.Ok;
            }
        }
    }
}
