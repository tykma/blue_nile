﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tykma.Core.Entity;

namespace Tykma.Icon.ArrayMarkingHelpers
{
    public class ArrayHelper
    {
        public bool IsArray(IEnumerable<Core.Entity.EntityInformation> ids)
        {
            bool array = false;
            foreach (var item in ids)
            {
                if (item.IndexCount > 0)
                {
                    array = true;
                    break;
                }
            }

            return array;
        }

        public int ArrayMax(IEnumerable<Core.Entity.EntityInformation> ids)
        {
            int max = 0;
            foreach (var item in ids)
            {
                if (item.IndexCount > max)
                {
                    max = item.IndexCount;
                }
            }

            return max;
        }

        /// <summary>
        /// Gets a list of entities that we should mark given an entity list and a quantity.
        /// </summary>
        /// <param name="ids">List of IDs in the project.</param>
        /// <param name="count">How many cells we would like to mark.</param>
        /// <returns>List of entities that should be marked.</returns>
        public List<EntityInformation> EntsToMark(IEnumerable<Core.Entity.EntityInformation> ids, int count)
        {
            var markList = new List<EntityInformation>();


            foreach (var item in ids)
            {
                if (item.IndexCount <= count)
                {
                    markList.Add(item);
                }
            }

            var uniqueEnts = new List<string>();

            int maxItems = 0;
            foreach (var item in ids)
            {
                if (item.IndexCount > maxItems)
                {
                    maxItems = item.IndexCount;
                }

                if (!uniqueEnts.Contains(item.NameNoIndex))
                {
                    uniqueEnts.Add(item.NameNoIndex);
                }
            }

            var orderedList = new List<EntityInformation>();

            for (int i = 1; i <= maxItems; i++)
            {
                foreach (var item in uniqueEnts)
                {
                    var allitemswithname = markList.Where(x => x.NameNoIndex.Equals(item));

                    foreach (var n in allitemswithname)
                    {
                        if (n.IndexCount == i)
                        {
                            orderedList.Add(n);
                            break;
                        }
                    }
                }
            }

            return orderedList;
        }
    }
}
