﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NativeMethods.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.OSInfo
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// Native Methods.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements must be documented", Justification = "Well documented by microsoft.")]
    internal static class NativeMethods
    {
        [DllImport("user32.dll")]
        internal static extern bool GetSystemMetrics([In] int nIndex);

        [DllImport("kernel32.dll")]
        internal static extern bool VerifyVersionInfo(ref OSInfo.OSVERSIONINFOEX osVersionInfo, [In] uint dwTypeMask, [In] ulong dwlConditionMask);

        [DllImport("kernel32.dll")]
        internal static extern ulong VerSetConditionMask([In] ulong dwlConditionMask, [In] uint dwTypeBitMask, [In] byte dwConditionMask);

        [DllImport("kernel32.dll")]
        internal static extern bool GetVersionEx(ref OSInfo.OSVERSIONINFOEX osVersionInfo);

        [DllImport("kernel32.dll")]
        internal static extern bool GetProductInfo(
          [In] int dwOSMajorVersion,
          [In] int dwOSMinorVersion,
          [In] int dwSpMajorVersion,
          [In] int dwSpMinorVersion,
          [Out] out int pdwReturnedProductType);
    }
}
