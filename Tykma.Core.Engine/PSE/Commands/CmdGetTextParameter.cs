﻿// <copyright file="CmdGetTextParameter.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;
    using System.Text;
    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// Command to get text object's parameters.
    /// </summary>
    internal class CmdGetTextParameter : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdGetTextParameter"/> class.
        /// </summary>
        /// <param name="lmcApi">SE API pointer.</param>
        /// <param name="ent">Entity name.</param>
        public CmdGetTextParameter(IntPtr lmcApi, string ent) :
            base(lmcApi)
        {
            this.WhichEntity = ent;
        }

        /// <summary>
        /// Gets the name of the entity to review.
        /// </summary>
        private string WhichEntity { get; }

        private delegate int lmc1_GetTextEntParam4(
            [MarshalAs(UnmanagedType.LPWStr)] string pEntName,
            [MarshalAs(UnmanagedType.LPWStr)]  StringBuilder fontName,
             ref int nTextSpaceMode,
             ref double dTextSpace,
             ref double CharHeight,
             ref double CharWidthRatio,
             ref double CharAngle,
             ref double CharSpace,
             ref double LineSpace,
             ref double dNullCharWidthRatio,
             ref int nTextAlign,
             ref bool bBold,
             ref bool bItalic);

        internal TextParameter Execute()
        {
            var cmd = this.LoadFunction<lmc1_GetTextEntParam4>("lmc1_GetTextEntParam4");

            if (cmd == null)
            {
                throw new MethodFaultException("Missing method: lmc1_GetTextEntParam4.");
            }

            StringBuilder fontName = new StringBuilder(255);
            int textspacemode = 0, textalign = 0;
            double textspace = 0, charheight = 0, charwidthratio = 0, charangle = 0, charspace = 0, linespace = 0, nullcharwidthratio = 0;
            bool bold = false, italic = false;

            int result = cmd(
                this.WhichEntity,
                fontName,
                ref textspacemode,
                ref textspace,
                ref charheight,
                ref charwidthratio,
                ref charangle,
                ref charspace, ref linespace,
                ref nullcharwidthratio, ref textalign, ref bold, ref italic);

            string f;
            if (fontName == null)
            {
                f = string.Empty;
            }
            else
            {
                f = fontName.ToString();
            }

            var textparamter = new TextParameter(f, textspace, charheight, charwidthratio, charangle,
                charspace, linespace, nullcharwidthratio, textalign, bold, italic, textspacemode);

            return textparamter;
        }
    }
}
