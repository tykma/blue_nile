﻿// <copyright file="SimpleMessage.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>

namespace Tykma.Interface.NG.DialogService
{
    using System.Windows.Forms;

    /// <summary>
    /// Simple message form.
    /// </summary>
    public partial class SimpleMessage : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleMessage"/> class.
        /// </summary>
        public SimpleMessage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message
        {
            get
            {
                return this.ux_Message.Text;
            }

            set
            {
                this.ux_Message.Text = value;
            }
        }

        /// <summary>
        /// Handle the form closing event.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Form closing event args.</param>
        private void SimpleMessage_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }
    }
}
