﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdParamPenToggle.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;

    using CustomExceptions;

    /// <summary>
    /// The command that toggles a pen's enabled state.
    /// </summary>
    internal class CmdParamPenToggle : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdParamPenToggle"/> class.
        /// </summary>
        /// <param name="lmcApi">The LMC API.</param>
        /// <param name="pentotoggle">The pen to toggle.</param>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        public CmdParamPenToggle(IntPtr lmcApi, int pentotoggle, bool enable)
            : base(lmcApi)
        {
            this.PenToToggle = pentotoggle;
            this.EnablePen = enable;
        }

        /// <summary>
        /// Returns pen number associated with an entity.
        /// </summary>
        /// <param name="whichPen">Which pen.</param>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        /// <returns>
        /// Pointer to function.
        /// </returns>
        private delegate int LMC1ApiSetPenDisableState(int whichPen, bool enable);

        /// <summary>
        /// Gets the pen number to toggle.
        /// </summary>
        /// <value>
        /// The pen number to toggle.
        /// </value>
        private int PenToToggle { get; }

        /// <summary>
        /// Gets a value indicating whether to enable pen.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enable pen]; otherwise, <c>false</c>.
        /// </value>
        private bool EnablePen { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        /// <exception cref="EngineNotConnectedException">Engine is not connected.</exception>
        /// <exception cref="MethodFaultException">Problem toggling pen.</exception>
        internal bool Execute()
        {
            var lmc1SetPenDisable = this.LoadFunction<LMC1ApiSetPenDisableState>("lmc1_SetPenDisableState");

            if (lmc1SetPenDisable == null)
            {
                throw new EngineNotConnectedException();
            }

            var rm = this.GetMiniLaseReturnCode(lmc1SetPenDisable(this.PenToToggle, this.EnablePen));

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
