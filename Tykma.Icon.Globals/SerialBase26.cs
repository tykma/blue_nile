﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SerialBase26.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.Globals
{
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// The serial base 26.
    /// </summary>
    public class SerialBase26 : ISerializer
    {
        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns>
        /// Serialized number.
        /// </returns>
        public string GetValue(long number)
        {
            return this.IntToLetters(number);
        }

        /// <summary>
        /// Gets the value from a string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Integer to letters.</returns>
        public string GetValue(string value)
        {
            long lvalue;

            return long.TryParse(value, out lvalue) ? this.GetValue(lvalue) : string.Empty;
        }

        /// <summary>
        /// Converts integers to letters (bijective base 26)..
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Value in base 26.</returns>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Bijective is a word")]
        private string IntToLetters(long value)
        {
            string result = string.Empty;
            while (--value >= 0)
            {
                result = (char)('A' + (value % 26)) + result;
                value /= 26;
            }

            return result;
        }
    }
}
