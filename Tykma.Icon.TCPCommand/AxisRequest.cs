﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AxisRequest.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.TCPCommand
{
    using Tykma.Icon.AxisInfo;

    /// <summary>
    /// Axis request class.
    /// </summary>
    public class AxisRequest
    {
        /// <summary>
        /// The axis.
        /// </summary>
        private AxisSetting theaxis;

        /// <summary>
        /// Gets or sets a value indicating whether [valid axis].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [valid axis]; otherwise, <c>false</c>.
        /// </value>
        public bool ValidAxis { get; set; }

        /// <summary>
        /// Gets or sets the axis.
        /// </summary>
        /// <value>
        /// The axis.
        /// </value>
        public AxisSetting Axis
        {
            get
            {
                return this.theaxis;
            }

            set
            {
                this.theaxis = value;
                this.ValidAxis = true;
            }
        }
    }
}
