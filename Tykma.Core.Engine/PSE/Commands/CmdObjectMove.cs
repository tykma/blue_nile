﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdObjectMove.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// Command to move an object.
    /// </summary>
    internal class CmdObjectMove : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdObjectMove" /> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking API.</param>
        /// <param name="moveEnt">The move object.</param>
        /// <param name="movex">The move x.</param>
        /// <param name="movey">The move y.</param>
        public CmdObjectMove(IntPtr lmcApi, string moveEnt, double movex, double movey)
            : base(lmcApi)
        {
            this.ObjectToMove = moveEnt;
            this.MoveX = movex;
            this.MoveY = movey;
        }

        /// <summary>
        /// Moves an entity.
        /// </summary>
        /// <param name="pEntName">Name of the entity.</param>
        /// <param name="dMovex">X position.</param>
        /// <param name="dMovey">Y position.</param>
        /// <returns>Return code.</returns>
        private delegate int LMC1ApiMoveEnt(
            [MarshalAs(UnmanagedType.LPWStr)] string pEntName,
            double dMovex,
            double dMovey);

        /// <summary>
        /// Gets the object to move.
        /// </summary>
        /// <value>
        /// The object to move.
        /// </value>
        private string ObjectToMove { get; }

        /// <summary>
        /// Gets the move x position.
        /// </summary>
        /// <value>
        /// The move x position.
        /// </value>
        private double MoveX { get; }

        /// <summary>
        /// Gets the move y position.
        /// </summary>
        /// <value>
        /// The move y position.
        /// </value>
        private double MoveY { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        /// <exception cref="EngineNotConnectedException">Card not connected.</exception>
        /// <exception cref="MethodFaultException">Problem moving object.</exception>
        internal bool Execute()
        {
            var lmc1MoveReturn = this.LoadFunction<LMC1ApiMoveEnt>("lmc1_MoveEnt");

            if (lmc1MoveReturn == null)
            {
                throw new EngineNotConnectedException();
            }

            var rm = this.GetMiniLaseReturnCode(lmc1MoveReturn(this.ObjectToMove, this.MoveX, this.MoveY));

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}
