﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdObjectCreateText.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine.PSE.Commands
{
    using System;
    using System.Runtime.InteropServices;

    using Tykma.Core.Engine.CustomExceptions;

    /// <summary>
    /// The command to create a text object.
    /// </summary>
    internal class CmdObjectCreateText : CmdBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CmdObjectCreateText" /> class.
        /// </summary>
        /// <param name="lmcApi">The laser marking API.</param>
        /// <param name="objectToCreate">The object to create.</param>
        public CmdObjectCreateText(IntPtr lmcApi, TextObjectAttributes objectToCreate)
            : base(lmcApi)
        {
            this.ObjectToCreate = objectToCreate;
        }

        /// <summary>
        /// Add text to the the marking window.
        /// </summary>
        /// <param name="pStr">The entity string.</param>
        /// <param name="pEntName">Name of the entity.</param>
        /// <param name="dPosX">X position.</param>
        /// <param name="dPosY">Y position.</param>
        /// <param name="dPosZ">Z position .</param>
        /// <param name="nAlign">The n align.</param>
        /// <param name="dTextRotateAngle">The text rotate angle.</param>
        /// <param name="penNo">The pen no.</param>
        /// <param name="bHatchText">if set to <c>true</c> [hatch text].</param>
        /// <returns>Return Code</returns>
        private delegate int LMC1ApiAddTextToLib(
                                                [MarshalAs(UnmanagedType.LPWStr)] string pStr,
                                                [MarshalAs(UnmanagedType.LPWStr)] string pEntName,
                                                double dPosX,
                                                double dPosY,
                                                double dPosZ,
                                                int nAlign,
                                                double dTextRotateAngle,
                                                int penNo,
                                                bool bHatchText);

        /// <summary>
        /// Gets the object to create.
        /// </summary>
        /// <value>
        /// The object to create.
        /// </value>
        private TextObjectAttributes ObjectToCreate { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        /// <returns>Success state.</returns>
        /// <exception cref="EngineNotConnectedException">Engine not connected.</exception>
        /// <exception cref="MethodFaultException">Method fault.</exception>
        internal bool Execute()
        {
            var lmc1AddTextToLibReturn = this.LoadFunction<LMC1ApiAddTextToLib>("lmc1_AddTextToLib");

            if (lmc1AddTextToLibReturn == null)
            {
                throw new EngineNotConnectedException();
            }

            var result = lmc1AddTextToLibReturn(
                this.ObjectToCreate.Value,
               this.ObjectToCreate.Name,
                this.ObjectToCreate.PositionX,
                this.ObjectToCreate.PositionY,
                this.ObjectToCreate.Z,
                this.ObjectToCreate.Align,
                this.ObjectToCreate.RotationAngle,
                this.ObjectToCreate.PenNumber,
                this.ObjectToCreate.Hatched);

            ReturnCodeMessage rm = this.GetMiniLaseReturnCode(result);

            if (rm.Success)
            {
                return true;
            }

            throw new MethodFaultException(rm.ReturnDescription);
        }
    }
}