﻿// <copyright file="TestAxisSettings.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.Engine.ConfigSettings.UnitTests
{
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Tykma.Core.Engine.ConfigSettings;

    /// <summary>
    /// Test axis settings.
    /// </summary>
    [TestClass]
    public class TestAxisSettings
    {
        /// <summary>
        /// Tests the axis status.
        /// </summary>
        [TestMethod]
        public void TestAxisStatusBasic()
        {
            IAxisStatus status = new AxisStatusConfig(new List<string> { "ACTIVEAXIS0=0", "ACTIVEAXIS1=0" });
            Assert.IsFalse(status.IsAxisOn(0));
            Assert.IsFalse(status.IsAxisOn(1));

            IAxisStatus status2 = new AxisStatusConfig(new List<string> { "ACTIVEAXIS0=1", "ACTIVEAXIS1=1" });
            Assert.IsTrue(status2.IsAxisOn(0));
            Assert.IsTrue(status2.IsAxisOn(1));
        }

        /// <summary>
        /// Test toggling axis enabled state.
        /// </summary>
        [TestMethod]
        public void TestTogglingAxisEnabled()
        {
            var config = new List<string> { "ACTIVEAXIS0=0", "ACTIVEAXIS1=0" };

            var axisEnabler = new AxisStatusConfig(config);

            var l = axisEnabler.ToggleAxisEnabled(0, true);

            Assert.AreEqual("ACTIVEAXIS0=1", l[0]);
            Assert.AreEqual("ACTIVEAXIS1=0", l[1]);
        }

        /// <summary>
        /// Test toggling a non existent axis.
        /// </summary>
        [TestMethod]
        public void TestTogglingNonExistentAxis()
        {
            var status = new AxisStatusConfig(ConfigHelper.GetSampleFile());

            // Let's turn off axis 0 and 1.
            var l = status.ToggleAxisEnabled(0, false);
            var newstatus = new AxisStatusConfig(l).ToggleAxisEnabled(1, false);

            Assert.IsFalse(new AxisStatusConfig(newstatus).IsAxisOn(0));
            Assert.IsFalse(new AxisStatusConfig(newstatus).IsAxisOn(1));
        }

        /// <summary>
        /// Test disabling an axis.
        /// </summary>
        [TestMethod]
        public void TestDisablingAxis()
        {
            var config = new List<string> { "ACTIVEAXIS0=1", "ACTIVEAXIS1=1" };

            var axisEnabler = new AxisStatusConfig(config);

            var l = axisEnabler.ToggleAxisEnabled(0, false);

            Assert.AreEqual("ACTIVEAXIS0=0", l[0]);
            Assert.AreEqual("ACTIVEAXIS1=1", l[1]);
        }

        /// <summary>
        /// Test retrieving axis home speeds.
        /// </summary>
        [TestMethod]
        public void TestGettingAxisHomeSpeed()
        {
            IAxisStatus status = new AxisStatusConfig(new List<string> { "AXISZEROSPD1=1.500000e+003", "AXISZEROSPD0=1.600000e+003" });

            Assert.AreEqual(1600, status.GetAxisHomeSpeed(0));
            Assert.AreEqual(1500, status.GetAxisHomeSpeed(1));
        }

        /// <summary>
        /// Test setting the axis speed.
        /// </summary>
        [TestMethod]
        public void TestSettingAxisSpeeds()
        {
            var status = new AxisStatusConfig(ConfigHelper.GetSampleFile());

            var lines = status.SetAxisHomeSpeed(0, 1500);

            Assert.IsTrue(lines.Contains("AXISZEROSPD0=1.500000e+003"));
        }

        /// <summary>
        /// Test retrieving the axis home time out.
        /// </summary>
        [TestMethod]
        public void TestGettingHomeTimeOut()
        {
            var status = new AxisStatusConfig(ConfigHelper.GetSampleFile());
            Assert.AreEqual(10, status.GetAxisTimeOut(0));
            Assert.AreEqual(100, status.GetAxisTimeOut(1));
        }

        /// <summary>
        /// Test setting the axis home time out.
        /// </summary>
        [TestMethod]
        public void TestSettingHomeTimeOut()
        {
            IAxisStatus status = new AxisStatusConfig(new List<string> { "AXISTIMEOUT0=10", "AXISTIMEOUT1=100" });

            var lines = status.SetHomeTimeOut(0, 1500);
            Assert.AreEqual("AXISTIMEOUT0=1500", lines[0]);
            Assert.AreEqual("AXISTIMEOUT1=100", lines[1]);
        }

        /// <summary>
        /// Test retrieving data when the config is null.
        /// </summary>
        [TestMethod]
        public void TestRetrievingDataWithNullConfig()
        {
            IAxisStatus status = new AxisStatusConfig(null);
            Assert.AreEqual(0, status.GetAxisHomeSpeed(0));
            Assert.AreEqual(0, status.GetAxisTimeOut(0));
            Assert.IsFalse(status.IsAxisOn(0));
            Assert.AreEqual(0, status.GetAxisMaximumPosition(0));
            Assert.AreEqual(0, status.GetAxisMinimumPosition(0));
        }

        /// <summary>
        /// Test retrieving data when the config is empty.
        /// </summary>
        [TestMethod]
        public void TestRetrievingDataWithEmptyConfig()
        {
            IAxisStatus status = new AxisStatusConfig(new List<string>());
            Assert.AreEqual(0, status.GetAxisHomeSpeed(0));
            Assert.AreEqual(0, status.GetAxisTimeOut(0));
            Assert.IsFalse(status.IsAxisOn(0));
            Assert.AreEqual(0, status.GetAxisMaximumPosition(0));
            Assert.AreEqual(0, status.GetAxisMinimumPosition(0));
        }

        /// <summary>
        /// Test setting data with a null config.
        /// </summary>
        [TestMethod]
        public void TestSettingDataWithNullConfig()
        {
            IAxisStatus status = new AxisStatusConfig(null);
            Assert.AreEqual(0, status.SetAxisHomeSpeed(0, 0).Count);
            Assert.AreEqual(0, status.SetHomeTimeOut(0, 0).Count);
            Assert.AreEqual(0, status.SetAxisMaximumPosition(0, 0).Count);
            Assert.AreEqual(0, status.SetAxisMinPosition(0, 0).Count);
            Assert.AreEqual(0, status.ToggleAxisEnabled(0, true).Count);
        }

        /// <summary>
        /// Test setting data with an empty config.
        /// </summary>
        [TestMethod]
        public void TestSettingDataWithEmptyConfig()
        {
            IAxisStatus status = new AxisStatusConfig(new List<string>());
            Assert.AreEqual(0, status.SetAxisHomeSpeed(0, 0).Count);
            Assert.AreEqual(0, status.SetHomeTimeOut(0, 0).Count);
            Assert.AreEqual(0, status.SetAxisMaximumPosition(0, 0).Count);
            Assert.AreEqual(0, status.SetAxisMinPosition(0, 0).Count);
            Assert.AreEqual(0, status.ToggleAxisEnabled(0, true).Count);
        }

        /// <summary>
        /// Test getting the min position.
        /// </summary>
        [TestMethod]
        public void TestGettingMinimumPosition()
        {
            IAxisStatus status = new AxisStatusConfig(new List<string> { "AXISMINPOS0=-3.600000e+002", "AXISMINPOS1=0.000000e+000" });
            Assert.AreEqual(-360, status.GetAxisMinimumPosition(0));
            Assert.AreEqual(0, status.GetAxisMinimumPosition(1));
        }

        /// <summary>
        /// Test getting the max position.
        /// </summary>
        [TestMethod]
        public void TestGettingMaximumPosition()
        {
            IAxisStatus status = new AxisStatusConfig(new List<string> { "AXISMAXPOS0=3.600000e+002", "AXISMAXPOS1=1.000000e+003" });
            Assert.AreEqual(360, status.GetAxisMaximumPosition(0));
            Assert.AreEqual(1000, status.GetAxisMaximumPosition(1));
        }

        /// <summary>
        /// Test setting the min position.
        /// </summary>
        [TestMethod]
        public void TestSettingMinimumPosition()
        {
            IAxisStatus status = new AxisStatusConfig(new List<string> { "AXISMINPOS0=3.600000e+002", "AXISMINPOS1=1.000000e+003" });

            var lines = status.SetAxisMinPosition(0, 1500);

            Assert.AreEqual(1500, new AxisStatusConfig(lines).GetAxisMinimumPosition(0));

            lines = status.SetAxisMinPosition(1, 1000);
            Assert.AreEqual(1000, new AxisStatusConfig(lines).GetAxisMinimumPosition(1));
        }

        /// <summary>
        /// Test setting the max position.
        /// </summary>
        [TestMethod]
        public void TestSettingMaximumPosition()
        {
            IAxisStatus status = new AxisStatusConfig(new List<string> { "AXISMAXPOS0=3.600000e+002", "AXISMAXPOS1=1.000000e+003" });

            var lines = status.SetAxisMaximumPosition(0, 1500);

            Assert.AreEqual(1500, new AxisStatusConfig(lines).GetAxisMaximumPosition(0));

            lines = status.SetAxisMaximumPosition(1, 1000);
            Assert.AreEqual(1000, new AxisStatusConfig(lines).GetAxisMaximumPosition(1));
        }

        /// <summary>
        /// Test getting the pulses per round.
        /// </summary>
        [TestMethod]
        public void TestGetingPulsesPerRound()
        {
            var status = new AxisStatusConfig(ConfigHelper.GetSampleFile());
            Assert.AreEqual(12000, status.GetPulsesPerRound(0));
            Assert.AreEqual(200, status.GetPulsesPerRound(1));
        }

        /// <summary>
        /// Test getting pulses per round with empty config.
        /// </summary>
        [TestMethod]
        public void TestGettingPulsesPerRoundWithSectionMissing()
        {
            IAxisStatus status = new AxisStatusConfig(new List<string>());
            Assert.AreEqual(0, status.GetPulsesPerRound(0));
            Assert.AreEqual(0, status.GetPulsesPerRound(1));
        }

        /// <summary>
        /// Test getting the distance per round.
        /// </summary>
        [TestMethod]
        public void TestGetingDistancePerRound()
        {
            var status = new AxisStatusConfig(ConfigHelper.GetSampleFile());
            Assert.AreEqual(5, status.GetDistancePerRound(0));
            Assert.AreEqual(2, status.GetDistancePerRound(1));
        }

        /// <summary>
        /// Test getting distance per round with empty config.
        /// </summary>
        [TestMethod]
        public void TestGettingDistancePerRoundWithSectionMissing()
        {
            IAxisStatus status = new AxisStatusConfig(new List<string>());
            Assert.AreEqual(0, status.GetDistancePerRound(0));
            Assert.AreEqual(0, status.GetDistancePerRound(1));
        }
    }
}
