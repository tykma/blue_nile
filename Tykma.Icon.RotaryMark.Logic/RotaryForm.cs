﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tykma.Icon.RotaryMark.Logic
{
    using System.Runtime.CompilerServices;

    public partial class RotaryForm : Form, INotifyPropertyChanged
    {

        public decimal Z
        {
            get
            {
                return this.numericUpDown3.Value;
            }

            set
            {
                if (value != this.numericUpDown3.Value)
                {
                    this.numericUpDown3.Value = (decimal)value;
                }
            }
        }
        public decimal SplitSize
        {
            get
            {
                return this.numericUpDown2.Value;
            }

            set
            {
                if (value != this.numericUpDown2.Value)
                {
                    this.numericUpDown2.Value = (decimal)value;
                }
            }
        }

        public decimal Diameter
        {
            get
            {
                return this.numericUpDown1.Value;
            }

            set
            {
                if (value != this.numericUpDown1.Value)
                {
                    this.numericUpDown1.Value = (decimal)value;
                }
            }

        }
        public RotaryForm()
        {
            InitializeComponent();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            this.OnPropertyChanged(nameof(this.Diameter));
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            this.OnPropertyChanged(nameof(this.SplitSize));
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            this.OnPropertyChanged(nameof(this.Z));
        }
    }
}
