﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OutputsMemory.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
//   Work around for knowing output state given a lack of "Read output port".
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Core.Engine
{
    /// <summary>
    /// Work around for knowing output state given a lack of "Read output port".
    /// </summary>
    public class OutputsMemory
    {
        /// <summary>
        /// The output states.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields must be private", Justification = "Public as a hack to allow unit testing.")]
        public readonly bool[] OutputStates = new bool[16];

        /// <summary>
        /// Gets a value indicating whether this <see cref="OutputsMemory"/> is out1.
        /// </summary>
        /// <value>
        ///   <c>true</c> if out1; otherwise, <c>false</c>.
        /// </value>
        public bool Out1 => this.OutputStates[0];

        /// <summary>
        /// Gets a value indicating whether this <see cref="OutputsMemory" /> is out2.
        /// </summary>
        /// <value>
        ///   <c>true</c> if out2; otherwise, <c>false</c>.
        /// </value>
        public bool Out2 => this.OutputStates[1];

        /// <summary>
        /// Gets a value indicating whether this <see cref="OutputsMemory" /> is out3.
        /// </summary>
        /// <value>
        ///   <c>true</c> if out3; otherwise, <c>false</c>.
        /// </value>
        public bool Out3 => this.OutputStates[2];

        /// <summary>
        /// Gets a value indicating whether this <see cref="OutputsMemory" /> is out4.
        /// </summary>
        /// <value>
        ///   <c>true</c> if out4; otherwise, <c>false</c>.
        /// </value>
        public bool Out4 => this.OutputStates[3];

        /// <summary>
        /// Gets a value indicating whether this <see cref="OutputsMemory"/> is out5.
        /// </summary>
        /// <value>
        ///   <c>true</c> if out5; otherwise, <c>false</c>.
        /// </value>
        public bool Out5 => this.OutputStates[4];

        /// <summary>
        /// Gets a value indicating whether this <see cref="OutputsMemory"/> is out6.
        /// </summary>
        /// <value>
        ///   <c>true</c> if out6; otherwise, <c>false</c>.
        /// </value>
        public bool Out6 => this.OutputStates[5];

        /// <summary>
        /// Gets a value indicating whether this <see cref="OutputsMemory"/> is out7.
        /// </summary>
        /// <value>
        ///   <c>true</c> if out7; otherwise, <c>false</c>.
        /// </value>
        public bool Out7 => this.OutputStates[6];

        /// <summary>
        /// Gets a value indicating whether this <see cref="OutputsMemory"/> is out8.
        /// </summary>
        /// <value>
        ///   <c>true</c> if out8; otherwise, <c>false</c>.
        /// </value>
        public bool Out8 => this.OutputStates[7];

        /// <summary>
        /// Gets a value indicating whether this <see cref="OutputsMemory"/> is out9.
        /// </summary>
        /// <value>
        ///   <c>true</c> if out9; otherwise, <c>false</c>.
        /// </value>
        public bool Out9 => this.OutputStates[8];

        /// <summary>
        /// Gets a value indicating whether this <see cref="OutputsMemory"/> is out10.
        /// </summary>
        /// <value>
        ///   <c>true</c> if out10; otherwise, <c>false</c>.
        /// </value>
        public bool Out10 => this.OutputStates[9];

        /// <summary>
        /// Gets a value indicating whether this <see cref="OutputsMemory"/> is out11.
        /// </summary>
        /// <value>
        ///   <c>true</c> if out11; otherwise, <c>false</c>.
        /// </value>
        public bool Out11 => this.OutputStates[10];

        /// <summary>
        /// Gets a value indicating whether this <see cref="OutputsMemory"/> is out12.
        /// </summary>
        /// <value>
        ///   <c>true</c> if out12; otherwise, <c>false</c>.
        /// </value>
        public bool Out12 => this.OutputStates[11];

        /// <summary>
        /// Gets a value indicating whether this <see cref="OutputsMemory"/> is out13.
        /// </summary>
        /// <value>
        ///   <c>true</c> if out13; otherwise, <c>false</c>.
        /// </value>
        public bool Out13 => this.OutputStates[12];

        /// <summary>
        /// Gets a value indicating whether this <see cref="OutputsMemory"/> is out14.
        /// </summary>
        /// <value>
        ///   <c>true</c> if out14; otherwise, <c>false</c>.
        /// </value>
        public bool Out14 => this.OutputStates[13];

        /// <summary>
        /// Gets a value indicating whether this <see cref="OutputsMemory"/> is out15.
        /// </summary>
        /// <value>
        ///   <c>true</c> if out15; otherwise, <c>false</c>.
        /// </value>
        public bool Out15 => this.OutputStates[14];

        /// <summary>
        /// Gets a value indicating whether this <see cref="OutputsMemory"/> is out16.
        /// </summary>
        /// <value>
        ///   <c>true</c> if out16; otherwise, <c>false</c>.
        /// </value>
        public bool Out16 => this.OutputStates[15];
    }
}
