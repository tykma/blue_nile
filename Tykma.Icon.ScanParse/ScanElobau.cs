﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScanElobau.cs" company="Tykma Technologies">
//   All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Tykma.Icon.ScanParse
{
    using System.Linq;

    /// <summary>
    /// A custom scanner class for ELOBAU matrix codes.
    /// </summary>
    public class ScanElobau : ScanKeyValuePairs, IBaseDataReturn
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScanElobau"/> class.
        /// </summary>
        /// <param name="code">The code.</param>
        public ScanElobau(string code)
        {
            this.OriginalString = code;
            this.Data = string.Empty;
            this.SerialNumber = string.Empty;
            this.ConfirmationNumber = string.Empty;

            // We will receive a code separated by dashes, the first item being the job.
            var splitItems = code.Split('-');

            // The first item will be the job.
            if (splitItems.Any())
            {
                this.Data = splitItems[0];
            }

            if (splitItems.Count() >= 3 && this.Data != string.Empty)
            {
                this.FullyValidCode = true;
            }

            if (splitItems.Count() >= 2)
            {
                this.ConfirmationNumber = splitItems[1];
            }

            if (splitItems.Count() >= 3)
            {
                this.SerialNumber = splitItems[2];
            }

            for (int i = 3; i < splitItems.Length; i++)
            {
                this.Add(string.Format("ID{0}", i - 2), splitItems[i]);
            }
        }

        /// <summary>
        /// Gets or sets the data command.
        /// </summary>
        /// <value>
        /// The data command.
        /// </value>
        public string Data { get; set; }

        /// <summary>
        /// Gets the confirmation number.
        /// </summary>
        /// <value>
        /// The confirmation number.
        /// </value>
        public string ConfirmationNumber { get; private set; }

        /// <summary>
        /// Gets the serial number.
        /// </summary>
        /// <value>
        /// The serial number.
        /// </value>
        public string SerialNumber { get; private set; }

        /// <summary>
        /// Gets a value indicating whether [fully valid code].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [fully valid code]; otherwise, <c>false</c>.
        /// </value>
        public bool FullyValidCode { get; private set; }

        /// <summary>
        /// Gets the original string.
        /// </summary>
        /// <value>
        /// The original string.
        /// </value>
        public string OriginalString { get; private set; }
    }
}