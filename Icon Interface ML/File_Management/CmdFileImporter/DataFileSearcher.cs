﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataFileSearcher.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
// <summary>
// Search for data files that we could import.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Icon_Interface.File_Management.CmdFileImporter
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Timers;

    using NLog;

    using Tykma.Core.Entity;
    using Tykma.Icon.FileParse;

    /// <summary>
    /// Searches a folder for a file with data tags.
    /// </summary>
    internal sealed class DataFileSearcher : IDisposable
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The file parser.
        /// </summary>
        private readonly IFoundFileParserInterface fileParser = new TykmaParse();

        /// <summary>
        /// The file scan timer.
        /// </summary>
        private readonly Timer scanTimer;

        /// <summary>
        /// The INI settings.
        /// </summary>
        private readonly IApplicationSettings iniSettings;

        /// <summary>
        /// The list of imported data.
        /// </summary>
        private List<EntityInformation> listOfImportedData = new List<EntityInformation>();

        /// <summary>
        /// The oldest file found.
        /// </summary>
        private FileInfo oldestFile;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataFileSearcher"/> class.
        /// </summary>
        /// <param name="settings">Settings class.</param>
        internal DataFileSearcher(IApplicationSettings settings)
        {
            this.iniSettings = settings;
            this.scanTimer = new Timer();
            if (this.iniSettings.DATASEARCHSPEED > 0)
            {
                this.scanTimer.Interval = this.iniSettings.DATASEARCHSPEED;
            }

            this.scanTimer.Elapsed += this.ScanTimerElapsed;
            Logger.Debug("Scan time interval set to {0}", this.iniSettings.DATASEARCHSPEED);
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="DataFileSearcher"/> class.
        /// </summary>
        ~DataFileSearcher()
        {
            this.listOfImportedData = null;
        }

        /// <summary>
        /// Occurs when data is ready.
        /// </summary>
        public event EventHandler<FileSearchEventMessage> FileSearchEvent;

        /// <summary>
        /// Gets the data list.
        /// </summary>
        /// <value>
        /// The data list.
        /// </value>
        internal IEnumerable<EntityInformation> DataList => this.listOfImportedData;

        /// <summary>
        /// Gets the name of the job.
        /// </summary>
        /// <value>
        /// The name of the job.
        /// </value>
        internal string JobName { get; private set; }

        /// <summary>
        /// Gets the quantity.
        /// </summary>
        /// <value>
        /// The quantity.
        /// </value>
        internal int Quantity { get; private set; }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Enables the file searching timer.
        /// </summary>
        /// <param name="enableSearch">if set to <c>true</c> [enable search].</param>
        internal void Search(bool enableSearch)
        {
            if (enableSearch)
            {
                if (!this.scanTimer.Enabled)
                {
                    var args = new FileSearchEventMessage { Status = FileImportState.Searching };

                    this.FileSearchEvent?.Invoke(this, args);

                    if (this.scanTimer != null)
                    {
                        this.scanTimer.Interval = this.iniSettings.DATASEARCHSPEED;
                        this.scanTimer.Enabled = true;
                        Logger.Debug("File search enabled");
                    }
                }
            }
            else
            {
                if (this.scanTimer != null)
                {
                    this.scanTimer.Enabled = false;
                    Logger.Debug("File search disabled");
                }
            }
        }

        /// <summary>
        /// Moves the last file into done.
        /// </summary>
        /// <returns>
        /// Success state.
        /// </returns>
        internal bool MoveLastFile()
        {
            if (this.oldestFile == null)
            {
                return false;
            }

            // We need to make sure the file still exists and that the done folder still exists (and create it, if it does not).
            if (File.Exists(this.oldestFile.FullName))
            {
                if (Directory.Exists(this.iniSettings.DATADONEFOLDER))
                {
                }
                else
                {
                    Logger.Debug("Done folder does not exist, creating it.");
                    Directory.CreateDirectory(this.iniSettings.DATADONEFOLDER);
                }

                var newFileNameAndLocation = Path.Combine(this.iniSettings.DATADONEFOLDER, this.oldestFile.Name);

                // If this already exists, we'll need to change the name...
                if (File.Exists(newFileNameAndLocation))
                {
                    newFileNameAndLocation = Files.GetUniqueFilename(newFileNameAndLocation);
                    Logger.Debug("New done filename is: {0}", newFileNameAndLocation);
                }

                try
                {
                    File.Move(this.oldestFile.FullName, newFileNameAndLocation);
                    Logger.Debug("Moved done file to {0}", newFileNameAndLocation);
                }
                catch (Exception e)
                {
                    Logger.Fatal("Could not move file, error: {0}", e.Message);
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Moves the no mark file into NoMark\.
        /// </summary>
        internal void MoveNoMarkFile()
        {
            if (File.Exists(this.oldestFile.FullName))
            {
                if (!Directory.Exists(this.iniSettings.DATAERRORFOLDER))
                {
                    Logger.Debug("Error folder does not exist, creating it.");
                    Directory.CreateDirectory(this.iniSettings.DATAERRORFOLDER);
                }

                var newFileNameAndLocation = Path.Combine(this.iniSettings.DATAERRORFOLDER, this.oldestFile.Name);

                // If this already exists, we'll need to change the name...
                if (File.Exists(newFileNameAndLocation))
                {
                    newFileNameAndLocation = Files.GetUniqueFilename(newFileNameAndLocation);
                    Logger.Debug("New error filename is: {0}", newFileNameAndLocation);
                }

                this.LogNoMarkFile(newFileNameAndLocation);

                try
                {
                    File.Move(this.oldestFile.FullName, newFileNameAndLocation);
                    Logger.Debug("Moved error file to {0}", newFileNameAndLocation);
                }
                catch (Exception e)
                {
                    Logger.Fatal("Could not move error file, error: {0}", e.Message);
                }
            }
        }

        /// <summary>
        /// Logs the no mark file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        private void LogNoMarkFile(string fileName)
        {
            var noMarkLogFile = Path.Combine(this.iniSettings.DATAERRORFOLDER, "NoMark.log");

            using (var writer = new StreamWriter(noMarkLogFile, true))
            {
                writer.WriteLine("{0} - Could not parse file: {1}", DateTime.Now, fileName);
                Logger.Fatal("{0} - Could not parse file: {1}", DateTime.Now, fileName);
            }
        }

        /// <summary>
        /// The bulk of the clean-up code is implemented in Dispose(true).
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                this.scanTimer?.Dispose();
            }
        }

        /// <summary>
        /// Parses the data file.
        /// </summary>
        /// <param name="fileToParse">The file to parse.</param>
        private async void ParseDataFile(FileInfo fileToParse)
        {
            if (this.IsBinaryFile(fileToParse.FullName))
            {
                Logger.Fatal("File to parse appears to be a binary file");
                this.MoveLastFile();
                return;
            }

            // We will expect the data to be in this format.
            this.listOfImportedData.Clear();
            this.JobName = string.Empty;

            var args = new FileSearchEventMessage { Status = FileImportState.Parsing };

            this.FileSearchEvent?.Invoke(this, args);

            await Task.Delay(this.iniSettings.DATAWAITFORREAD);

            // The file could still be deleted.
            try
            {
                using (var ms = new MemoryStream(File.ReadAllBytes(fileToParse.FullName)))
                {
                    var fileData = this.fileParser.ParseJob(ms);
                    this.listOfImportedData = fileData.ListOfEntities;
                    this.JobName = fileData.JobName;
                    this.Quantity = fileData.Quantity;
                }
            }
            catch (Exception)
            {
                Logger.Error("File was created - and then deleted");
            }

            if (this.listOfImportedData.Count > 0)
            {
                var argsfound = new FileSearchEventMessage { Status = FileImportState.Complete };

                // We raise a finished event to let the main form know to pull our data.
                // If this event has no subscribers, it will be null.
                this.FileSearchEvent?.Invoke(this, argsfound);
            }
            else
            {
                this.MoveNoMarkFile();
                var argserror = new FileSearchEventMessage { Status = FileImportState.Error };

                // We raise a finished event to let the main form know to pull our data.
                // If this event has no subscribers, it will be null.
                this.FileSearchEvent?.Invoke(this, argserror);
            }
        }

        /// <summary>
        /// Determines whether [is binary file] [the specified file path].
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="sampleSize">Size of the sample.</param>
        /// <returns>
        ///   <c>true</c> if [is binary file] [the specified file path]; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="System.ArgumentException">File path is not valid;filePath.</exception>
        private bool IsBinaryFile(string filePath, int sampleSize = 10240)
        {
            if (!File.Exists(filePath))
            {
                return false;
            }

            var buffer = new char[sampleSize];
            string sampleContent = string.Empty;

            try
            {
                using (var sr = new StreamReader(filePath))
                {
                    int length = sr.Read(buffer, 0, sampleSize);
                    sampleContent = new string(buffer, 0, length);
                }
            }
            catch (IOException e)
            {
                Debug.Print(e.Message);
            }

            // Look for 4 consecutive binary zeroes.
            return sampleContent.Contains("\0\0\0\0");
        }

        /// <summary>
        /// Unimportant group timer elapsed.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="e">The <see cref="ElapsedEventArgs" /> instance containing the event data.</param>
        private void ScanTimerElapsed(object source, ElapsedEventArgs e)
        {
            // We will look for a new file here.
            // First, check that our file search directory exists.
            if (Directory.Exists(this.iniSettings.DATASEARCHFOLDER))
            {
                var searchDirectory = new DirectoryInfo(this.iniSettings.DATASEARCHFOLDER);
                this.oldestFile = searchDirectory.GetFiles().OrderByDescending(f => f.LastWriteTime).FirstOrDefault();

                if (this.oldestFile != null)
                {
                    // We have a file.
                    this.Search(false);
                    this.ParseDataFile(this.oldestFile);
                }
            }
        }
    }
}
