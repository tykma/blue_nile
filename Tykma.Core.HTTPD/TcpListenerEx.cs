﻿//--------------------------------------------------------------------
// <copyright file="TcpListenerEx.cs" company="Tykma Technologies">
// Copyright (c) Tykma Technologies. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Tykma.Core.HTTPD
{
    using System.Net;
    using System.Net.Sockets;

    /// <summary>
    /// Wrapper around TCP Listener that exposes the Active property.
    /// </summary>
    public class TcpListenerEx : TcpListener
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TcpListenerEx"/> class.
        /// </summary>
        /// <param name="localEndPoint">An <see cref="T:System.Net.IPEndPoint" /> that represents the local endpoint to which to bind the listener <see cref="T:System.Net.Sockets.Socket" />.</param>
        public TcpListenerEx(IPEndPoint localEndPoint)
            : base(localEndPoint)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TcpListenerEx"/> class.
        /// </summary>
        /// <param name="localaddr">An <see cref="T:System.Net.IPAddress" /> that represents the local IP address.</param>
        /// <param name="port">The port on which to listen for incoming connection attempts.</param>
        public TcpListenerEx(IPAddress localaddr, int port)
            : base(localaddr, port)
        {
        }

        /// <summary>
        /// Gets a value indicating whether <see cref="T:System.Net.Sockets.TcpListener" /> is actively listening for client connections.
        /// </summary>
        /// <returns>true if <see cref="T:System.Net.Sockets.TcpListener" /> is actively listening; otherwise, false.</returns>
        public new bool Active => base.Active;
    }
}
