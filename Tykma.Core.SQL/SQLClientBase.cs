﻿// <copyright file="SQLClientBase.cs" company="Tykma Electrox">
// Copyright (c) Tykma Electrox. All rights reserved.
// </copyright>

namespace Tykma.Core.SQL.Client
{
    /// <summary>
    /// Base properties for a tykma sql client.
    /// </summary>
    public class SQLClientBase
    {
        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>
        /// The connection string.
        /// </value>
        internal string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the SQL table.
        /// </summary>
        /// <value>
        /// The SQL table.
        /// </value>
        internal string SQLTable { get; set; }

        /// <summary>
        /// Gets or sets the SQL primary field.
        /// </summary>
        /// <value>
        /// The SQL primary field.
        /// </value>
        internal string SQLPrimaryField { get; set; }

        /// <summary>
        /// Gets or sets the SQL job column.
        /// </summary>
        /// <value>
        /// The SQL job column.
        /// </value>
        internal string SQLJobColumn { get; set; }

        /// <summary>
        /// Gets or sets the SQL quantity column.
        /// </summary>
        /// <value>
        /// The SQL quantity column.
        /// </value>
        internal string SQLQuantityColumn { get; set; }
    }
}
